<?php

    $s_kickoff_ini= "ut_index_runlatest.ini";

    $s_file_exists='Y';
    if(file_exists($s_kickoff_ini))
    {
        $array_lines = file($s_kickoff_ini);
    }
    else
    {
        $s_file_exists='N';
        echo "<br>ERROR: ut_index_runlatest.php - Err:001 <br>The config file ut_index_runlatest.ini does not exist";
        GOTO Z900_EXIT;
    }

    $i = 0;
    $s_line_in = "";
    $s_current_html  = "NONE";

B100_GET_REC:
    IF ($i >= count($array_lines))
    {
        goto B900_END;
    }
    $s_line_in = $array_lines[$i];
    IF (substr($s_line_in,0,1) == "*")
    {
        goto B200_GET_NEXT;
    }
    // set site session variables
    $ar_line_details = explode("|",$array_lines[$i]);
    IF (strtoupper(trim($ar_line_details[0])) == "CURRENT_HTML")
    {
        $s_current_html  = $ar_line_details[1];
        goto B200_GET_NEXT;
    }
B200_GET_NEXT:
    $i = $i + 1;
    goto B100_GET_REC;

B900_END:
    if($s_current_html  == "NONE"){
        ECHO "<br>ERROR: ut_index_runlatest.php - Err:002 <br>The config file ut_index_runlatest.ini does include the Current_html value";
        GOTO Z900_EXIT;
    }
    echo "ut_index_runlatest";
    $s_do_url = $s_current_html;
    header("Location: $s_do_url");


Z900_EXIT:


?>
