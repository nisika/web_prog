<?php
//gw20131104 - moved to web_prog
// gw 20120810 - get to work for Bolle orders, add company processing to deliveries

// http://devel.ncss.com.au/als_web_prog/dev/ut_kickoff.php?path=/var/www/html/ud_jm_dev/&init_process=runclass&class_library=class_ujp_process_events.php&function_name=cl_process&inifile=kickoff_ncss_linux.ini&debug=no

// http://dev.tdx.com.au/udelivered/sales-dev2/udj_process_events.php?runtype=ignoresched&comp_id=2
// http://dev.tdx.com.au/udelivered/sales-dev2/udj_process_events.php
// http://localhost/udelivered/proof-dev/udj_process_events.php?runtype=ignoresched&comp_id=3
// http://localhost/udelivered/proof-dev/udj_process_events.php?runtype=ignoresched&comp_id=3&sendtype=email
// http://localhost/udelivered/sales-dev2/udj_process_events.php?runtype=ignoresched&comp_id=2
// http://localhost/udelivered/proof-dev/udj_process_events.php?runtype=ignoresched&comp_id=3
// http://localhost/udelivered/proof-dev/udj_process_events.php?runtype=ignoresched&comp_id=25

    class class_udj_events
//gw20130131 - to get sales-test working    class class_kickoff
    {

function pf_customError($errno,$errstr,$errfile,$errline,$errcontext)
  {
// NOTE **** THIS ONLY CATCHES NON-FATAL ERRORS
//  echo "<b>Error gw12:</b> [$errno] $errstr $errfile $errline $errcontext.<br>";
// gw 20100315       switch ($error_level) {
       switch ($errno) {
        case E_USER_ERROR:
            echo "<b>My ERROR</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            echo "  Fatal error on line $errline in file $errfile";
            echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
            echo "Aborting...<br />\n";
            exit(1);
            break;

        case E_USER_WARNING:
            echo "<b>My WARNING</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        case E_USER_NOTICE:
            echo "<b>My NOTICE</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        default:
            echo "<BR>Unknown error type: [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;
        }

    return true;

 }

//    $sys_debug = strtoupper("yes");
//    $_SESSION['ko_prog_path'] = 'c:\\tdx\\web_prog\\dev\\';

function cl_process()
{
    $sys_debug = "NO";

A000_SET_RUN:
     //set error handler
    set_error_handler("pf_customError", E_ALL);
//gw20111114 - changed on server - pkn to update his code    date_default_timezone_set('Australia/Brisbane');

    $timezone = 'Australia/Sydney';

    date_default_timezone_set($timezone);
//    session_start();
    $sys_debug_text = "<br> gw set debug ";
    $sys_function_name = "class_jp_process_onarrived cl_process ";

    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." start of process";};
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." set session vars ";};
    $ps_job_start = microtime(true);

    if (isset($_SESSION['ko_prog_path']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
    }else{
    $_SESSION['ko_prog_path'] = "";
    }
    if (isset($_SESSION['ko_dbase_to_connectto']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
    }else{
    $_SESSION['ko_dbase_to_connectto'] = "tdxpryda";
    }

    if (isset($_SESSION['ko_map_path']))
    {
        $ko_map_path = $_SESSION['ko_map_path'];
    }
    else
    {
        $_SESSION['ko_map_path'] = "";
    }

    if (isset($_SESSION['ko_usrdata_path']))
    {
        $ko_usrdata_path = $_SESSION['ko_usrdata_path'];
    }
    else
    {
        $_SESSION['ko_usrdata_path'] = "";
    }

    if (isset($_SESSION['ko_email_runon_server']))
    {
    }else{
    $_SESSION['ko_email_runon_server'] = "dma.tdx.com.au";
    }

    $s_out = "";
    $s_out =     $s_out."<!-- v5 -->";

    $s_out = $s_out.'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">';
    $s_out = $s_out.'<html>';
    $s_out = $s_out.'<head>';
    $s_out = $s_out.'<title>udelivered_data_processing</title>';
    $s_out = $s_out.'<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">';
    echo $s_out;
    $s_out = "";
//    $s_out = $s_out.'<script language="JavaScript" type="text/javascript" src="js/ajax.js"></script>';
    $s_out = $s_out.'<script type="text/javascript">';
    $s_out = $s_out.'function toggle(element) {document.getElementById(element).style.display = (document.getElementById(element).style.display == "none") ? "" : "none";}';
    $s_out = $s_out.'</script>';
    $s_out = $s_out.'</head>';
    echo $s_out;
    $s_out = "";
    $s_out = $s_out.'<body topmargin="0">';
    $s_out = $s_out.'<form name="thisform" action="ut_action.php?p=501^logonid&map=udjj_bm_jobdetails.htm" method="post">';

    $s_out = $s_out.'<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" >';
    $s_out = $s_out.'    <tr height="40"><td background="http://www.udelivered.com/email_data/udp_bdymain_top_lc.png" width="20" height="40">&nbsp;</td><td><table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" ><tr><td background="http://www.udelivered.com/email_data/udp_bdymain_top_mid.png">&nbsp;</td></tr></table></td><td  align="right" background="http://www.udelivered.com/email_data/udp_bdymain_top_rc.png" width="20">&nbsp;</td></tr>';
    $s_out = $s_out.'    <tr >';
    $s_out = $s_out.'    <td background="http://www.udelivered.com/email_data/udp_bdymain_mid_l.png" width="20" height="40">&nbsp;</td><td valign="top">';
    $s_out = $s_out.'    <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" ><tr><td background="http://www.udelivered.com/email_data/udp_bdymain_mid_mid.png">';

//    $s_out = $s_out.'    <table width="100%" border="0"  cellpadding="0" cellspacing="0" >';
    $s_out = $s_out.'        <tr align="center"><td>';
    $s_out = $s_out.'        <img src="http://www.udelivered.com/email_data/ipad_splash_portrait_blue2_200.png"  alt=""  align="middle" border="0" title="uDelivered">';
    $s_out = $s_out.'    </td>';
    $s_out = $s_out.'    <td>';
    $s_out = $s_out.'    <table width="100%" align="right" border="0">';
    $s_out = $s_out.'       <tr><td colspan="6" >uDelivered - Home of Simple, Small and Smart Solutions for your business  <BR><hr></td></tr>';
    $s_out = $s_out.'           <tr class="udj_heading_row" align="center">';
    $s_out = $s_out.'               <td colspan="6">uDelivered Data Processing</td></tr>';
//    $s_out = $s_out.'<tr><td align="center" bgcolor="cyan" colspan="10"> Events Export Process </td></tr>';
//    $s_out = $s_out.'<tr><td width="50px">&nbsp;</td><td width="100px">&nbsp;</td><td width="600px">&nbsp;</td><td width="100px">&nbsp;</td><td width="100px">&nbsp;</td><td width="250">&nbsp;</td> </tr>';
    echo $s_out;


A000_DEFINE_VARIABLES:
    $sys_debug = "NO";
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." a000_define_vars";};
    $sys_prog_name = "";
    $ps_file_exists = '';
    $ps_filename="";
    $map = "";
    $tmp_array = array();
    $sys_function_out = "";
    $ps_temp_value = "";

    $s_developer_run = "NO";

    $ps_sessionno = "";

    $s_process_id=date("YmdHis");
    $s_eventtype_group = "NOGROUP";

    $s_eventtype_dom="";
    $s_eventtype_time="";

// gw 20120810 - set defaults
    $s_runtype="NORMALRUN";
    $s_comp_id="ALL";
    $s_sendtype='NOTSETUP';
    $s_Companyid = $s_comp_id;
    $get_forjobno = "";
    $get_resetsincecymdhms = "";

//GW20120810-makeconditional
     if (isset($_GET['runtype'])) $s_runtype=trim($_GET['runtype']);;
     if (isset($_GET['comp_id'])) $s_comp_id=trim($_GET['comp_id']);;
     if (isset($_GET['sendtype'])) $s_sendtype=trim($_GET['sendtype']);;
//gw20131111 - started but not implemented
     if (isset($_GET['forjobno'])) $get_forjobno=trim($_GET['forjobno']);;
//gw2013111 - added this
     if (isset($_GET['resetsince'])) $get_resetsincecymdhms=trim($_GET['resetsince']);;

    $s_runtype=strtoupper($s_runtype);
// gw 20120810    $s_comp_id=trim($_GET['comp_id']);
    // pkn 20120802 - not needed so commented out to avoid errors
    //$s_sendtype=trim($_GET['sendtype']);
    //$s_sendtype=strtoupper($s_sendtype);

    if (isset($_GET['developer_run']))
    {
        if (strtoupper(trim($_GET['developer_run']))<>"NO"){
            $s_developer_run=strtoupper(trim($_GET['developer_run']));
            $s_out = $s_out.'<tr><td width="50px">&nbsp;</td><td width="100px">&nbsp;</td><td width="600px">&nbsp;</td><td width="100px">&nbsp;</td><td width="100px">&nbsp;</td><td width="250">&nbsp;</td> </tr>';
            echo $s_out;
            echo '<tr><td colspan="10" bgcolor="#FF8080"> s_developer_run='.$s_developer_run.'</td></tr>';
            echo '<tr><td colspan="10" bgcolor="#FF8080">  s_runtype=[]'.$s_runtype.'[] s_comp_id=[]'.$s_comp_id.'[] s_sendtype=[]'.$s_sendtype.'[] </td></tr>';
            echo '<tr><td colspan="10" bgcolor="#FF8080">  get_forjobno = []'.$get_forjobno.'[] $get_resetsincecymdhms = []'.$get_resetsincecymdhms.'[] </td></tr>';
        }
    }
    ;


    echo '<tr><td colspan="10"> Your uDelivered Company Id is '.$s_comp_id.' and this is a '.$s_runtype.' run</td></tr>';

    $ps_file_count = "0";
    $ps_file_count_error = "0";
    $ps_file_count_skip = "0";
    $ps_file_count_unknown = "0";

    $s_test_utime = "NO_TEST_TIME";

    //    $s_test_utime = '1329440981';
A200_LOAD_LIBRARIES:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." a200_load_libraries";};

    global $class_main;
    global $class_sql;


     IF ($sys_debug == "YES"){echo "_get=";print_r($_GET);echo "<br>";};
     IF ($sys_debug == "YES"){echo "_post=";print_r($_POST);echo "<br>";};

A300_CONNECT_TODBASE:
     $dbcnx = $class_sql->c_sqlclient_connect();
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." A300_CONNECT_TODBASE";};

A400_GET_PARAMETERS:

A500_SET_VALUES:

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};
    $s_utime_search = time();
    $s_check_back_seconds = 600;
    //60 = 1min, 120=2min, 300=5min, 600=10min, 1200=20min, 3600=1hr, 7200=2hr, 14400=4hrs, 28800=8hrs, 86400=24hrs, 604800=7days
    $s_utime_since = $s_utime_search - $s_check_back_seconds;
    // pkn 20120220 - testing from a particular point in time
    //$s_utime_since = 1329695643;

    iF($s_test_utime <> "NO_TEST_TIME")
    {
        $s_utime_since = $s_test_utime;
//        echo "<BR><hr>";
        echo "<br> ********* a TEST TIME of ".$s_utime_since." = ".date("Ymd",$s_utime_since)." at ".date('H:m:s',$s_utime_since)."is being used ";
        echo "<BR><hr>";
    }

    $s_current_date=date("Ymd");
    $s_current_time=date("Hi");
    $s_current_day=date("D");
    $s_current_day=trim(strtoupper($s_current_day));
    $s_current_time_mm = substr($s_current_time,2,2);

    IF ($s_developer_run=="YES"){
        echo '<tr><td colspan="10">'.'Current Date='.$s_current_date.'  Time='.$s_current_time.' Day='.$s_current_day.' Minutes='.$s_current_time_mm.'</td></tr>';
    }

    $s_date_1_month = strtotime(date("Ymd", strtotime($s_current_date)) . "+1 month");
    $s_date_1_month = date("Ymd", $s_date_1_month);
    $s_date_1_week = strtotime(date("Ymd", strtotime($s_current_date)) . " +1 week");
    $s_date_1_week = date("Ymd", $s_date_1_week);
    $s_date_1_day = strtotime(date("Ymd", strtotime($s_current_date)) . " +1 day");
    $s_date_1_day = date("Ymd", $s_date_1_day);
    $s_date_1_hour = strtotime(date("Hi", strtotime($s_current_time)) . " +1 hour");
    $s_date_1_hour_hh = date("H", $s_date_1_hour);
    $s_date_1_hour = date("Hi", $s_date_1_hour);

    IF ($s_developer_run=="YES"){
        echo '<tr><td colspan="10">'.'Process selection +Day='.$s_date_1_day.' +Week='.$s_date_1_week.' +Month='.$s_date_1_month.' +Hour='.$s_date_1_hour.' +HH='.$s_date_1_hour_hh.'</td></tr>';
    }

    $s_last_processed = microtime(true);
    IF ($s_developer_run=="YES"){
        echo '<tr><td colspan="10">'.'Current processed time ='.$s_last_processed.'</td></tr>';
    }
    //exit();

B100_PROCESS:
    echo "<tr><td colspan='10' bgcolor='#FFC0FF'> Start of event processing  at ".date('H:m:s')."</td></tr>";
    IF ($s_developer_run=="YES"){
        echo "<tr><td> &nbsp;</td><td> &nbsp;</td><td colspan='9'> &nbsp;&nbsp;&nbsp; s_check_back_seconds=".$s_check_back_seconds."</td></tr>";
        echo "<tr><td> &nbsp;</td><td> &nbsp;</td><td colspan='9'>&nbsp;&nbsp;&nbsp; s_utime_search=".$s_utime_search." = ".date("Ymd",$s_utime_search)." at ".date('H:m:s',$s_utime_search)."</td></tr>";
        echo "<tr><td> &nbsp;</td><td> &nbsp;</td><td colspan='9'>&nbsp;&nbsp;&nbsp; s_utime_since=".$s_utime_since." = ".date("Ymd",$s_utime_since)." at ".date('H:m:s',$s_utime_since)."</td></tr>";
    }

    $s_company_id_sql='';

    $s_company_id_sql=" where CompanyId = '{$s_comp_id}' ";

//GW20131111 - ADDED RESET OF PROCESS ID TO ALLOW JOBS TO BE PREPROCESSED
    if ($s_runtype<>'RESETPROCESSID')
    {
        GOTO B105_CONTINUE;
    }
    IF ($s_developer_run=="YES"){
        echo "<tr><td> &nbsp;</td><td> &nbsp;</td><td colspan='9'> &nbsp;&nbsp;&nbsp; DOING THE RESETPROCES ID"."[]</td></tr>";
    }
    $s_sql = "update events set process_id = 'notdone'  ".$s_company_id_sql." and process_id like '".$get_resetsincecymdhms."%'";
    IF ($s_developer_run=="YES"){
        echo "<tr><td> &nbsp;</td><td> &nbsp;</td><td colspan='9'> &nbsp;&nbsp;&nbsp; s_sql=[]".$s_sql."[]</td></tr>";
    }
    $result_a = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    IF ($s_developer_run=="YES"){
        echo "<tr><td> &nbsp;</td><td> &nbsp;</td><td colspan='9'> &nbsp;&nbsp;&nbsp; s_sql=[]".$s_sql."[]</td></tr>";
        echo "<tr><td> &nbsp;</td><td> &nbsp;</td><td colspan='9'> &nbsp;&nbsp;&nbsp; result_a=[]".$result_a."[]</td></tr>";
    }

    $s_sql = "update deliveries set process_id = 'notdone'   ".$s_company_id_sql." and process_id like '".$get_resetsincecymdhms."%'";
    IF ($s_developer_run=="YES"){
        echo "<tr><td> &nbsp;</td><td> &nbsp;</td><td colspan='9'> &nbsp;&nbsp;&nbsp; s_sql=[]".$s_sql."[]</td></tr>";
    }
    $result_a = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    IF ($s_developer_run=="YES"){
        echo "<tr><td> &nbsp;</td><td> &nbsp;</td><td colspan='9'> &nbsp;&nbsp;&nbsp; s_sql=[]".$s_sql."[]</td></tr>";
        echo "<tr><td> &nbsp;</td><td> &nbsp;</td><td colspan='9'> &nbsp;&nbsp;&nbsp; result_a=[]".$result_a."[]</td></tr>";
    }
    GOTO Z900_EXIT;
B105_CONTINUE:
    if ($s_runtype<>'IGNORESCHED')
    {
        $s_company_id_sql='';
        GOTO B105_SKIP_IGNORESCHED;
    }

B105_TEST_COMPANY_EVENTS:
    $s_sql = "select distinct(GlobalEventTypeId), count(*) as rec_count from events ".$s_company_id_sql." and process_id = 'notdone' group by GlobalEventTypeId order by GlobalEventTypeId ASC";

    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};

    $i_record_count = "0";

B105_A:
    echo "<tr><td colspan='10' bgcolor='yellow'> Start of event summary for company on Ignoresched run  at ".date('H:m:s')." </td></tr>";
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B105_A";};
    $result_a = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_numrows = mysql_num_rows($result_a);
    if ($s_numrows == 0)
    {
        ECHO "<tr><td width='50px'> &nbsp;</td><td width='100%' colspan='10'> No events to be exported</td></tr>";
        IF ($s_developer_run=="YES"){
            ECHO "<tr><td width='50px'> &nbsp;</td><td width='100%' colspan='10'> No events to be exported <br> s_sql=".$s_sql."</td></tr>";
        }
        GOTO B900_EXIT;
    }else{
        echo "<tr><td width='50px'> &nbsp;</td><td width='100%' colspan='10'> Have located ".$s_numrows." types of events to be exported</td></tr>";
    }
    IF ($s_developer_run=="YES"){
        echo '<tr><td colspan="10" bgcolor="#FF8080"> s_sql='.$s_sql.'</td></tr>';
    }
    $i_count = 1;
B105_B_GET_RECORDS:
    $row_a = mysql_fetch_array($result_a, MYSQL_ASSOC);
    IF ($row_a === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B105_EXIT;
    }

    ECHO "<tr><td> &nbsp;</td><td colspan='10'> Record ".$i_count." of ".$s_numrows." ".trim($row_a["rec_count"])." of GlobalEventTypeId = ".trim($row_a["GlobalEventTypeId"])."</td></tr>";
    $i_count = $i_count + 1;
    GOTO B105_B_GET_RECORDS;
B105_EXIT:
B105_SKIP_IGNORESCHED:


B110_DO_PROCESS:
    echo "<tr><td colspan='10' bgcolor='cyan'> Start of company eventtype grouping and export  at ".date('H:m:s')." </td></tr>";

    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};
    //$s_sql = "SELECT DISTINCT(CompanyId) from events where DateReceived >= '".$s_utime_since."' group by CompanyId order by LogDate,CompanyId";
    //$s_sql = "SELECT DISTINCT(CompanyId) from events where process_id = '' group by CompanyId order by LogDate,CompanyId";
    $s_sql = "select * from eventtypes ".$s_company_id_sql." order by GlobalEventTypeId ASC";
    //echo 'pkn s_sql='.$s_sql.'<br>';
    IF ($s_developer_run=="YES"){
        echo '<tr><td colspan="10" bgcolor="#FF8080">'.date("Ymd").'" at '.date('H:m:s').' s_sql_eventtypes='.$s_sql.'</td></tr>';
    }
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};

    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result_b = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result_b);

    if ($s_numrows == 0)
    {
        ECHO "<BR> cl_jp_proc_onarrival no records found for s_sql=".$s_sql."<br>";
        goto B_900_END_RECORDS;
    }
    ECHO "<tr><td> &nbsp;</td><td colspan='10'> Located ".$s_numrows." event master records - Now processing the event exports for each event master "."</td></tr>";
//    ECHO "<a href='javascript:toggle('targets')'>Show Me</a>'";
    ECHO "</td></tr>";
//    echo "<div id='set_event_processid' style='display: none;'>";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result_b, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
    $i_record_count = $i_record_count + 1;

    $s_globaleventtypeid=trim($row["GlobalEventTypeId"]);
    $s_Companyid = trim($row["CompanyId"]);
    $s_process_type = trim(strtoupper($row["process_type"]));
    $s_process_time = trim(strtoupper($row["process_time"]));
    $s_display_name = trim(strtoupper($row["DisplayName"]));
    $s_eventtype_group = "NOGROUP";
// gw 20120426 - this is what it needs to be  $s_eventtype_group  = trim(strtoupper($row["export_group"]));
    $s_eventtype_group  = strtolower($s_display_name);
    $s_eventtype_group  = str_replace(" ","",$s_eventtype_group);
    $s_eventtype_group  = str_replace("_","",$s_eventtype_group);

    if (!(strpos($s_display_name,"ACT!") === false))
    {
        $s_eventtype_group  = "SAGEACT";
    }

    if ($s_runtype=='IGNORESCHED')
    {
        goto B_157_PROCESS_RECORDS;
    }

    if ($s_process_type=='MONTHLY')
    {
        goto B_151_CHECK_MONTHLY;
    }
    if ($s_process_type=='WEEKLY')
    {
        goto B_152_CHECK_WEEKLY;
    }
    if ($s_process_type=='DAILY')
    {
        goto B_153_CHECK_DAILY;
    }
    if ($s_process_type=='HOURLY')
    {
        goto B_154_CHECK_HOURLY;
    }
    if ($s_process_type=='MINUTES')
    {
        goto B_155_CHECK_MINUTES;
    }

    //goto B_157_PROCESS_RECORDS;
    goto B_158_NEXT;

B_151_CHECK_MONTHLY:

    //echo 's_globaleventtypeid='.$s_globaleventtypeid.'<br>';
    //echo 's_Companyid='.$s_Companyid.'<br>';
    //echo 's_process_type='.$s_process_type.'<br>';
    //echo 's_process_time='.$s_process_time.'<br>';
    //echo 's_last_processed='.$s_last_processed.'<br>';

    $ar_process_time=explode('|',$s_process_time);
    // pkn 20120302 - DOM = Date of Month
    $s_eventtype_dom=$ar_process_time[0];
    $s_eventtype_time=$ar_process_time[1];

    //echo 's_eventtype_dom='.$s_eventtype_dom.'<br>';
    //echo 's_eventtype_time='.$s_eventtype_time.'<br>';

    if ($s_current_date<>$s_eventtype_dom)
    {
        goto B_158_NEXT;
    }
    if ($s_current_time<$s_eventtype_time)
    {
        goto B_158_NEXT;
    }

    goto B_157_PROCESS_RECORDS;

B_152_CHECK_WEEKLY:

    //echo 's_globaleventtypeid='.$s_globaleventtypeid.'<br>';
    //echo 's_Companyid='.$s_Companyid.'<br>';
    //echo 's_process_type='.$s_process_type.'<br>';
    //echo 's_process_time='.$s_process_time.'<br>';
    //echo 's_last_processed='.$s_last_processed.'<br>';

    $ar_process_time=explode('|',$s_process_time);
    // pkn 20120302 - DOW = Date of Work
    $s_eventtype_date=$ar_process_time[0];
    $s_eventtype_dow=$ar_process_time[1];
    $s_eventtype_time=$ar_process_time[2];

    //echo 's_eventtype_date='.$s_eventtype_date.'<br>';
    //echo 's_eventtype_dow='.$s_eventtype_dow.'<br>';
    //echo 's_eventtype_time='.$s_eventtype_time.'<br>';

    if ($s_current_date<>$s_eventtype_date)
    {
        goto B_158_NEXT;
    }
    if ($s_current_day<>$s_eventtype_dow)
    {
        goto B_158_NEXT;
    }
    if ($s_current_time<$s_eventtype_time)
    {
        goto B_158_NEXT;
    }

    goto B_157_PROCESS_RECORDS;

B_153_CHECK_DAILY:

    //echo 's_globaleventtypeid='.$s_globaleventtypeid.'<br>';
    //echo 's_Companyid='.$s_Companyid.'<br>';
    //echo 's_process_type='.$s_process_type.'<br>';
    //echo 's_process_time='.$s_process_time.'<br>';
    //echo 's_last_processed='.$s_last_processed.'<br>';

    $s_eventtype_time=$s_process_time;

    //echo 's_eventtype_time='.$s_eventtype_time.'<br>';

    if ($s_current_time<$s_eventtype_time)
    {
        goto B_158_NEXT;
    }

    goto B_157_PROCESS_RECORDS;

B_154_CHECK_HOURLY:

    //echo 's_globaleventtypeid='.$s_globaleventtypeid.'<br>';
    //echo 's_Companyid='.$s_Companyid.'<br>';
    //echo 's_process_type='.$s_process_type.'<br>';
    //echo 's_process_time='.$s_process_time.'<br>';
    //echo 's_last_processed='.$s_last_processed.'<br>';

    $s_eventtype_time=$s_process_time;

    //echo 's_eventtype_time='.$s_eventtype_time.'<br>';
    //echo 's_current_time='.$s_current_time.'<br>';

    if ($s_current_time<$s_eventtype_time)
    {
        goto B_158_NEXT;
    }

    goto B_157_PROCESS_RECORDS;

B_155_CHECK_MINUTES:

    //goto B_158_NEXT;
    goto B_157_PROCESS_RECORDS;

B_157_PROCESS_RECORDS:

//    echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows."<br>";
//    echo "<BR>******************************************************************************************";
//    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp; Process Company ".$s_Companyid."";


    $s_full_process_id=$s_process_id.'_'.$s_Companyid.'_'.$s_eventtype_group.'_'.$s_globaleventtypeid;

    IF ($s_developer_run=="YES"){
        echo '<tr><td colspan="10" bgcolor="#80FF80">'.date("Ymd").' at '.date('H:m:s').' Processing '.$i_record_count.' of '.$s_numrows.'  Event '.$s_display_name.' ( '.$s_globaleventtypeid.' ) for  Company '.$s_Companyid.' begins </td></tr>';
        echo '<tr><td colspan="10" bgcolor="#FF8080">'.date("Ymd").' at '.date('H:m:s').'  PROCESS ID = []'.$s_full_process_id.'[] </td></tr>';
    }

    $s_temp = $this->fn_B100_do_company($dbcnx,$sys_debug, "CL_PROCESS B158", "NONE", $s_Companyid,$s_globaleventtypeid,$s_utime_since,$s_full_process_id,$s_developer_run,$s_sendtype);
     //." Globaleventid=".$row["GlobalEventId"]." GlobalEventTypeId=".$row["GlobalEventTypeId"];
    IF ($s_developer_run=="YES"){
        echo '<tr><td> &nbsp;</td><td colspan="10" bgcolor="C0FFC0">'.date("Ymd").' at '.date('H:m:s').' Complete Event '.$s_display_name.' for  Company '.$s_Companyid.'  </td></tr>';
    }

B_157_PROCESS_RECORDS_END:

    $s_update_process_time='';
    $s_update_eventtype='N';
    if ($s_process_type=='MONTHLY')
    {
        $s_update_process_time=$s_date_1_month.'|'.$s_eventtype_time;
        $s_update_eventtype='Y';
    }
    if ($s_process_type=='WEEKLY')
    {
        $s_update_process_time=$s_date_1_week.'|'.$s_eventtype_dow.'|'.$s_eventtype_time;
        $s_update_eventtype='Y';
    }
    if ($s_process_type=='DAILY')
    {
        $s_update_process_time=$s_eventtype_time;
        $s_update_eventtype='Y';
    }
    if ($s_process_type=='HOURLY')
    {
        $s_eventtype_time_mm=substr($s_eventtype_time,2,2);
        $s_update_process_time=$s_date_1_hour_hh.$s_eventtype_time_mm;
        $s_update_eventtype='Y';
    }
    if ($s_process_type=='MINUTES')
    {
        $s_eventtype_time_mm=substr($s_eventtype_time,2,2);
        $s_update_process_time=$s_date_1_hour_hh.$s_eventtype_time_mm;
        $s_update_eventtype='Y';
    }

    if ($s_update_eventtype<>'Y')
    {
        goto B_158_NEXT;
    }

    $insert_sql = "update eventtypes set process_time = '{$s_update_process_time}', last_processed = '{$s_last_processed}' where GlobalEventTypeId = '{$s_globaleventtypeid}' ";
    IF ($s_developer_run=="YES"){
        echo '<tr><td colspan="10" bgcolor="#FF8080">'.date("Ymd").'" at '.date('H:m:s').' insert_sql='.$insert_sql.'</td></tr>';
    }
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$insert_sql);

B_158_NEXT:
    IF ($s_developer_run=="YES"){
        echo '<tr><td colspan="10" bgcolor="#FF8080">'.date("Ymd").'" at '.date('H:m:s').' B158_GET_NEXT_RECORD</td></tr>';
    }

    GOTO B_100_GET_RECORDS;
B_159_END:

B_900_END_RECORDS:
//    echo "</div>";
    echo "<tr><td colspan='10' bgcolor='cyan'> End of company event processing  at ".date('H:m:s')."</td></tr>";

//    echo "</td></tr>";



/*
log start
update "last started"  utility record
get "last completed - events " utility record to display
get "last completed - delivereies " utility record to display

do events
// every event record is created with the Date Receved = server time received
    keep the current utime
    SELECT * FROM events where LogDate > utime(now) -1

    get all records not processed sorted by company and eventype
    process records

        get the actions for the company + eventtype + onarrival
        do the actions
        update record as processed
        next record


do deliveries
    same as events

update "last completed" utility reocrd with the current time OR the date received time of the last record processed
// *** potential is to miss a record if it is written between processing the last record and writting the utility record

log end

*/
B900_EXIT:
B999_END_PROCESS:


C100_POD_IMAGE_PROCESS:
    // pkn 20110829 - hard codes to Vellex 28 until a better method is created by Will
    //echo "<tr><td colspan='10' bgcolor='cyan'> Start of POD Image Processing at ".date('H:m:s')."</td></tr>";
    //$s_Companyid = "28";
    //$s_temp = $this->fn_D100_do_pod_images($dbcnx,$sys_debug, "CL_PROCESS D100_POD_IMAGE_PROCESS", "NONE", $s_Companyid,$s_utime_since,$s_process_id,$s_developer_run,$s_sendtype);
    //echo "<tr><td colspan='10' bgcolor='cyan'> End of POD Image Processing at ".date('H:m:s')."</td></tr>";
D100_DO_EXPORTS:
    echo "<tr><td colspan='10' bgcolor='cyan'> Start of Event Export and Date Exchange Processing at ".date('H:m:s')."</td></tr>";
    $s_temp = $this->fn_D300_do_event_exports($dbcnx,$sys_debug, "CL_PROCESS D300_DO_EXPORTS", "NONE", $s_Companyid,$s_utime_since,$s_process_id,$s_developer_run,$s_sendtype);
    echo "<tr><td colspan='10' bgcolor='cyan'> End of Export Processing at ".date('H:m:s')."</td></tr>";
E100_SALESORDER_PROCESS:
    echo "<tr><td colspan='10' bgcolor='cyan'> Start of SalesOrder Processing at ".date('H:m:s')."</td></tr>";
    $s_delivery_type='SALESORDER';
    $s_utime_since='';
    $s_temp = $this->fn_D400_do_deliveries($dbcnx,$sys_debug, "CL_PROCESS D400_DELIVERY_PROCESS", "NONE", $s_utime_since,$s_delivery_type,$s_process_id,$s_developer_run,$s_sendtype,$s_comp_id);
    echo "<tr><td colspan='10' bgcolor='cyan'> End of SalesOrder Processing at ".date('H:m:s')."</td></tr>";
F100_DELIVERY_PROCESS:
    echo "<tr><td colspan='10' bgcolor='cyan'> Start of Delivery Processing at ".date('H:m:s')."</td></tr>";
    $s_delivery_type='DELIVERY';
    $s_utime_since='';
    $s_temp = $this->fn_D400_do_deliveries($dbcnx,$sys_debug, "CL_PROCESS D400_DELIVERY_PROCESS", "NONE", $s_utime_since,$s_delivery_type,$s_process_id,$s_developer_run,$s_sendtype,$s_comp_id);
    echo "<tr><td colspan='10' bgcolor='cyan'> End of Delivery Processing at ".date('H:m:s')."</td></tr>";

C999_END_FILE_PROCESS:
    echo "<tr><td colspan='10' bgcolor='#FFC0FF'> End of all event procssing at ".date('H:m:s')."</td></tr>";
//    echo "</table>";
Z900_EXIT:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." Z900_EXIT ";};
    $s_out = "";

//gw20130408      $s_out = $s_out.'        <tr align="center"><td colspan="6">Unleash the power of the SAGE ACT! data kit - talk to you local SAGE ACT! consultant today</td></tr>';
    $s_out = $s_out.'        </table>';
    $s_out = $s_out.'     </TD></table>';
    $s_out = $s_out.'    </td><td  align="right" background="http://www.udelivered.com/email_data/udp_bdymain_mid_r.png" width="20">&nbsp;</td>';
    $s_out = $s_out.'    </tr>';
//    $s_out = $s_out.'    </table>';

//    $s_out = $s_out.'    </td></tr>';
    $s_out = $s_out.'    <tr height="50"><td background="http://www.udelivered.com/email_data/udp_bdymain_btm_lc.png" width="20" >&nbsp;</td><td><table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" ><tr><td background="http://www.udelivered.com/email_data/udp_bdymain_btm_mid.png">&nbsp;</td></tr></table></td><td  align="right" background="http://www.udelivered.com/email_data/udp_bdymain_btm_rc.png" width="20">&nbsp;</td></tr>';
    $s_out = $s_out.'    <tr><td height="100%">&nbsp;</td></tr>';
    $s_out = $s_out.'    </table>';

    $s_out = $s_out.'</form></body></html>';
    echo $s_out;

//END OF PRIMARY RUN
    }

function fn_B100_do_company($ps_dbcnx,$ps_debug,$ps_fromwhere,$ps_options,$ps_companyid,$ps_globaleventtypeid,$ps_utime_since,$ps_process_id,$ps_developer_run,$ps_sendtype)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    IF ($ps_developer_run=="YES"){
        echo '<tr><td colspan="10" bgcolor="#FF8080">'.date("Ymd").'" at '.date('H:m:s').' start of the b100_do_company function ps_companyid='.$ps_companyid.',ps_globaleventtypeid='.$ps_globaleventtypeid.',$ps_utime_since='.$ps_utime_since.',$ps_process_id='.$ps_process_id.'  </td></tr>';
    }

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_B100_do_company";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

    $s_log_file_path = trim($_SESSION['ko_log_path']);
    $s_log_file = $s_log_file_path.date("YmdHis").".txt";
    @ $fp = fopen($s_log_file,"a");
    flock($fp,2); //lock the file for writing

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;

    $ko_udelivered_type="DEFAULT";
    if (isset($_SESSION['ko_udelivered_type']))
    {
        $ko_udelivered_type=strtoupper(trim($_SESSION['ko_udelivered_type']));
    }
    $ko_udelivered_company_id='';
    if (isset($_SESSION['ko_udelivered_company_id']))
    {
        $ko_udelivered_company_id=strtoupper(trim($_SESSION['ko_udelivered_company_id']));
    }
    if (trim($ko_udelivered_company_id)=='')
    {
        $ko_udelivered_company_id='3';
    }
//    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ko_udelivered_company_id=".$ko_udelivered_company_id."<br>";
//    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ko_udelivered_type=".$ko_udelivered_type."<br>";
    //exit();

//    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Do Events for Company ".$ps_companyid."";

    IF ($ps_developer_run=="YES"){
        echo '<tr><td colspan="10" bgcolor="#FF8080">'.date("Ymd").'" at '.date('H:m:s').' b100_do_company function ko_udelivered_company_id='.$ko_udelivered_company_id.',ko_udelivered_type='.$ko_udelivered_type.  '</td></tr>';
        echo "<tr><td colspan='10'>";
    }

    $outputstring = date("Ymd")." at ".date('H:m:s')." ".$sys_function_name."   Do Events for Company ".$ps_companyid.""."\n";
    fwrite($fp, $outputstring);

    /*
    if ($ps_companyid == "28") //vellex
    {
        goto B000_START;
    }
    if ($ps_companyid == "2")  // demo@udelivered
    {
        goto B000_START;
    }*/

    //gw20120529 -  the company id does not need to match the one set in the kickoff.....
//    if ($ps_companyid === $ko_udelivered_company_id)
//    {
        goto B000_START;
//    }

//    IF ($ps_developer_run=="YES"){
//        echo '<tr><td colspan="10" bgcolor="#FF8080">'.date("Ymd").'" at '.date('H:m:s').' b100_do_company function '.$sys_function_name.'   Skipping Company []'.$ps_companyid.'[] ko_udelivered_company_id=[]'.$ko_udelivered_company_id.'[]</td></tr>';
//    }
//    goto Z900_EXIT;
B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};
// gw 20120218 - went to the param utime_since
// gw 201202    $s_utime_search = time();
// gw 201202    $s_check_back_seconds = 600;
// gw 201202    //60 = 1min, 120=2min, 300=5min, 600=10min, 1200=20min, 3600=1hr, 7200=2hr, 14400=4hrs, 28800=8hrs, 86400=24hrs, 604800=7days
// gw 201202    $s_utime_since = $s_utime_search - $s_check_back_seconds;
// gw 201202
// gw 201202    $s_utime_since = '1329440981';

    $s_utime_since = $ps_utime_since;

//    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp; Check All activity since utime=".$s_utime_since." = ".date("F j, Y, g:i a",$s_utime_since)."";
// gw 20120423    ECHO "<tr><td> &nbsp;</td><td colspan='10'> Processing all unprocessed records "."</td></tr>";

B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    //$s_sql = "SELECT DISTINCT(GlobalEventTypeId) from events where LogDate >= '".$s_utime_since."' and CompanyId = '".$ps_companyid."' group by GlobalEventTypeId order by GlobalEventTypeId";
    // pkn 20111102 -
    //$s_sql = "SELECT * from events where DateReceived >= '".$s_utime_since."' and CompanyId = '".$ps_companyid."' order by LogDate ASC";
    $s_sql = "SELECT * from events where process_id = 'notdone' and CompanyId = '{$ps_companyid}' and GlobalEventTypeId = '{$ps_globaleventtypeid}' order by LogDate ASC";
//    echo 'company s_sql='.$s_sql.'<br>';
    IF ($ps_developer_run=="YES"){
        echo '<tr><td colspan="10" bgcolor="#FF8080">'.date("Ymd").'" at '.date('H:m:s').' b100_do_company function company s_sql='.$s_sql.'</td></tr>';
    }
    //exit();
    $outputstring = 'events sql: '.$s_sql."\n";
    fwrite($fp, $outputstring);

    //exit();
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        IF ($ps_developer_run=="YES"){
            ECHO "<tr><td> &nbsp;</td><td colspan='10'> No records located to export "."</td></tr>";
        }
//        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql."";
//        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";

        $outputstring = 'no records found for this run'."\n";
        fwrite($fp, $outputstring);

        goto B_900_END_RECORDS;
    }
// gw 20120418    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql."";
// gw 20120418    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";
    IF ($ps_developer_run=="YES"){
        echo '<tr><td colspan="10" bgcolor="#FF8080">'.date("Ymd").'" at '.date('H:m:s').' b100_do_company function s_numrows='.$s_numrows.' company s_sql='.$s_sql.'</td></tr>';
    }

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
//    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name."  s_sql = ".$s_sql."<BR>key1=".$row['key1']."  key2 =".$row['key2']." key3= ".$row['key3']." key4= ".$row['key4'];};

B_158_NEXT:
    $i_record_count = $i_record_count + 1;

//    echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows."<br>";
    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." ######################</td></tr>";
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." Process  GlobalEventTypeId=".$row["GlobalEventTypeId"]."</td></tr>";
    }
    $s_temp = $this->fn_C200_check_data_integration($dbcnx,$sys_debug, "fn_B100_do_company B158", "NONE", $ps_companyid,$row["GlobalEventTypeId"],$ps_developer_run,$ps_sendtype);
    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." Result from data integration check s_temp=".$s_temp."</td></tr>";
    }
    if (strtoupper($s_temp) <> "YES")
    {
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; So skip the GlobalEventTypeId";
        GOTO B_800_NEXT_REC;
    }
    IF ($ps_developer_run=="YES"){
        echo '<tr><td colspan="10" bgcolor="#FF8080">find out if the event type has a data integration record for the companay  </td></tr>';
        echo '<tr><td colspan="10" bgcolor="#FF8080">if not skip the entire event   </td></tr>';
        echo '<tr><td colspan="10" bgcolor="#FF8080">else do same DI for each record  </td></tr>';
    }

    $s_globaleventid=$row["GlobalEventId"];
    $s_GlobalDeliveryId=$row["GlobalDeliveryId"];
    $s_GlobalEventTypeId=$row["GlobalEventTypeId"];
    $s_eventnotes=trim($row["Note"]);
    $s_GlobalEventTypeId=$row["GlobalEventTypeId"];

    IF ($ps_developer_run=="YES"){
        echo '<tr><td colspan="10" bgcolor="#FF8080">s_globaleventid1='.$s_globaleventid.'  </td></tr>';
        echo '<tr><td colspan="10" bgcolor="#FF8080">s_GlobalDeliveryId1='.$s_GlobalDeliveryId.' </td></tr>';
    }

    // pkn 20120305 - using process_id so dont need this anymore
    /*
    $s_event_rec_exists='NO';
    $outputstring = 'checking if an event status record already exists for eventid:'.$s_globaleventid."\n";
    fwrite($fp, $outputstring);

    if ($ko_udelivered_type=='FIELD_SERVICE')
    {
        $s_event_rec_exists=$this->fn_F100_check_event_status($dbcnx,$sys_debug, "fn_B100_do_company B158", "NONE", $ps_companyid, $s_globaleventid,$fp);
    }

    $outputstring = 'eventid:'.$s_globaleventid.' event rec exists='.$s_event_rec_exists."\n";
    fwrite($fp, $outputstring);

    echo '<br>Devrun---s_event_rec_exists='.$s_event_rec_exists.'';
    echo '<br>Devrun---s_globaleventid='.$s_globaleventid.'';

    // pkn 20111107 - if event status record exists for event, event has already been done so skip and goto next record
    if ($s_event_rec_exists == "YES")
    {
        echo '<br>Devrun---s_globaleventid='.$s_globaleventid.' is being skipped'.'';
        $outputstring = 's_globaleventid='.$s_globaleventid.' is being skipped'."\n";
        fwrite($fp, $outputstring);

        goto B_800_NEXT_REC;
    }

    if ($ko_udelivered_type=='FIELD_SERVICE')
    {
        $this->fn_F200_create_event_status($dbcnx,$sys_debug, "fn_B100_do_company B158", "NONE", $ps_companyid, $s_globaleventid,$fp);
    }
    */

    //if ($ko_udelivered_type=='SALES_ORDER')
    //{
    //    echo '<br>Devrun--- s_GlobalDeliveryId='.$s_GlobalDeliveryId.'';
    //    $this->fn_G100_create_sales_order_xml($dbcnx,$sys_debug, "fn_B100_do_company B158", "NONE", $ps_companyid, $s_globaleventid,$s_GlobalDeliveryId,$s_GlobalEventTypeId,$s_eventnotes,$ps_process_id,$fp);
    //}

    //exit();

    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." Start Events for GlobalEventTypeId=".$row["GlobalEventTypeId"]."</td></tr>";
    }

    $outputstring = 'Start Events for GlobalEventTypeId='.$s_globaleventid."\n";
    fwrite($fp, $outputstring);

    //$s_temp = $this->fn_C100_do_event($dbcnx,$sys_debug, "fn_B100_do_company B158", "NONE", $ps_companyid,$row["GlobalEventTypeId"] );
    $s_temp = $this->fn_C100_do_event($dbcnx,$sys_debug,"fn_B100_do_company B158","NONE",$ps_companyid,$s_globaleventid,$ps_utime_since,$ps_process_id,$ps_developer_run,$ps_sendtype);
    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." Complete GlobalEventTypeId=".$row["GlobalEventTypeId"]."</td></tr>";
    }

B_800_NEXT_REC:

// pkn 20120301 - update process_id to datetime so to signify the event has been processed

    $s_update_sql="update events set process_id = '{$ps_process_id}' where GlobalEventId = '{$s_globaleventid}' ";
    IF ($ps_developer_run=="YES"){
        echo '<tr><td colspan="10" bgcolor="#FF8080">s_update_sql='.$s_update_sql.'  </td></tr>';
    }
    $update_result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_update_sql);

    GOTO B_100_GET_RECORDS;
B_900_END_RECORDS:


Z900_EXIT:
    flock($fp,3); //release the write lock
    fclose($fp);

    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    return $sys_function_out;
}


//function fn_C100_do_event($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid,$ps_GlobalEventTypeId )
function fn_C100_do_event($ps_dbcnx,$ps_debug,$ps_fromwhere,$ps_options,$ps_companyid,$ps_globaleventid,$ps_utime_since,$ps_process_id,$ps_developer_run,$ps_sendtype)
{
     require_once($_SESSION['ko_prog_path'].'lib/class_process_jobline.php');
     $class_clprocessjobline = new clprocessjobline();

A010_START:
    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_C100_do_event";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;

    //echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."  Do records for Company ".$ps_companyid." and GlobalEventTypeId=".$ps_GlobalEventTypeId;
    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')."".$sys_function_name."  Do records for Company ".$ps_companyid." and globaleventid=".$ps_globaleventid."</td></tr>";
    }

// gw 20120429 - this whole section is really for procedssing job related exports

    if ($ps_companyid == "28") //vellex
    {
        goto b120_check_28;
    }
    if ($ps_companyid == "2")  // demo@udelivered
    {
        goto b110_check_2;
    }
    if ($ps_companyid == "3")  // ncss
    {
        goto B000_START;
    }
//    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='red'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Skipping events for Company ".$ps_companyid."</td></tr>";
    goto Z900_EXIT;
b110_check_2:
    if ($ps_globaleventid == "391")  // delivery scans
    {
        goto B000_START;
    }
    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Skipping this event  ".$ps_globaleventid."</td></tr>";
    }
    goto Z900_EXIT;
b120_check_28:

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};

// gw 20120218 used the param time
// gw 201202    $s_utime_search = time();
// gw 201202    $s_check_back_seconds = 600;
// gw 201202    //60 = 1min, 120=2min, 300=5min, 600=10min, 1200=20min, 3600=1hr, 7200=2hr, 14400=4hrs, 28800=8hrs, 86400=24hrs, 604800=7days
// gw 201202    $s_utime_since = $s_utime_search - $s_check_back_seconds;
// gw 201202    $s_utime_since = '1329440981';

    $s_utime_since = $ps_utime_since;
B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};
    //$s_sql = "SELECT * from events where LogDate >= '".$s_utime_since."' and CompanyId = '".$ps_companyid."' and GlobalEventTypeId = ".$ps_GlobalEventTypeId."  ";
    $s_sql = "SELECT * from events where CompanyId = '".$ps_companyid."' and GlobalEventId = ".$ps_globaleventid." and process_id = 'notdone' ";

    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td>event s_sql=".$s_sql."</td></tr>";
    }

    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql."</td></tr>";
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."ref:b000&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."</td></tr>";
        goto B_900_END_RECORDS;
    }
    IF ($ps_developer_run=="YES"){
        echo '<tr><td colspan="10" bgcolor="#FF8080">'.date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql."";
        echo '<tr><td colspan="10" bgcolor="#FF8080">'.date("Ymd")." at ".date('H:m:s')."ref:b000a&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";
    }

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
//    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name."  s_sql = ".$s_sql."<BR>key1=".$row['key1']."  key2 =".$row['key2']." key3= ".$row['key3']." key4= ".$row['key4'];};

    if (isset($_SESSION['ko_udelivered_type']))
    {
        $ko_udelivered_type=strtoupper(trim($_SESSION['ko_udelivered_type']));
    }
    $ko_udelivered_type='FIELD_SERVICE';
    if ($ko_udelivered_type <> 'FIELD_SERVICE')
    {
            goto B_158_NEXT;
    }
    $s_GlobalEventId = trim($row["GlobalEventId"]);
    $s_GlobalEventTypeId = trim($row["GlobalEventTypeId"]);
    $s_GlobalDeliveryId = trim($row["GlobalDeliveryId"]);
    $s_event_note = trim($row["Note"]);
    $s_event_date = trim($row["Date"]);
    $s_event_logdate = trim($row["LogDate"]);

    $s_event_fieldname_def='|';
    $s_event_fieldname_data='|';

    $s_event_fieldname_def=$s_event_fieldname_def.'UD_date|UD_logdate|POSTV_process_id|';
    $s_event_fieldname_data=$s_event_fieldname_data.$s_event_date.'|'.$s_event_logdate.'|'.$ps_process_id.'|';

    // pkn 20110117 - start from here tomorrow
    //echo 's_GlobalEventId='.$s_GlobalEventId.'<br>';
    //echo 's_GlobalEventTypeId='.$s_GlobalEventTypeId.'<br>';
    //echo 's_GlobalDeliveryId='.$s_GlobalDeliveryId.'<br>';
    //echo 's_event_note='.$s_event_note.'<br>';

    $s_event_note=strtr($s_event_note,chr(13),'|'); // replace carriage return with |
    $s_event_note=strtr($s_event_note,chr(10),'|'); // replace line feed with |
    $s_event_note=str_replace('||','|',$s_event_note);
    $s_event_note=str_replace("'"," ",$s_event_note);

    if (strpos($s_event_note,"|") === false )
    {
        if (strpos($s_event_note,":=:") === false )
        {
            $s_event_fieldname_def=$s_event_fieldname_def.'UD_note_field|';
            $s_event_fieldname_data=$s_event_fieldname_data.$s_event_note.'|';
            goto B_157_GET_JOB_DETS;
        }
    }

B_156_DO_ENTERED_VALUES:
    $ar_event_note=explode('|',$s_event_note);
    $cnt=0;
    while($ar_event_note[$cnt]<>'')
    {
        $ar_event_note_details=explode(':=:',$ar_event_note[$cnt]);
        $s_event_fieldname=trim($ar_event_note_details[0]);
        $s_event_fieldname = str_replace(" ","_",$s_event_fieldname);
        $s_event_fieldvalue=trim($ar_event_note_details[1]);
        $s_event_fieldname_def=$s_event_fieldname_def.'UD_'.$s_event_fieldname.'|';
        $s_event_fieldname_data=$s_event_fieldname_data.$s_event_fieldvalue.'|';
        $cnt++;
    }
B_157_GET_JOB_DETS:

    $s_temp = $this->fn_E100_get_cn_number_fields($dbcnx,$sys_debug, "CL_PROCESS E100_GET_CN_NUMBER", "NONE", $ps_companyid, $s_GlobalDeliveryId,$ps_developer_run,$ps_sendtype);
    if (trim($s_temp=='|^%##%^|'))
    {
        goto B_158_NEXT;
    }
    if (strpos($s_temp,"|^%##%^|") ===false)
    {
        goto B_158_NEXT;
    }else{
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_ud_delivery_details_def = "|".$ar_line_details[0];
        $s_ud_delivery_details_data = "|".$ar_line_details[1];
        $s_temp = "";
    };

    $s_job_number = $class_main->clmain_v300_set_variable("REC_uddev_CNNO",$s_ud_delivery_details_def,$s_ud_delivery_details_data," NO","11","UJP_PROCESS_EVENT C100 B158");
    $s_temp = $this->fn_E200_get_jobs_id_fields($dbcnx,$sys_debug, "CL_PROCESS E200_GET_JOBS_ID", "NONE", $ps_companyid, $s_job_number,$ps_developer_run,$ps_sendtype);
    $s_skip_u720_process='N';
    if (strpos($s_temp,"|^%##%^|") ===false)
    {
    }else{
        $ar_line_details = explode("|^%##%^|",$s_temp);
        if (trim($ar_line_details[0])=='' and trim($ar_line_details[1])=='')
        {
            // pkn 20120402 = if job is not found then skip u720 process below
            $s_skip_u720_process='Y';
        }
        $s_jm_details_def = "|".$ar_line_details[0];
        $s_jm_details_data = "|".$ar_line_details[1];
        $s_temp = "";
    };

    $s_temp = $this->fn_E310_get_event_type_fields($dbcnx,$sys_debug, "CL_PROCESS E300_GET_EVENT_TYPE", "NONE", $ps_companyid, $s_GlobalEventTypeId,$ps_developer_run,$ps_sendtype);
    if (strpos($s_temp,"|^%##%^|") ===false)
    {
    }
    else
    {
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_ev_details_def = "|".$ar_line_details[0];
        $s_ev_details_data = "|".$ar_line_details[1];

        $s_temp = "";
    };

    $_COOKIE['ud_logonname']='ud_process_events';
    $_COOKIE['ud_companyid']='3';
    $xml_arr='';
    $s_sessionno='11';
    $s_details_def='START'.$s_ud_delivery_details_def.$s_jm_details_def.$s_ev_details_def.$s_event_fieldname_def;
    $s_details_data='START'.$s_ud_delivery_details_data.$s_jm_details_data.$s_ev_details_data.$s_event_fieldname_data;

    $timezone = date_default_timezone_get();
    $_SESSION[$s_sessionno.'ut_logon_timezone']=$timezone;

    if ($s_skip_u720_process=='Y')
    {
        goto B_158_NEXT;
    }

    $s_actiondetails="sys_classV2|v1|jcl_result|clmain_u720_do_process(jobs_masters,jm_id,#p%!_REC_jm_jobs_id_!%#p,udelivered,#p%!_REC_ev_DisplayName_!%#p,NO,udj_process_events)|NO|END";
    $s_returned = $class_clprocessjobline->pf_clprocessjobline($ps_dbcnx,$s_actiondetails,$s_details_def,$s_details_data,$xml_arr,$s_sessionno,'YES');

/*
    $s_job_number = $this->fn_E100_get_cn_number($dbcnx,$sys_debug, "CL_PROCESS E100_GET_CN_NUMBER", "NONE", $ps_companyid, $s_GlobalDeliveryId);
    $s_jobs_id = $this->fn_E200_get_jobs_id($dbcnx,$sys_debug, "CL_PROCESS E200_GET_JOBS_ID", "NONE", $ps_companyid, $s_job_number);
    $s_event_name = $this->fn_E300_get_event_type($dbcnx,$sys_debug, "CL_PROCESS E200_GET_EVENT_TYPE", "NONE", $ps_companyid, $s_GlobalEventTypeId);

    $s_jobs_id=trim($s_jobs_id);

    if ($s_jobs_id=='')
    {
        goto B_158_NEXT;
    }

    echo 's_job_number='.$s_job_number.'<br>';
    echo 's_jobs_id='.$s_jobs_id.'<br>';
    echo 's_event_name='.$s_event_name.'<br>';

    $_COOKIE['ud_logonname']='ud_process_events';
    $_COOKIE['ud_companyid']='3';
    $xml_arr='';
    $s_sessionno='11';
    $s_details_def='START|POSTV_jobs_id|POSTV_companyid_job_no|POSTV_event_name'.$s_event_fieldname_def;
    $s_details_data='START|'.$s_jobs_id.'|'.$s_job_number.'|'.$s_event_name.$s_event_fieldname_data;
//    echo 's_details_def='.$s_details_def.'<br>';
//    echo 's_details_data='.$s_details_data.'<br>';

    $s_actiondetails="sys_classV2|v1|jcl_result|clmain_u720_do_process(jobs_masters,jm_id,#p%!_postv_jobs_id_!%#p,udelivered,#p%!_postv_event_name_!%#p,NO,udj_process_events)|NO|END";
    $s_returned = $class_clprocessjobline->pf_clprocessjobline($ps_dbcnx,$s_actiondetails,$s_details_def,$s_details_data,$xml_arr,$s_sessionno,'NO');
*/

B_158_NEXT:
    $i_record_count = $i_record_count + 1;

    $s_GlobalEventId = $row["GlobalEventId"];
    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Process Event s_GlobalEventId = ".$s_GlobalEventId." (REF:A)"."</td></tr>";
B_200_DO_TASKS:

B_290_END:
    GOTO B_880_GET_RECORDS;


B_880_GET_RECORDS:
    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Complete Event s_GlobalEventId = ".$s_GlobalEventId.""."</td></tr>";
    GOTO B_100_GET_RECORDS;





B_159_END:
        exit();

B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    return $sys_function_out;
}
//#################################################################################################################################################################################################
function fn_C200_check_data_integration($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid,$ps_GlobalEventTypeId ,$ps_developer_run,$ps_sendtype)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "";


    $sys_function_out = "YES";
    goto Z900_EXIT;

    // get the data_integration records
    // if exist = YES
    // if not == NO

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_C200_check_data_integration";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;

//    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Do records for Company ".$ps_companyid." and GlobalEventTypeId=".$ps_GlobalEventTypeId;
B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};
B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    $s_sql = "SELECT * from events where LogDate >= '".$s_utime_since."' and CompanyId = '".$ps_companyid."' and GlobalEventTypeId = ".$ps_GlobalEventTypeId."  ";
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql.""."</td></tr>";
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."ref:c200_b000&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";
        goto B_900_END_RECORDS;
    }
    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql.""."</td></tr>";
    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."ref:c200_b000a&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
//    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name."  s_sql = ".$s_sql."<BR>key1=".$row['key1']."  key2 =".$row['key2']." key3= ".$row['key3']." key4= ".$row['key4'];};

B_158_NEXT:
    $i_record_count = $i_record_count + 1;

    $s_GlobalEventId = $row["GlobalEventId"];
    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Process Event s_GlobalEventId = ".$s_GlobalEventId."(REF:B)"."</td></tr>";

    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Complete Event s_GlobalEventId = ".$s_GlobalEventId.""."</td></tr>";

    GOTO B_100_GET_RECORDS;
B_159_END:

B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    return $sys_function_out;
}

//#################################################################################################################################################################################################
function fn_D100_do_pod_images($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid,$ps_utime_since,$ps_developer_run,$ps_sendtype)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_D100_do_pod_images";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;

// gw 20120424    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Do POD Images for Company ".$ps_companyid.""."</td></tr>";

    if ($ps_companyid == "28") //vellex
    {
        goto B000_START;
    }

    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Skipping Company ".$ps_companyid.""."</td></tr>";
    goto Z900_EXIT;
B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};
// gw 20120218 - moved to param
// gw 201202    $s_utime_search = time();
// gw 201202    $s_check_back_seconds = 600;
// gw 201202    //60 = 1min, 120=2min, 300=5min, 600=10min, 1200=20min, 3600=1hr, 7200=2hr, 14400=4hrs, 28800=8hrs, 86400=24hrs, 604800=7days
// gw 201202    $s_utime_since = $s_utime_search - $s_check_back_seconds;
// gw 201202    $s_utime_since = '1329440981';

    $s_utime_since = $ps_utime_since;


B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    $s_sql = " select * from deliveries where DeliveryDate > '{$s_utime_since}' and companyid = '{$ps_companyid}' order by DeliveryDate" ;
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<tr><td>&nbsp;</td><td colspan='10'> at ".date('H:m:s')."  NO Proof of Delivery records found "."</td></tr>";
        goto B_900_END_RECORDS;
    }
    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql.""."</td></tr>";
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."ref:d100_b000&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";
    }
B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
    $s_globaldeliveryid = trim($row['GlobalDeliveryId']);
    $s_cn_number = trim($row['CNNO']);

    $s_temp = $this->fn_D200_send_pod_image($dbcnx,$sys_debug, "fn_D100_do_pod_images B150", "NONE", $s_globaldeliveryid,$ps_companyid,$s_cn_number,$ps_utime_since ,$ps_developer_run,$ps_sendtype);

B_158_NEXT:
    $i_record_count = $i_record_count + 1;

B_800_NEXT_REC:
    GOTO B_100_GET_RECORDS;
B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    return $sys_function_out;
}
//#################################################################################################################################################################################################
function fn_D200_send_pod_image($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options,$ps_globaldeliveryid,$ps_companyid,$ps_cn_number,$ps_utime_since ,$ps_developer_run,$ps_sendtype)
{
    A010_START:
    $sys_debug_text = "";
    $sys_debug = "";
    $s_image_cnt=0;

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_D200_send_pod_image";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};
// gw 20120218 - moved to param    $s_utime_search = time();
// gw 201202    $s_check_back_seconds = 600;
// gw 201202    //60 = 1min, 120=2min, 300=5min, 600=10min, 1200=20min, 3600=1hr, 7200=2hr, 14400=4hrs, 28800=8hrs, 86400=24hrs, 604800=7days
// gw 201202    $s_utime_since = $s_utime_search - $s_check_back_seconds;
// gw 201202    $s_utime_since = '1329440981';

    $s_utime_since = $ps_utime_since;
B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    $s_sql = " select * from attachments where globaldeliveryid = '{$ps_globaldeliveryid}' ";
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql.""."</td></tr>";
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."ref:d200_b000&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";
        goto B_900_END_RECORDS;
    }
    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql.""."</td></tr>";
    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."ref:d200_b000&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
    $s_image_cnt++;

    $s_pod_filename = $row["FileName"];
    $s_pod_filepath = "C:\\tdx\\html\\udelivered\\proof2\\usrdata\\".$ps_companyid."\\";
    $s_pod_file_from = $s_pod_filepath.$s_pod_filename;

B151_CREATE_CN_POD_FILE:
    $s_pod_tco="";
    if ($ps_companyid=="28")
    {
        $s_pod_tco="VELLA";
        $s_pod_company="VELLA";
    }

    $s_pod_filename_to=$s_pod_tco.'_'.$s_pod_company.'_'.$ps_cn_number.'_'.$s_image_cnt.'.JPG';
    $s_pod_filepath_to="C:\\tdx\\html\\ftp\\udelivered\\from_ud\\";
    $s_pod_file_to=$s_pod_filepath_to.$s_pod_filename_to;

    if (!copy($s_pod_file_from, $s_pod_file_to))
    {
        echo "failed to copy $s_pod_file_from...\n";
    }

B_158_NEXT:
    $i_record_count = $i_record_count + 1;

B_200_FTP_POD_FILE:
    $s_site_address="vellaexpress.com.au";
    $s_upload_folder="";
    $s_username="vell_ud";
    $s_password="vell_ud";
    $s_upload_file=$s_pod_file_to;
    $s_ftp_mode="PASV";

    $this->fn_Z200_ftp_file($s_site_address,$s_upload_folder,$s_username,$s_password,$s_upload_file,$s_ftp_mode,$ps_developer_run,$ps_sendtype);

B_210_ARCHIVE_FILE:
    $s_archive_date=date("Ymd");
    $s_archive_path=$s_pod_filepath_to.$s_archive_date;
    $s_md = @mkdir($s_archive_path,0777);
    $s_archive_path=$s_archive_path."\\";
    rename("{$s_pod_file_to}","{$s_archive_path}{$s_pod_filename_to}");

B_290_END:
    GOTO B_880_GET_RECORDS;


B_880_GET_RECORDS:
    GOTO B_100_GET_RECORDS;
B_159_END:

B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of function sys_function_out = ".$sys_function_out."<br>";};
    return $sys_function_out;
}

function fn_D300_do_event_exports($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options,$ps_utime_since,$ps_delivery_type,$ps_process_id,$ps_developer_run,$ps_sendtype)
{
    require_once($_SESSION['ko_prog_path'].'lib/class_process_jobline.php');
    $class_clprocessjobline = new clprocessjobline();

A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_D300_do_event_exports";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;
    $s_export_file = "";

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};
// gw 20120218 - moved to param
// gw 201202    $s_utime_search = time();
// gw 201202    $s_check_back_seconds = 600;
// gw 201202    //60 = 1min, 120=2min, 300=5min, 600=10min, 1200=20min, 3600=1hr, 7200=2hr, 14400=4hrs, 28800=8hrs, 86400=24hrs, 604800=7days
// gw 201202    $s_utime_since = $s_utime_search - $s_check_back_seconds;
// gw 201202    $s_utime_since = '1329440981';

    $s_utime_since = $ps_utime_since;
    $s_company_id_prev = "NONE";
    $s_company_id = "";
    $s_GlobalEventTypeId_prev = "NONE";
    $s_event_type = "";
    $s_event_group = "";
    $s_event_group_prev = "NONE";
    $s_device_id = "";

    $ar_line_details = array();
    $s_details_def_co = "";
    $s_details_data_co = "";

    $s_details_def_event = "";
    $s_details_data_event = "";

    $s_details_def = "";
    $s_details_data = "";

    $s_email_address = "";
    $s_subject = "";
    $s_body_map = "";
    $s_outfilenameonly = "";
    $s_outfilepath = "";

    $s_company_file = "";
    $s_special_file = "";

B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

// gw 20120427    $s_sql = " select distinct(process_id) from events where process_id like '{$ps_process_id}%' order by process_id";
    $s_sql = " select *  from events where process_id like '{$ps_process_id}%' order by process_id asc, deviceid asc,  logdate asc";
    //echo 's_sql='.$s_sql.'<br>';
    IF ($ps_developer_run=="YES"){
        echo '<tr><td colspan="10" bgcolor="#FF8080"> s_sql='.$s_sql.'</td></tr>';
    }
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<tr><td>&nbsp;</td><td colspan='10'>at ".date('H:m:s')."  NO event records found to export "."</td></tr>";
        goto B_990_EXIT;
    }
    IF ($ps_developer_run=="YES"){
        echo '<tr><td colspan="10" bgcolor="#FF8080">'.date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql.""."</td></tr>";
        echo '<tr><td colspan="10" bgcolor="#FF8080">'.date("Ymd")." at ".date('H:m:s')."ref:d300_b000&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";
        echo "<tr><td colspan='10'>";
    }

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
    IF ($ps_developer_run=="YES"){
        echo '<tr><td colspan="10" bgcolor="#00FF00">'.date("Ymd")." at ".date('H:m:s')."b150_do_records </td></tr>";
    }
    $s_process_id=$row["process_id"];
    $s_other_params = "none_20120423";
    $s_device_id =$row["DeviceId"];
    $s_GlobalDeliveryId=$row['GlobalDeliveryId'];
    $s_GlobalEventId=$row['GlobalEventId'];
    $s_event_notes=$row['Note'];
    $s_GlobalEventTypeId_rec=$row['GlobalEventTypeId'];

    $s_temp = $class_main->clmain_u820_fn_Z999_build_def_data($ps_dbcnx,$ps_debug, "d300 b600_body",$row,"Note");
    if (strpos($s_temp,"|^%##%^|") ===false)
    {
    }
    else
    {
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def_note = $ar_line_details[0];
        $s_details_data_note = $ar_line_details[1];
        $s_temp = "";
    }

B_152_GET_EVENTTYPE:
    $s_details_def_eventtype = "";
    $s_details_data_eventtype = "";
    $s_temp = $this->fn_E310_get_event_type_fields($dbcnx,$sys_debug, "CL_PROCESS E300_GET_EVENT_TYPE B_152_GET_EVENTTYPE", "NONE", $s_company_id, $s_GlobalEventTypeId_rec,$ps_developer_run,$ps_sendtype);
    if (strpos($s_temp,"|^%##%^|") ===false)
    {
    }
    else
    {
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def_eventtype = $ar_line_details[0];
        $s_details_data_eventtype = $ar_line_details[1];
        $s_temp = "";
    };

    $s_event_displayname = $class_main->clmain_v300_set_variable("REC_ev_displayname",$s_details_def_eventtype,$s_details_data_eventtype," NO","11","UJP_PROCESS_EVENT C100 B158");
    $s_event_displayname_normal = $s_event_displayname;
    $s_event_displayname_normal=str_replace(" ","_",strtolower($s_event_displayname_normal));
B_158_GET_NOTES:
    $s_eventnote = $class_main->clmain_v300_set_variable("REC_Note_DB_Note_Note_Value",$s_details_def_note,$s_details_data_note," NO","11","UJP_PROCESS_EVENT C100 B158");

    IF ($ps_developer_run=="YES"){
        echo '<tr><td colspan="10" bgcolor="#FF8080">'.date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_process_id=".$s_process_id.""." s_event_displayname = []".$s_event_displayname."[] s_event_displayname_normal =[]".$s_event_displayname_normal."[] s_GlobalEventTypeId_rec =[]".$s_GlobalEventTypeId_rec."[]</td></tr>";
    }
    $s_company_id = $s_process_id;
    $s_company_id = substr($s_company_id,strpos($s_company_id,"_")+1);
    $s_company_id = substr($s_company_id,0,strpos($s_company_id,"_"));

    $s_event_group  = $s_process_id;
    $s_event_group = substr($s_event_group,strpos($s_event_group,"_")+1);
    $s_event_group = substr($s_event_group,strpos($s_event_group,"_")+1);
    $s_event_group = substr($s_event_group,0,strpos($s_event_group,"_"));

    $s_GlobalEventTypeId  = $s_process_id;
    $s_GlobalEventTypeId = substr($s_GlobalEventTypeId,strpos($s_GlobalEventTypeId,"_")+1);
    $s_GlobalEventTypeId = substr($s_GlobalEventTypeId,strpos($s_GlobalEventTypeId,"_")+1);
    $s_GlobalEventTypeId = substr($s_GlobalEventTypeId,strpos($s_GlobalEventTypeId,"_")+1);

    $s_run_id = $s_process_id;
    $s_run_id = substr($s_run_id,0,strpos($s_run_id,"_"));

    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." s_company_id = ".$s_company_id." </td></tr>";
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." s_event_group = ".$s_event_group."</td></tr>";
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." s_GlobalEventTypeId = ".$s_GlobalEventTypeId."</td></tr>";
    }

B_159_DO_PER_EVENT:
//gw20131105 - added per event process
    $s_details_def='';
    $s_details_data='';

B_159A_GET_CO:
    $s_details_def_co = "";
    $s_details_data_co = "";
    $s_temp = $this->fn_Z400_GET_COMPANY($dbcnx,$sys_debug, "D300_B150", $s_company_id, $s_other_params,$ps_developer_run,$ps_sendtype);
    if (strpos($s_temp,"|^%##%^|") ===false)
    {
    }else{
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def_co = $ar_line_details[0];
        $s_details_data_co = $ar_line_details[1];
        $s_temp = "";
    };
B_159B_GET_DEVICE:
    $s_details_def_evd = "";
    $s_details_data_evd = "";
    $s_temp = $this->fn_Z500_GET_DEVICE($dbcnx,$sys_debug, "D300_B658", $s_company_id,$s_device_id, $s_other_params,$ps_developer_run,$ps_sendtype);
    if (strpos($s_temp,"|^%##%^|") ===false)
    {
    }else{
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def_evd = $s_details_def_evd.$ar_line_details[0];
        $s_details_data_evd = $s_details_data_evd.$ar_line_details[1];
        $s_temp = "";
    };
B_159C_GET_DELIVERY:
    $s_details_def_delv = "";
    $s_details_data_delv = "";
    $s_temp = $this->fn_Z700_GET_DELIVERY($dbcnx,$sys_debug, "D300_B658", $s_GlobalDeliveryId,$s_device_id, $s_other_params,$ps_developer_run,$ps_sendtype);
    if (strpos($s_temp,"|^%##%^|") ===false)
    {
    }else{
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def_delv = $s_details_def_delv.$ar_line_details[0];
        $s_details_data_delv = $s_details_data_delv.$ar_line_details[1];
        $s_temp = "";
    };

    $s_details_def = $s_details_def_note."|".$s_details_def_event."|".$s_details_def_eventtype."|".$s_details_def_co."|".$s_details_def_evd."|".$s_details_def_delv."|s_event_displayname_normal|";
    $s_details_data = $s_details_data_note."|".$s_details_data_event."|".$s_details_data_eventtype."|".$s_details_data_co."|".$s_details_data_evd."|".$s_details_data_delv."|".$s_event_displayname_normal."|";

    $s_per_event_result = $this->fn_DE100_do_per_event($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options,$ps_utime_since,$ps_delivery_type,$s_process_id,$ps_developer_run,$ps_sendtype,$s_details_def,$s_details_data,$i_record_count,$s_company_id);
    $s_details_def='';
    $s_details_data='';

B_159_DO_GROUPING_EVENT:
    IF ($s_GlobalEventTypeId_prev == $s_GlobalEventTypeId)
    {
        GOTO B600_DO_BODY;
    }

B160_CHECK_JCL_EXISTS:
    $s_jcl_outfilenameonly='ud_export_'.Date("YmdHis").$s_GlobalEventId.'.txt';
    $s_jcl_outfilepath=$_SESSION['ko_usrdata_path'].$s_company_id.'/from_ud/';
    //echo 's_jcl_outfilepath='.$s_jcl_outfilepath.'<br>';
    $s_jcl_full_outfilename=$s_jcl_outfilepath.$s_jcl_outfilenameonly;

    $s_actiondetails='';
    $s_details_def='';
    $s_details_data='';
    $xml_arr='';
    $s_sessionno='11';
    $s_jcl_name="ujp_process_events/ud_process_event_".$s_company_id."_".$s_GlobalEventTypeId.".jcl";
    //echo 's_jcl_name='.$s_jcl_name.'<br>';
    $s_check_count = "1";
B162_CHECK:
    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_jcl_name);

    if(file_exists($s_filename))
    {
        GOTO B170_RUN_JCL;
    }

    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  D300 FILE DOES NOT EXIST s_filename=[]'.$s_filename.'[]</td></tr>';}
    $s_check_count =     $s_check_count + 1;
    if ($s_check_count == "2")
    {
        $s_jcl_name="ujp_process_events/ud_process_event_allco_".$s_GlobalEventTypeId.".jcl";
        GOTO B162_CHECK;
    }
    if ($s_check_count == "3")
    {
        $s_jcl_name="ujp_process_events/ud_process_event_allco_".$s_event_displayname_normal.".jcl";
        GOTO B162_CHECK;
    }
    if ($s_check_count == "4")
    {
        $s_jcl_name="ujp_process_events/ud_process_event_allco_allevents.jcl";
        GOTO B162_CHECK;
    }

    /* gw 20131104 -  did the check loop instead
//
    $s_jcl_name="ujp_process_events/ud_process_event_default_".$s_GlobalEventTypeId.".jcl";
    //echo 's_jcl_name2='.$s_jcl_name.'<br>';
    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_jcl_name);
    if(file_exists($s_filename))
    {
        GOTO B170_RUN_JCL;
    }

    $s_jcl_name="ujp_process_events/ud_process_event_default.jcl";
    //echo 's_jcl_name2='.$s_jcl_name.'<br>';
    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_jcl_name);
    if(file_exists($s_filename))
    {
        GOTO B170_RUN_JCL;
    }
*/
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  D300 Unable to locate a jcl file skip jcl process </td></tr>';}

    goto B300_DO_GROUPING_OF_EVENTS;

B170_RUN_JCL:

    $_COOKIE['ud_logonname']='ud_export';
    $_COOKIE['ud_companyid']=$s_company_id;

    $file_date=Date("YmdHis");

    $pod_imagepath=$_SESSION['ko_udp_image_home_url']."usrdata/".$s_company_id."/";

    $s_details_def='START|POSTV_globaleventid|POSTV_globaldeliveryid|POSTV_jcl_outfilenameonly|POSTV_jcl_outfilepath|POSTV_full_outfilename|POSTV_process_id|POSTV_globaleventtypeid|POSTV_pod_imagepath|POSTV_eventnote|END';
    $s_details_data='START|'.$s_GlobalEventId.'|'.$s_GlobalDeliveryId.'|'.$s_jcl_outfilenameonly.'|'.$s_jcl_outfilepath.'|'.$s_jcl_full_outfilename.'|'.$s_process_id.'|'.$s_GlobalEventTypeId.'|'.$pod_imagepath.'|'.$s_eventnote.'|END';

    $s_actiondetails="run_jcl|".$s_jcl_name."|end";
    $s_returned = $class_clprocessjobline->pf_clprocessjobline($ps_dbcnx,$s_actiondetails,$s_details_def,$s_details_data,$xml_arr,$s_sessionno,'NO');

    //echo '<br>Devrun---g100-b200- s_returned START OF ='.'<br>';
    //print_r($s_returned);
    //echo '<br>Devrun---g100-b200- END OF s_returned='.'<br>';

B180_RUN_JCL_NEXT_REC:
    $i_record_count = $i_record_count + 1;
    GOTO B_100_GET_RECORDS;


B300_DO_GROUPING_OF_EVENTS:
B300_CHECK_COMPANY_ID:
//gw20131104    $s_outfilepath='C:/tdx/html/udelivered/actlog/usrdata/'.$s_company_id.'/from_ud/';

    $s_outfilepath = $_SESSION['ko_usrdata_path'].$s_company_id.'/from_ud/';
    IF ($ps_developer_run=="YES"){
        echo '<tr><td colspan="10" bgcolor="#FF8080">'.date("Ymd")." at ".date('H:m:s')." Create folder = s_outfilepath[]".$s_outfilepath."[]</td></tr>";
    }
    IF (!$class_main->clmain_u100_mkdirs($s_outfilepath,0777)){
        IF ($ps_developer_run=="YES"){
            echo '<tr><td colspan="10" bgcolor="#FF8080">'.date("Ymd")." at ".date('H:m:s')." Create folder failed = s_outfilepath[]".$s_outfilepath."[]</td></tr>";
        }
//        die ("<br>Failed to create the folder ".$s_outfilepath);
    }

    $s_company_file =  $s_run_id."_".$s_event_displayname_normal."_group.csv";
//gw20131120    $s_company_file =  $s_company_id."_".$s_run_id."_".$s_event_displayname_normal."_group.csv";
//gw20131104 1 file per group    $s_company_file =  $s_company_id."_".$s_event_displayname_normal."_".$s_run_id.".csv";
//gw20131104    $s_company_file =  $s_company_id."_actLogger_".$s_run_id.".csv";
    $s_special_file = "";
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  b300_check company id []'.$s_company_file.'[] </td></tr>';}
//gw20131105 - even if same company need to do the same event type footer and group ( group = displayname)
//     if ($s_company_id == $s_company_id_prev){
//         GOTO B390_EXIT;
//     }
// gw 20120424 first time through
     IF ($s_company_id_prev == "NONE"){
         GOTO B320_SKIP_FOOTER;
        };

    $s_details_def = $s_details_def."|".$s_details_def_event."|".$s_details_def_eventtype."|";
    $s_details_data = $s_details_data."|".$s_details_data_event."|".$s_details_data_eventtype."|";
    $s_temp = $this->fn_E400_DO_EVENTTYPE_PROCESS($dbcnx,$sys_debug, "B300_CHECK_COMPANY_ID", "FOOTER", $s_process_id, $s_GlobalEventTypeId_prev,$s_company_id, $s_other_params,$s_details_def,$s_details_data,$ps_developer_run,$ps_sendtype,$s_export_file,$s_event_group_prev);
    $s_body_map = $s_body_map.$s_temp;
    $s_temp = "";

    $s_details_def = "";
    $s_details_data = "";
B316_DO_GROUP_FOOTER:
    $s_details_def = $s_details_def."|".$s_details_def_event."|";
    $s_details_data = $s_details_data."|".$s_details_data_event."|";
    $s_temp = $this->fn_E600_DO_EVENTTYPE_GROUP($dbcnx,$sys_debug, "B400_CHECK_TYPE", "FOOTER", $s_process_id, $s_event_group_prev,$s_company_id, $s_other_params,$s_details_def,$s_details_data,$ps_developer_run,$ps_sendtype,$s_export_file);
    $s_body_map = $s_body_map.$s_temp;
    $s_temp = "";
    $s_details_def = "";
    $s_details_data = "";

    // if the event has a particular transfer methed to the event transfer
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')." fn_E600_TRANSFER_FILE for event </td></tr>";
//$s_temp = $this->fn_600_TRANSFER_FILE($dbcnx,$sys_debug, "D300_B150", $s_transfer_method, $s_process_id, $s_event_type,$s_company_id, $s_other_params);

B317_DO_CO_FOOTER:
    $s_details_def = $s_details_def."|".$s_details_def_co."|";
    $s_details_data = $s_details_data."|".$s_details_data_co."|";
    $s_temp = $this->fn_E500_DO_COMPANY_PROCESS($dbcnx,$sys_debug, "B300_CHECK_COMPANY_ID", "FOOTER", $s_process_id, $s_GlobalEventTypeId_prev,$s_company_id, $s_other_params,$s_details_def,$s_details_data,$ps_developer_run,$ps_sendtype,$s_export_file);

    $s_body_map = $s_body_map.$s_temp;
    $s_temp = "";
    $s_details_def = "";
    $s_details_data = "";
B318_DO_CO_MESSAGE:
    $s_details_def = $s_details_def."|".$s_details_def_co."|";
    $s_details_data = $s_details_data."|".$s_details_data_co."|";
    $s_temp = $this->fn_E800_DO_CO_MESSAGE($dbcnx,$sys_debug, "B300_CHECK_COMPANY_ID", "FOOTER", $s_process_id, $s_company_id, $s_other_params,$s_details_def,$s_details_data,$ps_developer_run,$ps_sendtype,$s_export_file);
    $s_body_map = $s_body_map.$s_temp;
    $s_temp = "";
    $s_details_def = "";
    $s_details_data = "";
B319_DO_UD:
    $s_details_def = $s_details_def."|".$s_details_def_co."|";
    $s_details_data = $s_details_data."|".$s_details_data_co."|";
    $s_temp = $this->fn_E700_DO_UDELIVERED($dbcnx,$sys_debug, "B300_CHECK_COMPANY_ID", "FOOTER", $s_process_id, $s_company_id, $s_other_params,$s_details_def,$s_details_data,$ps_developer_run,$ps_sendtype,$s_export_file);
    $s_body_map = $s_body_map.$s_temp;
    $s_temp = "";
    $s_details_def = "";
    $s_details_data = "";

    // transfer the file email/ftp/webservice
// how to handle multiple methods for an event
// how to handle 1 method for the company data and another for a particular eventtype
// gw 201204    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')." fn_E600_TRANSFER_FILE fro company</td></tr>";
//    $s_temp = $this->fn_E600_TRANSFER_FILE($dbcnx,$sys_debug, "D300_B150", $s_transfer_method, $s_process_id, $s_event_type,$s_company_id, $s_other_params);
// gw 201204    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')." fn_Z300_SEND_EMAIL </td></tr>";
//   $s_temp = $this->fn_Z300_SEND_EMAIL($dbcnx,$sys_debug, "D300_B150", "FOOTER", $s_process_id, $s_event_type,$s_company_id, $s_other_params,$ps_details_def,$ps_details_data,$ps_email_address,$ps_subject,$ps_body_map);

B320_SKIP_FOOTER:

     $s_outfilenameonly =   $s_company_file;
     $s_export_file = $s_outfilepath.$s_company_file;
    IF ($ps_developer_run=="YES"){
        echo '<tr><td colspan="10" bgcolor="#FF8080">'.date("Ymd")." at ".date('H:m:s')." export file name s_export_file[]".$s_export_file."[]</td></tr>";
    }

    $s_temp = $this->fn_Z400_GET_COMPANY($dbcnx,$sys_debug, "D300_B150", $s_company_id, $s_other_params,$ps_developer_run,$ps_sendtype);
    if (strpos($s_temp,"|^%##%^|") ===false)
    {
    }else{
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def_co = $ar_line_details[0];
        $s_details_data_co = $ar_line_details[1];
        $s_temp = "";
    };
B322_DO_UD:
    $s_details_def = $s_details_def."|".$s_details_def_co."|";
    $s_details_data = $s_details_data."|".$s_details_data_co."|";
    $s_temp = $this->fn_E700_DO_UDELIVERED($dbcnx,$sys_debug, "B320_SKIP_FOOTER", "HEADER", $s_process_id, $s_company_id, $s_other_params,$s_details_def,$s_details_data,$ps_developer_run,$ps_sendtype,$s_export_file);
    $s_body_map = $s_body_map.$s_temp;
    $s_temp = "";
    $s_details_def = "";
    $s_details_data = "";
B323_DO_CO_MESSAGE:
    $s_details_def = $s_details_def."|".$s_details_def_co."|";
    $s_details_data = $s_details_data."|".$s_details_data_co."|";
    $s_temp = $this->fn_E800_DO_CO_MESSAGE($dbcnx,$sys_debug, "B320_SKIP_FOOTER", "HEADER", $s_process_id, $s_company_id, $s_other_params,$s_details_def,$s_details_data,$ps_developer_run,$ps_sendtype,$s_export_file);
    $s_body_map = $s_body_map.$s_temp;
    $s_temp = "";
    $s_details_def = "";
    $s_details_data = "";

B324_CO_HEADER:
//    $s_temp = $this->fn_E500_DO_COMPANY_PROCESS($dbcnx,$sys_debug, "D300_B150", "HEADER", $s_process_id, $s_event_type,$s_company_id, $s_other_params);
    $s_details_def = $s_details_def."|".$s_details_def_co."|";
    $s_details_data = $s_details_data."|".$s_details_data_co."|";
    $s_temp = $this->fn_E500_DO_COMPANY_PROCESS($dbcnx,$sys_debug, "B320_SKIP_FOOTER", "HEADER", $s_process_id, $s_GlobalEventTypeId,$s_company_id, $s_other_params,$s_details_def,$s_details_data,$ps_developer_run,$ps_sendtype,$s_export_file);
    $s_body_map = $s_body_map.$s_temp;
    $s_temp = "";
    $s_details_def = "";
    $s_details_data = "";
B380_END:
    $s_event_group_prev = "NONE";
    $s_GlobalEventTypeId_prev = "NONE";
B390_EXIT:

B400_CHECK_GROUP:
    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." B400_CHECK_GROUP s_group_type =[]".$s_event_group."[] s_event_group_prev=[]".$s_event_group_prev."[]</td></tr>";
    }
     if ($s_event_group == $s_event_group_prev){
        goto B490_EXIT;
    }
    IF ($s_event_group_prev == "NONE")
    {
        GOTO B420_SKIP_FOOTER;
    }
    $s_details_def = $s_details_def."|".$s_details_def_event."|";
    $s_details_data = $s_details_data."|".$s_details_data_event."|";
    $s_temp = $this->fn_E400_DO_EVENTTYPE_PROCESS($dbcnx,$sys_debug, "B400_CHECK_TYPE", "FOOTER", $s_process_id, $s_GlobalEventTypeId_prev,$s_company_id, $s_other_params,$s_details_def,$s_details_data,$ps_developer_run,$ps_sendtype,$s_export_file,$s_event_group_prev);
    $s_body_map = $s_body_map.$s_temp;
    $s_temp = "";

    $s_details_def = "";
    $s_details_data = "";

    $s_details_def = $s_details_def."|".$s_details_def_event."|";
    $s_details_data = $s_details_data."|".$s_details_data_event."|";
    $s_temp = $this->fn_E600_DO_EVENTTYPE_GROUP($dbcnx,$sys_debug, "B400_CHECK_TYPE", "FOOTER", $s_process_id, $s_event_group_prev,$s_company_id, $s_other_params,$s_details_def,$s_details_data,$ps_developer_run,$ps_sendtype,$s_export_file);
    $s_body_map = $s_body_map.$s_temp;
    $s_temp = "";

    $s_details_def = "";
    $s_details_data = "";


B420_SKIP_FOOTER:
B450_DO_EVENTTYPE_HDR:
    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." fn_E310_get_event_type_fields </td></tr>";
    }
    $s_temp = $this->fn_E310_get_event_type_fields($dbcnx,$sys_debug, "CL_PROCESS E300_GET_EVENT_TYPE", "NONE", $s_company_id, $s_GlobalEventTypeId,$ps_developer_run,$ps_sendtype);
    if (strpos($s_temp,"|^%##%^|") ===false)
    {
    }
    else
    {
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def_event = "|".$ar_line_details[0];
        $s_details_data_event = "|".$ar_line_details[1];
        $s_temp = "";
    };
    $s_details_def = $s_details_def."|".$s_details_def_event."|";
    $s_details_data = $s_details_data."|".$s_details_data_event."|";
    $s_temp = $this->fn_E600_DO_EVENTTYPE_GROUP($dbcnx,$sys_debug, "B400_CHECK_TYPE", "HEADER", $s_process_id, $s_event_group,$s_company_id, $s_other_params,$s_details_def,$s_details_data,$ps_developer_run,$ps_sendtype,$s_export_file);
    $s_body_map = $s_body_map.$s_temp;
    $s_temp = "";

    $s_details_def = "";
    $s_details_data = "";

     $s_GlobalEventTypeId_prev = "NONE";
B459_END:
B490_EXIT:


B500_CHECK_TYPE:
    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." B500_CHECK_TYPE s_event_type =[]".$s_GlobalEventTypeId."[] s_event_type_prev=[]".$s_GlobalEventTypeId_prev."[]</td></tr>";
    }
     if ($s_GlobalEventTypeId == $s_GlobalEventTypeId_prev){
        goto B590_EXIT;
    }
    IF ($s_GlobalEventTypeId_prev == "NONE")
    {
        GOTO B520_SKIP_FOOTER;
    }
    $s_details_def = $s_details_def."|".$s_details_def_event."|";
    $s_details_data = $s_details_data."|".$s_details_data_event."|";
    $s_temp = $this->fn_E400_DO_EVENTTYPE_PROCESS($dbcnx,$sys_debug, "B500_CHECK_TYPE", "FOOTER", $s_process_id, $s_GlobalEventTypeId_prev,$s_company_id, $s_other_params,$s_details_def,$s_details_data,$ps_developer_run,$ps_sendtype,$s_export_file,$s_event_group_prev);
    $s_body_map = $s_body_map.$s_temp;
    $s_temp = "";

    $s_details_def = "";
    $s_details_data = "";

B520_SKIP_FOOTER:
    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." fn_E310_get_event_type_fields </td></tr>";
    }
    $s_temp = $this->fn_E310_get_event_type_fields($dbcnx,$sys_debug, "CL_PROCESS E300_GET_EVENT_TYPE", "NONE", $s_company_id, $s_GlobalEventTypeId,$ps_developer_run,$ps_sendtype);
    if (strpos($s_temp,"|^%##%^|") ===false)
    {
    }
    else
    {
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def_event = "|".$ar_line_details[0];
        $s_details_data_event = "|".$ar_line_details[1];
        $s_temp = "";
    };
    $s_details_def = $s_details_def."|".$s_details_def_event."|";
    $s_details_data = $s_details_data."|".$s_details_data_event."|";

    $s_temp = $this->fn_E400_DO_EVENTTYPE_PROCESS($dbcnx,$sys_debug, "B520_SKIP_FOOTER", "HEADER", $s_process_id, $s_GlobalEventTypeId,$s_company_id, $s_other_params,$s_details_def,$s_details_data,$ps_developer_run,$ps_sendtype,$s_export_file,$s_event_group);
    $s_body_map = $s_body_map.$s_temp;
    $s_temp = "";

    $s_details_def = "";
    $s_details_data = "";

B590_EXIT:


B600_DO_BODY:
    echo "<tr><td>&nbsp;</td><td colspan='10'> at ".date('H:m:s')." Doing Company Id = ".$s_company_id." Group=".$s_event_group." Event ID=".$s_GlobalEventTypeId." For device id of ".$s_device_id."</td></tr>";

    $s_details_def_evd = "";
    $s_details_data_evd = "";

    $s_temp = $class_main->clmain_u820_fn_Z999_build_def_data($ps_dbcnx,$ps_debug, "d300 b600_body",$row,"evd");
    if (strpos($s_temp,"|^%##%^|") ===false)
    {
    }else{
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def_evd = $ar_line_details[0];
        $s_details_data_evd = $ar_line_details[1];
        $s_temp = "";
/* gw20120606 - worked but was messy
        $s_evd_csv_headings = str_replace("|",",",$ar_line_details[0]);
        $s_evd_csv_headings = str_replace("REC_evd_DB_",",",$s_evd_csv_headings);
        $s_evd_csv_headings = str_replace("REC_evd_",",",$s_evd_csv_headings);
        $s_evd_csv_data = str_replace("|",",",$ar_line_details[1]);

        $s_details_def_evd = $s_details_def_evd."|REC_evd_csv_headings|REC_evd_csv_data|";
        $s_details_data_evd = $s_details_data_evd."|".$s_evd_csv_headings."|".$s_evd_csv_data."|";
        IF ($ps_developer_run=="YES"){
            echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." fn_E310_get_event_type_fields  b600_do body s_details_def_evd=[]".$s_details_def_evd."[]</td></tr>";
            echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." fn_E310_get_event_type_fields  b600_do body s_details_data_evd=[]".$s_details_data_evd."[]</td></tr>";
        }
*/
    };

B658_NEXT:
    $s_temp = $this->fn_Z500_GET_DEVICE($dbcnx,$sys_debug, "D300_B658", $s_company_id,$s_device_id, $s_other_params,$ps_developer_run,$ps_sendtype);
    if (strpos($s_temp,"|^%##%^|") ===false)
    {
    }else{
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def_evd = $s_details_def_evd.$ar_line_details[0];
        $s_details_data_evd = $s_details_data_evd.$ar_line_details[1];
        $s_temp = "";
    };
B690_DRAW_BODY:
    $s_details_def = $s_details_def."|".$s_details_def_event."|".$s_details_def_evd."|";
    $s_details_data = $s_details_data."|".$s_details_data_event."|".$s_details_data_evd."|";

//GW20131213 - THIS MIGHT WORK
    $s_details_def = $s_details_def_note."|".$s_details_def_event."|".$s_details_def_eventtype."|".$s_details_def_co."|".$s_details_def_evd."|".$s_details_def_delv."|s_event_displayname_normal|";
    $s_details_data = $s_details_data_note."|".$s_details_data_event."|".$s_details_data_eventtype."|".$s_details_data_co."|".$s_details_data_evd."|".$s_details_data_delv."|".$s_event_displayname_normal."|";


    $s_temp = $this->fn_E400_DO_EVENTTYPE_PROCESS($dbcnx,$sys_debug, "B500_DO_BODY", "BODY", $s_process_id, $s_GlobalEventTypeId,$s_company_id, $s_other_params,$s_details_def,$s_details_data,$ps_developer_run,$ps_sendtype,$s_export_file,$s_event_group);
    $s_body_map = $s_body_map.$s_temp;
    $s_temp = "";

    $s_details_def = "";
    $s_details_data = "";
    $s_company_id_prev = $s_company_id;
    $s_GlobalEventTypeId_prev = $s_GlobalEventTypeId;
    $s_event_group_prev = $s_event_group;

// gw 20120423
/*
    $_COOKIE['ud_logonname']='ud_process_events';
    $_COOKIE['ud_companyid']='3';
    $xml_arr='';
    $s_sessionno='11';
    $s_details_def='START|PROG_process_id';
    $s_details_data='START|'.$s_full_process_id;


    $s_actiondetails="sys_classV2|v1|jcl_result|clmain_u720_do_process(events,process_id_id,#p%!_PROG_process_id_!%#p,udelivered,,NO,udj_process_events)|NO|END";
    $s_returned = $class_clprocessjobline->pf_clprocessjobline($ps_dbcnx,$s_actiondetails,$s_details_def,$s_details_data,$xml_arr,$s_sessionno,'NO');
*/

B_800_NEXT_REC:
    $i_record_count = $i_record_count + 1;
    GOTO B_100_GET_RECORDS;
B_900_END_RECORDS:
    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." end records </td></tr>";
    }

B_905_EVENT_FOOTER:
    $s_details_def = $s_details_def."|".$s_details_def_event."|";
    $s_details_data = $s_details_data."|".$s_details_data_event."|";
    $s_temp = $this->fn_E400_DO_EVENTTYPE_PROCESS($dbcnx,$sys_debug, "B_900_END_RECORDS", "FOOTER", $s_process_id, $s_GlobalEventTypeId_prev,$s_company_id, $s_other_params,$s_details_def,$s_details_data,$ps_developer_run,$ps_sendtype,$s_export_file,$s_event_group_prev);
    $s_body_map = $s_body_map.$s_temp;
    $s_temp = "";

    $s_details_def = "";
    $s_details_data = "";
B_910_GROUP_FOOTER:
    $s_details_def = $s_details_def."|".$s_details_def_event."|";
    $s_details_data = $s_details_data."|".$s_details_data_event."|";
    $s_temp = $this->fn_E600_DO_EVENTTYPE_GROUP($dbcnx,$sys_debug, "B400_CHECK_TYPE", "FOOTER", $s_process_id, $s_event_group_prev,$s_company_id, $s_other_params,$s_details_def,$s_details_data,$ps_developer_run,$ps_sendtype,$s_export_file);
    $s_body_map = $s_body_map.$s_temp;
    $s_temp = "";

    $s_details_def = "";
    $s_details_data = "";

// if the event has a particular transfer methed to the event transfer
// gw 201204        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')." fn_600_TRANSFER_FILE for event </td></tr>";
//$s_temp = $this->fn_600_TRANSFER_FILE($dbcnx,$sys_debug, "D300_B150", $s_transfer_method, $s_process_id, $s_event_type,$s_company_id, $s_other_params);
B_915_CO_FOOTER:
    $s_details_def = $s_details_def."|".$s_details_def_co."|";
    $s_details_data = $s_details_data."|".$s_details_data_co."|";
    $s_temp = $this->fn_E500_DO_COMPANY_PROCESS($dbcnx,$sys_debug, "B_900_END_RECORDS", "FOOTER", $s_process_id, $s_GlobalEventTypeId_prev,$s_company_id, $s_other_params,$s_details_def,$s_details_data,$ps_developer_run,$ps_sendtype,$s_export_file);
    $s_body_map = $s_body_map.$s_temp;
    $s_temp = "";

    $s_details_def = "";
    $s_details_data = "";
B316_DO_CO_MESSAGE:
    $s_details_def = $s_details_def."|".$s_details_def_co."|";
    $s_details_data = $s_details_data."|".$s_details_data_co."|";
    $s_temp = $this->fn_E800_DO_CO_MESSAGE($dbcnx,$sys_debug, "B320_SKIP_FOOTER", "FOOTER", $s_process_id, $s_company_id, $s_other_params,$s_details_def,$s_details_data,$ps_developer_run,$ps_sendtype);
    $s_body_map = $s_body_map.$s_temp;
    $s_temp = "";
    $s_details_def = "";
    $s_details_data = "";
B_918_DO_TDX:
    $s_details_def = $s_details_def."|".$s_details_def_co."|";
    $s_details_data = $s_details_data."|".$s_details_data_co."|";
    $s_temp = $this->fn_E700_DO_UDELIVERED($dbcnx,$sys_debug, "B_900_END_RECORDS", "FOOTER", $s_process_id, $s_company_id, $s_other_params,$s_details_def,$s_details_data,$ps_developer_run,$ps_sendtype,$s_export_file);
    $s_body_map = $s_body_map.$s_temp;
    $s_temp = "";
    $s_details_def = "";
    $s_details_data = "";

// transfer the file email/ftp/webservice
// how to handle multiple methods for an event
// how to handle 1 method for the company data and another for a particular eventtype
// gw 201204    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')." fn_600_TRANSFER_FILE for company </td></tr>";
//    $s_temp = $this->fn_E600_TRANSFER_FILE($dbcnx,$sys_debug, "D300_B150", $s_transfer_method, $s_process_id, $s_event_type,$s_company_id, $s_other_params);
    $s_details_def = $s_details_def."|".$s_details_def_co."|";
    $s_details_data = $s_details_data."|".$s_details_data_co."|";

// gw 20120420 email comes off the user file for the company
// get the user for the company then the email address from than
// $s_email_address  = $class_main->clmain_v300_set_variable("REC_co_emailAddress",$s_details_def,$s_details_data,"NO", "12","UJP_PROCESS_EVENT B_918");
//gw20120529
//    $s_outfilenameonly = "testfile1.txt;testfile.txt";
//    $s_outfilepath = "c:/tdx/html/gwjunk/";

    $s_outfilenameonly = $this->fn_z600_get_filenames($s_outfilepath,$s_company_id,$s_run_id,$ps_developer_run,$ps_sendtype);
    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." calling email s_outfilenameonly=[]".$s_outfilenameonly."[]   s_outfilepath=[]".$s_outfilepath."[]</td></tr>";
    }

    if ($ps_delivery_type<>'SALESORDER')
    {
        goto B_990_EXIT;
    }

    $s_temp = $this->fn_Z300_SEND_EMAIL($dbcnx,$sys_debug, "B_900_END_RECORDS", $s_process_id, $s_GlobalEventTypeId,$s_company_id, $s_other_params,$ps_developer_run,$ps_sendtype,$s_details_def,$s_details_data,$s_email_address,$s_subject,$s_body_map,$s_outfilenameonly,$s_outfilepath,$ps_developer_run,$ps_sendtype);
    $s_details_def = "";
    $s_details_data = "";

    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." D300 918 sending email via http://".$_SESSION['ko_email_runon_server']."/email/utils/email_process.php</td></tr>";
    }

    $curl_connection = curl_init('http://'.$_SESSION['ko_email_runon_server'].'/email/utils/email_process.php');
    $result = curl_exec($curl_connection);

    goto B_990_EXIT;

B_990_EXIT:
    echo "</td></tr>";

Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    return $sys_function_out;
}
//#################################################################################################################################################################################################
function fn_D400_do_deliveries($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options,$ps_utime_since,$ps_delivery_type,$ps_process_id,$ps_developer_run,$ps_sendtype,$ps_comp_id)
{
     require_once($_SESSION['ko_prog_path'].'lib/class_process_jobline.php');
     $class_clprocessjobline = new clprocessjobline();

A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_D400_do_deliveries";
    $sys_function_out = "novalueset";

    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;

    $pod_image_attachments = "";
    $s_attach_filename = "";
    $s_attach_filenameonly = "";
    $pod_imageurl = "";
    $pod_imagepath = "";
    $s_dateadded = "";
    $s_deliveryDate = "";
    //    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Do Delivery for type:".$ps_delivery_type.""."</td></tr>";

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};
// gw 20120218 - moved to param
// gw 201202    $s_utime_search = time();
// gw 201202    $s_check_back_seconds = 600;
// gw 201202    //60 = 1min, 120=2min, 300=5min, 600=10min, 1200=20min, 3600=1hr, 7200=2hr, 14400=4hrs, 28800=8hrs, 86400=24hrs, 604800=7days
// gw 201202    $s_utime_since = $s_utime_search - $s_check_back_seconds;
// gw 201202    $s_utime_since = '1329440981';

    $s_utime_since = $ps_utime_since;

B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

// gw 20120810 - standard sql and add the company only process
    $s_sql = " select * from deliveries where (process_id = 'notdone' and entrycategory = '{$ps_delivery_type}' and ( DeliveryStatus = 'delivered' or DeliveryStatus = 'delivered_endofday'))  order by DeliveryDate" ;
    if ($ps_comp_id <> "ALL"){
        $s_sql = $s_sql." and companyid = '".$ps_comp_id."'";
    }

// gw20131220
// need to add the ablity to do a company specific sql
// if //config/delivery_select.sqlmap exits
// use the sql from in there
//


// gw 20120810    if ($ps_delivery_type=='SALESORDER')
// gw 20120810    {
// gw 20120810        $s_sql = " select * from deliveries where process_id = 'notdone' and entrycategory = '{$ps_delivery_type}' order by DeliveryDate" ;
// gw 20120810    }
// gw 20120810    if ($ps_delivery_type=='DELIVERY')
// gw 20120810    {
// gw 20120810        $s_sql = " select * from deliveries where process_id = 'notdone' and entrycategory = '{$ps_delivery_type}' and deliverydate is not null order by DeliveryDate";
// gw 20120810    }
    IF ($ps_developer_run=="YES"){
        echo '<tr><td colspan="10" bgcolor="#FF8080"> s_sql='.$s_sql.'</td></tr>';
    }
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result_set = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result_set);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<tr><td>&nbsp;</td><td colspan='10'>at ".date('H:m:s')." NO ".$ps_delivery_type." records found to process "."</td></tr>";
        goto B_900_END_RECORDS;
    }
    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql.""."</td></tr>";
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."ref:d400_b000&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";
    }
B_100_GET_RECORDS:
    IF ($ps_developer_run=="YES"){
        echo '<tr><td colspan="10" bgcolor="#FF8080"> D400 b100_get_records doing record '.$i_record_count.' of total = '.$s_numrows.' </td></tr>';
    }
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if (($i_record_count + 1)> $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result_set, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
    $s_globaldeliveryid = trim($row['GlobalDeliveryId']);
    $s_companyid = trim($row['CompanyId']);
    $s_dateadded  = trim($row['DateAdded']);
    $s_deliveryDate = trim($row['DeliveryDate']);
    //echo 's_globaldeliveryid='.$s_globaldeliveryid.'<br>';

    $s_full_process_id=$ps_process_id.'_'.$s_companyid;

B151_CREATE_DELIVERY_EXPORT:

B151_DO_JCL:

    if ($ps_delivery_type=='SALESORDER')
    {
        $s_jcl_outfilenameonly=$ps_delivery_type.'_'.Date("YmdHis").'_'.$s_globaldeliveryid.'.csv';
    }
    if ($ps_delivery_type=='DELIVERY')
    {
        $s_jcl_outfilenameonly=$ps_delivery_type.'_'.Date("YmdHis").$s_globaldeliveryid.'.xml';
    }
    $ar_jcl_outfilenameonly[$i_record_count]=$s_jcl_outfilenameonly;
    $s_jcl_outfilepath=$_SESSION['ko_usrdata_path'].$s_companyid.'/from_ud/';
    $ar_jcl_outfilepath[$i_record_count]=$s_jcl_outfilepath;
    $s_jcl_full_outfilename=$s_jcl_outfilepath.$s_jcl_outfilenameonly;

    //echo 's_jcl_full_outfilename='.$s_jcl_full_outfilename.'<br>';

    $s_actiondetails='';
    $s_details_def='';
    $s_details_data='';
    $xml_arr='';
    $s_sessionno='11';


    if ($ps_delivery_type=='DELIVERY')
    {
        $s_map_path = $_SESSION['ko_usrdata_path'].$s_companyid.'/config/';
        $s_map_name = "deliveryjob.jcl";
        $s_filename = $class_main->clmain_set_map_path_n_name("",$s_map_path.$s_map_name);
        if(file_exists($s_filename))
        {
            $s_jcl_name = $s_filename;
            $s_jcl_outfilenameonly=$ps_delivery_type.'_'.Date("YmdHis").$s_globaldeliveryid.'.csv';
            $s_jcl_full_outfilename=$s_jcl_outfilepath.$s_jcl_outfilenameonly;
            GOTO B152_RUN_JCL;
        }
        IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  D400  FILE DOES NOT EXIST $s_jcl_name=[]'.$s_jcl_name.'[]</td></tr>';}
    }
    if ($ps_delivery_type=='SALESORDER')
    {
        $s_jcl_name="ujp_process_events/uds_export_salesorder_coid_".$s_companyid.".jcl";
    }
    if ($ps_delivery_type=='DELIVERY')
    {
        $s_jcl_name="ujp_process_events/ud_process_event_coid_".$s_companyid."_deltype_".$ps_delivery_type.".jcl";
    }

    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_jcl_name);
// gw 20120810    echo 's_filename1='.$s_filename.'<br>';
// gw 20130410 - change all to work out of ujp_process_events folder
    if(file_exists($s_filename))
    {
        GOTO B152_RUN_JCL;
    }
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  D400  FILE DOES NOT EXIST s_filename=[]'.$s_filename.'[]</td></tr>';}

    if ($ps_delivery_type=='SALESORDER')
    {
        $s_jcl_name="ujp_process_events/uds_export_salesorder_default.jcl";
    }
    if ($ps_delivery_type=='DELIVERY')
    {
        $s_jcl_name="ujp_process_events/ud_process_event_default_delivery.jcl";
    }

    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_jcl_name);
    if(file_exists($s_filename))
    {
        GOTO B152_RUN_JCL;
    }
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  D400  JCL NOT FOUND  s_filename=[]'.$s_filename.'[]</td></tr>';}
    goto B_800_NEXT_REC;

B152_RUN_JCL:
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  D400  USING JCL  s_filename2=[]'.$s_filename.'[]</td></tr>';}

    $_COOKIE['ud_logonname']='delivery_export';
    $_COOKIE['ud_companyid']=$s_companyid;

    $file_date=Date("YmdHis");

    $pod_imageurl=$_SESSION['ko_udp_image_home_url']."/usrdata/".$s_companyid."/";

    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  D400  output file name  s_jcl_full_outfilename=[]'.$s_jcl_full_outfilename.'[]</td></tr>';}

    $s_details_def='START|POSTV_globaldeliveryid|POSTV_jcl_outfilenameonly|POSTV_jcl_outfilepath|POSTV_full_outfilename|POSTV_process_id|POSTV_pod_imageurl|POSTV_pod_imagepath|POSTV_USRDATA_PATH|END';
    $s_details_data='START|'.$s_globaldeliveryid.'|'.$s_jcl_outfilenameonly.'|'.$s_jcl_outfilepath.'|'.$s_jcl_full_outfilename.'|'.$s_full_process_id.'|'.$pod_imageurl.'|'.$pod_imagepath.'|'.$_SESSION['ko_usrdata_path'].'|END';

    $s_actiondetails="run_jcl|".$s_jcl_name."|end";
    $s_returned = $class_clprocessjobline->pf_clprocessjobline($ps_dbcnx,$s_actiondetails,$s_details_def,$s_details_data,$xml_arr,$s_sessionno,'NO');

    //echo '<br>Devrun---g100-b200- s_returned START OF ='.'<br>';
    //echo '<br>Devrun---g100-b200- END OF s_returned='.'<br>';
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  D400  B152_RUN_JCL RETURNED[]'.$s_returned.'[]</td></tr>';}

B200_ADD_IMAGES: // gw 20130409 - added
    //add the attachments
    // get the attachments list - if no path in the filename add the pod_imagepath

    $pod_imagepath=$_SESSION['ko_usrdata_path'].$s_companyid.'/';

    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  D400  B200_ADD_IMAGES START</td></tr>';}
B200_100_PROCESS:
    $s_sql = "SELECT * from attachments where Globaldeliveryid = '".$s_globaldeliveryid."'";
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  D400  B200_100_PROCESS sql=[]'.$s_sql.'[]</td></tr>';}
    $i_attach_record_count = "0";

B210_RECORD_LOOP:
    $result_attach = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_attach_rec_found = "NO";
    $s_attach_numrows = mysql_num_rows($result_attach);
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  D400  B210_RECORD_LOOP DOING  record count is s_attach_numrows=[]'.$s_attach_numrows.'[]</td></tr>';}

    if ($s_attach_numrows == 0)
    {
        IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  D400  B210_RECORD_LOOP NO RECORDS FOUND FOR sql=[]'.$s_sql.'[]</td></tr>';}
        goto B249_END_RECORDS;
    }

B220_GET_RECORDS:
    if ($i_attach_record_count > $s_attach_numrows) {
        IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  D400  B220_GET_RECORDS DOING attach_record_count=[]'.$i_attach_record_count.'[] of s_attach_numrows=[]'.$s_attach_numrows.'[]</td></tr>';}
        goto B249_END_RECORDS;
    }

    $row_attach = mysql_fetch_array($result_attach, MYSQL_ASSOC);
    IF ($row_attach === false )
    {
        IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  D400  B220_GET_RECORDS fetch failed</td></tr>';}
        goto B249_END_RECORDS;
    }
    $s_attach_rec_found = "YES";

B230_DO_RECORDS:
    $s_attach_filename = $row_attach["FileName"];
    if (strpos($s_attach_filename,"/")!==false)
    {
        $pod_image_attachments = $pod_image_attachments.";".$s_attach_filename;
        GOTO B248_NEXT;
    }
// does the file exist
    if(file_exists($pod_imagepath.$s_attach_filename))
    {
        $pod_image_attachments = $pod_image_attachments.";".$pod_imagepath.$s_attach_filename;
        $s_attach_filenameonly = $s_attach_filenameonly.";".$s_attach_filename;
        GOTO B248_NEXT;
    }
// if not build the path
    $pod_image_attachments = $pod_image_attachments.";".$pod_imagepath.date('Y/m/d/', $s_deliveryDate).$s_attach_filename;
    $s_attach_filenameonly = $s_attach_filenameonly.";".$s_attach_filename;

B248_NEXT:
    $i_attach_record_count = $i_attach_record_count + 1;
    GOTO B220_GET_RECORDS;
B249_END:

B249_END_RECORDS:



B_500_DO_CO_MESSAGE:
    $s_other_params = "";
    $s_process_id = "";
    $s_body_map = "";
    $s_GlobalEventTypeId = "";
    $s_outfilenameonly = $s_jcl_outfilenameonly;

    if ($ps_delivery_type<>'SALESORDER')
    {
        IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  D400  b_500 not doing the do co message</td></tr>';}
        goto B_509_DO_CO_MESSAGE_END;
    }

    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  D400  b_500 do co message</td></tr>';}

    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  D400  b_500 load company details</td></tr>';}
    $s_temp = $this->fn_Z400_GET_COMPANY($dbcnx,$sys_debug, "D300_B150", $s_companyid, $s_other_params,$ps_developer_run,$ps_sendtype);
    if (strpos($s_temp,"|^%##%^|") ===false)
    {
    }
    else
    {
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def_co = $ar_line_details[0];
        $s_details_data_co = $ar_line_details[1];
        $s_temp = "";
    };

    $s_details_def = $s_details_def."|REC_company_CompanyId|".$s_details_def_co;
    $s_details_data = $s_details_data."|".$s_companyid."|".$s_details_data_co;
//gw20130419      $s_export_file = $s_jcl_full_outfilename;
    $s_export_file = ""; // no details added to the extract file created by the jcl

    $s_temp = $this->fn_E500_DO_COMPANY_PROCESS($dbcnx,$sys_debug, "B320_SKIP_FOOTER", "HEADER", $s_process_id, $s_GlobalEventTypeId,$s_companyid, $s_other_params,$s_details_def,$s_details_data,$ps_developer_run,$ps_sendtype,$s_export_file);
    $s_body_map = $s_body_map.$s_temp;
    $s_temp = "";

    $s_temp = $this->fn_E500_DO_COMPANY_PROCESS($dbcnx,$sys_debug, "B320_SKIP_FOOTER", "MESSAGE_BODY", $s_process_id, $s_GlobalEventTypeId,$s_companyid, $s_other_params,$s_details_def,$s_details_data,$ps_developer_run,$ps_sendtype,$s_export_file);
    $s_body_map = $s_body_map.$s_temp;
    $s_temp = "";

    $s_temp = $this->fn_E500_DO_COMPANY_PROCESS($dbcnx,$sys_debug, "B320_SKIP_FOOTER", "FOOTER", $s_process_id, $s_GlobalEventTypeId_prev,$s_companyid, $s_other_params,$s_details_def,$s_details_data,$ps_developer_run,$ps_sendtype,$s_export_file);
    $s_body_map = $s_body_map.$s_temp;
    $s_temp = "";

    $s_details_def = "";
    $s_details_data = "";
    //$s_email_address = "petern@thedataexchange.com.au";
    //$s_email_address = "swolters@bolle.com.au";
    // gw 20120813  $s_email_address = "service@thedataexchange.com.au";

    //gw 20130408 - use the company email address
    $s_email_address=$this->fn_G300_get_users_details($dbcnx,$sys_debug, "fn_D400_do_deliveries B_500_DO_CO_MESSAGE", "NONE", $s_companyid,$ps_developer_run,$ps_sendtype);

    $s_subject = "uDelivered Sales Order Activity Notification";

    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  D400  b_500 email attachments $s_outfilenameonly=[]'.$s_outfilenameonly.'[] </td></tr>';}
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  D400  b_500 email attachments number of entries in $ar_jcl_outfilenameonly=[]'.count($ar_jcl_outfilenameonly).'[] </td></tr>';}

// gw 20120813    $cnt=0;
// gw 20120813    while($ar_jcl_outfilenameonly[$cnt]<>'')
// gw 20120813    {
// gw 20120813        $s_outfilenameonly=$s_outfilenameonly.$ar_jcl_outfilenameonly[$cnt].";";
// gw 20120813        $cnt++;
// gw 20120813    }
    $s_outfilepath=$s_jcl_full_outfilename;

    $s_outfilepath=$s_outfilepath.$pod_image_attachments.";";

B_505_SEND_EMAIL:
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  D400  b_500 send the email</td></tr>';}
    $s_temp = $this->fn_Z300_SEND_EMAIL($dbcnx,$sys_debug, "B_900_END_RECORDS", $s_process_id, $s_GlobalEventTypeId,$s_companyid, $s_other_params,$ps_developer_run,$ps_sendtype,$s_details_def,$s_details_data,$s_email_address,$s_subject,$s_body_map,$s_outfilenameonly,$s_outfilepath,$ps_developer_run,$ps_sendtype);
    $s_details_def = "";
    $s_details_data = "";

    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." D400 b_500 sending email via http://".$_SESSION['ko_email_runon_server']."/email/utils/email_process.php  to address ".$s_email_address."</td></tr>";
    }

    $curl_connection = curl_init('http://'.$_SESSION['ko_email_runon_server'].'/email/utils/email_process.php');
    $result = curl_exec($curl_connection);
    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." D400 result of trying to run email send []".$result."[]</td></tr>";
    }

B_509_DO_CO_MESSAGE_END:

B_800_NEXT_REC:
    $i_record_count = $i_record_count + 1;

// pkn 20120301 - update process_id to datetime so to signify the delivery has been processed

    $s_globaldeliveryid = trim($row['GlobalDeliveryId']);
    $s_update_sql="update deliveries set process_id = '{$s_full_process_id}' where GlobalDeliveryId = '{$s_globaldeliveryid}' ";
    $update_result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_update_sql);

    GOTO B_100_GET_RECORDS;


B_900_END_RECORDS:

Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    return $sys_function_out;
}

//#################################################################################################################################################################################################
function fn_DE100_do_per_event($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options,$ps_utime_since,$ps_delivery_type,$ps_process_id,$ps_developer_run,$ps_sendtype,$ps_details_def,$ps_details_data,$pi_record_count,$ps_company_id)
{

     require_once($_SESSION['ko_prog_path'].'lib/class_pdf_creator.php');
     $objPdfCreator = new cls_pdf_creator();


A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_DE100_do_per_event";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_event_displayname = $class_main->clmain_v300_set_variable("REC_ev_displayname",$ps_details_def,$ps_details_data," NO","11","UJP_PROCESS_EVENT C100 B158");
    $s_event_displayname_normal = $class_main->clmain_v300_set_variable("s_event_displayname_normal",$ps_details_def,$ps_details_data," NO","11","de100 ");

    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s').$sys_function_name." start - s_event_displayname_normal=[]".$s_event_displayname_normal."[]</td></tr>";
    }
//    $s_temp = $class_main->clmain_v300_set_variable("DETAILS_DEF_DUMP",$ps_details_def,$ps_details_data," NO","11","UJP_PROCESS_EVENT C100 B158");

//    IF ($ps_developer_run=="YES"){
//        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s').$s_temp."[]".$ps_details_def."[] </td></tr>";
//    }
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;

    $s_run_id = $ps_process_id;
    $s_run_id = substr($s_run_id,0,strpos($s_run_id,"_"));
    $i_record_count =$pi_record_count;
    $s_company_id = $ps_company_id;
    $s_email_filenames = "";
    $s_progress_description = "";
B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};
    $s_outfiletemppath = $_SESSION['ko_usrdata_path'].$s_company_id.'/temp/'; ;
    IF (!$class_main->clmain_u100_mkdirs($s_outfiletemppath,0777)){
        IF ($ps_developer_run=="YES"){
            echo '<tr><td colspan="10" bgcolor="#FF8080">'.date("Ymd")." at ".date('H:m:s').$s_sys_function_name." Create folder failed = s_outfilepath[]".$s_outfilepath."[]</td></tr>";
        }
    }
    $s_outfilepath = $_SESSION['ko_usrdata_path'].$s_company_id.'/from_ud/'; ;
    IF (!$class_main->clmain_u100_mkdirs($s_outfilepath,0777)){
        IF ($ps_developer_run=="YES"){
            echo '<tr><td colspan="10" bgcolor="#FF8080">'.date("Ymd")." at ".date('H:m:s').$s_sys_function_name." Create folder failed = s_outfilepath[]".$s_outfilepath."[]</td></tr>";
        }
    }

C100_DO_PDF:
    $s_output_file = $s_outfiletemppath.$s_run_id."_".$s_event_displayname_normal."_".$i_record_count.".topdfmap";
    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." s_output_file []".$s_output_file."[] </td></tr>";
    }
//make pdf
    $s_check_count = "1";
    $s_map_path = $_SESSION['ko_usrdata_path'].$s_company_id.'/config/';
    $s_map_name = $s_event_displayname_normal.".mappdf";
C110_CHECK:
    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_map_path.$s_map_name);
    if(file_exists($s_filename))
    {
        GOTO C150_DO_MAP;
    }

    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  '.$s_sys_function_name.' FILE DOES NOT EXIST s_filename=[]'.$s_filename.'[]</td></tr>';}
    $s_check_count =     $s_check_count + 1;
    if ($s_check_count == "2")
    {
        $s_map_path = "";
        $s_map_name="ujp_process_events/".$s_event_displayname_normal.".mappdf";
        GOTO C110_CHECK;
    }
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  '.$s_sys_function_name.' D300 Unable to locate a PDFMAP  </td></tr>';}
    $s_progress_description = $s_progress_description."No PDF created";
    goto C900_END;
C150_DO_MAP:
    $s_body_map = $class_main->clmain_v100_load_html_screen($s_filename,$ps_details_def,$ps_details_data,"no","WHOLE_FILE");

    $s_body_map = str_replace("#add_pipe#","|",$s_body_map);
    $s_body_map = str_replace("<BR>",PHP_EOL,$s_body_map);

    $fh = fopen($s_output_file, 'a');
    IF ($ps_developer_run=="YES"){fwrite($fh, $sys_function_name.' '.PHP_EOL);}
    fwrite($fh, $s_body_map.PHP_EOL);
    fclose($fh);

C300_MAKE_PDF:
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  '.$s_sys_function_name.' b300_make_pdf</td></tr>';}

    $s_output_pdf = $s_run_id."_".$s_event_displayname_normal."_".$i_record_count.".pdf";
    $s_email_filenames = $s_output_pdf;
    $s_email_filepath = $s_outfilepath;

    $s_output_pdf = $s_outfilepath.$s_output_pdf;
//        $pdf_file_path = $ps_get_site_path."pick_slips/".date("Ymd")."/";

//        $pdf_file = $pdf_file_path."pdf_".date("YmdHis").".pdf";
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  '.$s_sys_function_name.' b300_make_pdf s_output_pdf=[]'.$s_output_pdf.'[] s_output_file=[]'.$s_output_file." _SESSION['ko_ut_home_url']=[]".$_SESSION['ko_ut_home_url']."[]</td></tr>";}

        $objPdfCreator->cls_create_pdf_file($s_output_pdf,$s_output_file,$_SESSION['ko_ut_home_url']);

    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  '.$s_sys_function_name.' b300_make_pdf after pdf </td></tr>';}

C900_END:

D100_DO_CSV:
    $s_body_map = "";
    $s_output_filename = $s_run_id."_".$s_event_displayname_normal."_".$i_record_count.".csv";
    $s_output_file = $s_outfilepath.$s_output_filename;
    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." s_output_file []".$s_output_file."[] </td></tr>";
    }
//make pdf
    $s_check_count = "1";
    $s_map_path = $_SESSION['ko_usrdata_path'].$s_company_id.'/config/';
    $s_map_name = $s_event_displayname_normal.".mapcsv";
D110_CHECK:
    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_map_path.$s_map_name);
    if(file_exists($s_filename))
    {
        GOTO D150_DO_MAP;
    }

    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  '.$s_sys_function_name.' FILE DOES NOT EXIST s_filename=[]'.$s_filename.'[]</td></tr>';}
    $s_check_count =     $s_check_count + 1;
    if ($s_check_count == "2")
    {
        $s_map_path = "";
        $s_map_name="ujp_process_events/".$s_event_displayname_normal.".mapcsv";
        GOTO D110_CHECK;
    }
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  '.$s_sys_function_name.' D300 Unable to locate a PDFMAP  </td></tr>';}
    $s_progress_description = $s_progress_description."No PDF created";
    goto D900_END;
D150_DO_MAP:
    $s_body_map = $class_main->clmain_v100_load_html_screen($s_filename,$ps_details_def,$ps_details_data,"no","WHOLE_FILE");

    $s_body_map = str_replace("#add_pipe#","|",$s_body_map);
    $s_body_map = str_replace("<BR>",PHP_EOL,$s_body_map);

    $fh = fopen($s_output_file, 'a');
//    IF ($ps_developer_run=="YES"){fwrite($fh, $sys_function_name.' '.PHP_EOL);}
    fwrite($fh, $s_body_map.PHP_EOL);
    fclose($fh);

D300_MAKE_FILE:
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  '.$s_sys_function_name.' d300_make_file</td></tr>';}

    if (trim($s_email_filenames) <> "")
    {
        $s_email_filenames = $s_email_filenames.";";
    }
    $s_email_filenames = $s_email_filenames.$s_output_filename;
    $s_email_filepath = $s_outfilepath;

//    $s_output_pdf = $s_outfilepath.$s_output_pdf;

    iF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  '.$s_sys_function_name.' d300_make_file after csv</td></tr>';}

D900_END:

X800_TRANSPORT_FILE:

X810_EMAIL:
// send email
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  '.$s_sys_function_name.' X810_EMAIL</td></tr>';}
    if (trim($s_email_filenames) == "")
    {
        goto X819_END;
    }
    $s_email_address =  "service@thedataexchange.com.au";
    $s_subject = "Event notification for ".$s_event_displayname;
    $s_body_map = "This is an automated event notification";
    $s_other_params = "";

    $s_REC_Note_GlobalEventId = $class_main->clmain_v300_set_variable("REC_Note_GlobalEventId",$ps_details_def,$ps_details_data," NO","11","UJP_PROCESS_EVENT C100 B158");
    $s_REC_Note_GlobalEventTypeId = $class_main->clmain_v300_set_variable("REC_Note_GlobalEventTypeId",$ps_details_def,$ps_details_data," NO","11","UJP_PROCESS_EVENT C100 B158");
    $s_REC_Note_DeviceId = $class_main->clmain_v300_set_variable("REC_Note_DeviceId",$ps_details_def,$ps_details_data," NO","11","UJP_PROCESS_EVENT C100 B158");
    $s_REC_Note_GlobalDeliveryId = $class_main->clmain_v300_set_variable("REC_Note_GlobalDeliveryId",$ps_details_def,$ps_details_data," NO","11","UJP_PROCESS_EVENT C100 B158");
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  '.$s_sys_function_name.' X810_EMAIL before blob</td></tr>';}

    $s_data_blob = "";
//    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  '.$s_sys_function_name.' X810_EMAIL 1 before blob</td></tr>';}
    $s_data_blob = $s_data_blob.$class_main->clmain_u570_build_xml("UT_FILESTART","data_blob","NO","process_events X810_EMAIL");
//    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  '.$s_sys_function_name.' X810_EMAIL 2 before blob</td></tr>';}
    $s_data_blob = $s_data_blob.$class_main->clmain_u570_build_xml("ut_start","data_blob","NO","u560-2");
//    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  '.$s_sys_function_name.' X810_EMAIL 3 before blob</td></tr>';}

    $s_data_blob = $s_data_blob.$class_main->clmain_u570_build_xml("GlobalEventId",$s_REC_Note_GlobalEventId,"NO","process_events X810_EMAIL");
//    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  '.$s_sys_function_name.' X810_EMAIL 4 before blob</td></tr>';}
    $s_data_blob = $s_data_blob.$class_main->clmain_u570_build_xml("GlobalEventTypeId",$s_REC_Note_GlobalEventTypeId,"NO","process_events X810_EMAIL");
//    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  '.$s_sys_function_name.' X810_EMAIL 5 before blob</td></tr>';}
    $s_data_blob = $s_data_blob.$class_main->clmain_u570_build_xml("DeviceId",$s_REC_Note_DeviceId,"NO","process_events X810_EMAIL");
//    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  '.$s_sys_function_name.' X810_EMAIL 6 before blob</td></tr>';}
    $s_data_blob = $s_data_blob.$class_main->clmain_u570_build_xml("GlobalDeliveryId",$s_REC_Note_GlobalDeliveryId,"NO","process_events X810_EMAIL");
//    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  '.$s_sys_function_name.' X810_EMAIL 7 before blob</td></tr>';}
    $s_data_blob = $s_data_blob.$class_main->clmain_u570_build_xml("subject",$s_subject,"NO","process_events X810_EMAIL");
//    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  '.$s_sys_function_name.' X810_EMAIL 8 before blob</td></tr>';}

    $s_data_blob = $s_data_blob.$class_main->clmain_u570_build_xml("ut_end","data_blob","NO","process_events X810_EMAIL");

    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  '.$s_sys_function_name.' 7 X810_EMAIL activity log</td></tr>';}
//($ps_dbcnx,$ps_rectype,$ps_companyid,$ps_userid,$ps_summary , $ps_data_blob, $ps_key1, $ps_key1def, $ps_key2, $ps_key2def, $ps_key3, $ps_key3def, $ps_key4, $ps_key4def, $ps_key5, $ps_key5def)
    $tmp = $class_main->clmain_u540_utl_activity_log($dbcnx,"EMAILSEND", $s_company_id,"CLASS_UJP_PROCESS_EVENTS",$s_subject,$s_data_blob,$s_REC_Note_GlobalEventId,"globaleventid",$s_REC_Note_GlobalDeliveryId,"GlobalDeliveryId",$s_REC_Note_DeviceId,"DeviceId",$s_REC_Note_GlobalEventTypeId,"GlobalEventTypeId","","spare");

    $s_GlobalEventTypeId = $s_REC_Note_GlobalEventTypeId;

    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  '.$s_sys_function_name.' X810_EMAIL send email</td></tr>';}
    $s_temp = $this->fn_Z300_SEND_EMAIL($ps_dbcnx,$sys_debug, "de100 - x800 transport", $ps_process_id, $s_GlobalEventTypeId,$s_company_id, $s_other_params,$ps_developer_run,$ps_sendtype,$ps_details_def,$ps_details_data,$s_email_address,$s_subject,$s_body_map,$s_email_filenames,$s_email_filepath,$ps_developer_run,$ps_sendtype);
    $s_details_def = "";
    $s_details_data = "";

    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." D300 918 sending email via http://".$_SESSION['ko_email_runon_server']."/email/utils/email_process.php</td></tr>";
    }

    $curl_connection = curl_init('http://'.$_SESSION['ko_email_runon_server'].'/email/utils/email_process.php');
    $result = curl_exec($curl_connection);
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  '.$s_sys_function_name.' X819_EMAIL</td></tr>';}
X819_END:

Z900_EXIT:
    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s').$sys_function_name." END </td></tr>";
    }
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    return $sys_function_out;
}

//#################################################################################################################################################################################################
function fn_E100_get_cn_number($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid, $ps_GlobalDeliveryId,$ps_developer_run,$ps_sendtype)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_E100_get_cn_number";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;

    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Get CN Number ".$ps_companyid.""."</td></tr>";

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};

B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    $s_sql = " select * from deliveries where GlobalDeliveryId = '{$ps_GlobalDeliveryId}' and companyid = '{$ps_companyid}' " ;
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql.""."</td></tr>";
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."ref:e100_b000&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";
        goto B_900_END_RECORDS;
    }
    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql.""."</td></tr>";
    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."ref:e100_b000&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:

    $s_cn_number = trim($row['CNNO']);

B_158_NEXT:
    $i_record_count = $i_record_count + 1;

B_800_NEXT_REC:
    GOTO B_100_GET_RECORDS;
B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    $sys_function_out=$s_cn_number;
    return $sys_function_out;
}
//#################################################################################################################################################################################################
function fn_E100_get_cn_number_fields($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid, $ps_GlobalDeliveryId,$ps_developer_run,$ps_sendtype)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_E100_get_cn_number_fields";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;
    $s_details_def = "";
    $s_details_data = "";

    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Get CN Number ".$ps_companyid.""."</td></tr>";

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};

B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    $s_sql = " select * from deliveries where GlobalDeliveryId = '{$ps_GlobalDeliveryId}' and companyid = '{$ps_companyid}' " ;
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql.""."</td></tr>";
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."ref:e1100_b000&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";
        goto B_900_END_RECORDS;
    }
    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql.""."</td></tr>";
    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."ref:e1100_b000&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
    foreach( $row as $key=>$value)
    {
        if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
        {
            GOTO B_151_SKIP_DATA_BLOB;
        }
        if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
        {
            GOTO B_151_SKIP_DATA_BLOB;
        }
        if (strpos(strtoupper($value),"XML") === false)
        {
            GOTO B_151_SKIP_DATA_BLOB;
        }

        $ar_xml = simplexml_load_string($value);
        $newArry = array();
        $ar_xml = (array) $ar_xml;
        foreach ($ar_xml as $key => $value)
        {
            $s_details_def .= "|REC_uddev_DB_".$key;
            $s_details_data .= "|".$class_main->clmain_u596_xml_decode_char($value);
        }
        continue;

 B_151_SKIP_DATA_BLOB:
        if (strpos(strtoupper($key),"DETAILS_D") === FALSE)
        {
            $s_details_def .= "|REC_uddev_".$key;
            $s_details_data .= "|".$value;
        }
         continue;
    }
B_158_NEXT:
    $i_record_count = $i_record_count + 1;

B_800_NEXT_REC:
    GOTO B_100_GET_RECORDS;
B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    $sys_function_out = $s_details_def."|^%##%^|".$s_details_data;
    return $sys_function_out;
}
//#################################################################################################################################################################################################
function fn_E200_get_jobs_id($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid, $ps_job_number,$ps_developer_run,$ps_sendtype)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_E200_get_jobs_id";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;

    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Get Jobs ID ".$ps_companyid.""."</td></tr>";

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};

B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    $s_sql = " select * from jobs where companyid_job_no = '{$ps_job_number}' and companyid = '{$ps_companyid}' " ;
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql.""."</td></tr>";
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."ref:e200_b000&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";
        goto B_900_END_RECORDS;
    }
    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql.""."</td></tr>";
    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."ref:e200_b000&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:

    $s_jobs_id = trim($row['jobs_id']);

B_158_NEXT:
    $i_record_count = $i_record_count + 1;

B_800_NEXT_REC:
    GOTO B_100_GET_RECORDS;
B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    $sys_function_out=$s_jobs_id;
    return $sys_function_out;
}
//#################################################################################################################################################################################################
function fn_E200_get_jobs_id_fields($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid, $ps_job_number,$ps_developer_run,$ps_sendtype)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_E200_get_jobs_id_fields";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;
    $s_details_def = "";
    $s_details_data = "";

    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Get Jobs ID ".$ps_companyid.""."</td></tr>";

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};

B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    $s_sql = " select * from jobs where companyid_job_no = '{$ps_job_number}' and companyid = '{$ps_companyid}' ";
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql.""."</td></tr>";
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."ref:e2200_b000&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";
        goto B_900_END_RECORDS;
    }
    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql.""."</td></tr>";
    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."ref:e2200_b000&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
    foreach( $row as $key=>$value)
    {
        if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
        {
            GOTO B_151_SKIP_DATA_BLOB;
        }
        if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
        {
            GOTO B_151_SKIP_DATA_BLOB;
        }
        if (strpos(strtoupper($value),"XML") === false)
        {
            GOTO B_151_SKIP_DATA_BLOB;
        }

        $ar_xml = simplexml_load_string($value);
        $newArry = array();
        $ar_xml = (array) $ar_xml;
        foreach ($ar_xml as $key => $value)
        {
            $s_details_def .= "|REC_jm_DB_".$key;
            $s_details_data .= "|".$class_main->clmain_u596_xml_decode_char($value);
        }
        continue;

B_151_SKIP_DATA_BLOB:
        if (strpos(strtoupper($key),"DETAILS_D") === FALSE)
        {
            $s_details_def .= "|REC_jm_".$key;
            $s_details_data .= "|".$value;
        }
        continue;
    }
B_158_NEXT:
    $i_record_count = $i_record_count + 1;

B_800_NEXT_REC:
    GOTO B_100_GET_RECORDS;
B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    $sys_function_out = $s_details_def."|^%##%^|".$s_details_data;
    return $sys_function_out;
}
//#################################################################################################################################################################################################
function fn_E300_get_event_type($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid, $ps_GlobalEventTypeId,$ps_developer_run,$ps_sendtype)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_E300_get_event_type";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;

    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Get Event Type ".$ps_companyid.""."</td></tr>";

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};

B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    $s_sql = " select * from eventtypes where GlobalEventTypeId = '{$ps_GlobalEventTypeId}' " ;
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql.""."</td></tr>";
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";
        goto B_900_END_RECORDS;
    }
    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql.""."</td></tr>";
    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:

    $s_event_name = trim($row['DisplayName']);

B_158_NEXT:
    $i_record_count = $i_record_count + 1;

B_800_NEXT_REC:
    GOTO B_100_GET_RECORDS;
B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    $sys_function_out=$s_event_name;
    return $sys_function_out;
}
//#################################################################################################################################################################################################
function fn_E310_get_event_type_fields($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid, $ps_GlobalEventTypeId,$ps_developer_run,$ps_sendtype)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_E310_get_event_type_fields";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;
    $s_details_def = "";
    $s_details_data = "";

    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Get Event Type ".$ps_companyid.""."</td></tr>";
    }

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};

B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    $s_sql = " select * from eventtypes where GlobalEventTypeId = '{$ps_GlobalEventTypeId}' " ;
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql.""."</td></tr>";
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";
        goto B_900_END_RECORDS;
    }
    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql.""."</td></tr>";
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";
    }

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
    foreach( $row as $key=>$value)
    {
        if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
        {
            GOTO B_151_SKIP_DATA_BLOB;
        }
        if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
        {
            GOTO B_151_SKIP_DATA_BLOB;
        }
        if (strpos(strtoupper($value),"XML") === false)
        {
            GOTO B_151_SKIP_DATA_BLOB;
        }

        $ar_xml = simplexml_load_string($value);
        $newArry = array();
        $ar_xml = (array) $ar_xml;
        foreach ($ar_xml as $key => $value)
        {
            $value = trim($value);
            $s_details_def .= "|REC_ev_DB_".$key;
            $s_details_data .= "|".$class_main->clmain_u596_xml_decode_char($value);
        }
        continue;

 B_151_SKIP_DATA_BLOB:
        if (strpos(strtoupper($key),"DETAILS_D") === FALSE)
        {
            $value = trim($value);
            $s_details_def .= "|REC_ev_".$key;
            $s_details_data .= "|".$value;
        }
        continue;
    }
B_158_NEXT:
    $i_record_count = $i_record_count + 1;

B_800_NEXT_REC:
    GOTO B_100_GET_RECORDS;
B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    $sys_function_out = $s_details_def."|^%##%^|".$s_details_data;
    return $sys_function_out;
}
//#################################################################################################################################################################################################
function  fn_E400_DO_EVENTTYPE_PROCESS($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_action, $ps_process_id, $ps_event_type,$ps_company_id, $ps_other_params,$ps_details_def,$ps_details_data,$ps_developer_run,$ps_sendtype,$ps_export_file,$ps_event_group)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_E400_DO_EVENTTYPE_PROCESS";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:

    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;
    $s_details_def = "";
    $s_details_data = "";
    $s_body_map = "";
    $s_company_id = $ps_company_id;

B000_START:
    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;

    $s_ev_DisplayName  = $class_main->clmain_v300_set_variable("REC_ev_DisplayName",$s_details_def,$s_details_data,"NO","12","UJP_PROCESS_EVENT e400");
    $s_ev_DisplayName  = strtolower($s_ev_DisplayName);
    $s_ev_DisplayName  = str_replace(" ","_",$s_ev_DisplayName);
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0"> '.$sys_function_name.' Doing EVENTTYPE_'.$ps_action.'   COMPANY_ company id '.$ps_company_id.' eventtype = []'.$ps_event_type.'[]  action = []'.$ps_action.'[]</td></tr>';}

//        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')." fn_E600_DO_EVENTTYPE_GROUP ps_fromwhere= ".$ps_called_from." ps_action ".$ps_action."  ps_process_id=".$ps_process_id."</td></tr>";
//       $sys_function_out =  "fn_E600_DO_EVENTTYPE_GROUP DOING =".$ps_action;
C100_DRAW:
    $s_code_group = $ps_action;

    $s_filename = "ujp_process_events/ud_process_event_eventtype_".$s_ev_DisplayName.".htm";
    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_filename);
    if(file_exists($s_filename))
    {
        GOTO C900_DO_DRAW;
    }
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">   E400 FILE DOES NOT EXIST s_filename=[]'.$s_filename.'[]</td></tr>';}

    $s_filename = "ujp_process_events/ud_process_event_eventtype_".$ps_event_type.".htm";
    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_filename);
    if(file_exists($s_filename))
    {
        GOTO C900_DO_DRAW;
    }
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">   E400 D FILE DOES NOT EXIST s_filename=[]'.$s_filename.'[]</td></tr>';}
    //        $sys_function_out = "<TR><TD> FILE DOES NOT EXIST s_filename=[]".$s_filename."[]</TD></TR>";
    $s_filename = "ujp_process_events/ud_process_event_eventtype.htm";
    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_filename);

C900_DO_DRAW:
    $s_body_map = $class_main->clmain_v100_load_html_screen($s_filename,$ps_details_def,$ps_details_data,"no",$s_code_group);

    $sys_function_out = $sys_function_out.$s_body_map;

C100_EXPORT_FILE:
    if (trim($ps_export_file) == "")
    {
        IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0"> Doing EVENTTYPE_'.$ps_action.'  no export file required ps_export_file=[]'.$ps_export_file.'[]</td></tr>';}
        goto C900_EXIT;
    }
    $s_export_file = $ps_export_file;

    if (strtoupper(trim($ps_event_group)) == "NOGROUP")
    {
        GOTO C200_SKIP_FILENAME;
    }
    if (strtoupper(trim($ps_event_group)) == "NONE")
    {
        GOTO C200_SKIP_FILENAME;
    }
//gw20131105 - not such  thing as nogroup its the name normalised
//    $s_export_file = str_replace(".csv","",$s_export_file)."_".$ps_event_group.".csv";
C200_SKIP_FILENAME:
    $s_filename = $_SESSION['ko_usrdata_path'].$s_company_id.'/config/';
    $s_filename = $s_filename.$s_ev_DisplayName;
    $s_filename = $s_filename."_group.mapcsv";
    if(file_exists($s_filename))
    {
        GOTO C800_DRAW;
    }
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">   D400 C200 FILE DOES NOT EXIST s_filename=[]'.$s_filename.'[]</td></tr>';}

    $s_filename = "ujp_process_events/ud_process_event_eventtype_".$s_ev_DisplayName."_xml_.txt";
    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_filename);
    if(file_exists($s_filename))
    {
        GOTO C800_DRAW;
    }
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">   D400 C200 FILE DOES NOT EXIST s_filename=[]'.$s_filename.'[]</td></tr>';}
    $s_filename = "ujp_process_events/ud_process_event_eventtype_".$s_ev_DisplayName.".txt";
    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_filename);
    if(file_exists($s_filename))
    {
        GOTO C800_DRAW;
    }
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">   D400 C210 FILE DOES NOT EXIST s_filename=[]'.$s_filename.'[]</td></tr>';}

    $s_filename = "ujp_process_events/ud_process_event_eventtype_".$ps_event_type."_xml_.txt";
    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_filename);
    if(file_exists($s_filename))
    {
        GOTO C800_DRAW;
    }
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">   D400 C220 FILE DOES NOT EXIST s_filename=[]'.$s_filename.'[]</td></tr>';}

    $s_filename = "ujp_process_events/ud_process_event_eventtype_".$ps_event_type.".txt";
    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_filename);
    if(file_exists($s_filename))
    {
        GOTO C800_DRAW;
    }
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">   D400 C230 FILE DOES NOT EXIST s_filename=[]'.$s_filename.'[]</td></tr>';}

    //        $sys_function_out = "<TR><TD> FILE DOES NOT EXIST s_filename=[]".$s_filename."[]</TD></TR>";
    $s_filename = "ujp_process_events/ud_process_event_eventtype_xml_.txt";
    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_filename);

    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0"> Doing EVENTTYPE_'.$ps_action.'  export file  ps_export_file=[]'.$ps_export_file.'[]</td></tr>';}

    $s_filename = "ujp_process_events/ud_process_event_eventtype.txt";
    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_filename);

    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0"> Doing EVENTTYPE_'.$ps_action.'  export file  ps_export_file=[]'.$ps_export_file.'[]</td></tr>';}
    $s_code_group = $ps_action;
C800_DRAW:
    if (strpos(strtolower($s_filename),"_xml_") !==false)
    {
        $s_export_file = str_replace(".csv",".xml",$s_export_file);
    }

    $s_body_map = $class_main->clmain_v100_load_html_screen($s_filename,$ps_details_def,$ps_details_data,"no",$s_code_group);


    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0"> E400 C800 Doing EVENTTYPE_'.$ps_action.'  export file  s_export_file=[]'.$s_export_file.'[] s_filename=[]'.$s_filename.'[] s_body_map=[]'.$s_body_map.'[]</td></tr>';}

    if (trim($s_body_map) == "")
    {
        goto C900_EXIT;
    }
    $fh = fopen($s_export_file, 'a');
//    IF ($ps_developer_run=="YES"){fwrite($fh, $sys_function_name.' Doing EVENTTYPE_'.$ps_action.'   company id '.$ps_company_id.' action = []'.$ps_action.'[]'.PHP_EOL);}

    $s_body_map = str_replace("<BR>",PHP_EOL,$s_body_map);
    fwrite($fh, $s_body_map.PHP_EOL);
    fclose($fh);
C900_EXIT:

//    echo "<tr><td  bgcolor='#8080FF'>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')." event next cell is comment email body </td></tr>";
//    echo "<tr><td  bgcolor='#8080FF'>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."[]<!-- []".$sys_function_out."[]   -->[] </td></tr>";
//    echo "<tr><td  bgcolor='#8080FF'>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')." above is commented email body </td></tr>";

Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    return $sys_function_out;
}
//#################################################################################################################################################################################################
function fn_E500_DO_COMPANY_PROCESS($ps_dbcnx,$ps_debug, $ps_called_from, $ps_action, $ps_process_id, $ps_event_type,$ps_company_id, $ps_other_params,$ps_details_def,$ps_details_data,$ps_developer_run,$ps_sendtype,$ps_export_file)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_called_from;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_E500_DO_COMPANY_PROCESS";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;
    $s_details_def = "";
    $s_details_data = "";
    $s_body_map = "";
    $s_company_id = $ps_company_id;
B000_START:
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0"> e500_  Doing COMPANY_'.$ps_action.'   company id '.$ps_company_id.' action = []'.$ps_action.'[]</td></tr>';}

    $s_filename = $_SESSION['ko_usrdata_path'].$s_company_id.'/config/';

    $s_filename = $s_filename.".mapcsv";

    $s_code_group = $ps_action;
    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_filename);

    if(!file_exists($s_filename))
    {
        $s_filename = "ujp_process_events/ud_process_event_company.htm";
        $s_filename = $class_main->clmain_set_map_path_n_name("",$s_filename);
    }
    $s_body_map = $class_main->clmain_v100_load_html_screen($s_filename,$ps_details_def,$ps_details_data,"no",$s_code_group);
    $sys_function_out = $sys_function_out.$s_body_map;
C100_EXPORT_FILE:
    if (trim($ps_export_file) == "")
    {
        IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">  e500_ Doing COMPANY_'.$ps_action.'  no export file required ps_export_file=[]'.$ps_export_file.'[]</td></tr>';}
        goto C900_EXIT;
    }
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0"> e500_  Doing COMPANY_'.$ps_action.'  c100 export file  ps_export_file=[]'.$ps_export_file.'[]</td></tr>';}

    $s_filename = $_SESSION['ko_usrdata_path'].$s_company_id.'/config/';
    $s_filename = $s_filename.$ps_process_id;
    $s_filename = $s_filename."_group.mapcsv";
    if(file_exists($s_filename))
    {
        GOTO C800_DO_MAP;
    }
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0"> e500_  Doing COMPANY_'.$ps_action.'  c100 export file  did not find s_filename=[]'.$s_filename.'[]</td></tr>';}

    $s_filename = $_SESSION['ko_usrdata_path'].$s_company_id.'/config/';
    $s_filename = $s_filename."event_group.mapcsv";
    if(file_exists($s_filename))
    {
        GOTO C800_DO_MAP;
    }
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0"> e500_  Doing COMPANY_'.$ps_action.'  c100 export file  did not find s_filename=[]'.$s_filename.'[]</td></tr>';}

    $s_filename = "ujp_process_events/ud_process_event_company.txt";
    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_filename);

C800_DO_MAP:
    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_filename);
    $s_code_group = $ps_action;
    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_filename);
    $s_body_map = $class_main->clmain_v100_load_html_screen($s_filename,$ps_details_def,$ps_details_data,"no",$s_code_group);

    if (trim($s_body_map) == "")
    {
        goto C900_EXIT;
    }
    $fh = fopen($ps_export_file, 'a');
//    IF ($ps_developer_run=="YES"){fwrite($fh, $sys_function_name.' Doing COMPANY_'.$ps_action.'   company id '.$ps_company_id.' action = []'.$ps_action.'[] ps_event_type=[]'.$ps_event_type."[]  s_filename=[]".$s_filename."[]".PHP_EOL);}
    fwrite($fh, $s_body_map.PHP_EOL);
    fclose($fh);
C900_EXIT:

Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    return $sys_function_out;

}
//#################################################################################################################################################################################################
function fn_E600_DO_EVENTTYPE_GROUP($ps_dbcnx,$ps_debug, $ps_called_from, $ps_action, $ps_process_id, $ps_event_type_group,$ps_company_id, $ps_other_params,$ps_details_def,$ps_details_data,$ps_developer_run,$ps_sendtype,$ps_export_file)
{
A010_START:


    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_called_from;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_E600_DO_EVENTTYPE_GROUP";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
// gw 20120429 decided I needed headings and footers for all the nongrouped events
// gw 201204    IF ($ps_event_type_group == "NOGROUP")
// gw 201204    {
// gw 201204        $sys_function_out = "";
// gw 201204        goto Z900_EXIT;
// gw 201204    }

    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;
    $s_details_def = "";
    $s_details_data = "";
    $s_body_map = "";
B000_START:
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">'.$sys_function_name.' Doing GROUP_'.$ps_action.'  company id '.$ps_company_id.' ps_event_type_group = []'.$ps_event_type_group.'[]  action = []'.$ps_action.'[]</td></tr>';}

    $s_filename = "ujp_process_events/ud_process_event_eventtype_group_".$ps_event_type_group.".htm";
    $s_code_group = $ps_action;
    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_filename);
    if(!file_exists($s_filename))
    {
        $s_filename = "ujp_process_events/ud_process_event_eventtype_group.htm";
        GOTO B900_DRAW;
    }

B900_DRAW:
    $s_body_map = $class_main->clmain_v100_load_html_screen($s_filename,$ps_details_def,$ps_details_data,"no",$s_code_group);

    $sys_function_out = $sys_function_out.$s_body_map;
C100_EXPORT_FILE:
    if (trim($ps_export_file) == "")
    {
        IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">'.$sys_function_name.'  Doing GROUP_'.$ps_action.'  no export file required ps_export_file=[]'.$ps_export_file.'[]</td></tr>';}
        goto C900_EXIT;
    }
    $s_export_file = $ps_export_file;

    if (strtoupper(trim($ps_event_type_group)) == "NOGROUP")
    {
        GOTO C200_SKIP_FILENAME;
    }
    if (strtoupper(trim($ps_event_type_group)) == "NONE")
    {
        GOTO C200_SKIP_FILENAME;
    }
//gw20131105 there are no "nogroup" anymore - the group is the name normalised
//    $s_export_file = str_replace(".csv","",$s_export_file)."_".$ps_event_type_group.".csv";
C200_SKIP_FILENAME:
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">'.$sys_function_name.'  Doing GROUP_'.$ps_action.'  export file  s_export_file=[]'.$s_export_file.'[]</td></tr>';}

    $s_filename = "ujp_process_events/ud_process_event_eventtype_group_".$ps_event_type_group."_xml_.txt";
    $s_code_group = $ps_action;
    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_filename);
    if(file_exists($s_filename))
    {
        GOTO C300_DRAW;
    }

    $s_filename = "ujp_process_events/ud_process_event_eventtype_group_".$ps_event_type_group.".txt";
    $s_code_group = $ps_action;
    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_filename);
    if(file_exists($s_filename))
    {
        GOTO C300_DRAW;
    }
    $s_filename = "ujp_process_events/ud_process_event_eventtype_group.txt";
C300_DRAW:
    $s_body_map = $class_main->clmain_v100_load_html_screen($s_filename,$ps_details_def,$ps_details_data,"no",$s_code_group);

    if (strpos(strtolower($s_filename),"_xml_") !==false)
    {
        $s_export_file = str_replace(".csv",".xml",$s_export_file);
    }

    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0">'.$sys_function_name.'  Doing GROUP_'.$ps_action.'  export file  s_export_file=[]'.$s_export_file.'[]</td></tr>';}
    if (trim($s_body_map) == "")
    {
        goto C900_EXIT;
    }
    $fh = fopen($s_export_file, 'a');
    IF ($ps_developer_run=="YES"){fwrite($fh, $sys_function_name.' Doing GROUP_'.$ps_action.'   company id '.$ps_company_id.' action = []'.$ps_action.'[]'.PHP_EOL);}
    fwrite($fh, $s_body_map.PHP_EOL);
    fclose($fh);
C900_EXIT:

Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    return $sys_function_out;

}
//#################################################################################################################################################################################################
function fn_E700_DO_UDELIVERED($ps_dbcnx,$ps_debug, $ps_called_from, $ps_action, $ps_process_id, $ps_company_id, $ps_other_params,$ps_details_def,$ps_details_data,$ps_developer_run,$ps_sendtype,$ps_export_file)
{
A010_START:

    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_called_from;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_E700_DO_UDELIVERED";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:

    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;
    $s_details_def = "";
    $s_details_data = "";
    $s_body_map = "";
B000_START:
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0"> Doing UDELIVERED_'.$ps_action.'  company id '.$ps_company_id.' action = []'.$ps_action.'[] ps_export_file=[]'.$ps_export_file.'[]</td></tr>';}

    $s_filename = "ujp_process_events/ud_process_event_udelivered.htm";
    $s_code_group = $ps_action;
    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_filename);
    $s_body_map = $class_main->clmain_v100_load_html_screen($s_filename,$ps_details_def,$ps_details_data,"no",$s_code_group);

    $sys_function_out = $sys_function_out.$s_body_map;
C100_EXPORT_FILE:
    if (trim($ps_export_file) == "")
    {
        IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0"> Doing UDELIVERED_'.$ps_action.'  no export file required ps_export_file=[]'.$ps_export_file.'[]</td></tr>';}
        goto C900_EXIT;
    }
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0"> Doing UDELIVERED_'.$ps_action.'  export file  ps_export_file=[]'.$ps_export_file.'[]</td></tr>';}
    $s_filename = "ujp_process_events/ud_process_event_udelivered.mapcsv";

    $s_code_group = $ps_action;
    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_filename);
    $s_body_map = $class_main->clmain_v100_load_html_screen($s_filename,$ps_details_def,$ps_details_data,"no",$s_code_group);

    if (trim($s_body_map) == "")
    {
        goto C900_EXIT;
    }
    $fh = fopen($ps_export_file, 'a');
//    IF ($ps_developer_run=="YES"){fwrite($fh, $sys_function_name.' Doing UDELIVERED_'.$ps_action.'   company id '.$ps_company_id.' action = []'.$ps_action.'[]'.PHP_EOL);}
    fwrite($fh, $s_body_map.PHP_EOL);
    fclose($fh);
C900_EXIT:
Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    return $sys_function_out;

}
//#################################################################################################################################################################################################
function fn_E800_DO_CO_MESSAGE($ps_dbcnx,$ps_debug, $ps_called_from, $ps_action, $ps_process_id, $ps_company_id, $ps_other_params,$ps_details_def,$ps_details_data,$ps_developer_run,$ps_sendtype)
{
A010_START:

    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_called_from;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_E700_DO_UDELIVERED";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:

    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;
    $s_details_def = "";
    $s_details_data = "";
    $s_body_map = "";
B000_START:
    IF ($ps_developer_run=="YES"){echo '<tr><td colspan="10" bgcolor="#D0D0D0"> Doing CO_MESSAGE_'.$ps_action.'  company id '.$ps_company_id.' action = []'.$ps_action.'[]</td></tr>';}

    $s_filename = "ujp_process_events/ud_process_event_co_message_".$ps_company_id.".htm";
    $s_code_group = $ps_action;
    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_filename);
    if(!file_exists($s_filename))
    {
        $sys_function_out = "";
//        $sys_function_out = "<TR><TD> FILE DOES NOT EXIST s_filename=[]".$s_filename."[]</TD></TR>";
        goto Z900_EXIT;
    }

    $s_body_map = $class_main->clmain_v100_load_html_screen($s_filename,$ps_details_def,$ps_details_data,"no",$s_code_group);

    $sys_function_out = $sys_function_out.$s_body_map;

Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    return $sys_function_out;

}
//#################################################################################################################################################################################################
function fn_F100_check_event_status($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid, $ps_globaleventid, $fp,$ps_developer_run,$ps_sendtype)
{
    global $class_sql;
    global $class_main;

    $outputstring = 'fn_F100_check_event_status:'.$ps_globaleventid."\n";
    fwrite($fp, $outputstring);

A010_START:

    $dbcnx = $ps_dbcnx;
    $s_sql = "SELECT * from jobs_activity where jobsact_related_id = '".$ps_globaleventid."' and jobsact_group = 'SYSTEM' and jobsact_type = 'EVENTS_STATUS' ";
    $outputstring = 'sql:'.$s_sql."\n";
    fwrite($fp, $outputstring);
    //echo 's_sql='.$s_sql.'<br>';
    //exit();

B_000_RECORD_LOOP:
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_rec_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
        goto B_900_END_RECORDS;
    }
    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql.""."</td></tr>";
    echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;  s_numrows=".$s_numrows.""."</td></tr>";

B_100_GET_RECORDS:
    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_rec_found = "YES";

B_900_END_RECORDS:

Z900_EXIT:
    $outputstring = 'fn_F100_check_event_status, s_rec_found='.$s_rec_found."\n";
    fwrite($fp, $outputstring);

    return $s_rec_found;
}
//#################################################################################################################################################################################################
function fn_F200_create_event_status($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid, $ps_globaleventid,$ps_developer_run,$ps_sendtype)
{
    global $class_sql;
    global $class_main;
    require_once('lib/class_app.php');
    $class_app = new AppClass();

A100_CREATE_RECORD:

    $_COOKIE['ud_logonname']='process_events';
    $_COOKIE['ud_companyid']=$ps_companyid;

    $ps_debug="NO";
    $ps_details_def='';
    $ps_details_data='';
    $ps_sessionno="11";
    $s_Activity_recordgroup='SYSTEM';
    $s_Activity_recordtype='EVENTS_STATUS';
    $s_Activity_desc="SYSTEM EVENT STATUS RECORD";
    $s_Activity_notes="SYSTEM EVENT STATUS RECORD";

    $class_app->clapp_b290_make_jobs_activity($ps_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_globaleventid,$s_Activity_recordgroup,$s_Activity_recordtype,$s_Activity_desc,$s_Activity_notes);

}
/* gw20130409 - function not used
function fn_G100_create_sales_order_xml($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid, $ps_globaleventid,$ps_GlobalDeliveryId,$ps_GlobalEventTypeId,$ps_eventnotes,$ps_process_id,$fp,$ps_developer_run,$ps_sendtype)
{
    global $class_sql;
    global $class_main;
    require_once('lib/class_app.php');
    $class_app = new AppClass();
    global $class_clprocessjobline;
    require_once('lib/sfYaml.php');
    require_once('lib/sfYamlInline.php');
    $class_sfyaml = new sfYaml();
    $class_sfyamlinline = new sfYamlInline();

    $class_sfyaml->setSpecVersion('1.1');

    $i_cnt=0;

A100_GET_SALES_ORDER_DETAILS:

    //echo '<br>Devrun---g100------ DOING G100 ';
    //echo '<br>Devrun---g100-ps_companyid='.$ps_companyid.'';
    //echo '<br>Devrun---g100-ps_globaleventid='.$ps_globaleventid.'';
    //echo '<br>Devrun---g100-ps_GlobalDeliveryId='.$ps_GlobalDeliveryId.'';
    //echo '<br>Devrun---g100-ps_GlobalEventTypeId='.$ps_GlobalEventTypeId.'';
    //echo '<br>Devrun---g100-ps_eventnotes='.$ps_eventnotes.'';

    $s_return_values=$this->fn_G200_get_sales_order_details($ps_dbcnx,$ps_debug, "fn_G100_create_sales_order_xml B158", "NONE", $ps_companyid, $ps_globaleventid,$ps_GlobalDeliveryId,$ps_developer_run,$ps_sendtype);
    $s_company_email=$this->fn_G300_get_users_details($ps_dbcnx,$ps_debug, "fn_G100_create_sales_order_xml B158", "NONE", $ps_companyid,$ps_developer_run,$ps_sendtype);

    //echo '<br>Devrun---g100-s_company_email='.$s_company_email.'';

    //$ar_return_values=explode('|',$s_return_values);

    //$s_so_number = trim($ar_return_values[1]);
    //$s_so_name = trim($ar_return_values[2]);
    //$s_so_email = trim($ar_return_values[3]);
    //$s_so_phone_no = trim($ar_return_values[4]);
    //$s_so_address_1 = trim($ar_return_values[5]);
    //$s_so_address_2 = trim($ar_return_values[6]);
    //$s_so_suburb = trim($ar_return_values[7]);
    //$s_so_postcode = trim($ar_return_values[8]);
    //$s_so_note = trim($ar_return_values[9]);

    //echo 's_so_number='.$s_so_number.'<br>';
    //echo 's_so_name='.$s_so_name.'<br>';
    //echo 's_so_email='.$s_so_email.'<br>';
    //echo 's_so_phone_no='.$s_so_phone_no.'<br>';
    //echo 's_so_address_1='.$s_so_address_1.'<br>';
    //echo 's_so_address_2='.$s_so_address_2.'<br>';
    //echo 's_so_suburb='.$s_so_suburb.'<br>';
    //echo 's_so_postcode='.$s_so_postcode.'<br>';
    //echo 's_so_note='.$s_so_note.'<br>';

B100_GET_NOTES_FIELDS:

    //$yaml_eventnotes = $class_spyc->YAMLLoad($ps_eventnotes);

    //$yaml_eventnotes = yaml_parse($ps_eventnotes);
    //echo 'yaml_eventnotes='.'<br>';
    //print_r($yaml_eventnotes['Value']);
    //echo '<br>';

    if (substr($ps_eventnotes,0,2)<>'{"')
    {
        goto B150_GET_NOTES_FIELDS_END;
    }

    $yaml_eventnotes=$class_sfyamlinline->load($ps_eventnotes);

    $s_client_suburb=trim($yaml_eventnotes['Select a Client']['Value']['suburb']);
    $s_client_postcode=trim($yaml_eventnotes['Select a Client']['Value']['postcode']);
    $s_client_name=trim($yaml_eventnotes['Select a Client']['Value']['name']);
    $s_client_phone=trim($yaml_eventnotes['Select a Client']['Value']['phone']);
    $s_client_address_1=trim($yaml_eventnotes['Select a Client']['Value']['addressLine1']);
    $s_client_address_2=trim($yaml_eventnotes['Select a Client']['Value']['addressLine2']);
    $s_client_email=trim($yaml_eventnotes['Select a Client']['Value']['email']);
    $s_client_details=trim($yaml_eventnotes['Summary']['Value']);
    $s_client_details=str_replace('\n','',$s_client_details);

    //echo '<br>Devrun---g100-s_client_suburb='.$s_client_suburb.'';
    //echo '<br>Devrun---g100-s_client_postcode='.$s_client_postcode.'';
    //echo '<br>Devrun---g100-s_client_name='.$s_client_name.'';
    //echo '<br>Devrun---g100-s_client_phone='.$s_client_phone.'';
    //echo '<br>Devrun---g100-s_client_address_1='.$s_client_address_1.'';
    //echo '<br>Devrun---g100-s_client_address_2='.$s_client_address_2.'';
    //echo '<br>Devrun---g100-s_client_email='.$s_client_email.'';
    //echo '<br>Devrun---g100-s_client_details='.$s_client_details.'';

B150_GET_NOTES_FIELDS_END:

B200_CREATE_XML_RECORD:

    $i_cnt++;

    $s_jcl_outfilenameonly='sage_actlog_'.Date("YmdHis").$ps_globaleventid.'.xml';
    $s_jcl_outfilepath='C:/tdx/html/udelivered/sales-dev2/usrdata/'.$ps_companyid.'/from_ud/';
    $s_jcl_full_outfilename=$s_jcl_outfilepath.$s_jcl_outfilenameonly;

    //echo '<br>Devrun---g100-b200-s_jcl_full_outfilename='.$s_jcl_full_outfilename.'';

    $s_actiondetails='';
    $s_details_def='';
    $s_details_data='';
    $xml_arr='';
    $s_sessionno='11';
    $s_jcl_name='uds_sage_export_xml.jcl';
    if ($ps_GlobalEventTypeId=='139' or $ps_GlobalEventTypeId=='140' or $ps_GlobalEventTypeId=='141' or $ps_GlobalEventTypeId=='142' or $ps_GlobalEventTypeId=='143')
    {
        $s_jcl_name='uds_sage_actlog_export_xml.jcl';
    }

    $_COOKIE['ud_logonname']='pdf_printing';
    $_COOKIE['ud_companyid']=$ps_companyid;

    $file_date=Date("YmdHis");

    $s_details_def='START|POSTV_globaldeliveryid|POSTV_globaleventid|POSTV_globaleventtypeid|POSTV_client_suburb|POSTV_client_postcode|POSTV_client_name|POSTV_client_phone|POSTV_client_address_1|POSTV_client_address_2|POSTV_client_email|POSTV_client_details|POSTV_jcl_outfilenameonly|POSTV_jcl_outfilepath|POSTV_full_outfilename|POSTV_process_id|END';
    $s_details_data='START|'.$ps_GlobalDeliveryId.'|'.$ps_globaleventid.'|'.$ps_GlobalEventTypeId.'|'.$s_client_suburb.'|'.$s_client_postcode.'|'.$s_client_name.'|'.$s_client_phone.'|'.$s_client_address_1.'|'.$s_client_address_2.'|'.$s_client_email.'|'.$s_client_details.'|'.$s_jcl_outfilenameonly.'|'.$s_jcl_outfilepath.'|'.$s_jcl_full_outfilename.'|'.$ps_process_id.'|END';

    $s_actiondetails="run_jcl|".$s_jcl_name."|end";
    $s_returned = $class_clprocessjobline->pf_clprocessjobline($ps_dbcnx,$s_actiondetails,$s_details_def,$s_details_data,$xml_arr,$s_sessionno,'NO');

    //echo '<br>Devrun---g100-b200- s_returned START OF ='.'<br>';
    //print_r($s_returned);
    //echo '<br>Devrun---g100-b200- END OF s_returned='.'<br>';
C100_EMAIL_XML_RECORD:

    $s_subject = "uDelivered ACT! Activity Notification";
    $s_body_map = "The details of the udelivered ACT! message";

    $s_filename = "udelivered_act_notification.html";
    $s_code_group = "message_body";
    $s_body_map = $class_main->clmain_v100_load_html_screen($class_main->clmain_set_map_path_n_name("",$s_filename),$ps_details_def,$ps_details_data,"no",$s_code_group);

// gw 20120218    $s_body_map = '<table border="0"> <tr><td width="100"><img src="http://dev.udelivered.com/sales-dev2/images/banner100_2.png"  alt=""  align="middle" border="0" ></td> </tr>  <tr bgcolor="green"><td>test</td></tr>  <tr bgcolor="blue"><td>test2</td></tr>  </table>';

    $s_body_map_group = "NOMAP";
    $s_csr_notify_id = $s_company_email;

    $s_line_in ="sys_email|v1|sys_email_message|".$s_csr_notify_id."|".$s_subject."|".$s_body_map."|".$s_body_map_group."||NO|".$s_jcl_full_outfilename."|".$s_jcl_outfilenameonly."|END";
//* sys_email|v1|return def|f_clemail_create_email(emailaddress,subject ,body map filename, body map group ,dodug,calledfrom,$ps_details_def,$ps_details_data)| fail url | do debug|END

    //echo "<br>Devrun---g100-c100- all good  do email LINE_IN= ".$s_line_in;

    //echo '<br>Devrun---g100-c100-s_jcl_full_outfilename='.$s_jcl_full_outfilename.'';

    $ps_sessionno = "";

    $s_url_siteparams = "http://#p%!_server_HTTP_HOST_!%#p/als_web_prog/#p%!_SESSION_SUPER_ko_live_or_dev_!%#p/ut_menu.php?fnid=201&map=my_buddy_main.htm&p=1^mypid=#p%!_postV_mypalletid_!%#p^mode=EMAILERR";
    $s_temp = $class_main->clmain_u730_do_email($ps_dbcnx,$s_line_in,"NO",$s_details_def,$s_details_data,$ps_sessionno,$s_url_siteparams);

    //echo '<br>Devrun---g100-c100-after u730 $s_temp='.$s_temp.'';
}
*/
/* gw20130409 - function not used
function fn_G200_get_sales_order_details($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid, $ps_globaleventid,$ps_GlobalDeliveryId,$ps_developer_run,$ps_sendtype)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_G200_get_sales_order_details";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;
    $s_so_number = "NoOrderRecs";

    //echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Get Sales Order Details ".$ps_companyid.""."</td></tr>";

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};

B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    $s_sql = " select * from deliveries where GlobalDeliveryId = '{$ps_GlobalDeliveryId}' and companyid = '{$ps_companyid}' " ;
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        //echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql.""."</td></tr>";
        //echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";
        goto B_900_END_RECORDS;
    }
    //echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql.""."</td></tr>";
    //echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:

    $s_so_number = trim($row['CNNO']);
    $s_so_name = trim($row['Receiver']);
    $s_so_email = trim($row['Email']);
    $s_so_phone_no = trim($row['PhoneNo']);
    $s_so_address_1 = trim($row['AddrLine1']);
    $s_so_address_2 = trim($row['AddrLine2']);
    $s_so_suburb = trim($row['Suburb']);
    $s_so_postcode = trim($row['Postcode']);
    $s_so_note = trim($row['Note']);

    $s_return_values='START|'.$s_so_number.'|'.$s_so_name.'|'.$s_so_email.'|'.$s_so_phone_no.'|'.$s_so_address_1.'|'.$s_so_address_2.'|'.$s_so_suburb.'|'.$s_so_postcode.'|'.$s_so_note.'|END';
    return $s_return_values;

B_158_NEXT:
    $i_record_count = $i_record_count + 1;

B_800_NEXT_REC:
    GOTO B_100_GET_RECORDS;
B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    $sys_function_out=$s_so_number;
    return $sys_function_out;
}
*/

function fn_G300_get_users_details($ps_dbcnx,$ps_debug,$ps_fromwhere, $ps_options, $ps_companyid,$ps_developer_run,$ps_sendtype)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_G300_get_users_details";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;

    //echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Get Sales Users Details ".$ps_companyid.""."</td></tr>";

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};

B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    $s_sql = " select * from users where CompanyId = '{$ps_companyid}' " ;
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        //echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql.""."</td></tr>";
        //echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";
        goto B_900_END_RECORDS;
    }
    //echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql.""."</td></tr>";
    //echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:

    $s_company_email = trim($row['Email']);

    $s_return_values=$s_company_email;
    return $s_return_values;

B_158_NEXT:
    $i_record_count = $i_record_count + 1;

B_800_NEXT_REC:
    GOTO B_100_GET_RECORDS;
B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    $sys_function_out=$s_so_number;
    return $sys_function_out;
}

//#################################################################################################################################################################################################
function fn_Z100_do_date()
{
    $s_date=date("Ymd");
    $s_date_YYYY=substr($s_date,0,4);
    $s_date_MM=substr($s_date,4,2);
    $s_date_DD=substr($s_date,6,2);
    switch($s_date_MM)
    {
        case "01" : $s_date_MMM = "JAN"; break;
        case "02" : $s_date_MMM = "FEB"; break;
        case "03" : $s_date_MMM = "MAR"; break;
        case "04" : $s_date_MMM = "APR"; break;
        case "05" : $s_date_MMM = "MAY"; break;
        case "06" : $s_date_MMM = "JUN"; break;
        case "07" : $s_date_MMM = "JUL"; break;
        case "08" : $s_date_MMM = "AUG"; break;
        case "09" : $s_date_MMM = "SEP"; break;
        case "10" : $s_date_MMM = "OCT"; break;
        case "11" : $s_date_MMM = "NOV"; break;
        case "12" : $s_date_MMM = "DEC"; break;
    }
    $s_date=$s_date_YYYY.$s_date_MMM.$s_date_DD;

    return $s_date;
}
//#################################################################################################################################################################################################
function fn_Z200_ftp_file($ps_site_address,$ps_upload_folder,$ps_username,$ps_password,$ps_upload_file,$ps_ftp_mode,$ps_developer_run,$ps_sendtype)
{
    //echo 'ps_site_address='.$ps_site_address.'<br>';
    //echo 'ps_upload_folder='.$ps_upload_folder.'<br>';
    //echo 'ps_username='.$ps_username.'<br>';
    //echo 'ps_password='.$ps_password.'<br>';
    //echo 'ps_upload_file='.$ps_upload_file.'<br>';
    //echo 'ps_ftp_mode='.$ps_ftp_mode.'<br>';

    $s_conn_id = ftp_connect($ps_site_address);

    // login with username and password
    $login_result = ftp_login($s_conn_id, $ps_username, $ps_password);

    // check connection
    if ((!$s_conn_id) || (!$login_result)) {
        echo "FTP connection has failed!";
        echo "Attempted to connect to $ps_site_address for user $ps_username";
        return false;
    }
    else
    {
        //echo "Connected to $ps_site_address, for user $ps_username";
    }

    if (trim($ps_upload_folder)<>'')
    {
        $chdir = ftp_chdir($s_conn_id,$ps_upload_folder);

        if(!$chdir)
        {
            echo "Could not change directory to {$ps_upload_folder}";
            return false;
        }
    }

    // upload the file
    //$upload = ftp_put($s_conn_id, basename($ps_upload_file), $ps_upload_file, FTP_ASCII);
    $upload = ftp_put($s_conn_id, basename($ps_upload_file), $ps_upload_file, FTP_BINARY);

    // check upload status
    if (!$upload)
    {
        echo "FTP upload has failed!";
    }
    else
    {
        //echo "Uploaded $ps_upload_file to $ps_site_address as $ps_upload_file";
    }

    // close the FTP stream
    ftp_close($s_conn_id);
}
//#################################################################################################################################################################################################
function fn_Z300_SEND_EMAIL($ps_dbcnx,$ps_debug,$ps_called_from, $ps_process_id, $ps_GlobalEventTypeId,$ps_company_id, $ps_other_params, $ps_developer_run,$ps_sendtype,$ps_details_def,$ps_details_data,$ps_email_address,$ps_subject,$ps_body_map,$ps_outfilenameonly,$ps_outfilepath,$ps_developer_run,$ps_sendtype)
{

    global $class_main;

    $s_subject = $ps_subject;
    $s_body_map = $ps_body_map;
    $s_body_map_group = "NOMAP";
    $s_email_address = $ps_email_address;

    $s_outfilenameonly=$ps_outfilenameonly;
    $s_outfilepath=$ps_outfilepath;
    $s_full_outfilename=$s_outfilepath.$s_outfilenameonly;

    $attachment_total_cnt = substr_count($s_outfilenameonly,";");
    $attachment_total_cnt ++;
    $attachment_done_cnt = 0;
    if ($attachment_total_cnt > 1)
    {
        $attachment_name_array = explode(";", $s_outfilenameonly);
        $s_full_outfilename = "";
        while ($attachment_total_cnt > $attachment_done_cnt)
        {
            IF ($ps_developer_run=="YES"){
                echo "<tr><td  bgcolor='#8080FF'>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." EMAIL ATTACHMENT LOOP .attachment_total_cnt=[]".$attachment_total_cnt."[]  attachment_done_cnt=[]".$attachment_done_cnt."[]  s_full_outfilename=[]".$s_full_outfilename."[] </td></tr>";
            }
            $attachment_name = $attachment_name_array[$attachment_done_cnt];
            if (trim($s_full_outfilename) <> "")
            {
                $s_full_outfilename=$s_full_outfilename.";";
            };
            $s_full_outfilename=$s_full_outfilename.$s_outfilepath.$attachment_name;
            $attachment_done_cnt++;
        }
    }

    if (trim($s_subject) == ""){
        $s_subject = "uDelivered Activity Notification";
    }
    if (trim($s_body_map) == ""){
        $s_body_map = "The details of the udelivered activity";
    }
    if (trim($s_email_address) == ""){
        $s_email_address = "service@thedataexchange.com.au";
    }
// gw 201204
    IF ($ps_developer_run=="YES"){
        echo "<tr><td  bgcolor='#8080FF'>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." subject=[]".$s_subject."[]  address=[]".$s_email_address."[] </td></tr>";
        echo "<tr><td  bgcolor='#8080FF'>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." next cell is comment email body </td></tr>";
        echo "<!-- <tr><td <td colspan='10'  bgcolor='#FF8080'>Commented - check page source ".$s_body_map."</td></tr>-->";
        echo "<tr><td  bgcolor='#8080FF'>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." above is commented email body </td></tr>";
    }

//    $s_filename = "udelivered_act_notification.html";
//    $s_code_group = "message_body";
//    $s_body_map = $class_main->clmain_v100_load_html_screen($class_main->clmain_set_map_path_n_name("",$s_filename),$ps_details_def,$ps_details_data,"no",$s_code_group);
    $s_line_in ="sys_email|v1|sys_email_message|".$s_email_address."|".$s_subject."|".$s_body_map."|".$s_body_map_group."||NO|".$s_full_outfilename."|".$s_outfilenameonly."|END";
//* sys_email|v1|return def|f_clemail_create_email(emailaddress,subject ,body map filename, body map group ,dodug,calledfrom,$ps_details_def,$ps_details_data)| fail url | do debug|END

    $s_sessionno = "";
    $s_siteparams = "";

    $s_url_siteparams = "http://#p%!_server_HTTP_HOST_!%#p/als_web_prog/#p%!_SESSION_SUPER_ko_live_or_dev_!%#p/ut_menu.php?fnid=201&map=no_map.htm&p=1^mypid=#p%!_postV_mypalletid_!%#p^mode=EMAILERR";
    $s_temp = $class_main->clmain_u730_do_email($ps_dbcnx,$s_line_in,"NO",$ps_details_def,$ps_details_data,$s_sessionno,$s_siteparams);
            IF ($ps_developer_run=="YES"){
                echo "<tr><td  bgcolor='#8080FF'>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." Result of send email $s_temp = []".$s_temp."[] </td></tr>";
            }
}
//#################################################################################################################################################################################################
function fn_Z400_GET_COMPANY($ps_dbcnx,$ps_debug, $ps_called_from, $ps_company_id, $ps_other_params,$ps_developer_run,$ps_sendtype)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_called_from;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_Z400_GET_COMPANY";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." **********  get the company details  </td></tr>";
    }
B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;
    $s_details_def = "";
    $s_details_data = "";

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};

B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    $s_sql = " select * from companies where companyid  = '".$ps_company_id."' " ;
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        //echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql.""."</td></tr>";
        goto B_900_END_RECORDS;
    }
    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql.""."</td></tr>";
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";
    }

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
    $s_temp = $class_main->clmain_u820_fn_Z999_build_def_data($ps_dbcnx,$ps_debug, "d300 b600_body",$row,"company");
    if (strpos($s_temp,"|^%##%^|") ===false)
    {
    }else{
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $ar_line_details[0];
        $s_details_data = $ar_line_details[1];
        $s_temp = "";
    };

/*    foreach( $row as $key=>$value)
    {
        if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
        {
            GOTO B_151_SKIP_DATA_BLOB;
        }
        if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
        {
            GOTO B_151_SKIP_DATA_BLOB;
        }
        if (strpos(strtoupper($value),"XML") === false)
        {
            GOTO B_151_SKIP_DATA_BLOB;
        }

        $ar_xml = simplexml_load_string($value);
        $newArry = array();
        $ar_xml = (array) $ar_xml;
        foreach ($ar_xml as $key => $value)
        {
            $value = trim($value);
            $s_details_def .= "|REC_co_DB_".$key;
            $s_details_data .= "|".$class_main->clmain_u596_xml_decode_char($value);
        }
        continue;

 B_151_SKIP_DATA_BLOB:
        if (strpos(strtoupper($key),"DETAILS_D") === FALSE)
        {
            $value = trim($value);
            $s_details_def .= "|REC_co_".$key;
            $s_details_data .= "|".$value;
        }
        continue;
    }
*/
 B_158_NEXT:
    $i_record_count = $i_record_count + 1;

B_800_NEXT_REC:
    GOTO B_100_GET_RECORDS;
B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    $sys_function_out = $s_details_def."|^%##%^|".$s_details_data;
    return $sys_function_out;

}
//#################################################################################################################################################################################################
function fn_Z500_GET_DEVICE($ps_dbcnx,$ps_debug, $ps_called_from,$ps_company_id, $ps_device_id, $ps_other_params,$ps_developer_run,$ps_sendtype)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_called_from;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_Z500_GET_DEVICE";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." ********** z500  get the device details  </td></tr>";
    }
B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;
    $s_details_def = "";
    $s_details_data = "";

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};

B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    $s_sql = " select * from devices where companyid  = '".$ps_company_id."' and deviceid = '".$ps_device_id."' " ;
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        //echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql.""."</td></tr>";
        //echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";
        goto B_900_END_RECORDS;
    }
    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql.""."</td></tr>";
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";
    }

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
    $s_temp = $class_main->clmain_u820_fn_Z999_build_def_data($ps_dbcnx,$ps_debug, "z500 b150_process",$row,"device");
    if (strpos($s_temp,"|^%##%^|") ===false)
    {
    }else{
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $ar_line_details[0];
        $s_details_data = $ar_line_details[1];
        $s_temp = "";
    };
B_158_NEXT:
    $i_record_count = $i_record_count + 1;

B_800_NEXT_REC:
    GOTO B_100_GET_RECORDS;
B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    $sys_function_out = $s_details_def."|^%##%^|".$s_details_data;
    return $sys_function_out;

}
//#################################################################################################################################################################################################
function fn_z600_get_filenames($ps_outfilepath,$ps_company_id,$ps_run_id,$ps_developer_run,$ps_sendtype)
{
//    $s_outfilenameonly = "testfile1.txt;testfile.txt";
// dir on the folder for company_id."_".
//7_ActLogger_20120530135335.csv
//7_ActLogger_20120530135335SAGEACT.csv
    $s_file_list = "";

    $files_to_check = $ps_company_id.'_actlogger_'.$ps_run_id;
    $s_file_list = "no_files_located";
    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." ********** fn_z600_get_filenames  files_to_check=[]".$files_to_check."[]</td></tr>";
    }

 // open this directory
    $myDirectory = opendir($ps_outfilepath);
    if (!$myDirectory)
    {
        IF ($ps_developer_run=="YES"){
            echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." ********** fn_z600_get_filenames  folder failed to open </td></tr>";
        }
        GOTO Z900_EXIT;
    }
    // get each entry
    while($entryName = readdir($myDirectory)) {
        if (strpos(strtolower($entryName),strtolower($files_to_check)) !== false)
        {
            IF ($s_file_list == "no_files_located")
            {
                $s_file_list = "";
            }
            iF (trim($s_file_list) <> "")
            {
                $s_file_list = $s_file_list.";";
            }

            $s_file_list = $s_file_list.$entryName;
        }
    }

    // close directory
    closedir($myDirectory);

 Z900_EXIT:
    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." ********** fn_z600_get_filenames  s_file_list=[]".$s_file_list."[] </td></tr>";
    }
    return $s_file_list;

}
//#################################################################################################################################################################################################
function fn_Z700_GET_DELIVERY($ps_dbcnx,$ps_debug, $ps_called_from,$s_GlobalDeliveryId, $ps_device_id, $ps_other_params,$ps_developer_run,$ps_sendtype)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_called_from;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_Z700_GET_DELIVERY";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." ********** z700  get the device details  </td></tr>";
    }
B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;
    $s_details_def = "";
    $s_details_data = "";

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};

B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    $s_sql = " select * from deliveries where GlobalDeliveryId  = '".$s_GlobalDeliveryId."'  " ;
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        //echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql.""."</td></tr>";
        //echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";
        goto B_900_END_RECORDS;
    }
    IF ($ps_developer_run=="YES"){
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql.""."</td></tr>";
        echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows.""."</td></tr>";
    }

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
    $s_temp = $class_main->clmain_u820_fn_Z999_build_def_data($ps_dbcnx,$ps_debug, "z700 b150_process",$row,"delv");
    if (strpos($s_temp,"|^%##%^|") ===false)
    {
    }else{
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $ar_line_details[0];
        $s_details_data = $ar_line_details[1];
        $s_temp = "";
    };
B_158_NEXT:
    $i_record_count = $i_record_count + 1;

B_800_NEXT_REC:
    GOTO B_100_GET_RECORDS;
B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    $sys_function_out = $s_details_def."|^%##%^|".$s_details_data;
    return $sys_function_out;

}



//end of class
    }
    ?>
