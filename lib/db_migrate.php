<?php

require_once('DBConfig.php');
require_once('DBFunctions.php');
require_once('uDeliveredUtilFunctions.inc.php');


/*
 * Gets migration list
 */
$migrations_directory = dirname(__FILE__).'/../db/';

if (!is_dir($migrations_directory))
{
  die("Error 0x80003310: no 'db' directory at: $migrations_directory");
}


$migrations = array();

foreach (new DirectoryIterator($migrations_directory) as $fileInfo)
{
  if($fileInfo->isDot()) // ignore . and ..
    continue;
  if (!$fileInfo->isFile()) // ignore dirs
    continue;
  if (startsWith($fileInfo->getFilename(), "."))
    continue;
  
  if(strstr($fileInfo->getFilename(), ".down.sql") !== FALSE) // ignore *.down.sql for now
    continue;
    
  $migration = array();

  $migration['filename'] = dirname(__FILE__).'/../db/'.$fileInfo->getFilename();
  if (endsWith($fileInfo->getFilename(), ".up.sql"))
  {
    $migration['name'] = substr($fileInfo->getFilename(), 0, -strlen(".up.sql"));
  }
  else if (endsWith($fileInfo->getFilename(), ".sql"))
  {
    $migration['name'] = substr($fileInfo->getFilename(), 0, -strlen(".sql"));
  }
  else
  {
    continue;
  }    
  
  $migrations[$migration['name']] = $migration;
}

//print_r($migrations);
//echo("...<br />");

/*
 * Gets applied migration list
 */
$applied_migrations = array();

// gets migrations list
$sql = 'SELECT * FROM schema_migrations';
$result = exec_query($sql);
while ($migrationsRecord = mysql_fetch_array($result, MYSQL_BOTH))
{
  $applied_migrations[] = $migrationsRecord[0];
}
mysql_free_result($result);

//print_r($applied_migrations);
//echo("...<br />");


/*
 * Applies migrations
 */

// dequeues already applied migrations
foreach ($applied_migrations as $applied_migration)
{
  unset($migrations[$applied_migration]);
} 

// sorts alphabetically (also chronologically due to timestamp filename convention)
ksort($migrations);

//print_r($migrations);
//echo("...<br />");

// aborts if nothing to do
if (count($migrations) == 0)
{
  echo "Everything up to date, no action required.";
  exit;
}

// applies migrations in order
foreach ($migrations as $migration)
{
  exec_query("START TRANSACTION");

  $file_sql = file_get_contents($migration['filename']);

  $queries = preg_split("/;+(?=([^'|^\\\']*['|\\\'][^'|^\\\']*['|\\\'])*[^'|^\\\']*[^'|^\\\']$)/", $file_sql);    # ref: http://th.php.net/manual/en/function.mysql-query.php#89281 -> http://www.dev-explorer.com/articles/multiple-mysql-queries
  //$queries = preg_split("/;/", $sql); 
    
  $queries = array_map('trim', $queries);

  foreach($queries as $sql)
  {
    // strips multi-line comments
    $sql = preg_replace('/\/\*([\s\S]*?)\*\//m', '', $sql);  // ref: http://stackoverflow.com/questions/2458785/regex-to-remove-multi-line-comments

    if ($sql == "")
      continue;
  
    $result = mysql_query($sql);
  
    if (!$result)
    {
      $error_msg = mysql_error();
      $error_no = mysql_errno();
    
      exec_query("ROLLBACK");
      
      echo "Migration '{$migration['name']}' failed. The sub-query that failed was:<br />";
      echo "  <b>sql:</b> <br /><pre style='overflow: auto; padding-bottom: 3em; padding-top: 10px; padding-left: 10px; padding-right: 10px; border: solid 1px black;'>".$sql."</pre><br /><br />";
      echo "  <b>error:</b> <i>code</i> ".$error_no." [<a href='http://dev.mysql.com/doc/refman/5.5/en/error-handling.html'>lookup</a>]<br /><div style='border: solid 1px black; padding: 10px;'>". $error_msg."</div><br /><br />";
      echo "<p><b>This migration in its entirety was not applied & future migrations were aborted</b></p>";
      echo "<p>Consider applying this migration manually (or checking if it has already been applied), then adding <span style='background: b0c1ff;'>{$migration['name']}</span> to the <code>version</code> column of the <code>schema_migrations</code> table. Once that is done, you can run this script again to run the remaining migrations (if any)</p>";
      echo "<br /><br />  <b>full migration sql:</b> <br /><pre style='overflow: auto; padding-bottom: 3em; padding-top: 10px; padding-left: 10px; padding-right: 10px; border: solid 1px black;'>".$file_sql."</pre><br /><br />";
  
      exit;
    }
    else
    {
    }
  }
  
  // records migration
  $sql = "INSERT INTO schema_migrations (`version`) VALUES ('{$migration['name']}')";
  exec_query($sql);

  // all good, commits transaction
  exec_query("COMMIT");
  echo "Migration '{$migration['name']}' succeeded<br /><br />";

}


