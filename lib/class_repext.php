  <?php

    //************************************************************************************************
    //
    //    Class: Printer
    //
    //
    //************************************************************************************************


    class cls_repext
    {

function pf_customError($errno,$errstr,$errfile,$errline,$errcontext)
  {
// NOTE **** THIS ONLY CATCHES NON-FATAL ERRORS
//  echo "<b>Error gw12:</b> [$errno] $errstr $errfile $errline $errcontext.<br>";
// gw 20100315       switch ($error_level) {
       switch ($errno) {
        case E_USER_ERROR:
            echo "<b>My ERROR</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            echo "  Fatal error on line $errline in file $errfile";
            echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
            echo "Aborting...<br />\n";
            exit(1);
            break;

        case E_USER_WARNING:
            echo "<b>My WARNING</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        case E_USER_NOTICE:
            echo "<b>My NOTICE</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        default:
            echo "<BR>Unknown error type: [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;
        }

    /* Don't execute PHP internal error handler */
    return true;

  }


function clsrepext_do_file($ps_filename, $p_dbcnx, $ps_debug)
{

    $ko_map_path="";

    A000_SET_RUN:
         //set error handler
        set_error_handler("pf_customError", E_ALL);
//        date_default_timezone_set('Australia/Brisbane');
//        session_start();

        if (isset($_SESSION['ko_prog_path']))
        {
    //    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
        }else{
        $_SESSION['ko_prog_path'] = "";
        }
        if (isset($_SESSION['ko_dbase_to_connectto']))
        {
    //    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
        }else{
        $_SESSION['ko_dbase_to_connectto'] = "tdxpryda";
        }

        if (isset($_SESSION['ko_map_path']))
        {
    //    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
            $ko_map_path = $_SESSION['ko_map_path'];
        }else{
        $_SESSION['ko_map_path'] = "";
        }

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clsrepext_do_file";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){echo $sys_debug_text.=" ".$sys_function_name." ps_in_line = ".$ps_in_line." ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};



    A000_DEFINE_VARIABLES:
        $sys_prog_name = "";
        $sys_debug = '';
        $s_file_exists = '';
        $s_filename="";
        $map = "";
        $tmp_array = array();
        $sys_function_out = "";
        $s_temp_value = "";

        $s_sessionno = "";
        $s_helplocation = "";
        $function_id = "";
        $get_userid = "";
        $s_details_def="";
        $s_details_data="";
        $s_siteparams = "";
        $s_url_siteparams = "";
        $s_usr_style_list = "";
        $s_usr_style_values= "";
        $username = "";
        $s_inJCL ="";
        $s_inTASK  = "";
        $s_activity = "";

        $s_temp = "";
        $results = "";
        $s_MultiLevelloop1 = "";
        $s_MultiLevelloop2 = "";
        $s_MultiLevelloop3 = "";
        $s_no_jcl_in_file = "";

        $s_output_to_file = "";
        $s_output_to_filename = "";
        $s_output_to_atend = "";
        $s_output_to_atend_do="";
        $sys_downpage="0";
        $sys_linespacing="0";

    A200_LOAD_LIBRARIES:
        $sys_debug = strtoupper("NO");
        $sys_debug = $ps_debug;

    //    $sys_debug = strtoupper("yes");

         IF ($sys_debug == "YES"){echo $sys_prog_name." started debug=".$sys_debug." *** remember to view source - it will save you hours  <br>";};
         require_once($_SESSION['ko_prog_path'].'lib/class_sql.php');
         $class_sql = new wp_SqlClient();
         IF ($sys_debug == "YES"){echo $sys_prog_name." after class_sql<br>";};
         require_once($_SESSION['ko_prog_path'].'lib/class_main.php');
         $class_main = new clmain();
         IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_main <br>";};
         require_once($_SESSION['ko_prog_path'].'lib/class_pdf_creator.php');
         $class_pdf = new cls_pdf_creator();
         IF ($sys_debug == "YES"){echo  $sys_prog_name." after ut_ped_creator <br>";};


    //     require_once($_SESSION['ko_prog_path'].'lib/class_myplib1.php5');
    //     $class_myplib1 = new myplib1();
    //     IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_myplib1<br>";};

    //print_r ($_SESSION);

         require_once($_SESSION['ko_map_path'].'lib/class_app.php');
         $class_apps = new AppClass();

         IF ($sys_debug == "YES"){echo "_get=";print_r($_GET);echo "<br>";};
         IF ($sys_debug == "YES"){echo "_post=";print_r($_POST);echo "<br>";};

    A300_CONNECT_TODBASE:
         $dbcnx = $p_dbcnx; //$class_sql->c_sqlclient_connect();
         $map = $ps_filename;

    A400_GET_PARAMETERS:
//         if (isset($_GET['map'])) $map = $_GET["map"];
//         if (isset($_GET['p'])) $s_url_siteparams = $_GET["p"];

    A500_SET_VALUES:
         $ar_line_details = array();
        $s_MultiLevelloop1 = "NONE";
        $s_MultiLevelloop2 = "NONE";
        $s_MultiLevelloop3 = "NONE";
        $s_output_to_file = "NO";
        $s_output_to_filename = "";
        $s_firsttime = "YES";

        IF ($sys_debug == "YES"){        echo 'ko_map_path='.$ko_map_path.' map='.$map.'<br>';};

        $map = $class_main->clmain_set_map_path_n_name($ko_map_path,$map);
        IF ($sys_debug == "YES"){        echo 'map='.$map.'<br>';};
        $sys_prog_name = "ut_jcl.php";
        $sys_function_name = $sys_prog_name;
        IF ($sys_debug == "YES"){echo $sys_prog_name." ";};

        $s_file_exists='Y';
        if(file_exists($map))
        {
            $array_lines = file($map);
        }
        else
        {
            $s_file_exists='N';
            $sys_function_out =  "<br>".$sys_prog_name." ##### File Not Found-:".$map."<br>";
            GOTO Z900_EXIT;
        }
        $s_no_jcl_in_file = "The file ".$map." has no jcl code";


        IF ($sys_debug == "YES"){echo $sys_prog_name." after file exists check s_map_file_exists=".$s_file_exists."<br>";};
        IF ($sys_debug == "YES"){echo $sys_prog_name." number of lines in the file(array)=".count($array_lines)."<br>";};

        if (strpos($s_url_siteparams,"%!") > 0 )
        {
               $s_sessionno = "utmenu1";
                $_SESSION[$s_sessionno.'username'] = $username;
        }else{
                $s_sessionno = $class_main->clmain_get_param($s_url_siteparams,"s_sessionno","no");
        }
    //    echo "after file open<br>";
//gw20110313 - added user timezone
    if (isset($_SESSION[$s_sessionno.'ut_logon_timezone']))
    {
        $timezone=$_SESSION[$s_sessionno.'ut_logon_timezone'];
        date_default_timezone_set($timezone);
    }else{
    }

    //gw20100127       $s_sessionno = $class_main->clmain_get_param($s_url_siteparams,"s_sessionno","no");
        if (isset($_SESSION[$s_sessionno.'username']))
        {
            $username = $_SESSION[$s_sessionno.'username'];
        }else{
            $username = "Unknown ";
        }
        $s_details_def='START|USERNAME';
        $s_details_data='START|'.$username;
        $s_details_def.="|SITEPARAMS";
        $s_details_data.='|'.$s_url_siteparams;

        if (isset($_SESSION[$s_sessionno.'session_def']))
        {
            $s_details_def.=$_SESSION[$s_sessionno.'session_def'];
            $s_details_data.=$_SESSION[$s_sessionno.'session_data'];
        }
        $s_details_def.='|END';
        $s_details_data.='|END';
        $s_inJCL ="N";
        $s_inTASK  = "N";

        $i = 0;
        $s_line_in = "";

    B100_GET_REC:
        IF ($i >= count($array_lines))
        {
            goto Z700_END;
        }

        $s_line_in = $array_lines[$i];
        IF (substr($s_line_in,0,1) == "*")
        {
            goto Z800_GET_NEXT;
        }

    //echo "<br>got a line line_in = ".$s_line_in;


    // used to ignore the line
        IF (strpos(strtoupper($s_line_in),strtoupper("SKIP_LINE")) === false )
        {}else{
            IF ($sys_debug == "YES"){echo $sys_function_name." "."got the map skip line command <br>".$s_line_in."<br>";};
            goto Z800_GET_NEXT;
        }
    //echo "<br>PASSED TEST A <BR>";
    // used to indicate if there is a start JCL loop
        IF (strpos(strtoupper($s_line_in),strtoupper("START_JCL"))  === false )
        {}else{
            IF ($sys_debug == "YES"){echo $sys_function_name."got the map starter start line for jcl<br>".$s_line_in."<br>";};
            $s_inJCL ="Y";
            goto Z800_GET_NEXT;
        }
    //echo "<br>PASSED TEST B <BR>";
    // used to indicate if there is a end  JCL loop
        IF (strpos(strtoupper($s_line_in),strtoupper("END_JCL"))  === false  )
        {}else{
            IF ($sys_debug == "YES"){echo $sys_function_name."got the map end line for jcl<br>".$s_line_in."<br>";};
            $s_inJCL ="N";
            goto Z700_END;
        }
    //echo "<br>PASSED TEST C <BR>";
    // if before the required loop get the next line
        if ($s_inJCL =="N")
        {
            IF ($sys_debug == "YES"){echo $sys_function_name." ".$i." in jcl  loop<br>";};
            goto Z800_GET_NEXT;
        }

    //echo "<br>PASSED TEST 1 <BR>";



        $s_no_jcl_in_file = "";
        $ar_line_details = explode("|",$s_line_in);

        IF ($sys_debug == "YES"){echo $sys_function_name." line <br>".$s_line_in."<br>";};

        IF (strtoupper($ar_line_details[0]) == "DUMPTABLELISTTOSQLLOG")
        {
            $temp = $class_sql->c_sqlclient_list_tables_to_log($dbcnx);
            goto Z800_GET_NEXT;
        }

        // if fist col blank then = in_task
        if (strtoupper(trim($ar_line_details[0])) == "TASK")
        {
            IF ($sys_debug == "YES"){echo $sys_function_name." ".$i." setup/do a task<br>";};
            goto B200_DO_TASK;
        }

        if ($s_inTASK  == "PDF_MAP_MAKER")
        {
            goto A1_000_PDF_MAP_MAKER;
        }

        IF ($sys_debug == "YES"){        echo "<BR>***  unknown ACTIVITY LINE (WITHIN TASK) line  = ".$s_line_in."<br>";};
        goto Z800_GET_NEXT;

    B200_DO_TASK:
    // IF THE PREV TASK WAS PDF_MAP_MAKER THEN CLOSE THE OPEN FILE
        if ($s_inTASK  == "PDF_MAP_MAKER")
        {
            if (strtoupper(trim($ar_line_details[1]))  <> "PDF_MAP_MAKER")
            {
                    fclose($s_outfile_handle);
            }
        }

        $s_inTASK  = strtoupper(trim($ar_line_details[1]));
        if ($s_inTASK  == "PDF_MAP_MAKER")
        {
            goto A1_000_PDF_MAP_MAKER;
        }
        if ($s_inTASK  == "PDF_MAKER_FROM_MAP")
        {
            goto B1_000_PDF_MAKER_FROM_MAP;
        }
        if ($s_inTASK  == "PDF_MAKER_FROM_MAPIF")
        {
            goto B300_DO_IF;
        }
        if ($s_inTASK  == "DBASE")
        {
            goto C1_000_DO_DBASE;
        }
        if ($s_inTASK  == "EMAIL")
        {
            goto D1_000_DO_EMAIL;
        }

    B290_DO_TASK_END:
            IF ($sys_debug == "YES"){echo "<BR>***  unknown TASK line  = ".$s_line_in."<br>";};
        goto Z800_GET_NEXT;

    B300_DO_IF:
    //GW20101029 - IF PROCESSING HAS NOT BEEN TESTED
    /* STANDARISED IF PROCESSING
    EXTRACT THE IF VALUES AND DERTERMINE IF TRUE OR FALSE
    IF TRUE
    THEN BUILD THE LINE MINUS THE IF FIEDLS AND THE "IIF" IN THE NAME
    THEN SEND BACK TO DO_TASK
    IF FALSE
    NEXT LINE
        IF (strtoupper($ar_line_details[0]) <> "V1")
        {
            GOTO B310_DO_IF_V2;
        }
        $s_field_to_check = $ar_line_details[3];
        $s_comparison_operator = $ar_line_details[4];
        $s_compare_value  = $ar_line_details[5];
        $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl B1_200 ");
        $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_B1_200 ");
        if ($s_true == "FALSE")
        {
            IF ($sys_debug == "YES"){z901_dump( $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);};
            $sys_function_out = "";
            GOTO Z800_GET_NEXT;
        }

        FOR I = 0 TO COUNT($ar_line_details[])
        {
        IF I = 2,4,5
            NEXT I = DONT ADD TO TEMP
        END IF
        $S_TEMP = $S_TEMP."|".$ar_line_details[I]
        NEXT I
        }
        STRIP OF THE FIRST |
        $S_TEMP = MID($S_TEMP,2ND CHAR, REST OF STRING
        $ar_line_details = explode("|",$S_TEMP);
        $s_inTASK  == (STRIP OF THE LAST 2 CHAR FROM $S_INTASK)

        GOTO B1_400_PDF_MAKER_FROM_MAP;

    BUILD THE INBOUND LINE WITHOUT THE IF IN THE NAME AND THE IF FIELDS
    GOTO B200_DO_TASK
    B310_DO_IF_V2:

    B390_DO_IF_END
    */
    B900_END:


    A1_000_PDF_MAP_MAKER:
        IF ($s_inTASK <> "PDF_MAP_MAKER")
        {
            GOTO A1_900_END;
        }
    A1_100:
        $s_activity = strtoupper(trim($ar_line_details[1]));

        IF ($sys_debug == "YES"){echo "<BR>*** do pdf_map_maker activity=".$s_activity." line = ".$s_line_in."<br>";};

        if ($s_activity == "PDF_MAP_MAKER")
        {
            GOTO A1_210_DO_PDF_MAP_MAKER;
        }
        if ($s_activity == "DOREPORTHEADER")
        {
            GOTO A1_220_DO_DOREPORTHEADER;
        }
        if ($s_activity == "DOREPORTFOOTER")
        {
            GOTO A1_230_DO_DOREPORTFOOTER;
        }
    // THE ACTIVITY BECOMES THE LOOP TO PROCESS
        IF ($sys_debug == "YES"){echo "<BR>*** do pdf_map_maker  *** DO LOOP  =".$s_activity." line = ".$s_line_in."<br>";};
        $s_do_loop = $s_activity;
        GOTO A1_800_DO_MAP_LOOP;

    A1_210_DO_PDF_MAP_MAKER:
        // check the output file path exist or create it
        // set the output file name
        $s_outfilename = trim($ar_line_details[2]).".pdfmap";
        $s_outfilename = substr($s_outfilename,strpos($s_outfilename,":=:") + 3) ;
        $s_outfile_handle = fopen($s_outfilename,'w');
        $s_details_def_initial = $s_details_def;
        $s_details_data_initial = $s_details_data;

        $s_page_start = "7.5";
        $s_page_bottom = "27";

        IF ($sys_debug == "YES"){echo "<BR>*** do pdf_map_maker  doing pdf_map_maker acivity outfilename=".$s_outfilename."<br>";};
        GOTO A1_900_END;
    A1_219_END:

    A1_220_DO_DOREPORTHEADER:
        // SETS THE MAP TO BE USED FOR THE REPORT
        IF ($sys_debug == "YES"){echo "<BR>*** do pdf_map_maker  doing DOREPORTHEADER acivity<br>";};
        $s_map_filename = trim($ar_line_details[2]);
        $s_map_filename = str_replace('MAP:=:','',$s_map_filename);
        $s_do_loop = $s_activity;
        GOTO A1_800_DO_MAP_LOOP;
    A1_229_END:

    A1_230_DO_DOREPORTFOOTER:
        // SETS THE MAP TO BE USED FOR THE REPORT
        IF ($sys_debug == "YES"){echo "<BR>*** do pdf_map_maker  doing A1_220_DO_DOREPORTFOOTER acivity<br>";};
        $s_do_loop = $s_activity;
        GOTO A1_800_DO_MAP_LOOP;
    A1_239_END:

    A1_800_DO_MAP_LOOP:
        $s_details_def = $s_details_def_initial ;
        $s_details_data = $s_details_data_initial;
    // build - details def/data from field 2 onward
        $s_details_def = $s_details_def."|sys_downpage" ;
        $s_details_data = $s_details_data."|".$sys_downpage ;

        for ($i1=0;$i1<count($ar_line_details);$i1=$i1+1)
        {
            $s_details_def = $s_details_def."|".substr($ar_line_details[$i1],0,strpos($ar_line_details[$i1],":=:")) ;
            $s_details_data = $s_details_data."|".substr($ar_line_details[$i1],strpos($ar_line_details[$i1],":=:") +3) ;
        }
        $s_details_def=strtoupper($s_details_def);
//        echo "s_details_data=".$s_details_data."<br> s_details_def=",$s_details_def."<br>";
    // run the 'normal' map processing

        // pkn 20101201 - continue here

        $s_group=trim($ar_line_details[1]);
        //$s_filename=$ko_map_path.$s_filename;
        $s_filename=$ko_map_path.$s_map_filename;

        IF ($s_firsttime == "YES")
        {
            $sys_downpage=$s_page_start; //$this->pf_c100_calc_downpage($s_filename,$s_group,"NO",$sys_downpage,$sys_linespacing,"FIRSTTIME");
            if ($sys_downpage <>"0")
            {
                $s_firsttime = "NO";
            }
        }

        IF ($sys_debug == "YES"){echo '<br>s_group='.$s_group.' '.'s_filename='.$s_filename.'<br>';};
        $sys_function_out=pf_b100_DO_DRAW_HTML($s_filename,$s_group,"NO",$s_details_def,$s_details_data);
        IF ($sys_debug == "YES"){echo 'sys_function_out='.$sys_function_out.'<br>';};
        $sys_function_out= str_replace("#p","|",$sys_function_out);
        fwrite($s_outfile_handle,$sys_function_out."\r\n");

// check the map and see if there is a add
// SKIP_LINE  EVERYTIME|ADDTOVALUE|sys_downpage|LINESPACING|end
        $sys_downpage=$this->pf_c100_calc_downpage($s_filename,$s_group,"NO",$sys_downpage,$sys_linespacing,"ADDTOVALUE");

        if ($sys_downpage > $s_page_bottom)
        {
            $sys_downpage= $s_page_start; //$this->pf_c100_calc_downpage($s_filename,$s_group,"NO",$sys_downpage,$sys_linespacing,"FIRSTTIME");
            fwrite($s_outfile_handle,"*CMD |NEWPAGE|\r\n");
        }


/* THIS NEEDS TO BE REWRITTEN TO CALL THE MAP PROCESS AS A FUNCTION
// 20110222 - if the pagedown is > page_length then set to start down
        if ($s_sys_downpage <= $s_page_length)
        {
            goto A1_800_SKIP_CONTINUE_PAGE;
        }
// DO PAGECONTINUEFOOTER
        IF ($sys_debug == "YES"){echo "<BR>*** do pdf_map_maker  doing PAGECONTINUEFOOTER acivity<br>";};
        $s_do_loop = $s_activity;
        GOTO A1_800_DO_MAP_LOOP;
// DO PAGECONTINUEHEADER
        IF ($sys_debug == "YES"){echo "<BR>*** do pdf_map_maker  doing PAGECONTINUEHEADER acivity<br>";};
        $s_map_filename = trim($ar_line_details[2]);
        $s_map_filename = str_replace('MAP:=:','',$s_map_filename);
        $s_do_loop = $s_activity;
        GOTO A1_800_DO_MAP_LOOP;
// SET DOWNPAGE TO START_DOWN

        $s_sys_downpage = $s_start_down;
        GOTO A1_800_SKIP_CONTINUE_PAGE;
*/
A1_800_SKIP_CONTINUE_PAGE:
    // echo "<BR>clasreport *** sys_downpage ".$sys_downpage."<br>";
        goto Z800_GET_NEXT;
A1_900_END:

B1_000_PDF_MAKER_FROM_MAP:
    //TASK|PDF_MAKER_FROM_MAP|INFILE:=:C:\tdx\html\pdf_test\export\20102710\PDFA-C20100930_1143(.pdfmap)
        IF ($s_inTASK <> "PDF_MAKER_FROM_MAP")
        {
            GOTO B1_900_END;
        }
B1_100_DO_PDF_MAKER_FROM_MAP:
        $tmp_array = explode(":=:",$ar_line_details[2]);

        $s_map_data_file = trim($tmp_array[1]);
        $s_map_data_file = $s_map_data_file.".pdfmap";
        $s_pdf_filename = $s_map_data_file.".pdf";
        $s_temp = pf_b500_task_pdf_create($dbcnx,$s_map_data_file,$s_pdf_filename,"no",$s_details_def,$s_details_data,$s_sessionno,$s_url_siteparams);
        $s_temp = "";
    //    DIE ( "<br><br>DIE PDF_MAKER_FROM_MAP<br>".$s_details_def."<br>".$s_details_data);
        goto Z800_GET_NEXT;

B1_900_END:

C1_000_DO_DBASE:
        IF ($s_inTASK <> "DBASE")
        {
            GOTO C1_900_END;
        }
C1_100_DO_DBASE:
        IF ($sys_debug == "YES"){echo "<BR>**** do DBASE line = ".$s_line_in."<br>";};
        goto Z800_GET_NEXT;
    C1_900_END:

    D1_000_DO_EMAIL:
        IF ($s_inTASK <> "EMAIL")
        {
            GOTO D1_900_END;
        }
    D1_100:
        IF ($sys_debug == "YES"){echo "<BR>****  do EMAIL line = ".$s_line_in."<br>";};
        goto Z800_GET_NEXT;
    D1_900_END:

    Z800_GET_NEXT:
        $i = $i + 1;
        goto B100_GET_REC;

    Z700_END:
        if (trim($s_no_jcl_in_file," ") == "")
        { }else{
            $sys_function_out .= "<BR><BR>ERROR - from ut.jcl.php **".$s_no_jcl_in_file."**<br>";
        }
        if (trim($s_no_jcl_in_file," ") <> "")
        {
            $sys_function_out .= "<BR><BR>dddddERROR - from ut.jcl.php **".$s_no_jcl_in_file."**<br>";
        }
    Z900_EXIT:
    // PUT THIS IS A FILE
        if (trim($s_output_to_file) <> "YES")
        {
            GOTO Z950_SKIP_FILE;
        }
        IF (trim($s_output_to_filename) == "")
        {
            $s_output_to_filename = "ut_jcl_output_to_file.txt";
        }
        $s_folder_name = substr($s_output_to_filename,0,strrpos($s_output_to_filename,'\\'));
        if (!is_dir($s_folder_name)){
            IF (!mkdir($s_folder_name,0,true)){
                die ("Failed to create the folder ".$s_folder_name." for file ".$s_output_to_filename);
            }
        }
        $fh = fopen($s_output_to_filename,'a') or die("cant open file".$s_output_to_filename);
    //    fwrite($fh,date("Y-m-d H:i:s",time())."\r\n");
        fwrite($fh,$sys_function_out."\r\n");
        fclose($fh);

    /*
        echo "<br> now=".date("Y-m-d H:i:s",time());
        echo "<br> s_output_to_file=".$s_output_to_file;
    echo "<br> s_output_to_filename=".$s_output_to_filename;
    echo "<br> s_output_to_atend=".$s_output_to_atend;
    echo "<br> s_output_to_atend_do=".$s_output_to_atend_do;
    */
     //die ("gwdead");
        if (strtoupper($s_output_to_atend) == "OPEN")
        {
            GOTO Z905_OPEN_TEXT_FILE;
        }
        if (strtoupper($s_output_to_atend) == "DOMAP")
        {
            GOTO Z910_DO_MAP;
        }
        GOTO Z950_SKIP_FILE;

    Z905_OPEN_TEXT_FILE:
        $sys_function_out = "";
        $sys_function_out .= '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
        $sys_function_out .= '<html>';
        $sys_function_out .= '<head>';
    //    $sys_function_out .= '<meta HTTP-EQUIV="REFRESH" content="0; '.$s_output_to_atend_do.'">';
        $sys_function_out .= '<br>This process requires you to open <a href='.$s_output_to_atend_do.'>this file</a>';
    //    $sys_function_out .= '<br><a href='.$s_output_to_atend_do.'>map file</a>';
        $sys_function_out .= '</head>';
        $sys_function_out .= '</html>';
        GOTO Z950_SKIP_FILE;

        Z910_DO_MAP:
        $sys_function_out = "";
        $sys_function_out .= '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
        $sys_function_out .= '<html>';
        $sys_function_out .= '<head>';
    //    $sys_function_out .= '<meta HTTP-EQUIV="REFRESH" content="0; '.$s_output_to_atend_do.'">';
        $sys_function_out .= '<br>DO MAP PROCESS This process requires you to open <a href='.$s_output_to_atend_do.'>this file</a>';
    //    $sys_function_out .= '<br><a href='.$s_output_to_atend_do.'>map file</a>';
        $sys_function_out .= '</head>';
        $sys_function_out .= '</html>';

        GOTO Z950_SKIP_FILE;
    Z950_SKIP_FILE:
         IF ($sys_debug == "YES"){ ECHO $sys_function_out;};

}

// ***********************************************************************************************************************************
// start functions

function pf_b100_DO_DRAW_HTML($ps_filename,$ps_group,$ps_debug,$ps_details_def,$ps_details_data)
{

    global $class_main;

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - pf_b100_DO_DRAW_HTML";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_in_line = ".$ps_in_line." ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};

    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;

//    $sys_function_out .= "gwdebug ut_jcl.php load ".$ps_group." from file ".$ps_filename."<br>";

    $sys_function_out .= $class_main->clmain_v100_load_html_screen($ps_filename,$s_details_def,$s_details_data,"no",$ps_group);

    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ";};
    IF ($sys_debug == "YES"){$sys_function_out.=" <!--".$sys_debug_text."-->";};


    return $sys_function_out;

}
//##########################################################################################################################################################################

function pf_b110_DO_DRAWIF_HTML($ps_line_in,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    global $class_main;
//* DrawIF|v1|field|operator|value|map_name|loop|debug|END

//DrawIF|v1|url_MODE|=|WELCOME|my_buddy_main.htm|welcome_map|no|END

    $s_line = $ps_line_in;
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }

    if (strpos(strtoupper( $s_line),"**DODEBUG**") === false )
    {}else
    {
        $sys_debug  = "YES";
        $sys_debug_text = "Inline debug";
        $s_line = str_replace("**dodebug**","",$s_line);
        $s_line = str_replace("**DODEBUG**","",$s_line);
        $s_line = str_replace("**DoDebug**","",$s_line);
    }

    $sys_function_name = "";
    $sys_function_name = "debug - pf_b100_DO_DRAW_HTML";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo "<br>".$sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){echo "<br>".$sys_debug_text.=" ".$sys_function_name." ps_in_line = ".$ps_line_in." processing line = ".$s_line." ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};

A080_START:
    $ar_line_details = explode("|",$s_line);
    if (strtoupper($ar_line_details[0]) != "DRAWIF")
    {
        goto Z900_EXIT;
    }
    if (strtoupper($ar_line_details[1]) != "V1")
    {
        goto A1_DO_V2;
    }

A100_INIT_VARS:
    $s_field_to_check = $ar_line_details[2];
    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];

    $s_map_name = $class_main->clmain_set_map_path_n_name("",$ar_line_details[5]);
    $s_map_group = $ar_line_details[6];
    $s_line_debug = $ar_line_details[7];

    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;

B100_START:
    $sys_function_out = "?b110_unknown".$ps_line_in."?";
    $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b1100");
    IF ($sys_debug == "YES"){echo "<br>".$sys_debug_text.=" ".$sys_function_name." s_field_to_check = ".$s_field_to_check." s_field_value = ".$s_field_value;};

C100_DO_V1:
    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_b110");
    if ($s_true == "FALSE")
    {
        IF ($sys_debug == "YES"){z901_dump( $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);};
        $sys_function_out = "";
        GOTO Z900_EXIT;
    }



    $sys_function_out = $class_main->clmain_v100_load_html_screen($s_map_name,$s_details_def,$s_details_data,"NO",$s_map_group);

    IF ($sys_debug == "YES"){echo "<br>".$sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ";};
    IF ($sys_debug == "YES"){echo "<br>".$sys_function_out.=" <!--".$sys_debug_text."-->";};


GOTO Z900_EXIT;

A1_DO_V2:

Z900_EXIT:
    return $sys_function_out;

}
//##########################################################################################################################################################################
function Pf_b200_do_dataloop($ps_dbcnx,$ps_sql,$ps_filename,$ps_group,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno, $ps_page_current,$ps_page_limit)
{
    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - Pf_b200_do_dataloop";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." ps_sql = ".$ps_sql." ps_filename = ".$ps_filename."  ps_group = ".$ps_group."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ");};

    // define function specific variables and code
//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";};

//    $sys_function_out = "before globals";
//    return $sys_function_out;
//    EXIT;

    global $class_sql;
    global $class_main;

    $s_sql = "";
    $result = "";
    $s_rec_found = "";
    $s_details_def = "";
    $s_details_data = "";

    $ar_dd = array();

    $s_sql = $ps_sql;
    $s_details_def = $ps_details_def ;
    $s_details_data = $ps_details_data ;

    $s_sql = $class_main->clmain_v200_load_line( $s_sql,$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b200");

//    IF (strpos(strtoupper($s_sql),"|%!") > 0 )
//    {
//    }else{
//        //null
//    }


    $s_page_current = $ps_page_current;
    $s_page_limit = $ps_page_limit;

    $s_sql_start = microtime(true);

    $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_sql);
    $s_total_rows = mysql_affected_rows($ps_dbcnx);

    $s_total_pages = round($s_total_rows/$s_page_limit,0) +1;

    $s_page_next = $s_page_current + 1;
    if ($s_page_next > $s_total_pages)
    {
        $s_page_next = $s_total_pages;
    }
    $s_next_starts_at_row = $s_page_next * $s_page_limit;

    $s_page_prev = $s_page_current -1;
    if ($s_page_prev < 1)
    {
        $s_page_prev = 1 ;
    }
    $s_prev_starts_at_row = $s_page_prev * $s_page_limit;
    $s_current_starts_at_row =  ($s_page_current -1) * $s_page_limit;

    $s_sql = $class_sql->c_sqlclient_add_select_limit($s_sql,$s_page_current,$s_page_limit);

    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql." ");};
    $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_sql);
    $s_rec_found = "NO";

    $S_sql_end =  microtime(true);
    $s_sql_duration = round($S_sql_end - $s_sql_start,5);

    $s_page_def = "rundatasql_upd_affected_rows|rundatasql_duration|rundatasql_total_rows|rundatasql_page_current|rundatasql_current_starts_at_row|rundatasql_page_limit|rundatasql_total_pages|rundatasql_page_next|rundatasql_next_starts_at_row|rundatasql_page_prev|rundatasql_prev_starts_at_row|rundatasql_sql";
    $s_page_data = mysql_affected_rows($ps_dbcnx)."|".$s_sql_duration." secs|".$s_total_rows."|".$s_page_current."|".$s_current_starts_at_row."|".$s_page_limit."|".$s_total_pages."|".$s_page_next."|".$s_next_starts_at_row."|".$s_page_prev."|".$s_prev_starts_at_row."|".$s_sql;

    $_SESSION['rundata_sql_def'] = $s_page_def;
    $_SESSION['rundata_sql_data'] = $s_page_data;

    $s_next_prev_row = $class_main->clmain_v950_next_prev_row($_SESSION['rundata_sql_def'],"NO","ut_jcl rundata",$ps_details_def,$ps_details_data,$ps_sessionno);

    $s_details_def = $ps_details_def."|SYS_RD_NEXT_PREV_ROW";
    $s_details_data = $ps_details_data."|".$s_next_prev_row;
    $sys_function_out .= $class_main->clmain_v100_load_html_screen($ps_filename,$s_details_def,$s_details_data,"no",$ps_group."_header_(spaceonerror)");

//        echo "gwetst ".$s_page_def."   data = ".$s_page_data;

    while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
    {
        $s_rec_found = "YES";
        $s_details_def = $ps_details_def;
        $s_details_data = $ps_details_data;
        foreach( $row as $key=>$value)
        {
            if (strpos(strtoupper($key),"DETAILS_D") === FALSE)
            {
//                $s_details_def .= "|REC_".my_sql_field_table($result,0)."_".$key;
                $s_details_def .= "|REC_".mysql_field_table($result,0)."_".$key;
                IF (STRPOS($value,"|") === false)
                {
                    $s_details_data .= "|".$value;
                }else{
                    $s_details_data .= "|".str_replace("|","#*pipe*#",$value);
                    $array_data = explode("|",$value);
//                    echo "ut_jcl b200 - there is a pipe in the record data for def |REC_".mysql_field_table($result,0)."_".$key." value=".$value."<br>";
                   for ( $i2 = 0; $i2 < count($array_data); $i2++)
                   {
                        $s_details_def .= "|REC_".mysql_field_table($result,0)."_".$key."_".$i2;
                        $s_details_data .= "|".$array_data[$i2];
                    }
                }

                continue;
            }
            if (strtoupper($key) == "DETAILS_DEF" )
            {
//               $s_details_def .= "|DD_".$key;
               $ar_dd = explode("|",$value);
               for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
               {
                    $s_details_def .= "|DD_".$ar_dd[$i2];
                }
                continue;
            }
            if (strtoupper($key) == "DETAILS_DATA" )
            {
//               $s_details_data .= "|DD_".$value;
               $ar_dd = explode("|",$value);
               for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
               {
                    $s_details_data .= "|".$ar_dd[$i2];
                }
                continue;
            }
C600_FOR_EACH_END:
        }
C500_CHECK_DEF:

    $array_def = explode("|",$s_details_def);
    $array_data = explode("|",$s_details_data);

    if (count($array_def)<> count($array_data))
    {
           echo "clmain Pf_b200_do_dataloop  _ def/data field count error start of list  - def count=".count($array_def)." data count = ".count($array_data)."<br>";
           echo "s_fieldname =".$s_fieldname."  <br>";
           echo "s_details_def =".$s_details_def."  <br>";
           echo "s_details_data =".$s_details_data."  <br>";

           $s_count = count($array_def);
           if (count($array_data) > $s_count)
           {
               $s_count = count($array_data);
           }
           $ar_dd = explode("|",$s_details_def);
           $ar_ddata = explode("|",$s_details_data);
               for ( $i2 = 0; $i2 < $s_count; $i2++)
               {
                    echo "s_details_def ".$i2." =".$ar_dd[$i2]."=data =".$ar_ddata[$i2]."  <br>";
                }

           echo "class_repext Pf_b200_do_dataloop _ def/data field count error end of list <br>";
    }ELSE{
//                   echo "clmain Pf_b200_do_dataloop _ def/data ALL GOOD <br>";

    }

C590_END:
        $sys_function_out .= $class_main->clmain_v100_load_html_screen($ps_filename,$s_details_def,$s_details_data,"no",$ps_group);
C900_WHILE_LOOP:
    }
    if ($s_rec_found == "NO")
    {
// gw 20100502         $sys_function_out .= "There is no data for your selection ".$sys_function_name."<br>".$s_sql;
        $sys_function_out .= $class_main->clmain_v100_load_html_screen($ps_filename,$s_details_def,$s_details_data,"no",$ps_group."_nodata");
        if (strpos(strtoupper($sys_function_out),"*ERROR") === false)
        {}else
        {
           $sys_function_out = "There is no data for your selection and the group ".$ps_group."_nodata is not in the map.  ".$sys_function_name."<br>".$s_sql;
        }
    }

    $s_details_def = $ps_details_def."|SYS_RD_NEXT_PREV_ROW";
    $s_details_data = $ps_details_data."|".$s_next_prev_row;
    $sys_function_out .= $class_main->clmain_v100_load_html_screen($ps_filename,$s_details_def,$s_details_data,"no",$ps_group."_footer_(spaceonerror)");


    IF ($sys_debug == "YES"){z901_dump( $sys_function_name."  returned  = ".$sys_function_out." ");};

    return $sys_function_out;

}
//##########################################################################################################################################################################
function pf_b490_set_value($ps_dbcnx,$ps_mll_tally_value,$ps_field_value,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_calledfrom)
{
//A100_TEMPLATE-INIT
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - pf_b490_set_value  called from ".$ps_calledfrom;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_in_line = ".$ps_in_line." ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};
//A199_END_TEMPLATE_INIT:

//B100_ define function specific variables and codE

    if (trim($ps_field_value,"") == "")
    {
        $ps_field_value =  "0";
    }
    if (!is_numeric($ps_field_value))
    {
       if (strpos($ps_field_value,"%!_") === false)
       {
           $ps_field_value =  "0";
       }
       $sys_function_out =  $ps_field_value;
    }else{
        $sys_function_out = $ps_mll_tally_value + $ps_field_value;
    }

//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";};

//X900_EXIT:
    return $sys_function_out;
}
//##########################################################################################################################################################################
function pf_b500_task_pdf_create($ps_dbcnx,$ps_map_data_file, $ps_pdf_filename,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_url_siteparams)
{
    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - pf_b500_task_pdf_create";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." ps_map_data_file= ".$ps_map_data_file." ps_pdf_filename = ".$ps_pdf_filename." ps_debug=".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ");};

//    echo "start doing urlvalue<br>".$sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ";

    // define function specific variables and code
//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";};

//    $sys_function_out = "before globals";
//    return $sys_function_out;
//    EXIT;

    global $class_sql;
    global $class_main;
    global $class_pdf;

A001_DEFINE_VARS:
    $s_map_data_file= "";
    $s_pdf_filename="";

A100_INIT_VARS:

B001_DO_V1:
    $s_map_data_file=$ps_map_data_file; //'temp_files/map_file_test.txt';<br>
    $s_pdf_filename=$ps_pdf_filename;  //date("Ymd").date("His").".pdf";<br>
    $class_pdf->cls_create_pdf_file($s_pdf_filename,$s_map_data_file);


B900_END:
//    echo "doing urlvalue<br>s_field_name=".$s_field_name."<br>s_param_name=".$s_param_name."<br>sys_function_out=".$sys_function_out."<br>";
A1_DO_V2:

Z900_EXIT:
 ECHO "DONE";
//    $sys_function_out = $s_field_name."|^%##%^|".$sys_function_out;

    IF ($sys_debug == "YES"){z901_dump( $sys_function_name."  returned  = ".$sys_function_out." ");};

    return $sys_function_out;

}
//##########################################################################################################################################################################
function Pf_b600_do_app_class($ps_dbcnx,$ps_line,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - Pf_b600_do_app_class";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ");};

//    echo "start doing urlvalue<br>".$sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ";

    // define function specific variables and code
//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";};

//    $sys_function_out = "before globals";
//    return $sys_function_out;
//    EXIT;

    global $class_sql;
    global $class_main;
    global $class_apps;

A001_DEFINE_VARS:
    $ar_line_details = array();
    $s_action = "";
    $s_function = "";
    $s_function_param = "";
    $s_params = "";
    $s_field_name = "";

A100_INIT_VARS:
    $ar_line_details = explode("|",$ps_line);
    $s_field_name = $ar_line_details[2];
    $s_function_param = $ar_line_details[3];
    $sys_function_out = "?b600_unknown".$ps_line."?";
    $s_line_debug = $ar_line_details[4];

    $s_function = substr($s_function_param,0,strpos($s_function_param,"("));
    $s_params = substr($s_function_param,strpos($s_function_param,"("));
    $s_params = STR_REPLACE("#P","|",$s_params);
    $s_params = STR_REPLACE("#p","|",$s_params);
    $s_params = STR_REPLACE(",","^PARAM^",$s_params);
    $s_params = STR_REPLACE("(","",$s_params);
    $s_params = STR_REPLACE(")","",$s_params);
    $s_params = STR_REPLACE('#"',"^^#^",$s_params);
    $s_params = STR_REPLACE('"',"",$s_params);
    $s_params = STR_REPLACE("^^#^",'"',$s_params);
    $s_params = $class_main->clmain_v200_load_line( $s_params,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"ut_jcl b600a");
    $s_action = "RUN_FUNCTION";
    if (strtoupper(trim($s_field_name," ")) ==  strtoupper(trim("sys_app_function_list"," ")))
    {
        $s_action = "LIST_FUNCTIONS";
    }

    if (strtoupper(trim($s_field_name," ")) ==  strtoupper(trim("sys_app_function_help"," ")))
    {
        $s_action = "FUNCTIONS_HELP";
    }
//    echo "<br>s_function=".$s_function;
//    echo "<br>s_params=".$s_params;

B100_START:
    if (strtoupper($ar_line_details[0]) != "APP_CLASS")
    {
        goto Z900_EXIT;
    }
    if (strtoupper($ar_line_details[1]) != "V1")
    {
        goto A1_DO_V2;
    }
C100_DO_V1:
    $sys_function_out = $class_apps->clapp_a100_do_app_class($s_function,$s_params,$s_action,$ps_dbcnx,$s_line_debug,"ut_jcl b600b",$ps_details_def,$ps_details_data,$ps_sessionno);
    $sys_function_out = $class_main->clmain_v200_load_line($sys_function_out,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"ut_jcl b600c100");

//    echo "doing urlvalue<br>s_field_name=".$s_field_name."<br>s_param_name=".$s_param_name."<br>sys_function_out=".$sys_function_out."<br>";
A1_DO_V2:

Z900_EXIT:
    $sys_function_out = $s_field_name."|^%##%^|".$sys_function_out;
//    $sys_function_out = $s_field_name."|^%##%^|this is working".$sys_function_out;

    IF ($sys_debug == "YES"){z901_dump( $sys_function_name."  returned  = ".$sys_function_out." ");};

    return $sys_function_out;

}
//##########################################################################################################################################################################
//sys_classif|v1|url_MODE|=|NEWREC|NULL|clmain_v940_initialise_error_sessionvs(#p%!_SESSION_SUPER_validate_error_html_!%#p,NO,sysset)|NO|END
function Pf_b655_do_sys_classif($ps_dbcnx,$ps_line,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{

    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - Pf_b655_do_sys_classif";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ");};


    global $class_sql;
    global $class_main;
    global $class_apps;

A001_DO_IF:
    $ar_line_details = array();
    $s_action = "";
    $s_function = "";
    $s_function_param = "";
    $s_params = "";
    $s_field_name = "";

    $ar_line_details = explode("|",$ps_line);
    if (strtoupper($ar_line_details[0]) != "SYS_CLASSIF")
    {
        goto Z900_EXIT;
    }
    if (strtoupper($ar_line_details[1]) != "V1")
    {
        goto A1_DO_V2;
    }

    if (count($ar_line_details) <> 9 )
    {
        echo "<br> ****  ut_jcl - sys_classif *** the number of fields is not correct";
        echo "<br> ****  ".count($ar_line_details)." instead of the expected 9 <br>";

    }

    $s_field_to_check = $ar_line_details[2];
    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];

    $s_field_name = $ar_line_details[5];
    $s_function_param = $ar_line_details[6];
    $s_line_debug = $ar_line_details[7];

    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;

B100_START:
    $sys_function_out = "?b110_unknown".$ps_line."?";
    $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b655");

C100_DO_V1:
    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_b655a");
    if ($s_true == "FALSE")
    {
        IF ($sys_debug == "YES"){z901_dump( $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);};
        $sys_function_out = "";
        GOTO Z900_EXIT;
    }



A100_INIT_VARS:
    $sys_function_out = "?b650_unknown".$ps_line."?";

    $s_function = substr($s_function_param,0,strpos($s_function_param,"("));
    $s_params = substr($s_function_param,strpos($s_function_param,"("));
    $s_params = STR_REPLACE("#P","|",$s_params);
    $s_params = STR_REPLACE("#p","|",$s_params);
    $s_params = STR_REPLACE(",","^",$s_params);
    $s_params = STR_REPLACE("(","",$s_params);
    $s_params = STR_REPLACE(")","",$s_params);
    $s_params = STR_REPLACE('"',"",$s_params);

    $s_params = $class_main->clmain_v200_load_line( $s_params,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"ut_jcl b655b ");
    $s_action = "RUN_FUNCTION";
    if (strtoupper(trim($s_field_name," ")) ==  strtoupper(trim("sys_app_function_list"," ")))
    {
        $s_action = "LIST_FUNCTIONS";
    }

    if (strtoupper(trim($s_field_name," ")) ==  strtoupper(trim("sys_app_function_help"," ")))
    {
        $s_action = "FUNCTIONS_HELP";
    }

C100_DO_V1a:
    $sys_function_out = $class_main->clmain_a100_do_sys_class($s_function,$s_params,$s_action,$ps_dbcnx,$s_line_debug,"ut_jcl b655c ",$ps_details_def,$ps_details_data,$ps_sessionno);
    $sys_function_out = $class_main->clmain_v200_load_line($sys_function_out,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"ut_jcl b655d ");

//    echo "doing urlvalue<br>s_field_name=".$s_field_name."<br>s_param_name=".$s_param_name."<br>sys_function_out=".$sys_function_out."<br>";
A1_DO_V2:

Z900_EXIT:
    $sys_function_out = $s_field_name."|^%##%^|".$sys_function_out;
//    $sys_function_out = $s_field_name."|^%##%^|this is working".$sys_function_out;

    IF ($sys_debug == "YES"){z901_dump( $sys_function_name."  returned  = ".$sys_function_out." ");};

    return $sys_function_out;

}
//##########################################################################################################################################################################
// SKIP_LINE  EVERYTIME|ADDTOVALUE|sys_downpage|LINESPACING|end
function  pf_c100_calc_downpage($ps_filename,$ps_group,$ps_debug,$ps_sys_downpage,$ps_sys_linespacing,$ps_lookfor)
{
//open file
// process map group
// if line = everytime|addtovalue
// if ps_sys_linespacing = "0" use ".5"
// if LINESPACING use the ps_sys_linespacing
// else use the down value
//
    global $class_main;

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }

    $sys_function_name = "";
    $sys_function_name = "debug - pf_c100_calc_downpage";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo "<br>".$sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};

A080_START:

A100_INIT_VARS:
    $s_map_name = $class_main->clmain_set_map_path_n_name("",$ps_filename);
    $ps_map_set = $ps_group;
    $s_sys_downpage = $ps_sys_downpage;
    $s_sys_linespacing = $ps_sys_linespacing;
B100_START:

    $s_map_set = strtoupper($ps_map_set);

    $s_space_on_error = "N";

    if(file_exists($s_map_name))
    {
        $array_map_lines = file($s_map_name);
    }
    else
    {
        $s_map_file_exists='N';
        $s_map_line =  "<br>clmain_v100_load_html_screen ##### File Not Found-:".$s_map_name."<br>";
        return $s_map_line;
        exit();
    }
    IF ($sys_debug == "YES"){echo $sys_function_name."after file exists check s_map_file_exists=".$s_map_file_exists."<br>";};
    IF ($sys_debug == "YES"){echo $sys_function_name."number of lines in the file(array)=".count($array_map_lines)."<br>";};

    $s_map_file_error="file exists - last note before the read process - if you see this your map may not have a start_map_here or end_map_here";

    $s_before_map ="Y";
    $s_after_map ="N";
    $s_in_map ="N";
    $s_lineout ="";
    $s_inskip ="N";
    $s_line_tag_start = "START_".strtoupper($s_map_set)."_HERE";
    $s_line_tag_end = "END_".strtoupper($s_map_set)."_HERE";
    $s_indoif = "N";
    $s_inJCL = "N";

    $numrows = count($array_map_lines);
    $i_record_count = "0";
C100_GET_LINE:
    IF ($i_record_count > $numrows )
    {
        goto C109_END;
    };
    $s_line = $array_map_lines[$i_record_count];
//    echo "<BR>clasreport c100 - c100  sysdownpage=".$s_sys_downpage." s_line=".$s_line." s_in_map=".$s_in_map." s_line_tag_start= ".$s_line_tag_start."<br>";

    IF (strpos(strtoupper($s_line),strtoupper($s_line_tag_start)) === false )
    {}else{
        IF ($sys_debug == "YES"){echo $sys_function_name." "."got the map starter line for ".$s_line_tag_start."<br>".$s_line."<br>";};
        $s_in_map ="Y";
        goto C108_NEXT;
    }
    IF (strpos(strtoupper($s_line),strtoupper($s_line_tag_end)) === false )
    {}else{
        IF ($sys_debug == "YES"){echo $sys_function_name." "."got the map end line for ".$s_line_tag_end."<br>".$s_line."<br>";};
        $s_in_map ="N";
        goto C109_END;
    }
    if ($s_in_map = "Y" )
    {
        goto C102_DO_LINE;
    }
    goto C108_NEXT;
C102_DO_LINE:
    if (strtoupper($ps_lookfor) == "FIRSTTIME")
    {
//    echo "<BR>clasreport c100 - c102_GOTO FIRSTTIME<br>";
        GOTO C102A_FIRSTTIME;
    }
//    echo "<BR>clasreport c100 - c102_do_line sysdownpage=".$s_sys_downpage." s_line=".$s_line." s_in_map=".$s_in_map." s_line_tag_start= ".$s_line_tag_start."<br>";
// SKIP_LINE  EVERYTIME|ADDTOVALUE|sys_downpage|LINESPACING|end
    IF (strpos(strtoupper($s_line),strtoupper("EVERYTIME|ADDTOVALUE|")) === false ){
        goto C108_NEXT;
    }
//    echo "<BR>clasreport c100 - c102_do_line found the everytime line<br>";
    $ar_line = explode("|",$s_line);
    if ($s_sys_linespacing == "0")
    {
        $s_sys_linespacing = "1.5";
    }
    if (strtoupper($ar_line[3]) == "LINESPACING")
    {
//        echo "<BR>        add sys linespaceing _sys_linespacing = ".$s_sys_linespacing."<br>";
        $s_sys_downpage = $s_sys_downpage +  $s_sys_linespacing;
    }else{
//        echo "<BR>        add sys linespaceing _ar_line(3) = ".$ar_line(3)."<br>";
        $s_sys_downpage = $s_sys_downpage +  $ar_line(3);
    }
    GOTO C109_END;
C102A_FIRSTTIME:
//SKIP_LINE FIRSTTIME|SETVALUE|sys_downpage|8.5|end
    IF (strpos(strtoupper($s_line),strtoupper("FIRSTTIME|SETVALUE|")) === false ){
//    echo "<BR>clasreport c100 - not setvalue =".$s_line."<br>";
        goto C108_NEXT;
    }
//    echo "<BR>clasreport c100 - c102A_do_line found the everytime line<br>";
    $ar_line = explode("|",$s_line);
//    echo "<BR>        SET TO ar_line(3) = ".$ar_line(3)."<br>";
    $s_sys_downpage = $s_sys_downpage +  $ar_line(3);
    GOTO C109_END;

C108_NEXT:
    $i_record_count = $i_record_count + 1;
    goto C100_GET_LINE;
C109_END:

Z900_EXIT:
    IF ($sys_debug == "YES"){$s_sys_downpage.= "v100 **** DEBUG ACTIVE - VIEW SOURCE   ";};
    IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name." LINEOUT  = ".$s_lineout."");};

    return $s_sys_downpage;

}

//##########################################################################################################################################################################
function z901_dump($ps_text)
{

    $file_path  = "";
    if (isset($_SESSION['ko_map_path']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
        $file_path = $_SESSION['ko_map_path'];
    }

    $file_path = $file_path."logs";
    if (!file_exists($file_path))
    {
        $md= mkdir($file_path);
    }

    $dateym = date('Ym');
    $myfile = $file_path."\UT_JCL_".$dateym.".txt";
    $fh = fopen($myfile,'a') or die("cant open file".$myfile);
    fwrite($fh,date("Y-m-d H:i:s",time())." - ".$ps_text."\r\n");
    fclose($fh);

}

}

?>
