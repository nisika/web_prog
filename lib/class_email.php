<?php

    class sys_clemail
    {
/* gw 20100322  DECIDED NOT TO USE THE DO_CLASS
        function sys_clemail_a100_do_class($ps_function_name,$ps_function_params,$ps_action,$p_dbcnx,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno)
{
A100_TEMPLATE_INIT:
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - sys_clemail_a100_do_class  called from ".$ps_calledfrom;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text." ".$sys_function_name." ps_function_name = ".$ps_function_name." ps_function_params = ".$ps_function_params." ps_action = ".$ps_action." ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ");};

A199_END_TEMPLATE_INIT:
    $s_function_name = strtoupper(trim($ps_function_name," "));
    $s_function_found = "NO";
    $s_function_list = "APP FUNCTION LIST: ";
    $s_function_help = "APP FUNCTION HELP: ";
    $ar_params = array();
    $s_do_this_function = "";

    $s_action = strtoupper(trim($ps_action));
    if ($s_action == "LIST_FUNCTIONS")
    {
        goto B100_DO_FUNCTION;
    }
    if ($s_action == "FUNCTIONS_HELP")
    {
        goto B100_DO_FUNCTION;
    }
    if ($s_action == "RUN_FUNCTION")
    {
        goto B100_DO_FUNCTION;
    }
    $sys_function_out = "Error Unknown action of ".$ps_action." expecting LIST_FUNCTIONS,FUNCTIONS_HELP or RUN_FUNCTION";
    GOTO Z900_EXIT;
B100_DO_FUNCTION:
    IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text." DO FUNCTION s_action=".$s_action." "." s_function_name =".$s_function_name." ps_function_params=".$ps_function_params);};

    $ar_params = explode("^",$ps_function_params);
// ******************************************************************************************************************
B110_START:
    $s_do_this_function = "clemail_p100_create_email";
    $s_function_list = $s_function_list.$s_do_this_function.", ";
    $s_function_help = $s_function_help."clemail_p100_create_email(ps_email_address,ps_subject_line,ps_body_filename,ps_body_group,ps_debug,ps_calledfrom,ps_details_def,ps_details_data).<br>  ";

    if ($s_function_name != strtoupper(trim($s_do_this_function)))
    {
        GOTO B110_END;
    }
    if ($s_action == "RUN_FUNCTION")
    {
        $sys_function_out =  $this->clemail_p100_create_email($ar_params[0],$ar_params[1],$ar_params[2],$ar_params[3],$ar_params[4],"clemail a100",$ps_details_def,$ps_details_data);
    }
    $s_function_found = "YES";
    GOTO X900_EXIT;
B110_END:
// ******************************************************************************************************************

X900_EXIT:
    if ($s_action == "LIST_FUNCTIONS")
    {
        $sys_function_out = $s_function_list;
        GOTO Z900_EXIT;
    }
    if ($s_action == "FUNCTIONS_HELP")
    {
        $sys_function_out = $s_function_help;
        GOTO Z900_EXIT;
    }

    IF ($s_function_found == "NO")
    {
       $sys_function_out = "Error Unknown function of ".$s_function_name." expecting one of ".$s_function_list;
    }

Z900_EXIT:
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  z900_EXIT sys_function_out =  ".$sys_function_out);};
    return $sys_function_out;

}
*/

//* sys_email|v1|return def|
//clemail_p100_create_email(emailaddress,subject ,body map filename, body map group ,  dodug,calledfrom)| do debug|END

   function sys_clemail_p100_create_email($ps_email_address,$ps_subject_line,$ps_body_filename,$ps_body_group,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_attchment_list)
    {
        GLOBAL $class_main;

        $email_address=$ps_email_address;
        $email_address = STR_REPLACE("#P","|",$email_address);
        $email_address = STR_REPLACE("#p","|",$email_address);
        $email_address = $class_main->clmain_v200_load_line($email_address,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"clemail p100 em ");

        $subject = $ps_subject_line;
        $subject = STR_REPLACE("#P","|",$subject);
        $subject = STR_REPLACE("#p","|",$subject);
        $subject = $class_main->clmain_v200_load_line($subject,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"clemail p100 sub ");

        $body = "";
        if (strtoupper(trim($ps_body_group)) == "NOMAP")
        {
            $body = $ps_body_filename;
            GOTO B100_DO_ATTACHMENTS;
        }

        $body = $body.$class_main->clmain_v100_load_html_screen($ps_body_filename,$ps_details_def,$ps_details_data,"no",$ps_body_group);

        if (strtoupper(trim($ps_body_group)) == "SMS")
        {
            $body = $ps_body_filename;
        }
        if (strtoupper(trim($ps_body_group)) == "NOMAP")
        {
            $body = $ps_body_filename;
        }
B100_DO_ATTACHMENTS:
        $attachment_name='';

        $email_origin='email_bot';
        $email_from='mailmanager@thedataexchange.com.au';
        $email_failure_notice='mailmanager@thedataexchange.com.au';

        if (isset($_SESSION['ko_email_origin']))
        {
            $email_origin = $_SESSION['ko_email_origin'];
        }
        if (isset($_SESSION['ko_email_from']))
        {
            $email_from = $_SESSION['ko_email_from'];
        }
        if (isset($_SESSION['ko_email_fail_to_address']))
        {
            $email_failure_notice = $_SESSION['ko_email_fail_to_address'];
        }
        if (strtoupper(trim($ps_body_group)) == "SMS")
        {
//            $email_from = $_SESSION['ko_sms_from'];
            $email_from = "sms_sender@ncss.net.au";
            $attachment_name='SMS_NOATTACHMENTS';
        }
// gw20101002 - $ps_attchment_list is not tested fully
        $emailtransport_co=$this->main_create_job_email($email_address,$subject,$body,$attachment_name,$email_origin,$email_from,$email_failure_notice,$ps_attchment_list);

        $email_message = 'Has been emailed to: '.$email_address;

        return $email_message;
    }

function main_create_job_email($email_address,$email_subject,$email_body,$email_attachment_file,$email_origin,$email_from,$email_failure_notice,$ps_attchment_list)
    {
        GLOBAL $class_main;

        // pkn 20081021 - generate a random number to ensure that the email file is always unique
        $random_number=rand(1, 10000);

        $date_time_directory = date("Ymd").date("His").$random_number;

B100_DO_ATTACHMENTS:
// gw 20120218 gw testing the ps_attchmentlist will be full file path
//gw20120529 - attachments are ; seperated
    IF ($email_attachment_file=='SMS_NOATTACHMENTS'){
        $email_attachment_file_name = "";
        $email_attachment_file_name2 = "";
        goto B900_END;
    }

    $email_attachment_file_name2  = $ps_attchment_list;
    goto B900_END;

        $email_attachment_file_path = "../email/attachfiles/".$date_time_directory."/";
        $class_main->clmain_u100_mkdirs($email_attachment_file_path);
        $email_attachment_file_name = "";
        $email_attachment_file_name2 = "";
        if (trim($ps_attchment_list) == "")
        {
            goto B900_END;
        }
// gw20101002 - not tested
// explode around comma        $ps_attchment_list
// for each entry add an attachment link
        $email_attachment_file_name=$email_attachment_file_path.$filename;
        $email_attachment_file_name2="../attachfiles/$date_time_directory/$filename";

        //echo 'email_attachment_file_name2='.$email_attachment_file_name2.'<br>';
B900_END:
        $email_address_cnt = substr_count($email_address,";");
        $email_address_cnt ++;
        $email_sent_cnt = 0;
        $email_address_sent = $email_address;
        if ($email_address_cnt > 1)
        {
            $job_email_address_array = explode(";", $email_address);
            while ($email_address_cnt > $email_sent_cnt)
            {
                $job_email_address = $job_email_address_array[$email_sent_cnt];
                $this->main_create_job_email_file($email_origin,$job_email_address,$email_from,$email_subject,$email_body,$email_attachment_file_name2,$email_failure_notice);
                $email_sent_cnt++;
            }
        }
        else
        {
            $job_email_address = $email_address;
            $this->main_create_job_email_file($email_origin,$job_email_address,$email_from,$email_subject,$email_body,$email_attachment_file_name2,$email_failure_notice);
        }
//gw 20101002 - attachment process not tested
// not sure why this is here        //rename("{$email_attachment_file}","{$email_attachment_file_name}");
//        copy("{$email_attachment_file}","{$email_attachment_file_name}");

        return true;
    }

 function main_create_job_email_file($email_origin,$job_email_address,$email_from,$email_subject,$email_body,$email_attachment_file_name2,$email_failure_notice)
    {
        // pkn 20081021 - generate a random number to ensure that the email file is always unique
        $random_number=rand(1, 10000);

        $s_email_path = "";
        if (isset($_SESSION['ko_email_path']))
        {
            $s_email_path = $_SESSION['ko_email_path'];
        }

        $email_load_directory=$s_email_path.'email/load/';
        $email_filename=date("Ymhd").date("His").$random_number.".txt";

//gw20101002        echo "gwtest class_email main_create_job  email_load_directory=".$email_load_directory." email_filename= ".$email_filename;

        $email_job_file = $email_load_directory.$email_filename;
        //echo 'email_job_file='.$email_job_file.'<br>';
        @ $fp = fopen($email_job_file,"w+");
        flock($fp,2); //lock the file for writing

        $origin_line='<origin>'.$email_origin.'</origin>'."\r\n";
        $output_line=$origin_line;
        fwrite($fp,$output_line);
        $output_line = "";

        $emailto_line='<emailto>'.$job_email_address.'</emailto>'."\r\n";
        $output_line=$emailto_line;
        fwrite($fp,$output_line);
        $output_line = "";

        $emailfrom_line='<emailfrom>'.$email_from.'</emailfrom>'."\r\n";
        $output_line=$emailfrom_line;
        fwrite($fp,$output_line);
        $output_line = "";

        $subject_line='<subject>'.$email_subject.'</subject>'."\r\n";
        $output_line=$subject_line;
        fwrite($fp,$output_line);
        $output_line = "";

        $message_line='<message>'."\r\n".$email_body."\r\n".'</message>'."\r\n";
        $output_line=$message_line;
        fwrite($fp,$output_line);
        $output_line = "";

C100_DO_ATTACHMENTS:
        if (trim($email_attachment_file_name2) == "")
        {
            GOTO C900_END_ATTACHMENTS;
        }

        $attachment_total_cnt = substr_count($email_attachment_file_name2,";");
        $attachment_total_cnt ++;
        $attachment_done_cnt = 0;
        if ($attachment_total_cnt > 1)
        {
            $attachment_name_array = explode(";", $email_attachment_file_name2);
            while ($attachment_total_cnt > $attachment_done_cnt)
            {
                $attachment_name = $attachment_name_array[$attachment_done_cnt];

                $attachfile_line='<attachfile>'."\r\n";
                $output_line=$attachfile_line;
                fwrite($fp,$output_line);
                $attachfile_line=$attachment_name."\r\n";
                $output_line=$attachfile_line;
                fwrite($fp,$output_line);
                $attachfile_line='</attachfile>'."\r\n";
                $output_line=$attachfile_line;
                fwrite($fp,$output_line);
                $output_line = "";
                $attachment_done_cnt++;
            }
        }
        else
        {
            $attachfile_line='<attachfile>'."\r\n";
            $output_line=$attachfile_line;
            fwrite($fp,$output_line);
            $attachfile_line=$email_attachment_file_name2."\r\n";
            $output_line=$attachfile_line;
            fwrite($fp,$output_line);
            $attachfile_line='</attachfile>'."\r\n";
            $output_line=$attachfile_line;
            fwrite($fp,$output_line);
            $output_line = "";
        }
//gw20120529 - replaced by loop
/*
        if (trim($email_attachment_file_name2) <> "")
        {
            $attachfile_line='<attachfile>'."\r\n";
            $output_line=$attachfile_line;
            fwrite($fp,$output_line);
            $attachfile_line=$email_attachment_file_name2."\r\n";
            $output_line=$attachfile_line;
            fwrite($fp,$output_line);
            $attachfile_line='</attachfile>'."\r\n";
            $output_line=$attachfile_line;
        }
*/

C900_END_ATTACHMENTS:

        $failure_notice_line='<failure_notice>'.$email_failure_notice.'</failure_notice>'."\r\n";
        $output_line=$failure_notice_line;
        fwrite($fp,$output_line);
        $output_line = "";

        flock($fp,3); //release the write lock
        fclose($fp);
    }

    //endofclass
    }
    ?>
