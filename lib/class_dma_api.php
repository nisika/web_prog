<?php

    class cl_dma_api
    {

//##########################################################################################################################################################################
function cl_dmaapi_100_carrier_csemail($ps_request_xml,$ps_dbcnx,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    A100_TEMPLATE_INIT:

    global $class_main;


    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NODEBUG") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "cl_dmaapi_carrier_csemail  called from ".$ps_calledfrom;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){echo( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." params = ps_debug,ps_details_def,ps_details_data,ps_sessionno");};
    IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." params = ".$ps_debug.",".$ps_details_def.",".$ps_details_data.",".$ps_sessionno);};

    A199_END_TEMPLATE_INIT:
//    $sys_function_out = "xml supplied = ".$ps_request_xml;

    $s_taskrequestid = "NONEFOUND";

    $s_subject = "Client Request - update of consignment status";
    //$s_body_map = "Please provide an update of the following consignment number/s";
    $s_body_map = "Can you please lodge an enquiry for the following consignment number/s"."<br>";
    $s_body_map_group = "NOMAP";
    $s_email_address = "service@thedataexchange.com.au";

    if (isset($_SESSION['ko_cs_email_to']))
    {
        $s_email_address = $_SESSION['ko_cs_email_to'];
    }

    $s_outfilenameonly="NOFILENAME";
    $s_outfilepath="NOFILEPATH/";
    $s_full_outfilename=$s_outfilepath.$s_outfilenameonly;

    $ar_cndetails = "";
    $s_transport_company_list = "";
    $f_start_utime = microtime(true);
    $s_response_status = "UNKNOWN";
    $s_response_message = "The api has not responded.";
    $i_emails_sent = 0;
    $f_end_utime = 0;
    $f_process_time = 0;
    $s_tco_count = "0";
    $s_cnuk_count = "0";
    $s_repsonse_note = "";

    B100_PROCESS:
    //break down the xml

    $xml_data = $ps_request_xml;
    $xml_data=str_replace("'","",$xml_data);
    $xml_data=str_replace("&","",$xml_data);
    $xml_data=str_replace(", "," ",$xml_data);

    if($xml_data===false)
    {
        $tmp = $class_main->clmain_u540_utl_activity_log($ps_dbcnx,"dmaapi_carrier_csemail","SYSMON","DMAAPI","carrier_csemail","Error - unable to process the xml_data - xml_data is []".$xml_data."[]",$s_taskrequestid,"TASKREQUESTID","k2 value","k2 def","k3 value","k3 def","k4 value","k4 def","k5 value","k5 def");
        $sys_function_out = "Error cl_dmaapi_100_x1472 - xml-data is false";
        $s_response_status = "ERROR";
        $s_response_message = "Ref:dmaapi100_1000 - Unable to process the xml data.";
        GOTO Z900_EXIT;
    }

    $xmlObj = simplexml_load_string($ps_request_xml);
    $arrXml = $class_main->clmain_u599_xmlIntoArray($xmlObj);
    $xml_arr=$class_main->clmain_u598_xmlarray_keys_toupper($arrXml);

    $s_tasktype       =trim($xml_arr['TASKDETAILS']['TASKTYPE']);
    $s_mailto         =trim($xml_arr['TASKDETAILS']['TASKEMAILTO']);
    $s_requestid      =trim($xml_arr['TASKDETAILS']['TASKREQUEST_ID']);
    $s_requesttime    =trim($xml_arr['TASKDETAILS']['TASKREQUEST_TIMELOCAL']);
    $s_requesttimegmt =trim($xml_arr['TASKDETAILS']['TASKREQUEST_TIMEGMT']);
    $s_token          =trim($xml_arr['TASKDETAILS']['TASKTOKEN']);
    $s_client_name    =trim($xml_arr['TASKDETAILS']['TASKCLIENT_NAME']);
    $s_site_id        =trim($xml_arr['TASKDETAILS']['TASKSITE_ID']);
    $s_site_software  =trim($xml_arr['TASKDETAILS']['TASKSITE_SOFTWARE']);
    $s_site_version   =trim($xml_arr['TASKDETAILS']['TASKSITE_VERSION']);

    IF (isset($xml_arr['CNUNIQUEKEYS']['CNUK'])===false)
    {
//        echo "<br> NO CONNOTE CNUK PROVIDED <br> ";
        $tmp = $class_main->clmain_u540_utl_activity_log($ps_dbcnx,"dmaapi_carrier_csemail","SYSMON","DMAAPI","carrier_csemail","Error - no cnuk tags provided  - xml_data is []".$xml_data."[]",$s_taskrequestid,"TASKREQUESTID","k2 value","k2 def","k3 value","k3 def","k4 value","k4 def","k5 value","k5 def");
        $s_response_status = "ERROR";
        $s_response_message = "Ref:dmaapi100_2000 - no Connote keys provided.";
        GOTO Z900_EXIT;
    }

    $s_cnuk_count = count($xml_arr['CNUNIQUEKEYS']['CNUK']);
    if (is_array($xml_arr['CNUNIQUEKEYS']['CNUK']))
    {
//        echo "<br> there is a list of cnuks = ".$s_cnuk_count."in total<br> ";
        goto B300_DO_MULTI_LEVEL;
    }
//    echo "<br> only 1 cnuk  <br>";

    B200_DO_SINGLE:
    $s_cnuk = "<BR>".$s_body_map.trim($xml_arr['CNUNIQUEKEYS']['CNUK']);

    $s_cnuk = trim($xml_arr['CNUNIQUEKEYS']['CNUK']);

    $rs_temp = $this->cl_dmaapi_z100_getcnuk_cnheader_details($s_cnuk,$ps_dbcnx,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno);
    IF ($rs_temp == "NODATA")
    {
        $s_current_tco = "NA";
        $ar_cndetails[] = "cnuk:=:".$s_cnuk.":+:cncarrier:=:".$s_current_tco.":+:cnno:=:NA";
    }else{
        $s_current_tco = $rs_temp["transport_company"];
        $ar_cndetails[] = "cnuk:=:".$s_cnuk.":+:cncarrier:=:".$s_current_tco.":+:cnno:=:".$rs_temp["Key1"];
    }

    if (strpos($s_transport_company_list,$s_current_tco.":+:") === false)
    {
        $s_transport_company_list = $s_transport_company_list.$s_current_tco.":+:";
    }


    GOTO B390_END;

    B300_DO_MULTI_LEVEL:
    $s_count = 0;
    B310_DO_ENTRY:
    IF ($s_count >= $s_cnuk_count)
    {
        GOTO B390_END;
    }
    $s_cnuk = trim($xml_arr['CNUNIQUEKEYS']['CNUK'][$s_count]);

    $rs_temp = $this->cl_dmaapi_z100_getcnuk_cnheader_details($s_cnuk,$ps_dbcnx,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno);
    IF ($rs_temp == "NODATA")
    {
        $s_current_tco = "NA";
        $ar_cndetails[] = "cnuk:=:".$s_cnuk.":+:cncarrier:=:".$s_current_tco.":+:cnno:=:NA";
    }else{
        $s_current_tco = $rs_temp["transport_company"];
        $ar_cndetails[] = "cnuk:=:".$s_cnuk.":+:cncarrier:=:".$s_current_tco.":+:cnno:=:".$rs_temp["Key1"];
    }

    if (strpos($s_transport_company_list,$s_current_tco.":+:") === false)
    {
        $s_transport_company_list = $s_transport_company_list.$s_current_tco.":+:";
    }
    $s_count = $s_count +1;
    GOTO B310_DO_ENTRY;

    B390_END:
    IF ($sys_debug == "YES"){    echo "<br> s_transport_company_list=[]".$s_transport_company_list."[]<br> ";};
    B900_END:

    C100_DO_TCO:
    $ar_tcos = explode(":+:",$s_transport_company_list);
    $s_tco_count = count($ar_tcos) - 1;
    $s_temp_count = 0;
    $s_cnuk_count = count($ar_cndetails);
    C200_DO_TCO:
    if ($s_temp_count >= $s_tco_count)
    {
        goto C900_END;
    }
    C300_DO_ALL_CNS_FOR_TCO:
    $s_current_tco = strtoupper((trim($ar_tcos[$s_temp_count])));
    $s_count = 0;
    // GET THE CARRIER EMAIL ADDRESS
    // load carrier specific body header

//    $s_repsonse_note = $s_repsonse_note."current tco:".$s_current_tco.' client []'.$s_client_name." siteid=[]".$s_site_id."[]email=[]".$s_email_address."[]";

    // get email from utl file


    IF ($sys_debug == "YES"){    echo"<br>start carrier :".$s_current_tco;};

    C310_DO_ENTRY:
    IF ($s_count >= $s_cnuk_count)
    {
        GOTO C390_END;
    }
    $ar_cn_fields = explode(":+:",$ar_cndetails[$s_count]);

    $s_cn_cnuk = substr($ar_cn_fields[0],strpos($ar_cn_fields[0],":=:") + 3);
    $s_cn_tco = substr($ar_cn_fields[1],strpos($ar_cn_fields[1],":=:") + 3);
    $s_cn_cnno = substr($ar_cn_fields[2],strpos($ar_cn_fields[2],":=:") + 3);
    IF ($sys_debug == "YES"){    echo"<br>ar_cndetails[".$s_count."]  :".$ar_cndetails[$s_count]."[]  s_cn_cnuk[]".$s_cn_cnuk."[]";};

    IF(strtoupper(trim($s_cn_tco <> $s_current_tco)))
    {
        GOTO C380_DO_NEXT;
    }
    $s_body_map = $s_body_map."<br>".trim($s_cn_cnno);

// load carrier specific cn-line map

// update status of connote to "Queried with Carrier"
// BUILD SQL
    $s_details_data_out = "QWC|Queried with Carrier (DW notify carrier set)|".date("Ymd",time())."|".date("His",time())."|END";

//    $ssql = "UPDATE dmcntrk  set key3 = '".$s_cn_to_status."',Details_def = '".$s_details_def_out."', Details_data = '".$s_details_data_out."' where (Key2 = '".$s_cnuk."' and trackgroup = 'currstat' )";
    $ssql = "UPDATE dmcntrk  set key3 = 'Queried with Carrier', Details_data = '".$s_details_data_out."' where (Key2 = '".$s_cn_cnuk."' and trackgroup = 'currstat' )";

    if (trim(strtoupper($sys_debug)) == "YES")
    {echo "<br> debug:::::arecord_sql sql=".$ssql."<br>";
        echo "dbcnx=[".$ps_dbcnx."]END OF DBCNX DEBUG<br>";
    }
    $rs_temp = mysqli_query($ps_dbcnx,$ssql);
//    $rs_temp = "101";
    if (!$rs_temp)
    {
        $tmp = $class_main->clmain_u540_utl_activity_log($ps_dbcnx,"dma_api_montor","SYSMON","DMAAPI","cl_dmaapi_400_updatecnstatus","Error - unable to process the xml_data - xml_data is []".$xml_data."[]",$s_taskrequestid,"TASKREQUESTID","k2 value","k2 def","k3 value","k3 def","k4 value","k4 def","k5 value","k5 def");
        $s_response_status = "ERROR";
        $s_response_message = "Ref:dmaapi100_3000 - Update of the consignment details failed.";
        goto Z900_EXIT;
    }

    C380_DO_NEXT:
    $s_count = $s_count +1;
    GOTO C310_DO_ENTRY;
    C390_END:
    // load carrier specific body footer
    $s_body_map = $s_body_map."<br><br>Requestid:".trim($s_requestid);

    C500_DO_EMAIL:
    IF ($sys_debug == "YES"){    echo"<br>email carrier :".$s_current_tco;};

    IF ($s_outfilenameonly="NOFILENAME")
    {
        GOTO C550_SKIP_ATTACHMENTS;
    };

    $attachment_total_cnt = substr_count($s_outfilenameonly,";");
    $attachment_total_cnt ++;
    $attachment_done_cnt = 0;
    if ($attachment_total_cnt > 1)
    {
        $attachment_name_array = explode(";", $s_outfilenameonly);
        $s_full_outfilename = "";
        while ($attachment_total_cnt > $attachment_done_cnt)
        {
            IF ($sys_debug=="YES"){
                echo "<tr><td  bgcolor='#8080FF'>&nbsp;</td><td>&nbsp;</td><td colspan='10' bgcolor='#FF8080'>".date("Ymd")." at ".date('H:m:s')." EMAIL ATTACHMENT LOOP .attachment_total_cnt=[]".$attachment_total_cnt."[]  attachment_done_cnt=[]".$attachment_done_cnt."[]  s_full_outfilename=[]".$s_full_outfilename."[] </td></tr>";
            }
            $attachment_name = $attachment_name_array[$attachment_done_cnt];
            if (trim($s_full_outfilename) <> "")
            {
                $s_full_outfilename=$s_full_outfilename.";";
            };
            $s_full_outfilename=$s_full_outfilename.$s_outfilepath.$attachment_name;
            $attachment_done_cnt++;
        }
    }
    C550_SKIP_ATTACHMENTS:

    // pkn 20151217 - add process to get email address for transport company

    $s_email_address=$this->cl_dmaapi_z100_get_tco_email($s_current_tco,$s_email_address);

    $s_line_in ="sys_email|v1|sys_email_message|".$s_email_address."|".$s_subject."|".$s_body_map."|".$s_body_map_group."||NO|".$s_full_outfilename."|".$s_outfilenameonly."|END";
    $s_sessionno = "";
    $s_siteparams = "";
    $s_url_siteparams = "";//"http://#p%!_server_HTTP_HOST_!%#p/als_web_prog/#p%!_SESSION_SUPER_ko_live_or_dev_!%#p/ut_menu.php?fnid=201&map=no_map.htm&p=1^mypid=#p%!_postV_mypalletid_!%#p^mode=EMAILERR";
    $s_do_email_result = $class_main->clmain_u730_do_email($ps_dbcnx,$s_line_in,"NO",$ps_details_def,$ps_details_data,$ps_sessionno,$s_siteparams);
    if ( $s_do_email_result <> "ALL OK")
    {
        $tmp = $class_main->clmain_u540_utl_activity_log($ps_dbcnx,"dmaapi_carrier_csemail","SYSMON","DMAAPI","carrier_csemail","Error - sending carrier csemail - u730 says []".$s_do_email_result."[]",$s_requestid,"TASKREQUESTID","k2 value","k2 def","k3 value","k3 def","k4 value","k4 def","k5 value","k5 def");
        goto C590_END;
    }
    $i_emails_sent = $i_emails_sent + 1;
    C590_END:
    IF ($sys_debug == "YES"){    echo"<br>end carrier :".$s_current_tco;};

    $s_temp_count = $s_temp_count + 1;

    $s_subject = "Client Request - update of consignment status";
    $s_body_map = "Please provide an update of the following consignment number/s";
    $s_body_map_group = "NOMAP";
    $s_full_outfilename='';
    $s_outfilenameonly='';

    GOTO C200_DO_TCO;

    C900_END:

    Z800_END:
    $s_response_status = "PROCESSOK";
    $s_response_message = "The carrier email/s have been sent.".$i_emails_sent." Emails sent, ".$s_tco_count." carriers identified, ".$s_cnuk_count." connotes in xml";
    $s_response_message = $s_response_message.$s_repsonse_note;

    Z900_EXIT:

    $f_end_utime = microtime(true);
    $f_process_time = $f_end_utime - $f_start_utime;

    $tmp = $class_main->clmain_u540_utl_activity_log($ps_dbcnx,"dmaapi_carrier_csemail","SYSMON","DMAAPI","carrier_csemail","End of process: ".$i_emails_sent." Emails sent, ".$s_tco_count." carriers identified, ".$s_cnuk_count." connotes in xml",$s_requestid,"TASKREQUESTID",$f_process_time,"processingtime",$s_cnuk_count,"CnCount",$s_tco_count,"TCOCount","k5 value","k5 def");


    $sys_function_out = $sys_function_out."<querytaskreply>";

    $sys_function_out = $sys_function_out."<taskdetails>";
    $sys_function_out = $sys_function_out."<TASKTYPE>".$s_tasktype."</TASKTYPE>";
    $sys_function_out = $sys_function_out."<TASKEMAILTO>".$s_mailto."</TASKEMAILTO>";
    $sys_function_out = $sys_function_out."<TASKREQUEST_ID>".$s_requestid."</TASKREQUEST_ID>";
    $sys_function_out = $sys_function_out."<TASKREQUEST_TIMELOCAL>".$s_requesttime."</TASKREQUEST_TIMELOCAL>";
    $sys_function_out = $sys_function_out."<TASKREQUEST_TIMEGMT>".$s_requesttimegmt."</TASKREQUEST_TIMEGMT>";
    $sys_function_out = $sys_function_out."<TASKTOKEN>".$s_token."</TASKTOKEN>";
    $sys_function_out = $sys_function_out."<TASKCLIENT_NAME>".$s_client_name."</TASKCLIENT_NAME>";
    $sys_function_out = $sys_function_out."<TASKSITE_ID>".$s_site_id."</TASKSITE_ID>";
    $sys_function_out = $sys_function_out."<TASKSITE_SOFTWARE>".$s_site_software."</TASKSITE_SOFTWARE>";
    $sys_function_out = $sys_function_out."<TASKSITE_VERSION>".$s_site_version."</TASKSITE_VERSION>";
    $sys_function_out = $sys_function_out."</taskdetails>";

    $sys_function_out = $sys_function_out."<responsedetails>";
    $sys_function_out = $sys_function_out."<RESPONSEPROCESSSTART>".$f_start_utime."</RESPONSEPROCESSSTART>";
    $sys_function_out = $sys_function_out."<RESPONSEPROCESSEND>".$f_end_utime."</RESPONSEPROCESSEND>";
    $sys_function_out = $sys_function_out."<RESPONSEPROCESSDURATION>".$f_process_time."</RESPONSEPROCESSDURATION>";
    $sys_function_out = $sys_function_out."<RESPONSESTATUS>".$s_response_status."</RESPONSESTATUS>";
    $sys_function_out = $sys_function_out."<RESPONSEMESSAGE>".$s_response_message."</RESPONSEMESSAGE>";
    $sys_function_out = $sys_function_out."<RESPONSEEMAILCOUNT>".$i_emails_sent."</RESPONSEEMAILCOUNT>";
    $sys_function_out = $sys_function_out."<RESPONSECARRIERCOUNT>".$s_tco_count."</RESPONSECARRIERCOUNT>";
    $sys_function_out = $sys_function_out."<RESPONSECONNOTECOUNT>".$s_cnuk_count."</RESPONSECONNOTECOUNT>";
    $sys_function_out = $sys_function_out."</responsedetails>";

    $sys_function_out = $sys_function_out."</querytaskreply>";
    IF ($sys_debug == "YES"){echo( $sys_debug_text."  z900_EXIT sys_function_out =  ".$sys_function_out);};

    return $sys_function_out;

}
//##########################################################################################################################################################################
function cl_dmaapi_300_cn_csv($ps_request_xml,$ps_dbcnx,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno)
{
A100_TEMPLATE_INIT:

    global $class_main;


    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NODEBUG") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "cl_dmaapi_300_cn_csv  called from ".$ps_calledfrom;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){echo( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." params = ps_debug,ps_details_def,ps_details_data,ps_sessionno");};
    IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." params = ".$ps_debug.",".$ps_details_def.",".$ps_details_data.",".$ps_sessionno);};

A199_END_TEMPLATE_INIT:
//    $sys_function_out = "xml supplied = ".$ps_request_xml;

    $s_taskrequestid = "NONEFOUND";

    $ar_cndetails = "";
    $f_start_utime = microtime(true);
    $s_response_status = "UNKNOWN";
    $s_response_message = "The api has not responded.";
    $f_end_utime = 0;
    $f_process_time = 0;
    $s_cnuk_count = "0";
    $s_do_map_file = "NO";
    $s_line = "";
    $s_filename = "none";
    $s_filepath = "";
    $s_fileurl = "none";
    $s_CNDaySummary = "NO";

B100_PROCESS:
    //break down the xml

    $xml_data = $ps_request_xml;
    $xml_data=str_replace("'","",$xml_data);
    $xml_data=str_replace("&","",$xml_data);
    $xml_data=str_replace(", "," ",$xml_data);

    if($xml_data===false)
    {
        $tmp = $class_main->clmain_u540_utl_activity_log($ps_dbcnx,"dma_api_montor","SYSMON","DMAAPI","carrier_cn_csv","Error - unable to process the xml_data - xml_data is []".$xml_data."[]",$s_taskrequestid,"TASKREQUESTID","k2 value","k2 def","k3 value","k3 def","k4 value","k4 def","k5 value","k5 def");
        $sys_function_out = "Error dmaapi300_1000 - xml-data is false";
        $s_response_status = "ERROR";
        $s_response_message = "Ref:dmaapi300_1000 - Unable to process the xml data.";
        GOTO Z900_EXIT;
    }

    $xmlObj = simplexml_load_string($ps_request_xml);
    $arrXml = $class_main->clmain_u599_xmlIntoArray($xmlObj);
    $xml_arr=$class_main->clmain_u598_xmlarray_keys_toupper($arrXml);

    $s_tasktype       =trim($xml_arr['TASKDETAILS']['TASKTYPE']);
    $s_mailto         =trim($xml_arr['TASKDETAILS']['TASKEMAILTO']);
    $s_requestid      =trim($xml_arr['TASKDETAILS']['TASKREQUEST_ID']);
    $s_requesttime    =trim($xml_arr['TASKDETAILS']['TASKREQUEST_TIMELOCAL']);
    $s_requesttimegmt =trim($xml_arr['TASKDETAILS']['TASKREQUEST_TIMEGMT']);
    $s_token          =trim($xml_arr['TASKDETAILS']['TASKTOKEN']);
    $s_client_name    =trim($xml_arr['TASKDETAILS']['TASKCLIENT_NAME']);
    $s_site_id        =trim($xml_arr['TASKDETAILS']['TASKSITE_ID']);
    $s_site_software  =trim($xml_arr['TASKDETAILS']['TASKSITE_SOFTWARE']);
    $s_site_version   =trim($xml_arr['TASKDETAILS']['TASKSITE_VERSION']);

    IF (isset($xml_arr['CNUNIQUEKEYS']['CNUK'])===false)
    {
        $tmp = $class_main->clmain_u540_utl_activity_log($ps_dbcnx,"dma_api_montor","SYSMON","DMAAPI","carrier_cn_csv","Error - no connote cnuk fields supplied  - xml_data is []".$xml_data."[]",$s_taskrequestid,"TASKREQUESTID","k2 value","k2 def","k3 value","k3 def","k4 value","k4 def","k5 value","k5 def");
        $s_response_status = "ERROR";
        $s_response_message = "Ref:dmaapi300_2000 - No connote unique ( cnuk ) fields supplied.";
        GOTO Z900_EXIT;
    }

    $s_cnuk_count = count($xml_arr['CNUNIQUEKEYS']['CNUK']);
    if (is_array($xml_arr['CNUNIQUEKEYS']['CNUK']))
    {
//        echo "<br> there is a list of cnuks = ".$s_cnuk_count."in total<br> ";
        goto B300_DO_MULTI_LEVEL;
    }
//    echo "<br> only 1 cnuk s_cnuk_count".$s_cnuk_count." <br>";

B200_DO_SINGLE:
//    $s_cnuk = "<BR>".$s_body_map.trim($xml_arr['CNUNIQUEKEYS']['CNUK']);

/*    $rs_temp = $this->cl_dmaapi_z100_getcnuk_cnheader_details($s_cnuk,$ps_dbcnx,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno);
    IF ($rs_temp == "NODATA")
    {
        $s_current_tco = "NA";
        $ar_cndetails[] = "cnuk:=:".$s_cnuk.":+:cncarrier:=:".$s_current_tco.":+:cnno:=:NA";
    }else{
        $s_current_tco = $rs_temp["transport_company"];
        $ar_cndetails[] = "cnuk:=:".$s_cnuk.":+:cncarrier:=:".$s_current_tco.":+:cnno:=:".$rs_temp["Key1"];
    }
*/
    GOTO B390_END;

B300_DO_MULTI_LEVEL:
//    $s_count = 0;
B310_DO_ENTRY:
/*    IF ($s_count >= $s_cnuk_count)
    {
        GOTO B390_END;
    }
    $s_cnuk = trim($xml_arr['CNUNIQUEKEYS']['CNUK'][$s_count]);

    $rs_temp = $this->cl_dmaapi_z100_getcnuk_cnheader_details($s_cnuk,$ps_dbcnx,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno);
    IF ($rs_temp == "NODATA")
    {
        $s_current_tco = "NA";
        $ar_cndetails[] = "cnuk:=:".$s_cnuk.":+:cncarrier:=:".$s_current_tco.":+:cnno:=:NA";
    }else{
        $s_current_tco = $rs_temp["transport_company"];
        $ar_cndetails[] = "cnuk:=:".$s_cnuk.":+:cncarrier:=:".$s_current_tco.":+:cnno:=:".$rs_temp["Key1"];
    }

    $s_count = $s_count +1;
    GOTO B310_DO_ENTRY;
*/
B390_END:

B900_END:
//  gw20150202 - going to need to work out how to handle multiple carriers
// the multiple loop above would be used to create a list of carriers
// everything from c100 to c900 would need to loop for each carrier



C100_CREATE_FILE:
    $s_filename = "dmaapi300_cncsv_".time().".csv";
    $s_filepath = "";
    if (isset($_SESSION['ko_output_file_url']))
    {
    }else{
        $_SESSION['ko_output_file_url'] = $_SESSION['ko_udp_image_home_url'];
    }
    $s_fileurl = $_SESSION['ko_output_file_url'];
    $s_fileurl = $class_main->clmain_u870_set_path($s_fileurl,$ps_dbcnx,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno);
    $s_fileurl = $s_fileurl.$s_filename;

    $s_filepath  = $class_main->clmain_u870_set_path($s_filepath,$ps_dbcnx,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno);
    $s_filename = $s_filepath.$s_filename;

    $fh = fopen($s_filename,'a') or die("cant open file".$s_filename);

C300_DO_ALL_CNS:
    $s_count = 0;

C310_DO_ENTRY:
    $s_line = "";
//    echo "cl dma api c310 count=[]".$s_count."[]  cnuk coount=[]".$s_cnuk_count."[]";
    IF ($s_count >= $s_cnuk_count)
    {
        GOTO C390_END;
    }

    if($s_cnuk_count == 1)
    {
        $s_cnuk = trim($xml_arr['CNUNIQUEKEYS']['CNUK']);
    }else{
        $s_cnuk = trim($xml_arr['CNUNIQUEKEYS']['CNUK'][$s_count]);
    }

    $rs_temp = $this->cl_dmaapi_z100_getcnuk_cnheader_details($s_cnuk,$ps_dbcnx,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno);
    IF ($rs_temp == "NODATA")
    {
        $s_current_tco = "NA";
        $s_line = trim("No Data for CN Unique key ".$s_cnuk." (ERR_Ref:CLDMA_API:300_001");
        GOTO C370_WRITE;
    }
//    $s_current_tco = $rs_temp["transport_company"];
//    $ar_cndetails[] = "cnuk:=:".$s_cnuk.":+:cncarrier:=:".$s_current_tco.":+:cnno:=:".$rs_temp["Key1"];

    $s_current_tco = $rs_temp["transport_company"];
// for multi carrier processing if ( $s_current_tco <> $s_process_tco) skip the record

// load site specific csv map

//    $ar_cn_fields = explode(":+:",$ar_cndetails[$s_count]);


    IF ($sys_debug == "YES"){    echo"<br>ar_cndetails[".$s_count."]  :".$ar_cndetails[$s_count]."[]  s_cn_cnuk[]".$s_cnuk."[]";};

    $s_details_def = "rec_count"."|".$rs_temp["Details_def"];
    $s_details_data  = ($s_count +1)."|".$rs_temp["Details_data"];

C360_DO_MAP:
    $s_filepath = "";
    if (isset($_SESSION['ko_map_path']))
    {
        $s_filepath  = $_SESSION['ko_map_path']."config\\";
        $s_filepath  = $class_main->clmain_u870_set_path($s_filepath,$ps_dbcnx,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno);
    }
    $s_filename = "dw_cn_csv.map";
    if(!file_exists($s_filepath.$s_filename))
    {
        echo "cl dma api 300 - s_filepath=[]".$s_filepath."[] s_filename=[]".$s_filename."[]";
        GOTO C369_END;
    }
    $s_do_map_file = "YES";
    if($s_count==0)
    {
        $s_line = $class_main->clmain_v100_load_html_screen($s_filepath.$s_filename,$s_details_def,$s_details_data,"no","file_heading_(SPACEONERROR)");
        if(substr($s_line,-1) =="\r")
        {
            $s_line = substr($s_line,0,strlen($s_line)-1) ;
        }
        if(substr($s_line,-1) =="\n")
        {
            $s_line = substr($s_line,0,strlen($s_line)-1) ;
        }
        if (trim($s_line) <> "")
        {
            fwrite($fh,$s_line."\r\n");
        }
    }
    $s_line = $class_main->clmain_v100_load_html_screen($s_filepath.$s_filename,$s_details_def,$s_details_data,"no","cn_header_(SPACEONERROR)");
// strip the cr or lf from the end of the returned string
    if(substr($s_line,-1) =="\r")
    {
        $s_line = substr($s_line,0,strlen($s_line)-1) ;
    }
    if(substr($s_line,-1) =="\n")
    {
        $s_line = substr($s_line,0,strlen($s_line)-1) ;
    }

    if (trim($s_line) <> "")
    {
        fwrite($fh,$s_line."\r\n");
    }
    $s_line = $class_main->clmain_v100_load_html_screen($s_filepath.$s_filename,$s_details_def,$s_details_data,"no","cn_details_(SPACEONERROR)");
// strip the cr or lf from the end of the returned string
    if(substr($s_line,-1) =="\r")
    {
        $s_line = substr($s_line,0,strlen($s_line)-1) ;
    }
    if(substr($s_line,-1) =="\n")
    {
        $s_line = substr($s_line,0,strlen($s_line)-1) ;
    }

    if (trim($s_line) <> "")
    {
        fwrite($fh,$s_line."\r\n");
    }
    $s_line = $class_main->clmain_v100_load_html_screen($s_filepath.$s_filename,$s_details_def,$s_details_data,"no","cn_footer_(SPACEONERROR)");
// strip the cr or lf from the end of the returned string
    if(substr($s_line,-1) =="\r")
    {
        $s_line = substr($s_line,0,strlen($s_line)-1) ;
    }
    if(substr($s_line,-1) =="\n")
    {
        $s_line = substr($s_line,0,strlen($s_line)-1) ;
    }
    GOTO C370_WRITE;
C369_END:
    if($s_count==0)
    {
        $s_line = "count,ConnoteNo,RecName,RecTown,RecPC,Service,TotalItems,TotalWeight,TotalPallets,TotalCubic,carrier_id_name,SenderName,SenderTown,SenderPC,details_def_dump";
        fwrite($fh,$s_line."\r\n");
    }
    $s_line = "|%!_rec_count_!%|,|%!_ConnoteNo_!%|,|%!_RecName_!%|,|%!_RecTown_!%|,|%!_RecPC_!%|,|%!_Service_!%|,|%!_TotalItems_!%|,|%!_TotalWeight_!%|,|%!_TotalPallets_!%|,|%!_TotalCubic_!%|,|%!_carrier_id_name_!%|,|%!_SenderName_!%|,|%!_SenderTown_!%|,|%!_SenderPC_!%|,|%!_details_def_dump_!%|,";
    $s_line = str_replace("#p","|",$s_line);
    $s_line = str_replace("#P","|",$s_line);
    $s_line = $class_main->clmain_v200_load_line( $s_line,$s_details_def,$s_details_data,"no",$ps_sessionno,"cl_proc_jobline  b400");
    $s_line = str_replace("<br>",",",$s_line);
    $s_line = str_replace("<BR>",",",$s_line);

C370_WRITE:
    if (trim($s_line) <> "")
    {
        fwrite($fh,$s_line."\r\n");
    }

C380_DO_NEXT:
    $s_count = $s_count +1;
    GOTO C310_DO_ENTRY;
C390_END:
    IF($s_CNDaySummary == "YES")
    {
        $s_line = $class_main->clmain_v100_load_html_screen($s_filepath.$s_filename,$s_details_def,$s_details_data,"no","file_footer_(SPACEONERROR)");
        if(substr($s_line,-1) =="\r")
        {
            $s_line = substr($s_line,0,strlen($s_line)-1) ;
        }
        if(substr($s_line,-1) =="\n")
        {
            $s_line = substr($s_line,0,strlen($s_line)-1) ;
        }
        if (trim($s_line) <> "")
        {
            fwrite($fh,$s_line."\r\n");
        }
    }ELSE{
        $s_line = "End of File";
        fwrite($fh,$s_line."\r\n");
    }

    fclose($fh);
C900_END:

Z800_END:
    $s_response_status = "PROCESSOK";
    $s_response_message = "The consignment details are available from this file ".$s_filename;

Z900_EXIT:

    $f_end_utime = microtime(true);
    $f_process_time = $f_end_utime - $f_start_utime;

    $tmp = $class_main->clmain_u540_utl_activity_log($ps_dbcnx,"cl_dmaapi_300_cn_csv","SYSMON","DMAAPI","cn_csv","End of process to create a csv file : ".$s_filename." created ",$s_requestid,"TASKREQUESTID",$f_process_time,"processingtime",$s_cnuk_count,"CnCount","k4 value","k4 def","k5 value","k5 def");


    $sys_function_out = $sys_function_out."<querytaskreply>";

    $sys_function_out = $sys_function_out."<taskdetails>";
    $sys_function_out = $sys_function_out."<TASKTYPE>".$s_tasktype."</TASKTYPE>";
    $sys_function_out = $sys_function_out."<TASKEMAILTO>".$s_mailto."</TASKEMAILTO>";
    $sys_function_out = $sys_function_out."<TASKREQUEST_ID>".$s_requestid."</TASKREQUEST_ID>";
    $sys_function_out = $sys_function_out."<TASKREQUEST_TIMELOCAL>".$s_requesttime."</TASKREQUEST_TIMELOCAL>";
    $sys_function_out = $sys_function_out."<TASKREQUEST_TIMEGMT>".$s_requesttimegmt."</TASKREQUEST_TIMEGMT>";
    $sys_function_out = $sys_function_out."<TASKTOKEN>".$s_token."</TASKTOKEN>";
    $sys_function_out = $sys_function_out."<TASKCLIENT_NAME>".$s_client_name."</TASKCLIENT_NAME>";
    $sys_function_out = $sys_function_out."<TASKSITE_ID>".$s_site_id."</TASKSITE_ID>";
    $sys_function_out = $sys_function_out."<TASKSITE_SOFTWARE>".$s_site_software."</TASKSITE_SOFTWARE>";
    $sys_function_out = $sys_function_out."<TASKSITE_VERSION>".$s_site_version."</TASKSITE_VERSION>";
    $sys_function_out = $sys_function_out."</taskdetails>";

    $sys_function_out = $sys_function_out."<responsedetails>";
    $sys_function_out = $sys_function_out."<RESPONSEPROCESSSTART>".$f_start_utime."</RESPONSEPROCESSSTART>";
    $sys_function_out = $sys_function_out."<RESPONSEPROCESSEND>".$f_end_utime."</RESPONSEPROCESSEND>";
    $sys_function_out = $sys_function_out."<RESPONSEPROCESSDURATION>".$f_process_time."</RESPONSEPROCESSDURATION>";
    $sys_function_out = $sys_function_out."<RESPONSESTATUS>".$s_response_status."</RESPONSESTATUS>";
    $sys_function_out = $sys_function_out."<RESPONSEMESSAGE>".$s_response_message."</RESPONSEMESSAGE>";
    $sys_function_out = $sys_function_out."<RESPONSEFILENAME>".$s_filename."</RESPONSEFILENAME>";
    $sys_function_out = $sys_function_out."<RESPONSEFILEURL>".$s_fileurl."</RESPONSEFILEURL>";
    $sys_function_out = $sys_function_out."<RESPONSECONNOTECOUNT>".$s_cnuk_count."</RESPONSECONNOTECOUNT>";
    $sys_function_out = $sys_function_out."</responsedetails>";

    $sys_function_out = $sys_function_out."</querytaskreply>";
    IF ($sys_debug == "YES"){echo( $sys_debug_text."  z900_EXIT sys_function_out =  ".$sys_function_out);};

    return $sys_function_out;

}
//##########################################################################################################################################################################
function cl_dmaapi_400_updatecnstatus($ps_request_xml,$ps_dbcnx,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno)
{
A100_TEMPLATE_INIT:

    global $class_main;


    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NODEBUG") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "cl_dmaapi_400_updatecnstatus  called from ".$ps_calledfrom;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){echo( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." params = ps_debug,ps_details_def,ps_details_data,ps_sessionno");};
    IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." params = ".$ps_debug.",".$ps_details_def.",".$ps_details_data.",".$ps_sessionno);};

A199_END_TEMPLATE_INIT:
//    $sys_function_out = "xml supplied = ".$ps_request_xml;

    $s_taskrequestid = "NONEFOUND";

    $ar_cndetails = "";
    $f_start_utime = microtime(true);
    $s_response_status = "UNKNOWN";
    $s_response_message = "The api has not responded.";
    $f_end_utime = 0;
    $f_process_time = 0;
    $s_cnuk_count = "0";
    $s_do_map_file = "NO";
    $s_line = "";
    $s_filename = "none";
    $s_filepath = "";
    $s_fileurl = "none";
    $s_details_def_out = "";
    $s_details_data_out = "";
    $s_cn_to_status_dma = "";

B100_PROCESS:
    //break down the xml

    $xml_data = $ps_request_xml;
    $xml_data=str_replace("'","",$xml_data);
    $xml_data=str_replace("&","",$xml_data);
    $xml_data=str_replace(", "," ",$xml_data);

    if($xml_data===false)
    {
        $tmp = $class_main->clmain_u540_utl_activity_log($ps_dbcnx,"dma_api_montor","SYSMON","DMAAPI","cl_dmaapi_400_updatecnstatus","Error - unable to process the xml_data - xml_data is []".$xml_data."[]",$s_taskrequestid,"TASKREQUESTID","k2 value","k2 def","k3 value","k3 def","k4 value","k4 def","k5 value","k5 def");
        $s_response_status = "ERROR";
        $s_response_message = "Ref:dmaapi400_1000 - Unable to process the xml data.";
        GOTO Z900_EXIT;
    }

    $xmlObj = simplexml_load_string($ps_request_xml);
    $arrXml = $class_main->clmain_u599_xmlIntoArray($xmlObj);
    $xml_arr=$class_main->clmain_u598_xmlarray_keys_toupper($arrXml);

    $s_tasktype       =trim($xml_arr['TASKDETAILS']['TASKTYPE']);
    $s_mailto         =trim($xml_arr['TASKDETAILS']['TASKEMAILTO']);
    $s_requestid      =trim($xml_arr['TASKDETAILS']['TASKREQUEST_ID']);
    $s_requesttime    =trim($xml_arr['TASKDETAILS']['TASKREQUEST_TIMELOCAL']);
    $s_requesttimegmt =trim($xml_arr['TASKDETAILS']['TASKREQUEST_TIMEGMT']);
    $s_token          =trim($xml_arr['TASKDETAILS']['TASKTOKEN']);
    $s_client_name    =trim($xml_arr['TASKDETAILS']['TASKCLIENT_NAME']);
    $s_site_id        =trim($xml_arr['TASKDETAILS']['TASKSITE_ID']);
    $s_site_software  =trim($xml_arr['TASKDETAILS']['TASKSITE_SOFTWARE']);
    $s_site_version   =trim($xml_arr['TASKDETAILS']['TASKSITE_VERSION']);

    IF (isset($xml_arr['CNUNIQUEKEYS']['CNUK'])===false)
    {
        $tmp = $class_main->clmain_u540_utl_activity_log($ps_dbcnx,"dma_api_montor","SYSMON","DMAAPI","cl_dmaapi_400_updatecnstatus","Error - no connote cnuk fields supplied  - xml_data is []".$xml_data."[]",$s_taskrequestid,"TASKREQUESTID","k2 value","k2 def","k3 value","k3 def","k4 value","k4 def","k5 value","k5 def");
        $s_response_status = "ERROR";
        $s_response_message = "Ref:dmaapi400_2000 - No connote unique ( cnuk ) fields supplied.";
        GOTO Z900_EXIT;
    }

    $s_cnuk_count = count($xml_arr['CNUNIQUEKEYS']['CNUK']);
    if (is_array($xml_arr['CNUNIQUEKEYS']['CNUK']))
    {
//        echo "<br> there is a list of cnuks = ".$s_cnuk_count."in total<br> ";
        goto B300_DO_MULTI_LEVEL;
    }
//    echo "<br> only 1 cnuk s_cnuk_count".$s_cnuk_count." <br>";

B200_DO_SINGLE:
    GOTO B390_END;

B300_DO_MULTI_LEVEL:
B310_DO_ENTRY:
B390_END:

B900_END:

C100_UPDATE_THE_CN:

C300_DO_ALL_CNS:
    $s_count = 0;

C310_DO_ENTRY:
    $s_line = "";
//    echo "cl dma api c310 count=[]".$s_count."[]  cnuk coount=[]".$s_cnuk_count."[]";
    IF ($s_count >= $s_cnuk_count)
    {
        GOTO C390_END;
    }

    if($s_cnuk_count == 1)
    {
        $s_cnuk = trim($xml_arr['CNUNIQUEKEYS']['CNUK']);
        $s_cn_to_status = trim($xml_arr['CNUNIQUEKEYS']['CNTOSTATUS']);
    }else{
// GW20150204 - the multi level xml has not been tested!
        $s_cnuk = trim($xml_arr['CNUNIQUEKEYS']['CNUK'][$s_count]);
        $s_cn_to_status = trim($xml_arr['CNUNIQUEKEYS']['CNTOSTATUS'][$s_count]);
    }
C320_UPDATE_STATUS:
//        $s_details_def_out = "Status Summary|Status description|Activity YYYYMMDD|Activity HHMMSS|END";
    if( trim(strtoupper($s_cn_to_status)) == "WITHCARRIER")
    {
        $s_cn_to_status_dma = "ITS";
        $s_details_data_out = "ITS|In Transit (manually set)|".date("Ymd",time())."|".date("His",time())."|END";
        GOTO C329_END;
    }
    if( trim(strtoupper($s_cn_to_status)) == "OBD")
    {
        $s_cn_to_status_dma = "OBD";
        $s_details_data_out = "OBD|On Board for Delivery (manually set)|".date("Ymd",time())."|".date("His",time())."|END";
        GOTO C329_END;
    }
    if( trim(strtoupper($s_cn_to_status)) == "DELIVERED")
    {
        $s_cn_to_status_dma = "DELCON";
        $s_details_data_out = "DELCON|Delivery Confirmed (manually set)|".date("Ymd",time())."|".date("His",time())."|END";
        GOTO C329_END;
    }
    if( trim(strtoupper($s_cn_to_status)) == "BOOKED")
    {
        $s_cn_to_status_dma = "BOOKED";
        $s_details_data_out = "BOOKED|Delivery has been booked for delivery (manually set)|".date("Ymd",time())."|".date("His",time())."|END";
        GOTO C329_END;
    }
    $tmp = $class_main->clmain_u540_utl_activity_log($ps_dbcnx,"dma_api_montor","SYSMON","DMAAPI","cl_dmaapi_400_updatecnstatus","Error - invalid cn status = []".$s_cn_to_status."[]unable to process the xml_data - xml_data is []".$xml_data."[]",$s_taskrequestid,"TASKREQUESTID","k2 value","k2 def","k3 value","k3 def","k4 value","k4 def","k5 value","k5 def");
    $s_response_status = "ERROR";
    $s_response_message = "Ref:dmaapi400_4000 - Update of the consignment details failed because the new status of []".$s_cn_to_status."[] is not valid.";
    goto Z900_EXIT;
C329_END:

// BUILD SQL

//    $ssql = "UPDATE dmcntrk  set key3 = '".$s_cn_to_status."',Details_def = '".$s_details_def_out."', Details_data = '".$s_details_data_out."' where (Key2 = '".$s_cnuk."' and trackgroup = 'currstat' )";
    //$ssql = "UPDATE dmcntrk  set key3 = '".$s_cn_to_status_dma."', Details_data = '".$s_details_data_out."' where (Key2 = '".$s_cnuk."' and trackgroup = 'currstat' )";
    // pkn 20151120 - $s_cnuk is currstat uniquekey so changed sql update statement accordingly
    $ssql = "UPDATE dmcntrk  set key3 = '".$s_cn_to_status_dma."', Details_data = '".$s_details_data_out."' where (UniqueKey = '".$s_cnuk."' and trackgroup = 'currstat' )";
    //echo 'ssql='.$ssql.'<br>';

    if (trim(strtoupper($sys_debug)) == "YES")
    {echo "<br> debug:::::arecord_sql sql=".$ssql."<br>";
        echo "dbcnx=[".$ps_dbcnx."]END OF DBCNX DEBUG<br>";
    }
    $rs_temp = mysqli_query($ps_dbcnx,$ssql);
//    $rs_temp = "101";
    if (!$rs_temp)
    {
        $tmp = $class_main->clmain_u540_utl_activity_log($ps_dbcnx,"dma_api_montor","SYSMON","DMAAPI","cl_dmaapi_400_updatecnstatus","Error - unable to process the xml_data - xml_data is []".$xml_data."[]",$s_taskrequestid,"TASKREQUESTID","k2 value","k2 def","k3 value","k3 def","k4 value","k4 def","k5 value","k5 def");
        $s_response_status = "ERROR";
        $s_response_message = "Ref:dmaapi400_3000 - Update of the consignment details failed.";
        goto Z900_EXIT;
    }

//    echo "<BR>dmaapi_400_updatedcnstatus<br> $ssql=[]".$ssql."[] ";


//   echo "<BR>dmaapi_400_updatedcnstatus count=[]".$s_count."[]  cnuk coount=[]".$s_cnuk_count."[] s_cnuk=[]".$s_cnuk."[]";
// RUN SQL
C380_DO_NEXT:
    $s_count = $s_count +1;
    GOTO C310_DO_ENTRY;
C390_END:

C900_END:

Z800_END:
    $s_response_status = "PROCESSOK";
    $s_response_message = "The consignment(s) status has been updated";

Z900_EXIT:

    $f_end_utime = microtime(true);
    $f_process_time = $f_end_utime - $f_start_utime;

    $tmp = $class_main->clmain_u540_utl_activity_log($ps_dbcnx,"dmaapi_400_updatedcnstatus","SYSMON","DMAAPI","cn_update","End of process to update status of cn",$s_requestid,"TASKREQUESTID",$f_process_time,"processingtime",$s_cnuk_count,"CnCount","k4 value","k4 def","k5 value","k5 def");


    $sys_function_out = $sys_function_out."<querytaskreply>";

    $sys_function_out = $sys_function_out."<taskdetails>";
    $sys_function_out = $sys_function_out."<TASKTYPE>".$s_tasktype."</TASKTYPE>";
    $sys_function_out = $sys_function_out."<TASKEMAILTO>".$s_mailto."</TASKEMAILTO>";
    $sys_function_out = $sys_function_out."<TASKREQUEST_ID>".$s_requestid."</TASKREQUEST_ID>";
    $sys_function_out = $sys_function_out."<TASKREQUEST_TIMELOCAL>".$s_requesttime."</TASKREQUEST_TIMELOCAL>";
    $sys_function_out = $sys_function_out."<TASKREQUEST_TIMEGMT>".$s_requesttimegmt."</TASKREQUEST_TIMEGMT>";
    $sys_function_out = $sys_function_out."<TASKTOKEN>".$s_token."</TASKTOKEN>";
    $sys_function_out = $sys_function_out."<TASKCLIENT_NAME>".$s_client_name."</TASKCLIENT_NAME>";
    $sys_function_out = $sys_function_out."<TASKSITE_ID>".$s_site_id."</TASKSITE_ID>";
    $sys_function_out = $sys_function_out."<TASKSITE_SOFTWARE>".$s_site_software."</TASKSITE_SOFTWARE>";
    $sys_function_out = $sys_function_out."<TASKSITE_VERSION>".$s_site_version."</TASKSITE_VERSION>";
    $sys_function_out = $sys_function_out."</taskdetails>";

    $sys_function_out = $sys_function_out."<responsedetails>";
    $sys_function_out = $sys_function_out."<RESPONSEPROCESSSTART>".$f_start_utime."</RESPONSEPROCESSSTART>";
    $sys_function_out = $sys_function_out."<RESPONSEPROCESSEND>".$f_end_utime."</RESPONSEPROCESSEND>";
    $sys_function_out = $sys_function_out."<RESPONSEPROCESSDURATION>".$f_process_time."</RESPONSEPROCESSDURATION>";
    $sys_function_out = $sys_function_out."<RESPONSESTATUS>".$s_response_status."</RESPONSESTATUS>";
    $sys_function_out = $sys_function_out."<RESPONSEMESSAGE>".$s_response_message."</RESPONSEMESSAGE>";
    $sys_function_out = $sys_function_out."<RESPONSEFILENAME>".$s_filename."</RESPONSEFILENAME>";
    $sys_function_out = $sys_function_out."<RESPONSEFILEURL>".$s_fileurl."</RESPONSEFILEURL>";
    $sys_function_out = $sys_function_out."<RESPONSECONNOTECOUNT>".$s_cnuk_count."</RESPONSECONNOTECOUNT>";
    $sys_function_out = $sys_function_out."</responsedetails>";

    $sys_function_out = $sys_function_out."</querytaskreply>";
    IF ($sys_debug == "YES"){echo( $sys_debug_text."  z900_EXIT sys_function_out =  ".$sys_function_out);};

//        echo "<BR><BR>".(str_replace("<","{",$sys_function_out))."<BR><BR>";


    return $sys_function_out;

}
//##########################################################################################################################################################################
function cl_dmaapi_500_createcncomment($ps_request_xml,$ps_dbcnx,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    A100_TEMPLATE_INIT:

    global $class_main;
    global $class_sql;

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NODEBUG") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "cl_dmaapi_500_createcncomment  called from ".$ps_calledfrom;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){echo( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." params = ps_debug,ps_details_def,ps_details_data,ps_sessionno");};
    IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." params = ".$ps_debug.",".$ps_details_def.",".$ps_details_data.",".$ps_sessionno);};

    A199_END_TEMPLATE_INIT:
//    $sys_function_out = "xml supplied = ".$ps_request_xml;

    $s_taskrequestid = "NONEFOUND";

    $ar_cndetails = "";
    $f_start_utime = microtime(true);
    $s_response_status = "UNKNOWN";
    $s_response_message = "The api has not responded.";
    $f_end_utime = 0;
    $f_process_time = 0;
    $s_cnuk_count = "0";
    $s_do_map_file = "NO";
    $s_line = "";
    $s_filename = "none";
    $s_filepath = "";
    $s_fileurl = "none";
    $s_details_def_out = "";
    $s_details_data_out = "";
    $s_cn_commentcs_dma = "";
    $s_userid  = "dmaapi500";
    $s_repsonse_note = "";

    B100_PROCESS:
    //break down the xml

    $xml_data = $ps_request_xml;
    $xml_data=str_replace("'","",$xml_data);
    $xml_data=str_replace("&","",$xml_data);
    $xml_data=str_replace(", "," ",$xml_data);

    if($xml_data===false)
    {
        $tmp = $class_main->clmain_u540_utl_activity_log($ps_dbcnx,"dma_api_montor","SYSMON","DMAAPI","cl_dmaapi_500_createcncomment","Error - unable to process the xml_data - xml_data is []".$xml_data."[]",$s_taskrequestid,"TASKREQUESTID","k2 value","k2 def","k3 value","k3 def","k4 value","k4 def","k5 value","k5 def");
        $s_response_status = "ERROR";
        $s_response_message = "Ref:dmaapi500_1000 - Unable to process the xml data.";
        GOTO Z900_EXIT;
    }

    $xmlObj = simplexml_load_string($ps_request_xml);
    $arrXml = $class_main->clmain_u599_xmlIntoArray($xmlObj);
    $xml_arr=$class_main->clmain_u598_xmlarray_keys_toupper($arrXml);

    $s_tasktype       =trim($xml_arr['TASKDETAILS']['TASKTYPE']);
    $s_mailto         =trim($xml_arr['TASKDETAILS']['TASKEMAILTO']);
    $s_requestid      =trim($xml_arr['TASKDETAILS']['TASKREQUEST_ID']);
    $s_requesttime    =trim($xml_arr['TASKDETAILS']['TASKREQUEST_TIMELOCAL']);
    $s_requesttimegmt =trim($xml_arr['TASKDETAILS']['TASKREQUEST_TIMEGMT']);
    $s_token          =trim($xml_arr['TASKDETAILS']['TASKTOKEN']);
    $s_client_name    =trim($xml_arr['TASKDETAILS']['TASKCLIENT_NAME']);
    $s_userid         =trim($xml_arr['TASKDETAILS']['TASKCLIENT_USERID']);
    $s_site_id        =trim($xml_arr['TASKDETAILS']['TASKSITE_ID']);
    $s_site_software  =trim($xml_arr['TASKDETAILS']['TASKSITE_SOFTWARE']);
    $s_site_version   =trim($xml_arr['TASKDETAILS']['TASKSITE_VERSION']);
    $s_site_id        =trim($xml_arr['TASKDETAILS']['TASKSITE_ID']);


    if(strlen($s_userid) > 10 )
    {
        $s_repsonse_note = $s_repsonse_note."*Note* Userid exceeds 10 characters - it has been truncated.";
        $s_userid = substr($s_userid,1,10);
    }

    IF(strtoupper($s_tasktype) <> "CNCOMMENTCS")
    {
        $s_response_status = "ERROR";
        $s_response_message = "Ref:dmaapi500_3001 - Incorrect process type cncommentcs expected.";
        goto Z900_EXIT;
    }

    IF (isset($xml_arr['CNUNIQUEKEYS']['CNUK'])===false)
    {
        $tmp = $class_main->clmain_u540_utl_activity_log($ps_dbcnx,"dma_api_montor","SYSMON","DMAAPI","cl_dmaapi_500_createcncomment","Error - no connote cnuk fields supplied  - xml_data is []".$xml_data."[]",$s_taskrequestid,"TASKREQUESTID","k2 value","k2 def","k3 value","k3 def","k4 value","k4 def","k5 value","k5 def");
        $s_response_status = "ERROR";
        $s_response_message = "Ref:dmaapi500_2000 - No connote unique ( cnuk ) fields supplied.";
        GOTO Z900_EXIT;
    }

    $s_cnuk_count = count($xml_arr['CNUNIQUEKEYS']['CNUK']);
    if (is_array($xml_arr['CNUNIQUEKEYS']['CNUK']))
    {
//        echo "<br> there is a list of cnuks = ".$s_cnuk_count."in total<br> ";
        goto B300_DO_MULTI_LEVEL;
    }
//    echo "<br> only 1 cnuk s_cnuk_count".$s_cnuk_count." <br>";

    B200_DO_SINGLE:
    GOTO B390_END;

    B300_DO_MULTI_LEVEL:
    B310_DO_ENTRY:
    B390_END:

    B900_END:

    C100_UPDATE_THE_CN:

    C300_DO_ALL_CNS:
    $s_count = 0;

    C310_DO_ENTRY:
    $s_line = "";
//    echo "cl dma api c310 count=[]".$s_count."[]  cnuk coount=[]".$s_cnuk_count."[]";
    IF ($s_count >= $s_cnuk_count)
    {
        GOTO C390_END;
    }

    if($s_cnuk_count == 1)
    {
        $s_cnuk = trim($xml_arr['CNUNIQUEKEYS']['CNUK']);
        $s_cn_commentcs = trim($xml_arr['CNUNIQUEKEYS']['CNCOMMENTCS']);
    }else{
// GW20150204 - the multi level xml has not been tested!
        $s_cnuk = trim($xml_arr['CNUNIQUEKEYS']['CNUK'][$s_count]);
        $s_cn_commentcs = trim($xml_arr['CNUNIQUEKEYS']['CNCOMMENTCS'][$s_count]);
    }
    C329_END:

    $rs_temp = $this->cl_dmaapi_z100_getcnuk_cnheader_details($s_cnuk,$ps_dbcnx,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno);
    IF ($rs_temp == "NODATA")
    {
        $s_connoteno = "NA";
    }else{
        $s_connoteno = $rs_temp["Key1"];
    }

    $s_Key1=$s_connoteno;
    $s_Key2=$s_cnuk;
    $s_Key3="";
    $s_Key4="";
    $s_Key5="";
// from the cnheader record
    $s_company_id="";
    $s_transport_company="";
    $S_UUID = uniqid("dmaapi500-329", FALSE);
    $s_current_date=date("Ymd");
    $s_current_time=date("His");

    $rs_temp = $this->cl_dmaapi_z100_getcnuk_cnheader_details($s_cnuk,$ps_dbcnx,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno);
    IF ($rs_temp == "NODATA")
    {
        $s_current_tco = "NA";
        $s_line = trim("No Data for CN Unique key ".$s_cnuk." (ERR_Ref:CLDMA_API:300_001");
        GOTO C380_DO_NEXT;
    }
    $s_transport_company = $rs_temp["transport_company"];
    $s_company_id = $rs_temp["company_id"];

// BUILD SQL

//    $ssql = "UPDATE dmcntrk  set key3 = '".$s_cn_commentcs."',Details_def = '".$s_details_def_out."', Details_data = '".$s_details_data_out."' where (Key2 = '".$s_cnuk."' and trackgroup = 'currstat' )";
    $s_fieldlist = "";
    $s_fieldvalues = "";

    $s_fieldlist = $s_fieldlist." CreatedUserId".",";
    $s_fieldlist = $s_fieldlist." UpdatedUserId".",";
    $s_fieldlist = $s_fieldlist." UniqueKey".",";
    $s_fieldlist = $s_fieldlist." CreatedDateNTime".",";
    $s_fieldlist = $s_fieldlist." UpdatedDateNTime".",";
    $s_fieldlist = $s_fieldlist." History".",";
    $s_fieldlist = $s_fieldlist." Key1".",";
    $s_fieldlist = $s_fieldlist." Key2".",";
    $s_fieldlist = $s_fieldlist." Key3".",";
    $s_fieldlist = $s_fieldlist." Key4".",";
    $s_fieldlist = $s_fieldlist." Key5".",";
    $s_fieldlist = $s_fieldlist." Key1_def".",";
    $s_fieldlist = $s_fieldlist." Key2_def".",";
    $s_fieldlist = $s_fieldlist." Key3_def".",";
    $s_fieldlist = $s_fieldlist." Key4_def".",";
    $s_fieldlist = $s_fieldlist." Key5_def".",";
    $s_fieldlist = $s_fieldlist." Key_def".",";
    $s_fieldlist = $s_fieldlist." Details_def".",";
    $s_fieldlist = $s_fieldlist." Details_data".",";
    $s_fieldlist = $s_fieldlist." TrackGroup".",";
    $s_fieldlist = $s_fieldlist." TrackVersion".",";
    $s_fieldlist = $s_fieldlist." company_id".",";
    $s_fieldlist = $s_fieldlist." transport_company".",";
    $s_fieldlist = $s_fieldlist." process_id";

    $s_fieldvalues = $s_fieldvalues."'".$s_userid."',";
    $s_fieldvalues = $s_fieldvalues."'".$s_userid."',";
    $s_fieldvalues = $s_fieldvalues."'".$S_UUID."',";
    $s_fieldvalues = $s_fieldvalues."'".date("Ymdhis")."',";
    $s_fieldvalues = $s_fieldvalues."'".date("Ymdhis")."',";
    $s_fieldvalues = $s_fieldvalues."'History',";
    $s_fieldvalues = $s_fieldvalues."'".$s_Key1."',";
    $s_fieldvalues = $s_fieldvalues."'".$s_Key2."',";
    $s_fieldvalues = $s_fieldvalues."'".$s_Key3."',";
    $s_fieldvalues = $s_fieldvalues."'".$s_Key4."',";
    $s_fieldvalues = $s_fieldvalues."'".$s_Key5."',";
    $s_fieldvalues = $s_fieldvalues."'ConnoteNo',";
    $s_fieldvalues = $s_fieldvalues."'cnhdr uk',";
    $s_fieldvalues = $s_fieldvalues."'notused',";
    $s_fieldvalues = $s_fieldvalues."'notused',";
    $s_fieldvalues = $s_fieldvalues."'notused',";
    $s_fieldvalues = $s_fieldvalues."'notused',";
    $s_fieldvalues = $s_fieldvalues."'ConNoteNO|Activity YYYYMMDD|Activity HHMM|comment',";
    $s_fieldvalues = $s_fieldvalues."'{$s_connoteno}|{$s_current_date}|$s_current_time|$s_cn_commentcs',";
    $s_fieldvalues = $s_fieldvalues."'CNCOMMENT',";
    $s_fieldvalues = $s_fieldvalues."'V1.0.0',";
    $s_fieldvalues = $s_fieldvalues."'".$s_company_id."',";
    $s_fieldvalues = $s_fieldvalues."'".$s_transport_company."',";
    $s_fieldvalues = $s_fieldvalues."''";

    $ssql = "INSERT dmcntrk (";
    $ssql = $ssql.$s_fieldlist.") values (".$s_fieldvalues.")";

    if (trim(strtoupper($sys_debug)) == "YES")
    {echo "<br> debug:::::arecord_sql sql=".$ssql."<br>";
        echo "dbcnx=[".$ps_dbcnx."]END OF DBCNX DEBUG<br>";
    }

    $rs_temp = mysqli_query($ps_dbcnx,$ssql);
//    $rs_temp = "101";
    if (!$rs_temp)
    {
        $s_response_status = "ERROR";
        $s_response_message = "Ref:dmaapi500_3000 - Update of the consignment details failed.[]".mysqli_error()."[] ssql=[]".$ssql."[]";
        $tmp = $class_main->clmain_u540_utl_activity_log($ps_dbcnx,"dma_api_montor","SYSMON","DMAAPI","cl_dmaapi_500_createcncomment","Error - unable to process the xml_data - xml_data is []".$xml_data."[]",$s_taskrequestid,"TASKREQUESTID","k2 value","k2 def","k3 value","k3 def","k4 value","k4 def","k5 value","k5 def");
        goto Z900_EXIT;
    }

//    echo "<BR>cl_dmaapi_500_createcncomment<br> $ssql=[]".$ssql."[] ";


//   echo "<BR>cl_dmaapi_500_createcncomment count=[]".$s_count."[]  cnuk coount=[]".$s_cnuk_count."[] s_cnuk=[]".$s_cnuk."[]";
// RUN SQL
    C380_DO_NEXT:
    $s_count = $s_count +1;
    GOTO C310_DO_ENTRY;
    C390_END:

    C900_END:

    Z800_END:
    $s_response_status = "PROCESSOK";
    $s_response_message = "The consignment(s) comment has been added";
    $s_response_message = $s_response_message.$s_repsonse_note;

    Z900_EXIT:

    $f_end_utime = microtime(true);
    $f_process_time = $f_end_utime - $f_start_utime;

    $tmp = $class_main->clmain_u540_utl_activity_log($ps_dbcnx,"cl_dmaapi_500_createcncomment","SYSMON","DMAAPI","cn_update","End of process to update status of cn",$s_requestid,"TASKREQUESTID",$f_process_time,"processingtime",$s_cnuk_count,"CnCount","k4 value","k4 def","k5 value","k5 def");


    $sys_function_out = $sys_function_out."<querytaskreply>";

    $sys_function_out = $sys_function_out."<taskdetails>";
    $sys_function_out = $sys_function_out."<TASKTYPE>".$s_tasktype."</TASKTYPE>";
    $sys_function_out = $sys_function_out."<TASKEMAILTO>".$s_mailto."</TASKEMAILTO>";
    $sys_function_out = $sys_function_out."<TASKREQUEST_ID>".$s_requestid."</TASKREQUEST_ID>";
    $sys_function_out = $sys_function_out."<TASKREQUEST_TIMELOCAL>".$s_requesttime."</TASKREQUEST_TIMELOCAL>";
    $sys_function_out = $sys_function_out."<TASKREQUEST_TIMEGMT>".$s_requesttimegmt."</TASKREQUEST_TIMEGMT>";
    $sys_function_out = $sys_function_out."<TASKTOKEN>".$s_token."</TASKTOKEN>";
    $sys_function_out = $sys_function_out."<TASKCLIENT_NAME>".$s_client_name."</TASKCLIENT_NAME>";
    $sys_function_out = $sys_function_out."<TASKSITE_ID>".$s_site_id."</TASKSITE_ID>";
    $sys_function_out = $sys_function_out."<TASKSITE_SOFTWARE>".$s_site_software."</TASKSITE_SOFTWARE>";
    $sys_function_out = $sys_function_out."<TASKSITE_VERSION>".$s_site_version."</TASKSITE_VERSION>";
    $sys_function_out = $sys_function_out."</taskdetails>";

    $sys_function_out = $sys_function_out."<responsedetails>";
    $sys_function_out = $sys_function_out."<RESPONSEPROCESSSTART>".$f_start_utime."</RESPONSEPROCESSSTART>";
    $sys_function_out = $sys_function_out."<RESPONSEPROCESSEND>".$f_end_utime."</RESPONSEPROCESSEND>";
    $sys_function_out = $sys_function_out."<RESPONSEPROCESSDURATION>".$f_process_time."</RESPONSEPROCESSDURATION>";
    $sys_function_out = $sys_function_out."<RESPONSESTATUS>".$s_response_status."</RESPONSESTATUS>";
    $sys_function_out = $sys_function_out."<RESPONSEMESSAGE>".$s_response_message."</RESPONSEMESSAGE>";
    $sys_function_out = $sys_function_out."<RESPONSEFILENAME>".$s_filename."</RESPONSEFILENAME>";
    $sys_function_out = $sys_function_out."<RESPONSEFILEURL>".$s_fileurl."</RESPONSEFILEURL>";
    $sys_function_out = $sys_function_out."<RESPONSECONNOTECOUNT>".$s_cnuk_count."</RESPONSECONNOTECOUNT>";
    $sys_function_out = $sys_function_out."</responsedetails>";

    $sys_function_out = $sys_function_out."</querytaskreply>";
    IF ($sys_debug == "YES"){echo( $sys_debug_text."  z900_EXIT sys_function_out =  ".$sys_function_out);};

//        echo "<BR><BR>".(str_replace("<","{",$sys_function_out))."<BR><BR>";


    return $sys_function_out;

}
//##########################################################################################################################################################################
function cl_dmaapi_z100_getcnuk_cnheader_details($ps_cnuk,$ps_dbcnx,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    A100_TEMPLATE_INIT:

    global $class_main;


    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NODEBUG") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "cl_dmaapi_200_getcn_details  called from ".$ps_calledfrom;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){echo( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." params = ps_debug,ps_details_def,ps_details_data,ps_sessionno");};
    IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." params = ".$ps_debug.",".$ps_details_def.",".$ps_details_data.",".$ps_sessionno);};

    A199_END_TEMPLATE_INIT:
    $sys_function_out = "cnuk supplied = ".$ps_cnuk;

    $s_cnuk = $ps_cnuk;

B100_PROCESS:
    $ssql = "SELECT * from dmcntrk where (uniquekey = '".$s_cnuk."' and trackgroup = 'cnheader' )";
    if (trim(strtoupper($sys_debug)) == "YES")
    {echo "<br> debug:::::arecord_sql sql=".$ssql."<br>";
        echo "dbcnx=[".$ps_dbcnx."]END OF DBCNX DEBUG<br>";
    }
    $rs_temp = mysqli_query($ps_dbcnx,$ssql);
    if (!$rs_temp)
    {
        $sys_function_out = "NODATA";
        goto Z900_EXIT;
    }

    $sys_function_out = mysqli_fetch_array($rs_temp) ;

Z900_EXIT:
    IF ($sys_debug == "YES"){echo( $sys_debug_text."  z900_EXIT sys_function_out =  ".$sys_function_out);};

    return $sys_function_out;

}
//##########################################################################################################################################################################
    function cl_dmaapi_z100_get_tco_email($ps_current_tco,$ps_email_address)
    {
        A100_INIT:

        $s_file_start_field='';
        $s_file_tco='';
        $s_file_tco_email_address='';

        $s_tco_email_file=$_SESSION['ko_map_path'].'tco_emails.ini';
        $s_email_address=$ps_email_address;
        $ps_current_tco=trim(strtoupper($ps_current_tco));

        B100_READ_FILE:

        $handle = @fopen($s_tco_email_file, "r");
        if (!$handle)
        {
            goto Z900_EXIT;
        }
        while (!feof($handle))
        {
            //$line = fgets($handle, 4096);
            //$line = fgets($handle, 8096);
            $line = fgets($handle);
            if(!empty($line))
            {
                $ar_file_lines[]=$line;
            }
        }
        fclose($handle);

        C100_PROCESS_FILE:

        foreach ($ar_file_lines as $key=>$s_tco_email_file_line)
        {
            $ar_tco_email_file_line=explode('|',$s_tco_email_file_line);

            if (trim($ar_tco_email_file_line[0])=='')
            {
                goto C900_READ_NEXT_LINE;
            }
            if (substr($ar_tco_email_file_line[0],0,1)=='*')
            {
                goto C900_READ_NEXT_LINE;
            }

            $s_file_start_field=trim($ar_tco_email_file_line[0]);
            $s_file_tco=trim(strtoupper($ar_tco_email_file_line[1]));
            $s_file_tco_email_address=trim($ar_tco_email_file_line[2]);
            $s_file_end_field=trim($ar_tco_email_file_line[3]);

            if ($ps_current_tco<>$s_file_tco)
            {
                goto C900_READ_NEXT_LINE;
            }

            $s_email_address=$s_file_tco_email_address;

            C900_READ_NEXT_LINE:
        }

        Z900_EXIT:

        return $s_email_address;
    }
//##########################################################################################################################################################################
function z901_dump($ps_text)
{

    $file_path  = "";
    if (isset($_SESSION['ko_map_path']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
        $file_path = $_SESSION['ko_map_path'];
    }

    $file_path = $file_path."logs";
    if (!file_exists($file_path))
    {
        $md= mkdir($file_path);
    }

    $dateym = date('Ym');
    $myfile = $file_path."\cl_dma_api _".$dateym.".txt";
    $fh = fopen($myfile,'a') or die("cant open file".$myfile);
    fwrite($fh,date("Y-m-d H:i:s",time())." - ".$ps_text."\r\n");
    fclose($fh);

}



//##########################################################################################################################################################################


// end of class
}
    ?>
