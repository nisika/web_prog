<?php
/*
craeted on 20110430 from cl_proc_jobline  by gw
processes that have been run and checked
20100501 - added the *if singel test and then build the line
20100501 - TESTED sysclassv2 AND sysclassv2if
20100501 - TESTED sysclass AND sysclassif
20100501 - TESTED add_details AND add_detailsif
*
*
*
*
*/
    class clprocessjobline
    {



  function pf_clprocessjobline_customError($errno,$errstr,$errfile,$errline,$errcontext)
  {
// NOTE **** THIS ONLY CATCHES NON-FATAL ERRORS
//  echo "<b>Error gw12:</b> [$errno] $errstr $errfile $errline $errcontext.<br>";
// gw 20100315       switch ($error_level) {
       switch ($errno) {
        case E_USER_ERROR:
            echo "<b>My ERROR</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            echo "  Fatal error on line $errline in file $errfile";
            echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
            echo "Aborting...<br />\n";
            exit(1);
            break;

        case E_USER_WARNING:
            echo "<b>My WARNING</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        case E_USER_NOTICE:
            echo "<b>My NOTICE</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        default:
            echo "<BR>Unknown error type: [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;
        }

    /* Don't execute PHP internal error handler */
    return true;

  }
//gw201104501 - some job process require an array value that seems to get lost if past inaide the normal params so added par_array_value to be used as needed
// this showed up doing the xml import processes  ?? perhaps xml is a special type of array??
  function pf_clprocessjobline($ps_dbcnx,$ps_job_line,$ps_details_def,$ps_details_data,$par_array_value,$ps_sessionno, $ps_debug)
      {
        $ko_map_path="";

    A000_SET_RUN:
             //set error handler
//            set_error_handler("pf_clprocessjobline_customError", E_ALL);
            date_default_timezone_set('Australia/Brisbane');
//gw20110313 - added user timezone
    if (isset($_SESSION[$ps_sessionno.'ut_logon_timezone']))
    {
        $timezone=$_SESSION[$ps_sessionno.'ut_logon_timezone'];
        date_default_timezone_set($timezone);
    }

    A000_DEFINE_VARIABLES:
        $sys_prog_name = "";
        $sys_debug = '';
        $s_file_exists = '';
        $s_filename="";
        $map = "";
        $tmp_array = array();
        $sys_function_out = "";
        $sys_function_name = "<br>DEbug of pf_clprocessjobline ";
        $s_temp_value = "";

        $s_sessionno = "";
        $s_helplocation = "";
        $function_id = "";
        $get_userid = "";
        $s_details_def="";
        $s_details_data="";
        $s_siteparams = "";
        $s_url_siteparams = "";
        $s_usr_style_list = "";
        $s_usr_style_values= "";
        $username = "";
        $s_inJCL ="";
        $s_temp = "";
        $results = "";
        $s_MultiLevelloop1 = "";
        $s_MultiLevelloop2 = "";
        $s_MultiLevelloop3 = "";
        $s_no_jcl_in_file = "";

        $s_output_to_file = "";
        $s_output_to_filename = "";
        $s_output_to_atend = "";
        $s_output_to_atend_do="";

    A200_LOAD_LIBRARIES:
        $sys_debug = strtoupper("NO");
        $sys_debug = strtoupper($ps_debug);



 //       $sys_debug = strtoupper("YES");



        $s_sessionno = $ps_sessionno;
        //        $sys_debug = strtoupper("yes");

        global $class_apps;
//        global $class_main;
        global $class_sql;

//      require_once($_SESSION['ko_prog_path'].'lib/class_main.php');
      $class_main = new clmain();

//      require_once($_SESSION['ko_prog_path'].'lib/class_sql.php');
//      $class_sql = new wp_SqlClient();

         IF ($sys_debug == "YES"){echo $sys_function_name."_get=";print_r($_GET);echo "  ";};
         IF ($sys_debug == "YES"){echo $sys_function_name."_post=";print_r($_POST);echo "  ";};

         IF ($sys_debug == "YES"){echo $sys_function_name."ps_job_line=(".$ps_job_line.")  ";};
         IF ($sys_debug == "YES"){echo $sys_function_name."ps_details_def=(".$ps_details_def.")  ";};
         IF ($sys_debug == "YES"){echo $sys_function_name."ps_details_data=(".$ps_details_data.")  ";};
         IF ($sys_debug == "YES"){echo $sys_function_name."par_array_value=(".$par_array_value.")  ";};
         IF ($sys_debug == "YES"){echo $sys_function_name."ps_sessionno=(".$ps_sessionno.")  ";};
         IF ($sys_debug == "YES"){echo $sys_function_name."ps_debug=(".$ps_debug.")  ";};


    A300_CONNECT_TODBASE:
         $dbcnx = $ps_dbcnx;

    A400_GET_PARAMETERS:

    A500_SET_VALUES:
         $ar_line_details = array();

        $s_MultiLevelloop1 = "NONE";
        $s_MultiLevelloop2 = "NONE";
        $s_MultiLevelloop3 = "NONE";
        $s_output_to_file = "NO";
        $s_output_to_filename = "";

        $s_details_def=$ps_details_def;
        $s_details_data=$ps_details_data;

    B100_GET_REC:

        $s_line_in = $ps_job_line;

/* gw20110826
        IF (strpos(strtoupper($s_line_in),strtoupper("DO_ACTION_URL")) !== false )
        {
            echo " gwdebug in clprocline  s_details_def=(".$s_details_def.")<br>";
            echo "s_details_data=(".$s_details_data.")<br>";
        }
*/

        IF (substr($s_line_in,0,1) == "*")
        {
            goto Z800_END_OF_LINE;
        }
        IF (trim($s_line_in) == "")
        {
            goto Z800_END_OF_LINE;
        }

        IF (strpos(strtoupper($s_line_in),strtoupper("SKIP_LINE")) === false )
        {}else{
            IF ($sys_debug == "YES"){echo $sys_function_name." "."got the map skip line command <br>".$s_line_in."";};
            goto Z800_END_OF_LINE;
        }
    // used to indicate if there is a start JCL loop
        IF (strpos(strtoupper($s_line_in),strtoupper("START_JCL_HERE"))  === false )
        {}else{
            IF ($sys_debug == "YES"){echo $sys_function_name."got the map starter start line for jcl<br>".$s_line_in."";};
            $s_inJCL ="Y";
            goto Z800_END_OF_LINE;
        }
    // used to indicate if there is a end  JCL loop
        IF (strpos(strtoupper($s_line_in),strtoupper("END_JCL_HERE"))  === false  )
        {}else{
            IF ($sys_debug == "YES"){echo $sys_function_name."got the map end line for jcl<br>".$s_line_in."";};
            $s_inJCL ="N";
            goto Z700_END;
        }
    // if before the required loop get the next line
        if ($s_inJCL =="N")
        {
//            IF ($sys_debug == "YES"){echo $sys_function_name." ".$i." in jcl  loop";};
            goto Z800_END_OF_LINE;
        }

        $s_no_jcl_in_file = "";
        $ar_line_details = explode("|",$s_line_in);

        IF ($sys_debug == "YES"){echo $sys_function_name." line <br>".$s_line_in."";};

        IF (strtoupper($ar_line_details[0]) == "DUMPTABLELISTTOSQLLOG")
        {
            $temp = $class_sql->c_sqlclient_list_tables_to_log($dbcnx);
            goto Z800_END_OF_LINE;
        }
    C000_DO_LINES_TYPES:
//gw20110501 - does 1 check for all the *if fields then builds the line as though it does not include the if variables and continues
    E1_NEWIF:
        IF (substr(strtoupper($ar_line_details[0]),-2) <> STRTOUPPER("if"))
        {
            GOTO E1_900_EXIT;
        }
    E1_200_DO_IF:

//echo "<br> clpro_jobl e 1 if s_line_in=".$s_line_in."<br>";

        $s_temp = $this->pf_a100_test_if($s_line_in,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  E1_200_DO_IF ");

//echo "<br> clpro_jobl e 1 if s_temp=".$s_temp."<br>";

        if ($s_temp == "FALSE")
        {
            $sys_function_out = "";
            GOTO Z800_END_OF_LINE;
        }
        $s_line_in = $s_temp;
        $ar_line_details = explode("|",$s_line_in);
    E1_900_EXIT:
 //$sys_debug = "YES";
         IF ($sys_debug == "YES"){echo $sys_function_name." DEBUG SET IN E1_900 EXIT ";};

         IF ($sys_debug == "YES"){echo $sys_function_name."<br>";};
         IF ($sys_debug == "YES"){echo $sys_function_name."doing line s_line_in=(".$s_line_in.")";};
         IF ($sys_debug == "YES"){echo $sys_function_name."doing  ar_line_details[0]=(".$ar_line_details[0].")";};
         IF ($sys_debug == "YES"){echo $sys_function_name."s_details_def=(".$s_details_def.")";};
         IF ($sys_debug == "YES"){echo $sys_function_name."s_details_data=(".$s_details_data.")";};

    E100_START:
        $ar_line_details[0] = TRIM($ar_line_details[0]);
         IF ($sys_debug == "YES"){echo $sys_function_name." ar_line_details[0]=[]".$ar_line_details[0]."[]"." TRIM-ar_line_details[0]=[]".TRIM($ar_line_details[0])."[]";};
        // sys_class|version|fieldname|class function and parameters|end
    //* sys_class|v1|field_style|clmain_v800_set_field_style(field_style,my_buddy_main.htm,"")|END
        IF (strtoupper($ar_line_details[0]) <> "SYS_CLASS")
        {
            IF ($sys_debug == "YES"){echo $sys_function_name." NOT SYS_CLASS";};
            GOTO E190_END;
        }
            IF ($sys_debug == "YES"){echo $sys_function_name." DOING SYS_CLASS";};

//        echo "<br>GWDEBUG  doing SYS_CLASS - LINE ".$s_line_in."<br>".$s_temp."<br>";


        $s_temp = $class_main->clmain_a110_do_sys_class($dbcnx,$s_line_in,"no",$s_details_def,$s_details_data,$s_sessionno);

//        echo "<br>GWDEBUG2  doing SYS_CLASS - LINE ".$s_line_in."<br>".$s_temp."<br>";


        if(strtoupper(trim($ar_line_details[4]," ")) == 'YES')
        {
           echo $sys_function_name."<br>doing SYS_CLASS - LINE ".$s_line_in."<br>".$s_temp."<br>";
        }
        $sys_function_out = $s_temp;
        $s_temp = "";
        goto Z800_END_OF_LINE;
    E190_END:

    E2_000_START:
        IF (strtoupper($ar_line_details[0]) <> STRTOUPPER("SYS_CLASSV2"))
        {
            IF ($sys_debug == "YES"){echo $sys_function_name." NOT SYS_CLASSV2";};
            GOTO E2_900_END;
        }

 //      echo "<br>clDDDDass_process_line e2_000  s_temp=".$s_temp."<br><br>";

            IF ($sys_debug == "YES"){echo $sys_function_name." DOING SYS_CLASSV2 s_line_in=[]".$s_line_in."[]";};


        $ar_array = $par_array_value;
        $s_temp = $class_main->clmain_a125_do_call_sys_class($dbcnx,$s_line_in,$ar_array,"no",$s_details_def,$s_details_data,$s_sessionno);
        if(strtoupper(trim($ar_line_details[4]," ")) == 'YES')
        {
           echo "<br>doing SYS_CLASS - LINE ".$s_line_in."<br>".$s_temp."<br>";
        }

 //       echo "<br>class_process_line e2_000  s_temp=".$s_temp."<br><br>";

            IF ($sys_debug == "YES"){echo $sys_function_name." DOING SYS_CLASSV2 sys_function_out=[]".$sys_function_out."[]";};

        $sys_function_out = $s_temp;
        $s_temp = "";
        goto Z800_END_OF_LINE;
    E2_900_END:
//##################################################################################################################################
    E3_000_START:
        IF (strtoupper($ar_line_details[0]) <> STRTOUPPER("SET_COOKIE"))
        {
            GOTO E3_900_END;
        }
        $set_cookie =         setcookie($ar_line_details[1],$ar_line_details[2]);
        $sys_function_out = $s_temp;
        $s_temp = "";
        goto Z800_END_OF_LINE;
    E3_900_END:
//##################################################################################################################################
    E4_000_START:
        IF (strtoupper($ar_line_details[0]) <> STRTOUPPER("SET_SESSION"))
        {
            GOTO E4_900_END;
        }
        $_SESSION[$s_sessionno.$ar_line_details[1]] = $ar_line_details[2];
        $sys_function_out = $s_temp;
        $s_temp = "";
        goto Z800_END_OF_LINE;
    E4_900_END:
//##################################################################################################################################
    E8_000_START:
        IF (strtoupper($ar_line_details[0]) <> STRTOUPPER("ADD_DETAILS"))
        {
            GOTO E8_900_END;
        }

//echo "<br> e00start process line before the do details s_line_in=".$s_line_in."<br> ";
        $ar_array = $par_array_value;

//      echo "clpjl e8_000 gw []".get_class($class_main)."[]  []".get_class()."[]<br>";
//      $this->get_real_class($class_main) ;
//      echo "clpjl e8_000 gw []".get_class($class_main)."[]  []".get_class()."[]<br>";

      $s_temp = $class_main->clmain_u120_do_add_details_line($dbcnx,$s_line_in,"NO",$s_details_def,$s_details_data,$s_sessionno,$s_url_siteparams);
//      echo "clpjl e8_000 gw after u120<br>";

        $sys_function_out = $s_temp;
        $s_temp = "";
        goto Z800_END_OF_LINE;
    E8_900_END:
//##################################################################################################################################
    E9_000_START:
        IF (strtoupper($ar_line_details[0]) <> STRTOUPPER("ADD_RECORD"))
        {
            GOTO E9_900_END;
        }
        $s_temp = $class_sql->sql_a100_add_record($dbcnx,$s_line_in,"no",$s_details_def,$s_details_data,$s_sessionno);
        $sys_function_out = $s_temp;
        $s_temp = "";
        goto Z800_END_OF_LINE;
    E9_900_END:
//##################################################################################################################################
    //MAKE_XMLFIELD|DATA_BLOB|whofor="",addrline1="",addrlin2="",suburb="",postcode="",cross_street="",locE10_note="",phoneno="",mobile="",email="",sms_alert="",machine_make="",machine_model="",machine_type="",job_overview="",job_description=""}|END
    E10_000_START:
        IF (strtoupper($ar_line_details[0]) <> STRTOUPPER("MAKE_XMLFIELD"))
        {
            GOTO E10_900_END;
        }
        $s_temp = $class_main->clmaiE10_u575_make_xml($s_line_in,"no",$s_details_def,$s_details_data,$s_sessionno);
        $sys_functioE10_out = $s_temp;
        $s_temp = "";
        goto Z800_END_OF_LINE;
    E10_900_END:
//##################################################################################################################################
    E11_000_START:
        IF (strtoupper($ar_line_details[0]) <> STRTOUPPER("ADD_UTL_ACTIVITY"))
        {
            GOTO E11_900_END;
        }
    E11_400_RUN:
        $ar_line_details[3] = $class_main->clmain_v200_load_line($ar_line_details[3],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  p4a ");
        $ar_line_details[4] = $class_main->clmain_v200_load_line($ar_line_details[4],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  p4b ");
        $ar_line_details[5] = $class_main->clmain_v200_load_line($ar_line_details[5],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  p4b ");
        $ar_line_details[6] = $class_main->clmain_v200_load_line($ar_line_details[6],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  p4b ");
        $ar_line_details[7] = $class_main->clmain_v200_load_line($ar_line_details[7],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  p4e ");
        $ar_line_details[9] = $class_main->clmain_v200_load_line($ar_line_details[9],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  p4g ");
        $ar_line_details[11] = $class_main->clmain_v200_load_line($ar_line_details[11],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  p4i ");
        $ar_line_details[13] = $class_main->clmain_v200_load_line($ar_line_details[13],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  p4k ");
        $ar_line_details[15] = $class_main->clmain_v200_load_line($ar_line_details[15],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  p4m ");

        $tmp = $class_main->clmain_u540_utl_activity_log($dbcnx,$ar_line_details[2],$ar_line_details[3],$ar_line_details[4],$ar_line_details[5],$ar_line_details[6],$ar_line_details[7],$ar_line_details[8],$ar_line_details[9],$ar_line_details[10],$ar_line_details[11],$ar_line_details[12],$ar_line_details[13],$ar_line_details[14],$ar_line_details[15],$ar_line_details[16]);
        goto Z800_END_OF_LINE;
    E11_900_END:
//##################################################################################################################################
    E12_000_START:
        IF (strtoupper($ar_line_details[0]) <> STRTOUPPER("UPDATE_RECORD"))
        {
                GOTO E12_900_END;
        }
    E12_400_RUN:
      IF ($sys_debug == "YES"){echo $sys_function_name."e12_400 ar_line_details[0]=[]".$ar_line_details[0]."[]  ";};
      IF ($sys_debug == "YES"){echo $sys_function_name."e12_400 s_line_in=[]".$s_line_in."[]  ";};

      $s_do_data_blob = "no";
        IF (strpos(strtoupper($s_line_in), "UPDATE_DATA_BLOB_WITH_DB_FIELDS") !== false)
        {
            IF ($sys_debug == "YES"){echo $sys_function_name."e12_400 goto e510  ";};
            goto E12_510_build_data_blob_fields;
        }
        IF (strpos(strtoupper($s_line_in), "UPDATE_DATA_BLOB_WITH_DB_FIELDS_NOADD") !== false)
        {
            IF ($sys_debug == "YES"){echo $sys_function_name."e12_400 goto e510 too  ";};
            goto E12_510_build_data_blob_fields;
        }
        IF (strpos(strtoupper($s_line_in), "UPDATE_DATA_BLOB_FIELD") !== false)
        {
            IF ($sys_debug == "YES"){echo $sys_function_name."e12_400 goto e520  ";};
            goto E12_520_get_originaE12_data_blob;
        }
        goto E12_550_SKIP_DATA_BLOB;

E12_510_build_data_blob_fields:
      $s_do_data_blob = "yes";
        $s_data_blob_fields = "";
//update_record|V1|jobs|jobs_id|%!_postv_jobs_id_!%|NO| UPDATE_DATA_BLOB_WITH_DB_FIELDS |end
    // IF LINE HAS UPDATE_ALE12_DATA_BLOB
    // GET ALL THE POSTV VALUES WITH _DB_ IN THEM
        $array_def = explode("|",$s_details_def);
        $array_data = explode("|",$s_details_data);
        IF ($sys_debug == "YES"){echo $sys_function_name."e12_510 array_def=[]".$array_def."[]  ";};

        if (count($array_def)<> count($array_data))
        {
    // do nothting yet gw20110116
        }
    //    echo "<br>E12_400_run list of posted fields and values <br>";
        for ($i2 = 0; $i2 < count($array_def); $i2++)
        {
// gw 20140107 IF ((strtoupper(substr($array_def[$i2],0,9)) == "POSTV_DB_")
          if (!strpos(strtoupper($array_def[$i2]),"_DB_") === false)
            {
                IF ($sys_debug == "YES"){echo $sys_function_name."e12_510 doing _db_ field array_def[$i2]=[]".$array_def[$i2]."[]  ";};
                $s_tmp = $array_def[$i2];
                $s_tmp = str_replace("_db_","_DB_",$s_tmp);
                $s_tmp = substr($s_tmp,strpos($s_tmp,"_DB_"));
                $s_tmp = str_replace("_DB_","",$s_tmp);

                $s_tmp_data = $array_data[$i2];

//                echo "<br>clprojob s_tmp_data=".$s_tmp_data;

//gw20110521 - USED TO put html into the xml datablob

                $s_tmp_data = str_replace("Error^??","#p",$s_tmp_data);
                $s_tmp_data = str_replace("#p%!_","#start_field#",$s_tmp_data);
                $s_tmp_data = str_replace("#P%!_","#start_field#",$s_tmp_data);
                $s_tmp_data = str_replace("%!start_","#start_field#",$s_tmp_data);
                $s_tmp_data = str_replace("%!START_","#start_field#",$s_tmp_data);
                $s_tmp_data = str_replace("_!%#p","#end_field#",$s_tmp_data);
                $s_tmp_data = str_replace("_!%#P","#end_field#",$s_tmp_data);
                $s_tmp_data = str_replace("_!end%","#end_field#",$s_tmp_data);
                $s_tmp_data = str_replace("_!END%","#end_field#",$s_tmp_data);
                $s_tmp_data = str_replace('"',"#quote#",$s_tmp_data);
                $s_tmp_data = str_replace("|","^",$s_tmp_data);

                $s_data_blob_fields = $s_data_blob_fields."|".$s_tmp."#!%=%!#".$s_tmp_data;
//                echo "<br>clprojob s_tmp_data=".$s_tmp_data;
            }else{
              IF ($sys_debug == "YES"){echo $sys_function_name."e12_510 not doing  field array_def[$i2]=[]".$array_def[$i2]."[]  ";};
            }
        }
      IF ($sys_debug == "YES"){echo $sys_function_name."e12_510 s_data_blob_fields=[]".$s_data_blob_fields."[]  ";};

E12_520_get_originaE12_data_blob:
        $s_orig_data_blob = "";
        $s_sql = "SELECt DATA_BLOB FROM  ".$ar_line_details[2]." where ".$ar_line_details[3]." = '".$ar_line_details[4]."'";
        $s_sql = STR_REPLACE("#P","|",$s_sql);
        $s_sql = STR_REPLACE("#p","|",$s_sql);
        $s_sql = $class_main->clmain_v200_load_line( $s_sql,$s_details_def,$s_details_data,"no",$s_sessionno, "clprocess_jobline lxxx");
        $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
        $s_rec_found = "NO";
E12_525_DO_RECS:
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
        {
            foreach( $row as $key=>$value)
            {
                if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
                {}else
                {
                    $s_orig_data_blob = $value;
                    $s_rec_found = "YES";
                }
            }
        }

        if ($s_rec_found == "NO")
        {
           echo "<br>720_run no record found  s_sql".$s_sql."<br>";
        }
// UPDATE THE RECORD_DATA_BLOB VALUE WITH THE SCREEN VALUES

//echo "<br> cljobprod update s_orig_data_blob=".str_replace("<","tdx^",$s_orig_data_blob)."<br>";

E12_530_UPDATE_DATA_BLOB:
        $s_data_blob = $s_orig_data_blob;

      IF (strpos(strtoupper($s_line_in), "UPDATE_DATA_BLOB_FIELD") !== false)
        {
//update_record^v1^jobs^jobs_id^#p%!__postv_jobs_id__!%#p^NO^ UPDATE_DATA_BLOB_FIELD[,]field_name[,]field_value) ^end
             $array_data = explode("[,]",$ar_line_details[6]);

//// gw 20120402             DIE ("CLAP JOBS LINE UPDATE_DATA - :.s_line_in = ".$s_line_in.",array_data[1]=".$array_data[1]."  Array_data[2]=".$array_data[2]);

             $s_data_blob = $class_main->clmain_u590_modify_field_xml($s_data_blob,$array_data[1],$array_data[2],"NO","update_singlef_only");
             GOTO E12_550_SET_DATA_BLOB_TO_UPDATE;
        }

      $array_data = explode("|",$s_data_blob_fields);
      for ($i2 = 0; $i2 < count($array_data); $i2++)
        {
            IF (strpos($array_data[$i2],"#!%=%!#") === false)
            {
// this is null for each field that is not _db_ ( data blob )
            }else
            {
                $array_data2 = explode("#!%=%!#",$array_data[$i2]);

                IF (strpos(strtoupper($s_line_in), "UPDATE_DATA_BLOB_WITH_DB_FIELDS_NOADD") !== false)
                {
                     $s_data_blob = $class_main->clmain_u590_modify_field_xml($s_data_blob,"data_blob",$array_data2[0],$array_data2[1],"NO","update_only");
                     GOTO E12_535_UPDATE_DATA_BLOB;
                }
                IF (strpos(strtoupper($s_line_in), "UPDATE_DATA_BLOB_WITH_DB_FIELDS") !== false)
                {
                     $s_data_blob = $class_main->clmain_u580_insert_field_xml($s_data_blob,"data_blob",$array_data2[0],$array_data2[1],"NO","uaction_730");
                    GOTO E12_535_UPDATE_DATA_BLOB;
                }
    E12_535_UPDATE_DATA_BLOB:
            }
        }
        GOTO  E12_550_SET_DATA_BLOB_TO_UPDATE;
//echo "<br> cljobprod update s_orig_data_blob=".str_replace("<","tdx^",$s_data_blob)."<br>";


    E12_550_SET_DATA_BLOB_TO_UPDATE:
        $s_data_blob = str_replace("'","''",$s_data_blob);
        $s_upd_data_blob = " DATA_BLOB = '".$s_data_blob."'";
        $s_line_in = "UPDATE_RECORD|V1|".$ar_line_details[2]."|".$ar_line_details[3]."|".$ar_line_details[4]."|".$ar_line_details[5]."|".$s_upd_data_blob."|".$ar_line_details[7];
    E12_550_SKIP_DATA_BLOB:
        $s_temp = $class_sql->sql_a200_update_record($dbcnx,$s_line_in,"no",$s_details_def,$s_details_data,$s_sessionno);
        IF (STRPOS($s_temp,"|^%##%^|")  === FALSE)
        {}ELSE{
            $ar_line_details2 = explode("|^%##%^|",$s_temp);
            $s_details_def = $s_details_def."|".$ar_line_details2[0];
            $s_details_data = $s_details_data."|".$ar_line_details2[1];
            $s_temp = "";
        }
 //     echo "<br>called from do blob update<br>ps_job_line=".$ps_job_line."<br>";//ne_in=view text<!--".str_replace("<","<gw",$s_line_in)."<br>";
 //     echo "<br>ar_line_details[2]=".$ar_line_details[2]." ar_line_details[3]=".$ar_line_details[3]." ar_line_details[4]=".$ar_line_details[4]."<br>";
        if ($s_do_data_blob == "yes")
        {
            $s_result = $class_main->clmain_u705_set_db_recfields_from_dblob("UPDATE",$ar_line_details[2],$ar_line_details[3],$ar_line_details[4],"","NO","CL_PROCLINE E12",$dbcnx,"NO",$s_details_def,$s_details_data,$s_sessionno);
        }

        goto Z800_END_OF_LINE;
    E12_900_END:
//##################################################################################################################################
E13_000_START:
        IF (strtoupper($ar_line_details[0]) <> STRTOUPPER("APP_CLASS"))
        {
            GOTO E13_900_END;
        }
        $s_temp = $this->Pf_b600_do_app_class($dbcnx,$s_line_in,"no",$s_details_def,$s_details_data,$s_sessionno);
        if(strtoupper(trim($ar_line_details[4]," ")) == 'YES')
        {
           echo "<br>clprocessjobline doing APP_CLASS - LINE ".$s_line_in."<br>".$s_temp."<br>";
        }
        $sys_function_out = $s_temp;
        $s_temp = "";
        goto Z800_END_OF_LINE;
    E13_900_END:
//##################################################################################################################################
    E14_000_START:
        IF (strtoupper($ar_line_details[0]) <> STRTOUPPER("URLVALUE"))
        {
            GOTO E14_900_END;
        }
        $s_temp = $this->Pf_b500_do_url_value($ps_dbcnx,$s_line_in,"NO",$s_details_def,$s_details_data,$s_sessionno);
        $sys_function_out = $s_temp;
        $s_temp = "";
        goto Z800_END_OF_LINE;
    E14_900_END:
//##################################################################################################################################
    E15_000_START:
        IF (strtoupper($ar_line_details[0]) <> STRTOUPPER("RUN_JCL"))
        {
            GOTO E15_900_END;
        }

        $s_temp = $this->Pf_c100_run_jcl($ps_dbcnx,$s_line_in,"NO",$s_details_def,$s_details_data,$s_sessionno);
        $sys_function_out = $s_temp;
        $s_temp = "";
        goto Z800_END_OF_LINE;
    E15_900_END:
//##################################################################################################################################
    E16_000_START:
        IF (strtoupper($ar_line_details[0]) <> STRTOUPPER("OUTPUTTOTEXT"))
        {
            GOTO E16_900_END;
        }

// 20170106gw - this is run via ut_jcl.php - this does not work because the output_to_file flag is reset for each line - it does not work for a whole jcl file
 //     echo "gwdebug e16 doing clpj - outputtotext";

          if(trim(strtoupper($ar_line_details[3])) =="CPJ_DONOTHING"){
              goto Z800_END_OF_LINE;
          };

        $s_output_to_file = "YES";
        $s_output_to_filename = $ar_line_details[1];
        $s_output_to_filename =  substr($s_output_to_filename,strpos($s_output_to_filename,"=") + 1);
        $s_output_to_filename = STR_REPLACE("#P","|",$s_output_to_filename);
        $s_output_to_filename = STR_REPLACE("#p","|",$s_output_to_filename);
        $s_output_to_filename = $class_main->clmain_v200_load_line( $s_output_to_filename,$s_details_def,$s_details_data,"NO",$s_sessionno," cl_proc_jobline  g900a  ");

        $s_output_to_fileonly = $ar_line_details[2];
        $s_output_to_fileonly =  substr($s_output_to_fileonly,strpos($s_output_to_fileonly,"=") + 1);


        $s_output_to_atend =  substr($s_output_to_atend,strpos($s_output_to_atend,"=") + 1);
        $s_output_to_atend = STR_REPLACE("#P","|",$s_output_to_atend);
        $s_output_to_atend = STR_REPLACE("#p","|",$s_output_to_atend);
        $s_output_to_atend = $class_main->clmain_v200_load_line( $s_output_to_atend,$s_details_def,$s_details_data,"NO",$s_sessionno," cl_proc_jobline  g900b  ");
        $s_output_to_atend_do =  $s_output_to_atend;
        $s_output_to_atend =  substr($s_output_to_atend,0,strpos($s_output_to_atend,":"));
        $s_output_to_atend_do =  substr($s_output_to_atend_do,strpos($s_output_to_atend_do,":") + 1);
        $sys_function_out = $s_output_to_atend_do;
        $s_temp = "";

        goto Z800_END_OF_LINE;
    E16_900_END:
//##################################################################################################################################
// gw 20120108 - not sure that this is being used
    E17_000_START:
        IF (strtoupper($ar_line_details[0]) <> STRTOUPPER("DRAW"))
        {
            GOTO E17_900_END;
        }
//      echo "<br>GWDEBUG  doing draw - LINE ".$s_line_in."<br>".$s_temp."<br>";

        $sys_function_out = $this->pf_b100_DO_DRAW_HTML($class_main->clmain_set_map_path_n_name($ko_map_path,$ar_line_details[1]),$ar_line_details[2],"NO",$s_details_def,$s_details_data);
        $s_temp = "";
//      echo "<br>GWDEBUG2  doing draw - LINE ".$s_line_in."<br>".$s_temp."<br>";
        goto Z800_END_OF_LINE;
    E17_900_END:
//##################################################################################################################################
E17a_000_START:
      IF (strtoupper($ar_line_details[0]) <> STRTOUPPER("DRAWV2"))
      {
          GOTO E17a_900_END;
      }
      $ko_map_path = STR_REPLACE("#P","|",$ko_map_path);
      $ko_map_path = STR_REPLACE("#p","|",$ko_map_path);

      $s_filename= $class_main->clmain_set_map_path_n_nameV2($ko_map_path,$ar_line_details[1],$s_details_def,$s_details_data,$s_sessionno);

      $sys_function_out = $this->pf_b100_DO_DRAW_HTML($s_filename,$ar_line_details[2],"NO",$s_details_def,$s_details_data);

//      echo("gwdie class_process_jobline drav2 s_filename=[]".$s_filename."[]  sys_function_out=[]".$sys_function_out."[]");


      $s_temp = "";
      goto Z800_END_OF_LINE;
      E17a_900_END:
//##################################################################################################################################
      E17b_000_START:
      IF (strtoupper($ar_line_details[0]) <> STRTOUPPER("DRAW2FILE"))
      {
          GOTO E17b_900_END;
      }

      $s_filename= $class_main->clmain_set_map_path_n_nameV2($ko_map_path,$ar_line_details[1],$s_details_def,$s_details_data,$s_sessionno);

      $sys_function_out = $this->pf_b100_DO_DRAW_HTML($s_filename,$ar_line_details[2],"NO",$s_details_def,$s_details_data);

//      echo("gwdie class_process_jobline drav2 s_filename=[]".$s_filename."[]  sys_function_out=[]".$sys_function_out."[]");

    // write the details to the file
      $s_output_to_file = "YES";
       $s_output_to_filename = $class_main->clmain_v200_load_line( $ar_line_details[3],$s_details_def,$s_details_data,"NO",$s_sessionno," cl_proc_jobline  E17B  ");

//      die ("gwdie e17b ar_line_details[3]=[]".$ar_line_details[3]."[] s_output_to_filename=[]".$s_output_to_filename."[]");

      $s_temp = "";
      goto Z800_END_OF_LINE;
      E17b_900_END:
//##################################################################################################################################
    E18_000_START:
        IF (strtoupper($ar_line_details[0]) <> STRTOUPPER("MAKEOUTPUTFILE"))
        {
            GOTO E18_900_END;
        }
        $s_output_to_filename = $ar_line_details[1];

/*      echo "<br>details _def =[]".$s_details_def."[]";
      echo "<br>details _data =[]".$s_details_data."[]";
      echo "<br>>".$class_main->clmain_v200_load_line( "|%!_details_def_dump_!%|",$s_details_def,$s_details_data,"NO",$s_sessionno," cl_proc_jobline  g900a  ");
      echo "<br>>".$class_main->clmain_v200_load_line( "|%!_session_dumpvars_!%|",$s_details_def,$s_details_data,"NO",$s_sessionno," cl_proc_jobline  g900a  ");
*/
//      die("gwdie e18 makeoutputilfe  s_output_to_filename=[]".$s_output_to_filename."[]");


        $s_output_to_filename =  substr($s_output_to_filename,strpos($s_output_to_filename,"=") + 1);
        $s_output_to_filename = STR_REPLACE("#P","|",$s_output_to_filename);
        $s_output_to_filename = STR_REPLACE("#p","|",$s_output_to_filename);
        $s_output_to_filename = $class_main->clmain_v200_load_line( $s_output_to_filename,$s_details_def,$s_details_data,"NO",$s_sessionno," cl_proc_jobline  g900a  ");

        IF (trim($s_output_to_filename) == "")
        {
            $s_output_to_filename = "cl_proc_jobline _output_to_file.txt";
        }


//      die("gwdie e18 makeoutputilfe  s_output_to_filename=[]".$s_output_to_filename."[]");

        $s_folder_name = str_replace("/","\\",$s_output_to_filename);
        $s_folder_name = substr($s_folder_name,0,strrpos($s_folder_name,'\\'));

//        echo "<br> cl_pr_jl e18 s_folder_name=".$s_folder_name;

//        if (!is_dir($s_folder_name)){
//        echo "<br> cl_pr_jl e18 1 s_folder_name=".$s_folder_name;
            IF (!$class_main->clmain_u100_mkdirs($s_folder_name,0777)){
//        echo "<br> cl_pr_jl e18 2 s_folder_name=".$s_folder_name;
                die ("<br>Failed to create the folder ".$s_folder_name." for file ".$s_output_to_filename);
            }
//        }

        $sys_function_out = $s_output_to_filename;
        $s_temp = "";
        goto Z800_END_OF_LINE;
    E18_900_END:
//##################################################################################################################################
    E19_000_START:
        IF (strtoupper($ar_line_details[0]) <> STRTOUPPER("DATALOOP"))
        {
            GOTO E19_900_END;
        }
        $s_page_current = "1";
        $s_page_limit = "200";
        if (isset($_SESSION['ko_display_page_limit']))
        {
            $s_page_limit = $_SESSION['ko_display_page_limit'];
        }

      die("die cl_proc_jobline e19 page=[]".$s_page_limit."[] session var ".$s_sessionno.'udp_page_limit');

        if (isset($_SESSION[$s_sessionno.'udp_page_limit']))
        {
            $s_page_limit = $_SESSION[$s_sessionno.'udp_page_limit'];
            die("die cl_proc_jobline e19 page=[]".$s_page_limit."[] session var ".$s_sessionno.'udp_page_limit');
        }
/*
        IF (strpos($s_url_siteparams,"^spage") === false)
        {
            GOTO E19_100;
        }
    //        urlvalue|v1|url_mode|mode|end
        $s_temp = "urlvalue|v1|not needed|spage|end";
        $s_page_current = $class_main->clmain_u500_do_url_line($dbcnx,$s_temp,"NO",$s_details_def,$s_details_data,$s_sessionno,$s_url_siteparams);
        if (strpos($s_page_current,"|^%##%^|") ===false)
        {
        }else{
            $ar_line_details = explode("|^%##%^|",$s_page_current);
    //junk        $s_page_current = $ar_line_details[0];
            $s_page_current = $ar_line_details[1];
            $s_temp = "";
            GOTO E19_100;
        };
*/
    E19_100:
        $s_map = $ar_line_details[1];
        $s_group = $ar_line_details[2];
        $s_temp_value = $ar_line_details[3];
        $s_temp_value = STR_REPLACE("#P","|",$s_temp_value);
        $s_temp_value = STR_REPLACE("#p","|",$s_temp_value);


// echo "<br> clpro_jobl e19 if s_line_in=".$s_line_in."<br>";


        $sys_function_out = $this->clp_jl_Pf_b200_do_dataloop($dbcnx,$s_temp_value,$class_main->clmain_set_map_path_n_name($ko_map_path,$s_map),$s_group,"no",$s_details_def,$s_details_data,$s_sessionno,$s_page_current,$s_page_limit);
        $s_temp = "";
        goto Z800_END_OF_LINE;
    E19_900_END:
//##################################################################################################################################
      E19B_000_START:
      IF (strtoupper($ar_line_details[0]) <> STRTOUPPER("DATALOOP2FILE"))
      {
          GOTO E19B_900_END;
      }
    E19B_100:
      $s_map = $ar_line_details[1];
      $s_group = $ar_line_details[2];
      $s_temp_value = $ar_line_details[3];
      $s_temp_value = STR_REPLACE("#P","|",$s_temp_value);
      $s_temp_value = STR_REPLACE("#p","|",$s_temp_value);
      $s_page_current = "0";
      $s_page_limit = "999999";
      $sys_function_out = $this->clp_jl_Pf_b200_do_dataloop($dbcnx,$s_temp_value,$class_main->clmain_set_map_path_n_name($ko_map_path,$s_map),$s_group,"no",$s_details_def,$s_details_data,$s_sessionno,$s_page_current,$s_page_limit);
      $s_temp = "";
      // write the details to the file
      $s_output_to_file = "YES";

      $s_output_to_filename = $class_main->clmain_v200_load_line( $ar_line_details[4],$s_details_def,$s_details_data,"NO",$s_sessionno," cl_proc_jobline  E17B  ");

      goto Z800_END_OF_LINE;
    E19B_900_END:
//##################################################################################################################################
    E20_000_START:
        IF (strtoupper($ar_line_details[0]) <> STRTOUPPER("udj_bdymain.css"))
        {
            GOTO E20_900_END;
        }
        $s_temp_value = $ar_line_details[1];
        $s_temp_value = STR_REPLACE("#P","|",$s_temp_value);
        $s_temp_value = STR_REPLACE("#p","|",$s_temp_value);
        $s_temp = $class_main->clmain_u300_set_udj_bdymain.css($dbcnx,$s_temp_value,"no",$s_details_def,$s_details_data,$s_sessionno);
        $sys_function_out = $s_temp;
        $s_temp = "";
        goto Z800_END_OF_LINE;
    E20_900_END:
    // pkn 20110619 - rundata was missing for some reason causing errors in jcl code.  Have re-added below
//##################################################################################################################################
    E21_000_START:
        IF (strtoupper($ar_line_details[0]) <> STRTOUPPER("RUNDATA"))
        {
            GOTO E21_900_END;
        }
        $s_temp_value = $ar_line_details[1];
        $s_temp_value = STR_REPLACE("#P","|",$s_temp_value);
        $s_temp_value = STR_REPLACE("#p","|",$s_temp_value);

//      echo "clpjl e21_000 gw []".get_class($class_main)."[]  []".get_class()."[]<br>";
//      $this->get_real_class($class_main) ;
//      echo "clpjl e21_000 gw []".get_class($class_main)."[]  []".get_class()."[]<br>";

//      echo "clpjl e21_000 gw before u300 []";

        $s_temp = $class_main->clmain_u300_set_rundata($dbcnx,$s_temp_value,"no",$s_details_def,$s_details_data,$s_sessionno);
//      echo "clpjl e8_000 after u300 ";

        $sys_function_out = $s_temp;
        $s_temp = "";
        goto Z800_END_OF_LINE;
    E21_900_END:
//##################################################################################################################################
    E22_000_START:
        IF (strtoupper($ar_line_details[0]) <> STRTOUPPER("DO_ACTION_URL"))
        {
            GOTO E22_900_END;
        }

        $s_do_url = $ar_line_details[1];

//        ECHO "<BR> s_do_url=".$s_do_url."<BR>";
        $s_do_url = STR_REPLACE("#P","|",$s_do_url);
        $s_do_url = STR_REPLACE("#p","|",$s_do_url);
        $s_do_url = $class_main->clmain_v200_load_line( $s_do_url,$s_details_def,$s_details_data,"NO",$s_sessionno," cl_proc_jobline  E22  ");

//        echo "def=<br>".$s_details_def;
//        echo "def=<br>".$s_details_data;
//gw20121231 - added the debuganddie option
        IF (strpos(strtoupper($s_do_url),strtoupper("(DEBUGANDDIE)")) !== false )
        {
                 die ("<br> die clprocess e22 do_action_url <br>s_do_url COMMENTED =<!--".$s_do_url."--> end of url commented - use view source");
        }
//                 die ("<br> die clprocess _job line e15 <br>s_do_url COMMENTED =<!--".$s_do_url."-->");

        echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
        echo '<html>';
        echo '<head>';
        echo '<meta HTTP-EQUIV="REFRESH" content="0; url='.$s_do_url.'">';
        echo '</head>';
        echo '</html>';
        exit;
        goto Z800_END_OF_LINE;
    E22_900_END:
//##################################################################################################################################
    E23_000_START:
        IF (strtoupper($ar_line_details[0]) <> STRTOUPPER("DO_CALCVALUE"))
        {
            GOTO E23_900_END;
        }
//gw20110928 do_calcvalue|return_fieldname|calc|end
// calc = field/value^operator^field/value^operator
// operators are +=plus, - = minus, / = divide, * = multiple
// expressions are done left to right NOT using maths rules
        $s_field_name = $ar_line_details[1];
        $s_temp_value = $ar_line_details[2];
        $s_temp_value = STR_REPLACE("#P","|",$s_temp_value);
        $s_temp_value = STR_REPLACE("#p","|",$s_temp_value);
        $s_temp = $class_main->clmain_v200_load_line( $s_temp_value,$s_details_def,$s_details_data,"NO",$s_sessionno," cl_proc_jobline  E23  ");
        $s_temp = $class_main->clmain_u760_do_calc($dbcnx,"NO",$s_details_def,$s_details_data,$s_sessionno,$s_temp);

        $sys_function_out = $s_field_name."|^%##%^|".$s_temp;

        $s_temp = "";
        goto Z800_END_OF_LINE;
    E23_900_END:
//##################################################################################################################################
    E24_000_START:
        IF (strtoupper($ar_line_details[0]) <> STRTOUPPER("DO_HTML_DEBUG_FILE"))
        {
            GOTO E24_900_END;
        }
// gw 20111024 - create a html file with filename of all the postv, ud_, ws_, detail_def + data, values, session, cookie
        $s_filename = $ar_line_details[1];
        $s_content_type = $ar_line_details[2];
        $s_calledfrom = $ar_line_details[3];

        $s_filename = "gwtst";
        $s_content_type = "ALL";
        $s_calledfrom = "cl_proc_jobline e24";

        $s_temp = $class_main->clmain_u770_do_html_debug($dbcnx,"NO",$s_details_def,$s_details_data,$s_sessionno,$s_calledfrom, $s_filename,$s_content_type);

        $s_temp = "";
        goto Z800_END_OF_LINE;
    E24_900_END:
//##################################################################################################################################
    E25_000_START:
        IF (strtoupper($ar_line_details[0]) <> STRTOUPPER("ADD_RECORD_AUTOKEY"))
        {
            GOTO E25_900_END;
        }
        $s_temp = $class_sql->sql_a110_add_record_autokey($dbcnx,$s_line_in,"no",$s_details_def,$s_details_data,$s_sessionno);
        $sys_function_out = $s_temp;
        $s_temp = "";
        goto Z800_END_OF_LINE;
    E25_900_END:
//##################################################################################################################################


//##################################################################################################################################
    E26_000_START:
      //H100_DO_MULTILEVEL:
    IF (trim(strtoupper($ar_line_details[0])," ") == "MULTILEVEL_1")
    {
        $s_MultiLevelloop1 = $s_line_in;
        goto Z800_END_OF_LINE;
    }
    IF (trim(strtoupper($ar_line_details[0])," ") == "MULTILEVEL_2")
    {
        $s_MultiLevelloop2 = $s_line_in;
        goto Z800_END_OF_LINE;
    }
    IF (trim(strtoupper($ar_line_details[0])," ") == "MULTILEVEL_3")
    {
        $s_MultiLevelloop3 = $s_line_in;
        goto Z800_END_OF_LINE;
    }
    IF (trim(strtoupper($ar_line_details[0])," ") == "DO_MULTILEVEL")
    {
        $sys_function_out .= $this->cl_pjl_Pf_b400_do_multilevelloop($dbcnx,$s_MultiLevelloop1,$s_MultiLevelloop2,$s_MultiLevelloop3,"NO",$s_details_def,$s_details_data,$s_sessionno,"cl_proc_jobline .php DO_MULTILEVEL",$ko_map_path);
        goto Z800_END_OF_LINE;
    }
//GW20160809 ADDED THE JC process to differentiate from the ut_jcl code
      //H100_DO_MULTILEVEL:
      IF (trim(strtoupper($ar_line_details[0])," ") == "MULTILEVEL_1JC")
      {
//          $s_line_in = str_replace("JC|","|",$s_line_in);
//          $s_MultiLevelloop1 = $s_line_in;
//          $sys_function_out .= "<BR> MULTILEVEL_1JC[]".$s_line_in."[]<BR>";
          goto Z800_END_OF_LINE;
      }
      IF (trim(strtoupper($ar_line_details[0])," ") == "MULTILEVEL_2JC")
      {
//          $s_line_in = str_replace("JC|","|",$s_line_in);
//          $sys_function_out .= "<BR> MULTILEVEL_1JC[]".$s_MultiLevelloop1."[]<BR>";
//          $sys_function_out .= "<BR> MULTILEVEL_2JC[]".$s_line_in."[]<BR>";
//          $s_MultiLevelloop2 = $s_line_in;
//          $sys_function_out .= "<BR> MULTILEVEL_2JC[]".$s_MultiLevelloop2."[]<BR>";
          goto Z800_END_OF_LINE;
      }
      IF (trim(strtoupper($ar_line_details[0])," ") == "MULTILEVEL_3JC")
      {
//          $s_line_in = str_replace("JC|","|",$s_line_in);
//          $sys_function_out .= "<BR> MULTILEVEL_3JC[]".$s_line_in."[]<BR>";
//          $s_MultiLevelloop3 = $s_line_in;
          goto Z800_END_OF_LINE;
      }
      IF (trim(strtoupper($ar_line_details[0])," ") == "DO_MULTILEVELJC")
      {
          $ar_temp = explode("~~~",$s_line_in);
          $s_MultiLevelloop1 = $ar_temp[1];
          $s_MultiLevelloop2 = $ar_temp[2];
          if(count($ar_temp) > 3){
              $s_MultiLevelloop3 = $ar_temp[3];
          }

//          $sys_function_out .= "<BR> MULTILEVEL_1JC[]".$s_MultiLevelloop1."[]<BR>";
//          $sys_function_out .= "<BR> MULTILEVEL_2JC[]".$s_MultiLevelloop2."[]<BR>";
//          $sys_function_out .= "<BR> MULTILEVEL_3JC[]".$s_MultiLevelloop3."[]<BR>";

//          $sys_function_out .= "<BR> MULTILEVELJC[]".$s_line_in."[]<BR>";
          $sys_function_out .= $this->cl_pjl_Pf_b400_do_multilevelloop($dbcnx,$s_MultiLevelloop1,$s_MultiLevelloop2,$s_MultiLevelloop3,"NO",$s_details_def,$s_details_data,$s_sessionno,"cl_proc_jobline .php DO_MULTILEVEL",$ko_map_path);
          goto Z800_END_OF_LINE;
      }
    E26_900_END:
//##################################################################################################################################

//##################################################################################################################################
    E27_000_START:
      IF (strtoupper($ar_line_details[0]) <> STRTOUPPER("DO_URL"))
      {
          GOTO E27_900_END;
      }

      $s_do_url =  $ar_line_details[1];
      $s_do_url = STR_REPLACE("#P","|",$s_do_url);
      $s_do_url = STR_REPLACE("#p","|",$s_do_url);
//    ECHO" utaction  do_url   ".$s_do_url."<br>";
      $s_do_url = $class_main->clmain_v200_load_line($s_do_url,$s_details_def,$s_details_data,"no",$s_sessionno,"ut_action do url");
//    ECHO" utaction  do_url   ".$s_do_url."<br>";
//    ECHO" utaction  do_url   ".$s_do_url."<br>";
//        die ("DEBUG_DIE  UT_ACTION_  a400start in utaction s_do_url=[]".$s_do_url."[]");
//
      str_replace("target=_blank",'  target="_blank"',$s_do_url);
      if (!strpos(strtoupper($s_do_url),"BUGANDDIE")  === FALSE )
      {
          die ("DEBUG_DIE  CLASS_PROCESS_JOBLINE  E27_000_START in utaction s_do_url=[]".$s_do_url."[]");
      }
//          echo "<!-- pageheader!start -->";
      echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
      echo '<html>';
      echo '<head>';
      echo '<meta HTTP-EQUIV="REFRESH" content="0; url='.$s_do_url.'">';
      echo '</head>';
      echo '</html>';
      exit;

      goto Z800_END_OF_LINE;
E27_900_END:



      /*    H1_000_MULTILEVEL:


              IF (trim(strtoupper($ar_line_details[0])," ") <> "MULTILEVEL_1")
              {
                  GOTO H1_900_END;
              }
          H1_100_DO:
              $s_temp_value = $array_lines[$i];
              GOTO H1_400_DO;
          H1_400_DO:
              $s_MultiLevelloop1 = $s_temp_value;

              goto Z800_END_OF_LINE;
          H1_900_END:

          H2_000_MULTILEVEL:
              IF (trim(strtoupper($ar_line_details[0])," ") == "MULTILEVEL_2")
              {
                  GOTO H2_100_DO;
              }
              IF (trim(strtoupper($ar_line_details[0])," ") == "MULTILEVEL_2IF")
              {
                  GOTO H2_200_DOIF;
              }
              GOTO H2_900_END;
          H2_100_DO:
              $s_temp_value = $array_lines[$i];
              GOTO H2_400_DO;
          H2_200_DOIF:
              IF (strtoupper($ar_line_details[1]) <> "V1")
              {
                  GOTO H2_210_do_If_v2;
              }
              $s_field_to_check = $ar_line_details[2];
              $s_comparison_operator = $ar_line_details[3];
              $s_compare_value  = $ar_line_details[4];
              $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  h2_200 ");
              $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_h2_200 ");
              if ($s_true == "FALSE")
              {
                  IF ($sys_debug == "YES"){z901_dump( $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);};
          //        $sys_function_out = "";
                  GOTO Z800_END_OF_LINE;
              }

              $s_temp_value =  "MULTILEVEL_2"."|".$ar_line_details[5]."|".$ar_line_details[6]."|".$ar_line_details[7]."|".$ar_line_details[8]."|".$ar_line_details[9]."|".$ar_line_details[10]."|".$ar_line_details[11]."|".$ar_line_details[12]."|".$ar_line_details[13]."|".$ar_line_details[14]."|".$ar_line_details[15];

              GOTO H2_400_DO;
          H2_210_do_If_v2:
          H2_400_DO:
                  $s_MultiLevelloop2 = $s_temp_value;
              goto Z800_END_OF_LINE;
          H2_900_END:

          H3_000_MULTILEVEL:
              IF (trim(strtoupper($ar_line_details[0])," ") == "MULTILEVEL_3")
              {
                  GOTO H3_100_DO;
              }
              IF (trim(strtoupper($ar_line_details[0])," ") == "MULTILEVEL_3IF")
              {
                  GOTO H3_200_DOIF;
              }
              GOTO H3_900_END;
          H3_100_DO:
              $s_temp_value = $array_lines[$i];
              GOTO H3_400_DO;
          H3_200_DOIF:
              IF (strtoupper($ar_line_details[1]) <> "V1")
              {
                  GOTO H3_210_do_If_v2;
              }
              $s_field_to_check = $ar_line_details[2];
              $s_comparison_operator = $ar_line_details[3];
              $s_compare_value  = $ar_line_details[4];
              $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  h3_200 ");
              $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_h3_200 ");
              if ($s_true == "FALSE")
              {
                  IF ($sys_debug == "YES"){z901_dump( $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);};
          //        $sys_function_out = "";
                  GOTO Z800_END_OF_LINE;
              }

              $s_temp_value = $ar_line_details[0]."|".$ar_line_details[5]."|".$ar_line_details[6]."|".$ar_line_details[7]."|".$ar_line_details[8]."|".$ar_line_details[9]."|".$ar_line_details[10]."|".$ar_line_details[11]."|".$ar_line_details[12]."|".$ar_line_details[13]."|".$ar_line_details[14]."|".$ar_line_details[15];
              GOTO H3_400_DO;
          H3_210_do_If_v2:
          H3_400_DO:
                  $s_MultiLevelloop3 = $s_temp_value;
              goto Z800_END_OF_LINE;
          H3_900_END:
          H4_DO:
              IF (trim(strtoupper($ar_line_details[0])," ") == "DO_MULTILEVEL")
              {
          //
          //    echo "<br>s_MultiLevelloop1=".$s_MultiLevelloop1."<br>";
          //    echo "<br>s_MultiLevelloop2=".$s_MultiLevelloop2."<br>";
          //    echo "<br>s_MultiLevelloop3=".$s_MultiLevelloop3."<br>";

                  $sys_function_out .= cl_pjl_Pf_b400_do_multilevelloop($dbcnx,$s_MultiLevelloop1,$s_MultiLevelloop2,$s_MultiLevelloop3,"NO",$s_details_def,$s_details_data,$s_sessionno,"cl_proc_jobline .php DO_MULTILEVEL",$ko_map_path);
                  goto Z800_END_OF_LINE;
              }
          H4_900_EXIT:
*/












      //        echo "<br> clproc z800 line unknown <br>s_line_in".$s_line_in."<br>";



          $sys_function_out = $ar_line_details[0]." is an unknown action(class_process_jobline)";
       //       DIE ("<BR> cl_proc_jobline  PRE Z800_UNKNOWN LINE <BR>The jcl file ".$map." has an unknown line =".$s_line_in);
Z800_END_OF_LINE:

          Z700_END:
              if (trim($s_no_jcl_in_file," ") == "")
              { }else{
                  $sys_function_out .= "<BR><BR>ERROR - from CLASS_procesw_jobline.php **".$s_no_jcl_in_file."**<br>";
              }
              if (trim($s_no_jcl_in_file," ") <> "")
              {
                  $sys_function_out .= "<BR><BR>dddddERROR - from CLASS_procesw_jobline.php **".$s_no_jcl_in_file."**<br>";
              }
          Z900_EXIT:
//      ECHO "CLPJL_LINE1168 where is output to go  s_output_to_file=[]".$s_output_to_file."[] s_output_to_atend=[]".$s_output_to_atend."[] sys_function_out=[]".$sys_function_out."[]";

          // PUT THIS IS A FILE
              if (trim($s_output_to_file) <> "YES")
              {
                  GOTO Z950_SKIP_FILE;
              }
              IF (trim($s_output_to_filename) == "")
              {
                  $s_output_to_filename = "cl_proc_jobline _output_to_file.txt";
              }

              $s_folder_name = str_replace("/","\\",$s_output_to_filename);
              $s_folder_name = substr($s_folder_name,0,strrpos($s_folder_name,'\\'));

          //gw20110130    $s_folder_name = substr($s_output_to_filename,0,strrpos($s_output_to_filename,'\\'));
              if (!is_dir($s_folder_name)){
                  IF (!mkdir($s_folder_name,0,true)){
                      die ("Failed to create the folder ".$s_folder_name." for file ".$s_output_to_filename);
                  }
              }


              if ($s_output_to_filename != $sys_function_out)
              {
                  $fh = fopen($s_output_to_filename,'a') or die("cant open file".$s_output_to_filename);
              //    fwrite($fh,date("Y-m-d H:i:s",time())."\r\n");
                  $sys_function_out = str_replace("<tdx_cr>","\r\n",$sys_function_out);
                  fwrite($fh,$sys_function_out."\r\n");
                  fclose($fh);
 //                 ECHO "CLPJL_LINE1197 write output to file<br><br>=[]".$sys_function_out."[]<br>";
              }else{
 //                 ECHO "CLPJL_LINE1199 no  write output to file";
              }

          /*
              echo "<br> now=".date("Y-m-d H:i:s",time());
              echo "<br> s_output_to_file=".$s_output_to_file;
          echo "<br> s_output_to_filename=".$s_output_to_filename;
          echo "<br> s_output_to_atend=".$s_output_to_atend;
          echo "<br> s_output_to_atend_do=".$s_output_to_atend_do;
          */
     //die ("gwdead");
        if (strtoupper($s_output_to_atend) == "OPEN")
        {
            GOTO Z905_OPEN_TEXT_FILE;
        }
        if (strtoupper($s_output_to_atend) == "DOMAP")
        {
            GOTO Z910_DO_MAP;
        }
        GOTO Z950_SKIP_FILE;

    Z905_OPEN_TEXT_FILE:

        $sys_function_out = "";
        $sys_function_out .= '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
        $sys_function_out .= '<html>';
        $sys_function_out .= '<head>';
    //    $sys_function_out .= '<meta HTTP-EQUIV="REFRESH" content="0; '.$s_output_to_atend_do.'">';
        $sys_function_out .= '<br>This process requires you to open <a href='.$s_output_to_atend_do.'>this file</a>';
    //    $sys_function_out .= '<br><a href='.$s_output_to_atend_do.'>map file</a>';
        $sys_function_out .= '</head>';
        $sys_function_out .= '</html>';

        GOTO Z950_SKIP_FILE;

Z910_DO_MAP:
        $sys_function_out = "";
        $sys_function_out .= '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
        $sys_function_out .= '<html>';
        $sys_function_out .= '<head>';
    //    $sys_function_out .= '<meta HTTP-EQUIV="REFRESH" content="0; '.$s_output_to_atend_do.'">';
        $sys_function_out .= '<br>DO MAP PROCESS This process requires you to open <a href='.$s_output_to_atend_do.'>this file</a>';
    //    $sys_function_out .= '<br><a href='.$s_output_to_atend_do.'>map file</a>';
        $sys_function_out .= '</head>';
        $sys_function_out .= '</html>';

        GOTO Z950_SKIP_FILE;
    Z950_SKIP_FILE:
//        ECHO "<br> end of the class_procss_jobline clase=".$sys_function_out."<br> end of &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&";

      return $sys_function_out;
      }



    function get_real_class($obj) {
        $classname = get_class($obj);

        if (preg_match('@\\\\([\w]+)$@', $classname, $matches)) {
            $classname = $matches[1];
        }

        echo  "clpj - getclass=[]".$classname."[]";
    }

// ***********************************************************************************************************************************
// start functions
function pf_a100_test_if($ps_line_in,$ps_details_def,$ps_details_data,$ps_debug,$ps_sessionno,$ps_calledfrom)
{

//    echo "<br> if test line = ".$ps_line_in."<br>";

//    global $class_main;
    $class_main = new clmain();

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
//add_detailsDEBUG1_DIEif|v1|session_upd_job_no_prefix|equal|space|jcl_job_no_prefix_sql| ddd|NO|END
    IF (!strpos(strtoupper($ps_line_in),"DEBUG1_DIE") === false)
    {
        IF ($sys_debug ="NO") {
            $sys_debug  = "YES";
            $sys_debug_text = " in line debug1_die";
        }
    }

    $sys_function_name = "";
    $sys_function_name = "debug - pf_a100_test_if";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name."<br> ps_in_line = ".$ps_line_in."<br> ps_details_def = ".$ps_details_def."<br> ps_details_data ".$ps_details_data."<br> ps_debug = ".$ps_debug." ";};

    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;

B100_DO:

    $ar_line_details = explode("|",$ps_line_in);

    $ar_line_details[1] = str_replace("IF","",strtoupper($ar_line_details[1]));
    $ar_line_details[1] = str_replace("DEBUG1_DIE","",strtoupper($ar_line_details[1]));

    IF (strtoupper($ar_line_details[1]) <> "V1")
    {
        GOTO D100_DO_IF_V2;
    }

    $s_field_to_check = $ar_line_details[2];
    /* this may be necesarry in the future
    // gw 20140106 - if the field to check has #p then it may have more than 1 field to change
        if (!strpos($s_field_to_check,"#p") === false)
        {
            $s_field_to_check = str_replace("#p","|",$s_field_to_check);
            $s_field_to_check = str_replace("#P","|",$s_field_to_check);
            $s_field_to_check = str_replace("__","_",$s_field_to_check);
            IF ($sys_debug == "YES"){echo $sys_debug_text." pre  v300  s_field_to_check=[]".$s_field_to_check."[]";};
            $s_field_value = $class_main->clmain_v200_load_line($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl m200");
        }else{
            $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl m200");
        }
    */

    $s_field_to_check = str_replace("#p","",$s_field_to_check);
    $s_field_to_check = str_replace("#P","",$s_field_to_check);
    $s_field_to_check = str_replace("__","_",$s_field_to_check);

    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];

    $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$ps_sessionno,"cl_proc_jobline  pf_a100_test_if ");
    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcE2pf_a100_test_if ");

    IF (!strpos(strtoupper($ps_line_in),"DEBUG1_DIE") === false)
    {
        echo "<br>";
        echo "<br>s_field_to_check=(".$s_field_to_check.")";
        echo "<br>s_comparison_operator=(".$s_comparison_operator.")";
        echo "<br>s_compare_value=(".$s_compare_value.")";
        echo "<br>s_field_value=(".$s_field_value.")";
        echo "<br>s_true=(".$s_true.")";
        echo "<br>";
        die("<br>class_process_job line do if - debug1_die - linei in=".$ps_line_in."<br>");
    }

    if ($s_true == "FALSE")
    {
        IF ($sys_debug == "YES"){echo ( $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);};
        $sys_function_out = $s_true;
        GOTO Z900_EXIT;
    }
    $i_count = 4;
    $s_line_in = str_replace("IF","",strtoupper($ar_line_details[0]))."|";
C100_BUILD_LINE:
    $i_count = $i_count + 1;
    if ($i_count < count($ar_line_details))
    {
        $s_line_in = $s_line_in.str_replace("IF","",$ar_line_details[$i_count])."|";
        goto C100_BUILD_LINE;
    }
    $sys_function_out = $s_line_in;
    GOTO Z900_EXIT;
D100_DO_IF_V2:
    IF (strtoupper($ar_line_details[1]) <> "V2+OPTIONS")
    {
        GOTO E100_DO_IF_V3;
    }
//add_detailsif|v2|jcl_maps/tdxhds/cl1000_cn_send_email_hds_packedeml_#P%!_rec_company_id_!%#P.htm|file_exists|TRUE|jcl_maps/tdxhds/cl1000_cn_send_email_hds_packedeml_#P%!_rec_company_id_!%#P.htm (#ELSE#) jcl_maps/tdxhds/cl1000_cn_send_email_hds_packedeml.htm|jcl_mapname|##result_value##|NO|end
//0 add_detailsif
//1 |v2
//2 |jcl_maps/tdxhds/cl1000_cn_send_email_hds_packedeml_#P%!_rec_company_id_!%#P.htm
//3 |file_exists
//4 |TRUE
//5 |jcl_maps/tdxhds/cl1000_cn_send_email_hds_packedeml_#P%!_rec_company_id_!%#P.htm (#ELSE#) jcl_maps/tdxhds/cl1000_cn_send_email_hds_packedeml.htm
//6 |jcl_mapname
//7 |##result_value##
//8 |NO
//9 |end

    $s_field_to_check = $ar_line_details[2];

    $s_field_to_check = str_replace("__","_",$s_field_to_check);
    IF ($sys_debug == "YES"){echo $sys_debug_text." pre  v300  s_field_to_check=[]".$s_field_to_check."[] s_details_def=[]".$s_details_def."[] s_details_data=[]".$s_details_data."[] sys_debug=[]".$sys_debug."[] ps_sessionno=[]".$ps_sessionno."[]";};
    $s_field_value = $class_main->clmain_v200_load_line($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$ps_sessionno,"clpj_pf_a100_test_if");

    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];
    $s_result_value  = $ar_line_details[5];

    $s_field_to_check = str_replace("__","_",$s_field_to_check);

    $s_result_value = $class_main->clmain_v200_load_line($s_result_value,$s_details_def,$s_details_data,$sys_debug,$ps_sessionno,"clpj_pf_a100_test_if");

    $ar_result_value = explode("(#ELSE#)",$s_result_value);

//    $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$ps_sessionno,"cl_proc_jobline  pf_a100_test_if ");

    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcE2pf_a100_test_if ");

    $i_count = 5;
    $s_line_out = str_replace("IF","",strtoupper($ar_line_details[0]))."|";
D150_BUILD_LINE:
    $i_count = $i_count + 1;
    if ($i_count < count($ar_line_details))
    {
        $s_line_out = $s_line_out.str_replace("IF","",$ar_line_details[$i_count])."|";
        goto D150_BUILD_LINE;
    }

    if ($s_true == "FALSE")
    {
        $s_line_out = str_replace("##result_value##",trim($ar_result_value[1]),$s_line_out);
    }else{
        $s_line_out = str_replace("##result_value##",trim($ar_result_value[0]),$s_line_out);
    }
    $sys_function_out = $s_line_out;

    IF (!strpos(strtoupper($ps_line_in),"DEBUGV2OPTION_DIE") === false)
    {
        echo "<br>if v2 DEBUGV2OPTION_DIE";
        echo "<br>s_field_to_check=(".$s_field_to_check.")";
        echo "<br>s_comparison_operator=(".$s_comparison_operator.")";
        echo "<br>s_compare_value=(".$s_compare_value.")";
        echo "<br>s_field_value=(".$s_field_value.")";
        echo "<br>s_true=(".$s_true.")";
        echo "<br>sys_function_out=(".$sys_function_out.")";
        echo "<br>end if v2 DEBUGV2OPTION_DIE";
        die("<br>class_process_job line do if - DEBUGV2OPTION_DIE - linei in=".$ps_line_in."<br>");
    }

    GOTO Z900_EXIT;
E100_DO_IF_V3:



    die("<br>class_process_job line do if - invalid version - linei in=".$ps_line_in."<br>");

Z900_EXIT:
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ";};
    IF ($sys_debug == "YES"){$sys_function_out.=" <!--".$sys_debug_text."-->";};

    return $sys_function_out;
}


function pf_b100_DO_DRAW_HTML($ps_filename,$ps_group,$ps_debug,$ps_details_def,$ps_details_data)
{

    //gw20160212 global $class_main;
    $class_main = new clmain();

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - pf_b100_DO_DRAW_HTML";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_filename = []".$ps_filename."[] ps_group=[]".$ps_group."[] ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};

    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;

//    $sys_function_out .= "gwdebug cl_proc_jobline .php load ".$ps_group." from file ".$ps_filename."<br>";

    $sys_function_out .= $class_main->clmain_v100_load_html_screen($ps_filename,$s_details_def,$s_details_data,"no",$ps_group);

    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ";};
    IF ($sys_debug == "YES"){$sys_function_out.=" <!--".$sys_debug_text."-->";};


    return $sys_function_out;

}
//##########################################################################################################################################################################

function pf_b110_DO_DRAWIF_HTML($ps_line_in,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    global $class_main;
//* DrawIF|v1|field|operator|value|map_name|loop|debug|END

//DrawIF|v1|url_MODE|=|WELCOME|my_buddy_main.htm|welcome_map|no|END

    $s_line = $ps_line_in;
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }

    if (strpos(strtoupper( $s_line),"**DODEBUG**") === false )
    {}else
    {
        $sys_debug  = "YES";
        $sys_debug_text = "Inline debug";
        $s_line = str_replace("**dodebug**","",$s_line);
        $s_line = str_replace("**DODEBUG**","",$s_line);
        $s_line = str_replace("**DoDebug**","",$s_line);
    }

    $sys_function_name = "";
    $sys_function_name = "debug - pf_b100_DO_DRAW_HTML";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo "<br>".$sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){echo "<br>".$sys_debug_text.=" ".$sys_function_name." ps_in_line = ".$ps_line_in." processing line = ".$s_line." ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};

A080_START:
    $ar_line_details = explode("|",$s_line);
    if (strtoupper($ar_line_details[0]) != "DRAWIF")
    {
        goto Z900_EXIT;
    }
    if (strtoupper($ar_line_details[1]) != "V1")
    {
        goto A1_DO_V2;
    }

A100_INIT_VARS:
    $s_field_to_check = $ar_line_details[2];
    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];

    $s_map_name = $class_main->clmain_set_map_path_n_name("",$ar_line_details[5]);
    $s_map_group = $ar_line_details[6];
    $s_line_debug = $ar_line_details[7];

    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;

B100_START:
    $sys_function_out = "?b110_unknown".$ps_line_in."?";
    $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,"no",$ps_sessionno,"cl_proc_jobline  b1100");
    IF ($sys_debug == "YES"){echo "<br>".$sys_debug_text.=" ".$sys_function_name." s_field_to_check = ".$s_field_to_check." s_field_value = ".$s_field_value;};

C100_DO_V1:
    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_b110");
    if ($s_true == "FALSE")
    {
        IF ($sys_debug == "YES"){z901_dump( $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);};
        $sys_function_out = "";
        GOTO Z900_EXIT;
    }



    $sys_function_out = $class_main->clmain_v100_load_html_screen($s_map_name,$s_details_def,$s_details_data,"NO",$s_map_group);

    IF ($sys_debug == "YES"){echo "<br>".$sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ";};
    IF ($sys_debug == "YES"){echo "<br>".$sys_function_out.=" <!--".$sys_debug_text."-->";};


GOTO Z900_EXIT;

A1_DO_V2:

Z900_EXIT:
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clp_jl_Pf_b200_do_dataloop($ps_dbcnx,$ps_sql,$ps_filename,$ps_group,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno, $ps_page_current,$ps_page_limit)
{
    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }

    if(strpos($ps_sql,"*DODEBUG_DATALOOP*") <> false)
    {
        $ps_sql = str_replace("*DODEBUG_DATALOOP*","",$ps_sql);
        $sys_debug  = "YES";
        $sys_debug_text = "sql debug:"; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }

    $sys_function_name = "";
    $sys_function_name = "debug - Pf_b200_do_dataloop";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    z901_dump($sys_debug, $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");
    z901_dump($sys_debug, $sys_debug_text."=".$sys_function_name." ps_sql = ".$ps_sql." ps_filename = ".$ps_filename."  ps_group = ".$ps_group."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ");

    // define function specific variables and code
//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";};

//    $sys_function_out = "before globals";
//    return $sys_function_out;
//    EXIT;

//gw20160212    global $class_sql;
    $class_sql = new wp_SqlClient();
//gw20160212    global $class_main;
    $class_main = new clmain();


    $s_sql = "";
    $result = "";
    $s_rec_found = "";
    $s_details_def = "";
    $s_details_data = "";
    $s_dataloop_type = "";
    $s_dataloop_type = "newlist";
    $s_dataloop_action = "";

    $ar_dd = array();

    $s_sql = $ps_sql;
    $s_details_def = $ps_details_def ;
    $s_details_data = $ps_details_data ;

    $s_sql = $class_main->clmain_v200_load_line( $s_sql,$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b200");

//    IF (strpos(strtoupper($s_sql),"|%!") > 0 )
//    {
//    }else{
//        //null
//    }

    $sys_debug2 = "NO";
//    $sys_debug2 = "YES";
    z901_dump($sys_debug, $sys_debug_text."   ".$sys_function_name." s_sql after being loaded []".$s_sql."[]");

    $s_page_current = $ps_page_current; // 20150126 - replaced below
    $s_page_limit = $ps_page_limit;

    if (isset($_SESSION[$ps_sessionno.'udp_page_limit']))
    {
        $s_page_limit = $_SESSION[$ps_sessionno.'udp_page_limit'];
//        die("die cl_proc_jobline e19 page=[]".$s_page_limit."[] session var ".$ps_sessionno.'udp_page_limit');
    }


    if(strpos($s_sql,"***newlist***") <> false)
    {
        $s_sql = str_replace("***newlist***","",$s_sql);
        $s_dataloop_type = "newlist";
    }
    if(strpos($s_sql,"***nextpage***") <> false)
    {
        $s_dataloop_type = "pages";
        $s_dataloop_action = "nextpage";
        $s_sql = str_replace("***nextpage***","",$s_sql);
    }
    if(strpos($s_sql,"***prevpage***") <> false)
    {
        $s_dataloop_type = "pages";
        $s_dataloop_action = "prevpage";
        $s_sql = str_replace("***prevpage***","",$s_sql);
    }
    if(strpos($s_sql,"***firstpage***") <> false)
    {
        $s_dataloop_type = "pages";
        $s_dataloop_action = "firstpage";
        $s_sql = str_replace("***firstpage***","",$s_sql);
    }
    if(strpos($s_sql,"***lastpage***") <> false)
    {
        $s_dataloop_type = "pages";
        $s_dataloop_action = "lastpage";
        $s_sql = str_replace("***lastpage***","",$s_sql);
    }
    $s_sql_start = microtime(true);

    z901_dump($sys_debug, $sys_debug_text."   ".$sys_function_name." s_dataloop_type []".$s_dataloop_type."[]");
    A120_NEWLIST:
    IF(    $s_dataloop_type <> "newlist")
    {
        GOTO A129_END;
    }
//gw the totals for the full data set
    $s_last_check_time = microtime(true);
    IF ($sys_debug2 == "YES"){echo "<br>before exec_query gwutjcl b190 - ".(round(microtime(true) - $s_last_check_time,12));};
    $s_last_check_time = microtime(true);
    $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_sql);
    IF ($sys_debug2 == "YES"){echo "<br>after exec_query gwutjcl b190 - ".(round(microtime(true) - $s_last_check_time,15));};
    $s_last_check_time = microtime(true);

    $s_total_rows = mysqli_affected_rows($ps_dbcnx);
    IF ($sys_debug2 == "YES"){echo "<br>after affected rows  gwutjcl b190 - ".(round(microtime(true) - $s_last_check_time,15));};
    $s_last_check_time = microtime(true);

    z901_dump($sys_debug, $sys_debug_text."=".$sys_function_name." a120 s_total_rows = []".$s_total_rows."[]  s_page_limit = []".$s_page_limit."[] ");

    if(strtoupper(trim($s_page_limit)) =="ALL")
    {
        $s_total_pages = "1";
        GOTO A129_END;
    }
    $s_total_pages = ceil($s_total_rows/$s_page_limit);
    A129_END:

    A130_PAGES:
    IF($s_dataloop_type <> "pages")
    {
        GOTO A139_END;
    }
//gw the totals from the session var
    $s_total_rows = $class_main->clmain_v300_set_variable( "rundatasql_total_rows",$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b200a" );

    IF ($sys_debug2 == "YES"){echo "<br>after affected rows  gwutjcl b190 - ".(round(microtime(true) - $s_last_check_time,15));};
    $s_last_check_time = microtime(true);
    $s_total_pages = $class_main->clmain_v300_set_variable( "rundatasql_total_pages",$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b200b" );
    $s_page_current = $class_main->clmain_v300_set_variable( "rundatasql_page_current",$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b200c" );
    if($s_dataloop_action == "nextpage")
    {
        $s_page_current = $s_page_current + 1;
    };
    if($s_dataloop_action == "prevpage")
    {
        $s_page_current = $s_page_current - 1;
    };
    if($s_dataloop_action == "firstpage")
    {
        $s_page_current = 1;
    };
    if($s_dataloop_action == "lastpage")
    {
        $s_page_current = $s_total_pages;
    };
    A139_END:

    $s_next_starts_at_row = $s_page_current * $s_page_limit;

    $s_page_next = $s_page_current + 1;
    if ($s_page_next > $s_total_pages)
    {
        $s_page_next = $s_total_pages;
    }
//gw20150125     $s_next_starts_at_row = $s_page_next * $s_page_limit;

    $s_page_prev = $s_page_current -1;
    if ($s_page_prev < 1)
    {
        $s_page_prev = 1 ;
    }
//    $s_prev_starts_at_row = ($s_page_prev * $s_page_limit) - $s_page_limit;
    $s_prev_starts_at_row = $s_next_starts_at_row  - $s_page_limit;
    if($s_prev_starts_at_row< 0)
    {
        $s_prev_starts_at_row = 0;
    }
    $s_current_starts_at_row =  ($s_page_current -1) * $s_page_limit;

    $s_sql = $class_sql->c_sqlclient_add_select_limit($s_sql,$s_page_current,$s_page_limit);

A800_do_display_sql:
    z901_dump($sys_debug, $sys_debug_text."=".$sys_function_name." a800 s_sql = ".$s_sql." ");
    $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_sql);
    $s_rec_found = "NO";


//20160115gw     ECHO "UTJCL A800 - AFTER SQL";

//20160115gw     if(!$result )
//20160115gw     {
//20160115gw         z901_dump($sys_debug, $sys_debug_text."=".$sys_function_name." a800 no result for s_sql = ".$s_sql." ");
//20160115gw         ECHO "UTJCL A800 - BAD RESULT L";
//20160115gw     }ELSE{
//20160115gw         z901_dump($sys_debug, $sys_debug_text."=".$sys_function_name." a800 GOT A result for s_sql = ".$s_sql." ");
//20160115gw         ECHO "UTJCL A800 - GOOD SQL";
//20160115gw     }

    $S_sql_end =  microtime(true);
    $s_sql_duration = round($S_sql_end - $s_sql_start,5);
    IF ($sys_debug2 == "YES"){echo "<br>sql duration  gwutjcl b190".$s_sql_duration;};
    $s_last_check_time = microtime(true);

    $s_page_def = "rundatasql_upd_affected_rows|rundatasql_duration|rundatasql_total_rows|rundatasql_page_current|rundatasql_current_starts_at_row|rundatasql_page_limit|rundatasql_total_pages|rundatasql_page_next|rundatasql_next_starts_at_row|rundatasql_page_prev|rundatasql_prev_starts_at_row|rundatasql_sql";
    $s_page_data = mysqli_affected_rows($ps_dbcnx)."|".$s_sql_duration." secs|".$s_total_rows."|".$s_page_current."|".$s_current_starts_at_row."|".$s_page_limit."|".$s_total_pages."|".$s_page_next."|".$s_next_starts_at_row."|".$s_page_prev."|".$s_prev_starts_at_row."|".$s_sql;

    $_SESSION['rundata_sql_def'] = $s_page_def;
    $_SESSION['rundata_sql_data'] = $s_page_data;

    $s_next_prev_row = $class_main->clmain_v950_next_prev_row($_SESSION['rundata_sql_def'],"NO","ut_jcl rundata",$ps_details_def,$ps_details_data,$ps_sessionno);

//    die ("gw utjcl b200 s_dataloop_typedddd".$s_dataloop_type);
    $s_details_def = $ps_details_def."|SYS_RD_NEXT_PREV_ROW|".$s_page_def;
    $s_details_data = $ps_details_data."|".$s_next_prev_row."|".$s_page_data;
    $sys_function_out .= $class_main->clmain_v100_load_html_screen($ps_filename,$s_details_def,$s_details_data,"no",$ps_group."_header_(spaceonerror)");

//        echo "gwetst ".$s_page_def."   data = ".$s_page_data;
    B100_LOAD_FILE_TO_ARRAY:
//20160115gw     ECHO "UTJCL B100 LAD ARRAR A800 - GOOD SQL";

    $s_filename=$ps_filename;
    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_filename);

    $sys_language =$_SESSION['sys_language'];
    if (strtoupper($sys_language)!="ENGLISH")
    {
        $s_filename2 = substr($s_filename,0,strpos($s_filename,"."))."_".$sys_language.substr($s_filename,strpos($s_filename,"."));
        IF (file_exists($s_filename2))
        {
            $s_filename = $s_filename2;
        }
    }

    //print_r($s_returned_details_data);
    IF ($sys_debug == "YES"){echo $sys_function_name."after clmain_v100_load_html_screen<br>";};

    $s_map_file_exists='Y';
    if(file_exists($s_filename))
    {
        $array_map_lines = file($s_filename);
        $s_filename = $array_map_lines;
    }
    else
    {
        $s_map_file_exists='N';
        $s_map_line =  "<br>clmain_v100_load_html_screenE:utjcl_201  ##### File Not Found-:".$s_filename."<br>";
        z901_dump("ERROR", "WB_UT_JCL.PHPE:utjcl_201  ##### File Not Found-:[]".$s_filename."[]");
        return $s_map_line;
        exit();
    }
    B190_END:
//20160115gw     ECHO "<BR>UTJCL B190 - GOOD SQL";
    $s_last_check_time2 = microtime(true);
    $s_dataloop_reccount = 0;
//20160115gw     ECHO "<BR>UTJCL B190-2 - GOOD SQL";

//20160115gw     if(!$result )
//20160115gw     {
//20160115gw         ECHO "<BR>UTJCL B190 - BAD RESULT L";
//20160115gw     }ELSE{
//20160115gw         ECHO "<BR>UTJCL B190 - GOOD SQL";
//20160115gw     }

//20160115gw     ECHO "<BR>UTJCL B190 - RESULT =[]".$result."[]";
//20160115gw     ECHO "<BR>UTJCL B190 - RESULT COUNT =[]".count($result)."[]";
//20160115gw     ECHO "<BR>UTJCL B190 - RESULT ROWS =[]".@mysqli_num_rows( $result )."[]";

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
    {
        IF ($sys_debug2 == "YES"){echo "<br>next record gwutjcl b190 - ".(round(microtime(true) - $s_last_check_time2,15));};
        $s_last_check_time2 = microtime(true);
        $s_rec_found = "YES";
        $s_details_def = $ps_details_def;
        $s_details_data = $ps_details_data;

        $s_dataloop_reccount = $s_dataloop_reccount + 1;

        $_SESSION[$ps_sessionno.'DATALOOP_RECCOUNT'] = $s_dataloop_reccount;

        $s_details_def = $s_details_def."|"."DATALOOP_RECCOUNT";
        $s_details_data = $s_details_data."|".$s_dataloop_reccount;

        $s_table_name = mysqli_fetch_field_direct($result,0)->table;

        $s_temp = $class_main->clmain_u820_fn_Z999_build_def_data($ps_dbcnx,$ps_debug, "ut_jcl_b200dataloop",$row,$s_table_name);
        if (strpos($s_temp,"|^%##%^|") ===false)
        {
        }else{
            $ar_line_details = explode("|^%##%^|",$s_temp);
            $s_details_def = $s_details_def.$ar_line_details[0];
            $s_details_data = $s_details_data.$ar_line_details[1];
            $s_temp = "";
        };

C500_CHECK_DEF:

        $array_def = explode("|",$s_details_def);
        $array_data = explode("|",$s_details_data);

        $skip_start = trim($class_main->clmain_v200_load_line( " |%!_JCL_DATALOOP_START_AT_(ZEROONERROR)_!%| ",$s_details_def,$s_details_data,"no",$ps_sessionno,"cl_proc_jobline  b200a"));
        $skip_down_by = trim($class_main->clmain_v200_load_line( " |%!_JCL_DATALOOP_SKIPDOWN_BY_(ZEROONERROR)_!%| ",$s_details_def,$s_details_data,"no",$ps_sessionno,"cl_proc_jobline  b200b"));

        if (count($array_def)<> count($array_data))
        {
            if(isset ($_SESSION['ko_log_clmain_v300_field_count_errors'])===false) {
                echo "clp_jl Pf_b200_do_dataloop  _ def/data field count error start of list  - def count=".count($array_def)." data count = ".count($array_data)."<br>";
                echo "s_fieldname =".$ps_filename."  <br>";
                echo "s_details_def =".$s_details_def."  <br>";
                echo "s_details_data =".$s_details_data."  <br>";

                $s_count = count($array_def);
                if (count($array_data) > $s_count)
                {
                    $s_count = count($array_data);
                }
                $ar_dd = explode("|",$s_details_def);
                $ar_ddata = explode("|",$s_details_data);
                for ( $i2 = 0; $i2 < $s_count; $i2++)
                {
                    echo "s_details_def ".$i2." =".$ar_dd[$i2]."=data =".$ar_ddata[$i2]."  <br>";
                }

                echo "  clp_jl_Pf_b200_do_dataloop _ def/data field count error end of list <br>";
            }else{
//            echo "<br>process jobline b200 field mismatch " ;
            }
        }ELSE{
//                   echo "clp_jl Pf_b200_do_dataloop _ def/data ALL GOOD <br>";
        }

C590_END:
        IF ($sys_debug2 == "YES"){echo "<br>before load screen gwutjcl b190 - ".(round(microtime(true) - $s_last_check_time,15));};
        $s_last_check_time = microtime(true);
//        $sys_function_out .= $class_main->clmain_v100_load_html_screen($s_filename,$s_details_def,$s_details_data,"no",$ps_group);

        $skip_down = $skip_start + ( $skip_down_by * $s_dataloop_reccount);

//      die("gwdie cl pjl c590 sd=[]".$skip_down."[] st[]".$skip_start."[] sd[]".$skip_down_by."[]");

        $s_details_def = $s_details_def."|"."DATALOOP_SKIPDOWN";
        $s_details_data = $s_details_data."|".$skip_down;

        $sys_function_out .=$class_main->clmain_v100_load_html_screen($s_filename,$s_details_def,$s_details_data,"no",$ps_group);



        IF ($sys_debug2 == "YES"){echo "<br>after load screen  gwutjcl b190 - ".(round(microtime(true) - $s_last_check_time,15));};
        $s_last_check_time = microtime(true);
        C900_WHILE_LOOP:
    }

    IF ($sys_debug2 == "YES"){echo "<br>after c900  gwutjcl b190 - ".(round(microtime(true) - $s_last_check_time,15));};
    $s_last_check_time = microtime(true);

    if ($s_rec_found == "NO")
    {
// gw 20100502         $sys_function_out .= "There is no data for your selection ".$sys_function_name."<br>".$s_sql;
        $sys_function_out .= $class_main->clmain_v100_load_html_screen($ps_filename,$s_details_def,$s_details_data,"no",$ps_group."_nodata");
        if (strpos(strtoupper($sys_function_out),"*ERROR") === false)
        {}else
        {
            $sys_function_out = "There is no data for your selection and the group ".$ps_group."_nodata is not in the map.  ".$sys_function_name."<br>".$s_sql;
        }
    }

    $s_details_def = $ps_details_def."|SYS_RD_NEXT_PREV_ROW|".$s_page_def;
    $s_details_data = $ps_details_data."|".$s_next_prev_row."|".$s_page_data;
    $sys_function_out .= $class_main->clmain_v100_load_html_screen($ps_filename,$s_details_def,$s_details_data,"no",$ps_group."_footer_(spaceonerror)");


    z901_dump($sys_debug, $sys_function_name."  returned  = ".$sys_function_out." ");

    IF ($sys_debug2 == "YES"){echo "<br>end of data loop gwutjcl b190 - ".(round(microtime(true) - $s_last_check_time,15));};
    $s_last_check_time = microtime(true);
    return $sys_function_out;


}
//##########################################################################################################################################################################
    function clp_jl_Pf_b200_do_dataloop_20160811gw($ps_dbcnx,$ps_sql,$ps_filename,$ps_group,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno, $ps_page_current,$ps_page_limit)
    {
        $sys_debug_text = "";
        $sys_debug = "";

        $sys_debug = strtoupper($ps_debug);

        IF ($sys_debug !="NO") {
            $sys_debug  = "YES";
            $sys_debug_text = "<br>".$ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
        }
        $sys_function_name = "";
        $sys_function_name = "debug - clp_jl clp_jl_Pf_b200_do_dataloop";
        $sys_function_out = "";
        IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
        IF ($sys_debug == "YES"){echo( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
        IF ($sys_debug == "YES"){echo( $sys_debug_text."=".$sys_function_name." ps_sql = ".$ps_sql." ps_filename = ".$ps_filename."  ps_group = ".$ps_group."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ");};

        // define function specific variables and code
//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";};

//    $sys_function_out = "before globals";
//    return $sys_function_out;
//    EXIT;

        global $class_sql;
        global $class_main;

        $s_sql = "";
        $result = "";
        $s_rec_found = "";
        $s_details_def = "";
        $s_details_data = "";

        $ar_dd = array();

        $s_sql = $ps_sql;
        $s_details_def = $ps_details_def ;
        $s_details_data = $ps_details_data ;

        $s_sql = $class_main->clmain_v200_load_line( $s_sql,$s_details_def,$s_details_data,"no",$ps_sessionno,"cl_proc_jobline  b200");

//    IF (strpos(strtoupper($s_sql),"|%!") > 0 )
//    {
//    }else{
//        //null
//    }

        $sys_debug2 = "NO";

        $s_page_current = $ps_page_current;
        $s_page_limit = $ps_page_limit;

        $s_sql_start = microtime(true);
        $s_last_check_time = microtime(true);

        IF ($sys_debug2 == "YES"){echo "<br>before exec_query gwutjcl b190 - ".(round(microtime(true) - $s_last_check_time,12));};
        $s_last_check_time = microtime(true);
        $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_sql);
        IF ($sys_debug2 == "YES"){echo "<br>after exec_query gwutjcl b190 - ".(round(microtime(true) - $s_last_check_time,15));};
        $s_last_check_time = microtime(true);
        $s_total_rows = mysql_affected_rows($ps_dbcnx);
        IF ($sys_debug2 == "YES"){echo "<br>after affected rows  gwutjcl b190 - ".(round(microtime(true) - $s_last_check_time,15));};
        $s_last_check_time = microtime(true);


        $s_total_pages = round($s_total_rows/$s_page_limit,0) +1;

        $s_page_next = $s_page_current + 1;
        if ($s_page_next > $s_total_pages)
        {
            $s_page_next = $s_total_pages;
        }
        $s_next_starts_at_row = $s_page_next * $s_page_limit;

        $s_page_prev = $s_page_current -1;
        if ($s_page_prev < 1)
        {
            $s_page_prev = 1 ;
        }
        $s_prev_starts_at_row = $s_page_prev * $s_page_limit;
        $s_current_starts_at_row =  ($s_page_current -1) * $s_page_limit;

        $s_sql = $class_sql->c_sqlclient_add_select_limit($s_sql,$s_page_current,$s_page_limit);

        IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql." ");};
        $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_sql);
        $s_rec_found = "NO";

        $S_sql_end =  microtime(true);
        $s_sql_duration = round($S_sql_end - $s_sql_start,5);
        IF ($sys_debug2 == "YES"){echo "<br>sql duration  gwutjcl b190".$s_sql_duration;};
        $s_last_check_time = microtime(true);

        $s_page_def = "rundatasql_upd_affected_rows|rundatasql_duration|rundatasql_total_rows|rundatasql_page_current|rundatasql_current_starts_at_row|rundatasql_page_limit|rundatasql_total_pages|rundatasql_page_next|rundatasql_next_starts_at_row|rundatasql_page_prev|rundatasql_prev_starts_at_row|rundatasql_sql";
        $s_page_data = mysql_affected_rows($ps_dbcnx)."|".$s_sql_duration." secs|".$s_total_rows."|".$s_page_current."|".$s_current_starts_at_row."|".$s_page_limit."|".$s_total_pages."|".$s_page_next."|".$s_next_starts_at_row."|".$s_page_prev."|".$s_prev_starts_at_row."|".$s_sql;

        $_SESSION['rundata_sql_def'] = $s_page_def;
        $_SESSION['rundata_sql_data'] = $s_page_data;

        $s_next_prev_row = $class_main->clmain_v950_next_prev_row($_SESSION['rundata_sql_def'],"NO","cl_proc_jobline  rundata",$ps_details_def,$ps_details_data,$ps_sessionno);

        $s_details_def = $ps_details_def."|SYS_RD_NEXT_PREV_ROW";
        $s_details_data = $ps_details_data."|".$s_next_prev_row;
        $sys_function_out .= $class_main->clmain_v100_load_html_screen($ps_filename,$s_details_def,$s_details_data,"no",$ps_group."_header_(spaceonerror)");

//        echo "gwetst ".$s_page_def."   data = ".$s_page_data;
        B100_LOAD_FILE_TO_ARRAY:
        $s_filename=$ps_filename;
        $s_filename = $class_main->clmain_set_map_path_n_name("",$s_filename);

// gw 20120218 - add to cater for batch jobs
        if (isset($_SESSION['sys_language']))
        {
//DONOTHING
        }else{
            $_SESSION['sys_language'] = "ENGLISH";
        }

        $sys_language =$_SESSION['sys_language'];
        if (strtoupper($sys_language)!="ENGLISH")
        {
            $s_filename2 = substr($s_filename,0,strpos($s_filename,"."))."_".$sys_language.substr($s_filename,strpos($s_filename,"."));
            IF (file_exists($s_filename2))
            {
                $s_filename = $s_filename2;
            }
        }

        //print_r($s_returned_details_data);
        IF ($sys_debug == "YES"){echo $sys_function_name."after clmain_v100_load_html_screen<br>";};

        $s_map_file_exists='Y';
        if(file_exists($s_filename))
        {
            $array_map_lines = file($s_filename);
            $s_filename = $array_map_lines;
        }
        else
        {
            $s_map_file_exists='N';
            $s_map_line =  "<br>clmain_v100_load_html_screen ##### File Not Found-:".$s_filename."<br>";
            return $s_map_line;
            exit();
        }
        B190_END:
//ECHO "<BR>CLASS_PROCESS_JOBLINE B651 IN Bb190<BR>";

        $s_dataloop_reccount = "0";
        $s_last_check_time2 = microtime(true);
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
        {
//ECHO "<BR>CLASS_PROCESS_JOBLINE B651 IN Bb190 recs <BR>";

            IF ($sys_debug2 == "YES"){echo "<br>next record gwutjcl b190 - ".(round(microtime(true) - $s_last_check_time2,15));};
            $s_last_check_time2 = microtime(true);
            $s_rec_found = "YES";
            $s_details_def = $ps_details_def;
            $s_details_data = $ps_details_data;


            $s_dataloop_reccount = $s_dataloop_reccount + 1;

            $_SESSION[$ps_sessionno.'DATALOOP_RECCOUNT'] = $s_dataloop_reccount;

            $s_details_def = $s_details_def."|dataloop_reccount";
            $s_details_data = $s_details_data."|".$s_dataloop_reccount;

            foreach( $row as $key=>$value)
            {
//ECHO "<BR>CLASS_PROCESS_JOBLINE B651 IN Bb190 fields <BR>";
                if (strtoupper($key) == "DATA_BLOB" )
                {
                    if (strpos(strtoupper($value),"XML") === false)
                    {}else
                    {
                        $ar_xml = $value;
                        $ar_xml = simplexml_load_string($ar_xml);
                        //           var_dump($ar_xml);
                        $newArry = array();
                        $ar_xml = (array) $ar_xml;
                        foreach ($ar_xml as $key => $value)
                        {
                            //$s_details_def .= "|REC_".mysqli_field_table($result,0)."_DB_".$key;
                            $s_table_name = mysqli_fetch_field_direct($result,0)->table;
                            $s_details_def .= "|REC_".$s_table_name."_DB_".$key;
                            $s_details_data .= "|".$class_main->clmain_u596_xml_decode_char($value);
                        }
                    }
                    continue;
                }
                if (strpos(strtoupper($key),"DETAILS_D") === FALSE)
                {
//GW20131209 - added json - moved to below json process
                    GOTO C120_SKIP_DATA_BLOB;
                }
                if (strtoupper($key) == "DETAILS_DEF" )
                {
//               $s_details_def .= "|DD_".$key;
                    $ar_dd = explode("|",$value);
                    for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
                    {
                        $s_details_def .= "|DD_".$ar_dd[$i2];
                    }
                    continue;
                }
                if (strtoupper($key) == "DETAILS_DATA" )
                {
//               $s_details_data .= "|DD_".$value;
                    $ar_dd = explode("|",$value);
                    for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
                    {
                        $s_details_data .= "|".$ar_dd[$i2];
                    }
                    continue;
                }

                C120_SKIP_DATA_BLOB:
                //gw20131209 - copied from class_ujp_process_events.php
                B651_SKIP_DATA_BLOB:
//ECHO "<BR>CLASS_PROCESS_JOBLINE B651 IN B200<BR>";
                $ps_def_prefix = "B200DL";
                $s_fieldvalue = "";
                // if the first char is not { cant be json
                if (substr($value,0,2) <> '{"')
                {
                    goto B652_SKIP_JSON;
                }
                // if the last char is not } cant be json
                if ($value[strlen($value)-1] <> "}")
                {
                    goto B652_SKIP_JSON;
                }
                if ($value[strlen($value)-2] <> "}")
                {
                    goto B652_SKIP_JSON;
                }
                B652_A_DO_JSON:
                $dictionary = json_decode($value, TRUE);
                /*    if ($dictionary === NULL)
                    {
                            goto B652_SKIP_JSON;
                    }
                    echo $dictionary['fieldname'];
                */
                //echo "<br> raw  value";
                // print_r($value);

                //echo "<br> json dictionary values";
                //print_r(array_keys($dictionary));

                $s_json_data_csv_data = "";
                $s_json_data_csv_headings = "";
                foreach ($dictionary as $key1 => $value1)
                {
                    $s_fieldname = $key1;
                    $s_fieldvalue = " ";
                    if (is_array($value1))
                    {
//    echo "<br>******* keys s_fieldname=[]".$s_fieldname."[]****************************************<br>";
//                     print_r(array_keys($value1));
//    echo "<br>******* values s_fieldname=[]".$s_fieldname."[]****************************************<br>";
//                     print_r(array_values($value1));
//    echo "<br>***********************************************";

//            $s_temp = implode("+",$value1);
//    echo "<br> s_temp = []".$s_temp."[]";
//            $s_temp = "+".$s_temp;
//    echo "<br> s_temp2 = []".$s_temp."[]";

                        if (in_array('Value', $value1)) {
//gw20131209 - the in_array would not work for the AdditionalTaskslist field which was an array itself with no value selected
                            if (isset($value1['Value']))
                            {
//        echo "<br> no value field ******** ";
                                $s_fieldvalue = $value1['Value'];
//        echo "<br> found value 1 keys";
//                         print_r(array_keys($value1));
//        echo "<br> class_process_jboline dd found value 1 values";
//                         print_r(array_values($value1));
                                GOTO B652_C_MAKE_DEF_DATA;
                            }
                        }
                        //echo "<br> ";
                        //                print_r(" there is no value in the json for field s_fieldname=[]".$s_fieldname."[]  value1=[]".$value1."[]");
                        //echo "<br> value 1 keys";
                        //                 print_r(array_keys($value1));
                        //echo "<br> value 1 values";
                        //                 print_r(array_values($value1));
                        //echo "<br> array count  value1= []".count($value1)."[] ";
                        foreach ($value1 as $key2 => $value2)
                        {
                            IF (strtoupper($key2) <> "VALUE")
                            {
                                GOTO B652_B_SKIP_VALUE;
                            }
                            if (is_array($value2))
                            {
                                foreach ($value2 as $key3 => $value3)
                                {
                                    $s_fieldname = $key1."_".$key3;
                                    $s_fieldvalue = $value2[$key3];
                                    $s_details_def .= "|REC_".$ps_def_prefix."_DB_".$key."_".str_replace(" ","_",trim($s_fieldname));
                                    $s_details_data .= "|".trim($s_fieldvalue);
                                    $s_json_data_csv_data = $s_json_data_csv_data.str_replace(","," ",$s_fieldvalue).",";
                                    $s_json_data_csv_headings = $s_json_data_csv_headings.str_replace("_Value","",(str_replace(" ","_",trim($s_fieldname)))).",";
                                }
                                goto B652_X_NEXT_FIELD;
                            }else{
                                $s_fieldname = $key1."_".$key2;
                                $s_fieldvalue = $value1[$key2];
                            }
                            B652_B_SKIP_VALUE:
                            //echo "<br> s_fieldname = []".$s_fieldname."[] ";
                            //echo "<br> s_fieldvalue = []".$s_fieldvalue."[]";
                            B652_B_NEXT_FIELD:
                        }
                    }else{
                        $s_fieldvalue = $value1;
                    }
                    B652_C_MAKE_DEF_DATA:
                    $s_details_def .= "|REC_".$ps_def_prefix."_DB_".$key."_".str_replace(" ","_",trim($s_fieldname));

                    if(is_array($s_fieldvalue))
                    {
                        $s_fieldvalue = implode("+",$s_fieldvalue);
//    echo "<br> array value s_fieldname = []".$s_fieldname."[]  s_fieldvalue=[] ".$s_fieldvalue."[]";
//    print_r("<br> s_fieldvalue = []".$s_fieldvalue."[]");
                    }
                    $s_details_data .= "|".trim($s_fieldvalue);


                    $s_json_data_csv_data = $s_json_data_csv_data.str_replace(","," ",$s_fieldvalue).",";
                    $s_json_data_csv_headings = $s_json_data_csv_headings.str_replace("_Value","",(str_replace(" ","_",trim($s_fieldname)))).",";

                    B652_X_NEXT_FIELD:
                }
                //echo "<br> json end def";
                //                 print_r($s_details_def);
                //echo "<br> json end data";
                //                 print_r($s_details_data);
                $s_details_def .= "|REC_".$ps_def_prefix."_DB_csv_headings|REC_".$ps_def_prefix."_DB_csv_data";
                $s_details_data .= "|".trim($s_json_data_csv_headings)."|".trim($s_json_data_csv_data);

                B652_SKIP_JSON:

//GW20131209 - added json - moved to below json process

//                $s_details_def .= "|REC_".my_sql_field_table($result,0)."_".$key;
                //$s_details_def .= "|REC_".mysqli_field_table($result,0)."_".$key;
                $s_table_name = mysqli_fetch_field_direct($result,0)->table;
                $s_details_def .= "|REC_".$s_table_name."_".$key;
                IF (STRPOS($value,"|") === false)
                {
                    $s_details_data .= "|".$value;
                }else{
                    $s_details_data .= "|".str_replace("|","#*pipe*#",$value);
                    $array_data = explode("|",$value);
//                    echo "cl_proc_jobline  b200 - there is a pipe in the record data for def |REC_".mysqli_field_table($result,0)."_".$key." value=".$value."<br>";
                    for ( $i2 = 0; $i2 < count($array_data); $i2++)
                    {
                        //$s_details_def .= "|REC_".mysqli_field_table($result,0)."_".$key."_".$i2;
                        $s_table_name = mysqli_fetch_field_direct($result,0)->table;
                        $s_details_def .= "|REC_".$s_table_name."_".$key."_".$i2;
                        $s_details_data .= "|".$array_data[$i2];
                    }
                }

                continue;


                C600_FOR_EACH_END:
            }
            C500_CHECK_DEF:

            $array_def = explode("|",$s_details_def);
            $array_data = explode("|",$s_details_data);

            if (count($array_def)<> count($array_data))
            {
                if(isset ($_SESSION['ko_log_clmain_v300_field_count_errors'])===false) {
                    echo "clmain clp_jl_Pf_b200_do_dataloop  _ def/data field count error start of list  - def count=".count($array_def)." data count = ".count($array_data)."<br>";
                    echo "s_fieldname =".$s_fieldname."  <br>";
                    echo "s_details_def =".$s_details_def."  <br>";
                    echo "s_details_data =".$s_details_data."  <br>";

                    $s_count = count($array_def);
                    if (count($array_data) > $s_count)
                    {
                        $s_count = count($array_data);
                    }
                    $ar_dd = explode("|",$s_details_def);
                    $ar_ddata = explode("|",$s_details_data);
                    for ( $i2 = 0; $i2 < $s_count; $i2++)
                    {
                        echo "s_details_def ".$i2." =".$ar_dd[$i2]."=data =".$ar_ddata[$i2]."  <br>";
                    }

                    echo "clprocess_jobline clp_jl_Pf_b200_do_dataloop _ def/data field count error end of list <br>";
                }else{
//            echo "<br>process jobline b200 field mismatch " ;
                }
            }ELSE{
//                   echo "clmain clp_jl_Pf_b200_do_dataloop _ def/data ALL GOOD <br>";
            }

            C590_END:
            IF ($sys_debug2 == "YES"){echo "<br>before load screen gwutjcl b190 - ".(round(microtime(true) - $s_last_check_time,15));};
            $s_last_check_time = microtime(true);
            $sys_function_out .= $class_main->clmain_v100_load_html_screen($s_filename,$s_details_def,$s_details_data,"no",$ps_group);

            IF ($sys_debug2 == "YES"){echo "<br>after load screen  gwutjcl b190 - ".(round(microtime(true) - $s_last_check_time,15));};
            $s_last_check_time = microtime(true);
            C900_WHILE_LOOP:
        }

        IF ($sys_debug2 == "YES"){echo "<br>after c900  gwutjcl b190 - ".(round(microtime(true) - $s_last_check_time,15));};
        $s_last_check_time = microtime(true);


        if ($s_rec_found == "NO")
        {
// gw 20100502         $sys_function_out .= "There is no data for your selection ".$sys_function_name."<br>".$s_sql;
            $sys_function_out .= $class_main->clmain_v100_load_html_screen($ps_filename,$s_details_def,$s_details_data,"no",$ps_group."_nodata");
            if (strpos(strtoupper($sys_function_out),"*ERROR") === false)
            {}else
            {
                $sys_function_out = "There is no data for your selection and the group ".$ps_group."_nodata is not in the map.  ".$sys_function_name."<br>".$s_sql;
            }
        }

        $s_details_def = $ps_details_def."|SYS_RD_NEXT_PREV_ROW";
        $s_details_data = $ps_details_data."|".$s_next_prev_row;
        $sys_function_out .= $class_main->clmain_v100_load_html_screen($ps_filename,$s_details_def,$s_details_data,"no",$ps_group."_footer_(spaceonerror)");


        IF ($sys_debug == "YES"){z901_dump( $sys_function_name."  returned  = ".$sys_function_out." ");};

        IF ($sys_debug2 == "YES"){echo "<br>end of data loop gwutjcl b190 - ".(round(microtime(true) - $s_last_check_time,15));};
        $s_last_check_time = microtime(true);
        return $sys_function_out;

    }
//##########################################################################################################################################################################
//##########################################################################################################################################################################
// B400, B410 AND B420 are very, very, very similar.
function cl_pjl_Pf_b400_do_multilevelloop($ps_dbcnx,$ps_MultiLevelloop1,$ps_MultiLevelloop2,$ps_MultiLevelloop3,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_calledfrom,$ps_ko_map_path)
{
// Update checklist
// if you want to copy b400 down then after you copy do the following
// a. remove all reference to multileveloop1
// b. change all mll1 to mll2
// c. change all _b400_ to _b410_
// d. c

    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - cl_pjl_Pf_b400_do_multilevelloop called from ".$ps_calledfrom." ";
    $sys_function_out = "";

    $s_skip_error = "";
    $s_skip_error = "NO";

    IF ($sys_debug == "YES"){echo "<br>".$sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump("Parameters= s_MultiLevelloop1,s_MultiLevelloop2,s_MultiLevelloop3,ps_debug,s_details_def,s_details_data,s_sessionno,ps_calledfrom");};
    IF ($sys_debug == "YES"){z901_dump("Values= ".$ps_MultiLevelloop1.",".$ps_MultiLevelloop2.",".$ps_MultiLevelloop3.",".$ps_debug.",".$ps_details_def.",".$ps_details_data.",".$ps_sessionno.",".$ps_calledfrom);};
    // define function specific variables and code
    //EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";};

    IF ($ps_MultiLevelloop1 == "NONE")
    {
        IF ($ps_MultiLevelloop2 == "NONE")
        {
            IF ($ps_MultiLevelloop3 == "NONE")
            {
                $sys_function_out .= "cl_pj_e2009 - There is no multilevel loops defined ".$sys_function_name;
                z901_dump($sys_function_out);
                GOTO Z900_EXIT;
            }
        }
    }

    IF ($ps_MultiLevelloop1 == "NONE")
    {
        IF ($ps_MultiLevelloop2 <> "NONE")
        {
                $sys_function_out .= "cl_pj_e2009 - There is a 2nd level multilevel without a first level ".$sys_function_name;
                z901_dump($sys_function_out);
                GOTO Z900_EXIT;
        }
    }


    IF ($sys_debug == "YES"){z901_dump("got past the multilevel loop test");};

    global $class_sql;
    global $class_main;


    //DECLARE
    $s_sql = "";
    $result = "";
    $s_rec_found = "";
    $s_details_def = "";
    $s_details_data = "";
    $s_reccount = "";
    $s_numrows = "";
    $s_field_value = "";
    $row = array();
    $ar_dd = array();
    $ar_line_details = array();
    $ar_keys = array_keys($row);
    $ar_values = array_values($row);
    $key = "";
    $value = "";

    $s_mll_preloop_map = "";
    $s_mll_preloop_group = "";
    $s_mll_postloop_map = "";
    $s_mll_postloop_group = "";
    $s_mll_tally1_field = "";
    $s_mll_tally1_value = "";
    $s_mll_tally2_field = "";
    $s_mll_tally2_value = "";
    $s_mll_tally3_field = "";
    $s_mll_tally3_value = "";
    $s_mll_tally4_field = "";
    $s_mll_tally4_value = "";
    $s_mll_tally5_field = "";
    $s_mll_tally5_value = "";

//INITIALISE
    $s_details_def = $ps_details_def ;
    $s_details_data = $ps_details_data ;


    $ar_line_details = explode("|",$ps_MultiLevelloop1);
    $s_mll_preloop_map = strtoupper(TRIM($ar_line_details[1],""));
    $s_mll_preloop_group = TRIM($ar_line_details[2],"");
    $s_mll_postloop_map = strtoupper(TRIM($ar_line_details[3],""));
    $s_mll_postloop_group = TRIM($ar_line_details[4],"");
    $s_sql = TRIM($ar_line_details[5],"");
    $s_mll_tally1_field = TRIM($ar_line_details[6],"");
    $s_mll_tally2_field = TRIM($ar_line_details[7],"");
    $s_mll_tally3_field = TRIM($ar_line_details[8],"");
    $s_mll_tally4_field = TRIM($ar_line_details[9],"");
    $s_mll_tally5_field = TRIM($ar_line_details[10],"");
    $s_reccount = "0";
    $s_mll_tally1_value = "0";
    $s_mll_tally2_value = "0";
    $s_mll_tally3_value = "0";
    $s_mll_tally4_value = "0";
    $s_mll_tally5_value = "0";

    if (strpos(strtoupper($s_sql),"SKIP_ERROR") === FALSE )
    {    }else
    {
        $s_skip_error = "YES";
        $s_sql = str_replace("SKIP_ERROR"," ",$s_sql);
        $s_sql = str_replace("skip_error"," ",$s_sql);
    }
    if (strtoupper(substr($s_sql,0,9)) =="*NOERROR*")
    {
        $s_skip_error = "YES";
        $s_sql = substr($s_sql,9);
    }


    $s_sql = str_replace("#p","|",$s_sql);
    $s_sql = str_replace("#P","|",$s_sql);


    $s_sql = $class_main->clmain_v200_load_line( $s_sql,$s_details_def,$s_details_data,"no",$ps_sessionno,"cl_proc_jobline  b400");
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql." ");};


C000_RECORD_LOOP:
    $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_sql);
    $s_rec_found = "NO";
    $s_numrows = mysqli_num_rows($result);

C100_DO_RECORDS:
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    IF ($row === false )
    {
        goto C900_END_RECORDS;
    }
    if ($s_numrows==0)
    {
        goto C900_END_RECORDS;
    }
    if (empty($row))
    {
        goto C900_END_RECORDS;
    }

    $s_details_def_end = "NO";
    $s_rec_found = "YES";
    $s_reccount = $s_reccount + 1;
    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name."   in while loops = recs found");};


    $i_c150 = 0;
    $ar_keys = array_keys($row);
    $ar_values = array_values($row);
    //$s_table_name = mysqli_field_table($result,0);
    $s_table_name = mysqli_fetch_field_direct($result,0)->table;
//    PRINT_R(mysqli_field_table($result,0));
    //    print_r($row);

    $s_table_name = mysqli_fetch_field_direct($result,0)->table;
    $s_temp = $class_main->clmain_u820_fn_Z999_build_def_data($ps_dbcnx,$ps_debug, "cl_pjl b410",$row,$s_table_name);
    if (strpos($s_temp,"|^%##%^|") ===false)
    {
    }else{
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def.$ar_line_details[0];
        $s_details_data = $s_details_data.$ar_line_details[1];
        $s_temp = "";
    };

    /* 20160809gw

    C150_BUILD_DATA:
        if ($i_c150 == count($row))
        {
            goto C159_END;
        }
        if ($i_c150 > count($row))
        {
            goto C159_END;
        }
        $s_details_def = $ps_details_def;
        $s_details_data = $ps_details_data;
    //     z901_dump( "field ".$i_c150." count row=".count($row)." key = ".$key. " value = ".$value." count keys=".count($ar_keys)." count values = ".count($ar_values));
        //    print_r($row);

        $key = $ar_keys[$i_c150];
        $value = $ar_values[$i_c150];
    //            echo "<br> gw 300setrun<br>";
        if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
        {
            GOTO C152_SKIP_DATA_BLOB;
        }
        $ar_xml = simplexml_load_string($value);
    //           var_dump($ar_xml);
        $newArry = array();
        $ar_xml = (array) $ar_xml;
        foreach ($ar_xml as $key => $value)
        {
            //$s_details_def .= "|REC_".mysqli_field_table($result,0)."_DB_".$key;
            $s_table_name = mysqli_fetch_field_direct($result,0)->table;
            $s_details_def .= "|REC_".$s_table_name."_DB_".$key;
            $s_details_data .= "|".$class_main->clmain_u596_xml_decode_char($value);
        }
        GOTO C158_NEXT;

    C152_SKIP_DATA_BLOB:
        if (strpos(strtoupper($key),"DETAILS_D") === FALSE)
        {
            $s_details_def .= "|REC_".$s_table_name."_".$key;
            $s_details_data .= "|".$value;
            GOTO C158_NEXT;
        }
        if (strtoupper($key) == "DETAILS_DEF" )
        {
           $ar_dd = explode("|",$value);
            $s_details_def .= "|mmloopversion";
           for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
           {
               IF (strtoupper($ar_dd[$i2]) <> "END")
               {
                   $s_details_def .= "|DD_".$s_table_name.$ar_dd[$i2];
               }
           }
            GOTO C158_NEXT;
        }
        if (strtoupper($key) == "DETAILS_DATA" )
        {
            $s_details_data .= "|mmloopv1";
           $ar_dd = explode("|",$value);
           for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
           {
               IF (strtoupper($ar_dd[$i2]) <> "END")
               {
                   $s_details_data .= "|".$ar_dd[$i2];
               }
            }
            GOTO C158_NEXT;
        }
C158_NEXT:
    $i_c150 = $i_c150 + 1;
    GOTO C150_BUILD_DATA;
C159_END:
    */

    $s_details_def .= "|MLL1_RECCOUNT|MLL1_NUMROWS|";
    $s_details_data .= "|".$s_reccount."|".$s_numrows."|";
C200_DO_TALLY:
    if (strtoupper($s_mll_tally1_field) != "NONE")
    {
        $s_field_value = $class_main->clmain_v300_set_variable( "|%!_".$s_mll_tally1_field."_!%|",$s_details_def,$s_details_data,"no",$ps_sessionno,"cl_proc_jobline  b400_T1" );
        $s_mll_tally1_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally1_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"cl_proc_jobline  b400_T1");
    }
    if (strtoupper($s_mll_tally2_field) != "NONE")
    {
        $s_field_value = $class_main->clmain_v300_set_variable( "%!_".$s_mll_tally2_field."_!%",$s_details_def,$s_details_data,"no",$ps_sessionno,"cl_proc_jobline  b400_T2" );
        $s_mll_tally2_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally2_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"cl_proc_jobline  b400_T2");
    }
    if (strtoupper($s_mll_tally3_field) != "NONE")
    {
        $s_field_value = $class_main->clmain_v300_set_variable( "%!_".$s_mll_tally3_field."_!%",$s_details_def,$s_details_data,"no",$ps_sessionno,"cl_proc_jobline  b400 t3" );
        $s_mll_tally3_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally3_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"cl_proc_jobline  b400_T3");
    }
    if (strtoupper($s_mll_tally4_field) != "NONE")
    {
        $s_field_value = $class_main->clmain_v300_set_variable( "%!_".$s_mll_tally4_field."_!%",$s_details_def,$s_details_data,"no",$ps_sessionno,"cl_proc_jobline  b400 t4" );
        $s_mll_tally4_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally4_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"cl_proc_jobline  b400_T4");
    }

    if (strtoupper($s_mll_tally5_field) != "NONE")
    {
        $s_field_value = $class_main->clmain_v300_set_variable( "%!_".$s_mll_tally5_field."_!%",$s_details_def,$s_details_data,"no",$ps_sessionno,"cl_proc_jobline  b400 t5" );
        $s_mll_tally5_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally5_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"cl_proc_jobline  b400_T5");
    }

    $s_details_def .= "|MLL1_TALLY1|MLL1_TALLY2|MLL1_TALLY3|MLL1_TALLY4|MLL1_TALLY5|";
    $s_details_data .= "|".$s_mll_tally1_value."|".$s_mll_tally2_value."|".$s_mll_tally3_value."|".$s_mll_tally4_value."|".$s_mll_tally5_value."|";
C299_END_DO_TALLY:

    if ($s_mll_preloop_map!="NONE"){
        $sys_function_out .= $class_main->clmain_v100_load_html_screen($class_main->clmain_set_map_path_n_name($ps_ko_map_path,$s_mll_preloop_map),$s_details_def,$s_details_data,"no",$s_mll_preloop_group);
    }
C300_DO_NEXT_LEVEL:
    $sys_function_out .= $this->cl_pjl_Pf_b410_do_multilevelloop2($ps_dbcnx,$ps_MultiLevelloop2,$ps_MultiLevelloop3,"NO",$s_details_def,$s_details_data,$ps_sessionno,"cl_proc_jobline  B410_mll2",$ps_ko_map_path);
//at level 2        $sys_function_out .= cl_pjl_Pf_b420_do_multilevelloop3($ps_dbcnx,$ps_MultiLevelloop3,"NO",$s_details_def,$s_details_data,$ps_sessionno,"cl_proc_jobline  b400_mll2");
// at level 3  nothing happes

    if ($s_mll_postloop_map!="NONE"){
        $sys_function_out .= $class_main->clmain_v100_load_html_screen($class_main->clmain_set_map_path_n_name($ps_ko_map_path,$s_mll_postloop_map),$s_details_def,$s_details_data,"no",$s_mll_postloop_group);
    }
C800_NEXT_REC:
    GOTO C100_DO_RECORDS;



C900_END_RECORDS:
    if ($s_rec_found == "NO")
    {
        if ($s_skip_error == "YES")
        {
            $sys_function_out .= "";
        }else
        {
//gw20101105 - replace with nodata loop            $sys_function_out .= "<br>There is no data in ".$sys_function_name."for ".$s_sql;
            $sys_function_out .= $class_main->clmain_v100_load_html_screen($s_mll_preloop_map,$s_details_def,$s_details_data,"no",$s_mll_preloop_group."_nodata");
            if (strpos(strtoupper($sys_function_out),"*ERROR") === false)
            {}else
            {
                $sys_function_out = "There is no data for your selection and the group ".$s_mll_preloop_group."_nodata is not in the map.  ".$s_mll_preloop_map."<br>".$s_sql;
            }
        }
    }

    IF ($sys_debug == "YES"){z901_dump( $sys_function_name."  returned  = ".$sys_function_out." ");};
Z900_EXIT:
    return $sys_function_out;
}
//##########################################################################################################################################################################
function cl_pjl_Pf_b410_do_multilevelloop2($ps_dbcnx,$ps_MultiLevelloop2,$ps_MultiLevelloop3,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_calledfrom,$ps_ko_map_path)
{
// Update checklist
// if you want to copy b400 down then after you copy do the following
// a. remove all reference to multileveloop1
// b. change all mll1 to mll2
// c. change all _b400_ to _b410_

    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }


    if (strpos(strtoupper( $ps_MultiLevelloop2),"**DODEBUG**") === false )
    {}else
    {
        $sys_debug  = "YES";
        $sys_debug_text = "Inline debug";
        $ps_MultiLevelloop2 = str_replace("**dodebug**","",$ps_MultiLevelloop2);
        $ps_MultiLevelloop2 = str_replace("**DODEBUG**","",$ps_MultiLevelloop2);
        $ps_MultiLevelloop2 = str_replace("**DoDebug**","",$ps_MultiLevelloop2);
    }

    $sys_function_name = "";
    $sys_function_name = "debug - Pf_B410_do_multilevelloop called from ".$ps_calledfrom." ";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump("Parameters= s_MultiLevelloop2,s_MultiLevelloop3,ps_debug,s_details_def,s_details_data,s_sessionno,ps_calledfrom");};
    IF ($sys_debug == "YES"){z901_dump("Values= ".$ps_MultiLevelloop2.",".$ps_MultiLevelloop3.",".$ps_debug.",".$ps_details_def.",".$ps_details_data.",".$ps_sessionno.",".$ps_calledfrom);};
    // define function specific variables and code
    //EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";};

        IF ($ps_MultiLevelloop2 == "NONE")
        {
            IF ($ps_MultiLevelloop3 == "NONE")
            {
                $sys_function_out .= "cl_pj_e2009 - There is no multilevel loops defined ".$sys_function_name."<br>";
                z901_dump($sys_function_out);
                GOTO Z900_EXIT;
            }
        }

    IF ($sys_debug == "YES"){z901_dump("got past the multilevel loop test");};

    global $class_sql;
    global $class_main;
//DECLARE
    $s_sql = "";
    $result = "";
    $s_rec_found = "";
    $s_details_def = "";
    $s_details_data = "";
    $s_reccount = "";
    $s_numrows = "";
    $s_field_value = "";
    $row = array();
    $ar_dd = array();
    $ar_line_details = array();
    $ar_keys = array_keys($row);
    $ar_values = array_values($row);
    $key = "";
    $value = "";

    $s_mll_preloop_map = "";
    $s_mll_preloop_group = "";
    $s_mll_postloop_map = "";
    $s_mll_postloop_group = "";
    $s_mll_tally1_field = "";
    $s_mll_tally1_value = "";
    $s_mll_tally2_field = "";
    $s_mll_tally2_value = "";
    $s_mll_tally3_field = "";
    $s_mll_tally3_value = "";
    $s_mll_tally4_field = "";
    $s_mll_tally4_value = "";
    $s_mll_tally5_field = "";
    $s_mll_tally5_value = "";

//INITIALISE
    $s_details_def = $ps_details_def ;
    $s_details_data = $ps_details_data ;

    $ar_line_details = explode("|",$ps_MultiLevelloop2);
    $s_mll_preloop_map = strtoupper(TRIM($ar_line_details[1],""));
    $s_mll_preloop_group = TRIM($ar_line_details[2],"");
    $s_mll_postloop_map = strtoupper(TRIM($ar_line_details[3],""));
    $s_mll_postloop_group = TRIM($ar_line_details[4],"");
    $s_sql = TRIM($ar_line_details[5],"");
    $s_mll_tally1_field = TRIM($ar_line_details[6],"");
    $s_mll_tally2_field = TRIM($ar_line_details[7],"");
    $s_mll_tally3_field = TRIM($ar_line_details[8],"");
    $s_mll_tally4_field = TRIM($ar_line_details[9],"");
    $s_mll_tally5_field = TRIM($ar_line_details[10],"");
    $s_reccount = "0";
    $s_mll_tally1_value = "0";
    $s_mll_tally2_value = "0";
    $s_mll_tally3_value = "0";
    $s_mll_tally4_value = "0";
    $s_mll_tally5_value = "0";

    $s_sql_noerror = "";
    $s_sql_noerror = "NO";
    if (strtoupper(substr($s_sql,0,9)) =="*NOERROR*")
    {
        $s_sql_noerror = "YES";
        $s_sql = substr($s_sql,9);
    }

    $s_sql = str_replace("#p","|",$s_sql);
    $s_sql = str_replace("#P","|",$s_sql);
    $s_sql = $class_main->clmain_v200_load_line( $s_sql,$s_details_def,$s_details_data,"no",$ps_sessionno,"cl_proc_jobline  b400");

    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql." ");};


C000_RECORD_LOOP:
    $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_sql);
    $s_rec_found = "NO";
    $s_numrows = mysqli_num_rows($result);

C100_DO_RECORDS:
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    IF ($row === false )
    {
        goto C900_END_RECORDS;
    }
    if ($s_numrows==0)
    {
        goto C900_END_RECORDS;
    }
    if (empty($row))
    {
        goto C900_END_RECORDS;
    }

    $s_rec_found = "YES";
    $s_reccount = $s_reccount + 1;
    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name."   in while loops = recs found");};


    $i_c150 = 0;
    $ar_keys = array_keys($row);
    $ar_values = array_values($row);

    $s_table_name = mysqli_fetch_field_direct($result,0)->table;
    $s_temp = $class_main->clmain_u820_fn_Z999_build_def_data($ps_dbcnx,$ps_debug, "cl_pjl b410",$row,$s_table_name);
    if (strpos($s_temp,"|^%##%^|") ===false)
    {
    }else{
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def.$ar_line_details[0];
        $s_details_data = $s_details_data.$ar_line_details[1];
        $s_temp = "";
    };
/*

    //    PRINT_R(mysqli_field_table($result,0));
        //    print_r($row);
    C150_BUILD_DATA:
        if ($i_c150 == count($row))
        {
            goto C159_END;
        }
        $key = $ar_keys[$i_c150];
        $value = $ar_values[$i_c150];

    //     z901_dump( "field ".$i_c150." count row=".count($row)." key = ".$key. " value = ".$value." count keys=".count($ar_keys)." count values = ".count($ar_values));
        //    print_r($row);
    //            echo "<br> gw 300setrun<br>";
        if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
        {
            GOTO C152_SKIP_DATA_BLOB;
        }
        $ar_xml = simplexml_load_string($value);
    //           var_dump($ar_xml);
        $newArry = array();
        $ar_xml = (array) $ar_xml;
        foreach ($ar_xml as $key => $value)
        {
            //$s_details_def .= "|REC_".mysqli_field_table($result,0)."_DB_".$key;
            $s_table_name = mysqli_fetch_field_direct($result,0)->table;
            $s_details_def .= "|REC_".$s_table_name."_DB_".$key;
            $s_details_data .= "|".$class_main->clmain_u596_xml_decode_char($value);
        }
        GOTO C158_NEXT;

    C152_SKIP_DATA_BLOB:
        if (strpos(strtoupper($key),"DETAILS_D") === FALSE)
        {
            IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name."  found a field = "."|REC_".$s_table_name."_".$key." with value ".$value);};

            $s_details_def .= "|REC_".$s_table_name."_".$key;
            $s_details_data .= "|".$value;
            GOTO C158_NEXT;
        }
        if (strtoupper($key) == "DETAILS_DEF" )
        {
           $ar_dd = explode("|",$value);
           for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
           {
               IF (strtoupper($ar_dd[$i2]) <> "END")
               {
                   $s_details_def .= "|DD_".$s_table_name.$ar_dd[$i2];
               }
            }
            GOTO C158_NEXT;
        }
        if (strtoupper($key) == "DETAILS_DATA" )
        {
           $ar_dd = explode("|",$value);
           for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
           {
               IF (strtoupper($ar_dd[$i2]) <> "END")
               {
                   $s_details_data .= "|".$ar_dd[$i2];
               }
            }
            GOTO C158_NEXT;
        }
C158_NEXT:
    $i_c150 = $i_c150 + 1;
    GOTO C150_BUILD_DATA;
C159_END:
    */

    $s_details_def .= "|MLL2_RECCOUNT|MLL2_NUMROWS|";
    $s_details_data .= "|".$s_reccount."|".$s_numrows."|";
C200_DO_TALLY:
    if (strtoupper($s_mll_tally1_field) != "NONE")
    {
        $s_field_value = $class_main->clmain_v300_set_variable( "|%!_".$s_mll_tally1_field."_!%|",$s_details_def,$s_details_data,"no",$ps_sessionno,"cl_proc_jobline  B410_T1" );
        $s_mll_tally1_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally1_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"cl_proc_jobline  B410_T1");
    }
    if (strtoupper($s_mll_tally2_field) != "NONE")
    {
        $s_field_value = $class_main->clmain_v300_set_variable( "%!_".$s_mll_tally2_field."_!%",$s_details_def,$s_details_data,"no",$ps_sessionno,"cl_proc_jobline  B410_T2" );
        $s_mll_tally2_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally2_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"cl_proc_jobline  B410_T2");
    }
    if (strtoupper($s_mll_tally3_field) != "NONE")
    {
        $s_field_value = $class_main->clmain_v300_set_variable( "%!_".$s_mll_tally3_field."_!%",$s_details_def,$s_details_data,"no",$ps_sessionno,"cl_proc_jobline  b400 t3" );
        $s_mll_tally3_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally3_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"cl_proc_jobline  B410_T3");
    }
    if (strtoupper($s_mll_tally4_field) != "NONE")
    {
        $s_field_value = $class_main->clmain_v300_set_variable( "%!_".$s_mll_tally4_field."_!%",$s_details_def,$s_details_data,"no",$ps_sessionno,"cl_proc_jobline  b400 t4" );
        $s_mll_tally4_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally4_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"cl_proc_jobline  B410_T4");
    }

    if (strtoupper($s_mll_tally5_field) != "NONE")
    {
        $s_field_value = $class_main->clmain_v300_set_variable( "%!_".$s_mll_tally5_field."_!%",$s_details_def,$s_details_data,"no",$ps_sessionno,"cl_proc_jobline  b400 t5" );
        $s_mll_tally5_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally5_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"cl_proc_jobline  B410_T5");
    }

    $s_details_def .= "|MLL2_TALLY1|MLL2_TALLY2|MLL2_TALLY3|MLL2_TALLY4|MLL2_TALLY5|";
    $s_details_data .= "|".$s_mll_tally1_value."|".$s_mll_tally2_value."|".$s_mll_tally3_value."|".$s_mll_tally4_value."|".$s_mll_tally5_value."|";
C299_END_DO_TALLY:

    if ($s_mll_preloop_map!="NONE"){
        $sys_function_out .= $class_main->clmain_v100_load_html_screen($class_main->clmain_set_map_path_n_name($ps_ko_map_path,$s_mll_preloop_map),$s_details_def,$s_details_data,"no",$s_mll_preloop_group);
    }
C300_DO_NEXT_LEVEL:
//at level 1    $sys_function_out .= cl_pjl_Pf_b410_do_multilevelloop2($ps_dbcnx,$ps_MultiLevelloop2,$ps_MultiLevelloop3,"NO",$s_details_def,$s_details_data,$ps_sessionno,"cl_proc_jobline  B410_mll2");
//at level 2

//// gw 20100420 - added the only do if not none
    IF ($ps_MultiLevelloop3 <> "NONE")
    {
        $sys_function_out .= $this->cl_pjl_Pf_b420_do_multilevelloop3($ps_dbcnx,$ps_MultiLevelloop3,"NO",$s_details_def,$s_details_data,$ps_sessionno,"cl_proc_jobline  b410_mll2",$ps_ko_map_path);
    }
// gw20100911 - added the eolonly process
    if ($s_mll_postloop_map!="NONE"){
        if ( strpos(strtoupper($s_mll_postloop_group),"EOLONLY") === false)
        {

//        die("gwdied it loop map = ".$s_mll_postloop_map);
            $sys_function_out .= $class_main->clmain_v100_load_html_screen($class_main->clmain_set_map_path_n_name($ps_ko_map_path,$s_mll_postloop_map),$s_details_def,$s_details_data,"no",$s_mll_postloop_group);
        }
    }
C800_NEXT_REC:
    GOTO C100_DO_RECORDS;

C900_END_RECORDS:
//gw120100911 - this may need to move to just before z900_exit so that the sql checks etc are done first
    if ($s_mll_postloop_map!="NONE"){
        if (!strpos(strtoupper($s_mll_postloop_group),"EOLONLY") === false)
        {
            $s_details_def .= "|MLL2_TALLY1|MLL2_TALLY2|MLL2_TALLY3|MLL2_TALLY4|MLL2_TALLY5|";
            $s_details_data .= "|".$s_mll_tally1_value."|".$s_mll_tally2_value."|".$s_mll_tally3_value."|".$s_mll_tally4_value."|".$s_mll_tally5_value."|";
            $sys_function_out .= $class_main->clmain_v100_load_html_screen($class_main->clmain_set_map_path_n_name($ps_ko_map_path,$s_mll_postloop_map),$s_details_def,$s_details_data,"no",$s_mll_postloop_group);
        }
    }

/* GW 20100911 - restructured into z810
    if ($s_rec_found == "NO")
    {
        if($s_sql_noerror = "YES")
        {
            $sys_function_out .= "";
        }else{
            $sys_function_out .= "<br>There is no data in ".$sys_function_name."for ".$s_sql;
        }
    }
*/

Z810_EXIT:
    if ($s_rec_found <> "NO")
    {
        GOTO Z900_EXIT;
    }
    if($s_sql_noerror = "YES")
    {
        $sys_function_out .= "";
        GOTO Z900_EXIT;
    }else{
//gw20101105 - replace with nodata loop         $sys_function_out .= "<br>There is no data in ".$sys_function_name."for ".$s_sql;
            $sys_function_out .= $class_main->clmain_v100_load_html_screen($s_mll_preloop_map,$s_details_def,$s_details_data,"no",$s_mll_preloop_group."_nodata");
            if (strpos(strtoupper($sys_function_out),"*ERROR") === false)
            {}else
            {
                $sys_function_out = "There is no data for your selection and the group ".$s_mll_preloop_group."_nodata is not in the map.  ".$s_mll_preloop_map."<br>".$s_sql;
            }
    }
Z900_EXIT:
    IF ($sys_debug == "YES"){z901_dump("dump of details_def=".$s_details_def);};
    IF ($sys_debug == "YES"){z901_dump("dump of details_data=".$s_details_data);};
    IF ($sys_debug == "YES"){z901_dump( $sys_function_name."  returned  = ".$sys_function_out." ");};
    return $sys_function_out;
}
//##########################################################################################################################################################################
function cl_pjl_Pf_b420_do_multilevelloop3($ps_dbcnx,$ps_MultiLevelloop3,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_calledfrom,$ps_ko_map_path)
{
// Update checklist
// if you want to copy b400 down then after you copy do the following
// a. remove all reference to multileveloop1
// b. change all mll1 to mll3
// c. change all _b400_ to _b420_

    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - Pf_b420_do_multilevelloop called from ".$ps_calledfrom." ";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump("Parameters= s_MultiLevelloop3,ps_debug,s_details_def,s_details_data,s_sessionno,ps_calledfrom");};
    IF ($sys_debug == "YES"){z901_dump("Values= ".$ps_MultiLevelloop3.",".$ps_debug.",".$ps_details_def.",".$ps_details_data.",".$ps_sessionno.",".$ps_calledfrom);};
    // define function specific variables and code
    //EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";};

            IF ($ps_MultiLevelloop3 == "NONE")
            {
                $sys_function_out .= "cl_pj_e2009 - There is no multilevel loops defined ".$sys_function_name;
                z901_dump($sys_function_out);
                GOTO Z900_EXIT;
            }

    IF ($sys_debug == "YES"){z901_dump("got past the multilevel loop test");};

    global $class_sql;
    global $class_main;
//DECLARE
    $s_sql = "";
    $result = "";
    $s_rec_found = "";
    $s_details_def = "";
    $s_details_data = "";
    $s_reccount = "";
    $s_numrows = "";
    $s_field_value = "";
    $row = array();
    $ar_dd = array();
    $ar_line_details = array();
    $ar_keys = array_keys($row);
    $ar_values = array_values($row);
    $key = "";
    $value = "";

    $s_mll_preloop_map = "";
    $s_mll_preloop_group = "";
    $s_mll_postloop_map = "";
    $s_mll_postloop_group = "";
    $s_mll_tally1_field = "";
    $s_mll_tally1_value = "";
    $s_mll_tally2_field = "";
    $s_mll_tally2_value = "";
    $s_mll_tally3_field = "";
    $s_mll_tally3_value = "";
    $s_mll_tally4_field = "";
    $s_mll_tally4_value = "";
    $s_mll_tally5_field = "";
    $s_mll_tally5_value = "";

//INITIALISE
    $s_details_def = $ps_details_def ;
    $s_details_data = $ps_details_data ;

    $ar_line_details = explode("|",$ps_MultiLevelloop3);
    $s_mll_preloop_map = strtoupper(TRIM($ar_line_details[1],""));
    $s_mll_preloop_group = TRIM($ar_line_details[2],"");
    $s_mll_postloop_map = strtoupper(TRIM($ar_line_details[3],""));
    $s_mll_postloop_group = TRIM($ar_line_details[4],"");
    $s_sql = TRIM($ar_line_details[5],"");
    $s_mll_tally1_field = TRIM($ar_line_details[6],"");
    $s_mll_tally2_field = TRIM($ar_line_details[7],"");
    $s_mll_tally3_field = TRIM($ar_line_details[8],"");
    $s_mll_tally4_field = TRIM($ar_line_details[9],"");
    $s_mll_tally5_field = TRIM($ar_line_details[10],"");
    $s_reccount = "0";
    $s_mll_tally1_value = "0";
    $s_mll_tally2_value = "0";
    $s_mll_tally3_value = "0";
    $s_mll_tally4_value = "0";
    $s_mll_tally5_value = "0";


    $s_sql = str_replace("#p","|",$s_sql);
    $s_sql = str_replace("#P","|",$s_sql);
    $s_sql = $class_main->clmain_v200_load_line( $s_sql,$s_details_def,$s_details_data,"no",$ps_sessionno,"cl_proc_jobline  b400");
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql." ");};


C000_RECORD_LOOP:
    $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_sql);
    $s_rec_found = "NO";
    $s_numrows = mysqli_num_rows($result);

    C100_DO_RECORDS:
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    IF ($row === false )
    {
        goto C900_END_RECORDS;
    }
    if ($s_numrows==0)
    {
        goto C900_END_RECORDS;
    }
    if (empty($row))
    {
        goto C900_END_RECORDS;
    }

    $s_rec_found = "YES";
    $s_reccount = $s_reccount + 1;
    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name."   in while loops = recs found");};


    $s_table_name = mysqli_fetch_field_direct($result,0)->table;
    $s_temp = $class_main->clmain_u820_fn_Z999_build_def_data($ps_dbcnx,$ps_debug, "cl_pjl b410",$row,$s_table_name);
    if (strpos($s_temp,"|^%##%^|") ===false)
    {
    }else{
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def.$ar_line_details[0];
        $s_details_data = $s_details_data.$ar_line_details[1];
        $s_temp = "";
    };

/*
    $i_c150 = 0;
    $ar_keys = array_keys($row);
    $ar_values = array_values($row);
    //$s_table_name = mysqli_field_table($result,0);
    $s_table_name = mysqli_fetch_field_direct($result,0)->table;
    //PRINT_R(mysqli_field_table($result,0));
    //    print_r($row);
C150_BUILD_DATA:
    if ($i_c150 == count($row))
    {
        goto C159_END;
    }
    $key = $ar_keys[$i_c150];
    $value = $ar_values[$i_c150];
//     z901_dump( "field ".$i_c150." count row=".count($row)." key = ".$key. " value = ".$value." count keys=".count($ar_keys)." count values = ".count($ar_values));
    //    print_r($row);
    if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
    {
        GOTO C152_SKIP_DATA_BLOB;
    }
    $ar_xml = simplexml_load_string($value);
//           var_dump($ar_xml);
    $newArry = array();
    $ar_xml = (array) $ar_xml;
    foreach ($ar_xml as $key => $value)
    {
        //$s_details_def .= "|REC_".mysqli_field_table($result,0)."_DB_".$key;
        $s_table_name = mysqli_fetch_field_direct($result,0)->table;
        $s_details_def .= "|REC_".$s_table_name."_DB_".$key;
        $s_details_data .= "|".$class_main->clmain_u596_xml_decode_char($value);
    }
    GOTO C158_NEXT;

C152_SKIP_DATA_BLOB:
    if (strpos(strtoupper($key),"DETAILS_D") === FALSE)
    {
        $s_details_def .= "|REC_".$s_table_name."_".$key;
        $s_details_data .= "|".$value;
        GOTO C158_NEXT;
    }
    if (strtoupper($key) == "DETAILS_DEF" )
    {
       $ar_dd = explode("|",$value);
       for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
       {
           IF (strtoupper($ar_dd[$i2]) <> "END")
           {
               $s_details_def .= "|DD_".$s_table_name.$ar_dd[$i2];
           }
        }
        GOTO C158_NEXT;
    }
    if (strtoupper($key) == "DETAILS_DATA" )
    {
       $ar_dd = explode("|",$value);
       for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
       {
           IF (strtoupper($ar_dd[$i2]) <> "END")
           {
               $s_details_data .= "|".$ar_dd[$i2];
           }
        }
        GOTO C158_NEXT;
    }

C158_NEXT:
    $i_c150 = $i_c150 + 1;
    GOTO C150_BUILD_DATA;
C159_END:
*/
    $s_details_def .= "|mll3_RECCOUNT|mll3_NUMROWS|";
    $s_details_data .= "|".$s_reccount."|".$s_numrows."|";
C200_DO_TALLY:
    if (strtoupper($s_mll_tally1_field) != "NONE")
    {
        $s_field_value = $class_main->clmain_v300_set_variable( "|%!_".$s_mll_tally1_field."_!%|",$s_details_def,$s_details_data,"no",$ps_sessionno,"cl_proc_jobline  B410_T1" );
        $s_mll_tally1_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally1_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"cl_proc_jobline  B410_T1");
    }
    if (strtoupper($s_mll_tally2_field) != "NONE")
    {
        $s_field_value = $class_main->clmain_v300_set_variable( "%!_".$s_mll_tally2_field."_!%",$s_details_def,$s_details_data,"no",$ps_sessionno,"cl_proc_jobline  B410_T2" );
        $s_mll_tally2_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally2_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"cl_proc_jobline  B410_T2");
    }
    if (strtoupper($s_mll_tally3_field) != "NONE")
    {
        $s_field_value = $class_main->clmain_v300_set_variable( "%!_".$s_mll_tally3_field."_!%",$s_details_def,$s_details_data,"no",$ps_sessionno,"cl_proc_jobline  b400 t3" );
        $s_mll_tally3_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally3_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"cl_proc_jobline  B410_T3");
    }
    if (strtoupper($s_mll_tally4_field) != "NONE")
    {
        $s_field_value = $class_main->clmain_v300_set_variable( "%!_".$s_mll_tally4_field."_!%",$s_details_def,$s_details_data,"no",$ps_sessionno,"cl_proc_jobline  b400 t4" );
        $s_mll_tally4_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally4_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"cl_proc_jobline  B410_T4");
    }

    if (strtoupper($s_mll_tally5_field) != "NONE")
    {
        $s_field_value = $class_main->clmain_v300_set_variable( "%!_".$s_mll_tally5_field."_!%",$s_details_def,$s_details_data,"no",$ps_sessionno,"cl_proc_jobline  b400 t5" );
        $s_mll_tally5_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally5_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"cl_proc_jobline  B410_T5");
    }

    $s_details_def .= "|mll3_TALLY1|mll3_TALLY2|mll3_TALLY3|mll3_TALLY4|mll3_TALLY5|";
    $s_details_data .= "|".$s_mll_tally1_value."|".$s_mll_tally2_value."|".$s_mll_tally3_value."|".$s_mll_tally4_value."|".$s_mll_tally5_value."|";
C299_END_DO_TALLY:

    if ($s_mll_preloop_map!="NONE"){
        $sys_function_out .= $class_main->clmain_v100_load_html_screen($class_main->clmain_set_map_path_n_name($ps_ko_map_path,$s_mll_preloop_map),$s_details_def,$s_details_data,"no",$s_mll_preloop_group);
    }
C300_DO_NEXT_LEVEL:
//at level 1    $sys_function_out .= Pf_b420_do_multilevelloop2($ps_dbcnx,$ps_MultiLevelloop2,$ps_MultiLevelloop3,"NO",$s_details_def,$s_details_data,$ps_sessionno,"cl_proc_jobline  B410_mll3");
//at level 2    $sys_function_out .= cl_pjl_Pf_b420_do_multilevelloop3($ps_dbcnx,$ps_MultiLevelloop3,"NO",$s_details_def,$s_details_data,$ps_sessionno,"cl_proc_jobline  b410_mll3");
// at level 3  nothing happes
    if ($s_mll_postloop_map!="NONE"){
        $sys_function_out .= $class_main->clmain_v100_load_html_screen($class_main->clmain_set_map_path_n_name($ps_ko_map_path,$s_mll_postloop_map),$s_details_def,$s_details_data,"no",$s_mll_postloop_group);
    }
C800_NEXT_REC:
    GOTO C100_DO_RECORDS;



C900_END_RECORDS:
    if ($s_rec_found == "NO")
    {
//gw20101105 - replace with nodata loop         $sys_function_out .= "<br>There is no data in ".$sys_function_name."for ".$s_sql;
        $sys_function_out .= $class_main->clmain_v100_load_html_screen($s_mll_preloop_map,$s_details_def,$s_details_data,"no",$s_mll_preloop_group."_nodata");
        if (strpos(strtoupper($sys_function_out),"*ERROR") === false)
        {}else
        {
           $sys_function_out = "There is no data for your selection and the group ".$s_mll_preloop_group."_nodata is not in the map.  ".$s_mll_preloop_map."<br>".$s_sql;
        }
    }

    IF ($sys_debug == "YES"){z901_dump( $sys_function_name."  returned  = ".$sys_function_out." ");};
Z900_EXIT:
    return $sys_function_out;
}
//##########################################################################################################################################################################
function pf_b490_set_value($ps_dbcnx,$ps_mll_tally_value,$ps_field_value,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_calledfrom)
{
//A100_TEMPLATE-INIT
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - pf_b490_set_value  called from ".$ps_calledfrom;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name."  ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};
//A199_END_TEMPLATE_INIT:

//B100_ define function specific variables and codE

    if (trim($ps_field_value,"") == "")
    {
        $ps_field_value =  "0";
    }
    if (!is_numeric($ps_field_value))
    {
       if (strpos($ps_field_value,"%!_") === false)
       {
           $ps_field_value =  "0";
       }
       $sys_function_out =  $ps_field_value;
    }else{
        $sys_function_out = $ps_mll_tally_value + $ps_field_value;
    }

//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";};

//X900_EXIT:
    return $sys_function_out;
}
//##########################################################################################################################################################################
function Pf_b500_do_url_value($ps_dbcnx,$ps_line,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{


    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
 //            echo "<br> Pf_b500_do_url_value -  checking url_value - ps_line=".$ps_line."<br>";

    if(strpos(strtoupper($ps_line),"+YESDEBUG") > 0 )
    {
        $sys_debug  = "YES";
        $sys_debug_text = "url debug - "; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }

    $sys_function_name = "";
    $sys_function_name = "debug - Pf_b500_do_url_value";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ");};

//    echo "start doing urlvalue<br>".$sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ";

    // define function specific variables and code
//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";};

//    $sys_function_out = "before globals";
//    return $sys_function_out;
//    EXIT;

//    global $class_sql;
//    global $class_main;

    $class_main = new clmain();


A001_DEFINE_VARS:
    $ar_line_details = array();
    $s_field_name = "";
    $s_param_name = "";
    $sys_function_out = "";
    $ar_url_siteparams = array();
    $ar_url_siteparams_vars = array();

A100_INIT_VARS:
    $ar_line_details = explode("|",$ps_line);
    $s_field_name = $ar_line_details[2];
    $s_param_name = $ar_line_details[3];
    $sys_function_out = "?b500_".$ar_line_details[2];


    $s_url_siteparams = $class_main->clmain_v300_set_variable("SITEPARAMS",$ps_details_def,$ps_details_data,$sys_debug,$ps_sessionno,"classprocess b500 ");
    $ar_url_siteparams = explode("^",$s_url_siteparams);

//          echo "<br> in process line checking url_value - s_url_siteparams=".$s_url_siteparams."<br>";

    if (strtoupper($ar_line_details[0]) != "URLVALUE")
    {
        goto Z900_EXIT;
    }
    if (strtoupper($ar_line_details[1]) != "V1")
    {
        goto A1_DO_V2;
    }
B001_DO_V1:
    $i = 0;

B100_ENTRY:
    IF ($i >= count($ar_url_siteparams))
    {
        goto B900_END;
    }
//    echo "doing param loop param value =".$ar_url_siteparams[$i]."<br>";
  //         echo "<br> in process line checking url_value - check field =".$ar_url_siteparams[$i]."<br>";

    if (strpos($ar_url_siteparams[$i],"=") === false)
    {
//        echo "doing param loop param no equals sign<br>";
        goto B200_GET_NEXT;
    }
    $ar_url_siteparams_vars = explode("=",$ar_url_siteparams[$i]);
//    echo "doing param loop check value in =".$ar_url_siteparams[$i]." explode url param 0=".strtoupper(trim($ar_url_siteparams_vars[0]," "))." 1 = ".strtoupper(trim($ar_url_siteparams_vars[1]))."<br>";
//    echo "doing check values 0=".(strtoupper(trim($ar_url_siteparams_vars[0]," "))."  param =".strtoupper(trim($s_param_name," ")))."<br>";
    $s_value_chk =strtoupper(trim($ar_url_siteparams_vars[0]," "));
    $s_param_name_chk = strtoupper(trim($s_param_name," "));
//    echo "doing check values2 0=*".$s_value_chk."*  param =*".$s_param_name_chk."*<br>";
    if ($s_value_chk == $s_param_name_chk)
    {
//        echo "compare worked param 0=".strtoupper(trim($ar_url_siteparams_vars[0]," "))." 1 = ".strtoupper(trim($ar_url_siteparams_vars[1]))."<br>";
        $sys_function_out = $ar_url_siteparams_vars[1];
//0           echo "<br> in process line checking url_value - field set to =".$sys_function_out."<br>";
        goto Z900_EXIT;
    }

B200_GET_NEXT:
    $i = $i + 1;
    goto B100_ENTRY;

B900_END:
    goto Z900_EXIT;
//    echo "doing urlvalue<br>s_field_name=".$s_field_name."<br>s_param_name=".$s_param_name."<br>sys_function_out=".$sys_function_out."<br>";
A1_DO_V2:

echo "<br>cl_proc_line b500 do url unknown line<br>";
echo "line is ps_line=".$ps_line;
echo "<br>Should be URLVALUE|V1|FIELDNAME|VALUE FROM THE URL|END";

Z900_EXIT:
    $sys_function_out = $s_field_name."|^%##%^|".$sys_function_out;

    IF ($sys_debug == "YES"){z901_dump( $sys_function_name."  returned  = ".$sys_function_out." ");};

    return $sys_function_out;

}
//##########################################################################################################################################################################
function Pf_b600_do_app_class($ps_dbcnx,$ps_line,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - Pf_b600_do_app_class";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ");};

//    echo "start doing urlvalue<br>".$sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ";

    // define function specific variables and code
//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";};

//    $sys_function_out = "before globals";
//    return $sys_function_out;
//    EXIT;

//    global $class_sql;
//    global $class_main;
    //global $class_apps;

    $class_main = new clmain();
//gw20160312 - move to in V1 process
//    require_once($_SESSION['ko_map_path'].'lib/class_app.php');
//    $class_apps = new AppClass();

A001_DEFINE_VARS:
    $ar_line_details = array();
    $s_action = "";
    $s_function = "";
    $s_function_param = "";
    $s_params = "";
    $s_field_name = "";

A100_INIT_VARS:
    $ar_line_details = explode("|",$ps_line);
    $s_field_name = $ar_line_details[2];
    $s_function_param = $ar_line_details[3];
    $sys_function_out = "?b600_unknown".$ps_line."?";
    $s_line_debug = $ar_line_details[4];

    $s_function = substr($s_function_param,0,strpos($s_function_param,"("));
    $s_params = substr($s_function_param,strpos($s_function_param,"("));
    $s_params = STR_REPLACE("#P","|",$s_params);
    $s_params = STR_REPLACE("#p","|",$s_params);
    $s_params = STR_REPLACE(",","^PARAM^",$s_params);
    $s_params = STR_REPLACE("(","",$s_params);
    $s_params = STR_REPLACE(")","",$s_params);
    $s_params = STR_REPLACE('#"',"^^#^",$s_params);
    $s_params = STR_REPLACE('"',"",$s_params);
    $s_params = STR_REPLACE("^^#^",'"',$s_params);
    $s_params = $class_main->clmain_v200_load_line( $s_params,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"cl_proc_jobline  b600a");

    $s_action = "RUN_FUNCTION";
    if (strtoupper(trim($s_field_name," ")) ==  strtoupper(trim("sys_app_function_list"," ")))
    {
        $s_action = "LIST_FUNCTIONS";
    }

    if (strtoupper(trim($s_field_name," ")) ==  strtoupper(trim("sys_app_function_help"," ")))
    {
        $s_action = "FUNCTIONS_HELP";
    }
//    echo "<br>s_function=".$s_function;
//    echo "<br>s_params=".$s_params;



B100_START:
    if (strtoupper($ar_line_details[0]) != "APP_CLASS")
    {
        goto Z900_EXIT;
    }
    if (strtoupper($ar_line_details[1]) != "V1")
    {
        goto A1_DO_V2;
    }
C100_DO_V1:
    require_once($_SESSION['ko_map_path'].'lib/class_app.php');
    $class_apps = new AppClass();

    $sys_function_out = $class_apps->clapp_a100_do_app_class($s_function,$s_params,$s_action,$ps_dbcnx,$s_line_debug,"cl_proc_jobline  b600b",$ps_details_def,$ps_details_data,$ps_sessionno);

//        echo "<br>utjcl ssss sys_function_out = +".$sys_function_out."+ <br>";

    if (strpos($sys_function_out,"|^%##%^|") ===false)
    {
        if (strpos(strtoupper($s_field_name),"_(RAW)") !== false)
        {
           $s_field_name = substr($s_field_name,0,strlen($s_field_name)-6);
           goto Z900_EXIT;
        }
        $sys_function_out = $class_main->clmain_v200_load_line($sys_function_out,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"cl_proc_jobline  b600c100");
//    echo "<br><br> utjcl app_class s_field_name=".$s_field_name."     sys_function_out=".$sys_function_out."<br>";
//    die ("<br>utjcl<br>");
    }else{
        $ar_line_details = explode("|^%##%^|",$sys_function_out);
        $s_field_name = $ar_line_details[0];
        $sys_function_out = $ar_line_details[1];
//    echo "<br><br> eeeutjcl app_class sys_function_out=".$sys_function_out."<br>";
//    die ("<br>utjcl<br>");
    };

//    echo "doing urlvalue<br>s_field_name=".$s_field_name."<br>s_param_name=".$s_param_name."<br>sys_function_out=".$sys_function_out."<br>";
    goto Z900_EXIT;
A1_DO_V2:
    if (strtoupper($ar_line_details[1]) != "V2")
    {
        goto A1_DO_V3;
    }

    $s_app_group = "_".substr($s_function,0,strpos($s_function,"/"));
    $s_app_group_path = substr($s_function,0,strpos($s_function,"/"));
    IF(strtoupper(trim($s_app_group)) == "_DMAV2")
    {
        $s_app_group_path = "";

    }
    $s_app_class = "AppClass".$s_app_group;
    $s_function =  substr($s_function,strpos($s_function,"/")+1);

    require_once($_SESSION['ko_map_path'].$s_app_group_path.'/lib/class_app'.$s_app_group.'.php');
    $class_appsv2 = new $s_app_class;
    $sys_function_out = $class_appsv2->clapp_a100_do_app_class($s_function,$s_params,$s_action,$ps_dbcnx,$s_line_debug,"cl_proc_jobline  b600b",$ps_details_def,$ps_details_data,$ps_sessionno);

//        echo "<br>utjcl ssss sys_function_out = +".$sys_function_out."+ <br>";

    if (strpos($sys_function_out,"|^%##%^|") ===false)
    {
        if (strpos(strtoupper($s_field_name),"_(RAW)") !== false)
        {
            $s_field_name = substr($s_field_name,0,strlen($s_field_name)-6);
            goto Z900_EXIT;
        }
        $sys_function_out = $class_main->clmain_v200_load_line($sys_function_out,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"cl_proc_jobline  b600c100");
//    echo "<br><br> utjcl app_class s_field_name=".$s_field_name."     sys_function_out=".$sys_function_out."<br>";
//    die ("<br>utjcl<br>");
    }else{
        $ar_line_details = explode("|^%##%^|",$sys_function_out);
        $s_field_name = $ar_line_details[0];
        $sys_function_out = $ar_line_details[1];
//    echo "<br><br> eeeutjcl app_class sys_function_out=".$sys_function_out."<br>";
//    die ("<br>utjcl<br>");
    };

//    echo "doing urlvalue<br>s_field_name=".$s_field_name."<br>s_param_name=".$s_param_name."<br>sys_function_out=".$sys_function_out."<br>";
    goto Z900_EXIT;

A1_DO_V3:

Z900_EXIT:
    $sys_function_out = $s_field_name."|^%##%^|".$sys_function_out;
//    $sys_function_out = $s_field_name."|^%##%^|this is working".$sys_function_out;

    IF ($sys_debug == "YES"){z901_dump( $sys_function_name."  returned  = ".$sys_function_out." ");};

 //       echo "<br>utjcl z900 a sys_function_out = +".$sys_function_out."+ <br>";
    return $sys_function_out;

}
//##########################################################################################################################################################################
function Pf_c100_run_jcl($ps_dbcnx,$ps_line_in,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    GLOBAL $class_main;
    global $class_sql;

//    ECHO "<BR> cl proc jobline pfc100_jcl    doing psline=(".$ps_line_in.")";



A500_SET_VALUES:
    $dbcnx = $ps_dbcnx;
    $s_sessionno = $ps_sessionno;
    if (isset($_SESSION['ko_map_path']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
        $ko_map_path = $_SESSION['ko_map_path'];
    }else{
    $_SESSION['ko_map_path'] = "";
    }

     $ar_line_details = array();
    $s_output_to_file = "NO";
    $s_output_to_filename = "";

    $ar_line_details = explode("|",$ps_line_in);
    $map = $ar_line_details[1];
    $sys_debug = $ps_debug;

    $map = $class_main->clmain_set_map_path_n_name($ko_map_path,$map);
    $sys_prog_name = "cl_proc_jobline .php";
    $sys_function_name = $sys_prog_name;
    IF ($sys_debug == "YES"){echo $sys_prog_name." ";};

    $s_file_exists='Y';
    if(file_exists($map))
    {
        $array_lines = file($map);
    }
    else
    {
        $s_file_exists='N';
        $sys_function_out =  "<br>".$sys_prog_name." ##### File Not Found-:".$map."<br>";
        echo "<br> Error unable to locate the jcl tempalte = (".$map.")<br>";
        GOTO Z900_EXIT;
    }
    $s_no_jcl_in_file = "The file ".$map." has no jcl code";


    IF ($sys_debug == "YES"){echo $sys_prog_name." after file exists check s_map_file_exists=".$s_file_exists."<br>";};
    IF ($sys_debug == "YES"){echo $sys_prog_name." number of lines in the file(array)=".count($array_lines)."<br>";};

//    echo "after file open<br>";
    if (isset($_SESSION[$s_sessionno.'ut_logon_timezone']))
    {
        $timezone=$_SESSION[$s_sessionno.'ut_logon_timezone'];
        date_default_timezone_set($timezone);
//        echo "<br> ************88  set the timezone to ".$timezone."<br>";
    }else{
//        echo "<br> ************88 not setup  set the timezone to ".$timezone."<br>";
    }

    $s_details_def =$ps_details_def;
    $s_details_data =$ps_details_data;
    $s_inJCL ="N";
    $s_CR_LF = "CR_LF";
    $s_filename = "NO_FILE";
    $s_out_line="";

    $i = 0;
    $s_line_in = "";

B100_GET_REC:
    IF ($i >= count($array_lines))
    {
        goto Z700_END;
    }

    $s_line_in = $array_lines[$i];
    IF (substr($s_line_in,0,1) == "*")
    {
        goto Z800_GET_NEXT;
    }
    IF (substr($s_line_in,0,2) == "//")
    {
        goto Z800_GET_NEXT;
    }
    IF (trim($s_line_in) == "")
    {
        goto Z800_GET_NEXT;
    }

// used to ignore the line
    IF (strpos(strtoupper($s_line_in),strtoupper("SKIP_LINE")) === false )
    {}else{
        IF ($sys_debug == "YES"){echo $sys_function_name." "."got the map skip line command <br>".$s_line_in."<br>";};
        goto Z800_GET_NEXT;
    }
// used to indicate if there is a start JCL loop
    IF (strpos(strtoupper($s_line_in),strtoupper("START_JCL_HERE"))  === false )
    {}else{
        IF ($sys_debug == "YES"){echo $sys_function_name."got the map starter start line for jcl<br>".$s_line_in."<br>";};
        $s_inJCL ="Y";
        goto Z800_GET_NEXT;
    }
// used to indicate if there is a end  JCL loop
    IF (strpos(strtoupper($s_line_in),strtoupper("END_JCL_HERE"))  === false  )
    {}else{
        IF ($sys_debug == "YES"){echo $sys_function_name."got the map end line for jcl<br>".$s_line_in."<br>";};
        $s_inJCL ="N";
        goto Z700_END;
    }
// if before the required loop get the next line
    if ($s_inJCL =="N")
    {
        IF ($sys_debug == "YES"){echo $sys_function_name." ".$i." in jcl  loop<br>";};
        goto Z800_GET_NEXT;
    }

    $s_no_jcl_in_file = "";
    $ar_line_details = explode("|",$s_line_in);

    IF ($sys_debug == "YES"){echo $sys_function_name." line <br>".$s_line_in."<br>";};

    IF (strtoupper($ar_line_details[0]) == "DUMPTABLELISTTOSQLLOG")
    {
        $temp = $class_sql->c_sqlclient_list_tables_to_log($dbcnx);
        goto Z800_GET_NEXT;
    }
A00_START:
    $xml_arr  ="";
    $s_temp = $this->pf_clprocessjobline($dbcnx,$s_line_in,$s_details_def,$s_details_data,$xml_arr,$s_sessionno,"NO");
    if (strpos($s_temp,"|^%##%^|") !== false)
    {
       if (strpos(strtoupper($s_line_in),strtoupper("|jcl_get_new_def_n_data|")) !== false)
        {
    //    echo "<br>clmain b100 in a125 <br>sys_function_out<br>".$sys_function_out."<br><br>";
    //gw20110512 -dont change sysout
            $ar_line_details = explode("|^%##%^|",$s_temp);
            $s_details_def = $ar_line_details[0];
            $s_details_data = $ar_line_details[1];
            goto Z800_GET_NEXT;
        }
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
        goto Z800_GET_NEXT;
    }
B00_NEXT:
    $ar_line_details = explode("|",$s_line_in);
    IF (strtoupper($ar_line_details[0]) == STRTOUPPER("MAKEOUTPUTFILE"))
    {
        $s_filename = $s_temp;
        IF (strtoupper($ar_line_details[2]) == STRTOUPPER("NO_CR_LF"))
        {
            $s_CR_LF = "NO_CR_LF";
        }
        IF (strtoupper($ar_line_details[2]) == STRTOUPPER("CR_ONLY"))
        {
            $s_CR_LF = "CR_ONLY";
        }
        IF (strtoupper($ar_line_details[2]) == STRTOUPPER("LF_ONLY"))
        {
            $s_CR_LF = "LF_ONLY";
        }
        IF (strtoupper($ar_line_details[2]) == STRTOUPPER("ONE_LINE"))
        {
            $s_CR_LF = "ONE_LINE";
        }
    }

    if ($s_filename != $s_temp)
    {
        $fh = fopen($s_filename,'a') or die("cant open file".$s_filename);
        if ($s_CR_LF == "ONE_LINE")
        {
            $s_temp=str_replace(chr(13),'!xxyuxx@',$s_temp); // replace carriage return with dash
            $s_temp=str_replace(chr(10),'!xxyuxx@',$s_temp); // replace linefeed return with dash
            $s_temp=str_replace('!xxyuxx@',"",$s_temp); // replace linefeed return with dash
             fwrite($fh,trim($s_temp));
             GOTO B100_CLOSE;
        }

        if ($s_CR_LF == "NO_CR_LF")
        {
            $s_temp=str_replace(chr(13),'!xxyuxx@',$s_temp); // replace carriage return with dash
            $s_temp=str_replace(chr(10),'!xxyuxx@',$s_temp); // replace linefeed return with dash
            $s_temp=str_replace('!xxyuxx@',"",$s_temp); // replace linefeed return with dash
             fwrite($fh,trim($s_temp));
             GOTO B100_CLOSE;
        }
        if ($s_CR_LF == "CR_ONLY")
        {
             fwrite($fh,trim($s_temp)."\r");
             GOTO B100_CLOSE;
        }
        if ($s_CR_LF == "LF_ONLY")
        {
             fwrite($fh,trim($s_temp)."\n");
             GOTO B100_CLOSE;
        }
        fwrite($fh,$s_temp."\r\n");
B100_CLOSE:
        fclose($fh);
    }

//    DIE ("<BR> cl_proc_jobline  PRE Z800_UNKNOWN LINE <BR>The jcl file ".$map." has an unknown line =".$s_line_in);
Z800_GET_NEXT:
    $i = $i + 1;
    goto B100_GET_REC;

Z700_END:

Z900_EXIT:

return "";
}

/*
function Pf_c100_run_jcl($ps_dbcnx,$ps_line_in,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
echo "<br><br> doing the pfc100 ps_line_in=".$ps_line_in."<br>";

global $class_clprocessjobline;


$s_line_in = "add_details|RUN_SCREENID|udj_response_hp_accept|end";

$dbcnx = $ps_dbcnx;
$s_details_def = $ps_details_def;
$s_details_data = $ps_details_data;
$s_sessionno = $ps_sessionno;


    $xml_arr  ="";
    $s_temp = $class_clprocessjobline->pf_clprocessjobline($dbcnx,$s_line_in,$s_details_def,$s_details_data,$xml_arr,$s_sessionno,"NO");
    if (strpos($s_temp,"|^%##%^|") !== false)
    {
       if (strpos(strtoupper($s_line_in),strtoupper("|jcl_get_new_def_n_data|")) !== false)
        {
    //    echo "<br>clmain b100 in a125 <br>sys_function_out<br>".$sys_function_out."<br><br>";
    //gw20110512 -dont change sysout
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $ar_line_details[0];
        $s_details_data = $ar_line_details[1];
        goto a1;
        }

        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
    }
a1:
$s_line_in = "sys_class|v1|jcl_start_utime|clmain_u620_time_function('set','none','none',NO|NO|END";

    $xml_arr  ="";
    $s_temp = $class_clprocessjobline->pf_clprocessjobline($dbcnx,$s_line_in,$s_details_def,$s_details_data,$xml_arr,$s_sessionno,"NO");
    if (strpos($s_temp,"|^%##%^|") !== false)
    {
       if (strpos(strtoupper($s_line_in),strtoupper("|jcl_get_new_def_n_data|")) !== false)
        {
    //    echo "<br>clmain b100 in a125 <br>sys_function_out<br>".$sys_function_out."<br><br>";
    //gw20110512 -dont change sysout
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $ar_line_details[0];
        $s_details_data = $ar_line_details[1];
        goto a2;
        }

        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
    }
a2:


$s_line_in = "urlvalue|v1|url_mode|mode|end";

    $xml_arr  ="";
    $s_temp = $class_clprocessjobline->pf_clprocessjobline($dbcnx,$s_line_in,$s_details_def,$s_details_data,$xml_arr,$s_sessionno,"NO");
    if (strpos($s_temp,"|^%##%^|") !== false)
    {
       if (strpos(strtoupper($s_line_in),strtoupper("|jcl_get_new_def_n_data|")) !== false)
        {
    //    echo "<br>clmain b100 in a125 <br>sys_function_out<br>".$sys_function_out."<br><br>";
    //gw20110512 -dont change sysout
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $ar_line_details[0];
        $s_details_data = $ar_line_details[1];
        goto a3;
        }

        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
    }
a3:

$s_line_in = "urlvalue|v1|url_job_id|job_id|end";

    $xml_arr  ="";
    $s_temp = $class_clprocessjobline->pf_clprocessjobline($dbcnx,$s_line_in,$s_details_def,$s_details_data,$xml_arr,$s_sessionno,"NO");
    if (strpos($s_temp,"|^%##%^|") !== false)
    {
       if (strpos(strtoupper($s_line_in),strtoupper("|jcl_get_new_def_n_data|")) !== false)
        {
    //    echo "<br>clmain b100 in a125 <br>sys_function_out<br>".$sys_function_out."<br><br>";
    //gw20110512 -dont change sysout
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $ar_line_details[0];
        $s_details_data = $ar_line_details[1];
        goto a4;
        }

        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
    }
a4:

$s_line_in = "urlvalue|v1|url_record_set|record_set|end";

    $xml_arr  ="";
    $s_temp = $class_clprocessjobline->pf_clprocessjobline($dbcnx,$s_line_in,$s_details_def,$s_details_data,$xml_arr,$s_sessionno,"NO");
    if (strpos($s_temp,"|^%##%^|") !== false)
    {
       if (strpos(strtoupper($s_line_in),strtoupper("|jcl_get_new_def_n_data|")) !== false)
        {
    //    echo "<br>clmain b100 in a125 <br>sys_function_out<br>".$sys_function_out."<br><br>";
    //gw20110512 -dont change sysout
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $ar_line_details[0];
        $s_details_data = $ar_line_details[1];
        goto a5;
        }

        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
    }
a5:



//ADD_RECORD|v1|ow_activity|autokey|autokey|NO| createddntime  = "#p%!_OSYSVAL_YMDHMSU_!%#p",  updateddntime  = "#p%!_OSYSVAL_YMDHMSU_!%#p", createduid  = "?pp_job",  updateduid  = "?pp_job",   createdutime = #p%!_time()_!%#p,  updatedutime = #p%!_time()_!%#p,  owa_group  ="ORDERACTIVITY",  owa_type  =  "ORDCOMPFILECREATESTART",  owa_desc  = "Start of the file creation at order close for order  #p%!_url_orderno_!%#p",  owa_notes  = "The process to close the order has started",  owa_number  = 0,  owa_number_desc = "not used",  owa_related_id  = "#p%!_url_opj_id_!%#p",  owa_related_id_desc ="opj_id",  ow_company_id  = "#p%!_SESSION_ko_ow_company_id_!%#p",  ow_client_id  =  "#p%!_SESSION_ko_ow_client_id_!%#p", data_blob = "new"||end
//update_record|v1|ow_op_jobs|WHERESTATEMENT| ( opj_id = #p%!_url_opj_id_!%#p ) |DEBUG|opj_status = "CLOSED"|end

//$s_line_in = "add_details|jcl_outfilenameonly|udj_hp_response_accept_#p%!_url_orderno_!%#p_#p%!_OSYSVAL_YMDHMSU_!%#p.txt|END";
$s_line_in = "add_details|jcl_outfilenameonly|udj_hp_response_accept_1011_#p%!_OSYSVAL_YMDHMSU_!%#p.txt|END";

    $xml_arr  ="";
    $s_temp = $class_clprocessjobline->pf_clprocessjobline($dbcnx,$s_line_in,$s_details_def,$s_details_data,$xml_arr,$s_sessionno,"NO");
    if (strpos($s_temp,"|^%##%^|") !== false)
    {
       if (strpos(strtoupper($s_line_in),strtoupper("|jcl_get_new_def_n_data|")) !== false)
        {
    //    echo "<br>clmain b100 in a125 <br>sys_function_out<br>".$sys_function_out."<br><br>";
    //gw20110512 -dont change sysout
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $ar_line_details[0];
        $s_details_data = $ar_line_details[1];
        goto a6;
        }

        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
    }
a6:

$s_line_in = "add_details|jcl_outfilename|#p%!_osysval_now_yyyy_!%#p/#p%!_osysval_now_MM_!%#p/#p%!_osysval_now_DD_!%#p/#p%!_jcl_outfilenameonly_!%#p|END";

    $xml_arr  ="";
    $s_temp = $class_clprocessjobline->pf_clprocessjobline($dbcnx,$s_line_in,$s_details_def,$s_details_data,$xml_arr,$s_sessionno,"NO");
    if (strpos($s_temp,"|^%##%^|") !== false)
    {
       if (strpos(strtoupper($s_line_in),strtoupper("|jcl_get_new_def_n_data|")) !== false)
        {
    //    echo "<br>clmain b100 in a125 <br>sys_function_out<br>".$sys_function_out."<br><br>";
    //gw20110512 -dont change sysout
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $ar_line_details[0];
        $s_details_data = $ar_line_details[1];
        goto a7;
        }

        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
    }
a7:

$s_line_in = "add_details|jcl_outfileurl|http://#p%!_server_HTTP_HOST_!%#p/ud_jm_dev/#p%!_jcl_outfilename_!%#p|END";

    $xml_arr  ="";
    $s_temp = $class_clprocessjobline->pf_clprocessjobline($dbcnx,$s_line_in,$s_details_def,$s_details_data,$xml_arr,$s_sessionno,"NO");
    if (strpos($s_temp,"|^%##%^|") !== false)
    {
       if (strpos(strtoupper($s_line_in),strtoupper("|jcl_get_new_def_n_data|")) !== false)
        {
    //    echo "<br>clmain b100 in a125 <br>sys_function_out<br>".$sys_function_out."<br><br>";
    //gw20110512 -dont change sysout
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $ar_line_details[0];
        $s_details_data = $ar_line_details[1];
        goto a8;
        }

        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
    }
a8:

////open file for output
//MAKEOUTPUTFILE|filename=#p%!_SESSION_ko_map_path_!%#p#p%!_jcl_outfilename_!%#p||end

$s_line_in = "MAKEOUTPUTFILE|filename=#p%!_SESSION_ko_map_path_!%#p#p%!_jcl_outfilename_!%#p|end";
echo "<br> doing line s_line_in=".$s_line_in;
    $xml_arr  ="";
    $s_temp = $this->pf_clprocessjobline($dbcnx,$s_line_in,$s_details_def,$s_details_data,$xml_arr,$s_sessionno,"NO");
    $s_filename = $s_temp;
    echo "<br>out from outputtotexxte ".$s_temp." <br>";
a9:


    $s_line_in = "Draw|udj_response_hp_accept.map|file_header|END";

    $xml_arr  ="";
    $s_temp = $class_clprocessjobline->pf_clprocessjobline($dbcnx,$s_line_in,$s_details_def,$s_details_data,$xml_arr,$s_sessionno,"NO");

        $fh = fopen($s_filename,'a') or die("cant open file".$s_filename);
        fwrite($fh,$s_temp."\r\n");
        fclose($fh);
echo "<br>draw has done <!--".$s_temp." --><br>";

a10:

    $s_line_in = "dataloop|udj_response_hp_accept.map|activity_loop|SELECT * From jobs as jobsrec WHERE ( companyid = '#p%!_COOKIE_ud_companyid_!%#p' and  (number_2 > 1999 and number_2 < 2999))   #p%!_jcl_app_filters_!%#p  limit 10   |END";

    $xml_arr  ="";
    $s_temp = $class_clprocessjobline->pf_clprocessjobline($dbcnx,$s_line_in,$s_details_def,$s_details_data,$xml_arr,$s_sessionno,"NO");

        $fh = fopen($s_filename,'a') or die("cant open file".$s_filename);
        fwrite($fh,$s_temp."\r\n");
        fclose($fh);
echo "<br>draw has done <!--".$s_temp." --><br>";


    $s_line_in = "rundataif|v1|url_mode|=|NEWREC|SELECT * from jobs as jobs_dets where jobs_id =  '#p%!_jobs_id_!%#p'  #p%!_jcl_app_filters_!%#p|END";

    $xml_arr  ="";
    $s_temp = $class_clprocessjobline->pf_clprocessjobline($dbcnx,$s_line_in,$s_details_def,$s_details_data,$xml_arr,$s_sessionno,"NO");
echo "<br>draw has done <!--".$s_temp." --><br>";
    if (strpos($s_temp,"|^%##%^|") !== false)
    {
       if (strpos(strtoupper($s_line_in),strtoupper("|jcl_get_new_def_n_data|")) !== false)
        {
    //    echo "<br>clmain b100 in a125 <br>sys_function_out<br>".$sys_function_out."<br><br>";
    //gw20110512 -dont change sysout
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $ar_line_details[0];
        $s_details_data = $ar_line_details[1];
        goto a11A;
        }

        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
    }
a11A:



$s_line_in = "sys_class|v1|jcl_end_utime|clmain_u620_time_function('set','none','none',NO|NO|END";

    $xml_arr  ="";
    $s_temp = $class_clprocessjobline->pf_clprocessjobline($dbcnx,$s_line_in,$s_details_def,$s_details_data,$xml_arr,$s_sessionno,"NO");
    if (strpos($s_temp,"|^%##%^|") !== false)
    {
       if (strpos(strtoupper($s_line_in),strtoupper("|jcl_get_new_def_n_data|")) !== false)
        {
    //    echo "<br>clmain b100 in a125 <br>sys_function_out<br>".$sys_function_out."<br><br>";
    //gw20110512 -dont change sysout
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $ar_line_details[0];
        $s_details_data = $ar_line_details[1];
        goto a11;
        }

        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
    }
a11:

$s_line_in = "sys_class|v1|jcl_run_utime|clmain_u620_time_function('diff',#p%!_jcl_start_utime_!%#p,#p%!_jcl_end_utime_!%#p,NO|NO|END";

    $xml_arr  ="";
    $s_temp = $class_clprocessjobline->pf_clprocessjobline($dbcnx,$s_line_in,$s_details_def,$s_details_data,$xml_arr,$s_sessionno,"NO");
    if (strpos($s_temp,"|^%##%^|") !== false)
    {
       if (strpos(strtoupper($s_line_in),strtoupper("|jcl_get_new_def_n_data|")) !== false)
        {
    //    echo "<br>clmain b100 in a125 <br>sys_function_out<br>".$sys_function_out."<br><br>";
    //gw20110512 -dont change sysout
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $ar_line_details[0];
        $s_details_data = $ar_line_details[1];
        goto a12;
        }

        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
    }
a12:

$s_line_in = "Draw|udj_response_hp_accept.map|file_footer|END";
    $xml_arr  ="";
    $s_temp = $class_clprocessjobline->pf_clprocessjobline($dbcnx,$s_line_in,$s_details_def,$s_details_data,$xml_arr,$s_sessionno,"NO");
echo "<br>draw has done <!--".$s_temp." --><br>";
        $fh = fopen($s_filename,'a') or die("cant open file".$s_filename);
    //    fwrite($fh,date("Y-m-d H:i:s",time())."\r\n");
        fwrite($fh,$s_temp."\r\n");
        fclose($fh);

a13:


//$s_line_in = "Add_utl_activity|V1|ACTIVITYTRACK|%!_COOKIE_ud_companyid_!%|%!_SESSION_username_!%|Create hp response accept file |Create hp response accept file|%!_run_screenid_!%|processid|%!_SESSION_username_!%| userid|%!_jcl_run_utime_!%|seconds duration|%!_notused_(SPACEONERROR)_!%| notused|%!_notused_(SPACEONERROR)_!%| notused|




}
*/
function moved_to_clmain_Pf_b650_do_sys_class($ps_dbcnx,$ps_line,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
/*
    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - Pf_b650_do_sys_class";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ");};

//    echo "start doing urlvalue<br>".$sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ";

    // define function specific variables and code
//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";};

//    $sys_function_out = "before globals";
//    return $sys_function_out;
//    EXIT;

    global $class_sql;
    global $class_main;
    global $class_apps;

A001_DEFINE_VARS:
    $ar_line_details = array();
    $s_action = "";
    $s_function = "";
    $s_function_param = "";
    $s_params = "";
    $s_field_name = "";

A100_INIT_VARS:
    $ar_line_details = explode("|",$ps_line);
    $s_field_name = $ar_line_details[2];
    $s_function_param = $ar_line_details[3];
    $sys_function_out = "?b650_unknown".$ps_line."?";
    $s_line_debug = $ar_line_details[4];

    $s_function = substr($s_function_param,0,strpos($s_function_param,"("));
    $s_params = substr($s_function_param,strpos($s_function_param,"("));
    $s_params = STR_REPLACE("#P","|",$s_params);
    $s_params = STR_REPLACE("#p","|",$s_params);
    $s_params = STR_REPLACE(",","^",$s_params);
    $s_params = STR_REPLACE("(","",$s_params);
    $s_params = STR_REPLACE(")","",$s_params);
    $s_params = STR_REPLACE('"',"",$s_params);
    $s_params = $class_main->clmain_v200_load_line( $s_params,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"cl_proc_jobline  b600a");
    $s_action = "RUN_FUNCTION";
    if (strtoupper(trim($s_field_name," ")) ==  strtoupper(trim("sys_app_function_list"," ")))
    {
        $s_action = "LIST_FUNCTIONS";
    }

    if (strtoupper(trim($s_field_name," ")) ==  strtoupper(trim("sys_app_function_help"," ")))
    {
        $s_action = "FUNCTIONS_HELP";
    }
//    echo "<br>s_function=".$s_function;
//    echo "<br>s_params=".$s_params;

B100_START:
    if (strtoupper($ar_line_details[0]) != "SYS_CLASS")
    {
        goto Z900_EXIT;
    }
    if (strtoupper($ar_line_details[1]) != "V1")
    {
        goto A1_DO_V2;
    }
C100_DO_V1:
    $sys_function_out = $class_main->clmain_a100_do_sys_class($s_function,$s_params,$s_action,$ps_dbcnx,$s_line_debug,"cl_proc_jobline  b650b",$ps_details_def,$ps_details_data,$ps_sessionno);
    $sys_function_out = $class_main->clmain_v200_load_line($sys_function_out,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"cl_proc_jobline  b650c100");

//    echo "doing urlvalue<br>s_field_name=".$s_field_name."<br>s_param_name=".$s_param_name."<br>sys_function_out=".$sys_function_out."<br>";
A1_DO_V2:

Z900_EXIT:
    $sys_function_out = $s_field_name."|^%##%^|".$sys_function_out;
//    $sys_function_out = $s_field_name."|^%##%^|this is working".$sys_function_out;

    IF ($sys_debug == "YES"){z901_dump( $sys_function_name."  returned  = ".$sys_function_out." ");};

    return $sys_function_out;
*/
}
//##########################################################################################################################################################################
//sys_classif|v1|url_MODE|=|NEWREC|NULL|clmain_v940_initialise_error_sessionvs(#p%!_SESSION_SUPER_validate_error_html_!%#p,NO,sysset)|NO|END
function Pf_b655_do_sys_classif($ps_dbcnx,$ps_line,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{

    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - Pf_b655_do_sys_classif";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ");};


    global $class_sql;
    global $class_main;
    global $class_apps;

A001_DO_IF:
    $ar_line_details = array();
    $s_action = "";
    $s_function = "";
    $s_function_param = "";
    $s_params = "";
    $s_field_name = "";

    $ar_line_details = explode("|",$ps_line);
    if (strtoupper($ar_line_details[0]) != "SYS_CLASSIF")
    {
        goto Z900_EXIT;
    }
    if (strtoupper($ar_line_details[1]) != "V1")
    {
        goto A1_DO_V2;
    }

    if (count($ar_line_details) <> 9 )
    {
        echo "<br> ****  cl_proc_jobline  - sys_classif *** the number of fields is not correct";
        echo "<br> ****  ".count($ar_line_details)." instead of the expected 9 <br>";

    }

    $s_field_to_check = $ar_line_details[2];
    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];

    $s_field_name = $ar_line_details[5];
    $s_function_param = $ar_line_details[6];
    $s_line_debug = $ar_line_details[7];

    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;

B100_START:
    $sys_function_out = "?b110_unknown".$ps_line."?";
    $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,"no",$ps_sessionno,"cl_proc_jobline  b655");

C100_DO_V1:
    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_b655a");
    if ($s_true == "FALSE")
    {
        IF ($sys_debug == "YES"){z901_dump( $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);};
        $sys_function_out = "";
        $s_field_name = $s_field_name."_if_".$s_compare_value."_failed";
        GOTO Z900_EXIT;
    }



A100_INIT_VARS:
    $sys_function_out = "?b650_unknown".$ps_line."?";

    $s_function = substr($s_function_param,0,strpos($s_function_param,"("));
    $s_params = substr($s_function_param,strpos($s_function_param,"("));
    $s_params = STR_REPLACE("#P","|",$s_params);
    $s_params = STR_REPLACE("#p","|",$s_params);
    $s_params = STR_REPLACE(",","^",$s_params);
    $s_params = STR_REPLACE("(","",$s_params);
    $s_params = STR_REPLACE(")","",$s_params);
    $s_params = STR_REPLACE('"',"",$s_params);

    $s_params = $class_main->clmain_v200_load_line( $s_params,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"cl_proc_jobline  b655b ");
    $s_action = "RUN_FUNCTION";
    if (strtoupper(trim($s_field_name," ")) ==  strtoupper(trim("sys_app_function_list"," ")))
    {
        $s_action = "LIST_FUNCTIONS";
    }

    if (strtoupper(trim($s_field_name," ")) ==  strtoupper(trim("sys_app_function_help"," ")))
    {
        $s_action = "FUNCTIONS_HELP";
    }

C100_DO_V1a:
    $sys_function_out = $class_main->clmain_a100_do_sys_class($s_function,$s_params,$s_action,$ps_dbcnx,$s_line_debug,"cl_proc_jobline  b655c ",$ps_details_def,$ps_details_data,$ps_sessionno);
    $sys_function_out = $class_main->clmain_v200_load_line($sys_function_out,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"cl_proc_jobline  b655d ");

//    echo "doing urlvalue<br>s_field_name=".$s_field_name."<br>s_param_name=".$s_param_name."<br>sys_function_out=".$sys_function_out."<br>";
A1_DO_V2:

Z900_EXIT:
    $sys_function_out = $s_field_name."|^%##%^|".$sys_function_out;
//    $sys_function_out = $s_field_name."|^%##%^|this is working".$sys_function_out;

    IF ($sys_debug == "YES"){z901_dump( $sys_function_name."  returned  = ".$sys_function_out." ");};


    echo "<br>gw cpj b655 returned []".$sys_function_out."[]";
    return $sys_function_out;

}
function z901_dump($ps_text)
{

    $file_path  = "";
    if (isset($_SESSION['ko_map_path']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
        $file_path = $_SESSION['ko_map_path'];
    }

    $file_path = $file_path."logs";
    if (!file_exists($file_path))
    {
        $md= mkdir($file_path);
    }

    $dateym = date('Ym');
    $myfile = $file_path."\cl_proc_jobline _".$dateym.".txt";
    $fh = fopen($myfile,'a') or die("cant open file".$myfile);
    fwrite($fh,date("Y-m-d H:i:s",time())." - ".$ps_text."\r\n");
    fclose($fh);

}
    }
?>
<?php

/*
<!--
function pf_prev_utl_jcl_stuff()
{
        A00_START:
        IF (strtoupper($ar_line_details[0]) == "DRAW")
        {
            $sys_function_out .= pf_b100_DO_DRAW_HTML($class_main->clmain_set_map_path_n_name($ko_map_path,$ar_line_details[1]),$ar_line_details[2],"NO",$s_details_def,$s_details_data);
            goto Z800_END_OF_LINE;
        }


    //* DrawIF|v1|field|operator|value|map_name|loop|END
    //DrawIF|v1|url_MODE|=|WELCOME|my_buddy_main.htm|welcome_map|END
        IF (strtoupper($ar_line_details[0]) == "DRAWIF")
        {
            $sys_function_out .= pf_b110_DO_DRAWIF_HTML($s_line_in,"NO",$s_details_def,$s_details_data,$s_sessionno);
            goto Z800_END_OF_LINE;
        }
    A900_END:
    B000_START:
    //dataloop|map|loop|Select * from devices |END
    //* dataloop|owl_bdymain.htm|detail_loop|SELECT * FROM sales_order  as S_O where (so_order_status <> "90" and so_order_status <> "99" and so_order_status <> "91"and so_order_status <> "75") AND so_whse_code = "3" order by so_order_no desc LIMIT 0 , 50 |END
    //* dataloop|v1|url_MODE|=|INITSCRN|owl_bdymain.htm|detail_loop|SELECT * FROM sales_order  as S_O where (so_order_status <> "90" and so_order_status <> "99" and so_order_status <> "91"and so_order_status <> "75") AND so_whse_code = "3" order by so_order_no desc LIMIT 0 , 50 |END

        IF (strtoupper($ar_line_details[0]) == "DATALOOP")
        {
            GOTO B100_NOIF;
        }
        IF (strtoupper($ar_line_details[0]) == "DATALOOPIF")
        {
            GOTO B200_IF;
        }
        GOTO B900_END;
    B100_NOIF:
        $s_temp_value = $ar_line_details[3];
        $s_map  = $ar_line_details[1];
        $s_group  = $ar_line_details[2];
        GOTO B500_DO;
    B200_IF:
        IF (strtoupper($ar_line_details[1]) <> "V1")
        {
            GOTO B210_do_V2;
        }
        $s_field_to_check = $ar_line_details[2];
        $s_comparison_operator = $ar_line_details[3];
        $s_compare_value  = $ar_line_details[4];
        $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  B200 ");
        $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"NO","utjcl_B200 ");
        if ($s_true == "FALSE")
        {
            IF ($sys_debug == "YES"){z901_dump( $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);};
    // gw 20100327  dont do this it erases all prev drawing        $sys_function_out = "";
            GOTO Z800_END_OF_LINE;
        }
        $s_map  = $ar_line_details[5];
        $s_group  = $ar_line_details[6];
        $s_temp_value = $ar_line_details[7];
        GOTO B500_DO;
    B210_do_V2:
    B500_DO:
        $s_page_current = "1";
        $s_page_limit = "35";
        if (isset($_SESSION['ko_display_page_limit']))
        {
            $s_page_limit = $_SESSION['ko_display_page_limit'];
        }

        IF (strpos($s_url_siteparams,"^spage") === false)
        {
            GOTO B520_DO;
        }
    //        urlvalue|v1|url_mode|mode|end
        $s_temp = "urlvalue|v1|not needed|spage|end";
        $s_page_current = $class_main->clmain_u500_do_url_line($dbcnx,$s_temp,"NO",$s_details_def,$s_details_data,$s_sessionno,$s_url_siteparams);
        if (strpos($s_page_current,"|^%##%^|") ===false)
        {
        }else{
            $ar_line_details = explode("|^%##%^|",$s_page_current);
    //junk        $s_page_current = $ar_line_details[0];
            $s_page_current = $ar_line_details[1];
            $s_temp = "";
            GOTO B520_DO;
        };

    // set the current page and page limit
    #p%!_postv_page_prev_!%#p
    B520_DO:
        //        $sys_function_out .= "BEFORE the dataloop<bR>";
        $s_temp_value = STR_REPLACE("#P","|",$s_temp_value);
        $s_temp_value = STR_REPLACE("#p","|",$s_temp_value);
        $sys_function_out .= clp_jl_Pf_b200_do_dataloop($dbcnx,$s_temp_value,$class_main->clmain_set_map_path_n_name($ko_map_path,$s_map),$s_group,"no",$s_details_def,$s_details_data,$s_sessionno,$s_page_current,$s_page_limit);
    //            $sys_function_out .= "AFTER the dataloop<bR>";
            goto Z800_END_OF_LINE;
    B900_END:

    C000_START:
    //*urlvalue|v1|result field name|url parameter name
    //* add the result_field_name to the details_def with the url_parameter_name from the url
        IF (strtoupper(substr($s_line_in,0,8)) <> "URLVALUE")
        {
            GOTO C900_END;
        }
        $s_temp = $class_main->clmain_u500_do_url_line($dbcnx,$s_line_in,"no",$s_details_def,$s_details_data,$s_sessionno,$s_url_siteparams);
        if (strpos($s_temp,"|^%##%^|") ===false)
        {
        }else{
            $sys_function_out = $s_temp;
            $s_temp = "";
            goto Z800_END_OF_LINE;
        };
    C900_END:
    D100_DO_APP_CLASS:
    //*app_class|v1|result field name|application class function and paramaters
    //* add the result_field_name to details_def after running the function in the app_class
        IF (strtoupper($ar_line_details[0]) == "APP_CLASS")
        {
            $s_temp = Pf_b600_do_app_class($dbcnx,$s_line_in,"no",$s_details_def,$s_details_data,$s_sessionno);
            if(strtoupper(trim($ar_line_details[4]," ")) == 'YES')
            {
               echo "<br>doing APP_CLASS - LINE ".$s_line_in."<br>".$s_temp."<br>";
            }
            $sys_function_out = $s_temp;
            $s_temp = "";
            goto Z800_END_OF_LINE;
        }

    //*app_class|v1|result field name|application class function and paramaters
    //app_classIF|v1|url_MODE|=|ADDITEMNEXT|b170_result|clapp_B170_pickssccitems(#p%!_url_sscc_no_!%#p, NO)|NO|END
        IF (strtoupper($ar_line_details[0]) <> "APP_CLASSIF")
        {
            GOTO D900_END;
        }

        $s_field_to_check = $ar_line_details[2];
        $s_comparison_operator = $ar_line_details[3];
        $s_compare_value  = $ar_line_details[4];
        $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  d100 ");
        $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_D100 ");

        if ($s_true == "FALSE")
        {
            IF ($sys_debug == "YES"){z901_dump( $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);};
            $sys_function_out = "";
            GOTO Z800_END_OF_LINE;
        }

    //    DIE ("<BR>GW DIR UT JCL D100 Dl  s_true".$s_true."  s_line_in=".$s_line_in);

        $s_line_in = "APP_CLASS|V1|".$ar_line_details[5]."|".$ar_line_details[6]."|".$ar_line_details[7];
        $s_temp = Pf_b600_do_app_class($dbcnx,$s_line_in,"no",$s_details_def,$s_details_data,$s_sessionno);
        if(strtoupper(trim($ar_line_details[7]," ")) == 'YES')
        {
           echo "<br>doing APP_CLASSif - LINE ".$s_line_in."<br>".$s_temp."<br>";
        }

        $sys_function_out = $s_temp;
        $s_temp = "";
        $s_temp = "";
        goto Z800_END_OF_LINE;

    D900_END:



/*
    E800_START:
        IF (strtoupper(substr($s_line_in,0,11)) <> "ADD_DETAILS")
        {
            GOTO E900_END;
        }
        $s_temp = $class_main->clmain_u120_do_add_details_line($dbcnx,$s_line_in,"NO",$s_details_def,$s_details_data,$s_sessionno,$s_url_siteparams);
        if (strpos($s_temp,"|^%##%^|") ===false)
        {
        }else{
            $sys_function_out = $s_temp;
            $s_temp = "";
            goto Z800_END_OF_LINE;
        };
    E900_END:
*/
    F000_RUNDATA:
    //rundata|SELECT * from sales_order where so_order_no = 544854|END
    // adds the results of the sql to the end of run wide details def/data

/*
        IF (strtoupper($ar_line_details[0]) == "RUNDATA")
        {
            GOTO F100_DO_RUNDATA;
        }
        IF (strtoupper($ar_line_details[0]) == "RUNDATAIF")
        {
            GOTO F200_DO_RUNDATAIF;
        }
        GOTO F900_END;
    F100_DO_RUNDATA:
        $s_temp_value = $ar_line_details[1];
        GOTO F400_RUNDATA;
    F200_DO_RUNDATAIF:
        IF (strtoupper($ar_line_details[1]) <> "V1")
        {
            GOTO F210_do_rundataif_v2;
        }
        $s_field_to_check = $ar_line_details[2];
        $s_comparison_operator = $ar_line_details[3];
        $s_compare_value  = $ar_line_details[4];
        $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  F200 sys_debug=".$sys_debug);
        $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_F200 ");
        if ($s_true == "FALSE")
        {
            IF ($sys_debug == "YES"){z901_dump( $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);};
            $sys_function_out = "";
            GOTO Z800_END_OF_LINE;
        }

        $s_temp_value = $ar_line_details[5];
        GOTO F400_RUNDATA;
    F210_do_rundataif_v2:
    F400_RUNDATA:
        $s_temp_value = STR_REPLACE("#P","|",$s_temp_value);
        $s_temp_value = STR_REPLACE("#p","|",$s_temp_value);
        $s_temp = $class_main->clmain_u300_set_rundata($dbcnx,$s_temp_value,"no",$s_details_def,$s_details_data,$s_sessionno);
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $ar_line_details[0];
        $s_details_data = $ar_line_details[1];
        $s_temp = "";

    //    DIE ( "<br><br>DIE RUNDATA<br>".$s_details_def."<br>".$s_details_data);


        goto Z800_END_OF_LINE;
    F900_END:

    G000_START:
    //    outtofileonly|filename=jobfileno.txt|fileonly=yes|end
        IF (strtoupper($ar_line_details[0]) <> "OUTPUTTOTEXT")
        {
            GOTO G900_END;
        }
        $s_output_to_file = "YES";
        $s_output_to_filename = $ar_line_details[1];
        $s_output_to_filename =  substr($s_output_to_filename,strpos($s_output_to_filename,"=") + 1);
        $s_output_to_filename = STR_REPLACE("#P","|",$s_output_to_filename);
        $s_output_to_filename = STR_REPLACE("#p","|",$s_output_to_filename);
        $s_output_to_filename = $class_main->clmain_v200_load_line( $s_output_to_filename,$s_details_def,$s_details_data,"NO",$s_sessionno," cl_proc_jobline  g900a  ");

        $s_output_to_fileonly = $ar_line_details[2];
        $s_output_to_fileonly =  substr($s_output_to_fileonly,strpos($s_output_to_fileonly,"=") + 1);

        $s_output_to_atend = $ar_line_details[3];
        $s_output_to_atend =  substr($s_output_to_atend,strpos($s_output_to_atend,"=") + 1);
        $s_output_to_atend = STR_REPLACE("#P","|",$s_output_to_atend);
        $s_output_to_atend = STR_REPLACE("#p","|",$s_output_to_atend);
        $s_output_to_atend = $class_main->clmain_v200_load_line( $s_output_to_atend,$s_details_def,$s_details_data,"NO",$s_sessionno," cl_proc_jobline  g900b  ");
        $s_output_to_atend_do =  $s_output_to_atend;
        $s_output_to_atend =  substr($s_output_to_atend,0,strpos($s_output_to_atend,":"));
        $s_output_to_atend_do =  substr($s_output_to_atend_do,strpos($s_output_to_atend_do,":") + 1);
    G900_END:




    I_000_DO_FUNCTION:
    //DOFUNCTION|Functionname|details_def_name|parameter|END
    // dofunction|countfolderfiles|folder_cnt|folder_cnt_1|end
    // adds the results of the sql to the end of run wide details def/data
        IF (strtoupper($ar_line_details[0]) == "DOFUNCTION")
        {
            GOTO I_100_DO;
        }
        IF (strtoupper($ar_line_details[0]) == "DOFUNCTIONIF")
        {
            GOTO I_200_DO_IF;
        }
        GOTO I_900_END;
    I_100_DO:
        $s_function = $ar_line_details[1];
        $s_def_name = $ar_line_details[2];
        $s_paramater = $ar_line_details[3];
        GOTO I_400_RUN;
    I_200_DO_IF:
        IF (strtoupper($ar_line_details[1]) <> "V1")
        {
            GOTO I_210_do_if_v2;
        }
        $s_field_to_check = $ar_line_details[2];
        $s_comparison_operator = $ar_line_details[3];
        $s_compare_value  = $ar_line_details[4];
        $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  i200 ");
        $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_i200 ");
        if ($s_true == "FALSE")
        {
            IF ($sys_debug == "YES"){z901_dump( $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);};
            $sys_function_out = "";
            GOTO Z800_END_OF_LINE;
        }

        $s_function = $ar_line_details[5];
        $s_def_name = $ar_line_details[6];
        $s_paramater = $ar_line_details[7];
        GOTO I_400_RUN;
    I_210_do_if_v2:
    I_400_RUN:
        IF (trim(strtoupper($ar_line_details[1])," ") == "COUNTFOLDERFILES")
        {
            $filecount = count(glob("".$s_paramater));
            $s_details_def = $s_details_def."|".$s_def_name;
            $s_details_data = $ar_line_details[1]."|".$filecount;
            goto Z800_END_OF_LINE;
        }

        goto Z800_END_OF_LINE;
    I_900_END:

    J_000_SUSPEND_REFRESH:
    //suspend_refresh|from=16:19|to=16:25|map=owl_bdymain_suspend.htm|loop=suspended_loop|end
        IF (strtoupper($ar_line_details[0]) == STRTOUPPER("suspend_refresh"))
        {
            GOTO J_100_DO;
        }
        IF (strtoupper($ar_line_details[0]) == STRTOUPPER("suspend_refreshif"))
        {
            GOTO J_200_DO_IF;
        }
        GOTO J_900_END;
    J_100_DO:
        $s_suspend_from = $ar_line_details[1];
        $s_suspend_to = $ar_line_details[2];
        $s_map = $ar_line_details[3];
        $s_loop  = $ar_line_details[4];
        $s_refresh  = $ar_line_details[5];
        GOTO J_400_RUN;
    J_200_DO_IF:
        IF (strtoupper($ar_line_details[1]) <> "V1")
        {
            GOTO J_210_do_if_v2;
        }
        $s_field_to_check = $ar_line_details[2];
        $s_comparison_operator = $ar_line_details[3];
        $s_compare_value  = $ar_line_details[4];
        $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  F200 ");
        $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_F200 ");
        if ($s_true == "FALSE")
        {
            IF ($sys_debug == "YES"){z901_dump( $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);};
            $sys_function_out = "";
            GOTO Z800_END_OF_LINE;
        }

        $s_suspend_from = $ar_line_details[5];
        $s_suspend_to = $ar_line_details[6];
        $s_map = $ar_line_details[7];
        $s_loop  = $ar_line_details[8];
        $s_refresh  = $ar_line_details[9];
        GOTO J_400_RUN;
    J_210_do_if_v2:
    J_400_RUN:
        $s_suspend_from = str_replace("from=","",$s_suspend_from);
        $s_suspend_to = str_replace("to=","",$s_suspend_to);
        $s_map = str_replace("map=","",$s_map);
        $s_loop  = str_replace("loop=","",$s_loop);

        $s_suspend_from = str_replace("FROM=","",$s_suspend_from);
        $s_suspend_to = str_replace("TO=","",$s_suspend_to);
        $s_map = str_replace("MAP=","",$s_map);
        $s_loop  = str_replace("LOOP=","",$s_loop);

        $s_refresh  =  substr($s_refresh,strpos($s_refresh,"=")+1);

        $s_suspend_from_chk = str_replace(":","",$s_suspend_from)."00";
        $s_suspend_to_chk = str_replace(":","",$s_suspend_to."00");

        $now_hm = date("His");
        $s_details_def = $s_details_def."|suspend_from|suspend_to|suspend_now|suspend_from_chk|suspend_to_chk|suspend_secs|suspend_now";
        $s_details_data = $s_details_data."|".$s_suspend_from."|".$s_suspend_to."|".$now_hm."|".$s_suspend_from_chk."|".$s_suspend_to_chk."|".$s_refresh."|".$now_hm;

        $s_suspend_from = str_replace(":","",$s_suspend_from);
        $s_suspend_to = str_replace(":","",$s_suspend_to);

        if ($now_hm > $s_suspend_from_chk)
        {
            if ($now_hm < $s_suspend_to_chk)
            {
                goto J_500_DO_SUSPEND;
            }
        }
        goto  J_900_END;
    J_500_DO_SUSPEND:
        $sys_function_out .= pf_b100_DO_DRAW_HTML($class_main->clmain_set_map_path_n_name($ko_map_path,$s_map), $s_loop,"NO",$s_details_def,$s_details_data);
        goto Z700_END;
    J_900_END:

    K_000_SQLCHECK:
    //sqlcheck|stopjcl=yes|sql=Select * from log where type = heartbeat and created time = last 20 mins|onfailmap=owl_bdymain_noheartbeat.htm|onfailloop=noheatbeat_loop|refresh_secs=700|end

        IF (strtoupper($ar_line_details[0]) == STRTOUPPER("SQLCHECK"))
        {
            GOTO K_100_DO;
        }
        IF (strtoupper($ar_line_details[0]) == STRTOUPPER("sqlcheckif"))
        {
            GOTO K_200_DO_IF;
        }
        GOTO K_900_END;
    K_100_DO:
        $s_stop_jcl =$ar_line_details[1];
        $s_sql= $ar_line_details[2];
        $s_map = $ar_line_details[3];
        $s_loop  = $ar_line_details[4];
        $s_refresh  = $ar_line_details[5];
        GOTO K_400_RUN;
    K_200_DO_IF:
        IF (strtoupper($ar_line_details[1]) <> "V1")
        {
            GOTO K_210_do_if_v2;
        }
        $s_field_to_check = $ar_line_details[2];
        $s_comparison_operator = $ar_line_details[3];
        $s_compare_value  = $ar_line_details[4];
        $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  k200 ");
        $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_k200 ");
        if ($s_true == "FALSE")
        {
            IF ($sys_debug == "YES"){z901_dump( $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);};
            $sys_function_out = "";
            GOTO Z800_END_OF_LINE;
        }

        $s_stop_jcl =$ar_line_details[5];
        $s_sql= $ar_line_details[6];
        $s_map = $ar_line_details[7];
        $s_loop  = $ar_line_details[8];
        $s_refresh  = $ar_line_details[9];
        GOTO K_400_RUN;
    K_210_do_if_v2:
    K_400_RUN:
        $s_stop_jcl =  substr($s_stop_jcl,strpos($s_stop_jcl,"=")+1);
        $s_sql = substr($s_sql,strpos($s_sql,"=")+1);
        $s_map = substr($s_map,strpos($s_map,"=")+1);
        $s_loop  = substr($s_loop,strpos($s_loop,"=")+1);
        $s_refresh  =  substr($s_refresh,strpos($s_refresh,"=")+1);

        $s_details_def = $s_details_def."|s_stop_jcl|s_sql";
        $s_details_data = $s_details_data."|".$s_stop_jcl."|".$s_sql;


        // run the sql
        // if got records goto end

        // draw html
        $sys_function_out .= pf_b100_DO_DRAW_HTML($class_main->clmain_set_map_path_n_name($ko_map_path,$s_map), $s_loop,"NO",$s_details_def,$s_details_data);
        // if fatal goto z900_end
        if ($s_stop_jcl == "YES") {
            goto Z900_EXIT;
        }
        // else goto k900_end
        goto  K_900_END;
    K_500_DO_SUSPEND:
        $sys_function_out .= pf_b100_DO_DRAW_HTML($class_main->clmain_set_map_path_n_name($ko_map_path,$s_map), $s_loop,"NO",$s_details_def,$s_details_data);
        goto Z700_END;
    K_900_END:

    L_000_UPDATE_RECORD:
    //"update_record|V1|tablename|uk_fieldname|uk_value|DEBUG|set statement|end";
    //"update_recordif|v1|session_udp_active_device|equal|all|tablename|uk_fieldname|uk_value|DEBUG|set statement|end";
        IF (strtoupper($ar_line_details[0]) == STRTOUPPER("UPDATE_RECORD"))
        {
            GOTO L_100_DO;
        }
        IF (strtoupper($ar_line_details[0]) == STRTOUPPER("UPDATE_RECORDif"))
        {
            GOTO L_200_DO_IF;
        }
        GOTO L_900_END;
    L_100_DO:
    // gw20101222 - no change to the inbound line     $s_line_in = $s_line_in;
        GOTO L_400_RUN;
    L_200_DO_IF:
        IF (strtoupper($ar_line_details[1]) <> "V1")
        {
            GOTO L_210_do_if_v2;
        }
        $s_field_to_check = $ar_line_details[2];
        $s_comparison_operator = $ar_line_details[3];
        $s_compare_value  = $ar_line_details[4];
        $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  l200 ");
        $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_l200 ");
        if ($s_true == "FALSE")
        {
            IF ($sys_debug == "YES"){z901_dump( $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);};
            $sys_function_out = "";
            GOTO Z800_END_OF_LINE;
        }
    //GW20101222 - ****************  NOT TESTED ******************
    // gw20101222 - build s_line_in with out the if stuff
        $s_line_in = "UPDATE_RECORD|V1|".$ar_line_details[5]."|".$ar_line_details[6]."|".$ar_line_details[7]."|".$ar_line_details[8]."|".$ar_line_details[9]."|".$ar_line_details[10];
        GOTO L_400_RUN;
    L_210_do_if_v2:
    L_400_RUN:
    // GW20110116 - DOES NOT APPLY TO UTJCL YET - SEE UT_ACTION FOR THE PROCESS TO MAKE THIS WORK
    /*
        IF (strpos(strtoupper($s_line_In), "UPDATE_DATA_BLOB_WITH_DB_FIELDS") === false)
        {
            goto L_500_SKIP_DATA_BLOB;
        }

        $s_data_blob_fields = "";
    L_410_build_data_blob_fields:
    // IF LINE HAS UPDATE_ALL_DATA_BLOB
    // GET ALL THE POSTV VALUES WITH _DB_ IN THEM
        $array_def = explode("|",$s_details_def);
        $array_data = explode("|",$s_details_data);

        if (count($array_def)<> count($array_data))
        {
    // do nothting yet gw20110116
        }
        for ($i2 = 0; $i2 < count($array_def); $i2++)
        {
            IF (strpos($array_def[$i2],"_DB_") === false)
            {}else
            {
                $s_tmp = $array_def[$i2];
                $s_tmp = substr($s_tmp,strpos($s_tmp,"_DB_"));
                $s_tmp = str_replace("_DB_","",$s_tmp);
                $s_data_blob_fields = $s_data_blob_fields."|".$s_tmp."=".$array_data[$i2];
            }
       }
    l_420_get_original_data_blob:
    // GET THE RECORD FOR THE KEY

    // UPDATE THE RECORD_DATA_BLOB VALUE WITH THE SCREEN VALUES

    //    FOR EACH ENTRY CHECK_FIELD
    //        DO THE CLMAIN_INSERT XML  -=WHICH WILL MODIFY THE VALUE IF IT EXITS
    //    E

        $s_upd_data_blob = ' DATA_BLOB = "'.$s_data_blob.'"';
        $s_line_in = "UPDATE_RECORD|V1|".$ar_line_details[2]."|".$ar_line_details[3]."|".$ar_line_details[4]."|".$ar_line_details[5]."|".$s_upd_data_blob."|".$ar_line_details[10];
    L_500_SKIP_DATA_BLOB:
    */
/*
        $s_temp = $class_sql->sql_a200_update_record($dbcnx,$s_line_in,"no",$s_details_def,$s_details_data,$s_sessionno);
        IF (STRPOS($s_temp,"|^%##%^|")  === FALSE)
        {}ELSE{
            $ar_line_details = explode("|^%##%^|",$s_temp);
            $s_details_def = $s_details_def."|".$ar_line_details[0];
            $s_details_data = $s_details_data."|".$ar_line_details[1];
            $s_temp = "";
        }
        goto Z800_END_OF_LINE;
    L_900_END:

    //\command|filecopy|(from filename)#p%!_SESSION_ko_map_path_!%#p#p%!_jcl_outfilenameonly_!%#p|(tofilename)z:\#p%!_jcl_outfilename_!%#p
    O_000_RUN_SYSCOMAND:
        IF (strtoupper($ar_line_details[0]) == STRTOUPPER("RUN_SYSCOMMAND"))
        {
            GOTO O_100_DO;
        }
        IF (strtoupper($ar_line_details[0]) == STRTOUPPER("RUN_SYSCOMMANDIF"))
        {
            GOTO O_200_DO_IF;
        }
        GOTO O_900_END;
    O_100_DO:
        GOTO O_400_RUN;
    O_200_DO_IF:
        IF (strtoupper($ar_line_details[1]) <> "V1")
        {
            GOTO O_210_do_if_v2;
        }
        $s_field_to_check = $ar_line_details[2];
        $s_comparisoO_operator = $ar_line_details[3];
        $s_compare_value  = $ar_line_details[4];
        $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  o200 ");
        $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparisoO_operator,$s_compare_value,"no","utjcO_o200 ");
        if ($s_true == "FALSE")
        {
            IF ($sys_debug == "YES"){z901_dump( $sys_debug_text." failed value ".$s_field_value." ".$s_comparisoO_operator." ".$s_compare_value);};
            $sys_functioO_out = "";
            GOTO Z800_END_OF_LINE;
        }
    //GW20101222 - ****************  NOT TESTED ******************
    // gw20101222 - build s_line_in with out the if stuff
        $s_line_in = "RUN_SYSCOMMAND|".$ar_line_details[5]."|".$ar_line_details[6]."|".$ar_line_details[7];
        GOTO O_400_RUN;
    O_210_do_if_v2:
    O_400_RUN:
        $s_temp = $class_main->clmain_v955_run_syscommand($s_line_in,"no","jcl_o400",$s_details_def,$s_details_data,$s_sessionno);
        IF (STRPOS($s_temp,"|^%##%^|")  === FALSE)
        {}ELSE{
            $ar_line_details = explode("|^%##%^|",$s_temp);
            $s_details_def = $s_details_def."|".$ar_line_details[0];
            $s_details_data = $s_details_data."|".$ar_line_details[1];
            $s_temp = "";
        }
        goto Z800_END_OF_LINE;
    O_900_END:
    //Add_utl_activity - see the u540_utl_activty explaination for paramaters
    /*    parameters = <br>
        clmain_u540_utl_activity_log($ps_dbcnx,<br>
        $ps_rectype,<br>
        $ps_companyid,<br>
        $ps_userid,<br>
        $ps_summary ,<br>
        $ps_data_blob,<br>
        $ps_key1, and  $ps_key1def, <br>
        $ps_key2, $ps_key2def, <br>
        $ps_key3, $ps_key3def, <br>
        $ps_key4, $ps_key4def,<br>
        $ps_key5, $ps_key5def<br>
    */
/*    P_000_ADD_ACTIVITY:
        IF (strtoupper($ar_line_details[0]) == STRTOUPPER("ADD_UTL_ACTIVITY"))
        {
            GOTO P_100_DO;
        }
//        IF (strtoupper($ar_line_details[0]) == STRTOUPPER("ADD_UTL_ACTIVITYIF"))
//        {
//            GOTO P_200_DP_IF;
//        }
        GOTO P_900_END;
    P_100_DO:
        GOTO P_400_RUN;

/*
    P_200_DP_IF:
        IF (strtoupper($ar_line_details[1]) <> "V1")
        {
            GOTO P_210_dP_if_v2;
        }
        $s_field_to_check = $ar_line_details[2];
        $s_comparisoo_operator = $ar_line_details[3];
        $s_compare_value  = $ar_line_details[4];
        $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  o200 ");
        $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparisoo_operator,$s_compare_value,"no","utjcP_o200 ");
        if ($s_true == "FALSE")
        {
            IF ($sys_debug == "YES"){z901_dump( $sys_debug_text." failed value ".$s_field_value." ".$s_comparisoo_operator." ".$s_compare_value);};
            $sys_function_out = "";
            GOTO Z800_END_OF_LINE;
        }

        $ar_line_details[6] = $class_main->clmain_v200_load_line($ar_line_details[6],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  p4d ");
        $ar_line_details[7] = $class_main->clmain_v200_load_line($ar_line_details[7],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  p4e ");
        $ar_line_details[8] = $class_main->clmain_v200_load_line($ar_line_details[8],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  p4b ");
        $ar_line_details[9] = $class_main->clmain_v200_load_line($ar_line_details[9],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  p4b ");
        $ar_line_details[10] = $class_main->clmain_v200_load_line($ar_line_details[10],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  p4h ");
        $ar_line_details[12] = $class_main->clmain_v200_load_line($ar_line_details[12],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  p4j ");
        $ar_line_details[14] = $class_main->clmain_v200_load_line($ar_line_details[14],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  p4l ");
        $ar_line_details[16] = $class_main->clmain_v200_load_line($ar_line_details[16],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  p4n ");
        $ar_line_details[18] = $class_main->clmain_v200_load_line($ar_line_details[18],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  p4a ");

        $tmp = $class_main->clmain_u540_utl_activity_log($dbcnx,$ar_line_details[5],$ar_line_details[6],$ar_line_details[7],$ar_line_details[8],$ar_line_details[9],$ar_line_details[10],$ar_line_details[11],$ar_line_details[12],$ar_line_details[13],$ar_line_details[14],$ar_line_details[15],$ar_line_details[16],$ar_line_details[17],$ar_line_details[18],$ar_line_details[19]);
        goto Z800_END_OF_LINE;
    P_210_dP_if_v2:

*/

    P_400_RUN:
/*
    // change the values on the lines
        $ar_line_details[3] = $class_main->clmain_v200_load_line($ar_line_details[3],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  p4a ");
        $ar_line_details[4] = $class_main->clmain_v200_load_line($ar_line_details[4],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  p4b ");
        $ar_line_details[5] = $class_main->clmain_v200_load_line($ar_line_details[5],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  p4b ");
        $ar_line_details[6] = $class_main->clmain_v200_load_line($ar_line_details[6],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  p4b ");
        $ar_line_details[7] = $class_main->clmain_v200_load_line($ar_line_details[7],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  p4e ");
        $ar_line_details[9] = $class_main->clmain_v200_load_line($ar_line_details[9],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  p4g ");
        $ar_line_details[11] = $class_main->clmain_v200_load_line($ar_line_details[11],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  p4i ");
        $ar_line_details[13] = $class_main->clmain_v200_load_line($ar_line_details[13],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  p4k ");
        $ar_line_details[15] = $class_main->clmain_v200_load_line($ar_line_details[15],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"cl_proc_jobline  p4m ");

        $tmp = $class_main->clmain_u540_utclmain_u300_set_rundatal_activity_log($dbcnx,$ar_line_details[2],$ar_line_details[3],$ar_line_details[4],$ar_line_details[5],$ar_line_details[6],$ar_line_details[7],$ar_line_details[8],$ar_line_details[9],$ar_line_details[10],$ar_line_details[11],$ar_line_details[12],$ar_line_details[13],$ar_line_details[14],$ar_line_details[15],$ar_line_details[16]);
        goto Z800_END_OF_LINE;
    P_900_END:

    Z800_END_OF_LINE:

}
-->
*/
?>
