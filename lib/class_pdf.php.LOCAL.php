<?php

// pkntest 20120724-1 + pkntest 20120724-2

    require('lib/pdf/fpdf.php');

    class PDF extends FPDF
    {
        function Header()
        {
            global $data_array;
            global $header_array;
            if(!empty($header_array) && !empty($data_array)) {
                $this->map_doPdfLoop($header_array,$data_array);
            }
        }

        function Footer()
        {
            //Position at 1.5 cm from bottom
            //$this->SetY(-15);
            //Arial italic 8
            //$this->SetFont('Arial','I',8);
            //Page number
            //$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
            global $footer_array;
            global $data_array;

            if(!empty($footer_array) && !empty($data_array))
            {
                $this->map_doPdfLoop($footer_array,$data_array);
            }
        }

        function PageFooter()
        {
            global $footer_array;
            global $data_array;

            /*
            if(!empty($footer_array) && !empty($data_array))
            {
                $this->map_doPdfLoop($footer_array,$data_array);
            } */
        }


        function map_doPdfLoop($map_array,$data_array)
        {
            $objMap = new Map();
            //echo 'map_array='.'<br>';
            //print_r($map_array);
            //exit();
            $MaxWidth=$this->maxWidth;
            $MaxHeight=$this->maxHeight;
            $offset=0.06;
            $z=0;
            $l_font_f="";
            $l_font_st="";
            $l_font_si="";
            $set_col_r="";
            $set_col_g="";
            $set_col_b="";
            foreach($map_array as $key=>$val)
            {
                $line_type = substr($val,1,3);
                //echo 'val='.$val.'<br>';
                //echo 'line_type='.$line_type.'<br>';
                //exit;
                if(strtoupper($line_type)==="PNM") {
                    $splitPrm = $this->handlePrm($val);
                    $pnm=$splitPrm[5]." ".$this->PageNo()."/{nb}";
                    if($set_col_r!=$splitPrm['C']['r'] || $set_col_g!=$splitPrm['C']['g'] || $set_col_b!=$splitPrm['C']['b']) {
                        $set_col_r=$splitPrm['C']['r'];
                        $set_col_g=$splitPrm['C']['g'];
                        $set_col_b=$splitPrm['C']['b'];
                        $this->SetTextColor($splitPrm['C']['r'],$splitPrm['C']['g'],$splitPrm['C']['b']);
                    }
                    if($l_font_f!=$splitPrm[3] || $l_font_st!=$splitPrm[7] || $l_font_si!=$splitPrm[4]) {
                        $l_font_f=$splitPrm[3];
                        $l_font_st=$splitPrm[7];
                        $l_font_si=$splitPrm[4];
                        $this->SetFont($splitPrm[3],$splitPrm[7],$splitPrm[4]);
                    }
                    $this->SetXY(($splitPrm[1]+$offset),($splitPrm[2]+$offset));
                    $this->Cell($this->GetStringWidth($pnm),0,$pnm);
                    $this->SetTextColor(0);
                }
                if(strtoupper($line_type)==="BOX") {
                    $splitBox = $objMap->handleBox($val,$MaxWidth,$MaxHeight);
                    $this->SetFillColor($splitBox[5],$splitBox[6],$splitBox[7]);
                    $this->Rect($splitBox[1],$splitBox[2],$splitBox[3],$splitBox[4],"F");
                }
                elseif(strtoupper($line_type)==="PRM") {
                    $splitPrm = $objMap->handlePrm($val);

                    if($splitPrm[3]==="C128A") {
                        $imgPath="{$_SESSION['site_uri']}lib/pdf/barcode/image.php?code={$splitPrm[5]}&style=66&".
                                 "type=C128A&width=100&height=40&xres=1&font={$splitPrm[4]}";
                        $this->Image(rtrim($imgPath),($splitPrm[1]+$offset),($splitPrm[2]+$offset),0,0,"PNG");
                    } elseif($splitPrm[3]==="C128B") {
                        $imgPath="{$_SESSION['site_uri']}lib/pdf/barcode/image.php?code={$splitPrm[5]}&style=66&".
                                 "type=C128B&width=200&height=40&xres=1&font={$splitPrm[4]}";
//                                 "type=C128B&width=100&height=40&xres=1&font={$splitPrm[4]}";
                        $this->Image(rtrim($imgPath),($splitPrm[1]+$offset),($splitPrm[2]+$offset),0,0,"PNG");
                    } elseif($splitPrm[3]==="C128C") {
                        $imgPath="{$_SESSION['site_uri']}lib/pdf/barcode/image.php?code={$splitPrm[5]}&style=66&".
                                 "type=C128C&width=100&height=40&xres=1&font={$splitPrm[4]}";
                        $this->Image(rtrim($imgPath),($splitPrm[1]+$offset),($splitPrm[2]+$offset),0,0,"PNG");
                    } elseif($splitPrm[3]==="C39") {
                        $imgPath="{$_SESSION['site_uri']}lib/pdf/barcode/image.php?code={$splitPrm[5]}&style=66&".
                                 "type=C39&width=100&height=40&xres=1&font={$splitPrm[4]}";
                        $this->Image(rtrim($imgPath),($splitPrm[1]+$offset),($splitPrm[2]+$offset),0,0,"PNG");
                    } else {
                        if($set_col_r!=$splitPrm['C']['r'] || $set_col_g!=$splitPrm['C']['g'] || $set_col_b!=$splitPrm['C']['b']) {
                            $set_col_r=$splitPrm['C']['r'];
                            $set_col_g=$splitPrm['C']['g'];
                            $set_col_b=$splitPrm['C']['b'];
                            $this->SetTextColor($splitPrm['C']['r'],$splitPrm['C']['g'],$splitPrm['C']['b']);
                        }
                        if($l_font_f!=$splitPrm[3] || $l_font_st!=$splitPrm[7] || $l_font_si!=$splitPrm[4]) {
                            $l_font_f=$splitPrm[3];
                            $l_font_st=$splitPrm[7];
                            $l_font_si=$splitPrm[4];
                            $this->SetFont($splitPrm[3],$splitPrm[7],$splitPrm[4]);
                        }
                        $this->SetXY(($splitPrm[1]+$offset),($splitPrm[2]+$offset));
                        //Cell(float w , float h , string txt , mixed border , int ln , string align , int fill , mixed link)
                        $this->Cell($this->GetStringWidth($splitPrm[5]),0,$splitPrm[5]);
                        $this->SetTextColor(0);
                    }
                }
                elseif(strtoupper($line_type)==="PIM") {
                    $splitImg = $this->handleImg($val);
                    $strip_img = preg_replace('/%+/','',$splitImg[3]);
                    $imgPath = strtolower($data_array['company_id'])."/images/".trim($strip_img);
                    $h=(($splitImg[6] / 100)* $splitImg[5]);
                    $w=(($splitImg[6] / 100)* $splitImg[4]);
                    $this->Image(rtrim($imgPath),$splitImg[1],$splitImg[2],$w,$h,"JPG");
                }
                elseif(strtoupper($line_type)==="IMG") {
                    $splitImg = $this->handleImg($val);
                    $strip_img = preg_replace('/%+/','',$splitImg[3]);
                    $imgPath = strtolower($data_array['company_id'])."/images/".$data_array[trim($strip_img)];

                    $this->Image(rtrim($imgPath),$splitImg[1],$splitImg[2],0,0,"JPG");
                }
                elseif(strtoupper($line_type)==="VAL")
                {
                    $splitVal = $objMap->handleVal($val);
                    //$endValData[val_max_lines] = $splitVal[5];
                    //$endValData[val_data_def] = $splitVal[6];
                    //$endValData[val_text_colour] = $splitVal[8];
                    //$endValData[val_text_type] = $splitVal[11];
                    //$strip_val = preg_replace('/%+/','',$splitVal[7]);
                    //$text=$data_array[$strip_val];
                    //$this->SetTextColor($splitVal['C']['r'],$splitVal['C']['g'],$splitVal['C']['b']);
                    $text=$splitVal[7];
                    $bc_style=66;
                    $bc_width=250;
//                    $bc_width=100;
                    $bc_height=40;
                    $bc_xres=1;
                    $img_width=($bc_width/72);
                    $img_height=($bc_height/72);
                    $size=$splitVal[4];
                    if(strstr($size,"^")) {
                        $bc_data=explode("^",$splitVal[4]);
                        if(is_array($bc_data)) {
                            $bc_size=$bc_data[0];
                            $bc_width=$bc_data[1];
                            $bc_height=$bc_data[2];
                            $bc_draw_text=$bc_data[3];
                            $bc_xres=$bc_data[4];
                            $img_width=($bc_width/72);
                            $img_height=($bc_height/72);
                        }
                        if($bc_draw_text==="Y") {
                            $bc_style+=136;
                        }
                    }
                    $img_width=number_format($img_width,2);
                    $img_height=number_format($img_height,2);
                    if($splitVal[3]==="C128A") {
                        $imgPath="{$_SESSION['site_uri']}lib/pdf/barcode/image.php?code={$text}&style=$bc_style&".
                                 "type=C128A&width=$bc_width&height=$bc_height&xres=$bc_xres&font={$size}";
                        $this->Image(rtrim($imgPath),($splitVal[1]+$offset),($splitVal[2]+$offset),$img_width,$img_height,"PNG");
                    } elseif($splitVal[3]==="C128B") {
                        $imgPath="{$_SESSION['site_uri']}lib/pdf/barcode/image.php?code={$text}&style=$bc_style&".
                                 "type=C128B&width=$bc_width&height=$bc_height&xres=$bc_xres&font={$size}";
                        $this->Image(rtrim($imgPath),($splitVal[1]+$offset),($splitVal[2]+$offset),$img_width,$img_height,"PNG");
                    } elseif($splitVal[3]==="C128C") {
                        $imgPath="{$_SESSION['site_uri']}lib/pdf/barcode/image.php?code={$text}&style=$bc_style&".
                                 "type=C128C&width=$bc_width&height=$bc_height&xres=$bc_xres&font={$size}";
                        $this->Image(rtrim($imgPath),($splitVal[1]+$offset),($splitVal[2]+$offset),$img_width,$img_height,"PNG");
                    } elseif($splitVal[3]==="C39") {
                        $imgPath="{$_SESSION['site_uri']}lib/pdf/barcode/image.php?code={$text}&style=$bc_style&".
                                 "type=C39&width=$bc_width&height=$bc_height&xres=$bc_xres&font={$size}";
                        $this->Image(rtrim($imgPath),($splitVal[1]+$offset),($splitVal[2]+$offset),$img_width,$img_height,"PNG");
                    } else {
                        if($text==="%PageNo%") {
                            $text=$this->PageNo();
                        }
                        if($l_font_f!=$splitVal[3] || $l_font_st!=$splitVal[9] || $l_font_si!=$size) {
                            $l_font_f=$splitVal[3];
                            $l_font_st=$splitVal[9];
                            $l_font_si=$size;
                            $this->SetFont($splitVal[3],$splitVal[9],$size);
                        }
                        $this->SetXY(($splitVal[1]+$offset),($splitVal[2]+$offset));
                        //if($this->GetStringWidth($text) >($MaxWidth-($splitVal[1]+$this->offset))) {
                        //    $this->MultiCell(($MaxWidth/2)-($splitVal[1]+$this->offset),0.3,$text,0,'L',$splitVal[10]);
                        //} else {
                            $this->Cell($this->GetStringWidth($text),0,$text,0,0,$splitVal[10]);
                        //}
                    }
                }
                elseif(strtoupper($line_type)==="ADP")
                {
                    $this->AddPage();
                }
                $z++;
            }
            //exit();
        }
    }

?>