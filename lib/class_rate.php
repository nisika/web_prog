<?php
//require_once('lib/class_myplib1.php5');;

    class clrate
    {
//##########################################################################################################################################################################
function clrate_a100_rate_this($ps_dbcnx,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_key1,$ps_key2,$ps_key3,$ps_key4,$ps_key5,$ps_options,$ps_reciprocal,$ps_debug_reference_no=FALSE)
{
A100_TEMPLATE_INIT:

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "clrate_a100_rate_this  called from ".$ps_calledfrom;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){echo( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." params = ps_debug,ps_details_def,ps_details_data,ps_sessionno,ps_key1,ps_key2,ps_key3,ps_key4,ps_key5,ps_options");};
    IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." params = ".$ps_debug.",".$ps_details_def.",".$ps_details_data.",".$ps_sessionno.",".$ps_key1.",".$ps_key2.",".$ps_key3.",".$ps_key4.",".$ps_key5.",".$ps_options);};

    $s_debug="NO";
    if (isset($_SESSION['scko_do_rate_debug']))
    {
        $s_debug = $_SESSION['scko_do_rate_debug'];
    }
    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_a100_rate_this called from $ps_calledfrom",$ps_debug_reference_no);
        $this->z901_dump("Do Debug clrate_a100_rate_this params = ps_debug,ps_details_def,ps_details_data,ps_sessionno,ps_key1,ps_key2,ps_key3,ps_key4,ps_key5,ps_options",$ps_debug_reference_no);
        $this->z901_dump("Do Debug clrate_a100_rate_this params = $ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_key1,$ps_key2,$ps_key3,$ps_key4,$ps_key5,$ps_options",$ps_debug_reference_no);
    }

    //global $class_main;
    //global $class_sql;

    $class_sql = new clSqlClient();
    $class_main = new clmain();

    global $gs_rate_uniquekey;
    global $gs_rate_data_blob;

    A199_END_TEMPLATE_INIT:
    $s_datablob = "none";
    $s_ur_id = "none";
    $s_rate_notes = "no notes";
    $ar_line_details = array();
    $s_details_def = "";
    $s_details_data = "";

    $ar_options = array();
    $s_option_runtype = "notset";

    if (strpos($ps_options,"|") === false)
    {
        goto B000_GET_JOB_DETAILS;
    }
    $ar_options = explode("|",$ps_options);
    $s_option_runtype = $ar_options[0];
    $s_recordgroup = $ar_options[13];
    $s_cn_items = $ar_options[14];
    $s_cn_date = $ar_options[15];

    //get the rate for the key values
B000_GET_JOB_DETAILS:
        //IF ($sys_debug == "YES"){ echo ( $sys_debug_text."=".$sys_function_name." get the job ");};
    $s_sql = "";
    $result = "";
    $s_rec_found = "";
    $s_reccount = "0";

    if (trim($s_recordgroup)=='CNDELETA')
    {
        goto B040_CHECK_CNDELETA;
    }

    //$s_sql = "SELECT * FROM utl_rates as raterec where (key1 = '".$ps_key1."')  and (key2 = '".$ps_key2."')  and (key3 = '".$ps_key3."')  and (key4 = '".$ps_key4."')  and (key5 = '".$ps_key5."') and (recordgroup = 'CLIENT' and recordtype = 'RATES')   and ( companyid = '#p%!_COOKIE_ud_companyid_!%#p') order by createdutime asc";
    $s_sql = "SELECT * FROM utl_rates as raterec where (key1 = '".$ps_key1."')  and (key2 = '".$ps_key2."')  and (key3 = '".$ps_key3."')  and (key4 = '".$ps_key4."')  and (key5 = '".$ps_key5."') and (recordgroup = '{$s_recordgroup}' and recordtype = 'RATES') order by createdutime asc";
    //echo 'rate s_sql1='.$s_sql.'<br>';
    $s_sql = str_replace("#p","|",$s_sql);
    $s_sql = str_replace("#P","|",$s_sql);
    $s_sql = $class_main->clmain_v200_load_line( $s_sql,$ps_details_def,$ps_details_data,"no",$ps_sessionno,$sys_function_name."_A");
    //IF ($sys_debug == "YES"){echo ( $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql." ");};
    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_a100_rate_this B000_GET_JOB_DETAILS - s_sql=$s_sql",$ps_debug_reference_no);
    }
    B015_DO:
    $result = mysqli_query($ps_dbcnx,$s_sql);
    $result_date = mysqli_query($ps_dbcnx,$s_sql);
    $s_rec_found = "NO";
    $s_numrows = mysqli_num_rows($result);
    $i_record_count = "0";
    
    if ($s_numrows>0)
    {
        goto B090_REC_FOUND;
    }    

B020_CHECK_ALL_TCO:
    
    $s_sql = "SELECT * FROM utl_rates as raterec where (key1 = 'ALL')  and (key2 = '".$ps_key2."')  and (key3 = '".$ps_key3."')  and (key4 = '".$ps_key4."')  and (key5 = '".$ps_key5."') and (recordgroup = '{$s_recordgroup}' and recordtype = 'RATES') order by createdutime asc";
    //echo 'rate s_sql2='.$s_sql.'<br>';
    $s_sql = str_replace("#p","|",$s_sql);
    $s_sql = str_replace("#P","|",$s_sql);
    $s_sql = $class_main->clmain_v200_load_line( $s_sql,$ps_details_def,$ps_details_data,"no",$ps_sessionno,$sys_function_name."_A");
    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_a100_rate_this B020_CHECK_ALL_TCO - s_sql=$s_sql",$ps_debug_reference_no);
    }
    $result = mysqli_query($ps_dbcnx,$s_sql);
    $result_date = mysqli_query($ps_dbcnx,$s_sql);
    $s_rec_found = "NO";
    $s_numrows = mysqli_num_rows($result);
    
    if ($s_numrows>0)
    {
        goto B090_REC_FOUND;
    }    

B030_CHECK_ALL_CLIENTS:

    $s_sql = "SELECT * FROM utl_rates as raterec where (key1 = '".$ps_key1."')  and (key2 = 'ALL')  and (key3 = '".$ps_key3."')  and (key4 = '".$ps_key4."')  and (key5 = '".$ps_key5."') and (recordgroup = '{$s_recordgroup}' and recordtype = 'RATES') order by createdutime asc";
    //echo 'rate s_sql3='.$s_sql.'<br>';
    //exit();
    $s_sql = str_replace("#p","|",$s_sql);
    $s_sql = str_replace("#P","|",$s_sql);
    $s_sql = $class_main->clmain_v200_load_line( $s_sql,$ps_details_def,$ps_details_data,"no",$ps_sessionno,$sys_function_name."_A");
    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_a100_rate_this B030_CHECK_ALL_CLIENTS - s_sql=$s_sql",$ps_debug_reference_no);
    }
    $result = mysqli_query($ps_dbcnx,$s_sql);
    $result_date = mysqli_query($ps_dbcnx,$s_sql);
    $s_rec_found = "NO";
    $s_numrows = mysqli_num_rows($result);
    
    if ($s_numrows>0)
    {
        goto B090_REC_FOUND;
    }    

    if ($s_numrows ==  "0")
    {
        $s_rate_notes = "No {$s_recordgroup} rates found for client: {$ps_key1}, carrier account: {$ps_key2}, service: {$ps_key3}, orig: {$ps_key4}, dest: {$ps_key5} ";
        IF ($s_debug == "YES")
        {
            //echo ( $sys_debug_text."=".$sys_function_name." there are no records for s_sql = ".$s_sql." ");
            $this->z901_dump("Do Debug clrate_a100_rate_this B030_CHECK_ALL_CLIENTS - there are no records for s_sql = $s_sql",$ps_debug_reference_no);
            $this->z901_dump("Do Debug clrate_a100_rate_this B030_CHECK_ALL_CLIENTS - No {$s_recordgroup} rates found for client: {$ps_key1}, carrier account: {$ps_key2}, service: {$ps_key3}, orig: {$ps_key4}, dest: {$ps_key5}",$ps_debug_reference_no);
        }

        //echo 's_products='.$s_products.'<br>'; 
        //echo 's_rateunit2='.$s_rateunit.'<br>';
        //echo 'ar_options[8]2='.$ar_options[8].'<br>';
        //echo 'ar_options[9]='.$ar_options[9].'<br>';
        //echo 'ps_details_data='.$ps_details_data.'<br>';
        //echo 's_rate_notes='.$s_rate_notes.'<br>';
        GOTO Z990_EXIT;
    }

B040_CHECK_CNDELETA:

    // pkn 20131025 - for ETA calc need to check for town to

    //echo 'ps_options='.$ps_options.'<br>';
    $ar_options = explode("|",$ps_options);
    $s_sender_suburb = $ar_options[5];
    $s_receiver_suburb = $ar_options[6];
    //echo 's_sender_suburb='.$s_sender_suburb.'<br>';
    //echo 's_receiver_suburb='.$s_receiver_suburb.'<br>';

    // pkn 20180730 - firstly check if CNDELETA record exists for rate zone from/to for specific carrier and account that is found

    $s_sql = "SELECT * FROM utl_rates as raterec where (key1 = '".$ps_key1."')  and (key2 = '".$ps_key2."')  and (key3 = '".$ps_key3."')  and (key4 = '".$ps_key4."')  and (key5 = '".$ps_key5."') and (recordgroup = '{$s_recordgroup}' and recordtype = 'RATES') order by createdutime asc";
    //echo 'cndeleta rate s_sql4='.$s_sql.'<br>';
    //exit();
    $s_sql = str_replace("#p","|",$s_sql);
    $s_sql = str_replace("#P","|",$s_sql);
    $s_sql = $class_main->clmain_v200_load_line( $s_sql,$ps_details_def,$ps_details_data,"no",$ps_sessionno,$sys_function_name."_A");
    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_a100_rate_this B040_CHECK_CNDELETA - s_sql=$s_sql",$ps_debug_reference_no);
    }
    $result = mysqli_query($ps_dbcnx,$s_sql);
    $result_date = mysqli_query($ps_dbcnx,$s_sql);
    $s_rec_found = "NO";
    $s_numrows = mysqli_num_rows($result);

    if ($s_numrows>0)
    {
        goto B090_REC_FOUND;
    }

    // pkn 20140916 - firstly check if CNDELETA record exists for rate zone from/to that is found

    $s_sql = "SELECT * FROM utl_rates as raterec where (key1 = '".$ps_key1."')  and (key2 = 'ALL')  and (key3 = '".$ps_key3."')  and (key4 = '".$ps_key4."')  and (key5 = '".$ps_key5."') and (recordgroup = '{$s_recordgroup}' and recordtype = 'RATES') order by createdutime asc";
    //echo 'cndeleta rate s_sql4a='.$s_sql.'<br>';
    //exit();
    $s_sql = str_replace("#p","|",$s_sql);
    $s_sql = str_replace("#P","|",$s_sql);
    $s_sql = $class_main->clmain_v200_load_line( $s_sql,$ps_details_def,$ps_details_data,"no",$ps_sessionno,$sys_function_name."_A");
    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_a100_rate_this B040_CHECK_CNDELETA - s_sql=$s_sql",$ps_debug_reference_no);
    }
    $result = mysqli_query($ps_dbcnx,$s_sql);
    $result_date = mysqli_query($ps_dbcnx,$s_sql);
    $s_rec_found = "NO";
    $s_numrows = mysqli_num_rows($result);
    
    if ($s_numrows>0)
    {
        goto B090_REC_FOUND;
    }

    $s_sql = "SELECT * FROM utl_rates as raterec where (key1 = '".$ps_key1."')  and (key2 = 'ALL')  and (key3 = '".$ps_key3."')  and (key4 = '".$ps_key4."')  and (key5 = '".$s_receiver_suburb."') and (recordgroup = '{$s_recordgroup}' and recordtype = 'RATES') order by createdutime asc";
    //echo 'cndeleta rate s_sql4b='.$s_sql.'<br>';
    //exit();
    $s_sql = str_replace("#p","|",$s_sql);
    $s_sql = str_replace("#P","|",$s_sql);
    $s_sql = $class_main->clmain_v200_load_line( $s_sql,$ps_details_def,$ps_details_data,"no",$ps_sessionno,$sys_function_name."_A");
    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_a100_rate_this B040_CHECK_CNDELETA - s_sql=$s_sql",$ps_debug_reference_no);
    }
    $result = mysqli_query($ps_dbcnx,$s_sql);
    $result_date = mysqli_query($ps_dbcnx,$s_sql);
    $s_rec_found = "NO";
    $s_numrows = mysqli_num_rows($result);

    if ($s_numrows>0)
    {
        goto B090_REC_FOUND;
    }

    $s_sql = "SELECT * FROM utl_rates as raterec where (key1 = '".$ps_key1."')  and (key2 = 'ALL')  and (key3 = '".$ps_key3."')  and (key4 = '".$s_sender_suburb."')  and (key5 = '".$ps_key5."') and (recordgroup = '{$s_recordgroup}' and recordtype = 'RATES') order by createdutime asc";
    //echo 'cndeleta rate s_sql5='.$s_sql.'<br>';
    //exit();
    $s_sql = str_replace("#p","|",$s_sql);
    $s_sql = str_replace("#P","|",$s_sql);
    $s_sql = $class_main->clmain_v200_load_line( $s_sql,$ps_details_def,$ps_details_data,"no",$ps_sessionno,$sys_function_name."_A");
    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_a100_rate_this B040_CHECK_CNDELETA - s_sql=$s_sql",$ps_debug_reference_no);
    }
    $result = mysqli_query($ps_dbcnx,$s_sql);
    $result_date = mysqli_query($ps_dbcnx,$s_sql);
    $s_rec_found = "NO";
    $s_numrows = mysqli_num_rows($result);
    
    if ($s_numrows>0)
    {
        goto B090_REC_FOUND;
    }        

    $s_sql = "SELECT * FROM utl_rates as raterec where (key1 = '".$ps_key1."')  and (key2 = 'ALL')  and (key3 = '".$ps_key3."')  and (key4 = '".$s_sender_suburb."')  and (key5 = '".$s_receiver_suburb."') and (recordgroup = '{$s_recordgroup}' and recordtype = 'RATES') order by createdutime asc";
    //echo 'cndeleta rate s_sql6='.$s_sql.'<br>';
    //exit();
    $s_sql = str_replace("#p","|",$s_sql);
    $s_sql = str_replace("#P","|",$s_sql);
    $s_sql = $class_main->clmain_v200_load_line( $s_sql,$ps_details_def,$ps_details_data,"no",$ps_sessionno,$sys_function_name."_A");
    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_a100_rate_this B040_CHECK_CNDELETA - s_sql=$s_sql",$ps_debug_reference_no);
    }
    $result = mysqli_query($ps_dbcnx,$s_sql);
    $result_date = mysqli_query($ps_dbcnx,$s_sql);
    $s_rec_found = "NO";
    $s_numrows = mysqli_num_rows($result);
    
    if ($s_numrows>0)
    {
        goto B090_REC_FOUND;
    }        
        
    if ($s_numrows ==  "0")
    {
        $s_rate_notes = "No {$s_recordgroup} rates found for client: {$ps_key1}, carrier account: {$ps_key2}, service: {$ps_key3}, orig: {$ps_key4}, dest: {$ps_key5} ";

        IF ($s_debug == "YES")
        {
            //echo ( $sys_debug_text."=".$sys_function_name." there are no records for s_sql = ".$s_sql." ");
            $this->z901_dump("Do Debug clrate_a100_rate_this B040_CHECK_CNDELETA - there are no records for s_sql = $s_sql",$ps_debug_reference_no);
            $this->z901_dump("Do Debug clrate_a100_rate_this B040_CHECK_CNDELETA - No {$s_recordgroup} rates found for client: {$ps_key1}, carrier account: {$ps_key2}, service: {$ps_key3}, orig: {$ps_key4}, dest: {$ps_key5}",$ps_debug_reference_no);
        }

        //echo 's_products='.$s_products.'<br>'; 
        //echo 's_rateunit2='.$s_rateunit.'<br>';
        //echo 'ar_options[8]2='.$ar_options[8].'<br>';
        //echo 'ar_options[9]='.$ar_options[9].'<br>';
        //echo 'ps_details_data='.$ps_details_data.'<br>';
        //echo 's_rate_notes='.$s_rate_notes.'<br>';
        GOTO Z990_EXIT;
    }
        
B090_REC_FOUND:

    //echo 'B090_REC_FOUND rate s_sql='.$s_sql.'<br>';

    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_a100_rate_this B090_REC_FOUND for s_sql = $s_sql",$ps_debug_reference_no);
    }

B095_DO_DATE_EFFECTIVE_DATES:

    while( $row_date = mysqli_fetch_array($result_date, MYSQLI_ASSOC) )
    {
        $s_data_blob_date = $row_date['data_blob'];

        $s_products = $class_main->clmain_u592_get_field_from_xml($s_data_blob_date, "products", $sys_debug, "clrate_b100_a", "NA");
        $s_rateunit = $class_main->clmain_u592_get_field_from_xml($s_data_blob_date, "rateunit", $sys_debug, "clrate_b100_a", "NA");

        $s_effectivefromdate = $class_main->clmain_u592_get_field_from_xml($s_data_blob_date, "effectivefromdate", $sys_debug, "clrate_b100_a", "NA");
        $s_effectivetodate = $class_main->clmain_u592_get_field_from_xml($s_data_blob_date, "effectivetodate", $sys_debug, "clrate_b100_a", "NA");

        //echo 's_effectivefromdate='.$s_effectivefromdate.'<br>';
        //echo 's_effectivetodate='.$s_effectivetodate.'<br>';

        if (trim(strtoupper($s_effectivefromdate)) == 'MONDAY' or trim(strtoupper($s_effectivefromdate)) == 'MONDAY')
        {
            goto B096_DO_EFFECTIVE_DAYS;
        }
        if (trim(strtoupper($s_effectivefromdate)) == 'TUESDAY' or trim(strtoupper($s_effectivefromdate)) == 'TUESDAY')
        {
            goto B096_DO_EFFECTIVE_DAYS;
        }
        if (trim(strtoupper($s_effectivefromdate)) == 'WEDNESDAY' or trim(strtoupper($s_effectivefromdate)) == 'WEDNESDAY')
        {
            goto B096_DO_EFFECTIVE_DAYS;
        }
        if (trim(strtoupper($s_effectivefromdate)) == 'THURSDAY' or trim(strtoupper($s_effectivefromdate)) == 'THURSDAY')
        {
            goto B096_DO_EFFECTIVE_DAYS;
        }
        if (trim(strtoupper($s_effectivefromdate)) == 'FRIDAY' or trim(strtoupper($s_effectivefromdate)) == 'FRIDAY')
        {
            goto B096_DO_EFFECTIVE_DAYS;
        }

        if (trim($s_effectivefromdate) == '' or trim($s_effectivefromdate) == '0')
        {
            goto B095_DO_DATE_EFFECTIVE_DATES_EXIT;
        }
        if (trim($s_effectivetodate) == '' or trim($s_effectivetodate) == '0')
        {
            goto B095_DO_DATE_EFFECTIVE_DATES_EXIT;
        }

        $ar_effectivefromdate = explode('/', $s_effectivefromdate);
        $s_effectivefromdate_DD = trim($ar_effectivefromdate[0]);
        if (strlen($s_effectivefromdate_DD) == 1)
        {
            $s_effectivefromdate_DD = '0' . trim($ar_effectivefromdate[0]);
        }
        $s_effectivefromdate_MM = trim($ar_effectivefromdate[1]);
        if (strlen($s_effectivefromdate_MM) == 1)
        {
            $s_effectivefromdate_MM = '0' . trim($ar_effectivefromdate[1]);
        }
        $s_effectivefromdate_YYYY = trim($ar_effectivefromdate[2]);
        $s_effectivefromdate = $s_effectivefromdate_YYYY . $s_effectivefromdate_MM . $s_effectivefromdate_DD;

        $ar_effectivetodate = explode('/', $s_effectivetodate);
        $s_effectivetodate_DD = trim($ar_effectivetodate[0]);
        if (strlen($s_effectivetodate_DD) == 1)
        {
            $s_effectivetodate_DD = '0' . trim($ar_effectivetodate[0]);
        }
        $s_effectivetodate_MM = trim($ar_effectivetodate[1]);
        if (strlen($s_effectivetodate_MM) == 1)
        {
            $s_effectivetodate_MM = '0' . trim($ar_effectivetodate[1]);
        }
        $s_effectivetodate_YYYY = trim($ar_effectivetodate[2]);
        $s_effectivetodate = $s_effectivetodate_YYYY . $s_effectivetodate_MM . $s_effectivetodate_DD;

        //echo 's_cn_date=' . $s_cn_date . '<br>';
        //echo 's_effectivefromdate=' . $s_effectivefromdate . '<br>';
        //echo 's_effectivetodate=' . $s_effectivetodate . '<br>';

        IF ($s_debug == "YES")
        {
            $this->z901_dump("Do Debug clrate_a100_rate_this B095_DO_DATE_EFFECTIVE_DATES for s_cn_date=$s_cn_date, s_effectivefromdate=$s_effectivefromdate,s_effectivetodate=$s_effectivetodate",$ps_debug_reference_no);
        }

        if ($s_cn_date >= $s_effectivefromdate and $s_cn_date <= $s_effectivetodate)
        {
            IF ($s_debug == "YES")
            {
                $this->z901_dump("Do Debug clrate_a100_rate_this B095_DO_DATE_EFFECTIVE_DATES rate record date matched",$ps_debug_reference_no);
            }

            $row=$row_date;
            GOTO B780_GOT_A_RECORD;
        }
    }

    //GOTO B800_NEXT;
    GOTO Z990_EXIT;


B095_DO_DATE_EFFECTIVE_DATES_EXIT:

    if ($s_numrows > "1")
    {
        //IF ($sys_debug == "YES"){ echo ( $sys_debug_text."=".$sys_function_name." thera are no records for s_sql = ".$s_sql." ");};
        $s_rate_notes = "More that 1 rate exists for sql :".$s_sql;
    }

    goto B100_GET_RECORDS;

B096_DO_EFFECTIVE_DAYS:

    $s_day_of_week=date('D',strtotime($s_cn_date));
    $s_day_of_week=trim(strtoupper($s_day_of_week));

    if ($s_day_of_week=='MON')
    {
        $s_day_of_week='MONDAY';
    }
    if ($s_day_of_week=='TUE')
    {
        $s_day_of_week='TUESDAY';
    }
    if ($s_day_of_week=='WED')
    {
        $s_day_of_week='WEDNESDAY';
    }
    if ($s_day_of_week=='THU')
    {
        $s_day_of_week='THURSDAY';
    }
    if ($s_day_of_week=='FRI')
    {
        $s_day_of_week='FRIDAY';
    }
    if ($s_day_of_week=='SAT')
    {
        $s_day_of_week='SATURDAY';
    }
    if ($s_day_of_week=='SUN')
    {
        $s_day_of_week='SUNDAY';
    }

    while( $row_date = mysqli_fetch_array($result_date, MYSQLI_ASSOC) )
    {
        $s_data_blob_date = $row_date['data_blob'];

        $s_products = $class_main->clmain_u592_get_field_from_xml($s_data_blob_date, "products", $sys_debug, "clrate_b100_a", "NA");
        $s_rateunit = $class_main->clmain_u592_get_field_from_xml($s_data_blob_date, "rateunit", $sys_debug, "clrate_b100_a", "NA");

        $s_effectivefromdate = $class_main->clmain_u592_get_field_from_xml($s_data_blob_date, "effectivefromdate", $sys_debug, "clrate_b100_a", "NA");
        $s_effectivetodate = $class_main->clmain_u592_get_field_from_xml($s_data_blob_date, "effectivetodate", $sys_debug, "clrate_b100_a", "NA");

        $s_effectivefromdate = trim(strtoupper($s_effectivefromdate));
        $s_effectivetodate = trim(strtoupper($s_effectivetodate));

        if (trim($s_effectivefromdate) == '' or trim($s_effectivefromdate) == '0')
        {
            goto B096_DO_EFFECTIVE_DAYS_EXIT;
        }
        if (trim($s_effectivetodate) == '' or trim($s_effectivetodate) == '0')
        {
            goto B096_DO_EFFECTIVE_DAYS_EXIT;
        }

        if ($s_day_of_week == $s_effectivefromdate and $s_day_of_week == $s_effectivetodate)
        {
            $row=$row_date;
            GOTO B780_GOT_A_RECORD;
        }
    }

    //GOTO B800_NEXT;
    GOTO Z990_EXIT;

B096_DO_EFFECTIVE_DAYS_EXIT:

    if ($s_numrows > "1")
    {
        //IF ($sys_debug == "YES"){ echo ( $sys_debug_text."=".$sys_function_name." thera are no records for s_sql = ".$s_sql." ");};
        $s_rate_notes = "More that 1 rate exists for sql :".$s_sql;
    }

    goto B100_GET_RECORDS;

B100_GET_RECORDS:
    if ($i_record_count > $s_numrows)
    {
        goto B850_END_RECORDS;
    }
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    IF ($row === false )
    {
        goto B850_END_RECORDS;
    }
    $s_rec_found = "YES";
B150_DO_RECORDS:

    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_a100_rate_this B150_DO_RECORDS",$ps_debug_reference_no);
    }

    $s_data_blob = $row['data_blob'];
    $gs_rate_data_blob = $row['data_blob'];
    $gs_rate_uniquekey = $row['ur_id'];
    IF ($s_option_runtype == "notset")
    {
        GOTO B780_GOT_A_RECORD;
    }
B160_DO_TCORATE:
//TCORATE|RATECO|ACCOUNT|PCF|PCT|TOWNFROM|TOWNTO|SERVICE|RATEUNIT|PRODUCTS|UNITCOUNT|KGS|CUBIC|
    IF ($s_option_runtype <> "TCORATE")
    {
        //GOTO B169_SKIP;
        GOTO B800_NEXT;
    }
    $s_products =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"products",$sys_debug,"clrate_b100_a","NA");
    $s_rateunit =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"rateunit",$sys_debug,"clrate_b100_a","NA");
    //echo 's_rateunit1='.$s_rateunit.'<br>';
    //echo 's_products1='.$s_products.'<br>';
    //echo 'ar_options[8]1='.$ar_options[8].'<br>';
    //echo 'ar_options[9]1='.$ar_options[9].'<br>';
    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_a100_rate_this B160_DO_TCORATE - s_rateunit=$s_rateunit, ar_options[8]=$ar_options[8]",$ps_debug_reference_no);
    }
    if (strtoupper(trim($ar_options[8])) <>  strtoupper(trim($s_rateunit)))
    {
        GOTO B800_NEXT;
    }
    //echo 's_products='.$s_products.'<br>';
    //echo 's_rateunit2='.$s_rateunit.'<br>';
    //echo 'ar_options[8]2='.$ar_options[8].'<br>';
    //echo 'ar_options[9]='.$ar_options[9].'<br>';
    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_a100_rate_this B160_DO_TCORATE - s_products=$s_products, ar_options[9]=$ar_options[9]",$ps_debug_reference_no);
    }
    if (strtoupper(trim($ar_options[9])) == "ALL")
    {
        GOTO B780_GOT_A_RECORD;
    }    
    if (strtoupper(trim($ar_options[9])) <>  strtoupper(trim($s_products)))
    {
        GOTO B800_NEXT;
    }
    GOTO B780_GOT_A_RECORD;
B169_SKIP:

B780_GOT_A_RECORD:

    //echo 'B780_GOT_A_RECORD'.'<br>';

    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_a100_rate_this B780_GOT_A_RECORD",$ps_debug_reference_no);
    }

    //IF ($sys_debug == "YES"){ echo ( $sys_debug_text."=".$sys_function_name." got the job record ");};
    $s_data_blob = $row['data_blob'];
    $s_ur_id = $row['ur_id'];
    $s_rate_notes ="Record located for key values";
    goto C100_CALC_VALUE;
B800_NEXT:
    $i_record_count = $i_record_count + 1;
    GOTO B100_GET_RECORDS;
B850_END_RECORDS:
B900_EXIT:

    // pkn 20160927 no rate records found so exit
    //goto Z900_EXIT;

C100_CALC_VALUE:

    $result = $this->clrate_b100_calc_value($ps_dbcnx,$sys_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$s_data_blob,$ps_options,$ps_reciprocal,$ps_debug_reference_no);
    // pkn 20130726 - cndeleta - delivery performance calculations so go straight to Z990_EXIT
    if (strtoupper(trim($ar_options[8]))=='DAYS')
    {
        $ar_line_details = explode("|^%##%^|",$result);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
        $result = "";
        goto Z990_EXIT;
    }
        
    if (!strpos($result,"|^%##%^|") ===false)
    {
        $ar_line_details = explode("|^%##%^|",$result);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
        $result = "";
    }

Z900_EXIT:

    $s_rate_notes=$s_data_blob;

Z990_EXIT:

    $s_details_def = $s_details_def."|rate_recid|rate_notes|";    
    //echo 'pkn whole s_data_blob='.$s_data_blob.'<br>';
    $s_details_data = $s_details_data."|".$s_ur_id."|".$s_rate_notes."|";
    $sys_function_out = $s_details_def."|^%##%^|".$s_details_data;
    //echo 'sys_function_out='.$sys_function_out.'<br>';
    //exit();
                                                           
    IF ($sys_debug == "YES"){echo( $sys_debug_text."  z900_EXIT sys_function_out =  ".$sys_function_out);};

    return $sys_function_out;

}
//##########################################################################################################################################################################
function clrate_b100_calc_value($ps_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_data_blob,$ps_options,$ps_reciprocal,$ps_debug_reference_no=FALSE)
{

    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";

    $sys_function_name = "debug - clmain_a110_do_sys_class";
    $sys_function_out = "";
    //IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    //IF ($sys_debug == "YES"){echo( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    //IF ($sys_debug == "YES"){echo( $sys_debug_text."=".$sys_function_name."sys_debug,ps_details_def,ps_details_data,ps_sessionno,ps_datablob");};
    //IF ($sys_debug == "YES"){echo( $sys_debug_text."=".$sys_function_name.$sys_debug."|".$ps_details_def."|".$ps_details_data."|".$ps_sessionno."|".$ps_data_blob);};

    $s_debug="YES";
    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_b100_calc_value params = ps_details_def,ps_details_data,ps_sessionno,ps_datablob",$ps_debug_reference_no);
        $this->z901_dump("Do Debug clrate_a100_rate_this params = $ps_details_def,|$ps_details_data,$ps_sessionno,$ps_data_blob",$ps_debug_reference_no);
    }

    //global $class_sql;
    //global $class_apps;
    //global $class_main;

    $class_sql = new clSqlClient();
    $class_main = new clmain();

A001_DEFINE_VARS:


A100_INIT_VARS:
    $ar_rate_fields = array();
    $s_calced_value = "0";
    $s_calc_notes = "";
    $s_cubic_conv_round='N';

    if (strtoupper($_SESSION['site_name'])=="TDXDELNET")
    {
        $s_cubic_conv_round='Y';
    }

    B150_DO_TRANSLATIONS:

    $ar_options = explode("|",$ps_options);
    $s_option_transport_company = trim($ar_options[1]);
    $s_option_company_id = trim($ar_options[2]);
    $s_option_service = trim($ar_options[7]);

    $s_translations_sql="select * from dmjobtrk where trackgroup='TRANSLATION' and transport_company='{$s_option_transport_company}' and company_id = '{$s_option_company_id}' ";
    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_a100_rate_this - B150_DO_TRANSLATIONS - s_translations_sql=$s_translations_sql",$ps_debug_reference_no);
    }
    //echo 's_translations_sql='.$s_translations_sql.'<br>';
    //exit();
    $s_translations_result = mysqli_query($ps_dbcnx,$s_translations_sql);
    while($s_translations_row = mysqli_fetch_array($s_translations_result, MYSQLI_ASSOC))
    {
        $s_details_def=$s_translations_row['Details_def'];
        $s_details_data=$s_translations_row['Details_data'];

        $s_field_name='TRANSLATE_AT';
        $s_translate_at=$this->clrate_d200_convertDatafield($s_details_def,$s_details_data,$s_field_name,0);
        $s_field_name='TRANSLATE_WHAT';
        $s_translate_what=$this->clrate_d200_convertDatafield($s_details_def,$s_details_data,$s_field_name,0);
        $s_field_name='TRANSLATE_WHEN';
        $s_translate_when=$this->clrate_d200_convertDatafield($s_details_def,$s_details_data,$s_field_name,0);
        $s_field_name='TRANSLATE_ACTION';
        $s_translate_action=$this->clrate_d200_convertDatafield($s_details_def,$s_details_data,$s_field_name,0);

        if ($s_translate_at<>'RATE')
        {
            goto B190_GET_NEXT_TRANSLATION;
        }

        if ($s_translate_what=='SETCUBICWEIGHTUP')
        {
            goto B160_SETCUBICWEIGHTUP_TRANSLATION;
        }

        goto B190_GET_NEXT_TRANSLATION;

        B160_SETCUBICWEIGHTUP_TRANSLATION:

        $ar_translate_when=explode('^',$s_translate_when);
        $s_translate_when_rateco=trim($ar_translate_when[0]);
        $s_translate_when_client=trim($ar_translate_when[1]);
        $s_translate_when_service=trim($ar_translate_when[2]);
        $s_translate_when_end=trim($ar_translate_when[3]);

        $ar_translate_action=explode('^',$s_translate_action);
        $s_translate_action_yes_no=trim($ar_translate_action[0]);
        $s_translate_action_end=trim($ar_translate_action[1]);

        if (strtoupper($s_translate_action_yes_no)=='NO')
        {
            goto B190_GET_NEXT_TRANSLATION;
        }

        B161_CHECK_RATECO:

        if ($s_translate_when_rateco=='ALL')
        {
            goto B162_CHECK_CLIENT;
        }

        if ($s_option_transport_company===$s_translate_when_rateco)
        {
            goto B162_CHECK_CLIENT;
        }

        goto B190_GET_NEXT_TRANSLATION;

        B162_CHECK_CLIENT:

        if ($s_translate_when_client=='ALL')
        {
            goto B163_CHECK_SERVICE;
        }

        if ($s_option_company_id===$s_translate_when_client)
        {
            goto B163_CHECK_SERVICE;
        }

        goto B190_GET_NEXT_TRANSLATION;

        B163_CHECK_SERVICE:

        if ($s_translate_when_service=='ALL')
        {
            goto B164_DO_SETCUBICWEIGHTUP;
        }

        if ($s_option_service===$s_translate_when_service)
        {
            goto B164_DO_SETCUBICWEIGHTUP;
        }

        goto B190_GET_NEXT_TRANSLATION;

        B164_DO_SETCUBICWEIGHTUP:

        $s_cubic_conv_round='Y';

        goto B190_GET_NEXT_TRANSLATION;

        B170_SETCUBICWEIGHTUP_TRANSLATION:

        // pkn 20150304 - this is done in class_rate

        goto B190_GET_NEXT_TRANSLATION;

        B190_GET_NEXT_TRANSLATION:
    }

A200_SET_OPTIONS:
    $ar_options = array();
    $s_option_runtype = "notset";

    if (strpos($ps_options,"|") === false)
    {
        goto B100_SET_RATE_DATA;
    }
    $ar_options = explode("|",$ps_options);
    $s_option_runtype = $ar_options[0];
    $s_recordgroup = $ar_options[13];
    $s_cn_items = $ar_options[14];

    B100_SET_RATE_DATA:
    $s_data_blob=$ps_data_blob;
    //echo 's_data_blob='.$s_data_blob.'<br>';
B100_CALC:
    IF ($s_option_runtype <>"notset")
    {
        GOTO B109_SKIP;
    }
    $s_ratebasic =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"ratebasic",$sys_debug,"clrate_b100_a","0");
    $s_calced_value =  $s_calced_value + $s_ratebasic;
    $s_calc_notes = $s_calc_notes."+ratebasic";
    GOTO Z900_EXIT;
B109_SKIP:

B160_DO_TCORATE:
//TCORATE|RATECO|ACCOUNT|PCF|PCT|TOWNFROM|TOWNTO|SERVICE|RATEUNIT|PRODUCTS|UNITCOUNT|KGS|CUBIC|
    IF (trim($s_option_runtype) <> "TCORATE")
    {
        GOTO B169_SKIP;
    }

    //echo 's_data_blob='.$s_data_blob.'<br>';

    $s_blob_ratecompany =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"ratecompany",$sys_debug,"clrate_b100_a","NA");
    $s_blob_rateaccount =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"rateaccount",$sys_debug,"clrate_b100_a","NA");
    $s_blob_rateservice =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"rateservice",$sys_debug,"clrate_b100_a","NA");
    $s_blob_rateunit =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"rateunit",$sys_debug,"clrate_b100_a","NA");
    $s_blob_cubicconv =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"cubicconv",$sys_debug,"clrate_b100_a","0");
    $s_blob_ratereciprocal =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"ratereciprocal",$sys_debug,"clrate_b100_a","NO");
    $s_blob_rateflataccum =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"rateflataccum",$sys_debug,"clrate_b100_a","FLAT");
    $s_blob_itemkgs =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"itemkgs",$sys_debug,"clrate_b100_a","0");
    $s_blob_itemexcess =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"itemexcess",$sys_debug,"clrate_b100_a","0");
    $s_blob_ratefrom =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"ratefrom",$sys_debug,"clrate_b100_a","NA");
    $s_blob_upto1 =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"upto1",$sys_debug,"clrate_b100_a","0");
    $s_blob_upto2 =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"upto2",$sys_debug,"clrate_b100_a","0");
    $s_blob_upto3 =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"upto3",$sys_debug,"clrate_b100_a","0");
    $s_blob_upto4 =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"upto4",$sys_debug,"clrate_b100_a","0");
    $s_blob_upto5 =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"upto5",$sys_debug,"clrate_b100_a","0");
    $s_blob_ratebasic =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"ratebasic",$sys_debug,"clrate_b100_a","0");
    $s_blob_minimum =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"minimum",$sys_debug,"clrate_b100_a","0");
    $s_blob_minimum = str_replace('$','',$s_blob_minimum);
    $s_blob_maximum =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"maximum",$sys_debug,"clrate_b100_a","0");
    $s_blob_rateto =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"rateto",$sys_debug,"clrate_b100_a","NA");
    $s_blob_charge1 =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"charge1",$sys_debug,"clrate_b100_a","0");
    $s_blob_charge2 =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"charge2",$sys_debug,"clrate_b100_a","0");
    $s_blob_charge3 =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"charge3",$sys_debug,"clrate_b100_a","0");
    $s_blob_charge4 =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"charge4",$sys_debug,"clrate_b100_a","0");
    $s_blob_charge5 =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"charge5",$sys_debug,"clrate_b100_a","0");
    $s_blob_effectivefromdate =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"effectivefromdate",$sys_debug,"clrate_b100_a","0");
    $s_blob_effectivetodate =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"effectivetodate",$sys_debug,"clrate_b100_a","0");
    $s_blob_transitdays =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"transitdays",$sys_debug,"clrate_b100_a","0");
    $s_blob_products =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"products",$sys_debug,"clrate_b100_a","NA");
    $s_blob_upto6 =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"upto6",$sys_debug,"clrate_b100_a","0");
    $s_blob_upto7 =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"upto7",$sys_debug,"clrate_b100_a","0");
    $s_blob_upto8 =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"upto8",$sys_debug,"clrate_b100_a","0");
    $s_blob_upto9 =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"upto9",$sys_debug,"clrate_b100_a","0");
    $s_blob_upto10 =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"upto10",$sys_debug,"clrate_b100_a","0");
    $s_blob_charge6 =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"charge6",$sys_debug,"clrate_b100_a","0");
    $s_blob_charge7 =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"charge7",$sys_debug,"clrate_b100_a","0");
    $s_blob_charge8 =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"charge8",$sys_debug,"clrate_b100_a","0");
    $s_blob_charge9 =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"charge9",$sys_debug,"clrate_b100_a","0");
    $s_blob_charge10 =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"charge10",$sys_debug,"clrate_b100_a","0");
    $s_blob_freeunits =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"freeunit",$sys_debug,"clrate_b100_a","0");
    $s_blob_perunit =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"perunit",$sys_debug,"clrate_b100_a","0");
    
    $s_blob_upto1=trim($s_blob_upto1);
    $s_blob_upto2=trim($s_blob_upto2);
    $s_blob_upto3=trim($s_blob_upto3);
    $s_blob_upto4=trim($s_blob_upto4);
    $s_blob_upto5=trim($s_blob_upto5);
    $s_blob_upto6=trim($s_blob_upto6);
    $s_blob_upto7=trim($s_blob_upto7);
    $s_blob_upto8=trim($s_blob_upto8);
    $s_blob_upto9=trim($s_blob_upto9);
    $s_blob_upto10=trim($s_blob_upto10);
        
B161_CHECK_CALC:

    $s_blob_rateunit = strtoupper(trim($s_blob_rateunit));
    $s_blob_ratebasic = str_replace('$','',$s_blob_ratebasic);

    //echo 'ar_options='.'<br>';
    //print_r($ar_options);
    //echo '<br>';
    
    $s_unit_count = $ar_options[10];
    $s_kgs = $ar_options[11];
    $s_cubic = $ar_options[12];
    
    $s_do_cubic_conv='N';
    
    //echo 's_blob_rateunit='.$s_blob_rateunit.'<br>';
    //echo 'ar_options[8]='.$ar_options[8].'<br>';
    //echo 's_unit_count1a='.$s_unit_count.'<br>';

    //echo 'ps_reciprocal='.$ps_reciprocal.'<br>';
    //exit();

    // pkn 20141126 - not really used at this stage
    if ($ps_reciprocal=='Y')
    {
        if ($s_blob_ratereciprocal<>'Y')
        {
            $s_calc_notes = $s_calc_notes."=rate is not reciprocal ";
            GOTO Z900_EXIT;
        }
    }
    
	//echo 'ps_options='.$ps_options.'<br>';
    //echo 's_blob_rateunit='.$s_blob_rateunit.'<br>';
    //echo 'ar_options[8]='.$ar_options[8].'<br>';
    //echo 's_unit_count='.$s_unit_count.'<br>';
    //exit();

    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_a100_rate_this - B161_CHECK_CALC - s_blob_rateunit=$s_blob_rateunit, ar_options[8]=$ar_options[8], ",$ps_debug_reference_no);
    }

    if (trim(strtoupper($s_blob_rateunit))==trim(strtoupper($ar_options[8])))
    {
        goto B162_DO_CALC;
    }
    if (trim(strtoupper($s_blob_rateunit))=='WEIGHT')
    {
        $s_unit_count=$s_kgs;
        $s_do_cubic_conv='Y';
        goto B162_DO_CALC;
    }
    if (trim(strtoupper($s_blob_rateunit))=='KILO')
    {
        $s_unit_count=$s_kgs;
        $s_do_cubic_conv='Y';
        goto B162_DO_CALC;
    }
    if (trim(strtoupper($s_blob_rateunit))=='ITEMWEIGHT')
    {
        $s_unit_count=$s_kgs;
        $s_do_cubic_conv='N';
        goto B162_DO_CALC;
    }
    if (trim(strtoupper($s_blob_rateunit))=='ITEMPER')
    {
        $s_unit_count=$s_cn_items;
        $s_do_cubic_conv='N';
        goto B162_DO_CALC;
    }
    if (trim(strtoupper($s_blob_rateunit))=='WEIGHTPERITEM')
    {
        $s_unit_count=$s_kgs;
        $s_do_cubic_conv='N';
        // pkn 20141024 - as per Ang requirements with Cross Docks XL Express invoice
        $s_do_cubic_conv='Y';
        goto B162_DO_CALC;
    }
    if (trim(strtoupper($s_blob_rateunit))=='WEIGHTPERPALLET')
    {
        $s_unit_count=$s_kgs;
        //$s_do_cubic_conv='Y';
        $s_do_cubic_conv='N';
        goto B162_DO_CALC;
    }
    if (trim(strtoupper($s_blob_rateunit))=='WEIGHTPERPALLETROUNDUP')
    {
        $s_unit_count=$s_kgs;
        //$s_do_cubic_conv='Y';
        $s_do_cubic_conv='N';
        goto B162_DO_CALC;
    }
    if (trim(strtoupper($s_blob_rateunit))=='FIXEDWEIGHT')
    {
        $s_unit_count=$s_kgs;
        $s_do_cubic_conv='N';
        goto B162_DO_CALC;
    }
    if (trim(strtoupper($s_blob_rateunit))=='SATCHEL_1_KG')
    {
        $s_unit_count=$s_kgs;
        $s_do_cubic_conv='N';
        goto B162_DO_CALC;
    }
    if (trim(strtoupper($s_blob_rateunit))=='SATCHEL_3_KG')
    {
        $s_unit_count=$s_kgs;
        $s_do_cubic_conv='N';
        goto B162_DO_CALC;
    }
    if (trim(strtoupper($s_blob_rateunit))=='SATCHEL_5_KG')
    {
        $s_unit_count=$s_kgs;
        $s_do_cubic_conv='N';
        goto B162_DO_CALC;
    }

    $s_calc_notes = $s_calc_notes."=rate unit is $ar_options[8] selected rate is $s_blob_rateunit ";

    GOTO Z900_EXIT;
    

B162_DO_CALC:

    // pkn 20150326 - for ATM always want to use cubic conversion where applicable
    if (strtoupper($_SESSION['site_name'])=="TDXDELNET")
    {
        if (trim(strtoupper($s_blob_rateunit))=='WEIGHT')
        {
            $s_unit_count=$s_kgs;
            $s_do_cubic_conv='Y';
        }
        if (trim(strtoupper($s_blob_rateunit))=='KILO')
        {
            $s_unit_count=$s_kgs;
            $s_do_cubic_conv='Y';
        }
        if (trim(strtoupper($s_blob_rateunit))=='ITEMWEIGHT')
        {
            $s_unit_count=$s_kgs;
            $s_do_cubic_conv='N';
        }
        if (trim(strtoupper($s_blob_rateunit))=='WEIGHTPERITEM')
        {
            $s_unit_count=$s_kgs;
            $s_do_cubic_conv='N';
            // pkn 20141024 - as per Ang requirements with Cross Docks XL Express invoice
            $s_do_cubic_conv='Y';
        }
        if (trim(strtoupper($s_blob_rateunit))=='WEIGHTPERPALLET')
        {
            $s_unit_count=$s_kgs;
            //$s_do_cubic_conv='Y';
            $s_do_cubic_conv='N';
        }
        if (trim(strtoupper($s_blob_rateunit))=='WEIGHTPERPALLETROUNDUP')
        {
            $s_unit_count=$s_kgs;
            //$s_do_cubic_conv='Y';
            $s_do_cubic_conv='N';
        }
        if (trim(strtoupper($s_blob_rateunit))=='FIXEDWEIGHT')
        {
            $s_unit_count=$s_kgs;
            $s_do_cubic_conv='N';
        }
    }

    // pkn 20170309 - always want to use cubic conversion for weight where applicable
    if (trim(strtoupper($s_blob_rateunit)) == 'WEIGHT')
    {
        $s_unit_count = $s_kgs;
        $s_do_cubic_conv = 'Y';
    }
    if (trim(strtoupper($s_blob_rateunit)) == 'KILO')
    {
        $s_unit_count = $s_kgs;
        $s_do_cubic_conv = 'Y';
    }

    //echo 'ar_options='.'<br>';
    //print_r($ar_options);
    //echo '<br>';
    
    //echo 's_unit_count1='.$s_unit_count.'<br>';
    //echo 's_blob_rateunit='.$s_blob_rateunit.'<br>';
    //echo 's_blob_freeunits1='.$s_blob_freeunits.'<br>';

    if ($s_blob_itemexcess=='ROUNDUP15MIN')
    {
        $s_unit_count = ceil($s_unit_count / 15);
        $s_unit_count = $s_unit_count * 15;
        $s_unit_count = $s_unit_count / 60;
    }
    if ($s_blob_itemexcess=='ROUNDUP30MIN')
    {
        $s_unit_count = ceil($s_unit_count / 30);
        $s_unit_count = $s_unit_count * 30;
        $s_unit_count = $s_unit_count / 60;
    }

    $s_unit_count = $s_unit_count - $s_blob_freeunits;
    if ($s_unit_count < 0 )
    {
        $s_unit_count = 0;
    }

    //echo 's_blob_freeunits2='.$s_blob_freeunits.'<br>';
    //echo 's_unit_count2='.$s_unit_count.'<br>';
    
    $s_units_to_rate = $s_unit_count;
    $s_cubic_converted_units = $s_cubic * $s_blob_cubicconv;

    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_a100_rate_this - B162_DO_CALC - s_blob_itemexcess=$s_blob_itemexcess, s_units_to_rate=$s_units_to_rate, s_cubic=$s_cubic, s_blob_cubicconv=$s_blob_cubicconv, s_cubic_converted_units=$s_cubic_converted_units, s_cubic_conv_round=$s_cubic_conv_round ",$ps_debug_reference_no);
    }

    if (trim(strtoupper($s_recordgroup))<>'CLIENT_REVENUE')
    {
        goto B162_SKIP_SELL_CHARGE_WEIGHT_ROUNDING;
    }

    // pkn 20180502 - scko_cn_sell_charge_weight_rounding

    $s_do_cn_sell_charge_weight_rounding='';
    if (isset($_SESSION['scko_cn_sell_charge_weight_rounding']))
    {
        $s_do_cn_sell_charge_weight_rounding=trim(strtoupper($_SESSION['scko_cn_sell_charge_weight_rounding']));
    }
    if ($s_do_cn_sell_charge_weight_rounding=='ROUNDUP')
    {
        $s_cubic_converted_units=ceil($s_cubic_converted_units);
    }
    if ($s_do_cn_sell_charge_weight_rounding=='ROUNDDOWN')
    {
        $s_cubic_converted_units=floor($s_cubic_converted_units);
    }

    goto B162_END_SELL_CHARGE_WEIGHT_ROUNDING;

    B162_SKIP_SELL_CHARGE_WEIGHT_ROUNDING:

    // pkn 20140619 - certain clients require cubic converted weight rounded up to the nearest kg
    if ($s_cubic_conv_round=='Y')
    {
        //$s_cubic_converted_units=ceil($s_cubic_converted_units);
        $s_cubic_converted_units=round($s_cubic_converted_units);

        if ($s_cubic_converted_units<1 and $s_cubic_converted_units>0)
        {
            $s_cubic_converted_units=1;
        }
    }

    $s_scko_rating_cubic_conv_rounding='';
    if (isset($_SESSION['scko_rating_cubic_conv_rounding']))
    {
        $s_scko_rating_cubic_conv_rounding=trim(strtoupper($_SESSION['scko_rating_cubic_conv_rounding']));
    }
    $s_scko_rating_cubic_conv_rounding_carriers_to_skip='';
    if (isset($_SESSION['scko_rating_cubic_conv_rounding_carriers_to_skip']))
    {
        $s_scko_rating_cubic_conv_rounding_carriers_to_skip=trim(strtoupper($_SESSION['scko_rating_cubic_conv_rounding_carriers_to_skip']));
    }

    $s_do_cubic_conv_rounding_for_carrier='N';
    if (strpos($s_scko_rating_cubic_conv_rounding_carriers_to_skip,$s_option_transport_company) === false)
    {
        $s_do_cubic_conv_rounding_for_carrier='Y';
    }

    if ($s_scko_rating_cubic_conv_rounding=='ROUND' and $s_do_cubic_conv_rounding_for_carrier=='Y')
    {
        $s_cubic_converted_units=round($s_cubic_converted_units);

        if ($s_cubic_converted_units<1 and $s_cubic_converted_units>0)
        {
            $s_cubic_converted_units=1;
        }
    }
    if ($s_scko_rating_cubic_conv_rounding=='ROUNDUP' and $s_do_cubic_conv_rounding_for_carrier=='Y')
    {
        $s_cubic_converted_units=ceil($s_cubic_converted_units);

        if ($s_cubic_converted_units<1 and $s_cubic_converted_units>0)
        {
            $s_cubic_converted_units=1;
        }
    }
    if ($s_scko_rating_cubic_conv_rounding=='ROUNDDOWN' and $s_do_cubic_conv_rounding_for_carrier=='Y')
    {
        $s_cubic_converted_units=floor($s_cubic_converted_units);

        if ($s_cubic_converted_units<1 and $s_cubic_converted_units>0)
        {
            $s_cubic_converted_units=1;
        }
    }

    B162_END_SELL_CHARGE_WEIGHT_ROUNDING:

    // pkn 20140619 - ITEMWEIGHT rate unit - divide weight by itemkgs, round up to nearest item and rate as normal
    if (strtoupper($s_blob_rateunit)=='ITEMWEIGHT')
    {
        /*
        echo 's_blob_itemkgs='.$s_blob_itemkgs.'<br>';
        echo 's_unit_count='.$s_unit_count.'<br>';
        echo 's_cn_items='.$s_cn_items.'<br>';
        echo 's_units_to_rate1='.$s_units_to_rate.'<br>';
        */

        $s_unit_count=$s_cn_items;

        // pkn 20140729 - if cn weight is less than itemkgs in rate then used cn items as number of units to rate instead
        if ($s_unit_count<$s_blob_itemkgs)
        {
            $s_units_to_rate=$s_cn_items;
        }
        else
        {
            $s_unit_count = $s_unit_count / $s_blob_itemkgs;
            $s_units_to_rate = ceil($s_unit_count);
        }
        IF ($s_debug == "YES")
        {
            $this->z901_dump("Do Debug clrate_a100_rate_this - B162_DO_CALC - s_blob_rateunit=$s_blob_rateunit, s_units_to_rate=$s_units_to_rate, s_blob_itemkgs=$s_blob_itemkgs, s_unit_count=$s_unit_count ",$ps_debug_reference_no);
        }
    }

    // pkn 20170614 - ITEMPER rate unit - divide items by itemkgs, use value in Rate Flat Accum field (ROUND,ROUND_UP,ROUND_DOWN) to determine how to round the rate units
    if (strtoupper($s_blob_rateunit)=='ITEMPER')
    {
        /*
        echo 's_blob_itemkgs='.$s_blob_itemkgs.'<br>';
        echo 's_blob_rateflataccum='.$s_blob_rateflataccum.'<br>';
        echo 's_unit_count='.$s_unit_count.'<br>';
        echo 's_cn_items='.$s_cn_items.'<br>';
        echo 's_units_to_rate1='.$s_units_to_rate.'<br>';
        */

        //$s_unit_count=$s_cn_items;

        // pkn 20140729 - if cn weight is less than itemkgs in rate then used cn items as number of units to rate instead
        //if ($s_unit_count<$s_blob_itemkgs)
        //{
        //    $s_units_to_rate=$s_cn_items;
        //}
        //else
        //{
            $s_unit_count = $s_unit_count / $s_blob_itemkgs;
            if (strtoupper(trim($s_blob_rateflataccum))=='ROUND')
            {
                $s_units_to_rate = round($s_unit_count);
            }
            if (strtoupper(trim($s_blob_rateflataccum))=='ROUND_UP')
            {
                $s_units_to_rate = ceil($s_unit_count);
            }
            if (strtoupper(trim($s_blob_rateflataccum))=='ROUND_DOWN')
            {
                $s_units_to_rate = floor($s_unit_count);
            }
        //}
        IF ($s_debug == "YES")
        {
            $this->z901_dump("Do Debug clrate_a100_rate_this - B162_DO_CALC - s_blob_rateunit=$s_blob_rateunit, s_units_to_rate=$s_units_to_rate, s_blob_itemkgs=$s_blob_itemkgs, s_unit_count=$s_unit_count ",$ps_debug_reference_no);
        }
    }

    if (strtoupper($_SESSION['db_name'])=='TDXDELNET_ATML' or strtoupper($_SESSION['db_name'])=='TDXDELNET_ATML_TEST')
    {
        //goto B162_DO_CALC_ATML_WEIGHTPERITEM;
        goto B162_DO_CALC_WEIGHTPERITEM;
    }

    if ($s_blob_rateunit=='WEIGHTPERPALLET')
    {
        goto B162_DO_CALC_WEIGHTPERPALLET;
    }
    if ($s_blob_rateunit=='WEIGHTPERPALLETROUNDUP')
    {
        goto B162_DO_CALC_WEIGHTPERPALLETROUNDUP;
    }
    if ($s_blob_rateunit=='WEIGHTACCUM1')
    {
        goto B162_DO_CALC_WEIGHTACCUM1;
    }
    if ($s_blob_rateunit=='FIXEDWEIGHT')
    {
        goto B162_DO_CALC_FIXEDWEIGHT;
    }

    goto B162_DO_CALC_WEIGHTPERITEM;

B162_DO_CALC_ATML_WEIGHTPERITEM:

    // pkn 20140909 - WEIGHTPERITEM rate unit - check if total weight equates to more than itemkgs per item, if yes use WEIGHTPERITEM rate if not use total items
    // eg, CN 7 items, 77 kgs, itmkgs - 25.  7*25=175 which is greater than 77.  Use items to rate
    // eg, CN 2 items, 60 kgs, itmkgs - 25.  2*25=50 which is less than 60.  Use WEIGHTPERITEM rate
    //echo 's_blob_rateunit='.$s_blob_rateunit.'<br>';
    if (strtoupper($s_blob_rateunit)=='WEIGHTPERITEM')
    {
        $s_cn_itemskgs_total=$s_blob_itemkgs*$s_cn_items;

        //echo 's_blob_itemkgs='.$s_blob_itemkgs.'<br>';
        //echo 's_unit_count='.$s_unit_count.'<br>';
        //echo 's_cn_items='.$s_cn_items.'<br>';
        //echo 's_units_to_rate1='.$s_units_to_rate.'<br>';
        //echo 's_cn_itemskgs_total='.$s_cn_itemskgs_total.'<br>';
        //exit();

        if ($s_cn_itemskgs_total>$s_blob_itemkgs)
        {
            //echo 'here1'.'<br>';
            $s_units_to_rate=$s_cn_items;
        }
        else
        {
            //echo 'here2'.'<br>';
            $s_unit_count = $s_unit_count / $s_blob_itemkgs;
            $s_units_to_rate = ceil($s_unit_count);
        }
        IF ($s_debug == "YES")
        {
            $this->z901_dump("Do Debug clrate_a100_rate_this - B162_DO_CALC_ATML_WEIGHTPERITEM - s_blob_rateunit=$s_blob_rateunit, s_units_to_rate=$s_units_to_rate, s_blob_itemkgs=$s_blob_itemkgs, s_unit_count=$s_unit_count ",$ps_debug_reference_no);
        }
    }

    goto B162_DO_CALC_EXIT;

B162_DO_CALC_WEIGHTPERITEM:

    // pkn 20140909 - WEIGHTPERITEM rate unit - check if total weight equates to more than itemkgs per item, if yes use WEIGHTPERITEM rate if not use total items
    // eg, CN 7 items, 77 kgs, itmkgs - 25.  7*25=175 which is greater than 77.  Use items to rate
    // eg, CN 2 items, 60 kgs, itmkgs - 25.  2*25=50 which is less than 60.  Use WEIGHTPERITEM rate
    if (strtoupper($s_blob_rateunit)=='WEIGHTPERITEM')
    {
        $s_cn_itemskgs_total=$s_blob_itemkgs*$s_cn_items;

        $s_cubic_converted_units = $s_cubic_converted_units - $s_blob_freeunits;
        $s_cubic_converted_weight=0;
        if ($s_do_cubic_conv=='Y' and $s_cubic_converted_units > $s_units_to_rate)
        {
            $s_cubic_converted_weight = $s_cubic_converted_units;
        }

        /*
        echo 's_cubic_converted_units='.$s_cubic_converted_units.'<br>';
        echo 's_cubic_converted_weight='.$s_cubic_converted_weight.'<br>';
        echo 's_cn_itemskgs_total='.$s_cn_itemskgs_total.'<br>';
        echo 's_blob_itemkgs='.$s_blob_itemkgs.'<br>';
        echo 's_unit_count='.$s_unit_count.'<br>';
        echo 's_cn_items='.$s_cn_items.'<br>';
        echo 's_kgs='.$s_kgs.'<br>';

        echo 's_blob_itemkgs='.$s_blob_itemkgs.'<br>';
        echo 's_unit_count='.$s_unit_count.'<br>';
        echo 's_cn_items='.$s_cn_items.'<br>';
        echo 's_units_to_rate1='.$s_units_to_rate.'<br>';
        echo 's_cn_itemskgs_total='.$s_cn_itemskgs_total.'<br>';
        */

        //if ($s_cubic_converted_weight > $s_blob_itemkgs)
        if ($s_cubic_converted_weight > $s_kgs)
        {
            if ($s_cubic_converted_weight > $s_unit_count)
            {
                $s_unit_count=$s_cubic_converted_weight;
            }
            $s_unit_count = $s_unit_count / $s_blob_itemkgs;
            $s_units_to_rate = ceil($s_unit_count);
        }
        elseif ($s_kgs>$s_cn_itemskgs_total)
        {
            $s_unit_count = $s_kgs / $s_blob_itemkgs;
            $s_units_to_rate = ceil($s_unit_count);
        }
        elseif ($s_cn_itemskgs_total>$s_blob_itemkgs)
        {
            $s_units_to_rate=$s_cn_items;
        }
        else
        {
            $s_unit_count = $s_unit_count / $s_blob_itemkgs;
            $s_units_to_rate = ceil($s_unit_count);
        }
        $s_cubic_converted_units=0;

        if ($s_cn_items>$s_units_to_rate)
        {
            $s_units_to_rate=$s_cn_items;
        }

        IF ($s_debug == "YES")
        {
            $this->z901_dump("Do Debug clrate_a100_rate_this - B162_DO_CALC - s_blob_rateunit=$s_blob_rateunit, s_cn_itemskgs_total=$s_cn_itemskgs_total, s_blob_itemkgs=$s_blob_itemkgs, s_cn_items=$s_cn_items, s_cubic_converted_units=$s_cubic_converted_units, s_blob_freeunits=$s_blob_freeunits, s_cubic_converted_weight=$s_cubic_converted_weight, s_do_cubic_conv=$s_do_cubic_conv, s_units_to_rate=$s_units_to_rate ",$ps_debug_reference_no);
        }
    }

    goto B162_DO_CALC_EXIT;

B162_DO_CALC_WEIGHTPERPALLET:

    // pkn 20161216 - WEIGHTPERPALLET rate unit - get total charge weight, divide by itemskgs.  Take that amount as units * unit charge + basic
    // eg CN Charge Weight = 1053, itemkgs = 500, unit charge = 80, basic charge = 9.  1053/500 = 2.106 x $80 + $9 = $177.48

    $s_cn_itemskgs_total=$s_unit_count/$s_blob_itemkgs;
    $s_units_to_rate=$s_cn_itemskgs_total;

    //echo 's_cn_itemskgs_total='.$s_cn_itemskgs_total.'<br>';
    //echo 's_unit_count='.$s_unit_count.'<br>';
    //echo 's_blob_itemkgs='.$s_blob_itemkgs.'<br>';
    //echo 's_units_to_rate='.$s_units_to_rate.'<br>';
    //echo 's_cn_itemskgs_total='.$s_cn_itemskgs_total.'<br>';

    goto B162_DO_CALC_EXIT;

B162_DO_CALC_WEIGHTPERPALLETROUNDUP:

    // pkn 20200114 - WEIGHTPERPALLETROUNDUP rate unit - get total charge weight, divide by itemskgs.  Round up to nearest whole number.  Take that amount as units * unit charge + basic
    // eg CN Charge Weight = 1053, itemkgs = 500, unit charge = 80, basic charge = 9.  1053/500 = 2.106, rounded to 3.  3 x $80 + $9 = $249.00

    $s_cn_itemskgs_total=$s_unit_count/$s_blob_itemkgs;
    $s_units_to_rate=$s_cn_itemskgs_total;
    //$s_units_to_rate=$s_units_to_rate-$s_blob_freeunits;
    $s_units_to_rate = ceil($s_units_to_rate);

    if ($s_units_to_rate<=0)
    {
        $s_units_to_rate=1;
    }

	// pkn 20200117
	// s_calced_value1 is the calculation based on maximum pallet weight set by using itemkgs field
	// s_calced_value2 is the calculation based on total pallets * the first charge amount break
	// highest is then used and the rest of the rating process is bypassed

	$s_calced_value1 = ($s_units_to_rate * $s_blob_charge1) + $s_blob_ratebasic;
	$s_calced_value2 = ($s_cn_items * $s_blob_charge1) + $s_blob_ratebasic;

	$s_calced_value = $s_calced_value1;
	if ($s_calced_value2>$s_calced_value1)
	{
		$s_calced_value = $s_calced_value2;
	}

    goto Z900_EXIT;

B162_DO_CALC_WEIGHTACCUM1:

    // pkn 20171010 - WEIGHTACCUM1 rate unit
    // UP TO 3KG = $8.26, THEN 4-5KG'S IS $14.22, THEN ANY KILO OVER CHARGED AT $1.81
    // works as follows-:
    // first 3 kg is $8.26
    // then next 2 kg is 5.96
    // then excess weight is multiplied by per kg rate of $1.80

    /*
    echo 's_blob_rateunit='.$s_blob_rateunit.'<br>';
    echo 's_units_to_rate='.$s_units_to_rate.'<br>';
    echo 's_blob_upto1='.$s_blob_upto1.'<br>';
    echo 's_blob_upto2='.$s_blob_upto2.'<br>';
    echo 's_blob_upto3='.$s_blob_upto3.'<br>';
    echo 's_blob_upto4='.$s_blob_upto4.'<br>';
    echo 's_blob_upto5='.$s_blob_upto5.'<br>';
    echo 's_blob_upto6='.$s_blob_upto6.'<br>';
    echo 's_blob_upto7='.$s_blob_upto7.'<br>';
    echo 's_blob_upto8='.$s_blob_upto8.'<br>';
    echo 's_blob_upto9='.$s_blob_upto9.'<br>';
    echo 's_blob_upto10='.$s_blob_upto10.'<br>';
    echo 's_blob_charge1='.$s_blob_charge1.'<br>';
    echo 's_blob_charge2='.$s_blob_charge2.'<br>';
    echo 's_blob_charge3='.$s_blob_charge3.'<br>';
    echo 's_blob_charge4='.$s_blob_charge4.'<br>';
    echo 's_blob_charge5='.$s_blob_charge5.'<br>';
    echo 's_blob_charge6='.$s_blob_charge6.'<br>';
    echo 's_blob_charge7='.$s_blob_charge7.'<br>';
    echo 's_blob_charge8='.$s_blob_charge8.'<br>';
    echo 's_blob_charge9='.$s_blob_charge9.'<br>';
    echo 's_blob_charge10='.$s_blob_charge10.'<br>';
    */

	//echo 's_units_to_rate='.$s_units_to_rate.'<br>';
	//echo 's_blob_upto2='.$s_blob_upto2.'<br>';
	//echo 's_blob_upto3='.$s_blob_upto3.'<br>';
	//echo 's_blob_upto4='.$s_blob_upto4.'<br>';

    $s_calced_value=0;
    $s_units_to_rate_accum=$s_units_to_rate;
    //echo 's_units_to_rate_accum_a='.$s_units_to_rate_accum.'<br>';
    if ($s_units_to_rate <= $s_blob_upto1)
    {
        $s_calced_value=$s_blob_charge1;
        $s_calc_notes = $s_calc_notes." Type=WEIGHTACCUM, units=$s_units_to_rate, UPTO1=$s_blob_upto1, CHARGE1=$s_blob_charge1, TOTALCHARGE=$s_calced_value";
        GOTO Z900_EXIT;
    }
    $s_calced_value=$s_calced_value+$s_blob_charge1;
    $s_calc_notes = $s_calc_notes." Type=WEIGHTACCUM, units=$s_units_to_rate, UPTO1=$s_blob_upto1, CHARGE1=$s_blob_charge1, ";

    if ($s_units_to_rate <= $s_blob_upto2)
    {
        if ($s_blob_upto2>999999)
        {
            $s_units_to_rate_accum=$s_units_to_rate_accum-$s_blob_upto1;
            $s_calc_accum_value=$s_units_to_rate_accum*$s_blob_charge2;
            $s_calced_value=$s_calced_value+$s_calc_accum_value;
            $s_calc_notes = $s_calc_notes." UPTO2=$s_blob_upto2, CHARGE2=$s_blob_charge2, ACCUM_CALC_VALUE=$s_calc_accum_value, TOTALCHARGE=$s_calced_value";
        }
        else
        {
            $s_calced_value=$s_calced_value+$s_blob_charge2;
            $s_calc_notes = $s_calc_notes." UPTO2=$s_blob_upto2, CHARGE2=$s_blob_charge2, TOTALCHARGE=$s_calced_value";
        }
        GOTO Z900_EXIT;
    }
    $s_calced_value=$s_calced_value+$s_blob_charge2;
    $s_calc_notes = $s_calc_notes." UPTO2=$s_blob_upto2, CHARGE2=$s_blob_charge2, ";

    if ($s_units_to_rate <= $s_blob_upto3)
    {
        if ($s_blob_upto3>999999)
        {
            $s_units_to_rate_accum=$s_units_to_rate_accum-$s_blob_upto2;
            $s_calc_accum_value=$s_units_to_rate_accum*$s_blob_charge3;
            $s_calced_value=$s_calced_value+$s_calc_accum_value;
            $s_calc_notes = $s_calc_notes." UPTO3=$s_blob_upto3, CHARGE3=$s_blob_charge3, ACCUM_CALC_VALUE=$s_calc_accum_value, TOTALCHARGE=$s_calced_value";
        }
        else
        {
            $s_calced_value = $s_calced_value + $s_blob_charge3;
            $s_calc_notes = $s_calc_notes . " UPTO3=$s_blob_upto3, CHARGE2=$s_blob_charge3, TOTALCHARGE=$s_calced_value";
        }
        GOTO Z900_EXIT;
    }
    $s_calced_value=$s_calced_value+$s_blob_charge3;
    $s_calc_notes = $s_calc_notes." UPTO3=$s_blob_upto3, CHARGE3=$s_blob_charge3, ";

    if ($s_units_to_rate <= $s_blob_upto4)
    {
        if ($s_blob_upto4>999999)
        {
            $s_units_to_rate_accum=$s_units_to_rate_accum-$s_blob_upto3;
            $s_calc_accum_value=$s_units_to_rate_accum*$s_blob_charge4;
            $s_calced_value=$s_calced_value+$s_calc_accum_value;
            $s_calc_notes = $s_calc_notes." UPTO4=$s_blob_upto4, CHARGE4=$s_blob_charge4, ACCUM_CALC_VALUE=$s_calc_accum_value, TOTALCHARGE=$s_calced_value";
        }
        else
        {
            $s_calced_value = $s_calced_value + $s_blob_charge4;
            $s_calc_notes = $s_calc_notes . " UPTO4=$s_blob_upto4, CHARGE4=$s_blob_charge4, TOTALCHARGE=$s_calced_value";
        }
        GOTO Z900_EXIT;
    }
    $s_calced_value=$s_calced_value+$s_blob_charge4;
    $s_calc_notes = $s_calc_notes." UPTO4=$s_blob_upto4, CHARGE4=$s_blob_charge4, ";

    if ($s_units_to_rate <= $s_blob_upto5)
    {
        if ($s_blob_upto5>999999)
        {
            $s_units_to_rate_accum=$s_units_to_rate_accum-$s_blob_upto4;
            $s_calc_accum_value=$s_units_to_rate_accum*$s_blob_charge5;
            $s_calced_value=$s_calced_value+$s_calc_accum_value;
            $s_calc_notes = $s_calc_notes." UPTO5=$s_blob_upto5, CHARGE5=$s_blob_charge5, ACCUM_CALC_VALUE=$s_calc_accum_value, TOTALCHARGE=$s_calced_value";
        }
        else
        {
            $s_calced_value = $s_calced_value + $s_blob_charge5;
            $s_calc_notes = $s_calc_notes . " UPTO5=$s_blob_upto5, CHARGE5=$s_blob_charge5, TOTALCHARGE=$s_calced_value";
        }
        GOTO Z900_EXIT;
    }
    $s_calced_value=$s_calced_value+$s_blob_charge5;
    $s_calc_notes = $s_calc_notes." UPTO5=$s_blob_upto5, CHARGE5=$s_blob_charge5, ";

    if ($s_units_to_rate <= $s_blob_upto6)
    {
        if ($s_blob_upto6>999999)
        {
            $s_units_to_rate_accum=$s_units_to_rate_accum-$s_blob_upto5;
            $s_calc_accum_value=$s_units_to_rate_accum*$s_blob_charge6;
            $s_calced_value=$s_calced_value+$s_calc_accum_value;
            $s_calc_notes = $s_calc_notes." UPTO6=$s_blob_upto6, CHARGE6=$s_blob_charge6, ACCUM_CALC_VALUE=$s_calc_accum_value, TOTALCHARGE=$s_calced_value";
        }
        else
        {
            $s_calced_value = $s_calced_value + $s_blob_charge6;
            $s_calc_notes = $s_calc_notes . " UPTO6=$s_blob_upto6, CHARGE6=$s_blob_charge6, TOTALCHARGE=$s_calced_value";
        }
        GOTO Z900_EXIT;
    }
    $s_calced_value=$s_calced_value+$s_blob_charge6;
    $s_calc_notes = $s_calc_notes." UPTO6=$s_blob_upto6, CHARGE6=$s_blob_charge6, ";

    if ($s_units_to_rate <= $s_blob_upto7)
    {
        if ($s_blob_upto7>999999)
        {
            $s_units_to_rate_accum=$s_units_to_rate_accum-$s_blob_upto6;
            $s_calc_accum_value=$s_units_to_rate_accum*$s_blob_charge7;
            $s_calced_value=$s_calced_value+$s_calc_accum_value;
            $s_calc_notes = $s_calc_notes." UPTO7=$s_blob_upto7, CHARGE7=$s_blob_charge7, ACCUM_CALC_VALUE=$s_calc_accum_value, TOTALCHARGE=$s_calced_value";
        }
        else
        {
            $s_calced_value = $s_calced_value + $s_blob_charge7;
            $s_calc_notes = $s_calc_notes . " UPTO7=$s_blob_upto7, CHARGE7=$s_blob_charge7, TOTALCHARGE=$s_calced_value";
        }
        GOTO Z900_EXIT;
    }
    $s_calced_value=$s_calced_value+$s_blob_charge7;
    $s_calc_notes = $s_calc_notes." UPTO7=$s_blob_upto7, CHARGE7=$s_blob_charge7, ";

    if ($s_units_to_rate <= $s_blob_upto8)
    {
        if ($s_blob_upto8>999999)
        {
            $s_units_to_rate_accum=$s_units_to_rate_accum-$s_blob_upto7;
            $s_calc_accum_value=$s_units_to_rate_accum*$s_blob_charge8;
            $s_calced_value=$s_calced_value+$s_calc_accum_value;
            $s_calc_notes = $s_calc_notes." UPTO8=$s_blob_upto8, CHARGE8=$s_blob_charge8, ACCUM_CALC_VALUE=$s_calc_accum_value, TOTALCHARGE=$s_calced_value";
        }
        else
        {
            $s_calced_value = $s_calced_value + $s_blob_charge8;
            $s_calc_notes = $s_calc_notes . " UPTO8=$s_blob_upto8, CHARGE8=$s_blob_charge8, TOTALCHARGE=$s_calced_value";
        }
        GOTO Z900_EXIT;
    }
    $s_calced_value=$s_calced_value+$s_blob_charge8;
    $s_calc_notes = $s_calc_notes." UPTO8=$s_blob_upto8, CHARGE8=$s_blob_charge8, ";

    if ($s_units_to_rate <= $s_blob_upto9)
    {
        if ($s_blob_upto9>999999)
        {
            $s_units_to_rate_accum=$s_units_to_rate_accum-$s_blob_upto8;
            $s_calc_accum_value=$s_units_to_rate_accum*$s_blob_charge9;
            $s_calced_value=$s_calced_value+$s_calc_accum_value;
            $s_calc_notes = $s_calc_notes." UPTO9=$s_blob_upto9, CHARGE9=$s_blob_charge9, ACCUM_CALC_VALUE=$s_calc_accum_value, TOTALCHARGE=$s_calced_value";
        }
        else
        {
            $s_calced_value = $s_calced_value + $s_blob_charge9;
            $s_calc_notes = $s_calc_notes . " UPTO9=$s_blob_upto9, CHARGE9=$s_blob_charge9, TOTALCHARGE=$s_calced_value";
        }
        GOTO Z900_EXIT;
    }
    $s_calced_value=$s_calced_value+$s_blob_charge9;
    $s_calc_notes = $s_calc_notes." UPTO9=$s_blob_upto9, CHARGE9=$s_blob_charge9, ";

    if ($s_units_to_rate <= $s_blob_upto10)
    {
        if ($s_blob_upto10>999999)
        {
            $s_units_to_rate_accum=$s_units_to_rate_accum-$s_blob_upto9;
            $s_calc_accum_value=$s_units_to_rate_accum*$s_blob_charge10;
            $s_calced_value=$s_calced_value+$s_calc_accum_value;
            $s_calc_notes = $s_calc_notes." UPTO10=$s_blob_upto10, CHARGE10=$s_blob_charge10, ACCUM_CALC_VALUE=$s_calc_accum_value, TOTALCHARGE=$s_calced_value";
        }
        else
        {
            $s_calced_value = $s_calced_value + $s_blob_charge10;
            $s_calc_notes = $s_calc_notes . " UPTO10=$s_blob_upto10, CHARGE10=$s_blob_charge10, TOTALCHARGE=$s_calced_value";
        }
        GOTO Z900_EXIT;
    }
    $s_calced_value=$s_calced_value+$s_blob_charge10;
    $s_calc_notes = $s_calc_notes." UPTO10=$s_blob_upto10, CHARGE10=$s_blob_charge10, ";

    GOTO Z900_EXIT;

B162_DO_CALC_FIXEDWEIGHT:

    $s_calced_value=0;
    if ($s_units_to_rate <= $s_blob_upto1)
    {
        $s_calced_value=$s_blob_charge1;
        $s_calc_notes = $s_calc_notes." Type=FIXEDWEIGHT, units=$s_units_to_rate, UPTO1=$s_blob_upto1, CHARGE1=$s_blob_charge1, TOTALCHARGE=$s_calced_value";
        GOTO Z900_EXIT;
    }
    if ($s_units_to_rate <= $s_blob_upto2)
    {
        $s_calced_value=$s_blob_charge2;
        $s_calc_notes = $s_calc_notes." Type=FIXEDWEIGHT, units=$s_units_to_rate, UPTO2=$s_blob_upto2, CHARGE2=$s_blob_charge2, TOTALCHARGE=$s_calced_value";
        GOTO Z900_EXIT;
    }
    if ($s_units_to_rate <= $s_blob_upto3)
    {
        $s_calced_value=$s_blob_charge3;
        $s_calc_notes = $s_calc_notes." Type=FIXEDWEIGHT, units=$s_units_to_rate, UPTO3=$s_blob_upto3, CHARGE3=$s_blob_charge3, TOTALCHARGE=$s_calced_value";
        GOTO Z900_EXIT;
    }
    if ($s_units_to_rate <= $s_blob_upto4)
    {
        $s_calced_value=$s_blob_charge4;
        $s_calc_notes = $s_calc_notes." Type=FIXEDWEIGHT, units=$s_units_to_rate, UPTO4=$s_blob_upto4, CHARGE4=$s_blob_charge4, TOTALCHARGE=$s_calced_value";
        GOTO Z900_EXIT;
    }
    if ($s_units_to_rate <= $s_blob_upto5)
    {
        $s_calced_value=$s_blob_charge5;
        $s_calc_notes = $s_calc_notes." Type=FIXEDWEIGHT, units=$s_units_to_rate, UPTO5=$s_blob_upto5, CHARGE5=$s_blob_charge5, TOTALCHARGE=$s_calced_value";
        GOTO Z900_EXIT;
    }
    if ($s_units_to_rate <= $s_blob_upto6)
    {
        $s_calced_value=$s_blob_charge6;
        $s_calc_notes = $s_calc_notes." Type=FIXEDWEIGHT, units=$s_units_to_rate, UPTO1=$s_blob_upto6, CHARGE6=$s_blob_charge6, TOTALCHARGE=$s_calced_value";
        GOTO Z900_EXIT;
    }
    if ($s_units_to_rate <= $s_blob_upto7)
    {
        $s_calced_value=$s_blob_charge7;
        $s_calc_notes = $s_calc_notes." Type=FIXEDWEIGHT, units=$s_units_to_rate, UPTO7=$s_blob_upto7, CHARGE7=$s_blob_charge7, TOTALCHARGE=$s_calced_value";
        GOTO Z900_EXIT;
    }
    if ($s_units_to_rate <= $s_blob_upto8)
    {
        $s_calced_value=$s_blob_charge8;
        $s_calc_notes = $s_calc_notes." Type=FIXEDWEIGHT, units=$s_units_to_rate, UPTO8=$s_blob_upto8, CHARGE8=$s_blob_charge8, TOTALCHARGE=$s_calced_value";
        GOTO Z900_EXIT;
    }
    if ($s_units_to_rate <= $s_blob_upto9)
    {
        $s_calced_value=$s_blob_charge9;
        $s_calc_notes = $s_calc_notes." Type=FIXEDWEIGHT, units=$s_units_to_rate, UPTO9=$s_blob_upto9, CHARGE9=$s_blob_charge9, TOTALCHARGE=$s_calced_value";
        GOTO Z900_EXIT;
    }
    if ($s_units_to_rate <= $s_blob_upto10)
    {
        $s_calced_value=$s_blob_charge10;
        $s_calc_notes = $s_calc_notes." Type=FIXEDWEIGHT, units=$s_units_to_rate, UPTO10=$s_blob_upto10, CHARGE10=$s_blob_charge10, TOTALCHARGE=$s_calced_value";
        GOTO Z900_EXIT;
    }

    GOTO Z900_EXIT;

B162_DO_CALC_EXIT:

    //echo 's_unit_count2='.$s_unit_count.'<br>';
    //echo 's_units_to_rate='.$s_units_to_rate.'<br>';

    // pkn 20131009 - subtract free units from cubic converted units
    //echo 's_cubic_converted_units1='.$s_cubic_converted_units.'<br>';
    $s_cubic_converted_units = $s_cubic_converted_units - $s_blob_freeunits;
    //echo 's_cubic_converted_units2='.$s_cubic_converted_units.'<br>';
    //echo 's_blob_freeunits='.$s_blob_freeunits.'<br>';
    //echo 's_blob_rateflataccum='.$s_blob_rateflataccum.'<br>';

    //echo 's_do_cubic_conv='.$s_do_cubic_conv.'<br>';
    //echo 's_cubic_converted_units='.$s_cubic_converted_units.'<br>';
    //echo 's_units_to_rate='.$s_units_to_rate.'<br>';
    //echo 's_blob_rateunit='.$s_blob_rateunit.'<br>';
    //echo 'ar_options[8]='.$ar_options[8].'<br>';
	//echo 's_option_transport_company='.$s_option_transport_company.'<br>';

    // pkn 20190121 - as per Wathsala change of mind I have changed the below code so Jayde also now uses cubic conversion
    /*
    if ($s_option_transport_company=='TLM_JAYDE')
    {

    }
    else
    {
        if ($s_do_cubic_conv=='Y' and $s_cubic_converted_units > $s_units_to_rate)
        {
            $s_units_to_rate = $s_cubic_converted_units;
        }
    }
    */

    if ($s_do_cubic_conv=='Y' and $s_cubic_converted_units > $s_units_to_rate)
    {
        $s_units_to_rate = $s_cubic_converted_units;
    }

    //echo 's_units_to_rate='.$s_units_to_rate.'<br>';
    //echo '<br>';

    $s_calced_value = $s_blob_ratebasic;

    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_a100_rate_this - B162_DO_CALC_EXIT - s_cubic_converted_units=$s_cubic_converted_units, s_blob_freeunits=$s_blob_freeunits, s_do_cubic_conv=$s_do_cubic_conv, s_units_to_rate=$s_units_to_rate, s_calced_value=$s_calced_value, s_blob_ratebasic=$s_blob_ratebasic ",$ps_debug_reference_no);
    }

B163_SET_CHARGE:
    $s_unit_charge = $s_blob_charge1;
    $s_unit_charge = str_replace('$','',$s_unit_charge);
    //echo 's_unit_charge pkn='.$s_unit_charge.'<br>';
    //echo 's_blob_rateflataccum='.$s_blob_rateflataccum.'<br>';
// set the unit charge based on the charge amount

    //echo 'B163_SET_CHARGE'.'<br>';
    //echo 's_blob_rateflataccum='.$s_blob_rateflataccum.'<br>';
    //exit();

    $s_blob_rateflataccum = "FLAT";
    if (strtoupper(trim($s_blob_rateflataccum)) == "FLAT")
    {
        goto B1631_DO_FLAT;
    }
    if (strtoupper(trim($s_blob_rateflataccum)) == "F")
    {
        goto B1631_DO_FLAT;
    }
    if (strtoupper(trim($s_blob_rateflataccum)) == "N")
    {
        goto B1631_DO_FLAT;
    }

    GOTO B1633_DO_INCREMENTAL;

B1631_DO_FLAT:
    $s_unit_charge = $s_blob_charge1;
    $s_unit_charge = str_replace('$','',$s_unit_charge);
    //echo 's_unit_charge1='.$s_unit_charge.'<br>';
    //echo 's_units_to_rate1='.$s_units_to_rate.'<br>';
    //echo 's_blob_upto1='.$s_blob_upto1.'<br>';
    //echo 's_blob_rateunit='.$s_blob_rateunit.'<br>';
    //exit();

    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_a100_rate_this - B1631_DO_FLAT - s_blob_rateunit=$s_blob_rateunit, s_blob_itemexcess=$s_blob_itemexcess, s_unit_charge=$s_unit_charge, s_blob_charge1=$s_blob_charge1 ",$ps_debug_reference_no);
    }

    if ($s_blob_itemexcess=='ROUNDUP15MIN')
    {
        $s_units_to_rate=$s_units_to_rate/$s_blob_upto1;
        //echo 's_units_to_rate2='.$s_units_to_rate.'<br>';
        goto B1632_END;
    }
    if ($s_blob_itemexcess=='ROUNDUP30MIN')
    {
        $s_units_to_rate=$s_units_to_rate/$s_blob_upto1;
        goto B1632_END;
    }
    if ($s_blob_rateunit=='CUBIC')
    {
        $s_units_to_rate=$s_unit_count;
        goto B1632_END;
    }

    if ($s_units_to_rate <= $s_blob_upto1)
    {
        goto B1632_END;
    }

    $s_unit_charge = $s_blob_charge2;
    $s_unit_charge = str_replace('$','',$s_unit_charge);
    if ($s_units_to_rate <= $s_blob_upto2)
    {
        goto B1632_END;
    }
    $s_unit_charge = $s_blob_charge3;
    $s_unit_charge = str_replace('$','',$s_unit_charge);
    if ($s_units_to_rate <= $s_blob_upto3)
    {
        goto B1632_END;
    }
    $s_unit_charge = $s_blob_charge4;
    $s_unit_charge = str_replace('$','',$s_unit_charge);
    if ($s_units_to_rate <= $s_blob_upto4)
    {
        goto B1632_END;
    }
    $s_unit_charge = $s_blob_charge5;
    $s_unit_charge = str_replace('$','',$s_unit_charge);
    if ($s_units_to_rate <= $s_blob_upto5)
    {
        goto B1632_END;
    }
    $s_unit_charge = $s_blob_charge6;
    $s_unit_charge = str_replace('$','',$s_unit_charge);
    if ($s_units_to_rate <= $s_blob_upto6)
    {
        goto B1632_END;
    }
    $s_unit_charge = $s_blob_charge7;
    $s_unit_charge = str_replace('$','',$s_unit_charge);
    if ($s_units_to_rate <= $s_blob_upto7)
    {
        goto B1632_END;
    }
    $s_unit_charge = $s_blob_charge8;
    $s_unit_charge = str_replace('$','',$s_unit_charge);
    if ($s_units_to_rate <= $s_blob_upto8)
    {
        goto B1632_END;
    }
    $s_unit_charge = $s_blob_charge9;
    $s_unit_charge = str_replace('$','',$s_unit_charge);
    if ($s_units_to_rate <= $s_blob_upto9)
    {
        goto B1632_END;
    }
    $s_unit_charge = $s_blob_charge10;
    $s_unit_charge = str_replace('$','',$s_unit_charge);
    if ($s_units_to_rate <= $s_blob_upto10)
    {
        goto B1632_END;
    }
B1632_END:
    $s_calced_value = $s_units_to_rate * $s_unit_charge;

	//echo 's_calced_value1='.$s_calced_value.'<br>';
	//echo 's_units_to_rate1='.$s_units_to_rate.'<br>';
	//echo 's_unit_charg1='.$s_unit_charge.'<br>';

    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_a100_rate_this - B1632_END - s_calced_value=$s_calced_value, s_units_to_rate=$s_units_to_rate, s_unit_charge=$s_unit_charge ",$ps_debug_reference_no);
    }

    if (trim(strtoupper($s_blob_ratecompany))<>'TLM_AUSPOST')
    {
    GOTO B1634_END_CHARGE;
    }
    if ($s_units_to_rate>$s_blob_itemexcess)
    {
        goto B1632_AUSPOST_PER_KG_CHARGE;
    }

    $s_calced_value = $s_unit_charge;

    goto Z900_EXIT;

    // pkn 20180827 - if TLM Auspost rates need to set calced value to unit charge
    // pkn 20180827 - if TLM Auspost rates and blob charge contains PER_KG then do standard units to rate * unit change
    // pkn 20180827 - rate unit needs to be weight
    // pkn 20180827 - add basic to only > 22 kgs
    // pkn 20180827 - do calc rates notes
    // pkn 20180827 - goto Z900_EXIT:

B1632_AUSPOST_PER_KG_CHARGE:

    $s_calced_value = $s_units_to_rate * $s_blob_itemkgs + $s_blob_ratebasic;

    goto Z900_EXIT;

B1633_DO_INCREMENTAL:
// for each up to
// $s_calculated_value = "0"
// if units_to_rate > upto1
//{
//     $s_calculated_value = $s_calculated_value + ((upto1) * charge1 )
// }else{
//     $s_calculated_value = $s_calculated_value + ((units_to_rate) * charge1 )
//}
// if units_to_rate > upto2
//{
//     $s_calculated_value = $s_calculated_value + ((upto2 - upto1) * charge2 )
// }else{
//     $s_calculated_value = $s_calculated_value + ((units_to_rate - upto1) * charge2 )
//}
// if units_to_rate > upto3
//{
//     $s_calculated_value = $s_calculated_value + ((upto3 - upto2) * charge3 )
// }else{
//     $s_calculated_value = $s_calculated_value + ((units_to_rate - upto3) * charge3 )
//}
B1634_END_CHARGE:

    $s_calced_value = $s_units_to_rate * $s_unit_charge;
    //echo 'pkn s_calced_value1='.$s_calced_value.'<br>';
    //echo 'pkn s_blob_minimum='.$s_blob_minimum.'<br>';
    //echo 's_units_to_rate='.$s_units_to_rate.'<br>';
    //echo 's_unit_charge='.$s_unit_charge.'<br>';
    //echo 'pkn s_calced_value2='.$s_calced_value.'<br>';
    //echo 's_blob_ratebasic pkn='.$s_blob_ratebasic.'<br>';
    $s_calced_value =  $s_calced_value + $s_blob_ratebasic;

	//echo 's_calced_value2='.$s_calced_value.'<br>';
	//echo 's_units_to_rate2='.$s_units_to_rate.'<br>';
	//echo 's_unit_charg2='.$s_unit_charge.'<br>';
	//echo 's_blob_ratebasic='.$s_blob_ratebasic.'<br>';

B1634_EXTRA_CHARGE_CALC:

    if (substr($s_blob_itemexcess,0,8)=='EXPRESS|')
    {
        $ar_blob_itemexcess=explode('|',$s_blob_itemexcess);
        $s_ec_service=trim($ar_blob_itemexcess[0]);
        $s_ec_item_type=trim($ar_blob_itemexcess[1]);
        $s_ec_charge=trim($ar_blob_itemexcess[2]);
        $s_ec_items=trim($ar_options[14]);
        $s_ec_calced_value=$s_ec_charge*$s_ec_items;
        $s_calced_value=$s_calced_value+$s_ec_calced_value;
    }

    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_a100_rate_this - B1634_END_CHARGE - s_calced_value=$s_calced_value, s_units_to_rate=$s_units_to_rate, s_unit_charge=$s_unit_charge, s_blob_ratebasic=$s_blob_ratebasic ",$ps_debug_reference_no);
    }

    $s_extra_charges=0;
    $s_item_kgs_excess=0;
    $s_item_excess_value=0;
    
B1635_DO_ITEM_1:

    if (strtoupper($s_blob_rateunit) <> "ITEM")
    {
        goto B1635_DO_ITEM_END;
    }
    if ($s_blob_itemexcess <= 0)
    {
        goto B1635_DO_ITEM_END;
    }
    
    $s_item_kgs_excess = $s_kgs - ($s_units_to_rate * $s_blob_itemkgs);
    if ($s_item_kgs_excess > 0)
    {
        $s_item_excess_value = $s_item_kgs_excess * $s_blob_itemexcess;
        $s_extra_charges = $s_extra_charges + $s_item_excess_value;
    }

    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_a100_rate_this - B1635_DO_ITEM_1 - s_blob_rateunit=$s_blob_rateunit, s_item_kgs_excess=$s_item_kgs_excess, s_kgs=$s_kgs, s_units_to_rate=$s_units_to_rate, s_blob_itemkgs=$s_blob_itemkgs, s_item_excess_value=$s_item_excess_value, s_blob_itemexcess=$s_blob_itemexcess, s_extra_charges=$s_extra_charges ",$ps_debug_reference_no);
    }

B1635_DO_ITEM_END:
        
B1636_DO_PALLET_1:

    if (strtoupper($s_blob_rateunit) <> "PALLET")
    {
        goto B1636_DO_PALLET_END;
    } 
    if ($s_blob_itemexcess <= 0)
    {
        goto B1636_DO_PALLET_END;
    }        
    if ($s_blob_itemkgs <= 0)
    {
        goto B1636_DO_PALLET_END;
    }
    
    $s_item_kgs_excess = $s_kgs - ($s_units_to_rate * $s_blob_itemkgs);                        
    if ($s_item_kgs_excess > 0)
    {
        $s_item_excess_value = $s_item_kgs_excess * $s_blob_itemexcess;
        $s_extra_charges = $s_extra_charges + $s_item_excess_value;        
    }

    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_a100_rate_this - B1636_DO_PALLET_1 - s_blob_rateunit=$s_blob_rateunit, $s_item_kgs_excess=$s_item_kgs_excess, $s_kgs=$s_kgs, $s_units_to_rate=$s_units_to_rate, $s_blob_itemkgs=$s_blob_itemkgs, $s_item_excess_value=$s_item_excess_value, $s_blob_itemexcess=$s_blob_itemexcess, $s_extra_charges=$s_extra_charges ",$ps_debug_reference_no);
    }

B1636_DO_PALLET_END:
    
B1637_DO_MINIMUM_CHECK:    
    
    $s_calced_value = $s_calced_value + $s_extra_charges;
        
	//echo 's_calced_value='.$s_calced_value.'<br>';
	//echo '<br>';

    if ($s_calced_value < $s_blob_minimum )
    {
        $s_calced_value = $s_blob_minimum;
        $s_calc_notes = $s_calc_notes."=minimum charge=".$s_blob_minimum." ";
        IF ($s_debug == "YES")
        {
            $this->z901_dump("Do Debug clrate_a100_rate_this - B1637_DO_MINIMUM_CHECK - $s_calc_notes=$s_calc_notes.=minimum charge=.$s_blob_minimum ",$ps_debug_reference_no);
        }
        GOTO Z900_EXIT;
    }        
     
    $s_calc_notes = $s_calc_notes."=ratebasic(".$s_blob_ratebasic.") + (units (".$s_units_to_rate.")*unitrate(".$s_unit_charge."))";

    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_a100_rate_this - B1637_DO_MINIMUM_CHECK - s_calc_notes=$s_calc_notes.ratebasic=$s_blob_ratebasic+ (units=$s_units_to_rate)*unitrate($s_unit_charge)) ",$ps_debug_reference_no);
    }

    GOTO Z900_EXIT;
B169_SKIP:
Z900_EXIT:

    //echo 's_calced_value='.$s_calced_value.'<br>';
    //echo 's_calc_notes='.$s_calc_notes.'<br>';

    //echo 'pkn s_calced_value1='.$s_calced_value.'<br>';
    $s_details_def = "calced_value|calc_notes|";
    $s_details_data = $s_calced_value."|".$s_calc_notes."|";
    $sys_function_out = $s_details_def."|^%##%^|".$s_details_data;
    //echo 'sys_function_out2='.$sys_function_out.'<br>';

    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_a100_rate_this - Z900_EXIT - s_details_def=$s_details_def ",$ps_debug_reference_no);
        $this->z901_dump("Do Debug clrate_a100_rate_this - Z900_EXIT - s_details_data=$s_details_data ",$ps_debug_reference_no);
    }

    IF ($sys_debug == "YES"){echo( $sys_function_name."  returned  = ".$sys_function_out." ");};

    return $sys_function_out;

}
//##########################################################################################################################################################################
function clrate_c100_rate_this_transport($ps_dbcnx,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_options,$ps_reciprocal,$ps_client_special_rating=FALSE,$ps_debug_reference_no=FALSE)
{
A100_TEMPLATE_INIT:

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "clrate_c100_rate_this_transport  called from ".$ps_calledfrom;
    $sys_function_out = "";
    //IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    //IF ($sys_debug == "YES"){echo( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    //IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." params = ps_debug,ps_details_def,ps_details_data,ps_sessionno,ps_key1,ps_key2,ps_key3,ps_key4,ps_key5,ps_options");};
    //IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." params = ".$ps_debug.",".$ps_details_def.",".$ps_details_data.",".$ps_sessionno.",".$ps_key1.",".$ps_key2.",".$ps_key3.",".$ps_key4.",".$ps_key5.",".$ps_options);};

    $s_debug="NO";
    if (isset($_SESSION['scko_do_rate_debug']))
    {
        $s_debug = $_SESSION['scko_do_rate_debug'];
    }
    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_c100_rate_this_transport called from $ps_calledfrom",$ps_debug_reference_no);
        $this->z901_dump("Do Debug clrate_c100_rate_this_transport params = ps_debug,ps_details_def,ps_details_data,ps_sessionno,ps_key1,ps_key2,ps_key3,ps_key4,ps_key5,ps_options",$ps_debug_reference_no);
        $this->z901_dump("Do Debug clrate_c100_rate_this_transport params = $ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_key1,$ps_key2,$ps_key3,$ps_key4,$ps_key5,$ps_options",$ps_debug_reference_no);
    }

    //global $class_main;
    //global $class_sql;

    $class_sql = new clSqlClient();
    $class_main = new clmain();

    //echo 'ps_client_special_rating='.$ps_client_special_rating.'<br>';

    A199_END_TEMPLATE_INIT:
    $ar_options = array();
    $s_option_runtype = "notset";                                                                 

    if (strpos($ps_options,"|") === false)
    {
        goto B000_RECORD;
    }
    $ar_options = explode("|",$ps_options);
    $s_option_runtype = $ar_options[0];


    $s_rateco = $ar_options[1];
    $s_account  = $ar_options[2];
    $s_service = $ar_options[3];
    $s_zone_from = $ar_options[4];
    $s_zone_to = $ar_options[5];

    $s_options = $ps_options;

B000_RECORD:
// translate the reateco, account service
// check to see if the towns have zones to use
// check to see if postcodes have zones to use
// use the postcodes if no zones found
B160_TCORATE:
    IF ($s_option_runtype <> "TCORATE")
    {
        GOTO B169_SKIP;
    }
    $s_pc_from = $ar_options[3];
    $s_pc_to = $ar_options[4];
    $s_town_from = $ar_options[5];
    $s_town_to = $ar_options[6];
    goto C100_ZONE_FROM_TOWN;
B169_SKIP:


$sys_debug = "YES";
//goto C100_CALC_VALUE;
C100_ZONE_FROM_TOWN:

    $s_check_type = "TOWN";
    $s_check_value = $s_town_from;
    $s_check_pc = $s_pc_from;
    // pkn 20140115 - BB rating - set pc from as main port of pc from, eg. 4210 becomes 4000
    if ($ps_client_special_rating=='BB')
    {
        $s_check_pc = substr($s_pc_to,0,1).'000';
        if (substr($s_pc_to,0,2)=='08')
        {
            $s_check_pc = substr($s_pc_to,0,2).'00';
        }
    }
    // pkn 20170207 -if ETA CN calculation use pc_from field
    if (trim(strtoupper($ar_options[13]))=='CNDELETA')
    {
        $s_check_pc = $s_pc_from;
    }

    $s_account = $ar_options[2];
    $s_zone_from = $this->clrate_c110_rate_zone($ps_dbcnx,$sys_debug,"clrate_c100_c100a",$ps_details_def,$ps_details_data,$ps_sessionno,$s_rateco,$s_account,$s_service,$s_check_type,$s_check_value,$s_check_pc,$ps_debug_reference_no );
    //IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." pc check1:".$s_zone_from."-".$s_rateco."-".$s_account."-".$s_service."-".$s_check_type."-".$s_check_value."-".$s_rateco."-".$s_account."-".$s_check_pc);};
    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_c100_rate_this_transport C100_ZONE_FROM_TOWN pc check1:$s_zone_from-$s_rateco-$s_account-$s_service-$s_check_type-$s_check_value-$s_rateco-$s_account-$s_check_pc",$ps_debug_reference_no);
    }

    if (substr(strtoupper($s_zone_from),0,16) == "MORE THAN 1 ZONE")
    {
        // pkn 20150410 - skip rating and show error in rate notes
        $result = $s_zone_from;
        goto Z900_EXIT;
    }
    if ($s_zone_from <> "NONE")
    {
        goto C200_ZONE_TO_TOWN;
    }

    $s_check_type = "TOWN";
    $s_check_value = 'ALL';
    $s_check_pc = $s_pc_from;
    // pkn 20140115 - BB rating - set pc from as main port of pc from, eg. 4210 becomes 4000
    if ($ps_client_special_rating=='BB')
    {
        $s_check_pc = substr($s_pc_to,0,1).'000';
        if (substr($s_pc_to,0,2)=='08')
        {
            $s_check_pc = substr($s_pc_to,0,2).'00';
        }
    }
    // pkn 20170207 -if ETA CN calculation use pc_from field
    if (trim(strtoupper($ar_options[13]))=='CNDELETA')
    {
        $s_check_pc = $s_pc_from;
    }

    $s_account = $ar_options[2];
    $s_zone_from = $this->clrate_c110_rate_zone($ps_dbcnx,$sys_debug,"clrate_c100_c100a",$ps_details_def,$ps_details_data,$ps_sessionno,$s_rateco,$s_account,$s_service,$s_check_type,$s_check_value,$s_check_pc,$ps_debug_reference_no );
    //IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." pc check1:".$s_zone_from."-".$s_rateco."-".$s_account."-".$s_service."-".$s_check_type."-".$s_check_value."-".$s_rateco."-".$s_account."-".$s_check_pc);};
    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_c100_rate_this_transport C100_ZONE_FROM_TOWN pc check1:$s_zone_from-$s_rateco-$s_account-$s_service-$s_check_type-$s_check_value-$s_rateco-$s_account-$s_check_pc",$ps_debug_reference_no);
    }
    if (substr(strtoupper($s_zone_from),0,16) == "MORE THAN 1 ZONE")
    {
        // pkn 20150410 - skip rating and show error in rate notes
        $result = $s_zone_from;
        goto Z900_EXIT;
    }
    if ($s_zone_from <> "NONE")
    {
        goto C200_ZONE_TO_TOWN;
    }

    $s_check_type = "TOWN";
    $s_check_value = $s_town_from;
    $s_check_pc = $s_pc_from;
    if ($ps_client_special_rating=='BB')
    {
        $s_check_pc = substr($s_pc_to,0,1).'000';
        if (substr($s_pc_to,0,2)=='08')
        {
            $s_check_pc = substr($s_pc_to,0,2).'00';
        }
    }
    // pkn 20170207 -if ETA CN calculation use pc_from field
    if (trim(strtoupper($ar_options[13]))=='CNDELETA')
    {
        $s_check_pc = $s_pc_from;
    }

    $s_account = "ALL";
    $s_zone_from = $this->clrate_c110_rate_zone($ps_dbcnx,$sys_debug,"clrate_c100_c100a",$ps_details_def,$ps_details_data,$ps_sessionno,$s_rateco,$s_account,$s_service,$s_check_type,$s_check_value,$s_check_pc,$ps_debug_reference_no );
    //IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." pc check2:".$s_zone_from."-".$s_rateco."-".$s_account."-".$s_service."-".$s_check_type."-".$s_check_value."-".$s_rateco."-".$s_account."-".$s_check_pc);};
    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_c100_rate_this_transport C100_ZONE_FROM_TOWN pc check2:$s_zone_from-$s_rateco-$s_account-$s_service-$s_check_type-$s_check_value-$s_rateco-$s_account-$s_check_pc",$ps_debug_reference_no);
    }
    if (substr(strtoupper($s_zone_from),0,16) == "MORE THAN 1 ZONE")
    {
        // pkn 20150410 - skip rating and show error in rate notes
        $result = $s_zone_from;
        goto Z900_EXIT;
    }
    if ($s_zone_from <> "NONE")
    {
        goto C200_ZONE_TO_TOWN;
    }

    $s_check_type = "TOWN";
    $s_check_value = 'ALL';
    $s_check_pc = $s_pc_from;
    if ($ps_client_special_rating=='BB')
    {
        $s_check_pc = substr($s_pc_to,0,1).'000';
        if (substr($s_pc_to,0,2)=='08')
        {
            $s_check_pc = substr($s_pc_to,0,2).'00';
        }
    }
    // pkn 20170207 -if ETA CN calculation use pc_from field
    if (trim(strtoupper($ar_options[13]))=='CNDELETA')
    {
        $s_check_pc = $s_pc_from;
    }

    $s_account = "ALL";
    $s_zone_from = $this->clrate_c110_rate_zone($ps_dbcnx,$sys_debug,"clrate_c100_c100a",$ps_details_def,$ps_details_data,$ps_sessionno,$s_rateco,$s_account,$s_service,$s_check_type,$s_check_value,$s_check_pc,$ps_debug_reference_no );
    //IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." pc check2:".$s_zone_from."-".$s_rateco."-".$s_account."-".$s_service."-".$s_check_type."-".$s_check_value."-".$s_rateco."-".$s_account."-".$s_check_pc);};
    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_c100_rate_this_transport C100_ZONE_FROM_TOWN pc check2:$s_zone_from-$s_rateco-$s_account-$s_service-$s_check_type-$s_check_value-$s_rateco-$s_account-$s_check_pc",$ps_debug_reference_no);
    }
    if (substr(strtoupper($s_zone_from),0,16) == "MORE THAN 1 ZONE")
    {
        // pkn 20150410 - skip rating and show error in rate notes
        $result = $s_zone_from;
        goto Z900_EXIT;
    }
    if ($s_zone_from <> "NONE")
    {
        goto C200_ZONE_TO_TOWN;
    }

C100_ZONE_FROM_PC:

    $s_check_type = "PC";
    $s_check_value = $s_pc_from;
    $s_check_pc = $s_pc_from;
    // pkn 20140115 - BB rating - set pc from as main port of pc from, eg. 4210 becomes 4000
    if ($ps_client_special_rating=='BB')
    {
        $s_check_value = substr($s_pc_to,0,1).'000';
        $s_check_pc = substr($s_pc_to,0,1).'000';
        if (substr($s_pc_to,0,2)=='08')
        {
            $s_check_value = substr($s_pc_to,0,2).'00';
            $s_check_pc = substr($s_pc_to,0,2).'00';
        }
    }
    // pkn 20170207 -if ETA CN calculation use pc_from field
    if (trim(strtoupper($ar_options[13]))=='CNDELETA')
    {
        $s_check_pc = $s_pc_from;
    }

    $s_rateco = $ar_options[1];
    $s_account = $ar_options[2];
    $s_zone_from = $this->clrate_c110_rate_zone($ps_dbcnx,$sys_debug,"clrate_c100_c100B",$ps_details_def,$ps_details_data,$ps_sessionno,$s_rateco,$s_account,$s_service,$s_check_type,$s_check_value,$s_check_pc,$ps_debug_reference_no );
    //IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." pc check3:".$s_zone_from."-".$s_rateco."-".$s_account."-".$s_service."-".$s_check_type."-".$s_check_value."-".$s_rateco."-".$s_account."-".$s_check_pc);};
    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_c100_rate_this_transport C100_ZONE_FROM_PC pc check3:$s_zone_from-$s_rateco-$s_account-$s_service-$s_check_type-$s_check_value-$s_rateco-$s_account-$s_check_pc",$ps_debug_reference_no);
    }
    if (substr(strtoupper($s_zone_from),0,16) == "MORE THAN 1 ZONE")
    {
        // pkn 20150410 - skip rating and show error in rate notes
        $result = $s_zone_from;
        goto Z900_EXIT;
    }
    if ($s_zone_from <> "NONE")
    {
        goto C200_ZONE_TO_TOWN;
    }
    $s_check_type = "PC";
    $s_check_value = $s_pc_from;
    $s_check_pc = $s_pc_from;
    if ($ps_client_special_rating=='BB')
    {
        $s_check_value = substr($s_pc_to,0,1).'000';
        $s_check_pc = substr($s_pc_to,0,1).'000';
        if (substr($s_pc_to,0,2)=='08')
        {
            $s_check_value = substr($s_pc_to,0,2).'00';
            $s_check_pc = substr($s_pc_to,0,2).'00';
        }
    }
    // pkn 20170207 -if ETA CN calculation use pc_from field
    if (trim(strtoupper($ar_options[13]))=='CNDELETA')
    {
        $s_check_pc = $s_pc_from;
    }

    $s_account = "ALL";
    $s_zone_from = $this->clrate_c110_rate_zone($ps_dbcnx,$sys_debug,"clrate_c100_c100B",$ps_details_def,$ps_details_data,$ps_sessionno,$s_rateco,$s_account,$s_service,$s_check_type,$s_check_value,$s_check_pc,$ps_debug_reference_no );
    //IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." pc check4:".$s_zone_from."-".$s_rateco."-".$s_account."-".$s_service."-".$s_check_type."-".$s_check_value."-".$s_rateco."-".$s_account."-".$s_check_pc);};
    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_c100_rate_this_transport C100_ZONE_FROM_PC pc check4:$s_zone_from-$s_rateco-$s_account-$s_service-$s_check_type-$s_check_value-$s_rateco-$s_account-$s_check_pc",$ps_debug_reference_no);
    }
    if (substr(strtoupper($s_zone_from),0,16) == "MORE THAN 1 ZONE")
    {
        // pkn 20150410 - skip rating and show error in rate notes
        $result = $s_zone_from;
        goto Z900_EXIT;
    }
    if ($s_zone_from <> "NONE")
    {
        goto C200_ZONE_TO_TOWN;
    }
        
    $s_zone_from = $s_pc_from;

    //echo 's_zone_from='.$s_zone_from.'<br>';
    //echo 's_pc_from='.$s_pc_from.'<br>';

C200_ZONE_TO_TOWN:

    $s_check_type = "TOWN";
    $s_check_value = $s_town_to;
    $s_check_pc = $s_pc_to;
    $s_rateco = $ar_options[1];
    $s_account  = $ar_options[2];
    $s_zone_to = $this->clrate_c110_rate_zone($ps_dbcnx,$sys_debug,"clrate_c100_c100a",$ps_details_def,$ps_details_data,$ps_sessionno,$s_rateco,$s_account,$s_service,$s_check_type,$s_check_value,$s_check_pc,$ps_debug_reference_no );
    //IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." pc check1:".$s_zone_to."-".$s_rateco."-".$s_account."-".$s_service."-".$s_check_type."-".$s_check_value."-".$s_rateco."-".$s_account."-".$s_check_pc);};
    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_c100_rate_this_transport C200_ZONE_TO_TOWN pc check1:$s_zone_to-$s_rateco-$s_account-$s_service-$s_check_type-$s_check_value-$s_rateco-$s_account-$s_check_pc",$ps_debug_reference_no);
    }
    if (substr(strtoupper($s_zone_to),0,16) == "MORE THAN 1 ZONE")
    {
        // pkn 20150410 - skip rating and show error in rate notes
        $result = $s_zone_to;
        goto Z900_EXIT;
    }
    if ($s_zone_to <> "NONE")
    {
        goto C300_ZONE_ALL;
    }

    $s_check_type = "TOWN";
    $s_check_value = 'ALL';
    $s_check_pc = $s_pc_to;
    $s_rateco = $ar_options[1];
    $s_account  = $ar_options[2];
    $s_zone_to = $this->clrate_c110_rate_zone($ps_dbcnx,$sys_debug,"clrate_c100_c100a",$ps_details_def,$ps_details_data,$ps_sessionno,$s_rateco,$s_account,$s_service,$s_check_type,$s_check_value,$s_check_pc,$ps_debug_reference_no );
    //IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." pc check1:".$s_zone_to."-".$s_rateco."-".$s_account."-".$s_service."-".$s_check_type."-".$s_check_value."-".$s_rateco."-".$s_account."-".$s_check_pc);};
    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_c100_rate_this_transport C200_ZONE_TO_TOWN pc check1:$s_zone_to-$s_rateco-$s_account-$s_service-$s_check_type-$s_check_value-$s_rateco-$s_account-$s_check_pc",$ps_debug_reference_no);
    }
    if (substr(strtoupper($s_zone_to),0,16) == "MORE THAN 1 ZONE")
    {
        // pkn 20150410 - skip rating and show error in rate notes
        $result = $s_zone_to;
        goto Z900_EXIT;
    }
    if ($s_zone_to <> "NONE")
    {
        goto C300_ZONE_ALL;
    }

    $s_check_type = "TOWN";
    $s_check_value = $s_town_to;
    $s_check_pc = $s_pc_to;
    $s_account = "ALL";
    $s_zone_to = $this->clrate_c110_rate_zone($ps_dbcnx,$sys_debug,"clrate_c100_c100a",$ps_details_def,$ps_details_data,$ps_sessionno,$s_rateco,$s_account,$s_service,$s_check_type,$s_check_value,$s_check_pc,$ps_debug_reference_no );
    //IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." pc check2:".$s_zone_to."-".$s_rateco."-".$s_account."-".$s_service."-".$s_check_type."-".$s_check_value."-".$s_rateco."-".$s_account."-".$s_check_pc);};
    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_c100_rate_this_transport C200_ZONE_TO_TOWN pc check1:$s_zone_to-$s_rateco-$s_account-$s_service-$s_check_type-$s_check_value-$s_rateco-$s_account-$s_check_pc",$ps_debug_reference_no);
    }
    if (substr(strtoupper($s_zone_to),0,16) == "MORE THAN 1 ZONE")
    {
        // pkn 20150410 - skip rating and show error in rate notes
        $result = $s_zone_to;
        goto Z900_EXIT;
    }
    if ($s_zone_to <> "NONE")
    {
        goto C300_ZONE_ALL;
    }

    $s_check_type = "TOWN";
    $s_check_value = 'ALL';
    $s_check_pc = $s_pc_to;
    $s_account = "ALL";
    $s_zone_to = $this->clrate_c110_rate_zone($ps_dbcnx,$sys_debug,"clrate_c100_c100a",$ps_details_def,$ps_details_data,$ps_sessionno,$s_rateco,$s_account,$s_service,$s_check_type,$s_check_value,$s_check_pc,$ps_debug_reference_no );
    //IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." pc check2:".$s_zone_to."-".$s_rateco."-".$s_account."-".$s_service."-".$s_check_type."-".$s_check_value."-".$s_rateco."-".$s_account."-".$s_check_pc);};
    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_c100_rate_this_transport C200_ZONE_TO_TOWN pc check2:$s_zone_to-$s_rateco-$s_account-$s_service-$s_check_type-$s_check_value-$s_rateco-$s_account-$s_check_pc",$ps_debug_reference_no);
    }
    if (substr(strtoupper($s_zone_to),0,16) == "MORE THAN 1 ZONE")
    {
        // pkn 20150410 - skip rating and show error in rate notes
        $result = $s_zone_to;
        goto Z900_EXIT;
    }
    if ($s_zone_to <> "NONE")
    {
        goto C300_ZONE_ALL;
    }

C200_ZONE_TO_PC:

    $s_check_type = "PC";
    $s_check_value = $s_pc_to;
    $s_check_pc = $s_pc_to;
    $s_rateco = $ar_options[1];
    $s_account = $ar_options[2];
    $s_zone_to = $this->clrate_c110_rate_zone($ps_dbcnx,$sys_debug,"clrate_c100_c100B",$ps_details_def,$ps_details_data,$ps_sessionno,$s_rateco,$s_account,$s_service,$s_check_type,$s_check_value,$s_check_pc,$ps_debug_reference_no );
    //IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." pc check3:".$s_zone_to."-".$s_rateco."-".$s_account."-".$s_service."-".$s_check_type."-".$s_check_value."-".$s_rateco."-".$s_account."-".$s_check_pc);};
    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_c100_rate_this_transport C200_ZONE_TO_PC pc check3:$s_zone_to-$s_rateco-$s_account-$s_service-$s_check_type-$s_check_value-$s_rateco-$s_account-$s_check_pc",$ps_debug_reference_no);
    }
    if (substr(strtoupper($s_zone_to),0,16) == "MORE THAN 1 ZONE")
    {
        // pkn 20150410 - skip rating and show error in rate notes
        $result = $s_zone_to;
        goto Z900_EXIT;
    }
    if ($s_zone_to <> "NONE")
    {
        goto C300_ZONE_ALL;
    }
    $s_check_type = "PC";
    $s_check_value = $s_pc_to;
    $s_check_pc = $s_pc_to;
    $s_account = "ALL";
    $s_zone_to = $this->clrate_c110_rate_zone($ps_dbcnx,$sys_debug,"clrate_c100_c100B",$ps_details_def,$ps_details_data,$ps_sessionno,$s_rateco,$s_account,$s_service,$s_check_type,$s_check_value,$s_check_pc,$ps_debug_reference_no );
    //IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." pc check4:".$s_zone_to."-".$s_rateco."-".$s_account."-".$s_service."-".$s_check_type."-".$s_check_value."-".$s_rateco."-".$s_account."-".$s_check_pc);};
    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_c100_rate_this_transport C200_ZONE_TO_PC pc check4:$s_zone_to-$s_rateco-$s_account-$s_service-$s_check_type-$s_check_value-$s_rateco-$s_account-$s_check_pc",$ps_debug_reference_no);
    }
    if (substr(strtoupper($s_zone_to),0,16) == "MORE THAN 1 ZONE")
    {
        // pkn 20150410 - skip rating and show error in rate notes
        $result = $s_zone_to;
        goto Z900_EXIT;
    }
    if ($s_zone_to <> "NONE")
    {
        goto C300_ZONE_ALL;
    }

    $s_zone_to = $s_pc_to;

C300_ZONE_ALL:

    /*
    echo 's_zone_from='.$s_zone_from.'<br>';
    echo 's_zone_to='.$s_zone_to.'<br>';
    echo 's_rateco='.$s_rateco.'<br>';
    echo 's_service='.$s_service.'<br>';
    echo 'ar_options[7]='.$ar_options[7].'<br>';
    echo 'ar_options='.'<br>';
    print_r($ar_options);
    echo '<br>';
    exit();
    */

    $s_rateco_check=trim($s_rateco);
    $s_service_check=trim($ar_options[7]);

    $s_do_all_rating=$this->clrate_d100_all_all_rating($s_rateco_check,$s_service_check);

    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_c100_rate_this_transport C300_ZONE_ALL s_rateco_check=$s_rateco_check, s_service_check=$s_service_check, s_do_all_rating=$s_do_all_rating",$ps_debug_reference_no);
    }

    //echo 's_do_all_rating='.$s_do_all_rating.'<br>';
    //exit();

    //if ($s_rateco=='RAM_EMPIRE' or $s_rateco=='RAM_JETCROY')
    if ($s_do_all_rating=='Y')
    {
        // pkn 20131213 - if no other ratezones are found check if ALL->ALL RATE_PCZONE exists for carrier
        $s_zone_from='ALL';
        $s_zone_to='ALL';
    }

C900_END:

C100_CALC_VALUE:
    //echo 'ar_options='.'<br>';
    //print_r($ar_options);
    //echo '<br>';

    // pkn 20170724 - start multi leg rating here?

    //echo 's_zone_from='.$s_zone_from.'<br>';
    //echo 's_zone_to='.$s_zone_to.'<br>';
    //exit();

    $s_rateco = $ar_options[1];
    $s_account  = $ar_options[2];
    $s_service = $ar_options[7];

    //echo 's_zone_from='.$s_zone_from.'<br>';
    //echo 's_zone_to='.$s_zone_to.'<br>';
    //echo 's_account='.$s_account.'<br>';
    //exit();

    if ($_SESSION['orig_use_dest_zone']=='Y')
    {
        $s_zone_from=$s_zone_to;
        if ($s_account=='FDM')
        {
            $s_zone_from='M';
        }        
    }                 

    //echo 's_zone_from2='.$s_zone_from.'<br>';
    //echo 's_zone_to2='.$s_zone_to.'<br>';

    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_c100_rate_this_transport s_zone_from=$s_zone_from, s_zone_to=$s_zone_to",$ps_debug_reference_no);
    }

    $result = $this->clrate_a100_rate_this($ps_dbcnx,$sys_debug,"clrate_c100_c100",$ps_details_def,$ps_details_data,$ps_sessionno,$s_rateco,$s_account,$s_service,$s_zone_from,$s_zone_to,$s_options,$ps_reciprocal,$ps_debug_reference_no);

Z900_EXIT:
    $sys_function_out = $result;

    IF ($sys_debug == "YES"){echo( $sys_debug_text."  z900_EXIT sys_function_out =  ".$sys_function_out);};

    return $sys_function_out;

}
//##########################################################################################################################################################################
function clrate_c110_rate_zone($ps_dbcnx,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_rateco,$ps_account,$ps_service,$ps_check_type,$ps_check_value,$ps_check_pc,$ps_debug_reference_no=FALSE)
{
A100_TEMPLATE_INIT:

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO")
    {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $s_debug = "NO";
    if (isset($_SESSION['scko_do_rate_debug']))
    {
        $s_debug = $_SESSION['scko_do_rate_debug'];
    }
    $sys_function_name = "";
    $sys_function_name = "clrate_c110_rate_zone  called from ".$ps_calledfrom;
    $sys_function_out = "";
    //IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    //IF ($sys_debug == "YES"){echo( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    //IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." params = ps_debug,ps_details_def,ps_details_data,ps_sessionno,ps_key1,ps_key2,ps_key3,ps_key4,ps_key5,ps_options");};
    //IF ($sys_debug == "YES"){echo($sys_debug_text." ".$sys_function_name." params = ".$ps_debug.",".$ps_details_def.",".$ps_details_data.",".$ps_sessionno.",".$ps_key1.",".$ps_key2.",".$ps_key3.",".$ps_key4.",".$ps_key5.",".$ps_options);};

    //global $class_main;
    //global $class_sql;

    $class_sql = new clSqlClient();
    $class_main = new clmain();

    A199_END_TEMPLATE_INIT:
    $s_zone = "NONE";
B100_ORIG_PC_TO_ZONE:
    $s_datablob = "none";
    $s_ur_id = "none";
    $s_rate_notes = "no zone";
    $ar_line_details = array();
    $s_details_def = "";
    $s_details_data = "";

    $ar_options = array();
    $s_option_runtype = "notset";

        //IF ($sys_debug == "YES"){ echo ( $sys_debug_text."=".$sys_function_name." get the job ");};
    $s_sql = "";
    $result = "";
    $s_rec_found = "";
    $s_reccount = "0";

    
    //$s_sql = "SELECT * FROM utl_rates as zonerec where (key1 = '".$ps_rateco."')  and (key2 = '".$ps_account."')  and (key3 = '".$ps_check_pc."')  and (key4 = '".$ps_check_pc."')  and (key5 = 'ALL')";//  and ( companyid = '#p%!_COOKIE_ud_companyid_!%#p') order by createdutime asc";
    $s_sql = "SELECT * FROM utl_rates as zonerec where (key1 = '".$ps_rateco."')  and (key2 = '".$ps_account."')  and (key3 <= '".$ps_check_pc."')  and (key4 >= '".$ps_check_pc."')  and (key5 = 'ALL') and (recordtype = 'RATE_PCZONE') ";//  and ( companyid = '#p%!_COOKIE_ud_companyid_!%#p') order by createdutime asc";
    if ($ps_check_type == "TOWN")
    {
        //$s_sql = "SELECT * FROM utl_rates as zonerec where (key1 = '".$ps_rateco."')  and (key2 = '".$ps_account."')  and (key3 = '".$ps_check_pc."')  and (key4 = '".$ps_check_pc."')  and (key5 = '".$ps_check_value."')";//  and ( companyid = '#p%!_COOKIE_ud_companyid_!%#p') order by createdutime asc";
        //$s_sql = "SELECT * FROM utl_rates as zonerec where (key1 = '".$ps_rateco."')  and (key2 = '".$ps_account."') and (key5 = '".$ps_check_value."') and (recordtype = 'RATE_PCZONE') ";//  and ( companyid = '#p%!_COOKIE_ud_companyid_!%#p') order by createdutime asc";
        $s_sql = "SELECT * FROM utl_rates as zonerec where (key1 = '".$ps_rateco."')  and (key2 = '".$ps_account."') and (key3 = '".$ps_check_pc."')  and (key4 = '".$ps_check_pc."') and (key5 = '".$ps_check_value."') and (recordtype = 'RATE_PCZONE') ";//  and ( companyid = '#p%!_COOKIE_ud_companyid_!%#p') order by createdutime asc";
        if (trim($ps_check_value)=='')
        {
            $s_sql = "SELECT * FROM utl_rates as zonerec where (key1 = '".$ps_rateco."')  and (key2 = '".$ps_account."') and (key3 = '".$ps_check_pc."')  and (key4 = '".$ps_check_pc."') and (recordtype = 'RATE_PCZONE') ";//  and ( companyid = '#p%!_COOKIE_ud_companyid_!%#p') order by createdutime asc";    
        }
        //echo 'town sql='.$s_sql.'<br>';
        //exit();
    }
    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_c110_rate_zone - B100_ORIG_PC_TO_ZONE - s_sql = $s_sql",$ps_debug_reference_no);
    }

    //echo 'ps_check_type='.$ps_check_type.'<br>';
    //echo 'ratezone sql='.$s_sql.'<br>';
B015_DO:
    $result = mysqli_query($ps_dbcnx,$s_sql);
    $s_rec_found = "NO";
    $s_numrows = mysqli_num_rows($result);
    //echo 's_numrows='.$s_numrows.'<br>';
    $i_record_count = "0";
    if ($s_numrows ==  "0")
    {
        IF ($s_debug == "YES")
        {
            //echo ( $sys_debug_text."=".$sys_function_name." thera are no records for s_sql = ".$s_sql." ");
            $this->z901_dump("Do Debug clrate_c110_rate_zone - B015_DO - there are no records for s_sql = \".$s_sql",$ps_debug_reference_no);
        }
        $s_rate_notes = "No data :".$s_sql;
        GOTO Z900_EXIT;
    }
    if ($s_numrows > "1")
    {
        $s_rate_notes = "More that 1 rate exists for sql :".$s_sql;
        $s_zone = "More than 1 zone exists for Carrier:{$ps_rateco}, Account:{$ps_account}, Town:{$ps_check_value}, Postcode:{$ps_check_pc}, SQL for TDX:{$s_sql}";
        IF ($s_debug == "YES")
        {
            //echo ( $sys_debug_text."=".$sys_function_name." thera are multiple records for s_sql = ".$s_sql." ");
            $this->z901_dump("Do Debug clrate_c110_rate_zone - B015_DO - More than 1 zone exists for Carrier:{$ps_rateco}, Account:{$ps_account}, Town:{$ps_check_value}, Postcode:{$ps_check_pc}, SQL for TDX:{$s_sql}",$ps_debug_reference_no);
        }
    }

B100_GET_RECORDS:
    if ($i_record_count > $s_numrows) {
        goto B850_END_RECORDS;
    }
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    IF ($row === false )
    {
        goto B850_END_RECORDS;
    }
    $s_rec_found = "YES";
B150_DO_RECORDS:
    //echo 'Found ratezone sql='.$s_sql.'<br>';
    $s_data_blob=$row['data_blob'];
    //echo 's_data_blob rate zone rec='.$s_data_blob.'<br>';
    $s_zone =  $class_main->clmain_u592_get_field_from_xml($s_data_blob,"ratezone",$sys_debug,"clrate_b1150","NA");

B780_GOT_A_RECORD:
    //IF ($sys_debug == "YES"){ echo ( $sys_debug_text."=".$sys_function_name." got the job record ");};
    $s_data_blob = $row['data_blob'];
    $s_ur_id = $row['ur_id'];
    $s_rate_notes ="Record located for key values";
    goto B900_EXIT;
B800_NEXT:
    $i_record_count = $i_record_count + 1;
    GOTO B100_GET_RECORDS;
B850_END_RECORDS:
B900_EXIT:

Z900_EXIT:
    //echo 's_zone='.$s_zone.'<br>';
    $sys_function_out = $s_zone;

    IF ($s_debug == "YES")
    {
        $this->z901_dump("Do Debug clrate_c110_rate_zone - Z900_EXIT - s_zone=$s_zone",$ps_debug_reference_no);
    }

    IF ($sys_debug == "YES"){echo( $sys_debug_text."  z900_EXIT sys_function_out =  ".$sys_function_out);};

    return $sys_function_out;

}

function clrate_d100_all_all_rating($ps_rateco_check,$ps_service_check)
{
    $s_file_name='';
    $s_translate_file='';
    $s_file_carrier='';
    $s_do_all_rating='N';

    //echo 'ps_rateco_check='.$ps_rateco_check.'<br>';
    //echo 'ps_service_check='.$ps_service_check.'<br>';

    A100_CHECK_FILE_EXISTS:

    $s_file_name=$_SESSION['site_path']."site-details/rating_all_all.ini";
    //echo 's_file_name='.$s_file_name.'<br>';
    //exit();
    if(file_exists($s_file_name))
    {
        $s_translate_file = file($s_file_name);
    }
    else
    {
        //echo "File Not Found: ".$s_file_name;
        goto B990_END;
    }

    B100_CHECK_LINE:


    foreach($s_translate_file as $key=>$s_file_line)
    {
        $s_file_line=strtoupper($s_file_line);

        if(trim($s_file_line)=="")
        {
            goto B900_READ_NEXT_REC;
        }
        if(substr($s_file_line,0,4)=="*TDX")
        {
            goto B900_READ_NEXT_REC;
        }

        $ar_file_line = explode("|",$s_file_line);

        $s_file_carrier=strtoupper(trim($ar_file_line[1]));
        $s_file_service=strtoupper(trim($ar_file_line[2]));

        //echo 's_file_carrier='.$s_file_carrier.'<br>';
        //echo 's_file_service='.$s_file_service.'<br>';

        if ($s_file_carrier<>$ps_rateco_check)
        {
            goto B900_READ_NEXT_REC;
        }
        if ($s_file_service<>$ps_service_check)
        {
            goto B900_READ_NEXT_REC;
        }

        //echo 's_file_carrier_found='.$s_file_carrier.'<br>';
        //echo 's_file_service_found='.$s_file_service.'<br>';

        $s_do_all_rating='Y';
        goto B990_END;

        B900_READ_NEXT_REC:

    }

    B990_END:

    //echo 's_do_all_rating='.$s_do_all_rating.'<br>';
    //exit();

    return $s_do_all_rating;
}

function clrate_d200_convertDatafield($details_def,$details_data,$field_name,$field_default)
{
    $final_data=$field_default;
    //$d_def = split("\|",$details_def);
    //$d_data = split("\|",$details_data);
    $d_def = explode("|",$details_def);
    $d_data = explode("|",$details_data);
    $d_def_count = count($d_def);
    $d_data_count = count($d_data);
    for($i=0;$i<$d_data_count;$i++)
    {
        $data_key = '';
        $data_key = strtoupper(trim($d_def[$i]));
        if (trim(strtoupper($field_name))==$data_key)
        {
            $final_data=$d_data[$i];
        }
    }
    return $final_data;
}

function z901_dump($ps_text,$ps_debug_reference_no=FALSE)
{
    $file_path = "/tdx_archive";
    if (!file_exists($file_path)) {
//        echo "create []".$file_path."[]";
        mkdir($file_path);
    }
    $file_path = $file_path."/".date("Ym",time());
    if (!file_exists($file_path)) {
//        echo "create []".$file_path."[]";
        mkdir($file_path);
    }
    $file_path = $file_path."/debug";
    if (!file_exists($file_path)) {
//        echo "create []".$file_path."[]";
        mkdir($file_path);
    }
    $file_path = $file_path."/".date("d",time());
    if (!file_exists($file_path)) {
//        echo "create []".$file_path."[]";
        mkdir($file_path);
    }
//

    $dateym = date('Ym');
    $myfile = $file_path."/".strtoupper($_SESSION['site_name'])."_class_rate_".$dateym.".html";
    if (trim($ps_debug_reference_no)<>'')
    {
        $myfile = $file_path."/".$ps_debug_reference_no."_".strtoupper($_SESSION['site_name'])."_class_rate_".$dateym.".html";
    }
    $fh = fopen($myfile,'a') or die("cant open file".$myfile);
    fwrite($fh,date("Y-m-d H:i:s",time())." - ".$ps_text."\r\n<br>");
    fclose($fh);
    //   die("gwdieds1ss: filepath=[]".$file_path."[] at ".date("Y-m-d H:i:s",time()));

}

//##########################################################################################################################################################################
} // end of class
?>
