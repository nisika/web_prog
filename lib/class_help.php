<?php

	//************************************************************************************************
	//
	//	Class: Help
	//
	//
	//************************************************************************************************
	
	class Help
	{
		function help_alterhelptbl() {
			$sql="alter table dmahelp add column Heading1 varchar(50) after Key5_def;
				  alter table dmahelp add column Heading2 varchar(50) after Heading1;
				  alter table dmahelp add column Heading3 varchar(50) after Heading2;
				  alter table dmahelp add column Heading4 varchar(50) after Heading3;
				  alter table dmahelp add Key Heading1 (Heading1), 
				  					  add Key Heading2 (Heading2), 
				  					  add Key Heading3 (Heading3), 
				  					  add Key Heading4 (Heading4);";
			$res=SqlClient::ExecuteQuery($sql);
			return $res;
		}
		
		function help_header() {
			$page_type="";
			if(!array_key_exists('p',$_GET)) {
				$page_type="SYSTEM";
			} else {
				$page_type="CONTENT";
			}
			?>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
			<title>The Data Exchange : Delivery Management Assistant : Help</title>
			<link type="text/css" rel="stylesheet" href="styleMenu.css" />
			<script language="JavaScript" type="text/javascript">
			<!--//--><![CDATA[//><!--
			function getHM(id,type) {
				var uri_str;
				var page_type="<?=$page_type?>";
				if(type!=null) {
					if(type=="COPY") {
						uri_str="mainpage.php?ReturnUrl=help_maint&id="+id+"&s&z";
					} else if(type=="DEL") {
						uri_str="mainpage.php?ReturnUrl=help_maint&id="+id+"&s&d";
					} else {
						uri_str="mainpage.php?ReturnUrl=help_maint&new&id="+id+"&type="+type;
					}
				} else {
					if(page_type=="SYSTEM") {
						uri_str="mainpage.php?ReturnUrl=help_maint&id="+id+"&s";
					} else {
						uri_str="mainpage.php?ReturnUrl=help_maint&id="+id+"&c";
					}
				}
				//winHelpMaint=window.open(uri_str,'helpMaintWindow','scrollbars,status=1,resizable,location=1');
				winHelpMaint=window.open(uri_str,'helpMaintWindow');
				winHelpMaint.focus();
			}
			function swap( divNum, expanding )
			{
				var c_t=document.getElementById("c" + divNum);
				var e_t=document.getElementById("e" + divNum);
				var t_t=document.getElementById("t" + divNum);
			    if (expanding)
			    {
					c_t.style.display="none";
					e_t.style.display="inline";
					t_t.style.display="inline";
			    }
			    else
			    {
					e_t.style.display="none";
					t_t.style.display="none";
					c_t.style.display="inline";
			    }
			}
			//-->
			</script>
			</head>
			<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" id="normal-table">
			<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
			  <tr>
			  	<td align="left" valign="top">
			<?
		}
		
		function help_footer() {
			?>	</td>
			  </tr>
			</table>
			</body>
			</html>
			<?
		}
		
		function help_display_search($search_param=false) {
			?>
			<form action="showhelp.php" enctype="multipart/form-data" method="post" name="helpForm">
			<table border="0" width="100%" id="normal-table">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="3" id="body-text" bordercolor="black">
							<tr class="helphead1">
								<td align="right">Search Help</td>
								<td align="center" width="190"><input type="text" maxlength="50" size="30" name="txtSearchHelp" value="<?=$search_param?>"></td>
								<td align="center"><input type="submit" name="btnSearchHelp" value="Search" class="buttons"></td>
								<td align="left"><input type="submit" name="btnSearchReset" value="Reset" class="buttons"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</form>
			<?
		}
		
		function help_display_err($txt=false) {
			?>
			<table border="0" width="100%" id="normal-table">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="3" id="body-text" bordercolor="black">
							<tr class="helphead1">
								<td align="center"><b><?=$txt?></b></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<?
		}
		
		function help_getlist($search_param=false) {
			$site_name=strtoupper($_SESSION['site_name']);
			if(!empty($_SESSION['help_db'])) {
				$use_db="{$_SESSION['help_db']}.";
			}
			$show_all=false;
			$comp_proc="";
			if(strtoupper($_SESSION['valid_user'])<>"TDXSUPPORT" && $_SESSION['user_role']<>"ADMIN") {
				$comp_proc=", '".strtoupper($_SESSION['user_company'])." PROCEDURES'";
				$help_restrict=strtoupper($_SESSION['help_restrict']);
					if(strlen($help_restrict)>0) {
					if($search_param<>false) {
						$search_sql="where upper(Heading1) in('$help_restrict'{$comp_proc}) and Heading2 like '%{$search_param}%' or upper(Heading1)='$help_restrict' and Heading3 like '%{$search_param}%' or upper(Heading1)='$help_restrict' and Heading4 like '%{$search_param}%' ";
						$p=$search_param;
					} else {
						$search_sql="where upper(Heading1) in('$help_restrict'{$comp_proc}) ";
					}
				} else {
					$show_all=true;
				}
			} else {
				$show_all=true;
			}
			if($show_all===true) {
				if($search_param<>false) {
					$search_sql="where Heading1 like '%{$search_param}%' or Heading2 like '%{$search_param}%' or Heading3 like '%{$search_param}%' or Heading4 like '%{$search_param}%' ";
					$p=$search_param;
				}
			}
			$sql="select UniqueKey,Key1,Key5,Heading1,Heading2,Heading3,Heading4,Details_def,Details_data from {$use_db}dmahelp {$search_sql}order by Heading1,Heading2,Heading3,Heading4";
			//echo $sql."<br />";
			$res=SqlClient::ExecuteQuery($sql);
			$cnt=SqlClient::GetRowCount($res);
			if($cnt>0) {
				while ($row=SqlClient::FetchArray($res)) {
					$data=SqlClient::convertData($row['Details_def'],$row['Details_data']);
					$h1=$row['Heading1']; $h2=$row['Heading2']; $h3=$row['Heading3']; $h4=$row['Heading4'];
					if(!empty($h1) && $search_param<>false) {
						$h1=preg_replace("/{$p}/i","<span id=\"highlight_search\">$0</span>",$h1);
					}
					if(!empty($h2) && $search_param<>false) {
						$h2=preg_replace("/{$p}/i","<span id=\"highlight_search\">$0</span>",$h2);
					}
					if(!empty($h3) && $search_param<>false) {
						$h3=preg_replace("/{$p}/i","<span id=\"highlight_search\">$0</span>",$h3);
					}
					if(!empty($h4) && $search_param<>false) {
						$h4=preg_replace("/{$p}/i","<span id=\"highlight_search\">$0</span>",$h4);
					}
					if(!empty($h1) && !empty($h2) && !empty($h3) && !empty($h4)) {
						$arr[$h1][$h2][$h3][$h4]['']['text']=$data['HelpText'];
						$arr[$h1][$h2][$h3][$h4]['']['name']="{$row['Key1']} - {$row['Key5']}";
						$arr[$h1][$h2][$h3][$h4]['']['key']=$row['UniqueKey'];
						$arr[$h1][$h2][$h3][$h4]['']['id']=$row['Key1'];
					} elseif(!empty($h1) && !empty($h2) && !empty($h3) && empty($h4)) {
						$arr[$h1][$h2][$h3]['']['text']=$data['HelpText'];
						$arr[$h1][$h2][$h3]['']['name']="{$row['Key1']} - {$row['Key5']}";
						$arr[$h1][$h2][$h3]['']['key']=$row['UniqueKey'];
						$arr[$h1][$h2][$h3]['']['id']=$row['Key1'];
					} elseif(!empty($h1) && !empty($h2) && empty($h3) && empty($h4)) {
						$arr[$h1][$h2]['']['text']=$data['HelpText'];
						$arr[$h1][$h2]['']['name']="{$row['Key1']} - {$row['Key5']}";
						$arr[$h1][$h2]['']['key']=$row['UniqueKey'];
						$arr[$h1][$h2]['']['id']=$row['Key1'];
					} elseif(!empty($h1) && empty($h2) && empty($h3) && empty($h4)) {
						$arr[$h1]['']['text']=$data['HelpText'];
						$arr[$h1]['']['name']="{$row['Key1']} - {$row['Key5']}";
						$arr[$h1]['']['key']=$row['UniqueKey'];
						$arr[$h1]['']['id']=$row['Key1'];
					}
				}
			}
			if(is_array($arr)) {
				return $arr;
			}
			return false;
		}
		
		function help_display_list($arr,$search_param=false) {
			if(!is_array($arr)) {
				return false;
			}
			$i=0;
			$l=0;
			$arr_cnt=0;
			$width=10;
			$plus_img="<img alt=\"more\" src=\"images/plus.gif\" border=\"0\" width=\"16\" height=\"16\">";
			$minus_img="<img alt=\"less\" src=\"images/minus.gif\" border=\"0\" width=\"16\" height=\"16\">";
			?>
			<table border="0" width="100%" id="normal-table">
				<tr>
					<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="3" id="body-text" bordercolor="black">
				<tr class="helphead1">
					<td>
						<table width="100%" border="0" cellspacing="3" cellpadding="0" class="helphead1">
						<?
						$arr_cnt=count($arr);
						foreach ($arr as $h1_k=>$h1_v) {
							if(empty($h1_k)) {
								echo "<tr><td colspan=\"2\">".SqlClient::main_formatHelpText($h1_v['text'])."</td></tr>";
							} else {
								$l++;
								echo "<tr><td colspan=\"2\">";
								?>
								<span class="short" id="c<?=$i?>"><a href="javascript:swap('<?=$i?>',true)"><?=$plus_img?></a></span>
								<span class="long" id="e<?=$i?>"><a href="javascript:swap('<?=$i?>',false)"><?=$minus_img?></a></span>
								<?
								echo "{$h1_k}</td></tr>";
								echo "<tr class=\"long\" id=\"t{$i}\"><td width=\"{$width}\">&nbsp;</td>";
								echo "<td width=\"100%\">";
								echo "<table width=\"100%\" height=\"100%\" cellspacing=\"0\" cellpadding=\"3\" border=\"0\" class=\"helphead2\">";
								$i++;
								foreach ($h1_v as $h2_k=>$h2_v) {
									$edit_button="";
									if($_SESSION['help_dev_access']==="Y" || strtoupper($_SESSION['user_company'])===strtoupper($h2_v['']['id'])) {
										$edit_button="<input type=\"button\" name=\"btnEdit\" value=\"Edit \" onClick=\"getHM('{$h2_v['']['key']}')\" class=\"buttons\" />";
										$edit_button.="<input type=\"button\" name=\"btnCopy\" value=\"Copy \" onClick=\"getHM('{$h2_v['']['key']}','COPY')\" class=\"buttons\" />";
										$edit_button.="<input type=\"button\" name=\"btnDel\" value=\"Delete \" onClick=\"getHM('{$h2_v['']['key']}','DEL')\" class=\"buttons\" />";
									}
									if(empty($h2_k)) {
										if($_SESSION['help_dev_access']==="Y" || strtoupper($_SESSION['user_company'])===strtoupper($h1_v['']['id'])) {
											$edit_button="<input type=\"button\" name=\"btnEdit\" value=\"Edit \" onClick=\"getHM('{$h1_v['']['key']}')\" class=\"buttons\" />";
											$edit_button.="<input type=\"button\" name=\"btnCopy\" value=\"Copy \" onClick=\"getHM('{$h1_v['']['key']}','COPY')\" class=\"buttons\" />";
											$edit_button.="<input type=\"button\" name=\"btnDel\" value=\"Delete \" onClick=\"getHM('{$h1_v['']['key']}','DEL')\" class=\"buttons\" />";
										}
										echo "<tr><td colspan=\"2\">".SqlClient::main_formatHelpText($h1_v['']['text'])."</td></tr>";
										echo "<tr><td colspan=\"2\" class=\"helpmini\">{$h1_v['']['name']}{$edit_button}</td></tr>";
									} else {
										echo "<tr><td colspan=\"2\">";
										?>
										<span class="short" id="c<?=$i?>"><a href="javascript:swap('<?=$i?>',true)"><?=$plus_img?></a></span>
										<span class="long" id="e<?=$i?>"><a href="javascript:swap('<?=$i?>',false)"><?=$minus_img?></a></span>
										<?
										echo "{$h2_k}</td></tr>";
										echo "<tr class=\"long\" id=\"t{$i}\"><td width=\"{$width}\">&nbsp;</td>";
										echo "<td width=\"100%\">";
										echo "<table width=\"100%\" height=\"100%\" cellspacing=\"0\" cellpadding=\"3\" border=\"0\" class=\"helphead3\">";
										$i++;
										foreach ($h2_v as $h3_k=>$h3_v) {
											if(empty($h3_k)) {
												$edit_button="";
												if($_SESSION['help_dev_access']==="Y" || strtoupper($_SESSION['user_company'])===strtoupper($h2_v['']['id'])) {
													$edit_button="<input type=\"button\" name=\"btnEdit\" value=\"Edit \" onClick=\"getHM('{$h2_v['']['key']}')\" class=\"buttons\" />";
													$edit_button.="<input type=\"button\" name=\"btnCopy\" value=\"Copy \" onClick=\"getHM('{$h2_v['']['key']}','COPY')\" class=\"buttons\" />";
															$edit_button.="<input type=\"button\" name=\"btnDel\" value=\"Delete \" onClick=\"getHM('{$h2_v['']['key']}','DEL')\" class=\"buttons\" />";
												}
												echo "<tr><td colspan=\"2\">".SqlClient::main_formatHelpText($h2_v['']['text'])."</td></tr>";
												echo "<tr><td colspan=\"2\" class=\"helpmini\">{$h2_v['']['name']}{$edit_button}</td></tr>";
											} else {
												echo "<tr><td colspan=\"2\">";
												?>
												<span class="short" id="c<?=$i?>"><a href="javascript:swap('<?=$i?>',true)"><?=$plus_img?></a></span>
												<span class="long" id="e<?=$i?>"><a href="javascript:swap('<?=$i?>',false)"><?=$minus_img?></a></span>
												<?
												echo "{$h3_k}</td></tr>";
												echo "<tr class=\"long\" id=\"t{$i}\"><td width=\"{$width}\">&nbsp;</td>";
												echo "<td width=\"100%\">";
												echo "<table width=\"100%\" height=\"100%\" cellspacing=\"0\" cellpadding=\"3\" border=\"0\" class=\"helphead4\">";
												$i++;
												foreach ($h3_v as $h4_k=>$h4_v) {
													if(empty($h4_k)) {
														$edit_button="";
														if($_SESSION['help_dev_access']==="Y" || strtoupper($_SESSION['user_company'])===strtoupper($h3_v['']['id'])) {
															$edit_button="<input type=\"button\" name=\"btnEdit\" value=\"Edit \" onClick=\"getHM('{$h3_v['']['key']}')\" class=\"buttons\" />";
															$edit_button.="<input type=\"button\" name=\"btnCopy\" value=\"Copy \" onClick=\"getHM('{$h3_v['']['key']}','COPY')\" class=\"buttons\" />";
															$edit_button.="<input type=\"button\" name=\"btnDel\" value=\"Delete \" onClick=\"getHM('{$h3_v['']['key']}','DEL')\" class=\"buttons\" />";
														}
														echo "<tr><td colspan=\"2\">".SqlClient::main_formatHelpText($h3_v['']['text'])."</td></tr>";
														echo "<tr><td colspan=\"2\" class=\"helpmini\">{$h3_v['']['name']}{$edit_button}</td></tr>";
													} else {
														$edit_button="";
														if($_SESSION['help_dev_access']==="Y" || strtoupper($_SESSION['user_company'])===strtoupper($h4_v['']['id'])) {
															$edit_button="<input type=\"button\" name=\"btnEdit\" value=\"Edit \" onClick=\"getHM('{$h4_v['']['key']}')\" class=\"buttons\" />";
															$edit_button.="<input type=\"button\" name=\"btnCopy\" value=\"Copy \" onClick=\"getHM('{$h4_v['']['key']}','COPY')\" class=\"buttons\" />";
															$edit_button.="<input type=\"button\" name=\"btnDel\" value=\"Delete \" onClick=\"getHM('{$h4_v['']['key']}','DEL')\" class=\"buttons\" />";
														}
														echo "<tr><td colspan=\"2\">";
														?>
														<span class="short" id="c<?=$i?>"><a href="javascript:swap('<?=$i?>',true)"><?=$plus_img?></a></span>
														<span class="long" id="e<?=$i?>"><a href="javascript:swap('<?=$i?>',false)"><?=$minus_img?></a></span>
														<?
														echo "{$h4_k}</td></tr>";
														echo "<tr class=\"long\" id=\"t{$i}\"><td width=\"{$width}\">&nbsp;</td>";
														echo "<td width=\"100%\">";
														echo "<table width=\"100%\" height=\"100%\" cellspacing=\"0\" cellpadding=\"3\" border=\"0\" class=\"helptext1\">";
														echo "<tr><td>".SqlClient::main_formatHelpText($h4_v['']['text'])."</td></tr>";
														echo "<tr><td class=\"helpmini\">{$h4_v['']['name']}{$edit_button}</td></tr>";
														echo "</table>";
														echo "</td>";
														echo "</tr>";
														$i++;
													}
												}
												echo "</table>";
												echo "</td>";
												echo "</tr>";
											}
										}
										echo "</table>";
										echo "</td>";
										echo "</tr>";
									}
								}
								echo "</table>";
								echo "</td>";
								echo "</tr>";
								if($arr_cnt>$l) {
									echo "<tr bgcolor=\"#000000\"><td colspan=\"2\"><img src=\"images/spacer.gif\" border=\"0\" height=\"1\" width=\"1\" /></td></tr>";
								}
							}
						}
						?>
						</table>
					</td>
				</tr>
			</table>
					</td>
				</tr>
			</table>
			<?
		}
		
		function help_display_id($id) {
			// check for userhelp desc, if not there, go to sitehelp
			$site_name=strtoupper($_SESSION['site_name']);
			$current_user=strtoupper($_SESSION['valid_user']);
			$use_db="";
			$help_desc=false;
			if(!empty($_SESSION['help_db'])) {
				$use_db="{$_SESSION['help_db']}.";
			}
			$help_user_sql="select Key5 from {$use_db}dmahelp where Key1='{$id}' and Key2='{$site_name}' and Key3='{$current_user}' and TrackGroup='USERHELP'";
			$help_user_res=SqlClient::arrayList($help_user_sql);
			if(is_array($help_user_res)) {
				$help_desc=$help_user_res[0]['Key5'];
			}
			if($help_desc===false) {
				$help_site_sql="select Key5 from {$use_db}dmahelp where Key1='{$id}' and Key2='{$site_name}' and TrackGroup='SITEHELP'";
				$help_site_res=SqlClient::arrayList($help_site_sql);
				if(is_array($help_site_res)) {
					$help_desc=$help_site_res[0]['Key5'];
				}
			}
			if($help_desc===false) {
				$help_site_sql="select Key5 from {$use_db}dmahelp where Key1='{$id}' and TrackGroup='SYSHELP'";
				$help_site_res=SqlClient::arrayList($help_site_sql);
				if(is_array($help_site_res)) {
					$help_desc=$help_site_res[0]['Key5'];
				}
			}
			if($help_desc===false) {
				$help_desc="Help for <b>{$id}</b>";
			}
			?>
			<table border="0" width="100%" id="normal-table">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="5" id="body-text" bordercolor="black">
							<tr bgcolor="#ECECEC">
								<td><?=$help_desc?></td><td align="right"><?=date("d/m/Y H:i A")?></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<?
		}
		
		function help_display_notes($id,$type) {
			$type=strtoupper($type);
			$site_name=strtoupper($_SESSION['site_name']);
			$current_user=strtoupper($_SESSION['valid_user']);
			$use_db="";
			if(!empty($_SESSION['help_db'])) {
				$use_db="{$_SESSION['help_db']}.";
			}
			$help_type_arr=PickUp::getStatusFilterData("HELP_MAINT_TYPES");
			$disp_type=$help_type_arr["{$type}HELP"];
			switch ($type) {
				case "USER":$k2=" and Key2='{$site_name}'";$k3=" and Key3='{$current_user}'";break;
				case "SITE":$k2=" and Key2='{$site_name}'";$k3="";break;
				default:$k2="";$k3="";
			}
			
			$help_sql="select * from {$use_db}dmahelp where Key1='{$id}'{$k2}{$k3} and TrackGroup='{$type}HELP'";
			$help_res=SqlClient::arrayList($help_sql);
			if(is_array($help_res)) {
				$help_row=SqlClient::convertData($help_res[0]['Details_def'],$help_res[0]['Details_data']);
				if(is_array($help_row)) {
					$HelpText=SqlClient::main_formatHelpText($help_row['HelpText']);
					$HelpType=$help_type_arr[$help_res[0]['TrackGroup']];
					$HelpButton="<input type=\"button\" class=\"buttons\" value=\"Edit\" onClick=\"getHM('{$help_res[0]['UniqueKey']}')\" />";
					if($help_type_arr==false) {
						switch ($type) {
							case "USER":$HelpType="User";break;
							case "SITE":$HelpType="Site";break;
							case "SYS":$HelpType="System";break;
							default:$HelpType="Development";
						}
					}
					switch ($type) {
						case "USER":$HelpInfo="{$HelpType} notes for {$current_user}";break;
						case "SITE":$HelpInfo="{$HelpType} notes for {$site_name}";break;
						case "SYS":$HelpInfo="{$HelpType} notes";break;
						default:$HelpInfo="{$HelpType}";
					}
				}
			} else {
				if($help_type_arr==false) {
					switch ($type) {
						case "USER":$disp_type="User";break;
						case "SITE":$disp_type="Site";break;
						case "SYS":$disp_type="System";break;
						default:$disp_type="Development";
					}
				}
				$HelpButton="<input type=\"button\" class=\"buttons\" value=\"Add\" onClick=\"getHM('{$id}','{$type}')\" />";
				$HelpInfo="{$disp_type} notes";
				$HelpText="No help information has been added for {$disp_type}.";
			}
			if(($type==="USER" && $_SESSION['help_user_access']<>"Y") || 
			   ($type==="SITE" && $_SESSION['help_site_access']<>"Y") || 
			   ($type==="SYS" && $_SESSION['help_sys_access']<>"Y")) {
				$HelpButton="";
			}
			?>
			<table border="0" width="100%" id="normal-table">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="5" id="body-text" bordercolor="black">
							<tr bgcolor="#ECECEC">
								<td><u><?=$HelpInfo?></u></td>
								<td align="right"><?=$HelpButton?></td>
							</tr>
							<tr bgcolor="#ECECEC">
								<td colspan="2"><?=$HelpText?></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<?
		}
		
		function help_helpMaint($data=false) {
			$dis_sid=false;
			$dis_uid=false;
			$dis_sname=false;
			$dis_help=false;
			$dis_headings=false;
			if($data['disableLoad']<>"Y") {
				Help::help_displayHelpLoad($data);
			}
			$help_type_arr=PickUp::getStatusFilterData("HELP_MAINT_TYPES");
			$help_type=PickUp::createSelectBox($help_type_arr,"Help_type_sel",$data['Help_type_sel'],0);
			if($data['disable']==="Y") {
				$dis=true;
				$dis_help=true;
			}
			if($data['is_edit']==="Y") {
				switch ($data['Help_type_sel']) {
					case "DEVHELP": $dis_uid=true; $dis_sname=true; $dis_sid=true; $dis_help=true; break;
					case "SYSHELP": $dis_uid=true; $dis_sid=true; $dis_help=true; break;
					case "SITEHELP": $dis_uid=true; $dis_sname=true; $dis_sid=true; $dis_help=true; break;
					case "USERHELP": $dis_uid=true; $dis_sname=true; $dis_sid=true; $dis_help=true; break;
				}
			}
			if(($_SESSION['help_user_access']==="Y" || $_SESSION['help_site_access']==="Y") && 
			   ($_SESSION['help_sys_access']<>"Y" || $_SESSION['help_dev_access']<>"Y")) {
				$dis_headings=true;
			}
			if($dis_help===true) {
				$help_type="{$help_type_arr[$data['Help_type_sel']]}<input type=\"hidden\" name=\"Help_type_sel\" maxlength=\"50\" value=\"{$data['Help_type_sel']}\">";
			}
			$body_id="";
			if($data['IsUpdate']==="Y") {
				$body_id="-copy";
			}
			?>
			<script language="JavaScript" type="text/javascript" src="js/bbscript.js">
			
			</script>
			<table border="0" width="740" id="normal-table">
				<tr>
					<td>
						<table width="740" border="0" cellspacing="0" cellpadding="2" id="body-text<?=$body_id?>" bordercolor="black">
							<tr>
								<td colspan="2">
									<table width="100%"  border="0" cellspacing="0" cellpadding="0" id="body-text-headers">
										<tr>
											<td><strong>&nbsp;Help Maintenance</strong></td>
										</tr>
									</table><hr width="95%" size="1"></td>
								</tr>
								<tr>
									<td width="30%">&nbsp;Screen ID :</td>
									<?if($dis_sid===true) {?>
									<td><?=$data['Screen_ID']?><input type="hidden" name="Screen_ID" maxlength="50" value="<?=$data['Screen_ID']?>"></td>
									<?} else {?>
									<td><input type="text" name="Screen_ID" size="30" maxlength="50" value="<?=$data['Screen_ID']?>"></td>
									<?}?>
								</tr>
								<tr>
									<td width="30%">&nbsp;Screen Description :</td>
									<td><input type="text" name="Screen_Desc" size="30" maxlength="50" value="<?=$data['Screen_Desc']?>"></td>
								</tr>
								<tr>
									<td width="30%">&nbsp;User ID :</td>
									<?if($dis_uid===true) {?>
									<td><?=$data['User_ID']?><input type="hidden" name="User_ID" maxlength="50" value="<?=$data['User_ID']?>"></td>
									<?} else {?>
									<td><input type="text" name="User_ID" size="30" maxlength="50" value="<?=$data['User_ID']?>"></td>
									<?}?>
								</tr>
								<tr>
									<td width="30%">&nbsp;Site Name :</td>
									<?if($dis_sname===true) {?>
									<td><?=$data['Site_Name']?><input type="hidden" name="Site_Name" maxlength="50" value="<?=$data['Site_Name']?>"></td>
									<?} else {?>
									<td><input type="text" name="Site_Name" size="30" maxlength="50" value="<?=$data['Site_Name']?>"></td>
									<?}?>
								</tr>
								<tr>
									<td width="30%">&nbsp;Help Type :</td>
									<td><?=$help_type?></td>
								</tr>
								<?if($dis_headings===false) {?>
								<tr>
									<td colspan="2">&nbsp;</td>
								</tr>
								<tr>
									<td width="30%">&nbsp;Heading 1 :</td>
									<td><input type="text" name="Heading1" size="30" maxlength="50" value="<?=$data['Heading1']?>"></td>
								</tr>
								<tr>
									<td width="30%">&nbsp;Heading 2 :</td>
									<td><input type="text" name="Heading2" size="30" maxlength="50" value="<?=$data['Heading2']?>"></td>
								</tr>
								<tr>
									<td width="30%">&nbsp;Heading 3 :</td>
									<td><input type="text" name="Heading3" size="30" maxlength="50" value="<?=$data['Heading3']?>"></td>
								</tr>
								<tr>
									<td width="30%">&nbsp;Heading 4 :</td>
									<td><input type="text" name="Heading4" size="30" maxlength="50" value="<?=$data['Heading4']?>"></td>
								</tr>
								<tr>
									<td colspan="2">&nbsp;</td>
								</tr>
								<?} else {?>
								<tr>
									<td colspan="2">&nbsp;
									<input type="hidden" name="Heading1" value="<?=$data['Heading1']?>">
									<input type="hidden" name="Heading2" value="<?=$data['Heading2']?>">
									<input type="hidden" name="Heading3" value="<?=$data['Heading3']?>">
									<input type="hidden" name="Heading4" value="<?=$data['Heading4']?>">
									</td>
								</tr>
								<?}?>
								<tr>
									<td colspan="2">
										<table width="535" cellpadding="5" cellspacing="2" border="0" align="center" id="body-text">
							            	<tr>
							            		<td width="370">Help Text</td>
							            		<td width="40" align="center"><input type="button" class="buttons" accesskey="b" name="addbbcode0" value="B " style="font-weight:bold; width: 30px" onClick="bbstyle(0,'HelpText')" /></td>
							            		<td width="40" align="center"><input type="button" class="buttons" accesskey="i" name="addbbcode2" value="i " style="font-style:italic; width: 30px" onClick="bbstyle(2,'HelpText')" /></td>
							            		<td width="40" align="center"><input type="button" class="buttons" accesskey="u" name="addbbcode4" value="u " style="text-decoration: underline; width: 30px" onClick="bbstyle(4,'HelpText')" /></td>
							            		<td width="45" align="center"><input type="button" class="buttons" accesskey="c" name="addbbcode16" value="CODE" style="width: 45px" onClick="bbstyle(8,'HelpText')" /></td>
							            	</tr>
							            	<tr>
							            		<td colspan="5" align="center">
							            			<p align="center">&nbsp;&nbsp;&nbsp;<textarea name="HelpText" rows="10" cols="70" class="post" onselect="storeCaret(this);" onclick="storeCaret(this);" onkeyup="storeCaret(this);" onkeydown="insertTab(this, event);" style="font-family:Courier New;"><?=$data['HelpText']?></textarea><b>&nbsp;&nbsp;&nbsp;</b></p>
							            		</td>
							            	</tr>
							            	<tr>
							            		<td colspan="5" align="center"><img src="images/spacer.gif" width="1" height="1"></td>
							            	</tr>
							            </table>
									</td>
								</tr>
								<tr>
									<td colspan="2">&nbsp;
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<table width="535" cellpadding="5" cellspacing="2" border="0" align="center" id="body-text">
							            	<tr>
							            		<td width="370">Description Text</td>
							            		<td width="40" align="center"><input type="button" class="buttons" accesskey="b" name="addbbcode0" value="B " style="font-weight:bold; width: 30px" onClick="bbstyle(0,'DescText')" /></td>
							            		<td width="40" align="center"><input type="button" class="buttons" accesskey="i" name="addbbcode2" value="i " style="font-style:italic; width: 30px" onClick="bbstyle(2,'DescText')" /></td>
							            		<td width="40" align="center"><input type="button" class="buttons" accesskey="u" name="addbbcode4" value="u " style="text-decoration: underline; width: 30px" onClick="bbstyle(4,'DescText')" /></td>
							            		<td width="45" align="center"><input type="button" class="buttons" accesskey="c" name="addbbcode16" value="CODE" style="width: 45px" onClick="bbstyle(8,'DescText')" /></td>
							            	</tr>
							            	<tr>
							            		<td colspan="5" align="center">
							            			<p align="center">&nbsp;&nbsp;&nbsp;<textarea name="DescText" rows="3" cols="70" class="post" onselect="storeCaret(this);" onclick="storeCaret(this);" onkeyup="storeCaret(this);" onkeydown="insertTab(this, event);" style="font-family:Courier New;"><?=$data['DescText']?></textarea><b>&nbsp;&nbsp;&nbsp;</b></p>
							            		</td>
							            	</tr>
							            	<tr>
							            		<td colspan="5" align="center"><img src="images/spacer.gif" width="1" height="1"></td>
							            	</tr>
							            </table>
									</td>
								</tr>
								<tr>
									<td align="center" colspan="2" height="40">
									<?
									if($data['IsUpdate']==="Y") {
									?>
										<input type="hidden" name="LoadHelpUK" value="<?=$data['LoadHelpUK']?>">
										<input type="hidden" name="IsUpdate" value="<?=$data['IsUpdate']?>">
										<input type="submit" name="btnUpdHelp" value="Update Help" class="buttons">
									<?
									} else {
										?><input type="submit" name="btnAddHelp" value="Add Help" class="buttons"><?
									}
									?>
									</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br />
			<?
		}
		
		function help_addHelpInfo($data) {
			$screen_id=trim($data['Screen_ID']);
			$screen_desc=trim($data['Screen_Desc']);
			$user_id=strtoupper(trim($data['User_ID']));
			$site_name=strtoupper(trim($data['Site_Name']));
			$help_type=$data['Help_type_sel'];
			$help_text=addslashes(htmlspecialchars($data['HelpText']));
			$desc_text=addslashes(htmlspecialchars($data['DescText']));
			preg_match_all('/\[code\](.*?)\[\/code\]/is',$help_text,$res);
			
			foreach ($res[1] as $r_k=>$r_v) {
				$res[1][$r_k]="[code]".htmlspecialchars_decode($r_v)."[/code]";
			}

			$help_text=str_replace($res[0],$res[1],$help_text);
			
			$h1=trim($data['Heading1']);
			$h2=trim($data['Heading2']);
			$h3=trim($data['Heading3']);
			$h4=trim($data['Heading4']);
			
			if(!empty($_SESSION['help_db'])) {
				$data['dbName']=$_SESSION['help_db'];
			}
			$data['tableName']="dmahelp";
			$data['TrackGroup']=$help_type;
			$data['Key1']=$screen_id;
			$data['Key1_def']="ProductKey";
			$data['Key2']=$site_name;
			$data['Key2_def']="SiteName";
			$data['Key3']=$user_id;
			$data['Key3_def']="UserID";
			$data['Key4']="";
			$data['Key4_def']="NotUsed";
			$data['Key5']=$screen_desc;
			$data['Key5_def']="ScreenDesc";
			$data['Details_def']="START|version|Screen_ID|Screen_Desc|User_ID|Site_Name|Help_type_sel|HelpText|DescText|Heading1|Heading2|Heading3|Heading4|Activity YYYYMMDD|Activity HHMMSS|END";
			$data['Details_data']="START|v001|{$screen_id}|{$screen_desc}|{$user_id}|{$site_name}|{$help_type}|{$help_text}|{$desc_text}|{$h1}|{$h2}|{$h3}|{$h4}|".date("Ymd")."|".date("His")."|END";
			$extra=", Heading1='{$h1}', Heading2='{$h2}', Heading3='{$h3}', Heading4='{$h4}'";
			if($data['IsUpdate']==="Y") {
				$data['UniqueKey']=$data['LoadHelpUK'];
				$extra.=", TrackGroup='{$help_type}'";
				$data_add=SqlClient::updateTableRecord($data,$extra);
			} else {
				$data_add=SqlClient::insertTableRecord($data,$extra);
			}
			if($data_add<>false) {
				return true;
			} else {
				return false;
			}
		}
		
		function help_displayHelpLoad($data=false) {
			//Old info get
			$help_type_arr_blank['']="--------------";
			$help_types=PickUp::getStatusFilterData("HELP_MAINT_TYPES");
			if(is_array($help_types)) {
				foreach ($help_types as $ht_key=>$ht_val) {
					$ht_list.="'{$ht_key}',";
				}
				$ht_list=rtrim($ht_list,",");
			}
			// get list of helps to load.
			$use_db="";
			if(!empty($_SESSION['help_db'])) {
				$use_db="{$_SESSION['help_db']}.";
			}
			$list_avail_help_sql="select UniqueKey,TrackGroup,Key1,Key2,Key3,Key5 from {$use_db}dmahelp where TrackGroup in({$ht_list})";
			$list_avail_help_res=SqlClient::ExecuteQuery($list_avail_help_sql);
			$list_avail_help_cnt=SqlClient::GetRowCount($list_avail_help_res);
			if($list_avail_help_cnt>0) {
				while($line=SqlClient::FetchArray($list_avail_help_res)) {
					$user_text="";
					if(strtoupper($line['TrackGroup'])==="USERHELP") {
						$user_text=" [{$line['Key3']}] [{$line['Key2']}]";
					}
					if(strtoupper($line['TrackGroup'])==="SITEHELP") {
						$user_text=" [{$line['Key2']}]";
					}
					$help_load_arr[$line['TrackGroup']][$line['UniqueKey']]="{$line['Key1']} - {$line['Key5']}{$user_text}";
				}
				if(is_array($help_load_arr)) {
					$help_select="<select name=\"Help_type_load_sel\" style=\"buttons\">";
					$tg_key="";
					foreach ($help_load_arr as $hla_key=>$hla_val) {
							$help_select.="<optgroup label=\"{$hla_key}\" style=\"font-weight:bold;\">";
							$help_select.="</optgroup>";
							foreach ($hla_val as $hla_indiv_key=>$hla_indiv_val) {
								$seld="";
								if($data['Help_type_load_sel']===$hla_indiv_key) {
									$seld=" selected";
								}
								$help_select.="<option value=\"$hla_indiv_key\"{$seld}>&nbsp;&nbsp;&nbsp;{$hla_indiv_val}</option>";
							}
					}
					$help_select.="</select>";
				}
			} else {
				$help_select="There is no Help to load.";
			}
			?>
			<table border="0" width="740" id="normal-table">
				<tr>
			      <td><table width="740" border="0" cellspacing="0" cellpadding="2" id="body-text" bordercolor="black">
			          <tr>
			            <td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0" id="body-text-headers">
			              <tr>
			                <td><strong>&nbsp;Load/Delete Help Information</strong></td>
			              </tr>
			            </table><hr width="95%" size="1"></td>
			          </tr>
			          <tr>
			          	<td width="75%" align="center"><?=$help_select?></td>
			            <td align="center" height="40"><input type="submit" name="btnLoadHelp" value="Load" class="buttons"></td>
			            <td align="center" height="40"><input type="submit" name="btnCopyHelp" value="Copy" class="buttons"></td>
			            <td align="center" height="40"><input type="submit" name="btnDelHelp" value="Delete" class="buttons"></td>
			          </tr>
			      </table></td>
			    </tr>
			</table>
			<br />
			<?	
		}
	}  
?>
