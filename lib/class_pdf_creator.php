<?php

    class cls_pdf_creator
    {

        function cls_create_pdf_file($ps_pdf_filename,$ps_map_data_file,$ps_get_site_path)
        {
            //require_once('class_pdf.php');
            //$objPDF = new PDF();
                 require_once($_SESSION['ko_prog_path'].'lib/pdf/fpdf.php');
//                 require_once('lib/pdf/fpdf.php');
            $objFPDF = new FPDF();

            $first_prm_line='YES';

            $pdf_filename=$ps_pdf_filename;

            $page_startdown=0;
            $page_type='PORTRAIT';
            $pdf_page_type='P';
            $page_linespacing=0;
            $page_repfootmin=28;
            $page_cnordstartatcn=26;
            $do_footer='N';

            //echo 'ps_pdf_filename='.$ps_pdf_filename.'<br>';

            //echo 'Processing file ='.$ps_map_data_file.'<br>';
            //exit();
            $fileArray = $this->cls_load_file($ps_map_data_file);
            //echo 'fileArray='.'<br>';
            //print_r($fileArray);
            //exit();
            foreach ($fileArray as $key=>$map_lines)
            {
                //echo 'map_lines='.$map_lines.'<br>';
                $field_type=substr(trim($map_lines),0,4);
                $field_type=strtoupper($field_type);
                $field_type=trim($field_type);

                //echo 'field_type='.$field_type.'<br>';

                $map_lines=trim($map_lines);

                if ($map_lines=='*DOFOOTER')
                {
                    $do_footer='Y';
                }
                if ($map_lines=='*/DOFOOTER')
                {
                    $do_footer='N';
                }
                $map_line_array=explode('|',$map_lines);
                if ($field_type=='*TDX')
                {
                    // pkn 20100929 - comment line so get next record
                    goto B100_NEXT_REC;
                }
                elseif($field_type=='*CMD')
                {
                    goto A100_DO_COMMAND;
                }
                elseif($field_type=='*PRM')
                {
                    goto A200_DO_PROMPT;
                }
                elseif($field_type=='*VAL')
                {
                    goto A300_DO_VALUE;
                }
                elseif($field_type=='*PIC')
                {
                    goto A400_DO_PICTURE;
                }
                elseif($field_type=='*BOX')
                {
                    goto A500_DO_BOX;
                }
                else
                {
                    // pkn 20100929 - other line type so get next record
                    goto B100_NEXT_REC;
                }

A100_DO_COMMAND:

                    if (trim(strtoupper($map_line_array[1]))=='PORTRAIT')
                    {
                        $page_type='PORTRAIT';
                        $pdf_page_type='P';

                        $objPDF = new FPDF($pdf_page_type,'cm','A4');
                        $objPDF->SetCompression(true);
                        $objPDF->SetMargins(0,0,0);
                        $objPDF->SetAutoPageBreak(true,0.1);
                        $objPDF->AddPage();
                    }
                    if (trim(strtoupper($map_line_array[1]))=='NEWPAGE')
                    {
                        $objPDF->AddPage();
                    }
                    if (trim(strtoupper($map_line_array[1]))=='LANDSCAPE')
                    {
                        $page_type='LANDSCAPE';
                        $pdf_page_type='L';

                        $objPDF = new PDF($pdf_page_type,'cm','A4');
                        $objPDF->SetCompression(true);
                        $objPDF->SetMargins(0,0,0);
                        $objPDF->AddPage();
                    }

                    if (trim(strtoupper($map_line_array[1]))=='STARTDOWN')
                    {
                        $page_startdown=trim(substr($map_line_array[2],0,5));
                    }
                    if (trim(strtoupper($map_line_array[1]))=='LINESPACING')
                    {
                        $page_linespacing=trim($map_line_array[2]);
                    }
                    if (trim(strtoupper($map_line_array[1]))=='REPFOOTMIN')
                    {
                        $page_repfootmin=trim($map_line_array[2]);
                    }
                    if (trim(strtoupper($map_line_array[1]))=='CNORDSTARTATCN')
                    {
                        $page_cnordstartatcn=trim($map_line_array[2]);
                    }

                    goto B100_NEXT_REC;

A200_DO_PROMPT:
                    if (strpos($map_lines,"|") === false)
                    {
                        echo 'ERROR - no pipe fields in map_line='.$map_lines.'<br>';
                        goto B100_NEXT_REC;
                    }

                    $prompt_across_position=trim($map_line_array[1]);
                    $prompt_down_position=trim($map_line_array[2]);
                    $prompt_font=trim($map_line_array[3]);
                    $prompt_font_size=trim($map_line_array[4]);
                    $prompt_value=trim($map_line_array[5]);
                    $prompt_transparent=trim($map_line_array[6]);
                    // pkn 20100929 - whether bold/underline/normal
                    $prompt_font_type=trim($map_line_array[7]);
                    $prompt_alignment="LEFT";
                    if (count($map_line_array) > 9)
                    {
                        $prompt_alignment=trim($map_line_array[8]);
                    }
                    $prompt_alignment=strtoupper($prompt_alignment);

                    $font_type='';
                    if ($prompt_font_type=='BOLD')
                    {
                        $font_type='B';
                    }
                    if ($prompt_font_type=='UNDERLINE')
                    {
                        $font_type='U';
                    }
                    if ($prompt_font_type=='NORMAL')
                    {
                        $font_type='';
                    }

                    if ($do_footer=='Y')
                    {
                        $prompt_down_position=$page_repfootmin+$prompt_down_position;
                    }

                	if(strtoupper($prompt_font)=='C39' or strtoupper($prompt_font)=='C128A' or strtoupper($prompt_font)=='C128B' or strtoupper($prompt_font)=='C128C')
                    {
//                        ECHO "<BR> pdf_creation gwdebug start of image doing set type  eof<br>";
                    	//$imgPath="{$_SESSION['dma_prog_url']}lib/pdf/barcode/image.php?code=".$prompt_value."&style=66&type=".$prompt_font."&width=100&height=40&xres=1&font=Arial";
                        //$imgPath="{$_SESSION['ko_map_path']}lib/pdf/barcode/image.php?code=".$prompt_value."&style=66&type=".$prompt_font."&width=100&height=40&xres=1&font=Arial";

                        $ar_prompt_font_size=explode('^',$prompt_font_size);
                        $s_barcode_width=trim($ar_prompt_font_size[1]);
                        $s_barcode_height=trim($ar_prompt_font_size[2]);

                        $imgPath="{$_SESSION['dma_prog_url']}lib/pdf/barcode/image.php?code={$prompt_value}&style=66&type={$prompt_font}&width={$s_barcode_width}&height={$s_barcode_height}&xres=1&font=Arial";

                        //echo 'imgPath='.$imgPath.'<br>';
                        //exit();

	                    $objPDF->Image(rtrim($imgPath),$prompt_across_position,$prompt_down_position,0,0,"PNG");
                    }
                    else
                    {
                    	//$objPDF->SetFont($prompt_font,$font_type,$prompt_font_size);
                        //$prompt_font='ARIAL';
                        $objPDF->SetFont($prompt_font,$font_type,$prompt_font_size);
                	}
                    $objPDF->SetXY($prompt_across_position,$prompt_down_position);

                    $prompt_position=0;
                    $alignment_type='L';
                    if ($prompt_alignment=='JUSTLEFT')
                    {
                        $alignment_type='L';
                    }
                    if ($prompt_alignment=='JUSTRIGHT')
                    {
                        $alignment_type='R';
                    }
                    if ($prompt_alignment=='CENTRE')
                    {
                        $alignment_type='C';
                    }
                    $objPDF->Cell(0,0,$prompt_value,0,$prompt_position,$alignment_type,false,'');

                    goto B100_NEXT_REC;

A300_DO_VALUE:
                    $value_across_position=trim($map_line_array[1]);
                    $value_across_position=$this->cls_pdf_calc_position($value_across_position);

                    $value_down_position=trim($map_line_array[2]);
                    $value_down_position=$this->cls_pdf_calc_position($value_down_position);

                    $value_font=trim($map_line_array[3]);
                    $value_font_size=trim($map_line_array[4]);
                    $value_font_size2=trim($map_line_array[5]);
                    $value_field_source=trim($map_line_array[6]);
                    $value_field_source=strtoupper($value_field_source);

                    $value_value=trim($map_line_array[7]);
                    $value_transparent=trim($map_line_array[8]);
                    // pkn 20100929 - whether bold/underline/normal
                    $value_font_type=trim($map_line_array[9]);
                    $value_alignment=trim($map_line_array[10]);
                    $value_alignment=strtoupper($value_alignment);
                    $value_field_type=trim($map_line_array[11]);

                    $font_type='';
                    if ($value_font_type=='BOLD')
                    {
                        $font_type='B';
                    }
                    if ($value_font_type=='UNDERLINE')
                    {
                        $font_type='U';
                    }
                    if ($value_font_type=='NORMAL')
                    {
                        $font_type='';
                    }

                    if(strtoupper($value_font)=='C39' or strtoupper($value_font)=='C128A' or strtoupper($value_font)=='C128B' or strtoupper($value_font)=='C128C')
                    {
//                        ECHO "<BR> pdf_creation gwdebug start of image doing set type  eof<br>";
                        //$imgPath="{$_SESSION['site_uri']}lib/pdf/barcode/image.php?code=".$prompt_value."&style=66&type=".$prompt_font."&width=100&height=40&xres=1&font=Arial";
                        //$imgPath="{$_SESSION['ko_map_path']}lib/pdf/barcode/image.php?code=".$value_value."&style=66&type=".$value_font."&width=100&height=40&xres=1&font=Arial";

                        //4^520^400^Y^3

                        $ar_value_font_size=explode('^',$value_font_size);
                        $s_barcode_width=trim($ar_value_font_size[1]);
                        $s_barcode_height=trim($ar_value_font_size[2]);

                        $imgPath="{$_SESSION['dma_prog_url']}lib/pdf/barcode/image.php?code={$value_value}&style=66&type={$value_font}&width={$s_barcode_width}&height={$s_barcode_height}&xres=1&font=Arial";
                        //echo 'imgPath='.$imgPath.'<br>';
                        //exit();

                        $objPDF->Image(rtrim($imgPath),$value_across_position,$value_down_position,0,0,"PNG");
                    }
                    else
                    {
                        $objPDF->SetFont($value_font,$font_type,$value_font_size);
                    }




                    if ($value_field_source=='CNOTE')
                    {
                        //$value_down_position=$page_startdown;
                        //$page_linespacing
                    }
                    if ($value_field_source=='CNORD')
                    {
                        $value_down_position=$page_startdown;
                        //$page_linespacing
                    }
                    if ($do_footer=='Y')
                    {
                        $value_down_position=$page_repfootmin+$value_down_position;
                    }

                    $objPDF->SetXY($value_across_position,$value_down_position);

                    $value_position=0;
                    $alignment_type='L';
                    if ($value_alignment=='JUSTLEFT')
                    {
                        $alignment_type='L';
                    }
                    if ($value_alignment=='JUSTRIGHT')
                    {
                        $alignment_type='R';
                    }
                    if ($value_alignment=='CENTRE')
                    {
                        $alignment_type='C';
                    }
                    //$objPDF->Cell($value_across_position,$value_down_position,$value_value,0,$value_position,$value_alignment,false,'');
                    $objPDF->Cell(0,0,$value_value,0,$value_position,$alignment_type,false,'');

                    goto B100_NEXT_REC;

                A400_DO_PICTURE:

                    $image_across_position=trim($map_line_array[1]);
                    $image_down_position=trim($map_line_array[2]);
                    $image_width=trim($map_line_array[3]);
                    $image_height=trim($map_line_array[4]);
                    $image_filename=trim($map_line_array[5]);

                    $objPDF->Image($image_filename,$image_across_position,$image_down_position,$image_width,$image_height,'JPG');

                    goto B100_NEXT_REC;

                A500_DO_BOX:

                    $box_position_x=trim($map_line_array[1]);
                    $box_position_y=trim($map_line_array[2]);
                    $box_width=trim($map_line_array[3]);
                    $box_height=trim($map_line_array[4]);

                    // $box_position_x=across,$box_position_y=down,$box_width,$box_height
                    $objPDF->Rect($box_position_x,$box_position_y,$box_width,$box_height);

                    goto B100_NEXT_REC;

                B100_NEXT_REC:
            }
            $objPDF->Output($pdf_filename);
        }

        function cls_load_file($filePath)
        {
            $handle = @fopen($filePath, "r");
            if (!$handle)
            {
                return false;
            }
            while (!feof($handle))
            {
                $line = fgets($handle, 4096);
                if(!empty($line))
                {
                    $lines[]=$line;
                }
            }
            fclose($handle);
            return $lines;
        }

        function cls_pdf_testing($ps_pdf_filename,$ps_map_data_file)
        {
            //require_once('class_pdf.php');
            //$objPDF = new PDF();
            require_once('lib/pdf/fpdf.php');
            $objFPDF = new FPDF();

            //$pdf_filename=date("Ymd").date("His").".pdf";
            $pdf_filename=$ps_pdf_filename;

            $objPDF = new PDF('P','cm','A4');
            $objPDF->SetCompression(true);
            $objPDF->SetMargins(0,0,0);
            $objPDF->SetAutoPageBreak(true,0.1);
            $objPDF->AddPage();

            // pkn 20100907 - pdf cell paramaters
            //Cell(float w , float h , string txt , mixed border , int ln , string align , int fill , mixed link)
            // w = Cell width. If 0, the cell extends up to the right margin.
            // h = Cell height. Default value: 0.
            // txt = String to print. Default value: empty string.
            // border = 0: no border, 1: frame, L: left, T: top, R: right, B: bottom, Default Value: 0.
            // ln = Indicates where the current position should go after the call. Possible values are:
            //      0: to the right, 1: to the beginning of the next line, 2: below
            // align = L or empty string: left align (default value), C: center, R: right align
            // fill = Indicates if the cell background must be painted (true) or transparent (false). Default value: false.
            // link = URL or identifier returned by AddLink().

            //$objPDF->SetFont('Arial','',8);
            //$objPDF->SetXY(1,29.3);
            //$objPDF->Cell('0','0','Hello PKN what are you doing here in this pdf document.......',0,0,'L',false,'');
            //$objPDF->SetXY(3,1);
            //$objPDF->Cell('0.2','0.5','Hello PKN doh',0,2,'L',false,'');
            //$objPDF->SetFont('Arial','B',16);
            //$objPDF->Cell('0.1','0.5','A PDF File',0,2,'L',false,'');
            //$objPDF->SetFont('Arial','U',12);
            //$objPDF->Cell('0.1','0.5','A PDF File2',0,2,'L',false,'');

            //$objPDF->Output($pdf_filename);
            //exit();

            // pkn 20100907 - pdf cell paramaters
            //Cell(float w , float h , string txt , mixed border , int ln , string align , int fill , mixed link)
            // w = Cell width. If 0, the cell extends up to the right margin.
            // h = Cell height. Default value: 0.
            // txt = String to print. Default value: empty string.
            // border = 0: no border, 1: frame, L: left, T: top, R: right, B: bottom, Default Value: 0.
            // ln = Indicates where the current position should go after the call. Possible values are:
            //      0: to the right, 1: to the beginning of the next line, 2: below
            // align = L or empty string: left align (default value), C: center, R: right align
            // fill = Indicates if the cell background must be painted (true) or transparent (false). Default value: false.
            // link = URL or identifier returned by AddLink().

            $objPDF->SetFont('Arial','',10);
            $objPDF->Cell('0.2','0.5','Hello PKN',0,2,'L',false,'');
            $objPDF->SetFont('Arial','B',16);
            $objPDF->Cell('0.1','0.5','A PDF File',0,2,'L',false,'');
            $objPDF->SetFont('Arial','U',12);
            $objPDF->Cell('0.1','0.5','A PDF File2',0,2,'L',false,'');

            // pkn 20100907 - pdf image parameters
            //image($file,$x,$y,$w=0,$h=0,$type='',$link='')
            // file = Path or URL of the image.
            // x = Abscissa of the upper-left corner. If not specified or equal to null, the current abscissa is used.
            // y = Ordinate of the upper-left corner. If not specified or equal to null, the current ordinate is used; moreover, a page break is triggered first if necessary (in case automatic page breaking is enabled) and, after the call, the current ordinate is moved to the bottom of the image.
            // w = Width of the image in the page. If not specified or equal to zero, it is automatically calculated.
            // h = Height of the image in the page. If not specified or equal to zero, it is automatically calculated.
            // type = Image format. Possible values are (case insensitive): JPG, JPEG, PNG and GIF. If not specified, the type is inferred from the file extension.
            // link = URL or identifier returned by AddLink()

            //$objPDF->Image(rtrim($imgPath),$splitImg[1],$splitImg[2],0,0,"JPG");
            //$objPDF->Image('c:\vellex image.jpg',6,'0.1');
            //$objPDF->Image('c:\Vellex Logo.jpg',6,'0.1',4,2,'JPG');

            // pkn 20100907 - pdf line parameters
            // Line(x1, y1, x2, y2)
            // x1 = Abscissa of first point.
            // y1 = Ordinate of first point.
            // x2 = Abscissa of second point.
            // y2 = Ordinate of second point.

            $objPDF->Line(10,10,11,10);

            // pkn 20100907 - pdf rectangle parameters
            // Rect(x, y, w, h, style)
            // x = Abscissa of upper-left corner.
            // y = Ordinate of upper-left corner.
            // w = Width.
            // h = Height.

            $objPDF->Rect(6,6,3,3);

            $objPDF->AddPage();

            $objPDF->Cell('0.1','0.5','A PDF File Page2',0,2,'L',false,'');
            $objPDF->Cell('0.0','0.5','A PDF File Page2-2',0,2,'L',false,'');

            $objPDF->Output($pdf_filename);
        }

function cls_pdf_calc_position($ps_position)
{
    $s_position = $ps_position;
    $s_position = str_replace("calc_pos:","",strtolower($s_position));
    $s_calc_postion = "0";
A100_CHECK_IF_CALC:
    if (strpos($s_position,"^") ===false)
    {
        GOTO Z900_EXIT;
    }
B100_DO_CALC:
    $ar_calc_fields = explode("^",$s_position);
    $s_calc_fields_count = count($ar_calc_fields);

    $s_value1 = $ar_calc_fields[0];
    $s_operator =  $ar_calc_fields[1];
    $s_value2 =  $ar_calc_fields[2];

//    echo "<br>s_value1 = ".$s_value1." s_operator =  ".$s_operator."  s_value2 =  ".$s_value2."  s_position=".$s_position;
B110_MULTIPLY:
    IF ($s_operator <> "*")
    {
        goto B119_END;
    }
    $s_calc_postion = $s_value1 * $s_value2;
B119_END:
B120_ADD:
    IF ($s_operator <> "+")
    {
        goto B129_END;
    }
    $s_calc_postion = $s_value1 + $s_value2;
B129_END:
B130_SUBTRACT:
    IF ($s_operator <> "-")
    {
        goto B139_END;
    }
    $s_calc_postion = $s_value1 - $s_value2;
B139_END:
B140_DIVIDE:
    IF ($s_operator <> "/")
    {
        goto B149_END;
    }
    $s_calc_postion = $s_value1 / $s_value2;
B149_END:

B900_END:
    $s_position = $s_calc_postion;
    if (count($ar_calc_fields) < 4)
    {
        GOTO Z800_EXIT;
    }

    $s_position = $s_position."^".$ar_calc_fields[3];
    $s_position = $s_position."^".$ar_calc_fields[4];
    GOTO A100_CHECK_IF_CALC;
Z800_EXIT:
    $s_position = $s_calc_postion;
    //echo "Position calculated original value = ".$ps_position." calculated value  = ".$s_position."<br>";
Z900_EXIT:
        return $s_position;
}


    }

?>

