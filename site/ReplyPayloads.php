<?php

require_once('DBFunctions.php');

function reply_add_device_details()
{
	global $DATAVERSION_STRING;
	global $deviceId;
	
	$result = exec_query("SELECT * FROM Devices INNER JOIN Companies ON Devices.CompanyId=Companies.CompanyId WHERE DeviceId=$deviceId");
	if ($deviceInfo = mysql_fetch_array($result, MYSQL_ASSOC))
	{
		$deviceDetailArgs = array(
								'Status'=>'LOGIN_SUCCESS',
								'DeviceName'=>mysql_real_escape_string($deviceInfo['DeviceName']),
								'CompanyName'=>mysql_real_escape_string($deviceInfo['CompanyName']),
								);
		
		reply_add_section("DeviceDetails", $deviceDetailArgs);
		
	}
	else
	{
		die("unknown error: 0x80008321 no device?");
	}
}
	
	
function reply_add_import_section()
{
	global $DATAVERSION_STRING;
	global $deviceId;
	
	$sql = "SELECT * FROM Deliveries WHERE AssignedToDeviceId=$deviceId AND (DeliveryStatus='scheduled' OR DeliveryStatus='senttodriver' OR DeliveryStatus='withdriver_cancelpending') AND ScheduledForDate < ".time()."";
	$result = exec_query($sql);


	if ($result && mysql_num_rows($result) > 0)
	{
		
		// TODO: lookup database
		
		echo "	<Import>";
		echo "		<RecordCount>".mysql_num_rows($result)."</RecordCount>";
		echo "		<PSV><![CDATA[";
		
		// PSV file header
		$time = time();
		
		echo "*tdx File id|version|source|source version|Timestamp|UDID|username|password|user_email|EOL\n";
		echo "Ud.pr.imp|$DATAVERSION_STRING|ud.pr.web|V1.01|".$time."|||||EOL\n";
		
		// PSV file header
		echo "*tdx GlobalDeliveryId|Version|ServerStatus|CNNO|Receiver's Name|PhoneNo|Address line 1|Address line 2|Suburb|Postcode|Note|Items|Weight|Delivered|DeliveryDate|DeliveryTime|ReceiversName|ReceivedByClient|Latitude|Longitude|LocationAccuracy|ImageNames|Timezone|EOL\n";
		
		// Loop all data and output in pipe-delimetered format
		while ($deliveryRecord = mysql_fetch_array($result, MYSQL_ASSOC))
		{
			$entryServerStatus = 0;
			if ($deliveryRecord['DeliveryStatus']=='withdriver_cancelpending')
			{
				$entryServerStatus = 3;
			}
			echo "{$deliveryRecord['GlobalDeliveryId']}|{$deliveryRecord['Version']}|$entryServerStatus|{$deliveryRecord['CNNO']}|{$deliveryRecord['Receiver']}|{$deliveryRecord['PhoneNo']}|{$deliveryRecord['AddrLine1']}|{$deliveryRecord['AddrLine2']}|{$deliveryRecord['Suburb']}|{$deliveryRecord['Postcode']}|{$deliveryRecord['Note']}|{$deliveryRecord['Items']}|{$deliveryRecord['Weight']}|0|0|0|||0.0|0.0|-1|||EOL\n";
		}
		
		// complete sample
		//M12358|Jana Citizen|(555) 123 456|Level 10|12 Creek Rd|Carina|4152|Test|1|3.0|1|2009-06-06|11:11:11|Will|TestClient|31.233944|121.467813|47||-500|EOL
		
		echo "]]></PSV>";
		echo "	</Import>";
	}
	
		// sents record as 'senttodriver'
	$sql = "UPDATE Deliveries Set DeliveryStatus='senttodriver' WHERE AssignedToDeviceId=$deviceId AND DeliveryStatus='scheduled' AND ScheduledForDate < ".time()."";
	exec_query($sql);
}

