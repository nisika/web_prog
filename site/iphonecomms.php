<?php

/* Error status code strings
 * UNKNOWN_USER = email address not registered
 * INCORRECT_PASSWORD = password does not match email
 */

$DATAVERSION_STRING = "0.91";
$NEWPASSWORD_CHARS = 6;

$version = $_POST["version"];  // version number, set by App
$UDID = $_POST["UDID"];

// invalid access of script - e.g. URL typed into a browser (just an early out, more auth later)
// all comms from the iPhone uses POST, not GET.
if (!isset($_POST["UDID"]))
{
	echo "Unknown Error 0x80008001";
	exit;
}

// exits if the client is older than the absolute oldest version supported.  More gracefull support for old, but still supported versions later.
if ($version < 0.9)
{

	die("uDelivered ".$version." is out of date, please use iTunes to update.");
}

	
	

/*
 * Includes
 */
require_once('DBFunctions.php');
require_once('ServerReplyFunctions.php');
require_once('ReplyPayloads.php');

/*
 * Config
 */
// ref: http://www.php.net/manual/en/function.mysql-real-escape-string.php#93005
if(get_magic_quotes_gpc())
{
    $_GET = stripslashes_deep($_GET);
	
	$_POST = stripslashes_deep($_POST);
}

// dumps all input variables into a variable
ob_start();
echo "$_SERVER=";
var_dump($_SERVER);
$httpArgs = ob_get_contents();
ob_clean();
echo "$_POST=";
//var_dump($_POST); // need to reduce the size of this
echo "$_GET=";
var_dump($_GET);
echo "$_COOKIES=";
var_dump($_COOKIES);
$requestArgs = ob_get_contents();
ob_end_clean();


$transactionBLOBData = mysql_real_escape_string(base64_decode($_POST['ProofModuleAppleTransactionReceipt']));
$restoredTransactionBLOBData = mysql_real_escape_string(base64_decode($_POST['ProofModuleAppleRestoredTransactionReceipt']));

	
// Logs request, regardless of the status of the iPhone.  Useful for companies wanting to know where their drivers are, and recover lost iPhones
$sql = "INSERT INTO RequestLog (UDID, RequestTime, RequestType, HTTPArgs, RequestArgs, OSVersion, uDeliveredVersion, UserDeviceName, LocationLat, LocationLong, LocationAccuracy, ProofModuleAppleTransactionId, ProofModuleAppleTransactionDate, ProofModuleAppleTransactionReceipt, ProofModuleAppleRestoredTransactionId, ProofModuleAppleRestoredTransactionDate, ProofModuleAppleRestoredTransactionReceipt) ";
$sql .= "VALUES ('".mysql_real_escape_string($_POST["UDID"])."', ".time().", '".mysql_real_escape_string($_POST['requestType'])."', '".mysql_real_escape_string($httpArgs)."', '".mysql_real_escape_string($requestArgs)."', '".mysql_real_escape_string($_POST['OSVersion'])."', '".mysql_real_escape_string($_POST['version'])."', '".mysql_real_escape_string($_POST['UserDeviceName'])."', ".doubleval($_POST['Latitude']).", ".doubleval($_POST['Longitude']).", ".intval($_POST['LocationAccuracy']).", '".mysql_real_escape_string($_POST['ProofModuleAppleTransactionId'])."', ".intval($_POST['ProofModuleAppleTransactionDate']).", '".$transactionBLOBData."', '".mysql_real_escape_string($_POST['ProofModuleAppleRestoredTransactionId'])."', ".intval($_POST['ProofModuleAppleRestoredTransactionDate']).", '".$restoredTransactionBLOBData."')"; 
exec_query($sql);
	

// check that the iPhone is not lost (if it is - report)
$result = exec_query("SELECT * FROM Devices WHERE UDID='".mysql_real_escape_string($UDID)."' AND Status = 'lost'");
if ($row = mysql_fetch_array($result, MYSQL_ASSOC))
{
	error_reply_and_exit("This iPhone has been reported lost. Please contact uDelivered (email support@udelivered.com or \ncall +61 (0) 422 199 403) to report the lost iPhone (reward given)");
}		
	

$deviceId = -1;
$companyId = -1;


$user_email = $_POST["user_email"];  // set by user
$password = $_POST["password"];  // set by user
$isLITE = $_POST["isLITE"];  // 1 if LITE, not present if not.  Set by App

$requestType = $_POST['requestType'];


// if request type is SIGNUP - processes it
if ($requestType == "SIGNUP")
{
	// transaction for SIGNUP operations
	exec_query("START TRANSACTION");
	
	$signupEmail = $_POST["SignupEmail"];
	$signupPassword = $_POST["SignupPassword"];

	// checks for duplicate
	$result = exec_query("SELECT UserId FROM Users WHERE Email='".mysql_real_escape_string($signupEmail)."'");
	if ($row = mysql_fetch_array($result, MYSQL_ASSOC))
	{
		error_reply_and_exit("The email address '".mysql_real_escape_string($signupEmail)."' is already registered.  Please use the 'log in' option instead.");
	}
	
	
	// if user doesn't exist, adds user
	exec_query("INSERT INTO Companies (CompanyName, DateAdded) VALUES ('No Name', ".time().")");
	$companyId = intval(mysql_insert_id());
	if ($companyId <= 0)
	{
		echo "Error 0x80001001, invalid companyId";
		exit;
	}
	
	// sets team UDAC code
	$teamUDAC = udacKey($companyId, 5000);
	exec_query("UPDATE Companies SET TeamUDAC='$teamUDAC' WHERE CompanyId=$companyId LIMIT 1");
	
	// if user doesn't exist, adds user
	$sql = "INSERT INTO Users (Email, Password, CompanyId, CompanyRole, DateAdded) VALUES ('".mysql_real_escape_string($signupEmail)."', '".md5(mysql_real_escape_string($signupPassword))."', $companyId, 'admin', ".time().")"; 
	exec_query($sql);
	$userId = intval(mysql_insert_id()); 

	// mark any existing device with this UDID as 'reassigned' (to a new account);
	$sql = "UPDATE Devices SET Status='reassigned' WHERE UDID='".mysql_real_escape_string($UDID)."'"; 
	exec_query($sql);
	
	// creates device from UserId
	$sql = "INSERT INTO Devices (CompanyId, UDID, UserId, DateAdded, HardwareType, DeviceName, Status) VALUES ($companyId, '".mysql_real_escape_string($UDID)."', $userId, ".time().", '".mysql_real_escape_string($_POST['HardwareVersion'])."', '".mysql_real_escape_string($_POST['UserDeviceName'])."', 'active')"; 
	exec_query($sql);
	$deviceId = intval(mysql_insert_id()); 
	
	// end transaction for SIGNUP operations
	exec_query("COMMIT");

	
	$loginReplyArgs = array(
		'Status'=>'SIGNUP_SUCCESS',
		'UserId'=>strval($userId),
		'Email'=>mysql_real_escape_string($signupEmail),
		'CompanyName'=>'No Name',
	);

	$headers = "From: uDelivered.proof <support@udelivered.com>\n";
	//$headers .= "X-Sender: uDelivered.proof <support@udelivered.com>\n";
	$headers .= "X-uDelivered-Version: $version";	
	
	$email_subject = "Welcome to uDelivered.proof";
	$email_message = "You can access the login interface here: \n\n";
	$email_message .= "Email Address: $signupEmail\n";
	$email_message .= "Password: $signupPassword";
	$email_to = $signupEmail;
	
	$ok = @mail($email_to, $email_subject, $email_message, $headers);
	
	// reply
	reply_start();
	reply_add_section("LoginReply", $loginReplyArgs);
	reply_end();
	exit;
}
// or if the request is a RECOVER_PASSWORD request, processes that
else if ($_POST['requestType'] == "RECOVER_PASSWORD")
{
	// gets UserId
	$result = exec_query("SELECT * FROM Users WHERE Email='".mysql_real_escape_string($user_email)."'");
	
	if ($row = mysql_fetch_array($result, MYSQL_ASSOC))
	{
		$userId = intval($row['UserId']);

		$newPassword = createRandomPassword($NEWPASSWORD_CHARS);

		$sql = "UPDATE Users SET TemporaryPassword='".md5($newPassword)."' WHERE UserId=".$userId." LIMIT 1";
		exec_query($sql);

		
		$headers = "From: uDelivered.proof <support@udelivered.com>\n";
		//$headers .= "X-Sender: uDelivered.proof <support@udelivered.com>\n";
		$headers .= "X-uDelivered-Version: $version";	
		
		$email_subject = "uDelivered.proof password recovery";
		$email_message = "Your new uDelivered password is: $newPassword";
		$email_to = $row['Email'];
		
		$ok = @mail($email_to, $email_subject, $email_message, $headers);
		
		// TODO: remove the new password from this message!!
		reply_start("OK", "A new password has been emailed to '".mysql_real_escape_string($user_email)."'");
		reply_end();
		exit;
	}
	else
	{
		error_reply_and_exit("Unknown email address '$user_email', please sign up.", "UNKNOWN_USER");
	}
}
else if ($_POST['requestType'] == "JOINTEAM")
{
	// transaction for JOINTEAM operations
	exec_query("START TRANSACTION");
	
	$signupTeamcode = $_POST["SignupTeamCode"];
	
	// checks for duplicate
	$result = exec_query("SELECT CompanyId, CompanyName FROM Companies WHERE TeamUDAC='".mysql_real_escape_string($signupTeamcode)."' AND TeamUDAC != ''");
	if ($companyRecord = mysql_fetch_array($result, MYSQL_ASSOC))
	{
		$companyId = $companyRecord['CompanyId'];
	}
	else
	{
		error_reply_and_exit("The given team code was incorrect.  Please confirm the value with your tech support staff and try again.");	
	}
	
	// if device exists, just updates the Nickname and creates a new key
	$result = exec_query("SELECT * FROM Devices WHERE CompanyId=$companyId AND UDID='".mysql_real_escape_string($UDID)."' AND DeviceKey IS NOT NULL AND (Status = 'active' OR Status = 'pending_approval')");
	if ($deviceInfo = mysql_fetch_array($result, MYSQL_ASSOC))
	{
		$deviceId = $deviceInfo['DeviceId'];
		if ($deviceId <= 0)
		{
			die("Error 0x80001007: invalid $deviceId: ".$deviceId);
		}
		
		exec_query("UPDATE Devices SET DeviceName='".mysql_real_escape_string($_POST['SignupNickname'])."',Status='pending_approval' WHERE DeviceId=$deviceId LIMIT 1");
	}
	else
	{
		// mark any existing device with this UDID as 'reassigned' (to a new account);
		$sql = "UPDATE Devices SET Status='reassigned' WHERE UDID='".mysql_real_escape_string($UDID)."'"; 
		exec_query($sql);
		
		// creates device from UserId
		$sql = "INSERT INTO Devices (CompanyId, UDID, DateAdded, HardwareType, DeviceName, Status) VALUES ($companyId, '".mysql_real_escape_string($UDID)."', ".time().", '".mysql_real_escape_string($_POST['HardwareVersion'])."', '".mysql_real_escape_string($_POST['SignupNickname'])."', 'pending_approval')"; 
		exec_query($sql);
		$deviceId = mysql_insert_id();
	}	
	
	// sets device key
	$deviceKey = udacKey($deviceId, 1000000000);
	exec_query("UPDATE Devices SET DeviceKey='$deviceKey' WHERE DeviceId=$deviceId LIMIT 1");
	
	
	// end transaction for JOINTEAM operations
	exec_query("COMMIT");

	
	$loginReplyArgs = array(
							'Status'=>'SIGNUP_SUCCESS',
							'DeviceKey'=>$deviceKey,
							'CompanyName'=>$companyRecord['CompanyName'],
							);
	
	// reply
	reply_start();
	reply_add_section("LoginReply", $loginReplyArgs);
	reply_end();
	exit;
}
// otherwise, validates user
else
{
	
	// gets the device info, exits if not active
	$result = exec_query("SELECT * FROM Devices WHERE UDID='".mysql_real_escape_string($UDID)."' AND Status != 'reassigned'");
	if ($deviceInfo = mysql_fetch_array($result, MYSQL_ASSOC))
	{
		
		// deactive iPhone
		if ($deviceInfo['Status'] == "deactive")
		{
			error_reply_and_exit("This iPhone has has been deactivated.  To re-activate, contact your company technical support staff.");
		}
		// iPhone not yet approved
		else if ($deviceInfo['Status'] == "pending_approval")
		{
			error_reply_and_exit("This iPhone is registered but awaiting admin approval. Please contact your company technical support staff for more information.");
		}
		// else, it's either active - or some unknown status (the lost status should have already been delt with)
		else if ($deviceInfo['Status'] != "active")
		{
			error_reply_and_exit("This iPhone has an unknown status. Please contact uDelivered support@udelivered.com for information");
		}
	}
	else
	{
		// no iphone with this UDID in the system.  This should never happen!  But presents a nice error message anyway
		error_reply_and_exit("This iPhone is not recognised.  Please Sign-up for an account, or join a team.");
	}
	
	
	// if this device is linked to a User - validates it with the email/password
	if (isset($deviceInfo['UserId']))
	{
		if (!isset($_POST["user_email"]))
		{
			die("Error: 0x8000600 UserId required, but none given from device");
		}
	
		// gets UserId
		$result = exec_query("SELECT * FROM Users WHERE Email='".mysql_real_escape_string($user_email)."'");
		
		if ($row = mysql_fetch_array($result, MYSQL_ASSOC))
		{
			// check password, if wrong, reply with message with a payload that opens the settings screen.
			$userId = intval($row['UserId']);
			
			if (isset($row['TemporaryPassword']) && md5($password) == $row['TemporaryPassword'])
			{
				// login with temporary password - sets as actual
				$sql = "UPDATE Users SET Password='".$row['TemporaryPassword']."', TemporaryPassword=NULL WHERE UserId=".$userId." LIMIT 1";
				exec_query($sql);	
			}
			else if (md5($password) != $row['Password'])
			{
				// TODO: should show password screen if not showing
				error_reply_and_exit("Incorrect password for '$user_email'.", "INCORRECT_PASSWORD");
			}
			else
			{
				// login with password - clears TemporaryPassword (if any)
				$sql = "UPDATE Users SET TemporaryPassword=NULL WHERE UserId=".$userId." LIMIT 1";
				exec_query($sql);	
			}
		}
		else
		{
			error_reply_and_exit("Unknown email address '$user_email', please sign up.", "UNKNOWN_USER");
		}
	}
	// else, this device is part of a team - validates that the (secret) device key matches the UDID
	else if (isset($deviceInfo['DeviceKey']) && $deviceInfo['DeviceKey'] != "")
	{
		if (!isset($_POST["DeviceKey"]))
		{
			die("Error: 0x8000600 DeviceKey required, but none given by device");
		}
		
		// exits if incorrect device key
		if ($deviceInfo['DeviceKey'] != $_POST["DeviceKey"])
		{
			 error_reply_and_exit("Error authenticating team device.  Contact support@udelivered.com or re-join the team.");
		}
	}
	else
	{
		die("Error 0x80001051: unknown error");
	}


	// if the script has not exited due to authentication issues, user is authenticated.
	
	$deviceId = intval($deviceInfo['DeviceId']);
	$companyId = intval($deviceInfo['CompanyId']);

}


// USER SIGNUP/TEAM JOIN SAFTY



// sanity checks
if ($deviceId <= 0)
{
	die("Error 0x80001002: invalid $deviceId: ".$deviceId);
}
if ($companyId <= 0)
{
	die("Error 0x80001003: invalid $companyId: ".$companyId);
}

	


// ------------------------------------------------------------

/*
 * Process requests
 */


if ($requestType == "LOGIN_CHECK")
{
	$loginReplyArgs = array(
					 'Status'=>'LOGIN_SUCCESS',
					 'DeviceId'=>strval($deviceId),  // just some info, not currently used by the App
					 'CompanyId'=>strval($companyId),  // just some info, not currently used by the App
					 );

	reply_start();
	reply_add_section("LoginReply", $loginReplyArgs);
	reply_end();
	exit;
}
else if (($requestType == "INCREMENTAL") || ($requestType == "ENDOFDAY"))
{
	// transaction for INCREMENTAL/ENDOFDAY operations
	exec_query("START TRANSACTION");

	
	/*
	 * Data from iPhone processing
	 */
	
	// export values
	$requestType = $_POST['requestType'];  // either "INCREMENTAL" or "ENDOFDAY"
	$fileCount = $_POST['fileCount']; // number of files in the export, including the textual data & images
	
	$deliveryCount = count($_POST['Deliveries']);
	$deliveryIds = array();
	
	if ($deliveryCount <= 0)
	{
		die("no records in export!");
	}
	
	for ($i = 0; $i < $deliveryCount; $i++)
	{
		// TODO: validate delivery status?
	
		$globalDeliveryId = -1;
		$deliveryStatus = "";
		
		// if a global deliveryId was set, looks it up
		if (isset($_POST['Deliveries'][$i]['GlobalDeliveryId']))
		{
			$sql = "SELECT GlobalDeliveryId, DeliveryStatus FROM Deliveries WHERE AssignedToDeviceId=$deviceId AND GlobalDeliveryId=".intval($_POST['Deliveries'][$i]['GlobalDeliveryId'])."";
			$result = exec_query($sql);

			if ($deliveryRecord = mysql_fetch_array($result, MYSQL_ASSOC))
			{
					$globalDeliveryId = intval($deliveryRecord['GlobalDeliveryId']);
					$deliveryStatus = $deliveryRecord['DeliveryStatus'];
					if ($globalDeliveryId <= 0)
					{
						die("invalid global globalDeliveryId: $globalDeliveryId");
					}
			}
			else
			{
				// if GlobalDeliveryId was given, it should be correct!
				
				// die("Error 0x8008325: GlobalDeliveryId ({$_POST['Deliveries'][$i]['GlobalDeliveryId']}) does not appear to be assigned to this device ($deviceId)");
			 
				// ignores it - maybe it's a phantom entry no longer assigned to this deviceId
				// TODO:  Log error to server
				continue;
			}
		}
		// else, looks up based on App Id and CreationDate
		else
		{
			$sql = "SELECT GlobalDeliveryId, DeliveryStatus FROM Deliveries WHERE AssignedToDeviceId=$deviceId AND AppDeliveryId=".intval($_POST['Deliveries'][$i]['DeliveryId'])." AND AppCreationDate=".intval($_POST['Deliveries'][$i]['CreationDate'])."";
			$result = exec_query($sql);
		
			if ($deliveryRecord = mysql_fetch_array($result, MYSQL_ASSOC))
			{
					$globalDeliveryId = intval($deliveryRecord['GlobalDeliveryId']);
					$deliveryStatus = $deliveryRecord['DeliveryStatus'];
					if ($globalDeliveryId == 0)
					{
						die("invalid global globalDeliveryId: $globalDeliveryId");
					}
			}
			else
			{
				// no problem.. doesn't have to exist
			}
		}
		
		// ignores already uploaded entries
		if ($globalDeliveryId != -1 && $deliveryStatus == "delivered_endofday")
		{
			continue;
		}
		
		if (!isset($companyId))
		{
			die("Error 0x800321: companyId not set");
		}
		if ($companyId <= 0)
		{
			die("Error 0x800322: companyId not valid");
		}

		// common entry data	
		$queryValueArray = array(
			'CompanyId'=>$companyId,
			'CNNO'=>''.$_POST['Deliveries'][$i]['CNNO'],
			'Receiver'=>''.$_POST['Deliveries'][$i]['Receiver'],
			'PhoneNo'=>''.$_POST['Deliveries'][$i]['PhoneNo'],
			'AddrLine1'=>''.$_POST['Deliveries'][$i]['AddrLine1'],
			'AddrLine2'=>''.$_POST['Deliveries'][$i]['AddrLine2'],
			'Suburb'=>''.$_POST['Deliveries'][$i]['Suburb'],
			'Postcode'=>''.$_POST['Deliveries'][$i]['Postcode'],
			'Note'=>''.$_POST['Deliveries'][$i]['Note'],
			'Items'=>''.$_POST['Deliveries'][$i]['Items'],
			'Weight'=>''.$_POST['Deliveries'][$i]['Weight'],
			'Timezone'=>''.$_POST['Deliveries'][$i]['Timezone']
		);
		
		// data if the entry has been delivered
		if (intval($_POST['Deliveries'][$i]['Delivered']) == 1)
		{
			$appendValues = array(
				'DeliveryDate'=>intval($_POST['Deliveries'][$i]['DeliveryDate']),
				'ReceivedByName'=>''.$_POST['Deliveries'][$i]['ReceivedByName'],
				'ReceivedByClient'=>''.$_POST['Deliveries'][$i]['ReceivedByClient'],
				'Latitude'=>floatval($_POST['Deliveries'][$i]['Latitude']),
				'Longitude'=>floatval($_POST['Deliveries'][$i]['Longitude']),
				'LocationAccuracy'=>intval($_POST['Deliveries'][$i]['LocationAccuracy']),
			);
			
			$queryValueArray = array_merge($queryValueArray, $appendValues);
			
			if ($requestType == "INCREMENTAL")
			{
				$queryValueArray['DeliveryStatus'] = 'delivered';
			}
			else
			{
				$queryValueArray['DeliveryStatus'] = 'delivered_endofday';
			}
		}
		else
		{
			$queryValueArray['DeliveryStatus'] = 'withdriver';
		}
		
		// data for new entries
		if ($globalDeliveryId == -1)
		{
			$appendValues = array(
				'AppDeliveryId'=>''.$_POST['Deliveries'][$i]['DeliveryId'],   // only set when the record came from the iphone
				'AppCreationDate'=>''.$_POST['Deliveries'][$i]['CreationDate'],    // only set when the record came from the iphone
				'CreatedByDeviceId'=>$deviceId,
				'AssignedToDeviceId'=>$deviceId,
				'ScheduledForDate'=>time(),
				'DateAdded'=>time(),
				'Source'=>'iphone',
				'Version'=>time()
			);
			
			$queryValueArray = array_merge($queryValueArray, $appendValues);
		}
		
		
		if ($globalDeliveryId == -1)
		{
			$queryNames = "";
			$queryValues = "";
			foreach ($queryValueArray as $key => $value)
			{
				
				if ($queryNames != "")
				{
					$queryNames .= ', ';
				}
				$queryNames .= '`'.$key.'`';
				
				if ($queryValues != "")
				{
					$queryValues .= ', ';
				}
				if (is_string($value))
				{
					$queryValues .= '\''.mysql_real_escape_string($value).'\'';
				}
				else
				{
						$queryValues .= $value;	
				}
			}
			
			$sql = "INSERT INTO Deliveries ($queryNames) VALUES ($queryValues)"; 
			//echo ($sql);
			exec_query($sql);
			$globalDeliveryId = mysql_insert_id();	
			
			if ($globalDeliveryId <= 0)
			{
				die("invalid globalDeliveryId".$globalDeliveryId);
			}
	
		}
		else
		{
			$queryValues = "";
			foreach ($queryValueArray as $key => $value)
			{
				if ($queryValues != "")
				{
					$queryValues .= ", ";
				}
				$queryValues .= "$key = ";
				if (is_string($value))
				{
					$queryValues .= '\''.mysql_real_escape_string($value).'\'';
				}
				else
				{
					$queryValues .= $value;	
				}
			}
			$sql = "UPDATE Deliveries SET $queryValues WHERE GlobalDeliveryId=$globalDeliveryId LIMIT 1";
			//echo $sql;
			exec_query($sql);
		}
		
		// map of local delivery ids to global delivery ids
		$deliveryIds[$_POST['Deliveries'][$i]['DeliveryId']] = $globalDeliveryId;
		
	}
	
	// adds attachments
	$fileCount = $_POST['fileCount']; // number of files in the export, including the textual data & images
	for ($i = 0; $i < $fileCount; $i++)
	{
	
		$base64encoded = $_POST["base64encoded".$i];
		$data = $_POST["DATA".$i];	
		$fileatt_type = mysql_real_escape_string($_POST["contentType".$i]); // "application/xml"; // File Type
		$fileatt_name = safe_filename($_POST["filename".$i]); // "GPSLog.gpx"; // Filename that will be used for the file as the attachment
				
		$globalDeliveryId = -1;
		
		if (stripos($fileatt_name, "jpg") !== FALSE)
		{
			// looks up GlobalEntryId for image
			$localDeliveryId = substr($fileatt_name, 1, strpos($fileatt_name, "_") - 1);
			
			$globalDeliveryId = intval($deliveryIds[intval($localDeliveryId)]);
			if ($globalDeliveryId <= 0)
			{
				continue;
				/*
				//echo $fileatt_name;
				//echo ":::";
				echo $localDeliveryId;
				var_dump($deliveryIds);
				die("invalid globalEntryId: $globalEntryId for $fileatt_name");
				*/
			}
		}
	
		$writeMode = 'w';
		if ($base64encoded)
		{
			$data = base64_decode($data);
			$writeMode = 'wb';
		}
		
		$filePathAndName = "usrdata/".$companyId."/".$fileatt_name;
		//echo $filePathAndName ;
		
		@mkdir("usrdata/".$companyId."/");
		
		if (!$fh = fopen($filePathAndName, 'a')) {
			 die("Cannot open file ($filePathAndName)");
		}
	
		if (fwrite($fh, $data) === FALSE) {
			die("Cannot write to file ($filePathAndName)");
		}
		fclose($fh);
	
		$sql = "INSERT IGNORE INTO Attachments (GlobalDeliveryId, FileName, ContentType, FileSize) VALUES ($globalDeliveryId, '$fileatt_name', '$fileatt_type', ".filesize($filePathAndName).")"; 
		exec_query($sql);
		$attachmentId = intval(mysql_insert_id()); 
		
		unset($data);
		unset($fileatt_name);
		unset($fileatt_type);
	}
	
	// end transaction for INCREMENTAL/ENDOFDAY operations
	exec_query("COMMIT");

	reply_start(); //"OK", "Data sent");

}
else if ($requestType == "TOIPHONEACK")
{
	// transaction for TOIPHONEACK operations
	exec_query("START TRANSACTION");
	
	$deliveryCount = count($_POST['Deliveries']);
	$deliveryIds = array();
	
	if ($deliveryCount <= 0)
	{
		die("no records in export!");
	}
	
	for ($i = 0; $i < $deliveryCount; $i++)
	{
		// TODO: validate delivery status?
	
		$globalDeliveryId = -1;
		$deliveryStatus = "";
		
		// if a global deliveryId was set, looks it up
		if (isset($_POST['Deliveries'][$i]['GlobalDeliveryId']))
		{
			$sql = "SELECT GlobalDeliveryId, DeliveryStatus FROM Deliveries WHERE AssignedToDeviceId=$deviceId AND GlobalDeliveryId=".intval($_POST['Deliveries'][$i]['GlobalDeliveryId'])."";
			$result = exec_query($sql);

			if ($deliveryRecord = mysql_fetch_array($result, MYSQL_ASSOC))
			{
				$newStatus = NULL;
				if ($deliveryRecord['DeliveryStatus'] == 'senttodriver')
				{
					$newStatus = 'withdriver';
				}
				else if ($deliveryRecord['DeliveryStatus'] == 'withdriver_cancelpending')
				{
					$newStatus = 'cancelled_confirmed';
				}
				else
				{
					// unknown!  but device could be out of date
				}
				
				if ($newStatus != NULL)
				{
					$sql = "UPDATE Deliveries SET DeliveryStatus='$newStatus' WHERE AssignedToDeviceId=$deviceId AND GlobalDeliveryId=".intval($_POST['Deliveries'][$i]['GlobalDeliveryId'])." AND Version=".intval($_POST['Deliveries'][$i]['Version'])." LIMIT 1";
					exec_query($sql);
				}
			}
			else
			{
				// if GlobalDeliveryId was given, it should be correct!
				
				die("invalid record index supplied '".intval($_POST['Deliveries'][$i]['GlobalDeliveryId'])."'");
			}
		}
	}
	
	// end transaction for TOIPHONEACK operations
	exec_query("COMMIT");
	
	reply_start(); // "OK", "Data ack'd"
}
else if ($requestType == "TOIPHONE")
{
	// no special handling, the TOIPHONE payload is added to all authenticated requests	
	reply_start();
}
else
{
	error_reply_and_exit("Unknown request type '$requestType'");
}

reply_add_device_details();
reply_add_import_section();

reply_end();

