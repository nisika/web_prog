<?php

require_once("DBConfig.php");


function exec_query($sql)
{
	$result = mysql_query($sql);
	if (!$result) {
		echo $sql;  // TODO: delete this line in production
		die('Invalid query: ' . mysql_error());
	}
	return $result;
}

// ref "Using stripslashes() on an array" : http://www.php.cn/php/function.stripslashes.html
function stripslashes_deep($value)
{
    $value = is_array($value) ?
                array_map('stripslashes_deep', $value) :
                stripslashes($value);

    return $value;
}


/*
 * Connect to Database
 */
$link = mysql_connect('localhost', $username, $password);
if (!$link) {
    die('Could not connect to database: ' . mysql_error());
}


// make mygpslog the current db
$db_selected = mysql_select_db($database, $link);
if (!$db_selected) {
    die ('Can\'t use gpslog_udelivered : ' . mysql_error());
}

// ref: http://omegadelta.net/2009/10/18/safer-php-filenames/
function safe_filename($filename)
{
	return preg_replace(array('/[^A-Za-z0-9_\-.]/', '/.php/'), '_', $filename);
}

// NOTE: This was modified from GPS Log's DBFunctions.php:urlKey.  Changes should be sync'd
function udacKey($integer, $checksum_size)
{
	srand();
	
	$integer *= $checksum_size;
	$integer += rand(0, $checksum_size-1);
	
	return unfracked_base_convert(strval($integer), 10, 32, "23456789abcdefghijkmnpqrstuvwxyz");
}

// NOTE: This was copied from GPS Log's DBFunctions.php:urlKey.  Changes should be sync'd
function urlKey($integer, $checksum_size)
{
	srand();
	
	$integer *= $checksum_size;
	$integer += rand(0, $checksum_size-1);
	
	return (unfracked_base_convert(strval($integer), 10, 64));
}

// ref http://cn2.php.net/manual/en/function.base-convert.php#66240 (Michael Renner)
// extended to support base 64
// NOTE: This was copied from GPS Log's DBFunctions.php:unfracked_base_convert (with modified charset).  Changes should be sync'd
function unfracked_base_convert ($numstring, $frombase, $tobase, $chars="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-") {
	
	if ($tobase > strlen($chars))
	{
		die("custom base_convert error, $tobase exceeds available chars");
	}
	$tostring = substr($chars, 0, $tobase);
	
	$length = strlen($numstring);
	$result = '';
	for ($i = 0; $i < $length; $i++) {
		$number[$i] = strpos($chars, $numstring{$i});
	}
	do {
		$divide = 0;
		$newlen = 0;
		for ($i = 0; $i < $length; $i++) {
			$divide = $divide * $frombase + $number[$i];
			if ($divide >= $tobase) {
				$number[$newlen++] = (int)($divide / $tobase);
				$divide = $divide % $tobase;
			} elseif ($newlen > 0) {
				$number[$newlen++] = 0;
			}
		}
		$length = $newlen;
		$result = $tostring{$divide} . $result;
	}
	while ($newlen != 0);
	return $result;
}


function getRelativeScriptPath()
{
	return substr($_SERVER['SCRIPT_NAME'], 0, strlen($_SERVER['SCRIPT_NAME'])-strlen(basename($_SERVER['SCRIPT_NAME'])));
}

function redirectToSelfAndExit()
{
	$url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
	redirectAndExit($url);
}

function redirectToRelativeURLAndExit($url)
{
	$url = "http://".$_SERVER['HTTP_HOST'].getRelativeScriptPath().$url;
	redirectAndExit($url);
}

function redirectAndExit($url)
{
	header("Location: $url");

	echo "Redirecting... <a href=\"$url\">click here to continue</a>.";
	
	exit;
}



