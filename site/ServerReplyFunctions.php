<?php


function reply_and_exit($status, $message)
{
	reply_start($status, $message);
	reply_end();
	exit;
}

function error_reply_and_exit($message, $statusCode = "")
{
	reply_start("ERROR", $message, $statusCode);
	reply_end();
	exit;
}

function reply_start($status = "OK", $message = "", $statusCode = "")
{
	/*
	 * Reply Message Output
	 */
	 
	// TODO: $message safe-chars xml
	
	header("Content-Type: text/xml");
	
	// status of the
	echo "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n";
	echo "<ServerReply>\n";
	echo "	<ServiceResults>\n";
	echo "		<Status>$status</Status>\n";
	if (isset($message) && $message != "")
	{
		echo "		<Message>".$message."</Message>\n";
	}
	if (isset($statusCode) && $statusCode != "")
	{
		echo "		<StatusCode>".$statusCode."</StatusCode>\n";
	}
	echo "	</ServiceResults>\n";
}

function reply_end()
{
	echo "</ServerReply>\n";
}

function reply_add_section($sectionName, $attribs)
{
	// TODO: $attribs safe-chars xml

	echo "	<$sectionName>\n";
	foreach ($attribs as $key => $value)
	{
		echo "		<$key>$value</$key>\n";
	}
	echo "	</$sectionName>\n";
	
}

// ref: http://www.totallyphp.co.uk/code/create_a_random_password.htm
function createRandomPassword($length = 5)
{
    $chars = "ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz23456789";

    srand((double)microtime()*1000000);

    $i = 0;
    $pass = '';

    while ($i < $length)
	{
        $num = rand() % strlen($chars);
        $tmp = substr($chars, $num, 1);
        $pass = $pass.$tmp;
        $i++;
    }

    return $pass;
}
