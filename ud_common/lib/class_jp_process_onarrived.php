<?php


    class class_kickoff
    {

function pf_customError($errno,$errstr,$errfile,$errline,$errcontext)
  {
// NOTE **** THIS ONLY CATCHES NON-FATAL ERRORS
//  echo "<b>Error gw12:</b> [$errno] $errstr $errfile $errline $errcontext.<br>";
// gw 20100315       switch ($error_level) {
       switch ($errno) {
        case E_USER_ERROR:
            echo "<b>My ERROR</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            echo "  Fatal error on line $errline in file $errfile";
            echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
            echo "Aborting...<br />\n";
            exit(1);
            break;

        case E_USER_WARNING:
            echo "<b>My WARNING</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        case E_USER_NOTICE:
            echo "<b>My NOTICE</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        default:
            echo "<BR>Unknown error type: [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;
        }

    return true;

 }

//    $sys_debug = strtoupper("yes");
//    $_SESSION['ko_prog_path'] = 'c:\\tdx\\web_prog\\dev\\';

function cl_process()
{    
    $sys_debug = "NO";
A000_SET_RUN:
     //set error handler
    set_error_handler("pf_customError", E_ALL);
    date_default_timezone_set('Australia/Brisbane');
//    session_start();
    $sys_debug_text = "<br> gw set debug ";
    $sys_function_name = "class_jp_process_onarrived cl_process ";

    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." start of process";};
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." set session vars ";};
    $ps_job_start = microtime(true);

    if (isset($_SESSION['ko_prog_path']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
    }else{
    $_SESSION['ko_prog_path'] = "";
    }
    if (isset($_SESSION['ko_dbase_to_connectto']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
    }else{
    $_SESSION['ko_dbase_to_connectto'] = "tdxpryda";
    }

    if (isset($_SESSION['ko_map_path']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
        $ko_map_path = $_SESSION['ko_map_path'];
    }else{
    $_SESSION['ko_map_path'] = "";
    }

A000_DEFINE_VARIABLES:
    $sys_debug = "NO"; 
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." a000_define_vars";};
    $sys_prog_name = "";
    $ps_file_exists = '';
    $ps_filename="";
    $map = "";
    $tmp_array = array();
    $sys_function_out = "";
    $ps_temp_value = "";

    $ps_sessionno = "";

    $ps_file_count = "0";
    $ps_file_count_error = "0";
    $ps_file_count_skip = "0";
    $ps_file_count_unknown = "0";

A200_LOAD_LIBRARIES:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." a200_load_libraries";};

    global $class_main;
    global $class_sql;


     IF ($sys_debug == "YES"){echo "_get=";print_r($_GET);echo "<br>";};
     IF ($sys_debug == "YES"){echo "_post=";print_r($_POST);echo "<br>";};

A300_CONNECT_TODBASE:
     $dbcnx = $class_sql->c_sqlclient_connect();
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." A300_CONNECT_TODBASE";};

A400_GET_PARAMETERS:

A500_SET_VALUES:

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};
    $s_utime_search = time();
    $s_check_back_seconds = 86400;
    //60 = 1min, 120=2min, 300=5min, 1200=20min, 3600=1hr, 7200=2hr, 14400=4hrs, 28800=8hrs, 86400=24hrs
    $s_utime_since = $s_utime_search - $s_check_back_seconds;
B100_PROCESS:
    echo "<br> Start of class_jp_progress_onarrived on ".date("Ymd")." at ".date('H:m:s');
    echo "<br> &nbsp;&nbsp;&nbsp; s_check_back_seconds=".$s_check_back_seconds."";
    echo "<br> &nbsp;&nbsp;&nbsp; s_utime_search=".$s_utime_search."";
    echo "<BR><hr>";

    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};
    $s_sql = "SELECT DISTINCT(CompanyId) from events where DateReceived >= '".$s_utime_since."' group by CompanyId order by CompanyId";

    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};

    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
        ECHO "<BR> cl_jp_proc_onarrival no records found for s_sql=".$s_sql."<br>";
        goto B_900_END_RECORDS;
    }
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql."";
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;  s_numrows=".$s_numrows."";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
//    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name."  s_sql = ".$s_sql."<BR>key1=".$row['key1']."  key2 =".$row['key2']." key3= ".$row['key3']." key4= ".$row['key4'];};

B_158_NEXT:
    $i_record_count = $i_record_count + 1;

//    echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows."<br>";
    $s_Companyid = $row["CompanyId"];
    echo "<BR>******************************************************************************************";
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp; Process Company ".$s_Companyid."";

    $s_temp = $this->fn_B100_do_company($dbcnx,$sys_debug, "CL_PROCESS B158", "NONE", $s_Companyid);
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp; Company ".$s_Companyid." Complete s_temp=".$s_temp;
     //." Globaleventid=".$row["GlobalEventId"]." GlobalEventTypeId=".$row["GlobalEventTypeId"];

    GOTO B_100_GET_RECORDS;
B_159_END:

B_900_END_RECORDS:

    echo "<BR><hr>";
    echo "<br> End of class_jp_progress_onarrived on ".date("Ymd")." at ".date('H:m:s');



/*
log start
update "last started"  utility record
get "last completed - events " utility record to display
get "last completed - delivereies " utility record to display

do events
// every event record is created with the Date Receved = server time received
    keep the current utime
    SELECT * FROM events where DateReceived > utime(now) -1

    get all records not processed sorted by company and eventype
    process records

        get the actions for the company + eventtype + onarrival
        do the actions
        update record as processed
        next record


do deliveries
    same as events

update "last completed" utility reocrd with the current time OR the date received time of the last record processed
// *** potential is to miss a record if it is written between processing the last record and writting the utility record

log end

*/
B900_EXIT:
B999_END_PROCESS:
C100_POD_IMAGE_PROCESS:
    // pkn 20110829 - hard codes to Vellex 28 until a better method is created by Will
    $s_Companyid = "28";
    $s_temp = $this->fn_D100_do_pod_images($dbcnx,$sys_debug, "CL_PROCESS C100_POD_IMAGE_PROCESS", "NONE", $s_Companyid);
C999_END_FILE_PROCESS:

Z900_EXIT:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." Z900_EXIT ";};

//END OF PRIMARY RUN
    }

function fn_B100_do_company($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_B100_do_company";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;

    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Do Events for Company ".$ps_companyid."";

    if ($ps_companyid == "28") //vellex
    {
        goto B000_START;
    }
    if ($ps_companyid == "2")  // demo@udelivered
    {
        goto B000_START;
    }

    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Skipping Company ".$ps_companyid."";
    goto Z900_EXIT;
B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};
    $s_utime_search = time();
    $s_check_back_seconds = 86400;
    //60 = 1min, 120=2min, 300=5min, 1200=20min, 3600=1hr, 7200=2hr, 14400=4hrs, 28800=8hrs, 86400=24hrs
    $s_utime_since = $s_utime_search - $s_check_back_seconds;
B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    $s_sql = "SELECT DISTINCT(GlobalEventTypeId) from events where DateReceived >= '".$s_utime_since."' and CompanyId = '".$ps_companyid."' group by GlobalEventTypeId order by GlobalEventTypeId";
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql."";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";
        goto B_900_END_RECORDS;
    }
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql."";
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
//    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name."  s_sql = ".$s_sql."<BR>key1=".$row['key1']."  key2 =".$row['key2']." key3= ".$row['key3']." key4= ".$row['key4'];};

B_158_NEXT:
    $i_record_count = $i_record_count + 1;

//    echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows."<br>";
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ######################";
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Process  GlobalEventTypeId=".$row["GlobalEventTypeId"]."";
    $s_temp = $this->fn_C200_check_data_integration($dbcnx,$sys_debug, "fn_B100_do_company B158", "NONE", $ps_companyid,$row["GlobalEventTypeId"] );
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Result from data integration check s_temp=".$s_temp."";
    if (strtoupper($s_temp) <> "YES")
    {
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; So skip the GlobalEventTypeId";
        GOTO B_800_NEXT_REC;
    }
    echo "<br> find out if the event type has a data integration record for the companay ";
    echo "<br> if not skip the entire event ";
    echo "<br> else do same DI for each record ";

    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Start Events for GlobalEventTypeId=".$row["GlobalEventTypeId"]."";
    $s_temp = $this->fn_C100_do_event($dbcnx,$sys_debug, "fn_B100_do_company B158", "NONE", $ps_companyid,$row["GlobalEventTypeId"] );
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Complete GlobalEventTypeId=".$row["GlobalEventTypeId"]."";


B_800_NEXT_REC:
    GOTO B_100_GET_RECORDS;
B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    return $sys_function_out;
}


function fn_C100_do_event($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid,$ps_GlobalEventTypeId )
{
    A010_START:
    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_C100_do_event";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;

    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."  Do records for Company ".$ps_companyid." and GlobalEventTypeId=".$ps_GlobalEventTypeId;

    if ($ps_companyid == "28") //vellex
    {
        goto b120_check_28;
    }
    if ($ps_companyid == "2")  // demo@udelivered
    {
        goto b110_check_2;
    }
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Skipping events for Company ".$ps_companyid."";
    goto Z900_EXIT;
b110_check_2:
    if ($ps_GlobalEventTypeId == "391")  // delivery scans
    {
        goto B000_START;
    }
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Skipping this event  ".$ps_GlobalEventTypeId."";
    goto Z900_EXIT;
b120_check_28:

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};
    $s_utime_search = time();
    $s_check_back_seconds = 86400;
    //60 = 1min, 120=2min, 300=5min, 1200=20min, 3600=1hr, 7200=2hr, 14400=4hrs, 28800=8hrs, 86400=24hrs
    $s_utime_since = $s_utime_search - $s_check_back_seconds;
B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    $s_sql = "SELECT * from events where DateReceived >= '".$s_utime_since."' and CompanyId = '".$ps_companyid."' and GlobalEventTypeId = ".$ps_GlobalEventTypeId."  ";
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql."";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";
        goto B_900_END_RECORDS;
    }
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql."";
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
//    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name."  s_sql = ".$s_sql."<BR>key1=".$row['key1']."  key2 =".$row['key2']." key3= ".$row['key3']." key4= ".$row['key4'];};

B_158_NEXT:
    $i_record_count = $i_record_count + 1;

    $s_GlobalEventId = $row["GlobalEventId"];
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Process Event s_GlobalEventId = ".$s_GlobalEventId."";
B_200_DO_DELIVERY_SCANS:

B_210_CREATE_SCAN_FILE:
    $s_scans_file_path="C:\\tdx\\html\\ftp\\udelivered\\from_ud\\";
    $s_scans_filename = $s_scans_file_path."udscans_".$ps_companyid."_".date("YmdHis").$i_record_count.".txt";
    @ $fp = fopen($s_scans_filename,"a");
    flock($fp,2); //lock the file for writing
    
B_220_CREATE_SCAN_FILE_HEADER:
    $s_date = $this->fn_Z100_do_date();    
    $s_time=date("His");
    $s_system_to="*DELNET";
    $s_system_from="DEPOTWATCH";
    $s_version="V1";
    $s_app_version="App=1.5.0";
    $s_coid="VELL_001";
    $s_pid="VEL";
    $s_sid="SYDOPS";
    $s_uid="VELL_001";
    $s_opr="OPS";
    $s_lcn="SYDOPS";
    
    $s_outputstring = $s_date."|".$s_time."|".$s_system_to."|".$s_system_from."|".$s_version."|App=".$s_app_version."|CoId=".$s_coid."|PID=".$s_pid."|SID=".$s_sid."|UID=".$s_uid."|OPR=".$s_opr."|LCN=".$s_lcn."|END|END"."\r\n";
    fwrite($fp, $s_outputstring);

B_230_CREATE_SCAN_FILE_DETAILS:
    $s_scanned_details=$row['Note'];
    
    $s_scanned_details=strtr($s_scanned_details,chr(13),'|'); // replace carriage return with |
    $s_scanned_details=strtr($s_scanned_details,chr(10),'|'); // replace line feed with |
    
    $ar_scanned_number=explode("|",$s_scanned_details);
    $i=0;
    while($ar_scanned_number[$i]<>'')
    {
        $s_date = $this->fn_Z100_do_date();
        $s_time=date("His");
        $s_scanned_number=trim($ar_scanned_number[$i]);

        $s_outputstring = $s_date."|".$s_time."|DN1|DA|V1|DELIVERED|".$s_scanned_number."|DELIVERED|".$s_scanned_number."|END"."\r\n";
        fwrite($fp, $s_outputstring);
        
        $i++;
    }
    
B_240_CREATE_SCAN_FILE_FOOTER:
    $s_date = $this->fn_Z100_do_date();
    $s_time=date("His");

    $s_outputstring = $s_date."|".$s_time."|*EOF|END"."\r\n";
    fwrite($fp, $s_outputstring);    

    flock($fp,3); //release the write lock
    fclose($fp);
  
B_250_FTP_FILE:
    $s_site_address="vellaexpress.com.au";
    $s_upload_folder="";
    $s_username="vell_ud";
    $s_password="vell_ud";
    $s_upload_file=$s_scans_filename;
    $s_ftp_mode="PASV";                
                
    $this->fn_Z200_ftp_file($s_site_address,$s_upload_folder,$s_username,$s_password,$s_upload_file,$s_ftp_mode);

B_290_END:
    GOTO B_880_GET_RECORDS;


B_880_GET_RECORDS:
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Complete Event s_GlobalEventId = ".$s_GlobalEventId."";
    GOTO B_100_GET_RECORDS;
B_159_END:

B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    return $sys_function_out;
}

function fn_C200_check_data_integration($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid,$ps_GlobalEventTypeId )
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "";


    $sys_function_out = "YES";
    goto Z900_EXIT;

    // get the data_integration records
    // if exist = YES
    // if not == NO

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_C200_check_data_integration";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;

//    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Do records for Company ".$ps_companyid." and GlobalEventTypeId=".$ps_GlobalEventTypeId;
B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};
B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    $s_sql = "SELECT * from events where DateReceived >= '".$s_utime_since."' and CompanyId = '".$ps_companyid."' and GlobalEventTypeId = ".$ps_GlobalEventTypeId."  ";
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql."";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";
        goto B_900_END_RECORDS;
    }
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql."";
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
//    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name."  s_sql = ".$s_sql."<BR>key1=".$row['key1']."  key2 =".$row['key2']." key3= ".$row['key3']." key4= ".$row['key4'];};

B_158_NEXT:
    $i_record_count = $i_record_count + 1;

    $s_GlobalEventId = $row["GlobalEventId"];
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Process Event s_GlobalEventId = ".$s_GlobalEventId."";

    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Complete Event s_GlobalEventId = ".$s_GlobalEventId."";

    GOTO B_100_GET_RECORDS;
B_159_END:

B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    return $sys_function_out;
}

function fn_D100_do_pod_images($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_D100_do_pod_images";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;

    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Do POD Images for Company ".$ps_companyid."";

    if ($ps_companyid == "28") //vellex
    {
        goto B000_START;
    }

    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Skipping Company ".$ps_companyid."";
    goto Z900_EXIT;
B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};
    $s_utime_search = time();
    $s_check_back_seconds = 86400;
    //60 = 1min, 120=2min, 300=5min, 1200=20min, 3600=1hr, 7200=2hr, 14400=4hrs, 28800=8hrs, 86400=24hrs
    $s_utime_since = $s_utime_search - $s_check_back_seconds;
B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};
    
    $s_sql = " select * from deliveries where DeliveryDate > '{$s_utime_since}' and companyid = '{$ps_companyid}' order by DeliveryDate" ;
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);
                                                                                                              
    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql."";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";
        goto B_900_END_RECORDS;
    }
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql."";
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
    $s_globaldeliveryid = trim($row['GlobalDeliveryId']);
    $s_cn_number = trim($row['CNNO']);
    
    $s_temp = $this->fn_D200_send_pod_image($dbcnx,$sys_debug, "fn_D100_do_pod_images B150", "NONE", $s_globaldeliveryid,$ps_companyid,$s_cn_number );

B_158_NEXT:
    $i_record_count = $i_record_count + 1;

B_800_NEXT_REC:
    GOTO B_100_GET_RECORDS;
B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    return $sys_function_out;
}

function fn_D200_send_pod_image($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options,$ps_globaldeliveryid,$ps_companyid,$ps_cn_number )
{
    A010_START:
    $sys_debug_text = "";
    $sys_debug = "";
    $s_image_cnt=0;

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_C100_do_event";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};
    $s_utime_search = time();
    $s_check_back_seconds = 86400;
    //60 = 1min, 120=2min, 300=5min, 1200=20min, 3600=1hr, 7200=2hr, 14400=4hrs, 28800=8hrs, 86400=24hrs
    $s_utime_since = $s_utime_search - $s_check_back_seconds;
B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    $s_sql = " select * from attachments where globaldeliveryid = '{$ps_globaldeliveryid}' ";
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql."";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";
        goto B_900_END_RECORDS;
    }
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql."";
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
    $s_image_cnt++;
    
    $s_pod_filename = $row["FileName"];
    $s_pod_filepath = "C:\\tdx\\html\\udelivered\\proof2\\usrdata\\".$ps_companyid."\\";
    $s_pod_file_from = $s_pod_filepath.$s_pod_filename;

B151_CREATE_CN_POD_FILE:
    $s_pod_tco="";
    if ($ps_companyid=="28")
    {
        $s_pod_tco="VELLA";
        $s_pod_company="VELLA";
    }
    
    $s_pod_filename_to=$s_pod_tco.'_'.$s_pod_company.'_'.$ps_cn_number.'_'.$s_image_cnt.'.JPG';    
    $s_pod_filepath_to="C:\\tdx\\html\\ftp\\udelivered\\from_ud\\";
    $s_pod_file_to=$s_pod_filepath_to.$s_pod_filename_to;
    
    if (!copy($s_pod_file_from, $s_pod_file_to)) 
    {
        echo "failed to copy $s_pod_file_from...\n";
    }
    
B_158_NEXT:
    $i_record_count = $i_record_count + 1;
  
B_200_FTP_POD_FILE:
    $s_site_address="vellaexpress.com.au";
    $s_upload_folder="";
    $s_username="vell_ud";
    $s_password="vell_ud";
    $s_upload_file=$s_pod_file_to;
    $s_ftp_mode="PASV";                
                
    $this->fn_Z200_ftp_file($s_site_address,$s_upload_folder,$s_username,$s_password,$s_upload_file,$s_ftp_mode);
    
B_210_ARCHIVE_FILE:
    $s_archive_date=date("Ymd");
    $s_archive_path=$s_pod_filepath_to.$s_archive_date;
    $s_md = @mkdir($s_archive_path,0777);
    $s_archive_path=$s_archive_path."\\";
    rename("{$s_pod_file_to}","{$s_archive_path}{$s_pod_filename_to}");

B_290_END:
    GOTO B_880_GET_RECORDS;


B_880_GET_RECORDS:
    GOTO B_100_GET_RECORDS;
B_159_END:

B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    return $sys_function_out;
}

function fn_Z100_do_date()
{
    $s_date=date("Ymd");
    $s_date_YYYY=substr($s_date,0,4);
    $s_date_MM=substr($s_date,4,2);
    $s_date_DD=substr($s_date,6,2);
    switch($s_date_MM) 
    {
        case "01" : $s_date_MMM = "JAN"; break;
        case "02" : $s_date_MMM = "FEB"; break;
        case "03" : $s_date_MMM = "MAR"; break;
        case "04" : $s_date_MMM = "APR"; break;
        case "05" : $s_date_MMM = "MAY"; break;
        case "06" : $s_date_MMM = "JUN"; break;
        case "07" : $s_date_MMM = "JUL"; break;
        case "08" : $s_date_MMM = "AUG"; break;
        case "09" : $s_date_MMM = "SEP"; break;
        case "10" : $s_date_MMM = "OCT"; break;
        case "11" : $s_date_MMM = "NOV"; break;
        case "12" : $s_date_MMM = "DEC"; break;
    }
    $s_date=$s_date_YYYY.$s_date_MMM.$s_date_DD;
    
    return $s_date;
}

function fn_Z200_ftp_file($ps_site_address,$ps_upload_folder,$ps_username,$ps_password,$ps_upload_file,$ps_ftp_mode)
{
    //echo 'ps_site_address='.$ps_site_address.'<br>';
    //echo 'ps_upload_folder='.$ps_upload_folder.'<br>';
    //echo 'ps_username='.$ps_username.'<br>';
    //echo 'ps_password='.$ps_password.'<br>';
    //echo 'ps_upload_file='.$ps_upload_file.'<br>';
    //echo 'ps_ftp_mode='.$ps_ftp_mode.'<br>';
    
    $s_conn_id = ftp_connect($ps_site_address);

    // login with username and password
    $login_result = ftp_login($s_conn_id, $ps_username, $ps_password);

    // check connection
    if ((!$s_conn_id) || (!$login_result)) {
        echo "FTP connection has failed!";
        echo "Attempted to connect to $ps_site_address for user $ps_username";
        return false;
    } 
    else 
    {
        echo "Connected to $ps_site_address, for user $ps_username";
    }

    if (trim($ps_upload_folder)<>'')
    {
        $chdir = ftp_chdir($s_conn_id,$ps_upload_folder);
        
        if(!$chdir) 
        {
            echo "Could not change directory to {$ps_upload_folder}";
            return false;
        }            
    }
        
    // upload the file
    //$upload = ftp_put($s_conn_id, basename($ps_upload_file), $ps_upload_file, FTP_ASCII);
    $upload = ftp_put($s_conn_id, basename($ps_upload_file), $ps_upload_file, FTP_BINARY);
        
    // check upload status
    if (!$upload) 
    {
        echo "FTP upload has failed!";
    } 
    else 
    {
        echo "Uploaded $ps_upload_file to $ps_site_address as $ps_upload_file";
    }

    // close the FTP stream
    ftp_close($s_conn_id);
}

    }
?>
