<?php

// http://devel.ncss.com.au/als_web_prog/dev/ut_kickoff.php?path=/var/www/html/ud_jm_dev/&init_process=runclass&class_library=class_ujp_process_events.php&function_name=cl_process&inifile=kickoff_ncss_linux.ini&debug=no

    class class_udj_events
    {

function pf_customError($errno,$errstr,$errfile,$errline,$errcontext)
  {
// NOTE **** THIS ONLY CATCHES NON-FATAL ERRORS
//  echo "<b>Error gw12:</b> [$errno] $errstr $errfile $errline $errcontext.<br>";
// gw 20100315       switch ($error_level) {
       switch ($errno) {
        case E_USER_ERROR:
            echo "<b>My ERROR</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            echo "  Fatal error on line $errline in file $errfile";
            echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
            echo "Aborting...<br />\n";
            exit(1);
            break;

        case E_USER_WARNING:
            echo "<b>My WARNING</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        case E_USER_NOTICE:
            echo "<b>My NOTICE</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        default:
            echo "<BR>Unknown error type: [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;
        }

    return true;

 }

//    $sys_debug = strtoupper("yes");
//    $_SESSION['ko_prog_path'] = 'c:\\tdx\\web_prog\\dev\\';

function cl_process()
{
    $sys_debug = "NO";
A000_SET_RUN:
     //set error handler
    set_error_handler("pf_customError", E_ALL);
//gw20111114 - changed on server - pkn to update his code    date_default_timezone_set('Australia/Brisbane');

    $timezone = 'Australia/Sydney';

    date_default_timezone_set($timezone);
//    session_start();
    $sys_debug_text = "<br> gw set debug ";
    $sys_function_name = "class_jp_process_onarrived cl_process ";

    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." start of process";};
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." set session vars ";};
    $ps_job_start = microtime(true);

    if (isset($_SESSION['ko_prog_path']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
    }else{
    $_SESSION['ko_prog_path'] = "";
    }
    if (isset($_SESSION['ko_dbase_to_connectto']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
    }else{
    $_SESSION['ko_dbase_to_connectto'] = "tdxpryda";
    }

    if (isset($_SESSION['ko_map_path']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
        $ko_map_path = $_SESSION['ko_map_path'];
    }else{
    $_SESSION['ko_map_path'] = "";
    }

A000_DEFINE_VARIABLES:
    $sys_debug = "NO";
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." a000_define_vars";};
    $sys_prog_name = "";
    $ps_file_exists = '';
    $ps_filename="";
    $map = "";
    $tmp_array = array();
    $sys_function_out = "";
    $ps_temp_value = "";

    $ps_sessionno = "";

    $ps_file_count = "0";
    $ps_file_count_error = "0";
    $ps_file_count_skip = "0";
    $ps_file_count_unknown = "0";

A200_LOAD_LIBRARIES:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." a200_load_libraries";};

    global $class_main;
    global $class_sql;


     IF ($sys_debug == "YES"){echo "_get=";print_r($_GET);echo "<br>";};
     IF ($sys_debug == "YES"){echo "_post=";print_r($_POST);echo "<br>";};

A300_CONNECT_TODBASE:
     $dbcnx = $class_sql->c_sqlclient_connect();
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." A300_CONNECT_TODBASE";};

A400_GET_PARAMETERS:

A500_SET_VALUES:

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};
    $s_utime_search = time();
    $s_check_back_seconds = 600;
    //60 = 1min, 120=2min, 300=5min, 600=10min, 1200=20min, 3600=1hr, 7200=2hr, 14400=4hrs, 28800=8hrs, 86400=24hrs, 604800=7days
    $s_utime_since = $s_utime_search - $s_check_back_seconds;
B100_PROCESS:
    echo "<br> Start of class_jp_progress_onarrived on ".date("Ymd")." at ".date('H:m:s');
    echo "<br> &nbsp;&nbsp;&nbsp; s_check_back_seconds=".$s_check_back_seconds."";
    echo "<br> &nbsp;&nbsp;&nbsp; s_utime_search=".$s_utime_search."";
    echo "<BR><hr>";

    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};
    $s_sql = "SELECT DISTINCT(CompanyId) from events where DateReceived >= '".$s_utime_since."' group by CompanyId order by LogDate,CompanyId";
    echo 's_sql='.$s_sql.'<br>';
    //exit();
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};

    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
        ECHO "<BR> cl_jp_process_events no records found for s_sql=".$s_sql."<br>";
        goto B_900_END_RECORDS;
    }
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql."";
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;  s_numrows=".$s_numrows."";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
//    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name."  s_sql = ".$s_sql."<BR>key1=".$row['key1']."  key2 =".$row['key2']." key3= ".$row['key3']." key4= ".$row['key4'];};

B_158_NEXT:
    $i_record_count = $i_record_count + 1;

//    echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows."<br>";
    $s_Companyid = $row["CompanyId"];
    echo "<BR>******************************************************************************************";
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp; Process Company ".$s_Companyid."";

    $s_temp = $this->fn_B100_do_company($dbcnx,$sys_debug, "CL_PROCESS B158", "NONE", $s_Companyid);
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp; Company ".$s_Companyid." Complete s_temp=".$s_temp;
     //." Globaleventid=".$row["GlobalEventId"]." GlobalEventTypeId=".$row["GlobalEventTypeId"];

    GOTO B_100_GET_RECORDS;
B_159_END:

B_900_END_RECORDS:

    echo "<BR><hr>";
    echo "<br> End of class_jp_progress_onarrived on ".date("Ymd")." at ".date('H:m:s');



/*
log start
update "last started"  utility record
get "last completed - events " utility record to display
get "last completed - delivereies " utility record to display

do events
// every event record is created with the Date Receved = server time received
    keep the current utime
    SELECT * FROM events where LogDate > utime(now) -1

    get all records not processed sorted by company and eventype
    process records

        get the actions for the company + eventtype + onarrival
        do the actions
        update record as processed
        next record


do deliveries
    same as events

update "last completed" utility reocrd with the current time OR the date received time of the last record processed
// *** potential is to miss a record if it is written between processing the last record and writting the utility record

log end

*/
B900_EXIT:
B999_END_PROCESS:


C100_POD_IMAGE_PROCESS:
    // pkn 20110829 - hard codes to Vellex 28 until a better method is created by Will
    $s_Companyid = "28";
    $s_temp = $this->fn_D100_do_pod_images($dbcnx,$sys_debug, "CL_PROCESS C100_POD_IMAGE_PROCESS", "NONE", $s_Companyid);
C999_END_FILE_PROCESS:

Z900_EXIT:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." Z900_EXIT ";};

//END OF PRIMARY RUN
    }

function fn_B100_do_company($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_B100_do_company";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

    $s_log_file_path = "/var/www/html/ud_jm/logs/events_log_";
    $s_log_file = $s_log_file_path.date("YmdHis").".txt";
    @ $fp = fopen($s_log_file,"a");
    flock($fp,2); //lock the file for writing

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;

    if (isset($_SESSION['ko_udelivered_type']))
    {
        $ko_udelivered_type=strtoupper(trim($_SESSION['ko_udelivered_type']));
    }
    $ko_udelivered_type='FIELD_SERVICE';
    if ($ko_udelivered_type=='FIELD_SERVICE')
    {
        //$_SESSION['ko_udelivered_type'];
    }
    //echo 'ko_udelivered_type='.$ko_udelivered_type.'<br>';

    if (isset($_SESSION['ko_udelivered_company_id']))
    {
        $ko_udelivered_company_id=strtoupper(trim($_SESSION['ko_udelivered_company_id']));
    }
    $ko_udelivered_company_id='3';
    //echo 'ko_udelivered_company_id='.$ko_udelivered_company_id.'<br>';

    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Do Events for Company ".$ps_companyid."";

    $outputstring = date("Ymd")." at ".date('H:m:s')." ".$sys_function_name."   Do Events for Company ".$ps_companyid.""."\n";
    fwrite($fp, $outputstring);

    /*
    if ($ps_companyid == "28") //vellex
    {
        goto B000_START;
    }
    if ($ps_companyid == "2")  // demo@udelivered
    {
        goto B000_START;
    }*/

    if ($ps_companyid === $ko_udelivered_company_id)
    {
        goto B000_START;
    }


    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Skipping Company ".$ps_companyid."";
    goto Z900_EXIT;
B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};
    $s_utime_search = time();
    $s_check_back_seconds = 600;
    //60 = 1min, 120=2min, 300=5min, 600=10min, 1200=20min, 3600=1hr, 7200=2hr, 14400=4hrs, 28800=8hrs, 86400=24hrs, 604800=7days
    $s_utime_since = $s_utime_search - $s_check_back_seconds;
B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    //$s_sql = "SELECT DISTINCT(GlobalEventTypeId) from events where LogDate >= '".$s_utime_since."' and CompanyId = '".$ps_companyid."' group by GlobalEventTypeId order by GlobalEventTypeId";
    // pkn 20111102 -
    $s_sql = "SELECT * from events where DateReceived >= '".$s_utime_since."' and CompanyId = '".$ps_companyid."' order by LogDate ASC";
    echo 'new s_sql='.$s_sql.'<br>';
    $outputstring = 'events sql: '.$s_sql."\n";
    fwrite($fp, $outputstring);

    //exit();
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql."";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";

        $outputstring = 'no records found for this run'."\n";
        fwrite($fp, $outputstring);

        goto B_900_END_RECORDS;
    }
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql."";
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
//    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name."  s_sql = ".$s_sql."<BR>key1=".$row['key1']."  key2 =".$row['key2']." key3= ".$row['key3']." key4= ".$row['key4'];};

B_158_NEXT:
    $i_record_count = $i_record_count + 1;

//    echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows."<br>";
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ######################";
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Process  GlobalEventTypeId=".$row["GlobalEventTypeId"]."";
    $s_temp = $this->fn_C200_check_data_integration($dbcnx,$sys_debug, "fn_B100_do_company B158", "NONE", $ps_companyid,$row["GlobalEventTypeId"] );
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Result from data integration check s_temp=".$s_temp."";
    if (strtoupper($s_temp) <> "YES")
    {
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; So skip the GlobalEventTypeId";
        GOTO B_800_NEXT_REC;
    }
    echo "<br> find out if the event type has a data integration record for the companay ";
    echo "<br> if not skip the entire event ";
    echo "<br> else do same DI for each record ";

    $s_globaleventid=$row["GlobalEventId"];
    $s_event_rec_exists='NO';
    $outputstring = 'checking if an event status record already exists for eventid:'.$s_globaleventid."\n";
    fwrite($fp, $outputstring);
    $s_event_rec_exists=$this->fn_F100_check_event_status($dbcnx,$sys_debug, "fn_B100_do_company B158", "NONE", $ps_companyid, $s_globaleventid,$fp);

    $outputstring = 'eventid:'.$s_globaleventid.' event rec exists='.$s_event_rec_exists."\n";
    fwrite($fp, $outputstring);

    echo 's_event_rec_exists='.$s_event_rec_exists.'<br>';
    echo 's_globaleventid='.$s_globaleventid.'<br>';

    // pkn 20111107 - if event status record exists for event, event has already been done so skip and goto next record
    if ($s_event_rec_exists == "YES")
    {
        echo 's_globaleventid='.$s_globaleventid.' is being skipped'.'<br>';
        $outputstring = 's_globaleventid='.$s_globaleventid.' is being skipped'."\n";
        fwrite($fp, $outputstring);

        goto B_800_NEXT_REC;
    }

    $this->fn_F200_create_event_status($dbcnx,$sys_debug, "fn_B100_do_company B158", "NONE", $ps_companyid, $s_globaleventid,$fp);

    //exit();

    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Start Events for GlobalEventTypeId=".$row["GlobalEventTypeId"]."";

    $outputstring = 'Start Events for GlobalEventTypeId='.$s_globaleventid."\n";
    fwrite($fp, $outputstring);

    //$s_temp = $this->fn_C100_do_event($dbcnx,$sys_debug, "fn_B100_do_company B158", "NONE", $ps_companyid,$row["GlobalEventTypeId"] );
    $s_temp = $this->fn_C100_do_event($dbcnx,$sys_debug, "fn_B100_do_company B158", "NONE", $ps_companyid, $s_globaleventid );
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Complete GlobalEventTypeId=".$row["GlobalEventTypeId"]."";

B_800_NEXT_REC:
    GOTO B_100_GET_RECORDS;
B_900_END_RECORDS:


Z900_EXIT:
    flock($fp,3); //release the write lock
    fclose($fp);

    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    return $sys_function_out;
}


//function fn_C100_do_event($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid,$ps_GlobalEventTypeId )
function fn_C100_do_event($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid, $ps_globaleventid )
{
    global $class_clprocessjobline;

A010_START:
    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_C100_do_event";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;

    //echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."  Do records for Company ".$ps_companyid." and GlobalEventTypeId=".$ps_GlobalEventTypeId;
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."  Do records for Company ".$ps_companyid." and globaleventid=".$ps_globaleventid;

    if ($ps_companyid == "28") //vellex
    {
        goto b120_check_28;
    }
    if ($ps_companyid == "2")  // demo@udelivered
    {
        goto b110_check_2;
    }
    if ($ps_companyid == "3")  // ncss
    {
        goto B000_START;
    }
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Skipping events for Company ".$ps_companyid."";
    goto Z900_EXIT;
b110_check_2:
    if ($ps_GlobalEventTypeId == "391")  // delivery scans
    {
        goto B000_START;
    }
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Skipping this event  ".$ps_globaleventid."";
    goto Z900_EXIT;
b120_check_28:

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};
    $s_utime_search = time();
    $s_check_back_seconds = 600;
    //60 = 1min, 120=2min, 300=5min, 600=10min, 1200=20min, 3600=1hr, 7200=2hr, 14400=4hrs, 28800=8hrs, 86400=24hrs, 604800=7days
    $s_utime_since = $s_utime_search - $s_check_back_seconds;
B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    //$s_sql = "SELECT * from events where LogDate >= '".$s_utime_since."' and CompanyId = '".$ps_companyid."' and GlobalEventTypeId = ".$ps_GlobalEventTypeId."  ";
    $s_sql = "SELECT * from events where LogDate >= '".$s_utime_since."' and CompanyId = '".$ps_companyid."' and GlobalEventId = ".$ps_globaleventid."  ";

    //echo 's_sql='.$s_sql.'<br>';
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql."";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";
        goto B_900_END_RECORDS;
    }
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql."";
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
//    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name."  s_sql = ".$s_sql."<BR>key1=".$row['key1']."  key2 =".$row['key2']." key3= ".$row['key3']." key4= ".$row['key4'];};

    if (isset($_SESSION['ko_udelivered_type']))
    {
        $ko_udelivered_type=strtoupper(trim($_SESSION['ko_udelivered_type']));
    }
    $ko_udelivered_type='FIELD_SERVICE';
    if ($ko_udelivered_type <> 'FIELD_SERVICE')
    {
            goto B_158_NEXT;
    }
    $s_GlobalEventId = trim($row["GlobalEventId"]);
    $s_GlobalEventTypeId = trim($row["GlobalEventTypeId"]);
    $s_GlobalDeliveryId = trim($row["GlobalDeliveryId"]);
    $s_event_note = trim($row["Note"]);
    $s_event_date = trim($row["Date"]);
    $s_event_logdate = trim($row["LogDate"]);

    $s_event_fieldname_def='|';
    $s_event_fieldname_data='|';

    $s_event_fieldname_def=$s_event_fieldname_def.'UD_date|UD_logdate|';
    $s_event_fieldname_data=$s_event_fieldname_data.$s_event_date.'|'.$s_event_logdate.'|';

    // pkn 20110117 - start from here tomorrow
    echo 's_GlobalEventId='.$s_GlobalEventId.'<br>';
    echo 's_GlobalEventTypeId='.$s_GlobalEventTypeId.'<br>';
    echo 's_GlobalDeliveryId='.$s_GlobalDeliveryId.'<br>';
    echo 's_event_note='.$s_event_note.'<br>';

    $s_event_note=strtr($s_event_note,chr(13),'|'); // replace carriage return with |
    $s_event_note=strtr($s_event_note,chr(10),'|'); // replace line feed with |
    $s_event_note=str_replace('||','|',$s_event_note);
    $s_event_note=str_replace("'"," ",$s_event_note);

    echo 's_event_note pipes='.$s_event_note.'<br>';

    if (strpos($s_event_note,"|") === false )
    {
        if (strpos($s_event_note,":=:") === false )
        {
            $s_event_fieldname_def=$s_event_fieldname_def.'UD_note_field|';
            $s_event_fieldname_data=$s_event_fieldname_data.$s_event_note.'|';
            goto B_157_GET_JOB_DETS;
        }
    }

B_156_DO_ENTERED_VALUES:
    $ar_event_note=explode('|',$s_event_note);
    $cnt=0;
    while($ar_event_note[$cnt]<>'')
    {
        $ar_event_note_details=explode(':=:',$ar_event_note[$cnt]);
        $s_event_fieldname=trim($ar_event_note_details[0]);
        $s_event_fieldname = str_replace(" ","_",$s_event_fieldname);
        $s_event_fieldvalue=trim($ar_event_note_details[1]);
        $s_event_fieldname_def=$s_event_fieldname_def.'UD_'.$s_event_fieldname.'|';
        $s_event_fieldname_data=$s_event_fieldname_data.$s_event_fieldvalue.'|';
        echo 's_event_fieldname='.$s_event_fieldname.'<br>';
        echo 's_event_fieldvalue='.$s_event_fieldvalue.'<br>';
        $cnt++;
    }
B_157_GET_JOB_DETS:

    $s_temp = $this->fn_E100_get_cn_number_fields($dbcnx,$sys_debug, "CL_PROCESS E100_GET_CN_NUMBER", "NONE", $ps_companyid, $s_GlobalDeliveryId);
    if (trim($s_temp=='|^%##%^|'))
    {
        goto B_158_NEXT;
    }
    if (strpos($s_temp,"|^%##%^|") ===false)
    {
        goto B_158_NEXT;
    }else{
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_ud_delivery_details_def = "|".$ar_line_details[0];
        $s_ud_delivery_details_data = "|".$ar_line_details[1];
        $s_temp = "";
    };

    $s_job_number = $class_main->clmain_v300_set_variable("REC_uddev_CNNO",$s_ud_delivery_details_def,$s_ud_delivery_details_data," NO","11","UJP_PROCESS_EVENT C100 B158");

    $s_temp = $this->fn_E200_get_jobs_id_fields($dbcnx,$sys_debug, "CL_PROCESS E200_GET_JOBS_ID", "NONE", $ps_companyid, $s_job_number);
    if (strpos($s_temp,"|^%##%^|") ===false)
    {
    }else{
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_jm_details_def = "|".$ar_line_details[0];
        $s_jm_details_data = "|".$ar_line_details[1];
        $s_temp = "";
    };

    $s_temp = $this->fn_E300_get_event_type_fields($dbcnx,$sys_debug, "CL_PROCESS E300_GET_EVENT_TYPE", "NONE", $ps_companyid, $s_GlobalEventTypeId);
    if (strpos($s_temp,"|^%##%^|") ===false)
    {
    }else{
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_ev_details_def = "|".$ar_line_details[0];
        $s_ev_details_data = "|".$ar_line_details[1];
        $s_temp = "";
    };

    $_COOKIE['ud_logonname']='ud_process_events';
    $_COOKIE['ud_companyid']='3';
    $xml_arr='';
    $s_sessionno='11';
    $s_details_def='START'.$s_ud_delivery_details_def.$s_jm_details_def.$s_ev_details_def.$s_event_fieldname_def;
    $s_details_data='START'.$s_ud_delivery_details_data.$s_jm_details_data.$s_ev_details_data.$s_event_fieldname_data;

//    echo 's_details_def='.$s_details_def.'<br>';
//    echo 's_details_data='.$s_details_data.'<br>';

	$timezone = date_default_timezone_get();
    $_SESSION[$s_sessionno.'ut_logon_timezone']=$timezone;
	echo '<br> process_events_b157 time zone = '.$timezone;


   $s_actiondetails="sys_classV2|v1|jcl_result|clmain_u720_do_process(jobs_masters,jm_id,#p%!_REC_jm_jobs_id_!%#p,udelivered,#p%!_REC_ev_DisplayName_!%#p,NO,udj_process_events)|NO|END";
    $s_returned = $class_clprocessjobline->pf_clprocessjobline($ps_dbcnx,$s_actiondetails,$s_details_def,$s_details_data,$xml_arr,$s_sessionno,'NO');

/*
    $s_job_number = $this->fn_E100_get_cn_number($dbcnx,$sys_debug, "CL_PROCESS E100_GET_CN_NUMBER", "NONE", $ps_companyid, $s_GlobalDeliveryId);
    $s_jobs_id = $this->fn_E200_get_jobs_id($dbcnx,$sys_debug, "CL_PROCESS E200_GET_JOBS_ID", "NONE", $ps_companyid, $s_job_number);
    $s_event_name = $this->fn_E300_get_event_type($dbcnx,$sys_debug, "CL_PROCESS E200_GET_EVENT_TYPE", "NONE", $ps_companyid, $s_GlobalEventTypeId);

    $s_jobs_id=trim($s_jobs_id);

    if ($s_jobs_id=='')
    {
        goto B_158_NEXT;
    }

    echo 's_job_number='.$s_job_number.'<br>';
    echo 's_jobs_id='.$s_jobs_id.'<br>';
    echo 's_event_name='.$s_event_name.'<br>';

    $_COOKIE['ud_logonname']='ud_process_events';
    $_COOKIE['ud_companyid']='3';
    $xml_arr='';
    $s_sessionno='11';
    $s_details_def='START|POSTV_jobs_id|POSTV_companyid_job_no|POSTV_event_name'.$s_event_fieldname_def;
    $s_details_data='START|'.$s_jobs_id.'|'.$s_job_number.'|'.$s_event_name.$s_event_fieldname_data;
//    echo 's_details_def='.$s_details_def.'<br>';
//    echo 's_details_data='.$s_details_data.'<br>';

    $s_actiondetails="sys_classV2|v1|jcl_result|clmain_u720_do_process(jobs_masters,jm_id,#p%!_postv_jobs_id_!%#p,udelivered,#p%!_postv_event_name_!%#p,NO,udj_process_events)|NO|END";
    $s_returned = $class_clprocessjobline->pf_clprocessjobline($ps_dbcnx,$s_actiondetails,$s_details_def,$s_details_data,$xml_arr,$s_sessionno,'NO');
*/

B_158_NEXT:
    $i_record_count = $i_record_count + 1;

    $s_GlobalEventId = $row["GlobalEventId"];
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Process Event s_GlobalEventId = ".$s_GlobalEventId."";

B_290_END:
    GOTO B_880_GET_RECORDS;


B_880_GET_RECORDS:
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Complete Event s_GlobalEventId = ".$s_GlobalEventId."";
    GOTO B_100_GET_RECORDS;
B_159_END:

        exit();

B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    return $sys_function_out;
}
//#################################################################################################################################################################################################
function fn_C200_check_data_integration($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid,$ps_GlobalEventTypeId )
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "";


    $sys_function_out = "YES";
    goto Z900_EXIT;

    // get the data_integration records
    // if exist = YES
    // if not == NO

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_C200_check_data_integration";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;

//    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Do records for Company ".$ps_companyid." and GlobalEventTypeId=".$ps_GlobalEventTypeId;
B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};
B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    $s_sql = "SELECT * from events where LogDate >= '".$s_utime_since."' and CompanyId = '".$ps_companyid."' and GlobalEventTypeId = ".$ps_GlobalEventTypeId."  ";
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql."";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";
        goto B_900_END_RECORDS;
    }
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql."";
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
//    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name."  s_sql = ".$s_sql."<BR>key1=".$row['key1']."  key2 =".$row['key2']." key3= ".$row['key3']." key4= ".$row['key4'];};

B_158_NEXT:
    $i_record_count = $i_record_count + 1;

    $s_GlobalEventId = $row["GlobalEventId"];
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Process Event s_GlobalEventId = ".$s_GlobalEventId."";

    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Complete Event s_GlobalEventId = ".$s_GlobalEventId."";

    GOTO B_100_GET_RECORDS;
B_159_END:

B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    return $sys_function_out;
}
//#################################################################################################################################################################################################
function fn_D100_do_pod_images($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_D100_do_pod_images";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;

    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Do POD Images for Company ".$ps_companyid."";

    if ($ps_companyid == "28") //vellex
    {
        goto B000_START;
    }

    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Skipping Company ".$ps_companyid."";
    goto Z900_EXIT;
B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};
    $s_utime_search = time();
    $s_check_back_seconds = 600;
    //60 = 1min, 120=2min, 300=5min, 600=10min, 1200=20min, 3600=1hr, 7200=2hr, 14400=4hrs, 28800=8hrs, 86400=24hrs, 604800=7days
    $s_utime_since = $s_utime_search - $s_check_back_seconds;
B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    $s_sql = " select * from deliveries where DeliveryDate > '{$s_utime_since}' and companyid = '{$ps_companyid}' order by DeliveryDate" ;
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql."";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";
        goto B_900_END_RECORDS;
    }
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql."";
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
    $s_globaldeliveryid = trim($row['GlobalDeliveryId']);
    $s_cn_number = trim($row['CNNO']);

    $s_temp = $this->fn_D200_send_pod_image($dbcnx,$sys_debug, "fn_D100_do_pod_images B150", "NONE", $s_globaldeliveryid,$ps_companyid,$s_cn_number );

B_158_NEXT:
    $i_record_count = $i_record_count + 1;

B_800_NEXT_REC:
    GOTO B_100_GET_RECORDS;
B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    return $sys_function_out;
}
//#################################################################################################################################################################################################
function fn_D200_send_pod_image($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options,$ps_globaldeliveryid,$ps_companyid,$ps_cn_number )
{
    A010_START:
    $sys_debug_text = "";
    $sys_debug = "";
    $s_image_cnt=0;

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_D200_send_pod_image";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};
    $s_utime_search = time();
    $s_check_back_seconds = 600;
    //60 = 1min, 120=2min, 300=5min, 600=10min, 1200=20min, 3600=1hr, 7200=2hr, 14400=4hrs, 28800=8hrs, 86400=24hrs, 604800=7days
    $s_utime_since = $s_utime_search - $s_check_back_seconds;
B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    $s_sql = " select * from attachments where globaldeliveryid = '{$ps_globaldeliveryid}' ";
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql."";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";
        goto B_900_END_RECORDS;
    }
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql."";
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
    $s_image_cnt++;

    $s_pod_filename = $row["FileName"];
    $s_pod_filepath = "C:\\tdx\\html\\udelivered\\proof2\\usrdata\\".$ps_companyid."\\";
    $s_pod_file_from = $s_pod_filepath.$s_pod_filename;

B151_CREATE_CN_POD_FILE:
    $s_pod_tco="";
    if ($ps_companyid=="28")
    {
        $s_pod_tco="VELLA";
        $s_pod_company="VELLA";
    }

    $s_pod_filename_to=$s_pod_tco.'_'.$s_pod_company.'_'.$ps_cn_number.'_'.$s_image_cnt.'.JPG';
    $s_pod_filepath_to="C:\\tdx\\html\\ftp\\udelivered\\from_ud\\";
    $s_pod_file_to=$s_pod_filepath_to.$s_pod_filename_to;

    if (!copy($s_pod_file_from, $s_pod_file_to))
    {
        echo "failed to copy $s_pod_file_from...\n";
    }

B_158_NEXT:
    $i_record_count = $i_record_count + 1;

B_200_FTP_POD_FILE:
    $s_site_address="vellaexpress.com.au";
    $s_upload_folder="";
    $s_username="vell_ud";
    $s_password="vell_ud";
    $s_upload_file=$s_pod_file_to;
    $s_ftp_mode="PASV";

    $this->fn_Z200_ftp_file($s_site_address,$s_upload_folder,$s_username,$s_password,$s_upload_file,$s_ftp_mode);

B_210_ARCHIVE_FILE:
    $s_archive_date=date("Ymd");
    $s_archive_path=$s_pod_filepath_to.$s_archive_date;
    $s_md = @mkdir($s_archive_path,0777);
    $s_archive_path=$s_archive_path."\\";
    rename("{$s_pod_file_to}","{$s_archive_path}{$s_pod_filename_to}");

B_290_END:
    GOTO B_880_GET_RECORDS;


B_880_GET_RECORDS:
    GOTO B_100_GET_RECORDS;
B_159_END:

B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    return $sys_function_out;
}
//#################################################################################################################################################################################################
function fn_E100_get_cn_number($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid, $ps_GlobalDeliveryId)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_E100_get_cn_number";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;

    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Get CN Number ".$ps_companyid."";

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};

B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    $s_sql = " select * from deliveries where GlobalDeliveryId = '{$ps_GlobalDeliveryId}' and companyid = '{$ps_companyid}' " ;
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql."";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";
        goto B_900_END_RECORDS;
    }
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql."";
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:

    $s_cn_number = trim($row['CNNO']);

B_158_NEXT:
    $i_record_count = $i_record_count + 1;

B_800_NEXT_REC:
    GOTO B_100_GET_RECORDS;
B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    $sys_function_out=$s_cn_number;
    return $sys_function_out;
}
//#################################################################################################################################################################################################
function fn_E100_get_cn_number_fields($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid, $ps_GlobalDeliveryId)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_E100_get_cn_number_fields";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;
    $s_details_def = "";
    $s_details_data = "";

    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Get CN Number ".$ps_companyid."";

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};

B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    $s_sql = " select * from deliveries where GlobalDeliveryId = '{$ps_GlobalDeliveryId}' and companyid = '{$ps_companyid}' " ;
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql."";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";
        goto B_900_END_RECORDS;
    }
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql."";
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
    foreach( $row as $key=>$value)
    {
        if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
        {
            GOTO B_151_SKIP_DATA_BLOB;
        }
        if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
        {
            GOTO B_151_SKIP_DATA_BLOB;
        }
        if (strpos(strtoupper($value),"XML") === false)
        {
            GOTO B_151_SKIP_DATA_BLOB;
        }

        $ar_xml = simplexml_load_string($value);
        $newArry = array();
        $ar_xml = (array) $ar_xml;
        foreach ($ar_xml as $key => $value)
        {
            $s_details_def .= "|REC_uddev_DB_".$key;
            $s_details_data .= "|".$class_main->clmain_u596_xml_decode_char($value);
        }
        continue;

 B_151_SKIP_DATA_BLOB:
        if (strpos(strtoupper($key),"DETAILS_D") === FALSE)
        {
            $s_details_def .= "|REC_uddev_".$key;
            $s_details_data .= "|".$value;
        }
         continue;
    }
B_158_NEXT:
    $i_record_count = $i_record_count + 1;

B_800_NEXT_REC:
    GOTO B_100_GET_RECORDS;
B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    $sys_function_out = $s_details_def."|^%##%^|".$s_details_data;
    return $sys_function_out;
}
//#################################################################################################################################################################################################
function fn_E200_get_jobs_id($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid, $ps_job_number)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_E200_get_jobs_id";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;

    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Get Jobs ID ".$ps_companyid."";

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};

B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    $s_sql = " select * from jobs where companyid_job_no = '{$ps_job_number}' and companyid = '{$ps_companyid}' " ;
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql."";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";
        goto B_900_END_RECORDS;
    }
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql."";
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:

    $s_jobs_id = trim($row['jobs_id']);

B_158_NEXT:
    $i_record_count = $i_record_count + 1;

B_800_NEXT_REC:
    GOTO B_100_GET_RECORDS;
B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    $sys_function_out=$s_jobs_id;
    return $sys_function_out;
}
//#################################################################################################################################################################################################
function fn_E200_get_jobs_id_fields($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid, $ps_job_number)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_E200_get_jobs_id_fields";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;
    $s_details_def = "";
    $s_details_data = "";

    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Get Jobs ID ".$ps_companyid."";

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};

B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    $s_sql = " select * from jobs where companyid_job_no = '{$ps_job_number}' and companyid = '{$ps_companyid}' " ;
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql."";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";
        goto B_900_END_RECORDS;
    }
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql."";
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
    foreach( $row as $key=>$value)
    {
        if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
        {
            GOTO B_151_SKIP_DATA_BLOB;
        }
        if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
        {
            GOTO B_151_SKIP_DATA_BLOB;
        }
        if (strpos(strtoupper($value),"XML") === false)
        {
            GOTO B_151_SKIP_DATA_BLOB;
        }

        $ar_xml = simplexml_load_string($value);
        $newArry = array();
        $ar_xml = (array) $ar_xml;
        foreach ($ar_xml as $key => $value)
        {
            $s_details_def .= "|REC_jm_DB_".$key;
            $s_details_data .= "|".$class_main->clmain_u596_xml_decode_char($value);
        }
        continue;

B_151_SKIP_DATA_BLOB:
        if (strpos(strtoupper($key),"DETAILS_D") === FALSE)
        {
            $s_details_def .= "|REC_jm_".$key;
            $s_details_data .= "|".$value;
        }
        continue;
    }
B_158_NEXT:
    $i_record_count = $i_record_count + 1;

B_800_NEXT_REC:
    GOTO B_100_GET_RECORDS;
B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    $sys_function_out = $s_details_def."|^%##%^|".$s_details_data;
    return $sys_function_out;
}
//#################################################################################################################################################################################################
function fn_E300_get_event_type($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid, $ps_GlobalEventTypeId)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_E300_get_event_type";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;

    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Get Event Type ".$ps_companyid."";

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};

B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    $s_sql = " select * from eventtypes where GlobalEventTypeId = '{$ps_GlobalEventTypeId}' " ;
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql."";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";
        goto B_900_END_RECORDS;
    }
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql."";
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:

    $s_event_name = trim($row['DisplayName']);

B_158_NEXT:
    $i_record_count = $i_record_count + 1;

B_800_NEXT_REC:
    GOTO B_100_GET_RECORDS;
B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    $sys_function_out=$s_event_name;
    return $sys_function_out;
}
//#################################################################################################################################################################################################
function fn_E300_get_event_type_fields($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid, $ps_GlobalEventTypeId)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_fromwhere;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_E300_get_event_type_fields";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;
    $s_details_def = "";
    $s_details_data = "";

    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$sys_function_name."   Get Event Type ".$ps_companyid."";

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};

B100_PROCESS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B100_PROCESS";};

    $s_sql = " select * from eventtypes where GlobalEventTypeId = '{$ps_GlobalEventTypeId}' " ;
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};
    $i_record_count = "0";

B_000_RECORD_LOOP:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_000_RECORD_LOOP";};
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
//        ECHO "<BR> ".$sys_function_name." no records found for s_sql=".$s_sql."<br>";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO record found s_sql=".$s_sql."";
        echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";
        goto B_900_END_RECORDS;
    }
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql."";
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; s_numrows=".$s_numrows."";

B_100_GET_RECORDS:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B_100_GET_RECORDS";};
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";

B_150_DO_RECORDS:
    foreach( $row as $key=>$value)
    {
        if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
        {
            GOTO B_151_SKIP_DATA_BLOB;
        }
        if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
        {
            GOTO B_151_SKIP_DATA_BLOB;
        }
        if (strpos(strtoupper($value),"XML") === false)
        {
            GOTO B_151_SKIP_DATA_BLOB;
        }

        $ar_xml = simplexml_load_string($value);
        $newArry = array();
        $ar_xml = (array) $ar_xml;
        foreach ($ar_xml as $key => $value)
        {
            $value = trim($value);
            $s_details_def .= "|REC_ev_DB_".$key;
            $s_details_data .= "|".$class_main->clmain_u596_xml_decode_char($value);
        }
        continue;

 B_151_SKIP_DATA_BLOB:
        if (strpos(strtoupper($key),"DETAILS_D") === FALSE)
        {
            $value = trim($value);
            $s_details_def .= "|REC_ev_".$key;
            $s_details_data .= "|".$value;
        }
        continue;
    }
B_158_NEXT:
    $i_record_count = $i_record_count + 1;

B_800_NEXT_REC:
    GOTO B_100_GET_RECORDS;
B_900_END_RECORDS:


Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    $sys_function_out = $s_details_def."|^%##%^|".$s_details_data;
    return $sys_function_out;
}

function fn_F100_check_event_status($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid, $ps_globaleventid, $fp)
{
    global $class_sql;
    global $class_main;

    $outputstring = 'fn_F100_check_event_status:'.$ps_globaleventid."\n";
    fwrite($fp, $outputstring);

A010_START:

    $dbcnx = $ps_dbcnx;
    $s_sql = "SELECT * from jobs_activity where jobsact_related_id = '".$ps_globaleventid."' and jobsact_group = 'SYSTEM' and jobsact_type = 'EVENTS_STATUS' ";
    $outputstring = 'sql:'.$s_sql."\n";
    fwrite($fp, $outputstring);
    //echo 's_sql='.$s_sql.'<br>';
    //exit();

B_000_RECORD_LOOP:
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_rec_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
        ECHO "<BR> fn_F100_check_event_status for s_sql=".$s_sql."<br>";
        goto B_900_END_RECORDS;
    }
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;&nbsp; s_sql=".$s_sql."";
    echo "<br>".date("Ymd")." at ".date('H:m:s')."&nbsp;&nbsp;&nbsp;  s_numrows=".$s_numrows."";

B_100_GET_RECORDS:
    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_rec_found = "YES";

B_900_END_RECORDS:

Z900_EXIT:
    $outputstring = 'fn_F100_check_event_status, s_rec_found='.$s_rec_found."\n";
    fwrite($fp, $outputstring);

    return $s_rec_found;
}

function fn_F200_create_event_status($ps_dbcnx,$ps_debug, $ps_fromwhere, $ps_options, $ps_companyid, $ps_globaleventid)
{
    global $class_sql;
    global $class_main;
    require_once('lib/class_app.php');
    $class_app = new AppClass();

A100_CREATE_RECORD:

    $_COOKIE['ud_logonname']='process_events';
    $_COOKIE['ud_companyid']=$ps_companyid;

    $ps_debug="NO";
    $ps_details_def='';
    $ps_details_data='';
    $ps_sessionno="11";
    $s_Activity_recordgroup='SYSTEM';
    $s_Activity_recordtype='EVENTS_STATUS';
    $s_Activity_desc="SYSTEM EVENT STATUS RECORD";
    $s_Activity_notes="SYSTEM EVENT STATUS RECORD";

    $class_app->clapp_b290_make_jobs_activity($ps_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_globaleventid,$s_Activity_recordgroup,$s_Activity_recordtype,$s_Activity_desc,$s_Activity_notes);

}

//#################################################################################################################################################################################################
function fn_Z100_do_date()
{
    $s_date=date("Ymd");
    $s_date_YYYY=substr($s_date,0,4);
    $s_date_MM=substr($s_date,4,2);
    $s_date_DD=substr($s_date,6,2);
    switch($s_date_MM)
    {
        case "01" : $s_date_MMM = "JAN"; break;
        case "02" : $s_date_MMM = "FEB"; break;
        case "03" : $s_date_MMM = "MAR"; break;
        case "04" : $s_date_MMM = "APR"; break;
        case "05" : $s_date_MMM = "MAY"; break;
        case "06" : $s_date_MMM = "JUN"; break;
        case "07" : $s_date_MMM = "JUL"; break;
        case "08" : $s_date_MMM = "AUG"; break;
        case "09" : $s_date_MMM = "SEP"; break;
        case "10" : $s_date_MMM = "OCT"; break;
        case "11" : $s_date_MMM = "NOV"; break;
        case "12" : $s_date_MMM = "DEC"; break;
    }
    $s_date=$s_date_YYYY.$s_date_MMM.$s_date_DD;

    return $s_date;
}
//#################################################################################################################################################################################################
function fn_Z200_ftp_file($ps_site_address,$ps_upload_folder,$ps_username,$ps_password,$ps_upload_file,$ps_ftp_mode)
{
    //echo 'ps_site_address='.$ps_site_address.'<br>';
    //echo 'ps_upload_folder='.$ps_upload_folder.'<br>';
    //echo 'ps_username='.$ps_username.'<br>';
    //echo 'ps_password='.$ps_password.'<br>';
    //echo 'ps_upload_file='.$ps_upload_file.'<br>';
    //echo 'ps_ftp_mode='.$ps_ftp_mode.'<br>';

    $s_conn_id = ftp_connect($ps_site_address);

    // login with username and password
    $login_result = ftp_login($s_conn_id, $ps_username, $ps_password);

    // check connection
    if ((!$s_conn_id) || (!$login_result)) {
        echo "FTP connection has failed!";
        echo "Attempted to connect to $ps_site_address for user $ps_username";
        return false;
    }
    else
    {
        echo "Connected to $ps_site_address, for user $ps_username";
    }

    if (trim($ps_upload_folder)<>'')
    {
        $chdir = ftp_chdir($s_conn_id,$ps_upload_folder);

        if(!$chdir)
        {
            echo "Could not change directory to {$ps_upload_folder}";
            return false;
        }
    }

    // upload the file
    //$upload = ftp_put($s_conn_id, basename($ps_upload_file), $ps_upload_file, FTP_ASCII);
    $upload = ftp_put($s_conn_id, basename($ps_upload_file), $ps_upload_file, FTP_BINARY);

    // check upload status
    if (!$upload)
    {
        echo "FTP upload has failed!";
    }
    else
    {
        echo "Uploaded $ps_upload_file to $ps_site_address as $ps_upload_file";
    }

    // close the FTP stream
    ftp_close($s_conn_id);
}

    }
?>

