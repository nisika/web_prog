<!--
history  utl_help.php
gjw  20110312 - created from my_help from mypallet bok
-->
<?php

function pf_customError($errno,$errstr,$errfile,$errline,$errcontext)
  {
// NOTE **** THIS ONLY CATCHES NON-FATAL ERRORS
//  echo "<b>Error gw12:</b> [$errno] $errstr $errfile $errline $errcontext.<br>";
// gw 20100315       switch ($error_level) {
       switch ($errno) {
        case E_USER_ERROR:
            echo "<b>My ERROR</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            echo "  Fatal error on line $errline in file $errfile";
            echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
            echo "Aborting...<br />\n";
            exit(1);
            break;

        case E_USER_WARNING:
            echo "<b>My WARNING</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        case E_USER_NOTICE:
            echo "<b>My NOTICE</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        default:
            echo "<BR>Unknown error type: [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;
        }

    /* Don't execute PHP internal error handler */
    return true;

  }

A000_SET_RUN:
     //set error handler
    set_error_handler("pf_customError", E_ALL);
    date_default_timezone_set('Australia/Brisbane');
    session_start();

//gw20110921 - added user timezone
    if (isset($_SESSION[$s_sessionno.'ut_logon_timezone']))
    {
        $timezone=$_SESSION[$s_sessionno.'ut_logon_timezone'];
        date_default_timezone_set($timezone);
    }else{
    }



    $sys_debug="";
    $sys_debug = strtoupper("NO");
//    $sys_debug = strtoupper("yes");

     IF ($sys_debug == "YES"){echo $sys_prog_name." started debug=".$sys_debug." *** remember to view source - it will save you hours  <br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_sql.php');
     $class_sql = new wp_SqlClient();
     IF ($sys_debug == "YES"){echo $sys_prog_name." after class_sql<br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_main.php');
     $class_main = new clmain();
     IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_main <br>";};

     $dbcnx = $class_sql->c_sqlclient_connect();

//    require_once('lib/class_main.php');
//    require_once('lib/class_sql.php');

//    require_once('lib/class_myplib1.php');;

//    $objlib1 = new myplib1();

//    $class_sql::sq1_connect();

    $get_helplocation = $_GET["p"];
    $get_userid = $_GET["userid"];
    $get_recgroup = $_GET["recgroup"];
    $get_rectype = $_GET["rectype"];
    $get_bunit = $_GET["bunit"];

    $s_utlh_id="NEWREC";
    $s_data_blob = "";

    IF ($sys_debug == "YES"){echo "help locaton  =".$get_helplocation."<br>";};
    IF ($sys_debug == "YES"){echo "userid =".$get_userid."<br>";};

   $s_usernotes = "No User notes apply.  See system notes";

    $s_summary = "";
    $s_data_blob = "";


       $s_map_name='ut_issue_report_map.html';
       $s_details_def='START|utlh_userid|utlh_helplocation|utlh_recgroup|utlh_rectype|utlh_id|';
       $s_details_data='START|'.$get_userid.'|'.$get_helplocation.'|'.$get_recgroup."|".$get_rectype.'|'.$s_utlh_id.'|';

       $s_details_def=$s_details_def.'END';


       $s_details_data=$s_details_data.'END';


IF ($sys_debug == "YES"){echo "s_filename =".$s_map_name."<br>";};
IF ($sys_debug == "YES"){echo "s_details_def =".$s_details_def."<br>";};
IF ($sys_debug == "YES"){echo "s_details_data =".$s_details_data."<br>";};

    $s_map_group = "issue_report_map";
    echo $class_main->clmain_v100_load_html_screen($s_map_name,$s_details_def,$s_details_data,"NO",$s_map_group);

IF ($sys_debug == "YES"){echo "after main<br>";};

?>
