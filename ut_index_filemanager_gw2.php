<?php




/****************************************************************/
/* function home()                                              */
/*                                                              */
/* Main function that displays contents of folders.             */
/****************************************************************/
function home() {
  global $folder, $tbcolor1, $tbcolor2, $tbcolor3, $filefolder, $HTTP_HOST;
  f_draw_maintop("Home");
  echo "<font face=\"tahoma\" size=\"2\"><b>\n"
      ."<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" width=100%>\n";

  $content1 = "";
  $content2 = "";

  $count = "0";
  $style = opendir($folder);
  $a=1;
  $b=1;
  $adminfile = "";

  if ($folder) {
    if (ereg("/home/",$folder)) {
      $folderx = ereg_replace("$filefolder", "", $folder);
      $folderx = "http://".$HTTP_HOST."/".$folderx;
    } else {
      $folderx = $folder;
    } 
  }

  while($stylesheet = readdir($style)) {
    if (strlen($stylesheet)>40) { 
      $sstylesheet = substr($stylesheet,0,40)."...";
    } else {
      $sstylesheet = $stylesheet;
    }
    if ($stylesheet[0] != "." && $stylesheet[0] != ".." ) {
      if (is_dir($folder.$stylesheet) && is_readable($folder.$stylesheet)) { 
        $content1[$a] ="<td>".$sstylesheet."</td>\n"
                 ."<td> "
                 //.disk_total_space($folder.$stylesheet)." Commented out due to certain problems
                 ."<td align=\"center\"><a href=\"".$adminfile."?op=home&folder=".$folder.$stylesheet."/\">Open</a>\n"
                 ."<td align=\"center\"><a href=\"".$adminfile."?op=ren&file=".$stylesheet."&folder=$folder\">Rename</a>\n"
                 ."<td align=\"center\"><a href=\"".$adminfile."?op=del&dename=".$stylesheet."&folder=$folder\">Delete</a>\n"
                 ."<td align=\"center\"><a href=\"".$adminfile."?op=mov&file=".$stylesheet."&folder=$folder\">Move</a>\n"
                 ."<td align=\"center\"> <tr height=\"2\"><td height=\"2\" colspan=\"3\">\n";
        $a++;
      } elseif (!is_dir($folder.$stylesheet) && is_readable($folder.$stylesheet)) {
        if ( substr(strtolower($stylesheet),0,3) <> "ut_")
        {
            $content2[$b] ="<td><a href=\"".$folderx.$stylesheet."\">".$sstylesheet."</a></td>\n"
                     ."<td align=\"left\"><img src=pixel.gif width=5 height=1>".filesize($folder.$stylesheet)
                     ."<td align=\"center\"><a href=\"".$adminfile."?op=edit&fename=".$stylesheet."&folder=$folder\">Edit</a>\n"
                     ."<td align=\"center\"><a href=\"".$adminfile."?op=ren&file=".$stylesheet."&folder=$folder\">Rename</a>\n"
                     ."<td align=\"center\"><a href=\"".$adminfile."?op=del&dename=".$stylesheet."&folder=$folder\">Delete</a>\n"
                     ."<td align=\"center\"><a href=\"".$adminfile."?op=mov&file=".$stylesheet."&folder=$folder\">Move</a>\n"
                     ."<td align=\"center\"><a href=\"".$adminfile."?op=viewframe&file=".$stylesheet."&folder=$folder\">View</a>\n"
                     ."<tr height=\"2\"><td height=\"2\" colspan=\"3\">\n";
            $b++;
          }
      } else {
        echo "Directory is unreadable\n";
      }
    $count++;
    } 
  }
  closedir($style);

  echo "Browsing: $folder\n"
       ."<br>Number of Files: " . $count . "<br><br>";

  echo "<tr bgcolor=\"$tbcolor3\" width=100%>"
      ."<td width=300>Filename\n"
      ."<td width=65>Size\n"
      ."<td align=\"center\" width=44>Open\n"
      ."<td align=\"center\" width=58>Rename\n"
      ."<td align=\"center\" width=57>Delete\n"
      ."<td align=\"center\" width=40>Move\n"
      ."<td align=\"center\" width=44>View\n"
      ."<tr height=\"2\"><td height=\"2\" colspan=\"3\">\n";

  for ($a=1; $a<count($content1)+1;$a++) {
    $tcoloring   = ($a % 2) ? $tbcolor1 : $tbcolor2;
    echo "<tr bgcolor=".$tcoloring." width=100%>";
    echo $content1[$a];
  }

  for ($b=1; $b<count($content2)+1;$b++) {
    $tcoloring   = ($a++ % 2) ? $tbcolor1 : $tbcolor2;
    echo "<tr bgcolor=".$tcoloring." width=100%>";
    echo $content2[$b];
  }

  echo"</table>";
  f_mainbottom();
}


/****************************************************************/
/* function up()                                                */
/*                                                              */
/* First step to Upload.                                        */
/* User enters a file and the submits it to upload()            */
/****************************************************************/
function up() {
  global $folder, $content, $filefolder; $adminfile;
  f_draw_maintop("Upload");

  echo "<FORM ENCTYPE=\"multipart/form-data\" ACTION=\"".$adminfile."?op=upload\" METHOD=\"POST\">\n"
      ."<font face=\"tahoma\" size=\"2\"><b>File:</b></font><br><input type=\"File\" name=\"upfile\" size=\"20\" class=\"text\">\n"

      ."<br><br>Destination:<br><select name=\"ndir\" size=1>\n"
      ."<option value=\"".$filefolder."\">".$filefolder."</option>";
  f_listdir($filefolder);
  echo $content
      ."</select><br><br>"

      ."<input type=\"submit\" value=\"Upload\" class=\"button\">\n"
      ."</form>\n";

  f_mainbottom();
}


/****************************************************************/
/* function upload()                                            */
/*                                                              */
/* Sencond step in upload.                                      */
/* Saves the file to the disk.                                  */
/* Recieves $upfile from up() as the uploaded file.             */
/****************************************************************/
function upload($upfile, $ndir) {

  global $folder;
  if (!$upfile) {
    error("Filesize too big or bytes=0");
  } elseif($upfile['name']) { 
    if(copy($upfile['tmp_name'],$ndir.$upfile['name'])) { 
      f_draw_maintop("Upload");
//      echo "The file ".$upfile['name'].$folder.$upfile_name." uploaded successfully.\n";
      echo "The file ".$upfile['name']." uploaded successfully.\n";
      f_mainbottom();
    } else {
      printerror("File $upfile failed to upload.");
    }
  } else {
    printerror("Please enter a filename.");
  }
}


/****************************************************************/
/* function del()                                               */
/*                                                              */
/* First step in delete.                                        */
/* Prompts the user for confirmation.                           */
/* Recieves $dename and ask for deletion confirmation.          */
/****************************************************************/
function del($dename) {
  global $folder;$adminfile
    if (!$dename == "") {
    f_draw_maintop("Delete");
    echo "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n"
        ."<font class=error>**WARNING: This will permanatly delete ".$folder.$dename.". This action is irreversable.**</font><br><br>\n"
        ."Are you sure you want to delete ".$folder.$dename."?<br><br>\n"
        ."<a href=\"".$adminfile."?op=delete&dename=".$dename."&folder=$folder\">Yes</a> | \n"
        ."<a href=\"".$adminfile."?op=home\"> No </a>\n"
        ."</table>\n";
    f_mainbottom();
  } else {
    home();
  }
}


/****************************************************************/
/* function delete()                                            */
/*                                                              */
/* Second step in delete.                                       */
/* Deletes the actual file from disk.                           */
/* Recieves $upfile from up() as the uploaded file.             */
/****************************************************************/
function delete($dename) {
  global $folder;
  if (!$dename == "") {
    f_draw_maintop("Delete");
    if (is_dir($folder.$dename)) {
      if(rmdir($folder.$dename)) {
        echo $dename." has been deleted.";
      } else {
        echo "There was a problem deleting this directory. ";
      }
    } else {
      if(unlink($folder.$dename)) {
        echo $dename." has been deleted.";
      } else {
        echo "There was a problem deleting this file. ";
      }
    }
    f_mainbottom();
  } else {
    home();
  }
}


/****************************************************************/
/* function edit()                                              */
/*                                                              */
/* First step in edit.                                          */
/* Reads the file from disk and displays it to be edited.       */
/* Recieves $upfile from up() as the uploaded file.             */
/****************************************************************/
function edit($fename) {
  global $folder; $adminfile;
  if (!$fename == "") {
    f_draw_maintop("Edit");
    echo $folder.$fename;

    echo "<form action=\"".$adminfile."?op=save\" method=\"post\">\n"
        ."<textarea cols=\"73\" rows=\"40\" name=\"ncontent\">\n";

   $handle = fopen ($folder.$fename, "r");
   $contents = "";
   $x = "";

    while ($x<1) {
      $data = @fread ($handle, filesize ($folder.$fename));
      if (strlen($data) == 0) {
        break;
      }
      $contents .= $data;
    }
    fclose ($handle);

    $replace1 = "</text";
    $replace2 = "area>";
    $replace3 = "< / text";
    $replace4 = "area>";
    $replacea = $replace1.$replace2;
    $replaceb = $replace3.$replace4;
    $contents = ereg_replace ($replacea,$replaceb,$contents);

    echo $contents;

    echo "</textarea>\n"
        ."<br><br>\n"
        ."<input type=\"hidden\" name=\"folder\" value=\"".$folder."\">\n"
        ."<input type=\"hidden\" name=\"fename\" value=\"".$fename."\">\n"
        ."<input type=\"submit\" value=\"Save\" class=\"button\">\n"
        ."</form>\n";
    f_mainbottom();
  } else {
    home();
  }
}


/****************************************************************/
/* function save()                                              */
/*                                                              */
/* Second step in edit.                                         */
/* Recieves $ncontent from edit() as the file content.          */
/* Recieves $fename from edit() as the file name to modify.     */
/****************************************************************/
function f_save($ncontent, $fename) {
  global $folder; $adminfile;
  if (!$fename == "") {
    f_draw_maintop("Edit");
    $loc = $folder.$fename;
    $fp = fopen($loc, "w");

    $replace1 = "</text";
    $replace2 = "area>";
    $replace3 = "< / text";
    $replace4 = "area>";
    $replacea = $replace1.$replace2;
    $replaceb = $replace3.$replace4;
    $ncontent = ereg_replace ($replaceb,$replacea,$ncontent);

    $ydata = stripslashes($ncontent);

    if(fwrite($fp, $ydata)) {
      echo "The file <a href=\"".$adminfile."?op=viewframe&file=".$fename."&folder=".$folder."\">".$folder.$fename."</a> was succesfully edited\n";
      $fp = null;
    } else {
      echo "There was a problem editing this file\n";
    }
    f_mainbottom();
  } else {
    home();
  }
}

/****************************************************************/
/* function cr()                                                */
/*                                                              */
/* First step in create.                                        */
/* Promts the user to a filename and file/directory switch.     */
/****************************************************************/
function cr() {
  global $folder, $content, $filefolder; $adminfile;
  f_draw_maintop("Create");
  if (!$content == "") { echo "<br><br>Please enter a filename.\n"; }
  echo "<form action=\"".$adminfile."?op=create\" method=\"post\">\n"
      ."Filename: <br><input type=\"text\" size=\"20\" name=\"nfname\" class=\"text\"><br><br>\n"
   
      ."Destination:<br><select name=ndir size=1>\n"
      ."<option value=\"".$filefolder."\">".$filefolder."</option>";
  f_listdir($filefolder);
  echo $content
      ."</select><br><br>";


  echo "File <input type=\"radio\" size=\"20\" name=\"isfolder\" value=\"0\" checked><br>\n"
      ."Directory <input type=\"radio\" size=\"20\" name=\"isfolder\" value=\"1\"><br><br>\n"
      ."<input type=\"hidden\" name=\"folder\" value=\"$folder\">\n"
      ."<input type=\"submit\" value=\"Create\" class=\"button\">\n"
      ."</form>\n";
  f_mainbottom();
}


/****************************************************************/
/* function create()                                            */
/*                                                              */
/* Second step in create.                                       */
/* Creates the file/directoy on disk.                           */
/* Recieves $nfname from cr() as the filename.                  */
/* Recieves $infolder from cr() to determine file trpe.         */
/****************************************************************/
function create($nfname, $isfolder, $ndir) {
  global $folder; $adminfile;
  if (!$nfname == "") {
    f_draw_maintop("Create");

    if ($isfolder == 1) {
      if(mkdir($ndir."/".$nfname, 0777)) {
        echo "Your directory, <a href=\"".$adminfile."?op=viewframe&file=".$nfname."&folder=$ndir\">".$ndir."/".$nfname."</a> was succesfully created.\n";
      } else {
        echo "The directory, ".$ndir."/".$nfname." could not be created. Check to make sure the permisions on the /files directory is set to 777\n";
      }
    } else {
      if(fopen($ndir."/".$nfname, "w")) {
        echo "Your file, <a href=\"".$adminfile."?op=viewframe&file=".$nfname."&folder=$ndir\">".$ndir.$nfname."</a> was succesfully created.\n";
      } else {
        echo "The file, ".$ndir."/".$nfname." could not be created. Check to make sure the permisions on the /files directory is set to 777\n";
      }
    }
    f_mainbottom();
  } else {
    cr();
  }
}


/****************************************************************/
/* function ren()                                               */
/*                                                              */
/* First step in rename.                                        */
/* Promts the user for new filename.                            */
/* Globals $file and $folder for filename.                      */
/****************************************************************/
function ren($file) {
  global $folder;
  if (!$file == "") {
    f_draw_maintop("Rename");
    echo "<form action=\"".$adminfile."?op=rename\" method=\"post\">\n"
        ."<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n"
        ."Renaming ".$folder.$file;

    echo "</table><br>\n"
        ."<input type=\"hidden\" name=\"rename\" value=\"".$file."\">\n"
        ."<input type=\"hidden\" name=\"folder\" value=\"".$folder."\">\n"
        ."New Name:<br><input class=\"text\" type=\"text\" size=\"20\" name=\"nrename\">\n"
        ."<input type=\"Submit\" value=\"Rename\" class=\"button\">\n";
    f_mainbottom();
  } else {
    home();
  }
}


/****************************************************************/
/* function renam()                                             */
/*                                                              */
/* Second step in rename.                                       */
/* Rename the specified file.                                   */
/* Recieves $rename from ren() as the old  filename.            */
/* Recieves $nrename from ren() as the new filename.            */
/****************************************************************/
function renam($rename, $nrename, $folder) {
  global $folder;$adminfile;
  if (!$rename == "") {
    f_draw_maintop("Rename");
    $loc1 = "$folder".$rename; 
    $loc2 = "$folder".$nrename;

    if(rename($loc1,$loc2)) {
      echo "The file ".$folder.$rename." has been changed to <a href=\"".$adminfile."?op=viewframe&file=".$nrename."&folder=$folder\">".$folder.$nrename."</a>\n";
    } else {
      echo "There was a problem renaming this file\n";
    }
    f_mainbottom();
  } else {
    home();
  }
}


/****************************************************************/
/* function f_listdir()                                           */
/*                                                              */
/* Recursivly lists directories and sub-directories.            */
/* Recieves $dir as the directory to scan through.              */
/****************************************************************/
function f_listdir($dir, $level_count = 0) {
  global $content;
    if (!@($thisdir = opendir($dir))) { return; }
    while ($item = readdir($thisdir) ) {
      if (is_dir("$dir/$item") && (substr("$item", 0, 1) != '.')) {
        f_listdir("$dir/$item", $level_count + 1);
      }
    }
    if ($level_count > 0) {
      $dir = ereg_replace("[/][/]", "/", $dir);
      if(substr(strtolower($dir),0,3) <> "ut_")
      {
          $content .= "<option value=\"".$dir."/\">".$dir."/</option>";
      }
    }
}


/****************************************************************/
/* function mov()                                               */
/*                                                              */
/* First step in move.                                          */
/* Prompts the user for destination path.                       */
/* Recieves $file and sends to move().                          */
/****************************************************************/
function mov($file) {
  global $folder, $content, $filefolder; $adminfile;
  if (!$file == "") {
    f_draw_maintop("Move");
    echo "<form action=\"".$adminfile."?op=move\" method=\"post\">\n"
        ."<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n"
        ."Move ".$folder.$file." to:\n"
        ."<select name=ndir size=1>\n"
        ."<option value=\"".$filefolder."\">".$filefolder."</option>";
    f_listdir($filefolder);
    echo $content
        ."</select>"
        ."</table><br><input type=\"hidden\" name=\"file\" value=\"".$file."\">\n"
        ."<input type=\"hidden\" name=\"folder\" value=\"".$folder."\">\n" 
        ."<input type=\"Submit\" value=\"Move\" class=\"button\">\n";
    f_mainbottom();
  } else {
    home();
  }
}


/****************************************************************/
/* function move()                                              */
/*                                                              */
/* Second step in move.                                         */
/* Moves the oldfile to the new one.                            */
/* Recieves $file and $ndir and creates $file.$ndir             */
/****************************************************************/
function move($file, $ndir, $folder) {
  global $folder;
  if (!$file == "") {
    f_draw_maintop("Move");
    if (rename($folder.$file, $ndir.$file)) {
      echo $folder.$file." has been succesfully moved to ".$ndir.$file;
    } else {
      echo "There was an error moving ".$folder.$file;
    }
    f_mainbottom();
  } else {
    home();
  }
}

function f_set_pwd1() {
    global $folder, $content, $filefolder;$adminfile;
        f_draw_maintop("Set Password");
        echo "<form action=\"".$adminfile."?op=setpwd2\" method=\"post\">\n"
            ."<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n"
            ."Set the password to gain access to administration<br>\n"
            ."New Password:<input type=\"text\" name=\"newpwd\" size=\"18\" border=\"0\" class=\"text\" value=\" \">\n";
        echo $content
            ."</select>"
            ."</table><br>"
            ."<input type=\"Submit\" value=\"Set\" class=\"button\">\n";
        f_mainbottom();
}

/****************************************************************/
function f_set_pwd2($newpwd) {
    global $folder;
    $s_settopwd = trim($newpwd);
    $fename = "ut_index_filemanager_password.txt";

//    echo "newpd=[]".$newpwd."[]".$s_settopwd."[]";
    if (!$s_settopwd == "") {
        f_draw_maintop("Setting new password");
        $loc = $folder.$fename;
        $fp = fopen($loc, "w");
        if(fwrite($fp, $s_settopwd)) {
            echo "The file <a href=\"".$adminfile."?op=viewframe&file=".$fename."&folder=".$folder."\">".$folder.$fename."</a> was succesfully updated\n";
            echo "<br>You are required to login again with the new password\n";
            $fp = null;
        } else {
            echo "There was a problem saving the password file []".$fename."[]\n";
        }
        f_mainbottom();
    } else {
        f_draw_maintop("Cannot set new password to blank");
        f_mainbottom();
    }
}

/****************************************************************/
/* function viewframe()                                         */
/*                                                              */
/* First step in viewframe.                                     */
/* Takes the specified file and displays it in a frame.         */
/* Recieves $file and sends it to viewtop                       */
/****************************************************************/
function viewframe($file) {
  global $sitetitle, $folder, $HTTP_HOST, $filefolder;  $adminfile;
  if ($filefolder == "/") {
    $error="**ERROR: You selected to view $file but your home directory is /.**";
    printerror($error);
    die();
  } elseif (ereg("/home/",$folder)) {
    $folderx = ereg_replace("$filefolder", "", $folder);
    $folder = "http://".$HTTP_HOST."/".$folderx;
  }
  echo "<html>\n"
      ."<head>\n"
      ."<title>$sitetitle :: Viewing file - $file</title>\n"
      ."</head>\n\n"

      ."<frameset rows=\"85,*\" frameborder=\"no\">\n"
      ."<frame name=\"top\" noresize src=\"".$adminfile."?op=viewtop&file=$file\" scrolling=\"no\">\n"
      ."<frame name=\"content\" noresize src=\"".$folder.$file."\">\n"
      ."</frameset>\n\n"

      ."<body>\n"
      ."</body>\n"
      ."</html>\n";
}


/****************************************************************/
/* function viewtop()                                           */
/*                                                              */
/* Second step in viewframe.                                    */
/* Controls the top bar on the viewframe.                       */
/* Recieves $file from viewtop.                                 */
/****************************************************************/
function viewtop($file) {
  global $viewing, $iftop;
  $viewing = "yes";
  $iftop = "target=_top";
  f_draw_maintop("Viewing file - $file");
}


/****************************************************************/
/* function logout()                                            */
/*                                                              */
/* Logs the user out and kills cookies                          */
/****************************************************************/
function logout() {
  global $login; $adminfile;
  setcookie("user","",time()-60*60*24*1);
  setcookie("pass","",time()-60*60*24*1);

  f_draw_maintop("Logout",false);
  echo "Your are now logged out."
      ."<br><br>"
      ."<a href=".$adminfile."?op=home>Click here to Log in again</a>";
  f_mainbottom();
}


/****************************************************************/
/* function f_mainbottom()                                        */
/*                                                              */
/* Controls the bottom copyright.                               */
/****************************************************************/
function f_mainbottom() {
  echo "</table></table>\n"
      ."<table width=100%><tr><td align=right><font class=copyright>Copyright &copy 2003 - ".date('Y')." thanks to  <a href=http://www.osfilemanager.com>osFileManager</a>, <a href=http://www.arzy.net>Arzy, LLC</a></font></table>\n"
      ."</table></table></body>\n"
      ."</html>\n";
  exit;
}


/****************************************************************/
/* function printerror()                                        */
/*                                                              */
/* Prints error onto screen                                     */
/* Recieves $error and prints it.                               */
/****************************************************************/
function printerror($error) {
  f_draw_maintop("ERROR");
  echo "<font class=error>\n".$error."\n</font>";
  f_mainbottom();
}


/****************************************************************/
/* function switch()                                            */
/*                                                              */
/* Switches functions.                                          */
/* Recieves $op() and switches to it                            *.
/****************************************************************/
switch($op) {

    case "home":
	home();
	break;

    case "up":
	up();
	break;

    case "upload":
	upload($_FILES['upfile'], $_REQUEST['ndir']);
	break;

    case "del":
	del($_REQUEST['dename']);
	break;

    case "delete":
	delete($_REQUEST['dename']);
	break;

    case "edit":
	edit($_REQUEST['fename']);
	break;

    case "save":
	f_save($_REQUEST['ncontent'], $_REQUEST['fename']);
	break;

    case "setpwd":
        f_set_pwd1();
        break;

    case "setpwd2":
        f_set_pwd2($_REQUEST['newpwd']);
        break;

    case "cr":
	cr();
	break;

    case "create":
	create($_REQUEST['nfname'], $_REQUEST['isfolder'], $_REQUEST['ndir']);
	break;

    case "ren":
	ren($_REQUEST['file']);
	break;

    case "rename":
	renam($_REQUEST['rename'], $_REQUEST['nrename'], $folder);
	break;

    case "mov":
	mov($_REQUEST['file']);
	break;

    case "move":
	move($_REQUEST['file'], $_REQUEST['ndir'], $folder);
	break;

    case "viewframe":
	viewframe($_REQUEST['file']);
	break;

    case "viewtop":
	viewtop($_REQUEST['file']);
	break;

    case "printerror":
	printerror($error);
	break;

    case "logout":
	logout();
	break;

    default:
	home();
	break;
}
?>