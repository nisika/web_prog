<?php
function pf_customError($errno,$errstr,$errfile,$errline,$errcontext)
  {
// NOTE **** THIS ONLY CATCHES NON-FATAL ERRORS
//  echo "<b>Error gw12:</b> [$errno] $errstr $errfile $errline $errcontext.<br>";
        switch ($errno) {
        case E_USER_ERROR:
            echo "<b>My ERROR</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            echo "  Fatal error on line $errline in file $errfile";
            echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
            echo "Aborting...<br />\n";
            exit(1);
            break;

        case E_USER_WARNING:
            echo "<b>My WARNING</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        case E_USER_NOTICE:
            echo "<b>My NOTICE</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        default:
            echo "<BR>Unknown error type: [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;
        }

    /* Don't execute PHP internal error handler */
    return true;

  }


$ko_map_path="";

A000_SET_RUN:
     //set error handler
    set_error_handler("pf_customError", E_ALL);
    date_default_timezone_set('Australia/Brisbane');
    session_start();

    if (isset($_SESSION['ko_prog_path']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
    }else{
    $_SESSION['ko_prog_path'] = "";
    }
    if (isset($_SESSION['ko_dbase_to_connectto']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
    }else{
    $_SESSION['ko_dbase_to_connectto'] = "tdxpryda";
    }

    if (isset($_SESSION['ko_map_path']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
        $ko_map_path = $_SESSION['ko_map_path'];
    }else{
    $_SESSION['ko_map_path'] = "";
    }

A000_DEFINE_VARIABLES:
    $sys_prog_name = "";
    $sys_debug = '';
    $s_file_exists = '';
    $s_filename="";
    $get_map = "";
    $tmp_array = array();
    $sys_function_out = "";
    $s_post_action = "";

    $s_sessionno = "";
    $s_helplocation = "";
    $function_id = "";
    $get_userid = "";
    $s_details_def="";
    $s_details_data="";
    $s_siteparams = "";
    $s_url_siteparams = "";
    $s_usr_style_list = "";
    $s_usr_style_values= "";
    $username = "";
    $s_inaction ="";
    $s_temp = "";
    $results = "";
    $s_MultiLevelloop1 = "";
    $s_MultiLevelloop2 = "";
    $s_MultiLevelloop3 = "";
    $s_no_action_in_file = "";

    $sys_function_name = "";
    $sys_function_name = "main part of ut_action.php ";

A200_LOAD_LIBRARIES:
    $sys_debug = strtoupper("NO");

     IF ($sys_debug == "YES"){echo $sys_prog_name." started debug=".$sys_debug."<br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_sql.php');
     $class_sql = new wp_SqlClient();
     IF ($sys_debug == "YES"){echo $sys_prog_name." after class_sql<br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_main.php');
     $class_main = new clmain();
     IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_main <br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_myplib1.php5');
     $class_myplib1 = new myplib1();
     IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_myplib1<br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_email.php');
     $class_email = new sys_clemail();
     IF ($sys_debug == "YES"){echo  $sys_prog_name." after clemail<br>";};

     require_once($_SESSION['ko_prog_path'].'lib/class_process_jobline.php');
     $class_clprocessjobline = new clprocessjobline();
     IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_process <br>";};

//     require_once($_SESSION['ko_map_path'].'lib/class_app.php');
//     $class_apps = new AppClass();
//     IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_appclass<br>";};

     IF ($sys_debug == "YES"){echo "DEBUG IS ACTIVE - CHECK THE SCREEN SOURCE FOR FULL DETAILS or you will spend ages looking for stuff that is hidden<br><br>";};
     IF ($sys_debug == "YES"){echo "_get=";print_r($_GET);echo "<br>";};
     IF ($sys_debug == "YES"){echo "_post=";print_r($_POST);echo "<br>";};

A300_CONNECT_TODBASE:

     $s_out = "";
     $s_out = $s_out."<br>ut_index_update_datablobs";
    $s_user = "root";
    $s_password = "tdxadmin10";
    $s_server = "localhost:3306";
    $s_dbase = "udelivered";


A400_GET_PARAMETERS:
     $get_table = "not set";
     $s_get_filter = "not set";
     $s_keyfield = "not set";
     $get_runtype = "not set";

     if (isset($_GET['runtype'])) $get_runtype = $_GET["runtype"];
     if (isset($_GET['table'])) $get_table = $_GET["table"];
     if (isset($_GET['filter'])) $s_get_filter = $_GET["filter"];
     if (isset($_GET['keyfield'])) $s_keyfield = $_GET["keyfield"];
     if (isset($_GET['user'])) $s_user = $_GET["user"];
     if (isset($_GET['password'])) $s_password = $_GET["password"];
     if (isset($_GET['server'])) $s_server = $_GET["server"];
     if (isset($_GET['dbase'])) $s_dbase = $_GET["dbase"];

     if ($get_runtype == "not set")
     {
         $s_out = $s_out."<br>get_runtype=".$get_runtype;
         goto X900_EXIT;
     }
     if ($get_table == "not set")
     {
         $s_out = $s_out."<br>get table=".$get_table;
         goto X900_EXIT;
     }
     if ($s_get_filter == "not set")
     {
         $s_out = $s_out."<br>s_get_filter=".$s_get_filter;
         goto X900_EXIT;
     }
     if ($s_keyfield == "not set")
     {
         $s_out = $s_out."<br>s_keyfield=".$s_keyfield;
         goto X900_EXIT;
     }

    $_SESSION['ko_dbase_userid'] = $s_user;
    $_SESSION['ko_dbase_password'] = $s_password;
    $_SESSION['ko_dbase_server'] = $s_server ;
    $_SESSION['ko_dbase_to_connectto'] = $s_dbase;


     $s_get_filter = str_replace("\\'","'",$s_get_filter);
     $s_out = $s_out."<br>get table=".$get_table;
     $s_out = $s_out."<br>s_get_filter=".$s_get_filter;
     $s_out = $s_out."<br>s_keyfield=".$s_keyfield;
     $s_out = $s_out."<br>s_user=".$s_user;
     $s_out = $s_out."<br>s_password=".$s_password;
     $s_out = $s_out."<br>s_server=".$s_server;
     $s_out = $s_out."<br>s_dbase=".$s_dbase;

     echo $s_out;
     $dbcnx = $class_sql->c_sqlclient_connect();

A500_SET_VALUES:
    IF ($sys_debug == "YES"){echo $sys_prog_name." after file exists check s_map_file_exists=".$s_file_exists."<br>";};
    IF ($sys_debug == "YES"){echo $sys_prog_name." number of lines in the file(array)=".count($array_lines)."<br>";};

C000_DO:
    $s_sql = "SELECT * from ".$get_table." where ".$s_get_filter."";
    $i_record_count = "0";
C000_RECORD_LOOP:
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_rec_found = "NO";
    $s_numrows = mysql_num_rows($result);
    if ($s_numrows == 0)
    {
        goto C900_END_RECORDS;
    }
C100_GET_RECORDS:
    if ($i_record_count > $s_numrows) {
        goto C900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        goto C900_END_RECORDS;
    }
    $s_rec_found = "YES";
C150_DO_RECORDS:
    $s_key_value = $row[$s_keyfield];

     $s_out = $s_out."<br>".$i_record_count."  update record with key =".$s_keyfield." value =".$s_key_value."";

    $s_details_def="";
    $s_details_data = "";
     if ($get_runtype == "update")
     {
        $s_result = $class_main->clmain_u705_set_db_recfields_from_dblob("UPDATE",$get_table,$s_keyfield,$s_key_value,"","NO","ut_index_update_datablobs",$dbcnx,"NO",$s_details_def,$s_details_data,$s_sessionno);
        $s_out = $s_out." - updated result = ".$s_result." ";
     }else{
        $s_out = $s_out." - checking and reporting only ";
     }
C158_NEXT:
    $i_record_count = $i_record_count + 1;
    GOTO C100_GET_RECORDS;
C159_END:
C900_END_RECORDS:

X900_EXIT:
    echo $s_out;
?>
