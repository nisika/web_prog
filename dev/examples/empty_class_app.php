<?php

    class AppClass
    {
function clapp_a100_do_app_class($ps_function_name,$ps_function_params,$ps_action,$p_dbcnx,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno)
{
A100_TEMPLATE_INIT:
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clapp_a100_do_app_class  called from ".$ps_calledfrom;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text." ".$sys_function_name." ps_function_name = ".$ps_function_name." ps_function_params = ".$ps_function_params." ps_action = ".$ps_action." ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ");};

A199_END_TEMPLATE_INIT:
    $s_function_name = strtoupper(trim($ps_function_name," "));
    $s_function_found = "NO";
    $s_function_list = "APP FUNCTION LIST: ";
    $s_function_help = "APP FUNCTION HELP: ";
    $ar_params = array();
    $s_do_this_function = "";

    $s_action = strtoupper(trim($ps_action));
    if ($s_action == "LIST_FUNCTIONS")
    {
        goto B100_DO_FUNCTION;
    }
    if ($s_action == "FUNCTIONS_HELP")
    {
        goto B100_DO_FUNCTION;
    }
    if ($s_action == "RUN_FUNCTION")
    {
        goto B100_DO_FUNCTION;
    }
    $sys_function_out = "Error Unknown action of ".$ps_action." expecting LIST_FUNCTIONS,FUNCTIONS_HELP or RUN_FUNCTION";
    GOTO Z900_EXIT;
B100_DO_FUNCTION:
    IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text." DO FUNCTION s_action=".$s_action." "." s_function_name =".$s_function_name." ps_function_params=".$ps_function_params);};

// gw 20100320      $ar_params = explode("^",$ps_function_params);
    $ar_params = explode("^PARAM^",$ps_function_params);

// ******************************************************************************************************************
B110_A200__securityoption:
    $s_do_this_function = "clapp_A200_securityoption";
    $s_function_list = $s_function_list.$s_do_this_function.", ";
    $s_function_help = $s_function_help.'clapp_A200_securityoption(option_to_return) = return the selection securityoption value '.",<br>  ";

    if ($s_function_name != strtoupper(trim($s_do_this_function)))
    {
        GOTO B110_A200__END;
    }
    if (count($ar_params) <> 5 )
    {
       echo "<br>Not Debug - Error ".$sys_function_name." the number of parameters is ".count($ar_params)." which is wrong for ".$s_do_this_function."<br>".$s_function_help."<br> parameters supplied=".$ps_function_params;
    }
    if ($s_action == "RUN_FUNCTION")
    {
        $sys_function_out =  $this->clapp_A200_securityoption($ps_details_def,$ps_details_data,$ps_sessionno,$p_dbcnx,$ar_params[0]);
    }
    $s_function_found = "YES";
    GOTO X900_EXIT;
B110_A200__END:
// ******************************************************************************************************************

// ******************************************************************************************************************
B110_CRYPTION:
    $s_do_this_function = "clapp_b100_cryption";
    $s_function_list = $s_function_list.$s_do_this_function.", ";
    $s_function_help = $s_function_help."clapp_b100_cryption(action,value) = depending on the value of action will decrypt or encrypt the value".",<br>  ";

    if ($s_function_name != strtoupper(trim($s_do_this_function)))
    {
        GOTO B110_END;
    }
    if (count($ar_params) <> 2 )
    {
       echo "<br>Not Debug - Error ".$sys_function_name." the number of parameters is ".count($ar_params)." which is wrong for ".$s_do_this_function."<br>".$s_function_help."<br> parameters supplied=".$ps_function_params;
    }


    if ($s_action == "RUN_FUNCTION")
    {
        $sys_function_out =  $this->clapp_b100_cryption($ar_params[0],$ar_params[1]);
    }
    $s_function_found = "YES";
    GOTO X900_EXIT;
B110_END:
// ******************************************************************************************************************
B120_UHEAD_HTML:
    $s_do_this_function = "clapp_s100_userheading_html";
    $s_function_list = $s_function_list.$s_do_this_function.", ";
    $s_function_help = $s_function_help."clapp_s100_userheading_html(mypalletid,dbase connection) = will make the heading for the pallet id".",<br> ";

    if ($s_function_name != strtoupper(trim($s_do_this_function)))
    {
        GOTO B120_END;
    }
    if ($s_action == "RUN_FUNCTION")
    {
        $sys_function_out =  $this->clapp_s100_userheading_html($ar_params[0],$p_dbcnx);
//        $sys_function_out = "result from clapp_b100_cryption()";
    }
    $s_function_found = "YES";
    GOTO X900_EXIT;
B120_END:
// ******************************************************************************************************************
B130_ADVERTS:
    $s_do_this_function = "clapp_s200_advert_url";
    $s_function_list = $s_function_list.$s_do_this_function.", ";
    $s_function_help = $s_function_help."clapp_s200_advert_url(add location,url or image) = gets the url or the image for the required add location".",<br> ";

    if ($s_function_name != strtoupper(trim($s_do_this_function)))
    {
        GOTO B130_END;
    }
    if ($s_action == "RUN_FUNCTION")
    {
        $sys_function_out =  $this->clapp_s200_advert_url($ar_params[0],$ar_params[1],$sys_debug);
//        $sys_function_out = "result from clapp_b100_cryption()";
    }
    $s_function_found = "YES";
    GOTO X900_EXIT;
B130_END:
// ******************************************************************************************************************
B140_HELP_HTML:
    $s_do_this_function = "clapp_s110_help_html";
    $s_function_list = $s_function_list.$s_do_this_function.", ";
    $s_function_help = $s_function_help."clapp_s110_help_html(helplocation,heading,mypalletid) = builds the help header with icon".",<br> ";

    if ($s_function_name != strtoupper(trim($s_do_this_function)))
    {
        GOTO B140_END;
    }
    if ($s_action == "RUN_FUNCTION")
    {
        $sys_function_out =  $this->clapp_s110_help_html($ar_params[0],$ar_params[1],$ar_params[2],$sys_debug);
//        $sys_function_out = "result from clapp_b100_cryption()";
    }
    $s_function_found = "YES";
    GOTO X900_EXIT;
B140_END:
// ******************************************************************************************************************
B150_DO:
    $s_do_this_function = "clapp_s300_summary_history";
    $s_function_list = $s_function_list.$s_do_this_function.", ";
    $s_function_help = $s_function_help."clapp_s300_summary_history(ps_mypalletid,ps_mypartnerid,debug,p_dbcnx) = builds the transaction summary ".",<br> ";

    if ($s_function_name != strtoupper(trim($s_do_this_function)))
    {
        GOTO B150_END;
    }
    if ($s_action == "RUN_FUNCTION")
    {
        $sys_function_out =  $this->clapp_s300_summary_history($ar_params[0],$ar_params[1],$sys_debug,$p_dbcnx);
//        $sys_function_out = "result from clapp_b100_cryption()";
    }
    $s_function_found = "YES";
    GOTO X900_EXIT;
B150_END:
// ******************************************************************************************************************


X900_EXIT:
    if ($s_action == "LIST_FUNCTIONS")
    {
        $sys_function_out = $s_function_list;
        GOTO Z900_EXIT;
    }
    if ($s_action == "FUNCTIONS_HELP")
    {
        $sys_function_out = $s_function_help;
        GOTO Z900_EXIT;
    }

    IF ($s_function_found == "NO")
    {
       $sys_function_out = "Error Unknown function of ".$s_function_name." expecting one of ".$s_function_list;
    }

/*EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$array_line_details[$i2]." ";};
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name." p_username = ".$p_username." p_pwd = ".$p_pwd."  ps_debug = ".$ps_debug." ");};

    IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ");};

    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ";};
    IF ($sys_debug == "YES"){$sys_function_out.=" <!--".$sys_debug_text."-->";};
*/
Z900_EXIT:
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  z900_EXIT sys_function_out =  ".$sys_function_out);};
    return $sys_function_out;

}

function clapp_A200_securityoption($ps_details_def,$ps_details_data,$ps_sessionno,$p_dbcnx,$ps_option_to_return)
{

// GW20110130 - NOT WORKING - SEE ORDWATCH FOR UPDATE
    $s_field_value = "&nbsp";
    require_once($_SESSION['ko_prog_path'].'lib/class_main.php');
    $class_main = new clmain();

   $s_field_value = $class_main->clmain_u610_pdf_to_view_html($ps_details_def,"no");

    return $s_field_value;

}



function clapp_b100_cryption($ps_act,$ps_value)
{
    $s_value = "";

    $s_value = $ps_value ;
    // w20090524 - need to make the encyption calc much better but this is a start
    if (trim(strtoupper($ps_act))=="ENCRYPT")
    {
        $s_value = "M".$ps_value."X";
        $s_value = "";
        $s_value .= $this->clapp_b110_cryption_char(substr($ps_value,0,1));
        $s_value .= $this->clapp_b110_cryption_char(substr($ps_value,1,1));
        $s_value .= $this->clapp_b110_cryption_char(substr($ps_value,2,1));
        $s_value .= $this->clapp_b110_cryption_char(substr($ps_value,3,1));
        $s_value .= $this->clapp_b110_cryption_char(substr($ps_value,4,1));
        $s_value .= substr($ps_value,5);
    }

    if (trim(strtoupper($ps_act))=="DECRYPT")
    {
//gw - 20100302        $s_value = substr($ps_value,1);    //removes frist char//
//        $s_value = substr($s_value,0,-1); //removes last char//

        if (is_numeric(substr($ps_value,0,1)))
           {
            $s_value = $ps_value;
           }else{
            $s_value = "";
            $s_value .= $this->clapp_b120_decryption_char(substr($ps_value,0,1));
            $s_value .= $this->clapp_b120_decryption_char(substr($ps_value,1,1));
            $s_value .= $this->clapp_b120_decryption_char(substr($ps_value,2,1));
            $s_value .= $this->clapp_b120_decryption_char(substr($ps_value,3,1));
            $s_value .= $this->clapp_b120_decryption_char(substr($ps_value,4,1));
            $s_value .= substr($ps_value,5);
           }


    }
    return $s_value;

}
function clapp_b110_cryption_char($ps_char)
{
    $s_out = "";
    if ($ps_char == "0"){$s_out = "Q";};
    if ($ps_char == "1"){$s_out = "G";};
    if ($ps_char == "2"){$s_out = "C";};
    if ($ps_char == "3"){$s_out = "K";};
    if ($ps_char == "4"){$s_out = "E";};
    if ($ps_char == "5"){$s_out = "L";};
    if ($ps_char == "6"){$s_out = "S";};
    if ($ps_char == "7"){$s_out = "H";};
    if ($ps_char == "8"){$s_out = "Y";};
    if ($ps_char == "9"){$s_out = "U";};

    return $s_out;
}
function clapp_b120_decryption_char($ps_char)
{
    $s_out = "";
    if ($ps_char == "Q"){$s_out = "0";};
    if ($ps_char == "G"){$s_out = "1";};
    if ($ps_char == "C"){$s_out = "2";};
    if ($ps_char == "K"){$s_out = "3";};
    if ($ps_char == "E"){$s_out = "4";};
    if ($ps_char == "L"){$s_out = "5";};
    if ($ps_char == "S"){$s_out = "6";};
    if ($ps_char == "H"){$s_out = "7";};
    if ($ps_char == "Y"){$s_out = "8";};
    if ($ps_char == "U"){$s_out = "9";};

    return $s_out;
}
function clapp_s100_userheading_html($ps_mypalletid,$p_dbcnx)
{
    $ssql = "SELECT * from usermaster where mypalletid = '".$ps_mypalletid."' ";

    $s_lineout = "";
//     ECHO "1";
    $s_mypalletdefndetails = $this->clapp_data100_get_arecord_sql_defndata($ssql,$p_dbcnx,"no");
    $s_username = $this->clapp_util110_get_field_defndata("myusername",$s_mypalletdefndetails,"","NO","");
    $s_company =   $this->clapp_util110_get_field_defndata("mycompany",$s_mypalletdefndetails,"","NO","");
    $s_lastaccess = "";
    $s_mypalletid = $ps_mypalletid;
    $s_ipaddress =  $this->clapp_util130__getRealIpAddr();
    $s_lineout .="<tr id='mylib_userheading'><td scope='col' align='left' class='pm_head_small'>Hi ".$s_username." of ".$s_company.", ID:".$s_mypalletid." (ip:".$s_ipaddress.")";
//    $s_lineout .="<tr><td scope='col' align='left' class='pm_head_small'>Hi ".$s_username." of ".$s_company."</td></tr>";
//    $s_lineout .="<tr><td align='left' class='pm_head_small'>last access ".$s_lastaccess."</td></tr>";
//    $s_lineout .="<tr><td align='left' class='pm_head_small'>Your id:".$s_mypalletid." (ip:".$s_ipaddress.")</td></tr>";
    $s_lineout .="<input name='mypalletid' type='hidden' id='mypalletid' value=".$s_mypalletid." /></td></tr>";
    return $s_lineout;

}
function clapp_s110_help_html($ps_helplocation,$ps_heading,$ps_mypalletid)
{
    $s_lineout = "";
    $s_lineout .='<table><tr>';
    $s_lineout .='<td valign="top" class="pm_head" height="10">'.$ps_heading.'</td>';
    $s_lineout .= "<td height='10'><a href=\"javascript:;\" onclick=\"getHelp('".$ps_helplocation."','".$ps_mypalletid."')\"><img src='|%!start_SESSION_SUPER_ko_ut_home_url_!end%|images/q_12.gif' height='12' width='12' border='0'></a></td>";
    $s_lineout .='</tr>';
    $s_lineout .='</table>';

    return $s_lineout;

}
function clapp_s200_advert_url($ps_location,$ps_what,$ps_debug)
{
    $s_debug = strtoupper(trim($ps_debug, " " ));
   $s_image = "";
    $s_location = strtoupper(trim($ps_location));
    $s_url = $s_location; //"http://delnet.com.au";
    $s_result = "";
    // goto dbase addvert table and get the url and image for the addvert
    if ($s_location == "MYB_SCREEN_MIDBOTTOM")
    {$s_url = "http://mypalletmanager.com";
      $s_image = "images/addmidb.png";
      $s_result = "GOOD";}
    if ($s_location == "MYB_SCREEN_TOPRIGHT")
    {$s_url = "http://mypalletpartner.com";
      $s_image = "images/addright.png";
      $s_result = "GOOD";}
    if ($s_location == "MYB_SCREEN_TOPLEFT")
    {$s_url = "http://mypalletaccount.com";
      $s_image = "images/addleft.png";
      $s_result = "GOOD";}

    IF ($s_result !== "GOOD")
    {
     $sys_function_out = "NOT GOOD".$ps_location.$ps_what;
     goto Z900_EXIT;
    }


    if (strtoupper(trim($ps_what)) == "URL")
        {$sys_function_out = $s_url;}
    if (strtoupper(trim($ps_what)) == "IMAGE")
        {$sys_function_out = $s_image;}

Z900_EXIT:
    if ($s_debug == "YES")
    {
        echo "clapp s200 out   ".$sys_function_out."<BR>";
    }
    return $sys_function_out;
}
function clapp_s300_summary_history($ps_mypalletid,$ps_mypartnerid,$ps_debug,$p_dbcnx)
{
    // gw 20100324 - not tested

require_once($_SESSION['ko_map_path'].'lib/class_myplib1.php5');
require_once($_SESSION['ko_map_path'].'lib/class_buddy.php');

$objbuddy = new clbuddy();
$objlib1 = new myplib1();

    $s_lineout = "";
    $s_security = "";
    $s_history_line = "";

    IF ($ps_debug =="YES"){ECHO "CLAP_S300 DOING DEBUG - VIEW SOURCE ";}
//    $s_security = " ( and mypalletid = ".$ps_mypalletid." or partnermypalletid  = ".$ps_mypalletid.")"
//    $ssql = "SELECT * from activity where mypalletid = '".$ps_mypalletid."' ".$s_security."  and partnerid = '".$ps_mypartnerid."'  ORDER BY CREATEDNT DESC LIMIT 5 OFFSET 0";
    $ssql = "SELECT * from activity where mypalletid = '".$ps_mypalletid."' ".$s_security."  and partnerid = '".$ps_mypartnerid."'  ORDER BY CREATEDNT DESC LIMIT 5 OFFSET 0";
    IF ($ps_debug =="YES"){ECHO "CLAP_S300 DOING DEBUG - sql  ".$ssql;}
    $rs_temp = mysql_query($ssql,$p_dbcnx);
     if (!$rs_temp)
        {echo("<P>Error performing query: ".mysql_error()." sql = ".$ssql."</P>");
         exit();
         }
    $rs_temp_row_cnt = mysql_num_rows($rs_temp);
    while ( $row = mysql_fetch_array($rs_temp) )
    {
        IF ($ps_debug =="YES"){ECHO "  CLAP_S300 DOING DEBUG - doing a line  <br>";}
        $s_partnername =  $objbuddy->f_clbuddy_set_partnerid_name($row["partnerid"],$p_dbcnx);
        $s_typename = $objlib1->f_myplib1_set_type_name($row["itemtype"]);
        $s_activityname = $objlib1->f_myplib1_set_activity_name($row["activity"]);
        $s_datedisplay = $objlib1->f_myplib1_set_date_display($row["activitycymd"]);
        $s_history_line = $s_partnername." - ".$s_typename." - ".$s_activityname." - ".$row["quantity"]." - ".$s_datedisplay;

        $s_lineout .="<tr>";
        $s_lineout .="<td class='pm_data'>".$s_history_line."</td>";
        $s_lineout .="</tr>";
    }

    IF ($ps_debug =="YES"){ECHO "  CLAP_S300 DOING DEBUG - s_lineout  ".$s_lineout;}
    return $s_lineout;
}

function clapp_data100_get_arecord_sql_defndata($ps_sql,$ps_dbcnx,$ps_debug)
{
  // ************  do not use for any sql that will return more that 1 record - it will go ape shit  gw20090505
    $ssql = $ps_sql;
    if (trim(strtoupper($ps_debug)) == "YES")
        {echo "<br> debug:::::arecord_sql sql=".$ssql."<br>";
         echo "dbcnx=[".$ps_dbcnx."]END OF DBCNX DEBUG<br>";
         };
    $rs_temp = mysql_query($ssql,$ps_dbcnx);
    if (!$rs_temp)
    {
        echo("<P>clapp_data100_get_arecord_sql_defndata  Error performing query: ".mysql_error()." sql = ".$ssql."</P>");
        exit();
    }

    $s_fieldnames = "START|";
    $s_fielddata = "START|";
    while ( $row = mysql_fetch_array($rs_temp) )
    {
     foreach ( array_keys($row) AS $header )
        {
                //you have integer keys as well as string keys because of the way PHP  handles arrays.
                if ( !is_int($header) )
                {
                    $s_fieldnames .=$header."|";
                }
        }
        //print the data row
        foreach ( $row AS $key=>$value )
        {
            if ( !is_int($key) )
            {
                $s_fielddata .=$value."|";
            }
        }
    }
    $s_fieldnames .= "END";
    $s_fielddata .= "END";

    return $s_fieldnames.'||DEF||'.$s_fielddata;
}
function clapp_util110_get_field_defndata($ps_fieldname,$ps_def,$ps_data,$ps_debug,$ps_default)
{
//EG    $s_test = myplib1::f_myplib1_get_field_defndata("type_description",$s_type_def,$s_type_data,"NO");
//    echo $s_test;
    $s_outdata = "?";
    IF (strpos($ps_def,"||DEF||") === FALSE)
    {
        $sa_defs = explode("|",$ps_def);
        $sa_data = explode("|",$ps_data);
    }ELSE{
// GW20090504 split the one field around the ||def|| marker and load results to the def &  data fields
        $sa_defs = explode("||DEF||",$ps_def);
        $sa_data = $sa_defs[1];
        $sa_defs = $sa_defs[0];
        $sa_defs = explode("|",$sa_defs);
        $sa_data = explode("|",$sa_data);
    }
    for ($i = 0; $i < count($sa_defs);$i++)
    {
//        echo $i." count ".count($sa_defs)."field=".strtoupper(trim($ps_fieldname))." def=".(strtoupper($sa_defs[$i]))."<br>";

        if (strtoupper(trim($ps_fieldname)) == (strtoupper($sa_defs[$i])))
        {
            $s_outdata = $sa_data[$i];
        }
        if (strtoupper(trim($ps_debug)) == "YES")
        {
            echo "debug:".strtoupper(trim($ps_fieldname))." ".$i." def=".(strtoupper($sa_defs[$i])) ." data=".(strtoupper($sa_data[$i]))."<br>";
        }
    }

    if($s_outdata =="?")
    {
     if (trim($ps_default) !== '')
     {
        $s_outdata = $ps_default;
     }
     if (trim($ps_default) == 'BLANK')
     {
        $s_outdata = "";
     }
    }
            return $s_outdata;
}
function clapp_util130__getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

// writes the log file for this class
function z901_dump($ps_text)
{
    $file_path  = "";
    if (isset($_SESSION['ko_map_path']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
        $file_path = $_SESSION['ko_map_path'];
    }

    $file_path = $file_path."logs";
    if (!file_exists($file_path))
    {
        $md= mkdir($file_path);
    }

    $dateym = date('Ym');
    $myfile = $file_path."\Log_clapp_".$dateym.".txt";
    $fh = fopen($myfile,'a') or die("cant open file".$myfile);
    fwrite($fh,date("Y-m-d H:i:s",time())." - ".$ps_text."\r\n");
    fclose($fh);
}


function f_myplib1_set_type_name ($ps_itemtype)
{
    $s_name = $ps_itemtype;
// go to the itemstypes table and get the name for the type
    return $s_name;
}

function f_myplib1_set_activity_name($ps_activity)
{
    $s_activityname = "";
    $s_activityname = $ps_activity;
    $s_activity = strtolower(trim($ps_activity));

    if ($s_activity =="givetothem")
    {$s_activityname = "Given to partner"; };
    if ($s_activity =="receievedfromthem")
    {$s_activityname = "Received from partner";};
    if ($s_activity =="exchange")
    {$s_activityname = "Exchanged with partner";};
    if ($s_activity =="transferto")
    {$s_activityname = "Transfer to Partner";};
    if ($s_activity =="transferfrom")
    {$s_activityname = "Transfer from Partner";};
    if ($s_activity =="hire")
    {$s_activityname = "Hire from supplier";};
    if ($s_activity =="dehire")
    {$s_activityname = "Dehire from supplier";};
    if ($s_activity =="reqdrop")
    {$s_activityname = "Request Pallet Drop";};
    if ($s_activity =="adjustup")
    {$s_activityname = "Increase stock";};
    if ($s_activity =="adjustdown")
    {$s_activityname = "Decrease stock";};

    return $s_activityname;
}
function f_myplib1_set_date_display($ps_datecymd)
{
require_once('lib/class_myplib1.php5');
$objlib1 = new myplib1();

    $s_date_display = "";
//get the format based on the mypalletid country  - format the date accordingly
    $s_date_display = $ps_datecymd;
    $s_date_display = substr($ps_datecymd,6,2)."/".$objlib1->f_myplib1_set_mm_to_month(substr($ps_datecymd,4,2),"SHORT")."/".substr($ps_datecymd,0,4) ;
    return $s_date_display;
}

    }

?>
