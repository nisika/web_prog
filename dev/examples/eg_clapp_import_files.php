<?php

    class clapp_import_files
    {

function clapp_fp_file_process($ps_dbcnx,$ps_debug, $ps_calledfrom,$ps_filename, $ps_filefmatfunction)
{
A100_TEMPLATE_INIT:
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clapp_fp_file_process  called from ".$ps_calledfrom;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text." ".$sys_function_name." ps_function_name = ".$ps_function_name." ps_debug = ".$ps_debug." ");};

A199_END_TEMPLATE_INIT:
    $sys_function_out = "";
    $sys_function_out = $sys_function_out."<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; doing clapp_fp_file_process for file ".$ps_filename." fmatfunction=".$ps_filefmatfunction ;
    ECHO $sys_function_out;

    if (strtoupper($ps_filefmatfunction) == "FP1215_IMPORT_JOBS")
    {
        $stmp = $this->fp1215_import_jobs($ps_dbcnx,$sys_debug,"clapp_fp_file_process b100",$ps_filename);
    }



Z900_EXIT:
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  z900_EXIT sys_function_out =  ".$sys_function_out);};
    return $sys_function_out;
}

function fp1215_import_jobs($ps_dbcnx,$ps_debug, $ps_calledfrom,$ps_filename)
//$ps_dbcnx,$ps_debug,$ps_filename)
{
A100_TEMPLATE_INIT:
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - fp1215_import_jobs  called from ".$ps_calledfrom;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text." ".$sys_function_name." ps_function_name = ".$ps_function_name." ps_function_params = ".$ps_function_params." ps_action = ".$ps_action." ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ");};

A199_END_TEMPLATE_INIT:

    $sys_function_out = "";
    $sys_function_out = $sys_function_out."<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; doing fp1215_import_jobs for file .".$ps_filename."";
    ECHO $sys_function_out;


Z900_EXIT:
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  z900_EXIT sys_function_out =  ".$sys_function_out);};
    return $sys_function_out;
}


    // end of class
    }

    /*
function pf_c010_do_pp_stock_scans($ps_dbcnx,$ps_filename,$ps_debug)
{
    global $class_main;
    global $class_sql;

    $dbcnx = $ps_dbcnx;
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "<br>debug - pf_c010_do_pp_stock_scans - from=".$sys_debug_text;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>DEBUG VIEW SOURCE FOR DETAILS#######################################################<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_filename = ".$ps_filename." ps_debug = ".$ps_debug." ";};

    $s_file_work = $ps_filename;
    $s_file_exists='Y';
    if(file_exists($s_file_work))
    {
        $array_lines = file($s_file_work);
    }
    else
    {
        $s_file_exists='N';
        $sys_function_out =  "<br>".$sys_function_name." ##### File Not Found-:".$s_file_work."<br>";
        GOTO Z900_EXIT;
    }

    IF ($sys_debug == "YES"){echo $sys_function_name." after file exists check s_file_exists=".$s_file_exists."<br>";};
    IF ($sys_debug == "YES"){echo $sys_function_name." number of lines in the file(array)=".count($array_lines)."<br>";};

    $i = 0;
    $s_line_in = "";

B100_GET_REC:
    IF ($i >= count($array_lines))
    {
        goto Z900_EXIT;
    }

    $s_line_in = $array_lines[$i];
    IF (substr($s_line_in,0,1) == "*")
    {
        goto Z800_GET_NEXT;
    }
    IF (strpos( $s_line_in,"start|StockMovement") === false)
    {
        goto Z800_GET_NEXT;
    }

    $ar_line_details = explode("|",$s_line_in);

    $sys_function_out =  $sys_function_out." <br>| ".$s_line_in;
    $sys_function_out =  $sys_function_out." <br>13 = ".$ar_line_details[13];
//    $sys_function_out =  $sys_function_out." <br>1 = ".$ar_line_details[1];
//010Aug03|214051|start|StockMovement|date=|20100803214051|time=|214051| nulonppc1|Nulon|Syd|process=|Production|Bayfrom=||StockId=|B1231251|bayto=|W01010101|quantity=|1|count=|0|END
//2010Aug03|214112|start|StockMovement|date=|20100803214112|time=|214112| nulonppc1|Nulon|Syd|process=|Tidy|Bayfrom=||StockId=|B1231251|bayto=|W01010104|quantity=|1|count=|2|END
//2010Aug03|214131|start|StockMovement|date=|20100803214131|time=|214131| nulonppc1|Nulon|Syd|process=|Replen|Bayfrom=||StockId=|B1231305|bayto=|W01010203|quantity=|1|count=|4|END
    IF (strtoupper($ar_line_details[12]) == "PRODUCTION")
    {
        GOTO C100_PRODUCTION;
    }
    IF (strtoupper($ar_line_details[12]) == "TIDY")
    {
        GOTO D100_TIDY;
    }
    IF (strtoupper($ar_line_details[12]) == "REPLEN")
    {
        GOTO E100_REPLEN;
    }

    GOTO Z800_GET_NEXT;

C100_PRODUCTION:
    $ssql = "select * from ow_stock_movement as st_m where (sm_to = '".$ar_line_details[16]."' and sm_rec_type = 'PAL_CREATED')";
    $sys_function_out = $sys_function_out."<br>select =".$ssql;

//    echo "<br>".$ssql."<br>";


    $result = mysql_query($ssql);
    while ($row = mysql_fetch_array($result))
    {
        $s_Rec_prod_code = $row['sm_prod_code'];
        $S_REC_pack_createdutime = $row['sm_pack_createdutime'];
        $s_REC_packid = $row['sm_packid'];
    }
    $s_quantity = $ar_line_details[20];
    $s_to = $ar_line_details[18];
    $s_from = "Production";
    $s_desc = "Moved from Production to Store";

    $s_tablename = "ow_stock_movement";
    $s_extra_sql = "`createddntime`,  `updateddntime`,  `createduid`,  `updateduid`,  `createdutime`,  `updatedutime`, `sm_prod_code`,  `sm_rec_group`,  `sm_rec_type`,  `sm_quantity`,  `sm_notes`,  `sm_from`,  `sm_to`, sm_packid, sm_pack_createdutime";
    $s_extra_sqlValues = "'".date("YmdHis")."',  '".date("YmdHis")."',  '?ppc',  '?ppc',  ".time().",  ".time().",  '".$s_Rec_prod_code."', 'MOVEMENT',  'STORE',   '".$s_quantity."',  '".$s_desc."','".$s_from."', '".$s_to."', '".$s_REC_packid."', '".$S_REC_pack_createdutime."'";

    $ssql = "INSERT into ".$s_tablename." (".$s_extra_sql.") VALUES (".$s_extra_sqlValues.")";
    $rs_temp = mysql_query($ssql,$dbcnx);

    if (!$rs_temp)
    {
        $sys_function_out = "^ERROR <P>sql_a100 Error performing query: ".mysql_error()." sql = ".$ssql."</P>";
        echo $sys_function_out;
        u900_write_text_file("ALERT", "OW_PP_PPC_IN_ERROR_INFILE.TXT", "The file .".$ps_filename.". has had a mysql error=".mysql_error()." doing ".$ssql);
        die ("***  ERROR   been an error with the sql  ".mysql_error());
        goto Z900_EXIT;
    }

    $sys_function_out = $sys_function_out."<br>insert=".$ssql;
    goto Z800_GET_NEXT;

D100_TIDY:
    $ssql = "select * from ow_stock_movement as st_m where (sm_to = '".$ar_line_details[16]."' and sm_rec_type = 'PAL_CREATED')";
    $sys_function_out = $sys_function_out."<br>select =".$ssql;

//    echo "<br>tidy   ".$ssql."<br>";

    $result = mysql_query($ssql);
    while ($row = mysql_fetch_array($result))
    {
        $s_Rec_prod_code = $row['sm_prod_code'];
        $S_REC_pack_createdutime = $row['sm_pack_createdutime'];
        $s_REC_packid = $row['sm_packid'];
    }
    $s_quantity = $ar_line_details[20];
    $s_to = $ar_line_details[18];
    $s_from = $ar_line_details[18]; // gw 20100808 - need to work out the last location for the stockid
    $s_from = "Store";
    $s_desc = "Moved within the Store";

    $s_tablename = "ow_stock_movement";
    $s_extra_sql = "`createddntime`,  `updateddntime`,  `createduid`,  `updateduid`,  `createdutime`,  `updatedutime`, `sm_prod_code`,  `sm_rec_group`,  `sm_rec_type`,  `sm_quantity`,  `sm_notes`,  `sm_from`,  `sm_to`, sm_packid, sm_pack_createdutime";
    $s_extra_sqlValues = "'".date("YmdHis")."',  '".date("YmdHis")."',  '?ppc',  '?ppc',  ".time().",  ".time().",  '".$s_Rec_prod_code."', 'MOVEMENT',  'STORE',   '".$s_quantity."',  '".$s_desc."','".$s_from."', '".$s_to."', '".$s_REC_packid."', '".$S_REC_pack_createdutime."'";


    $ssql = "INSERT into ".$s_tablename." (".$s_extra_sql.") VALUES (".$s_extra_sqlValues.")";
    $rs_temp = mysql_query($ssql,$dbcnx);

    if (!$rs_temp)
    {
        $sys_function_out = "^ERROR <P>sql_a100 Error performing query: ".mysql_error()." sql = ".$ssql."</P>";
        echo $sys_function_out;
        u900_write_text_file("ALERT", "OW_PP_PPC_IN_ERROR_INFILE.TXT", "The file .".$ps_filename.". has had a mysql error=".mysql_error()." doing ".$ssql);
        die ("***  ERROR   been an error with the sql  ".mysql_error());
        goto Z900_EXIT;
    }

    $sys_function_out = $sys_function_out."<br>insert=".$ssql;
    goto Z800_GET_NEXT;
E100_REPLEN:
    $ssql = "select * from ow_stock_movement as st_m where (sm_to = '".$ar_line_details[16]."' and sm_rec_type = 'PAL_CREATED')";
    $sys_function_out = $sys_function_out."<br>select =".$ssql;

//    echo "<br>tidy   ".$ssql."<br>";

    $result = mysql_query($ssql);
    while ($row = mysql_fetch_array($result))
    {
        $s_Rec_prod_code = $row['sm_prod_code'];
        $S_REC_pack_createdutime = $row['sm_pack_createdutime'];
        $s_REC_packid = $row['sm_packid'];
    }
    $s_quantity = $ar_line_details[20];
    $s_to = $ar_line_details[18];
    $s_from = $ar_line_details[18]; // gw 20100808 - need to work out the last location for the stockid
    $s_from = "Store";
    $s_desc = "Moved from Store to Pickface";

    $s_tablename = "ow_stock_movement";
    $s_extra_sql = "`createddntime`,  `updateddntime`,  `createduid`,  `updateduid`,  `createdutime`,  `updatedutime`, `sm_prod_code`,  `sm_rec_group`,  `sm_rec_type`,  `sm_quantity`,  `sm_notes`,  `sm_from`,  `sm_to`, sm_packid, sm_pack_createdutime";
    $s_extra_sqlValues = "'".date("YmdHis")."',  '".date("YmdHis")."',  '?ppc',  '?ppc',  ".time().",  ".time().",  '".$s_Rec_prod_code."', 'MOVEMENT',  'STORE',   '".$s_quantity."',  '".$s_desc."','".$s_from."', '".$s_to."', '".$s_REC_packid."', '".$S_REC_pack_createdutime."'";


    $ssql = "INSERT into ".$s_tablename." (".$s_extra_sql.") VALUES (".$s_extra_sqlValues.")";
    $rs_temp = mysql_query($ssql,$dbcnx);

    if (!$rs_temp)
    {
        $sys_function_out = "^ERROR <P>sql_a100 Error performing query: ".mysql_error()." sql = ".$ssql."</P>";
        echo $sys_function_out;
        u900_write_text_file("ALERT", "OW_PP_PPC_IN_ERROR_INFILE.TXT", "The file .".$ps_filename.". has had a mysql error=".mysql_error()." doing ".$ssql);
        die ("***  ERROR   been an error with the sql  ".mysql_error());
        goto Z900_EXIT;
    }

    $sys_function_out = $sys_function_out."<br>insert=".$ssql;
    goto Z800_GET_NEXT;


Z800_GET_NEXT:
    $i = $i + 1;
    goto B100_GET_REC;

Z900_EXIT:

}

    */
    ?>
