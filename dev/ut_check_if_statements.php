<?php
function pf_customError($errno,$errstr,$errfile,$errline,$errcontext)
  {
// NOTE **** THIS ONLY CATCHES NON-FATAL ERRORS
//  echo "<b>Error gw12:</b> [$errno] $errstr $errfile $errline $errcontext.<br>";
// gw 20100315       switch ($error_level) {
       switch ($errno) {
        case E_USER_ERROR:
            echo "<b>My ERROR</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            echo "  Fatal error on line $errline in file $errfile";
            echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
            echo "Aborting...<br />\n";
            exit(1);
            break;

        case E_USER_WARNING:
            echo "<b>My WARNING</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        case E_USER_NOTICE:
            echo "<b>My NOTICE</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        default:
            echo "<BR>Unknown error type: [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;
        }

    /* Don't execute PHP internal error handler */
    return true;

  }

//    $sys_debug = strtoupper("yes");
//    $_SESSION['ko_prog_path'] = 'c:\\tdx\\web_prog\\dev\\';

// TO RUN
// http://dev.tdx.com.au/als_web_prog/dev/ut_check_if_statements.php?filename=/tdx/html/ordwatch/owl_op_jp_nulon.php

A000_SET_RUN:
     //set error handler
    set_error_handler("pf_customError", E_ALL);
    date_default_timezone_set('Australia/Brisbane');
    session_start();

    echo "UT_CHECK_IF_STATEMENTS.PHP <BR>";

A000_DEFINE_VARIABLES:
$s_filename = "filename not provided";
     if (isset($_GET['filename'])) $s_filename = $_GET["filename"];

A200_LOAD_LIBRARIES:
    $sys_debug = strtoupper("NO");

    $s_file_work = $s_filename;
    $s_file_exists='Y';
    if(file_exists($s_file_work))
    {
        $array_lines = file($s_file_work);
    }
    else
    {
        $s_file_exists='N';
        echo  "<br> ##### File Not Found-:".$s_file_work."<br>";
        GOTO Z900_EXIT;
    }

    echo  "<br> LIST OF ALL = statements <br>";

    $i = 0;

B100_GET_REC:
    IF ($i >= count($array_lines))
    {
        goto B900_EXIT;
    }
    IF ($sys_debug == "YES"){echo $sys_function_name." doing line ".$i." of ".count($array_lines)." s_fieldname=".$s_fieldname."<br>";};

    $s_line_in = $array_lines[$i];

    IF (substr($s_line_in,0,2) == "//")
    {
        goto B800_GET_NEXT;
    }
B200_CHECK_IF:
    if (strpos(strtoupper($s_line_in),"IF ") === FALSE)
    {
        goto B800_GET_NEXT;
    }
    if (strpos(strtoupper($s_line_in)," = ") === FALSE)
    {
        goto B800_GET_NEXT;
    }
    if (!strpos(strtoupper($s_line_in),"==") === FALSE)
    {
        goto B800_GET_NEXT;
    }

    echo "<br>Check if statements = ".$s_line_in;

B800_GET_NEXT:
    $i = $i + 1;
    goto B100_GET_REC;

B900_EXIT:


    echo "<BR><br>List of all with a == sign ";
    $i = 0;
C100_GET_REC:
    IF ($i >= count($array_lines))
    {
        goto C900_EXIT;
    }
    IF ($sys_debug == "YES"){echo $sys_function_name." doing line ".$i." of ".count($array_lines)." s_fieldname=".$s_fieldname."<br>";};

    $s_line_in = $array_lines[$i];

    IF (substr($s_line_in,0,2) == "//")
    {
        goto C800_GET_NEXT;
    }
C200_CHECK_IF:
    if (strpos(strtoupper($s_line_in),"IF ") === FALSE)
    {
        goto C800_GET_NEXT;
    }
    if (strpos(strtoupper($s_line_in),"==") === FALSE)
    {
        goto C800_GET_NEXT;
    }

    echo "<br>Check if statements = ".$s_line_in;

C800_GET_NEXT:
    $i = $i + 1;
    goto C100_GET_REC;

C900_EXIT:


    echo "<BR><br>List of all with a = sign ".$s_line_in;
    $i = 0;
D100_GET_REC:
    IF ($i >= count($array_lines))
    {
        goto D900_EXIT;
    }
    IF ($sys_debug == "YES"){echo $sys_function_name." doing line ".$i." of ".count($array_lines)." s_fieldname=".$s_fieldname."<br>";};

    $s_line_in = $array_lines[$i];

    IF (substr($s_line_in,0,2) == "//")
    {
        goto D800_GET_NEXT;
    }
D200_CHECK_IF:
    if (strpos(strtoupper($s_line_in),"IF ") === FALSE)
    {
        goto D800_GET_NEXT;
    }
    if (strpos(strtoupper($s_line_in),"=") === FALSE)
    {
        goto D800_GET_NEXT;
    }

    echo "<br>Check if statements = ".$s_line_in;

D800_GET_NEXT:
    $i = $i + 1;
    goto D100_GET_REC;
D900_EXIT:

Z900_EXIT:

//END OF PRIMARY RUN
?>

