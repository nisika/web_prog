<?php
function pf_customError($errno,$errstr,$errfile,$errline,$errcontext)
  {
// NOTE **** THIS ONLY CATCHES NON-FATAL ERRORS
//  echo "<b>Error gw12:</b> [$errno] $errstr $errfile $errline $errcontext.<br>";
// gw 20100315       switch ($error_level) {
       switch ($errno) {
        case E_USER_ERROR:
            echo "<b>My ERROR</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            echo "  Fatal error on line $errline in file $errfile";
            echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
            echo "Aborting...<br />\n";
            exit(1);
            break;

        case E_USER_WARNING:
            echo "<b>My WARNING</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        case E_USER_NOTICE:
            echo "<b>My NOTICE</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        default:
            echo "<BR>Unknown error type: [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;
        }

    /* Don't execute PHP internal error handler */
    return true;

  }

//    $sys_debug = strtoupper("yes");
//    $_SESSION['ko_prog_path'] = 'c:\\tdx\\web_prog\\dev\\';

A000_SET_RUN:
     //set error handler
    set_error_handler("pf_customError", E_ALL);
    date_default_timezone_set('Australia/Brisbane');
    session_start();
//gw20110921 - added user timezone
    if (isset($_SESSION[$s_sessionno.'ut_logon_timezone']))
    {
        $timezone=$_SESSION[$s_sessionno.'ut_logon_timezone'];
        date_default_timezone_set($timezone);
    }else{
    }


    $s_job_start = microtime(true);

    if (isset($_SESSION['ko_prog_path']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
    }else{
    $_SESSION['ko_prog_path'] = "";
    }
    if (isset($_SESSION['ko_dbase_to_connectto']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
    }else{
    $_SESSION['ko_dbase_to_connectto'] = "tdxpryda";
    }

    if (isset($_SESSION['ko_map_path']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
        $ko_map_path = $_SESSION['ko_map_path'];
    }else{
    $_SESSION['ko_map_path'] = "";
    }

A000_DEFINE_VARIABLES:
    $sys_prog_name = "";
    $sys_debug = '';
    $s_file_exists = '';
    $s_filename="";
    $map = "";
    $tmp_array = array();
    $sys_function_out = "";
    $s_temp_value = "";

    $s_sessionno = "";

    $s_file_count = "0";
    $s_file_count_error = "0";
    $s_file_count_skip = "0";
    $s_file_count_unknown = "0";
A200_LOAD_LIBRARIES:
    $sys_debug = strtoupper("NO");

     IF ($sys_debug == "YES"){echo $sys_prog_name." started debug=".$sys_debug." *** remember to view source - it will save you hours  <br>";};
     require_once($_SESSION['ko_prog_path'].'lib\\class_sql.php');
     $class_sql = new wp_SqlClient();
     IF ($sys_debug == "YES"){echo $sys_prog_name." after class_sql<br>";};
     require_once($_SESSION['ko_prog_path'].'lib\\class_main.php');
     $class_main = new clmain();
     IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_main <br>";};

//     require_once($_SESSION['ko_prog_path'].'lib/class_myplib1.php5');
//     $class_myplib1 = new myplib1();
//     IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_myplib1<br>";};

//     require_once($_SESSION['ko_map_path'].'lib/class_app.php');
//     $class_apps = new AppClass();

     IF ($sys_debug == "YES"){echo "_get=";print_r($_GET);echo "<br>";};
     IF ($sys_debug == "YES"){echo "_post=";print_r($_POST);echo "<br>";};

A300_CONNECT_TODBASE:
     $dbcnx = $class_sql->c_sqlclient_connect();

A400_GET_PARAMETERS:

A500_SET_VALUES:
    // open this directory
    $path =$ko_map_path."\\load\\";
    $s_archive_path = "c:\\tdx_archive\\".date("Y")."\\".date("m")."-".date("M")."\\".date("d")."\\pp_ppc_in\\";

    if (!is_dir($s_archive_path))
    {
        if(!mkdir($s_archive_path,0,true))
        {
            die("Failed to create the archive fodler ".$s_archive_path);
        }
    }


B100_LIST_FILES:
    //    $path =$ko_map_path;
    $file_path = opendir($path);

    // get each entry
    while($entryName = readdir($file_path)) {
        $dirArray[] = $entryName;
    }

    // close directory
    closedir($file_path);

    //    count elements in array
    $indexCount    = count($dirArray);
//    Print ("$indexCount files<br>\n");
    // sort 'em
    sort($dirArray);

    // print 'em
    $sys_function_out = $sys_function_out."<TABLE border=1 cellpadding=5 cellspacing=0 class=whitelinks>";
    $sys_function_out = $sys_function_out."<TR><td colspan='5' bgcolor='cyan'>There are ".$indexCount." files in the ".$path." folder - including system folders";
    $sys_function_out = $sys_function_out."<br> files will be archived to ".$s_archive_path."</td></TR>";
    $sys_function_out = $sys_function_out."<TR><td colspan='5' bgcolor='green'>Files in ";
    $sys_function_out = $sys_function_out."<br>Yellow will not be processed";
    $sys_function_out = $sys_function_out."<br> Red are files that have had a problem";
    $sys_function_out = $sys_function_out."<br> White will be processed";
    $sys_function_out = $sys_function_out."<br> Orange are unknown fmat and will be skipped";
    $sys_function_out = $sys_function_out."</td></TR>";
    $sys_function_out = $sys_function_out."<TR><TH>Filename</TH><th>Filetype</th><th>Filesize</th><th>Modified</th><th>Format</th></TR>";
B200_CONTENTS_LOOP:
    // loop through the array of files and print them all
    for($index=0; $index < $indexCount; $index++)
    {
        if (substr("$dirArray[$index]", 0, 1) == "."){  // don't list hidden files
            GOTO B290_SKIP_LINE;
        }
        if (filetype($path.$dirArray[$index]) == "dir"){  // don't list directories
            GOTO B290_SKIP_LINE;
        }

        $filename = $dirArray[$index];
        $s_bgcolor="bgcolor='white'";
        $s_display_count = "na";

        $s_filefmat = pf_b300_set_filefmat($dbcnx,$path,$filename,"no");
        $s_bgcolor = pf_b200_set_process_colour($path,$filename,"nousedyet","notusedyet","no",$s_filefmat);
        if ($s_bgcolor=="bgcolor='yellow'")
        {
            $s_file_count_skip = $s_file_count_skip + 1;
            $s_display_count = $s_file_count_skip;
        }
        if ($s_bgcolor=="bgcolor='red'")
        {
            $s_file_count_error = $s_file_count_error + 1;
            $s_display_count = $s_file_count_error;
        }
        if ($s_bgcolor == "bgcolor='white'")
        {
            $s_file_count = $s_file_count + 1;
            $s_display_count = $s_file_count;
        }
        if ($s_bgcolor=="bgcolor='orange'")
        {
            $s_file_count_unknown = $s_file_count_unknown + 1;
            $s_display_count = $s_file_count_unknown;
        }

B100_DO_FILE:
        $sys_function_out = $sys_function_out."<TR ".$s_bgcolor."><TD>".$s_display_count." = $dirArray[$index]</a></td>";
        $sys_function_out = $sys_function_out."<td>";
        $sys_function_out = $sys_function_out.filetype($path.$filename);
        $sys_function_out = $sys_function_out."</td>";
        $sys_function_out = $sys_function_out."<td>";
        $sys_function_out = $sys_function_out.filesize($path.$filename);
        $sys_function_out = $sys_function_out."</td>";
        $sys_function_out = $sys_function_out."<td>";
        $sys_function_out = $sys_function_out.filemtime($path.$filename);
        $sys_function_out = $sys_function_out." - ";
        $sys_function_out = $sys_function_out.date("Y-m-d H:i:s",filemtime($path.$filename));
        $sys_function_out = $sys_function_out."</td>";
        $sys_function_out = $sys_function_out."<td>";
        $sys_function_out = $sys_function_out.$s_filefmat;
        $sys_function_out = $sys_function_out."</td>";

        $sys_function_out = $sys_function_out."</TR>";
B290_SKIP_LINE:
    }
B900_EXIT:
    $sys_function_out = $sys_function_out."<TR bgcolor='green'><td colspan='5'>There are";
    $sys_function_out = $sys_function_out."<br> ".$s_file_count." files to be processed";
    $sys_function_out = $sys_function_out."<br>".$s_file_count_skip." are to be skipped";
    $sys_function_out = $sys_function_out."<br>".$s_file_count_error." have had a previous error";
    $sys_function_out = $sys_function_out."<br>".$s_file_count_unknown." are unknown format files";
    $sys_function_out = $sys_function_out."</td></TR>";
    $sys_function_out = $sys_function_out."</TABLE>";
    ECHO $sys_function_out;
B999_END_OF_LIST_FILES:

C010_DO_FILE_PROCESS:
    u900_write_text_file("LOG", "OW_PP_PPC_IN".date("YM").".TXT", "There are".$s_file_count." files to be processed ".$s_file_count_skip." are to be skipped ".$s_file_count_error." have had a previous error ".$s_file_count_unknown." are unknown format files");

    $sys_function_out = "";
    $sys_function_out = $sys_function_out."<br>File processing started at ".date("Y-m-d H:i:s");
    ECHO $sys_function_out;
    $f_fprocess_start = microtime(true);


    $s_file_count_error = "0";
    $s_file_count_skip = "0";
    $s_file_count_now = "0";
C100_PROCESS_FILES:
    for($index=0; $index < $indexCount; $index++)
    {
        $f_dofile_start = microtime(true);
        if (substr("$dirArray[$index]", 0, 1) == "."){  // don't list hidden files
            GOTO C290_SKIP_LINE;
        }
        if (filetype($path.$dirArray[$index]) == "dir"){  // don't list directories
            GOTO C290_SKIP_LINE;
        }
        $filename = $dirArray[$index];
        $s_bgcolor="bgcolor='white'";
        $s_filefmat = pf_b300_set_filefmat($dbcnx,$path,$filename,"no");
        $s_bgcolor = pf_b200_set_process_colour($path,$filename,"nousedyet","notusedyet","no",$s_filefmat);

C100_DO_FILE:
        $sys_function_out = "";
        $sys_function_out = $sys_function_out."<br>File  Processing started ".$path.$dirArray[$index]." at ".date("Y-m-d H:i:s");
        ECHO $sys_function_out;
        if ($s_bgcolor=="bgcolor='yellow'")
        {
         //       pf_b300_move_to_alert($dbcnx, $path,$dirArray[$index],"c100_do_file",$s_archive_path);
            GOTO C200_END_FILE;
        }
        if ($s_bgcolor=="bgcolor='red'")
        {
            u900_write_text_file("ALERT", "OW_PP_PPC_IN_ERROR_FAILED_FILE.TXT", "The file .".$path.$filename.". has failed in a previous run or is being process while this process is trying to run");
            GOTO C200_END_FILE;
        }
        if ($s_bgcolor=="bgcolor='orange'")
        {
            $s_file_archive = $s_archive_path.$filename."_".date("YmdHis").".unknown";
            u900_write_text_file("LOG", "OW_PP_PPC_IN".date("YM").".TXT", "The file .".$path.$filename.". is an unknown format has has been moved to ".$s_file_archive);
            u900_write_text_file("ALERT", "ERROR_OW_PP_PPC_IN_UNKNOWN_FILE.TXT", "The file .".$path.$filename.". is an unknown format has has been moved to ".$s_file_archive);
            IF ($sys_debug == "YES"){echo $sys_function_name."  unknown file  ".$path.$filename. " started by renaming it to ".$s_file_archive;};
            rename($path.$filename,$s_file_archive);
            IF ($sys_debug == "YES"){echo $sys_function_name."  unknown file after the rename on file = ".$s_file_archive;};
            GOTO C200_END_FILE;
        }
        if ($s_bgcolor == "bgcolor='white'")
        {
            $s_file_count_now = $s_file_count_now +1;
            $sys_function_out = "";
            $sys_function_out = $sys_function_out."<br>File ".$s_file_count_now." of ".$s_file_count." Processing started ".$path.$dirArray[$index]." at ".date("Y-m-d H:i:s");
            ECHO $sys_function_out;

            pf_b100_process_file($dbcnx, $path,$dirArray[$index],"c100_do_file",$s_archive_path,$s_filefmat);
        }
C200_END_FILE:
        $f_dofile_end = microtime(true);
        $duration= round($f_dofile_end - $f_dofile_start,4);
        $sys_function_out = "";
        $sys_function_out = $sys_function_out."<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ended ".$path.$dirArray[$index]." at ".date("Y-m-d H:i:s")." in ".$duration." seconds";
        ECHO $sys_function_out;

C290_SKIP_LINE:
    }

C999_END_FILE_PROCESS:

Z900_EXIT:

//END OF PRIMARY RUN
?>

<?php
// ***********************************************************************************************************************************
// start functions

function pf_b100_process_file($ps_dbcnx,$ps_path,$ps_filename,$ps_debug,$ps_archive_path,$ps_filefmat )
{

    global $class_main;
    global $class_sql;

    $dbcnx = $ps_dbcnx;
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "<br>debug - pf_b100_process_file - from=".$sys_debug_text;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>DEBUG VIEW SOURCE FOR DETAILS#######################################################<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_filename = ".$ps_filename." ps_debug = ".$ps_debug." ";};


    $s_file_in = $ps_path.$ps_filename;
    $s_file_work = $ps_path.$ps_filename.date("his").".working";
    $s_file_archive = $ps_archive_path.$ps_filename."_".date("YmdHis").".done";

    IF ($sys_debug == "YES"){echo $sys_function_name."  I'm doing ".$s_file_in. " started by renaming it to ".$s_file_work;};
    rename($s_file_in,$s_file_work);
    IF ($sys_debug == "YES"){echo $sys_function_name."  Starting to process after the rename on file = ".$s_file_work;};

    if ($ps_filefmat == "OWPPWSTOCKSCANS")
    {
        pf_c010_do_pp_stock_scans($ps_dbcnx,$s_file_work,$ps_debug);
    }
ZZ900_EXIT:

Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_function_name."  I'm doing anarchive of the file ".$s_file_work." to ".$s_file_archive;};
    rename($s_file_work,$s_file_archive);
    IF ($sys_debug == "YES"){echo $sys_function_name."  the archive did not fail";};

    return $sys_function_out;

}
// ***********************************************************************************************************************************
function pf_b200_set_process_colour($ps_path,$ps_filename,$ps_no_proccess_list,$ps_error_list,$ps_debug,$ps_filefmat)
{
// need to modify to check the values against the error_list and no_process_lists
    $s_filename = $ps_filename;
    $s_bgcolor="bgcolor='white'";
    if (strtoupper($ps_filefmat) == "UNKNOWN")
    {
        $s_bgcolor="bgcolor='orange'";
        GOTO Z900_EXIT;
    }
B100_DO_NO_PROCESS:
        if (strpos($s_filename,".done") === false)
        {
        }else{
            $s_bgcolor="bgcolor='yellow'";
            goto Z900_EXIT;
        }
C100_DO_ERROR_LIST:
        if (strpos($s_filename,".working") === false)
        {
            GOTO Z900_EXIT;
        }else{
            $s_bgcolor="bgcolor='red'";
        }
Z900_EXIT:
    return $s_bgcolor;
}
// ***********************************************************************************************************************************
function pf_b300_set_filefmat($ps_dbcnx,$ps_path,$ps_filename,$ps_debug)
{
    global $class_main;
    global $class_sql;

    $dbcnx = $ps_dbcnx;
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "<br>debug - pf_b300_set_filefmat - from=".$sys_debug_text;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>DEBUG VIEW SOURCE FOR DETAILS#######################################################<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_filename = ".$ps_filename." ps_debug = ".$ps_debug." ";};

    $s_filefmat = "Unknown";
A020_USE_FNAME:
    IF (strpos(strtoupper($ps_filename),"NULON") === false)
    {}else{
        $s_filefmat = "OWPPWSTOCKSCANS";
    }

A900_EXIT:
A999_USE_FNAME_END:
    GOTO X100_TRANSLATE_FMAT;

B020_USE_HEADERLINE:  //GW 20100807 - NOT TESTED
/*
    $s_file_exists='Y';
    if(file_exists($s_file_work))
    {
        $array_lines = file($s_file_work);
    }
    else
    {
        $s_file_exists='N';
        $sys_function_out =  "<br>".$sys_function_name." ##### File Not Found-:".$s_file_work."<br>";
        GOTO Z900_EXIT;
    }

    IF ($sys_debug == "YES"){echo $sys_function_name." after file exists check s_file_exists=".$s_file_exists."<br>";};
    IF ($sys_debug == "YES"){echo $sys_function_name." number of lines in the file(array)=".count($array_lines)."<br>";};

    $i = 0;
    $s_line_in = "";

B100_GET_REC:
    IF ($i >= count($array_lines))
    {
        goto Z900_EXIT;
    }

    $s_line_in = $array_lines[$i];
    IF (substr($s_line_in,0,1) == "*")
    {
        goto B900_GET_NEXT;
    }
//USE THE FIRST LINE ONLY TO DETERMIN THE FMAT
    GOTO B990_END;

B900_GET_NEXT:
    $i = $i + 1;
    goto B100_GET_REC;

B990_END:
    GOTO X100_TRANSLATE_FMAT;

*/
C020_USE_FILECONTENTS: //GW 20100807 - NOT TESTED
/*
  //GW 20100807 - NOT TESTED
    $s_file_exists='Y';
    if(file_exists($s_file_work))
    {
        $array_lines = file($s_file_work);
    }
    else
    {
        $s_file_exists='N';
        $sys_function_out =  "<br>".$sys_function_name." ##### File Not Found-:".$s_file_work."<br>";
        GOTO Z900_EXIT;
    }

    IF ($sys_debug == "YES"){echo $sys_function_name." after file exists check s_file_exists=".$s_file_exists."<br>";};
    IF ($sys_debug == "YES"){echo $sys_function_name." number of lines in the file(array)=".count($array_lines)."<br>";};

    $i = 0;
    $s_line_in = "";

C100_GET_REC:
    IF ($i >= count($array_lines))
    {
        goto C990_END;
    }

    $s_line_in = $array_lines[$i];
    IF (substr($s_line_in,0,1) == "*")
    {
        goto C900_GET_NEXT;
    }



    GOTO C900_GET_NEXT;

C900_GET_NEXT:
    $i = $i + 1;
    goto C900_GET_NEXT;

C990_END:
    GOTO X100_TRANSLATE_FMAT;

*/
X100_TRANSLATE_FMAT:

Z900_EXIT:
    return $s_filefmat;
}
// ***********************************************************************************************************************************
function pf_c010_do_pp_stock_scans($ps_dbcnx,$ps_filename,$ps_debug)
{
    global $class_main;
    global $class_sql;

    $dbcnx = $ps_dbcnx;
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "<br>debug - pf_c010_do_pp_stock_scans - from=".$sys_debug_text;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>DEBUG VIEW SOURCE FOR DETAILS#######################################################<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_filename = ".$ps_filename." ps_debug = ".$ps_debug." ";};

    $s_file_work = $ps_filename;
    $s_file_exists='Y';
    if(file_exists($s_file_work))
    {
        $array_lines = file($s_file_work);
    }
    else
    {
        $s_file_exists='N';
        $sys_function_out =  "<br>".$sys_function_name." ##### File Not Found-:".$s_file_work."<br>";
        GOTO Z900_EXIT;
    }

    IF ($sys_debug == "YES"){echo $sys_function_name." after file exists check s_file_exists=".$s_file_exists."<br>";};
    IF ($sys_debug == "YES"){echo $sys_function_name." number of lines in the file(array)=".count($array_lines)."<br>";};

    $i = 0;
    $s_line_in = "";

B100_GET_REC:
    IF ($i >= count($array_lines))
    {
        goto Z900_EXIT;
    }

    $s_line_in = $array_lines[$i];
    IF (substr($s_line_in,0,1) == "*")
    {
        goto Z800_GET_NEXT;
    }
    IF (strpos( $s_line_in,"start|StockMovement") === false)
    {
        goto Z800_GET_NEXT;
    }

    $ar_line_details = explode("|",$s_line_in);

    $sys_function_out =  $sys_function_out." <br>| ".$s_line_in;
    $sys_function_out =  $sys_function_out." <br>13 = ".$ar_line_details[13];
//    $sys_function_out =  $sys_function_out." <br>1 = ".$ar_line_details[1];
//010Aug03|214051|start|StockMovement|date=|20100803214051|time=|214051| nulonppc1|Nulon|Syd|process=|Production|Bayfrom=||StockId=|B1231251|bayto=|W01010101|quantity=|1|count=|0|END
//2010Aug03|214112|start|StockMovement|date=|20100803214112|time=|214112| nulonppc1|Nulon|Syd|process=|Tidy|Bayfrom=||StockId=|B1231251|bayto=|W01010104|quantity=|1|count=|2|END
//2010Aug03|214131|start|StockMovement|date=|20100803214131|time=|214131| nulonppc1|Nulon|Syd|process=|Replen|Bayfrom=||StockId=|B1231305|bayto=|W01010203|quantity=|1|count=|4|END
    IF (strtoupper($ar_line_details[12]) == "PRODUCTION")
    {
        GOTO C100_PRODUCTION;
    }
    IF (strtoupper($ar_line_details[12]) == "TIDY")
    {
        GOTO D100_TIDY;
    }
    IF (strtoupper($ar_line_details[12]) == "REPLEN")
    {
        GOTO E100_REPLEN;
    }

    GOTO Z800_GET_NEXT;

C100_PRODUCTION:
    $ssql = "select * from ow_stock_movement as st_m where (sm_to = '".$ar_line_details[16]."' and sm_rec_type = 'PAL_CREATED')";
    $sys_function_out = $sys_function_out."<br>select =".$ssql;

//    echo "<br>".$ssql."<br>";


    $result = mysql_query($ssql);
    while ($row = mysql_fetch_array($result))
    {
        $s_Rec_prod_code = $row['sm_prod_code'];
        $S_REC_pack_createdutime = $row['sm_pack_createdutime'];
        $s_REC_packid = $row['sm_packid'];
    }
    $s_quantity = $ar_line_details[20];
    $s_to = $ar_line_details[18];
    $s_from = "Production";
    $s_desc = "Moved from Production to Store";

    $s_tablename = "ow_stock_movement";
    $s_extra_sql = "`createddntime`,  `updateddntime`,  `createduid`,  `updateduid`,  `createdutime`,  `updatedutime`, `sm_prod_code`,  `sm_rec_group`,  `sm_rec_type`,  `sm_quantity`,  `sm_notes`,  `sm_from`,  `sm_to`, sm_packid, sm_pack_createdutime";
    $s_extra_sqlValues = "'".date("YmdHis")."',  '".date("YmdHis")."',  '?ppc',  '?ppc',  ".time().",  ".time().",  '".$s_Rec_prod_code."', 'MOVEMENT',  'STORE',   '".$s_quantity."',  '".$s_desc."','".$s_from."', '".$s_to."', '".$s_REC_packid."', '".$S_REC_pack_createdutime."'";

    $ssql = "INSERT into ".$s_tablename." (".$s_extra_sql.") VALUES (".$s_extra_sqlValues.")";
    $rs_temp = mysql_query($ssql,$dbcnx);

    if (!$rs_temp)
    {
        $sys_function_out = "^ERROR <P>sql_a100 Error performing query: ".mysql_error()." sql = ".$ssql."</P>";
        echo $sys_function_out;
        u900_write_text_file("ALERT", "OW_PP_PPC_IN_ERROR_INFILE.TXT", "The file .".$ps_filename.". has had a mysql error=".mysql_error()." doing ".$ssql);
        die ("***  ERROR   been an error with the sql  ".mysql_error());
        goto Z900_EXIT;
    }

    $sys_function_out = $sys_function_out."<br>insert=".$ssql;
    goto Z800_GET_NEXT;

D100_TIDY:
    $ssql = "select * from ow_stock_movement as st_m where (sm_to = '".$ar_line_details[16]."' and sm_rec_type = 'PAL_CREATED')";
    $sys_function_out = $sys_function_out."<br>select =".$ssql;

//    echo "<br>tidy   ".$ssql."<br>";

    $result = mysql_query($ssql);
    while ($row = mysql_fetch_array($result))
    {
        $s_Rec_prod_code = $row['sm_prod_code'];
        $S_REC_pack_createdutime = $row['sm_pack_createdutime'];
        $s_REC_packid = $row['sm_packid'];
    }
    $s_quantity = $ar_line_details[20];
    $s_to = $ar_line_details[18];
    $s_from = $ar_line_details[18]; // gw 20100808 - need to work out the last location for the stockid
    $s_from = "Store";
    $s_desc = "Moved within the Store";

    $s_tablename = "ow_stock_movement";
    $s_extra_sql = "`createddntime`,  `updateddntime`,  `createduid`,  `updateduid`,  `createdutime`,  `updatedutime`, `sm_prod_code`,  `sm_rec_group`,  `sm_rec_type`,  `sm_quantity`,  `sm_notes`,  `sm_from`,  `sm_to`, sm_packid, sm_pack_createdutime";
    $s_extra_sqlValues = "'".date("YmdHis")."',  '".date("YmdHis")."',  '?ppc',  '?ppc',  ".time().",  ".time().",  '".$s_Rec_prod_code."', 'MOVEMENT',  'STORE',   '".$s_quantity."',  '".$s_desc."','".$s_from."', '".$s_to."', '".$s_REC_packid."', '".$S_REC_pack_createdutime."'";


    $ssql = "INSERT into ".$s_tablename." (".$s_extra_sql.") VALUES (".$s_extra_sqlValues.")";
    $rs_temp = mysql_query($ssql,$dbcnx);

    if (!$rs_temp)
    {
        $sys_function_out = "^ERROR <P>sql_a100 Error performing query: ".mysql_error()." sql = ".$ssql."</P>";
        echo $sys_function_out;
        u900_write_text_file("ALERT", "OW_PP_PPC_IN_ERROR_INFILE.TXT", "The file .".$ps_filename.". has had a mysql error=".mysql_error()." doing ".$ssql);
        die ("***  ERROR   been an error with the sql  ".mysql_error());
        goto Z900_EXIT;
    }

    $sys_function_out = $sys_function_out."<br>insert=".$ssql;
    goto Z800_GET_NEXT;
E100_REPLEN:
    $ssql = "select * from ow_stock_movement as st_m where (sm_to = '".$ar_line_details[16]."' and sm_rec_type = 'PAL_CREATED')";
    $sys_function_out = $sys_function_out."<br>select =".$ssql;

//    echo "<br>tidy   ".$ssql."<br>";

    $result = mysql_query($ssql);
    while ($row = mysql_fetch_array($result))
    {
        $s_Rec_prod_code = $row['sm_prod_code'];
        $S_REC_pack_createdutime = $row['sm_pack_createdutime'];
        $s_REC_packid = $row['sm_packid'];
    }
    $s_quantity = $ar_line_details[20];
    $s_to = $ar_line_details[18];
    $s_from = $ar_line_details[18]; // gw 20100808 - need to work out the last location for the stockid
    $s_from = "Store";
    $s_desc = "Moved from Store to Pickface";

    $s_tablename = "ow_stock_movement";
    $s_extra_sql = "`createddntime`,  `updateddntime`,  `createduid`,  `updateduid`,  `createdutime`,  `updatedutime`, `sm_prod_code`,  `sm_rec_group`,  `sm_rec_type`,  `sm_quantity`,  `sm_notes`,  `sm_from`,  `sm_to`, sm_packid, sm_pack_createdutime";
    $s_extra_sqlValues = "'".date("YmdHis")."',  '".date("YmdHis")."',  '?ppc',  '?ppc',  ".time().",  ".time().",  '".$s_Rec_prod_code."', 'MOVEMENT',  'STORE',   '".$s_quantity."',  '".$s_desc."','".$s_from."', '".$s_to."', '".$s_REC_packid."', '".$S_REC_pack_createdutime."'";


    $ssql = "INSERT into ".$s_tablename." (".$s_extra_sql.") VALUES (".$s_extra_sqlValues.")";
    $rs_temp = mysql_query($ssql,$dbcnx);

    if (!$rs_temp)
    {
        $sys_function_out = "^ERROR <P>sql_a100 Error performing query: ".mysql_error()." sql = ".$ssql."</P>";
        echo $sys_function_out;
        u900_write_text_file("ALERT", "OW_PP_PPC_IN_ERROR_INFILE.TXT", "The file .".$ps_filename.". has had a mysql error=".mysql_error()." doing ".$ssql);
        die ("***  ERROR   been an error with the sql  ".mysql_error());
        goto Z900_EXIT;
    }

    $sys_function_out = $sys_function_out."<br>insert=".$ssql;
    goto Z800_GET_NEXT;


Z800_GET_NEXT:
    $i = $i + 1;
    goto B100_GET_REC;

Z900_EXIT:

}
// ***********************************************************************************************************************************
function u900_write_text_file($ps_type, $ps_filename, $ps_text)
{
    $status = "ALLGOOD";
    $root = "/tdx_manage";
    $path =$root."/alerts/";

    if (strtoupper($ps_type) == "LOG")
    {
        $path =$root."/log/".date("Y")."/".date("m")."-".date("M")."/".date("d"."/");
    }
//GW20100808     $path = $path.date("Y")."/".date("m")."-".date("M")."/".date("d"."/");

    if (!is_dir($path))
    {
        if(!mkdir($path,0,true))
        {
            die(" u900_write_text_file Failed to create the fodler ".$path." trying to write ".$ps_text." to file ".$ps_filename);
        }
    }
B100_WRITE:
    $myfile = $path.$ps_filename;
    $fh = fopen($myfile,'a') or die("cant open file".$myfile." trying to write text ".$ps_text);
    fwrite($fh,date("Y-m-d H:i:s",time())." - ".$ps_text."\r\n");
    fclose($fh);

Z900_EXIT:
    return $status;

}

?>