<?php

class Connector
{

    function request($site, $location, $data)
    {
        $site = explode(':', $site);
        if(isset($site[1]) and is_numeric($site[1]))
        {
            $port = $site[1];
        }
        else
        {
            $port = 80;
        }
        $site = $site[0];

        $conn = fsockopen ($site, $port); #open the connection
        if(!$conn)
        { #if the connection was not opened successfully
            $err_arr['tdx_data']['err']['number']=10532;
            $err_arr['tdx_data']['err']['text']="Connection failed: Couldn't make the connection to $site.";
            return array(false,$err_arr);
        }
        else
        {
            $headers =
                "POST $location HTTP/1.0\r\n" .
                "Host: $site\r\n" .
                "Connection: close\r\n" .
                ($user_agent ? "User-Agent: $user_agent\r\n" : '') .
                "Content-Type: text/xml\r\n" .
                "Content-Length: " . strlen($data) . "\r\n\r\n";

            fputs($conn, "$headers");
            fputs($conn, $data);

            #socket_set_blocking ($conn, false);
            $response = "";
            while(!feof($conn))
            {
                $response .= fgets($conn, 1024);
            }
            fclose($conn);

            $data = Connector::XML_unserialize(substr($response, strpos($response, "\r\n\r\n")+4));
            if(count($data)<=0)
            {
                //echo 'no data returned';
                $err_arr['tdx_data']['err']['number']=10533;
                $err_arr['tdx_data']['err']['text']="Requested data could not be returned.";
                return array(false,$err_arr);
            }
            else
            {
                //echo 'data returned';
                return array(true,$data);
            }
        }

    }

    function response($return_value, $server = NULL){
        $data['tdx_data'] = &$return_value;
        //$return = Connector::XML_serialize(&$data);
        // pkn 20090127 - unsure if above is required, causes error in PHP so will comment it out and see what happens
        $return = Connector::XML_serialize($data);

        header("Connection: close");
        header("Content-Length: " . strlen($return));
        header("Content-Type: text/xml");
        header("Date: " . date("r"));
        if($server){
            header("Server: $server");
        }
        echo $return;
    }

    function client_err($faultCode, $faultString, $server = NULL){
        $array['tdx_data']['err']['number']=$faultCode;
        $array['tdx_data']['err']['text']=$faultString;

        $return = Connector::XML_serialize($array);

        header("Connection: close");
        header("Content-Length: " . strlen($return));
        header("Content-Type: text/xml");
        header("Date: " . date("r"));
        if($server){
            header("Server: $server");
        }
        echo $return;
    }

    function & XML_unserialize(&$xml){
        $xml_parser = new XML();
        //$data = &$xml_parser->parse(&$xml);
        // pkn 20090127 - unsure if above is required, causes error in PHP so will comment it out and see what happens
        $data = &$xml_parser->parse($xml);
        $xml_parser->destruct();
        return $data;
    }

    function & XML_serialize(&$data, $level = 0, $prior_key = NULL){
        #assumes a hash, keys are the variable names
        $xml_serialized_string = "";
        while(list($key, $value) = each($data)){
            $inline = false;
            $numeric_array = false;
            $attributes = "";
            #echo "My current key is '$key', called with prior key '$prior_key'<br>";
            if(!strstr($key, " attr")){ #if it's not an attribute
                if(array_key_exists("$key attr", $data)){
                    while(list($attr_name, $attr_value) = each($data["$key attr"])){
                        #echo "Found attribute $attribute_name with value $attribute_value<br>";
                        $attr_value = &htmlspecialchars($attr_value, ENT_QUOTES);
                        $attributes .= " $attr_name=\"$attr_value\"";
                    }
                }

                if(is_numeric($key)){
                    #echo "My current key ($key) is numeric. My parent key is '$prior_key'<br>";
                    $key = $prior_key;
                }else{
                    #you can't have numeric keys at two levels in a row, so this is ok
                    #echo "Checking to see if a numeric key exists in data.";
                    if(is_array($value) and array_key_exists(0, $value)){
                        #    echo " It does! Calling myself as a result of a numeric array.<br>";
                        $numeric_array = true;
                        $xml_serialized_string .= Connector::XML_serialize($value, $level, $key);
                    }
                    #echo "<br>";
                }

                if(!$numeric_array){
                    $xml_serialized_string .= str_repeat("\t", $level) . "<$key$attributes>";

                    if(is_array($value)){
                        $xml_serialized_string .= "\r\n" . Connector::XML_serialize($value, $level+1);
                    }else{
                        $inline = true;
                        $xml_serialized_string .= htmlspecialchars($value);
                    }

                    $xml_serialized_string .= (!$inline ? str_repeat("\t", $level) : "") . "</$key>\r\n";
                }
            }else{
                #echo "Skipping attribute record for key $key<bR>";
            }
        }
        if($level == 0){
            $xml_serialized_string = "<?phpxml version=\"1.0\" ?>\r\n" . $xml_serialized_string;
            return $xml_serialized_string;
        }else{
            return $xml_serialized_string;
        }
    }
}

class XML {
    var $parser; #a reference to the XML parser
    var $document; #the entire XML structure built up so far
    var $current; #a pointer to the current item - what is this
    var $parent; #a pointer to the current parent - the parent will be an array
    var $parents; #an array of the most recent parent at each level

    var $last_opened_tag;

    function XML($data=null){
        $this->parser = xml_parser_create();

        xml_parser_set_option ($this->parser, XML_OPTION_CASE_FOLDING, 0);
        xml_set_object($this->parser, $this);
        xml_set_element_handler($this->parser, "open", "close");
        xml_set_character_data_handler($this->parser, "data");
#        register_shutdown_function(array(&$this, 'destruct'));
    }

    function destruct(){
        xml_parser_free($this->parser);
    }

    function load_xml($filepath_name=false)
    {
        $xml_data="";
        if($filepath_name===false)
        {
            return $filepath_name;
        }
        if(file_exists($filepath_name))
        {
            $file_arr = file($filepath_name);
            if(is_array($file_arr))
            {
                $xml_data="";
                foreach ($file_arr as $file_line)
                {
                    $xml_data.=trim($file_line)."\r\n";
                }
            }
        }
        if(strlen($xml_data)>0) {
            return $xml_data;
        } else {
            return false;
        }
    }

    function parse($data){
        $this->document = array();
        $this->parent = &$this->document;
        $this->parents = array();
        $this->last_opened_tag = NULL;
        xml_parse($this->parser, $data);
        return $this->document;
    }

    function open($parser, $tag, $attributes){
        #echo "Opening tag $tag<br>\n";
        $this->data = "";
        $this->last_opened_tag = $tag; #tag is a string
        if(array_key_exists($tag, $this->parent)){
            #echo "There's already an instance of '$tag' at the current level ($level)<br>\n";
            if(is_array($this->parent[$tag]) and array_key_exists(0, $this->parent[$tag])){ #if the keys are numeric
                #need to make sure they're numeric (account for attributes)
                $key = XML::count_numeric_items($this->parent[$tag]);
                #echo "There are $key instances: the keys are numeric.<br>\n";
            }else{
                #echo "There is only one instance. Shifting everything around<br>\n";
                $temp = &$this->parent[$tag];
                unset($this->parent[$tag]);
                $this->parent[$tag][0] = &$temp;

                if(array_key_exists("$tag attr", $this->parent)){
                    #shift the attributes around too if they exist
                    $temp = &$this->parent["$tag attr"];
                    unset($this->parent["$tag attr"]);
                    $this->parent[$tag]["0 attr"] = &$temp;
                }
                $key = 1;
            }
            $this->parent = &$this->parent[$tag];
        }else{
            $key = $tag;
        }
        if($attributes){
            $this->parent["$key attr"] = $attributes;
        }

        $this->parent[$key] = array();
        $this->parent = &$this->parent[$key];
        array_unshift($this->parents, $this->parent);
    }

    function data($parser, $data){
        #echo "Data is '", htmlspecialchars($data), "'<br>\n";
        if($this->last_opened_tag != NULL){
            $this->data .= $data;
        }
    }

    function close($parser, $tag){
        #echo "Close tag $tag<br>\n";
        if($this->last_opened_tag == $tag){
            $this->parent = $this->data;
            $this->last_opened_tag = NULL;
        }
        array_shift($this->parents);
        $this->parent = &$this->parents[0];
    }

    function count_numeric_items(&$array){
        return is_array($array) ? count(array_filter(array_keys($array), 'is_numeric')) : 0;
    }

}


?>