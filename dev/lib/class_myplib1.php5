<?php
  // class lib1.php
class myplib1
{

function f_myplib1_set_uniquekey($ps_tablename,$ps_unique_fieldname)
{
    $s_cnt=0;
    $s_random_number=rand(1, 10000);
    $s_uniquekey = date("YmdHis").$s_random_number;
    while ($rs_temp_row_cnt > 0)
    {
        $ssql = "SELECT * from {$ps_tablename} where {$ps_unique_fieldname} = '{$s_uniquekey}' ";
        $rs_temp = mysql_query($ssql);
        if (!$rs_temp)
        {
            echo("<P>Error performing query: ".mysql_error()." sql = ".$ssql."</P>");
            //exit();
        }
        $s_cnt++;
        $s_uniquekey=$s_uniquekey.$s_cnt;
        $rs_temp_row_cnt = mysql_num_rows($rs_temp);
    }
    return $s_uniquekey;
}
function f_myplib1_set_mm_to_month($ps_mm,$ps_shortlong)
{
    $s_mm = $ps_mm;
    if (substr($s_mm,0,1)=="0")
    {
        $s_mm = substr($s_mm,1,1);
    }
    $s_monthnames = array(1 => 'January','February','March','April','May','June','July','August','September','October','November','December');
    if (strtoupper(trim($ps_shortlong))== "SHORT")
    {
      $s_monthnames = array(1 => 'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
    }
    //gw20090516 not used $s_month = $ps_monthname;

    $s_monthname = $s_monthnames[$s_mm];
//    if (trim($s_monthname) = "")
//    {$s_monthname = $ps_mm;};
    return $s_monthname;
}
function f_myplib1_date_select_MMM($ps_name)
{
    $s_lineout = "";
    $curr_month = date("m");
    $curr_day = date("d");
    $month = array (1=>"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
// do month
   $s_lineout = "<select name=\"".$ps_name."\">\n";
   foreach($month as $key => $val) {
       $s_lineout .= "\t<option val=\"$key\"";
       if($key == $curr_month) {
          $s_lineout .= " selected";
       }
       $s_lineout .= ">$val\n";
   }
    $s_lineout .= " </select>";
    return $s_lineout;
}
function f_myplib1_date_select_DD($ps_name)
{
    $counter = 0;
    $s_lineout = "";
    $curr_day = date("d");

    $s_lineout = "<select name=\"".$ps_name."\">";
    for ($counter = "1";$counter <= "31";$counter +=1){
        if ($counter == $curr_day){
                $s_lineout .= "<option selected value=\"".$counter."\">".$counter."</option>";
        }else{
                $s_lineout .= "<option value=\"".$counter."\">".$counter."</option>";
        }
    }
    $s_lineout .= " </select>";
    return $s_lineout;
}
function f_myplib1_date_select_YYYY($ps_name,$ps_back,$ps_forward)
{
    $counter = 0;
    $s_lineout = "";
    $curr_year = date("y") + 2000;
    $s_last = $curr_year + $ps_forward;
    $s_first = $curr_year - $ps_back;

    $s_lineout = "<select name=\"".$ps_name."\">";
    for ($counter = $s_first;$counter <= $s_last;$counter +=1){
        if ($counter == $curr_year){
                $s_lineout .= "<option selected value=\"".$counter."\">".$counter."</option>";
        }else{
                $s_lineout .= "<option value=\"".$counter."\">".$counter."</option>";
        }
    }
    $s_lineout .= " </select>";
    return $s_lineout;
}
function f_myplib1_getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}
function f_myplib1_set_date_display($ps_datecymd)
{
require_once('lib/class_myplib1.php5');
$objlib1 = new myplib1();

    $s_date_display = "";
//get the format based on the mypalletid country  - format the date accordingly
    $s_date_display = $ps_datecymd;
    $s_date_display = substr($ps_datecymd,6,2)."/".$objlib1->f_myplib1_set_mm_to_month(substr($ps_datecymd,4,2),"SHORT")."/".substr($ps_datecymd,0,4) ;
    return $s_date_display;
}
function f_myplib1_select_count($ps_name,$ps_default,$ps_first,$ps_last)
{
    $counter = 0;
    $s_lineout = "";
    $s_lineout = $s_lineout." <select name=".$ps_name.">";
    for ($counter = $ps_first;$counter <= $ps_last;$counter +=1){
        if ($counter == $ps_default){
                $s_lineout = $s_lineout."<option selected value='".$counter."'>".$counter."</option>";
        }else{
                $s_lineout = $s_lineout."<option value='".$counter."'>".$counter."</option>";
        }
    }
    if ($ps_last =="50")
    {
        $s_lineout = $s_lineout."<option value='100'>100</option>";
        $s_lineout = $s_lineout."<option value='200'>200</option>";
        $s_lineout = $s_lineout."<option value='300'>300</option>";
    }

    $s_lineout = $s_lineout." </select>";
    return $s_lineout;
}
function f_mypblib1_advert_url($ps_location,$ps_what)
{
   $s_image = "";
    $s_location = strtoupper(trim($ps_location));
    $s_url = $s_location; //"http://delnet.com.au";
    $s_result = "";
    // goto dbase addvert table and get the url and image for the addvert
    if ($s_location == "MYB_SCREEN_MIDBOTTOM")
    {$s_url = "http://mypalletmanager.com";
      $s_image = "images/addmidb.bmp";
      $s_result = "GOOD";}
    if ($s_location == "MYB_SCREEN_TOPRIGHT")
    {$s_url = "http://mypalletpartner.com";
     $s_image = "images/addright.bmp";
      $s_result = "GOOD";}
    if ($s_location == "MYB_SCREEN_TOPLEFT")
    {$s_url = "http://mypalletaccount.com";
     $s_image = "images/addleft.bmp";
      $s_result = "GOOD";}

    IF ($s_result !== "GOOD")
    {
     return "NOT GOOD".$ps_location.$ps_what;
    }


    if (strtoupper(trim($ps_what)) == "URL")
        {return $s_url;}
    if (strtoupper(trim($ps_what)) == "IMAGE")
        {return $s_image;}

}
function f_myplib1_month_to_mm($ps_monthname)
{
    $s_monthnames = array(1 => 'January','February','March','April','May','June','July','August','September','October','November','December');
    $s_month = $ps_monthname;
    $s_monthnumber = array_search($s_month,$s_monthnames);
    return $s_monthnumber;
}
function f_myplib1_set_activity_name($ps_activity)
{
    $s_activityname = "";
    $s_activityname = $ps_activity;
    $s_activity = strtolower(trim($ps_activity));

    if ($s_activity =="givetothem")
    {$s_activityname = "Given to partner"; };
    if ($s_activity =="receievedfromthem")
    {$s_activityname = "Received from partner";};
    if ($s_activity =="exchange")
    {$s_activityname = "Exchanged with partner";};
    if ($s_activity =="transferto")
    {$s_activityname = "Transfer to Partner";};
    if ($s_activity =="transferfrom")
    {$s_activityname = "Transfer from Partner";};
    if ($s_activity =="hire")
    {$s_activityname = "Hire from supplier";};
    if ($s_activity =="dehire")
    {$s_activityname = "Dehire from supplier";};
    if ($s_activity =="reqdrop")
    {$s_activityname = "Request Pallet Drop";};
    if ($s_activity =="adjustup")
    {$s_activityname = "Increase stock";};
    if ($s_activity =="adjustdown")
    {$s_activityname = "Decrease stock";};

    return $s_activityname;
}
function f_myplib1_set_activity_mirror($ps_activity)
{
    $s_activityname = "";
    $s_activityname = $ps_activity;
    $s_activity = strtolower(trim($ps_activity));

    if ($s_activity =="givetothem")
    {$s_activityname = "receievedfromthem"; };
    if ($s_activity =="receievedfromthem")
    {$s_activityname = "givetothem";};
    if ($s_activity =="exchange")
    {$s_activityname = "Exchanged with partner";};
    if ($s_activity =="transferto")
    {$s_activityname = "transferfrom";};
    if ($s_activity =="transferfrom")
    {$s_activityname = "transferto";};
    if ($s_activity =="hire")
    {$s_activityname = "dehire";};
    if ($s_activity =="dehire")
    {$s_activityname = "hire";};
    if ($s_activity =="reqdrop")
    {$s_activityname = "Request Pallet Drop";};
    if ($s_activity =="adjustup")
    {$s_activityname = "adjustdown";};
    if ($s_activity =="adjustdown")
    {$s_activityname = "adjustup";};

    return $s_activityname;
}
function f_myplib1_set_type_name ($ps_itemtype)
{
    $s_name = $ps_itemtype;
// go to the itemstypes table and get the name for the type
    return $s_name;
}
function f_myplib1_convertdata($ps_details_def,$ps_details_data)
{

echo "mylib1_convertdata def ".$ps_details_def."<br>";
echo "mylib1_convertdata data".$ps_details_data."<br>";


    $details_def=$ps_details_def;
    $details_data=$ps_details_data;

    $d_def = split("\|",$details_def);
    $d_data = split("\|",$details_data);
    $d_def_count = count($d_def);
    $d_data_count = count($d_data);
    if($d_data_count==$d_def_count)
    {
        for($i=0;$i<$d_data_count;$i++)
        {
            $data_key = $d_def[$i];
            $final_data[$data_key]=$d_data[$i];
        }
        //return myplib1::f_myplib1_stripslashes_deep($final_data);
        return $final_data;
    }
    else
    {
        for($i=0;$i<$d_data_count;$i++)
        {
            $data_key = $d_def[$i];
            $final_data[$data_key]=$d_data[$i];
        }
//        return $this->f_myplib1_stripslashes_deep($final_data);
        return $final_data;
        //return false;
    }
}
function f_myplib1_stripslashes_deep($value)
{
    //$value = is_array($value) ? array_map(array('SqlClient','stripslashes_deep'), $value) : stripslashes($value);
    //test
    $value = is_array($value) ?
        array_map(array('myplib1','f_myplib1_stripslashes_deep'), $value) :
        stripslashes($value);

    return $value;
}
function f_myplib1_load_html_screen($ps_filename,$ps_details_def,$ps_details_data)
{

    $s_filename=$ps_filename;
    $s_details_def=$ps_details_def;
    $s_details_data=$ps_details_data;

    $s_returned_details_data=myplib1::f_myplib1_convertdata($s_details_def,$s_details_data);

    @ $fp = fopen($s_filename,"a");
    flock($fp,2); //lock the file for writing

    $s_map_file_exists='Y';
    if(file_exists($s_filename))
    {
        $s_map_lines = file($s_filename);
    }
    else
    {
        $s_map_file_exists='N';
        echo "File Not Found-:".$s_filename;
        exit();
    }
    if ($s_map_file_exists=='Y')
    {
        $s_start_map='N';
        foreach($s_map_lines as $key=>$s_map_line)
        {
            $s_start_map_found=strpos($s_map_line,'start_map_here');
            if ($s_start_map_found>0 and $s_start_map<>'Y')
            {
                $s_start_map='Y';
            }
            $s_end_map_found=strpos($s_map_line,'end_map_here');
            if ($s_end_map_found>0)
            {
                $s_start_map='N';
            }
            if ($s_start_map=='Y')
            {
                $s_check_field='|%!start_';
                while (strpos($s_map_line,$s_check_field)>0)
                {
                    $s_map_field=$s_map_line;
                    $s_field_pos=strpos($s_map_field,'|%!start_');
                    $s_first_perc_pos=strpos($s_map_field,$s_check_field);
                    $s_map_line_len=strlen($s_map_line);
                    $s_map_field=substr($s_map_line,$s_first_perc_pos,$s_map_line_len);
                    $s_map_field=str_replace($s_check_field,"",$s_map_field);
                    $s_map_field=str_replace("%|","",$s_map_field);
                    $s_map_field=str_replace("<td>","",$s_map_field);
                    $s_map_field=str_replace("</td>","",$s_map_field);
                    $s_field_end_pos=strpos($s_map_field,'_!end');
                    $s_map_field=substr($s_map_field,0,$s_field_end_pos);
                    $s_map_field=trim($s_map_field);
                    $s_map_field_value=$s_returned_details_data[$s_map_field];
                    $s_map_field="|%!start_".$s_map_field.'_!end%|';
                    $s_map_line=str_replace($s_map_field,$s_map_field_value,$s_map_line);
                    $replace=substr($s_map_line,$s_field_pos,9);
                }
                $s_map_line=str_replace('%|','',$s_map_line);
                echo $s_map_line;
            }
        }
    }
}
function f_myplib1_get_type_description($ps_mytype,$ps_dbcnx)
{
/*
GW 20090506
dont use this anymore use the following

        $ssql = "SELECT * from itemtypes where mypalletid = '$ps_mypalletid' and type = '".$row["itemtype"]."' ";
        $s_temp = $objlib1->f_myplib1_get_arecord_sql_defndata($ssql,$ps_dbcnx);
        $s_typename = myplib1::f_myplib1_get_field_defndata("Item_DESCRIPTION",$s_temp,"","no","");

*/


    $s_type_description='';
    $ssql = "SELECT * from itemtypes where type = '{$ps_mytype}' ";
    $rs_temp = mysql_query($ssql,$ps_dbcnx);
    if (!$rs_temp)
    {
        //echo("<P>Error performing query: ".mysql_error()." sql = ".$ssql."</P>");
        //exit();
    }
    $rs_temp_row_cnt = mysql_num_rows($rs_temp);
    while ( $row = mysql_fetch_array($rs_temp) )
    {
        $s_type_description = $row["item_description"];
    }
    if (trim($s_type_description)=='')
    {
        $s_type_description=$ps_mytype."***";
    }

    return $s_type_description;
}
function f_myplib1_get_partner_name($ps_mypalletid,$ps_partnerid,$ps_dbcnx)
{
    $s_partner_name=$ps_partnerid;
    $ssql = "SELECT * from partners where mypalletid = '".$ps_mypalletid."' and partnerid = '{$ps_partnerid}' ";

    $rs_temp = mysql_query($ssql,$ps_dbcnx);
    if (!$rs_temp)
    {
   //     echo("<P>Error performing query: ".mysql_error()." sql = ".$ssql."</P>");
        //exit();
    }
    $rs_temp_row_cnt = mysql_num_rows($rs_temp);
    while ( $row = mysql_fetch_array($rs_temp) )
    {
        $s_partner_name = $row["partnername"];
    }
/*GW20090514
    if (trim($s_partner_name)=='')
    {
        $s_partner_name=$ps_partnerid;
    }
*/
    return $s_partner_name;
}
function f_myplib1_get_type_defndetails($ps_mypalletid, $ps_mytype,$ps_dbcnx)
{
//    echo 'A1';
    $ssql = "SELECT * from itemtypes where mypalletid = '$ps_mypalletid' and type = '$ps_mytype' ";
    $rs_temp = mysql_query($ssql,    $ps_dbcnx);
    if (!$rs_temp)
    {
        echo("<P>Error performing query: ".mysql_error()." sql = ".$ssql."</P>");
        exit();
    }
    $rs_temp_row_cnt = mysql_num_rows($rs_temp);
    while ( $row = mysql_fetch_array($rs_temp) )
    {
        $s_type_uniquekey = $row["uniquekey"];
        $s_mytype = $row["type"];
        $s_type_description = $row["item_description"];
        $s_creatednt = $row["creatednt"];
        $s_createdip = $row["createip"];
    }
    $s_returned_type_details='START|'.$s_type_uniquekey.'|'.$s_mytype.'|'.$s_type_description.'|'.$s_creatednt.'|'.$s_createdip.'|END';
    $s_returned_type_def='START|type_uniquekey|type_mytype|type_description|type_creatednt|ttoe_createip.|END';
    return $s_returned_type_def.'||DEF||'.$s_returned_type_details;
}
function f_myplib1_get_partner_details($ps_mypalletid, $ps_partnerid,$ps_dbcnx)
{
    $s_partner_name='';
    $ssql = "SELECT * from partners where partnerid = '{$ps_partnerid}' ";
    $rs_temp = mysql_query($ssql,$ps_dbcnx);
    if (!$rs_temp)
    {
        echo("<P>Error performing query: ".mysql_error()." sql = ".$ssql."</P>");
        exit();
    }
    $rs_temp_row_cnt = mysql_num_rows($rs_temp);
    while ( $row = mysql_fetch_array($rs_temp) )
    {
        $s_partner_name = $row["partnername"];
    }
    if (trim($s_partner_name)=='')
    {
        $s_partner_name=$ps_partnerid;
    }

    return $s_partner_name;
}
function f_myplib1_get_partner_defndetails($ps_mypalletid, $ps_partnerid,$ps_dbcnx)
{
    $s_partner_name='';
    $ssql = "SELECT * from partners where partnerid = '{$ps_partnerid}' ";
    $rs_temp = mysql_query($ssql,$ps_dbcnx);
    if (!$rs_temp)
    {
        echo("<P>Error performing query: ".mysql_error()." sql = ".$ssql."</P>");
        exit();
    }
    $rs_temp_row_cnt = mysql_num_rows($rs_temp);
    while ( $row = mysql_fetch_array($rs_temp) )
    {
        $s_partner_name = $row["partnername"];
    }
    if (trim($s_partner_name)=='')
    {
        $s_partner_name=$ps_partnerid;
    }
    $s_returned_type_details='START|'.$s_type_uniquekey.'|'.$s_mytype.'|'.$s_type_description.'|'.$s_creatednt.'|'.$s_createip.'|END';
    $s_returned_type_def='START|type_uniquekey|type_mytype|type_description|type_creatednt|ttoe_createip.|END';
    return $s_returned_type_def.'||DEF||'.$s_returned_type_details;
}
function f_myplib1_strip_start_end($ps_string)
{
    $s_string = "***".$ps_string."***";
    $s_string = str_replace("***START|","",$s_string);
    $s_string = str_replace("|END***","",$s_string);
    $s_string = str_replace("***start|","",$s_string);
    $s_string = str_replace("|end***","",$s_string);
    return $s_string;
}
function f_myplib1_get_field_defndata($ps_fieldname,$ps_def,$ps_data,$ps_debug,$ps_default)
{
//EG    $s_test = myplib1::f_myplib1_get_field_defndata("type_description",$s_type_def,$s_type_data,"NO");
//    echo $s_test;
    $s_outdata = "?";
    IF (strpos($ps_def,"||DEF||") === FALSE)
    {
        $sa_defs = explode("|",$ps_def);
        $sa_data = explode("|",$ps_data);
    }ELSE{
// GW20090504 split the one field around the ||def|| marker and load results to the def &  data fields
        $sa_defs = explode("||DEF||",$ps_def);
        $sa_data = $sa_defs[1];
        $sa_defs = $sa_defs[0];
        $sa_defs = explode("|",$sa_defs);
        $sa_data = explode("|",$sa_data);
    }
    for ($i = 0; $i < count($sa_defs);$i++)
    {
//        echo $i." count ".count($sa_defs)."field=".strtoupper(trim($ps_fieldname))." def=".(strtoupper($sa_defs[$i]))."<br>";

        if (strtoupper(trim($ps_fieldname)) == (strtoupper($sa_defs[$i])))
        {
            $s_outdata = $sa_data[$i];
        }
        if (strtoupper(trim($ps_debug)) == "YES")
        {
            echo "debug:".strtoupper(trim($ps_fieldname))." ".$i." def=".(strtoupper($sa_defs[$i])) ." data=".(strtoupper($sa_data[$i]))."<br>";
        }
    }

    if($s_outdata =="?")
    {
     if (trim($ps_default) !== '')
     {
        $s_outdata = $ps_default;
     }
     if (trim($ps_default) == 'BLANK')
     {
        $s_outdata = "";
     }
    }
            return $s_outdata;
}
function f_myplib1_get_mypalletid_defndata($ps_mypalletid)
{
//    ECHO "2A";
    $ssql = "SELECT * from usermaster where mypalletid = '$ps_mypalletid' ";
    $rs_temp = mysql_query($ssql);
    if (!$rs_temp)
    {
        echo("<P>Error performing query: ".mysql_error()." sql = ".$ssql."</P>");
        exit();
    }

    $s_fieldnames = "START|";
    $s_fielddata = "START|";
    while ( $row = mysql_fetch_array($rs_temp) )
    {
     foreach ( array_keys($row) AS $header )
        {
                //you have integer keys as well as string keys because of the way PHP  handles arrays.
                if ( !is_int($header) )
                {
                    $s_fieldnames .=$header."|";
                }
        }
        //print the data row
        foreach ( $row AS $key=>$value )
        {
            if ( !is_int($key) )
            {
                $s_fielddata .=$value."|";
            }
        }
    }
    $s_fieldnames .= "END";
    $s_fielddata .= "END";

    return $s_fieldnames.'||DEF||'.$s_fielddata;
}
function f_myplib1_userheading_html($ps_mypalletid)
{

    GLOBAL $objlib1;

    $s_lineout = "";
//     ECHO "1";
    $s_mypalletdefndetails = $objlib1->f_myplib1_get_mypalletid_defndata($ps_mypalletid);
    $s_username = $objlib1->f_myplib1_get_field_defndata("myusername",$s_mypalletdefndetails,"","NO","");
    $s_company =   $objlib1->f_myplib1_get_field_defndata("mycompany",$s_mypalletdefndetails,"","NO","");
    $s_lastaccess = "";
    $s_mypalletid = $ps_mypalletid;
    $s_ipaddress =  $objlib1->f_myplib1_getRealIpAddr();
    $s_lineout .="<tr><td scope='col' align='left' class='pm_head_small'>Hi ".$s_username." of ".$s_company.", ID:".$s_mypalletid." (ip:".$s_ipaddress.")</td></tr>";
//    $s_lineout .="<tr><td scope='col' align='left' class='pm_head_small'>Hi ".$s_username." of ".$s_company."</td></tr>";
//    $s_lineout .="<tr><td align='left' class='pm_head_small'>last access ".$s_lastaccess."</td></tr>";
//    $s_lineout .="<tr><td align='left' class='pm_head_small'>Your id:".$s_mypalletid." (ip:".$s_ipaddress.")</td></tr>";
    $s_lineout .="<input name='mypalletid' type='hidden' id='mypalletid' value=".$s_mypalletid." />";
    return $s_lineout;

}
function f_myplib1_get_arecord_sql_defndata($ps_sql,$ps_dbcnx,$ps_debug)
{
  // ************  do not use for any sql that will return more that 1 record - it will go ape shit  gw20090505
    $ssql = $ps_sql;
    if (trim(strtoupper($ps_debug)) == "YES")
        {echo "<br> debug:::::arecord_sql sql=".$ssql."<br>";
         echo "dbcnx=[".$ps_dbcnx."]END OF DBCNX DEBUG<br>";
         };
    $rs_temp = mysql_query($ssql,$ps_dbcnx);
    if (!$rs_temp)
    {
        echo("<P>f_myplib1_get_arecord_sql_defndata Error performing query: ".mysql_error()." sql = ".$ssql."</P>");
        exit();
    }

    $s_fieldnames = "START|";
    $s_fielddata = "START|";
    while ( $row = mysql_fetch_array($rs_temp) )
    {
     foreach ( array_keys($row) AS $header )
        {
                //you have integer keys as well as string keys because of the way PHP  handles arrays.
                if ( !is_int($header) )
                {
                    $s_fieldnames .=$header."|";
                }
        }
        //print the data row
        foreach ( $row AS $key=>$value )
        {
            if ( !is_int($key) )
            {
                $s_fielddata .=$value."|";
            }
        }
    }
    $s_fieldnames .= "END";
    $s_fielddata .= "END";

    return $s_fieldnames.'||DEF||'.$s_fielddata;
}
function f_myplib1_cryption($ps_act,$ps_value)
{
    $s_value = "";

    $s_value = $ps_value ;
    // w20090524 - need to make the encyption calc much better but this is a start
    if (trim(strtoupper($ps_act))=="ENCRYPT")
    {
        $s_value = "M".$ps_value."X";
    }

    if (trim(strtoupper($ps_act))=="DECRYPT")
    {
        $s_value = substr($ps_value,1);    //removes frist char//
        $s_value = substr($s_value,0,-1); //removes last char//
    }
    return $s_value;

    }
function f_myplib1_help_html($ps_helplocation,$ps_heading,$ps_mypalletid)
{
    $s_lineout = "";
    $s_helplocation = $ps_helplocation;
    $s_lineout .="<tr><td valign='top' class='pm_head'><table><tr>";
    $s_lineout .='<td valign="top" class="pm_head">'.$ps_heading.'</td>';
    $s_lineout .= "<td><a href=\"javascript:;\" onclick=\"getHelp('".$ps_helplocation."','".$ps_mypalletid."');\"><img src='images/q_12.gif' height='12' width='12' border='0'></a></td>";
    $s_lineout .='</tr>';
    $s_lineout .='</table><tr><td>';

    return $s_lineout;

}
function f_mylib1_page_header_html($ps_heading,$ps_mypalletid)
{
    $s_line = "";

    $s_line .= '<table width="100%" ><tr><td width="688" class="pmbanner2">'.$ps_heading.'</td><td >';
    $s_line .= '<table width="88">';
    $s_line .= myplib1::f_myplib1_userheading_html ($ps_mypalletid);
    $s_line .= '<tr><td>';
    $s_line .= '<table width="200" >';

    $s_line .= '<tr><td width=95><span class="pm_head"  bgcolor="#76EE00"><A href="my_manage.php?mypid='.$ps_mypalletid.'"<strong>Manage Me</strong></a></span></td></tr>';
    $s_line .= '</table></td></tr></table></td></tr></table>';
    return $s_line;
}
}