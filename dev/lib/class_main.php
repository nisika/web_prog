<?php
//gwtest
//gwtest + pkntest 20120724 +gjw-gittest-20120724 + pkn 20120724-2 + pkn 20120724-3 + pkn 20120724-4 + pkn 20120724-5
//require_once('lib/class_myplib1.php5');;

    class clmain
    {

function clmain_a100_do_sys_class($ps_function_name,$ps_function_params,$ps_action,$p_dbcnx,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno)
{
A100_TEMPLATE_INIT:

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_a100_do_sys_class  called from ".$ps_calledfrom;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text." ".$sys_function_name." ps_function_name = ".$ps_function_name." ps_function_params = ".$ps_function_params." ps_action = ".$ps_action." ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ");};

A199_END_TEMPLATE_INIT:
    $s_function_name = strtoupper(trim($ps_function_name," "));
    $s_function_found = "NO";
    $s_function_list = "APP FUNCTION LIST: ";
    $s_function_help = "APP FUNCTION HELP: ";
    $ar_params = array();
    $s_do_this_function = "";

    $s_action = strtoupper(trim($ps_action));
    if ($s_action == "LIST_FUNCTIONS")
    {
        goto B100_DO_FUNCTION;
    }
    if ($s_action == "FUNCTIONS_HELP")
    {
        goto B100_DO_FUNCTION;
    }
    if ($s_action == "RUN_FUNCTION")
    {
        goto B100_DO_FUNCTION;
    }
    $sys_function_out = "Error Unknown action of ".$ps_action." expecting LIST_FUNCTIONS,FUNCTIONS_HELP or RUN_FUNCTION";
    GOTO Z900_EXIT;
B100_DO_FUNCTION:
    IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text." DO FUNCTION s_action=".$s_action." "." s_function_name =".$s_function_name." ps_function_params=".$ps_function_params);};

    $ar_params = explode("^",$ps_function_params);
// ******************************************************************************************************************
B110_V800:
    $s_do_this_function = "clmain_v800_set_field_style";
    $s_function_list = $s_function_list.$s_do_this_function.", ";
    $s_function_help = $s_function_help."clmain_v800_set_field_style(field_style,map,error_list,debug,sytem set calledfrom) = extract the fst_ fields from within the map to build the styles used for errors".",<br>  ";

    if ($s_function_name != strtoupper(trim($s_do_this_function)))
    {
        GOTO B110_END;
    }
    if ($s_action == "RUN_FUNCTION")
    {
        $sys_function_out =  $this->clmain_v800_set_field_style($ar_params[0],$ar_params[1],$ar_params[2],$ar_params[3],"clmain a100");
    }
    $s_function_found = "YES";
    GOTO X900_EXIT;
B110_END:
// ******************************************************************************************************************
B120_V800:
    $s_do_this_function = "clmain_v920_error_row";
    $s_function_list = $s_function_list.$s_do_this_function.", ";
    $s_function_help = $s_function_help."clmain_v920_error_row(ps_error_text,Debug,calledfrom) = build a row with the error text in it".",<br>  ";

    if ($s_function_name != strtoupper(trim($s_do_this_function)))
    {
        GOTO B120_END;
    }
    if ($s_action == "RUN_FUNCTION")
    {
        $sys_function_out =  $this->clmain_v920_error_row($ar_params[0],$ar_params[1],"clmain a100");
    }
    $s_function_found = "YES";
    GOTO X900_EXIT;
B120_END:
// ******************************************************************************************************************

B130_V800:
    $s_do_this_function = "clmain_v930_field_startat";
    $s_function_list = $s_function_list.$s_do_this_function.", ";
    $s_function_help = $s_function_help."clmain_v930_field_startat(session_startatfield, default,debug, calledfrom) = build the value of the first field for data entry".",<br>  ";

    if ($s_function_name != strtoupper(trim($s_do_this_function)))
    {
        GOTO B130_END;
    }
    if ($s_action == "RUN_FUNCTION")
    {
        $sys_function_out =  $this->clmain_v930_field_startat($ar_params[0],$ar_params[1],$ar_params[2],"clmain a100");
    }
    $s_function_found = "YES";
    GOTO X900_EXIT;
B130_END:
// ******************************************************************************************************************

B140_START:
    $s_do_this_function = "clmain_v940_initialise_error_sessionvs";
    $s_function_list = $s_function_list.$s_do_this_function.", ";
    $s_function_help = $s_function_help."clmain_v940_initialise_error_sessionvs(ps_error_text, ps_debug, ps_calledfrom) = init the screen validation errors ".",<br>  ";

    if ($s_function_name != strtoupper(trim($s_do_this_function)))
    {
        GOTO B140_END;
    }
    if ($s_action == "RUN_FUNCTION")
    {
        $sys_function_out =  $this->clmain_v940_initialise_error_sessionvs($ar_params[0],$ar_params[1],"clmain a100");
    }
    $s_function_found = "YES";
    GOTO X900_EXIT;
B140_END:
// ******************************************************************************************************************

B150_START:
    $s_do_this_function = "clmain_v950_next_prev_row";
    $s_function_list = $s_function_list.$s_do_this_function.", ";
    $s_function_help = $s_function_help."clmain_v950_next_prev_row(ps_nex_prev_def, ps_debug, ps_calledfrom)) = build the next/prev screen html ".",<br>  ";

    if ($s_function_name != strtoupper(trim($s_do_this_function)))
    {
        GOTO B150_END;
    }
    if ($s_action == "RUN_FUNCTION")
    {
        $sys_function_out =  $this->clmain_v950_next_prev_row($ar_params[0],$ar_params[1],"clmain a100",$ps_details_def,$ps_details_data,$ps_sessionno);
    }
    $s_function_found = "YES";
    GOTO X900_EXIT;
B150_END:
// ******************************************************************************************************************


B160_START:
    $s_do_this_function = "clmain_u520_set_date_sql";
    $s_function_list = $s_function_list.$s_do_this_function.", ";
    $s_function_help = $s_function_help."clmain_u520_set_date_sql(ps_fieldname,ps_date_type,ps_debug, ps_calledfrom)) = build the sql for date filter on sql ".",<br>  ";

    if ($s_function_name != strtoupper(trim($s_do_this_function)))
    {
        GOTO B160_END;
    }
    if ($s_action == "RUN_FUNCTION")
    {
        $sys_function_out =  $this->clmain_u520_set_date_sql($ar_params[0],$ar_params[1],"NO","",$ps_details_def,$ps_details_data,$ps_sessionno);
    }
    $s_function_found = "YES";
    GOTO X900_EXIT;
B160_END:
// ******************************************************************************************************************
B170_START:
// echo "<br> test the add details function  strtoupper(trim(s_do_this_function)=".strtoupper(trim($s_do_this_function))."<br>";

    $s_do_this_function = "clmain_v945_adddetails_for_sesserr_fields";
    $s_function_list = $s_function_list.$s_do_this_function.", ";
    $s_function_help = $s_function_help."clmain_v945_adddetails_for_sesserr_fields( ps_debug, ps_calledfrom) =  add the screen fields after validation ".",<br>  ";

    if ($s_function_name != strtoupper(trim($s_do_this_function)))
    {
        GOTO B170_END;
    }
    if ($s_action == "RUN_FUNCTION")
    {
        $sys_function_out =  $this->clmain_v940_initialise_error_sessionvs($ar_params[0],"NO","clmain a100");
    }
    $s_function_found = "YES";
    GOTO X900_EXIT;
B170_END:
// ******************************************************************************************************************
B171_START:
// echo "<br> test the add details function  strtoupper(trim(s_do_this_function)=".strtoupper(trim($s_do_this_function))."<br>";
//    sys_class|v1|load_ui|clmain_v946_add_uidetails_for_sesserr_fields(NO,ow/man/ow_m_customerdet.htm)|NO|END = field names with ui_ for screens dvav2 20161107gw

    $s_do_this_function = "clmain_v946_add_uidetails_for_sesserr_fields";
    $s_function_list = $s_function_list.$s_do_this_function.", ";
    $s_function_help = $s_function_help."clmain_v946_add_uidetails_for_sesserr_fields( ps_debug, ps_calledfrom) =  add the screen fields after validation ".",<br>  ";

    if ($s_function_name != strtoupper(trim($s_do_this_function)))
    {
        GOTO B171_END;
    }
    if ($s_action == "RUN_FUNCTION")
    {
        $sys_function_out =  $this->clmain_v946_add_uidetails_for_sesserr_fields($ar_params[0],$ar_params[1]);
    }

    $s_function_found = "YES";
    GOTO X900_EXIT;
B171_END:
// ******************************************************************************************************************
B172_START:
// echo "<br> test the add details function  strtoupper(trim(s_do_this_function)=".strtoupper(trim($s_do_this_function))."<br>";
//    sys_classif|v1|url_MODE|equal|change|v1|load_ui|clmain_v947_add_uidetails_from_rundata(rec_owm_cm_db,NO,ow/man/ow_m_customerdet.htm)|no|END

    $s_do_this_function = "clmain_v947_add_uidetails_from_rundata";
    $s_function_list = $s_function_list.$s_do_this_function.", ";
    $s_function_help = $s_function_help."clmain_v947_add_uidetails_from_rundata( ps_debug, ps_calledfrom) =  add the screen fields after validation ".",<br>  ";

    if ($s_function_name != strtoupper(trim($s_do_this_function)))
    {
        GOTO B172_END;
    }
    if ($s_action == "RUN_FUNCTION")
    {
        $sys_function_out =  $this->clmain_v947_add_uidetails_from_rundata($ar_params[0],$ar_params[1],$ar_params[2],$ps_details_def,$ps_details_data);
    }

    $s_function_found = "YES";
    GOTO X900_EXIT;
B172_END:
// ******************************************************************************************************************
B180_START:
    $s_do_this_function = "clmain_u560_di_create_if_none_coheader";
    $s_function_list = $s_function_list.$s_do_this_function.", ";
    $s_function_help = $s_function_help."clmain_u560_di_create_if_none_coheader( ps_companyid,ps_debug) =  add the companies utl_data_integration record if it does not exist".",<br>  ";

    if ($s_function_name != strtoupper(trim($s_do_this_function)))
    {
        GOTO B180_END;
    }
    if ($s_action == "RUN_FUNCTION")
    {
        $sys_function_out =  $this->clmain_u560_di_create_if_none_coheader($ar_params[0],$ar_params[1],$p_dbcnx,"clmain a100",$ps_details_def,$ps_details_data,$ps_sessionno);
    }
    $s_function_found = "YES";
    GOTO X900_EXIT;
B180_END:
// ******************************************************************************************************************
B190_start:
    $s_do_this_function = "clmain_u620_time_function";
    $s_function_list = $s_function_list.$s_do_this_function.", ";
    $s_function_help = $s_function_help."clmain_u620_time_function(action(set or diff),utime_from,utime_to ( 'none' for set),debug) = set the field1 or diff between field1 and field 2 in seconds".",<br>  ";

    if ($s_function_name != strtoupper(trim($s_do_this_function)))
    {
        GOTO B190_END;
    }
    if (count($ar_params) <> 4 )
    {
       echo "<br>Not Debug - Error ".$sys_function_name." the number of parameters is ".count($ar_params)." which is wrong for ".$s_do_this_function."<br>".$s_function_help."<br> parameters supplied=".$ps_function_params;
    }

    if ($s_action == "RUN_FUNCTION")
    {
        $sys_function_out =  $this->clmain_u620_time_function($ar_params[0],$ar_params[1],$ar_params[2],$ar_params[3]);
    }
    $s_function_found = "YES";
    GOTO X900_EXIT;
B190_END:
// ******************************************************************************************************************
B200_start:
    $s_do_this_function = "clmain_u650_set_showat";
    $s_function_list = $s_function_list.$s_do_this_function.", ";
    $s_function_help = $s_function_help."clmain_u650_set_showat(screen,user,phase,contract,job_type,user_roles,debug,callfrom) = set the screens showat list".",<br>  ";
//$ps_screen,$ps_user,$ps_phase,$ps_contract,$ps_jobtype,$ps_linedebug,$ps_calledfrom,$ps_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
    if ($s_function_name != strtoupper(trim($s_do_this_function)))
    {
        GOTO B200_END;
    }
    if (count($ar_params) <> 8 )
    {
       echo "<br>Not Debug - Error ".$sys_function_name." the number of parameters is ".count($ar_params)." which is wrong for ".$s_do_this_function."<br>".$s_function_help."<br> parameters supplied=".$ps_function_params;
    }

    if ($s_action == "RUN_FUNCTION")
    {
        $sys_function_out =  $this->clmain_u650_set_showat($ar_params[0],$ar_params[1],$ar_params[2],$ar_params[3],$ar_params[4],$ar_params[5],$ar_params[6],$ar_params[7],$p_dbcnx,"NO",$ps_details_def,$ps_details_data,$ps_sessionno);
    }
    $s_function_found = "YES";
    GOTO X900_EXIT;
B200_END:
// ******************************************************************************************************************
B210_start:
    $s_do_this_function = "clmain_u660_set_userroles";
    $s_function_list = $s_function_list.$s_do_this_function.", ";
    $s_function_help = $s_function_help."clmain_u660_set_userroles(userid,debug,callfrom) = set the screens showat list".",<br>  ";
//$ps_screen,$ps_user,$ps_phase,$ps_contract,$ps_jobtype,$ps_linedebug,$ps_calledfrom,$ps_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
    if ($s_function_name != strtoupper(trim($s_do_this_function)))
    {
        GOTO B210_END;
    }
    if (count($ar_params) <> 3 )
    {
       echo "<br>Not Debug - Error ".$sys_function_name." the number of parameters is ".count($ar_params)." which is wrong for ".$s_do_this_function."<br>".$s_function_help."<br> parameters supplied=".$ps_function_params;
    }

    if ($s_action == "RUN_FUNCTION")
    {
        $sys_function_out =  $this->clmain_u660_set_userroles($ar_params[0],$ar_params[1],$ar_params[2],$p_dbcnx,"NO",$ps_details_def,$ps_details_data,$ps_sessionno);
    }
    $s_function_found = "YES";
    GOTO X900_EXIT;
B210_END:
// ******************************************************************************************************************
B220_start:
    $s_do_this_function = "clmain_u670_utl_issue_log";
    $s_function_list = $s_function_list.$s_do_this_function.", ";
    $s_function_help = $s_function_help."clmain_u670_utl_issue_log(recgroup,rectype,companyid,userid,summary,data_blob,key1,key1def,key2,key2def,key3,key3def,key4,key4def,key5,key5def) = fields to store ".",<br>  ";
    if ($s_function_name != strtoupper(trim($s_do_this_function)))
    {
        GOTO B220_END;
    }
    if (count($ar_params) <> 16)
    {
       echo "<br>Not Debug - Error ".$sys_function_name." the number of parameters is ".count($ar_params)." which is wrong for ".$s_do_this_function."<br>".$s_function_help."<br> parameters supplied=".$ps_function_params;
    }

    if ($s_action == "RUN_FUNCTION")
    {
        $sys_function_out =  $this->clmain_u670_utl_issue_log($p_dbcnx,$ar_params[0],$ar_params[1],$ar_params[2],$ar_params[3],$ar_params[4],$ar_params[5],$ar_params[6],$ar_params[7],$ar_params[8],$ar_params[9],$ar_params[10],$ar_params[11],$ar_params[12],$ar_params[13],$ar_params[14],$ar_params[15],"NO",$ps_details_def,$ps_details_data,$ps_sessionno);
    }
    $s_function_found = "YES";
    GOTO X900_EXIT;
B220_END:
// ******************************************************************************************************************
B230_start:
    $s_do_this_function = "clmain_u740_make_from_template";
    $s_function_list = $s_function_list.$s_do_this_function.", ";
    $s_function_help = $s_function_help."clmain_u740_make_from_template(table,key fieldname,template,recgroup,rectype,fields (field1:is:value::field2:is:value),debug,calledfrom)".",<br>  ";
    if ($s_function_name != strtoupper(trim($s_do_this_function)))
    {
        GOTO B230_END;
    }
    if (count($ar_params) <> 6)
    {
       echo "<br>Not Debug - Error ".$sys_function_name." the number of parameters is ".count($ar_params)." which is wrong for ".$s_do_this_function."<br>".$s_function_help."<br> parameters supplied=".$ps_function_params;
    }

    if ($s_action == "RUN_FUNCTION")
    {
        $sys_function_out =  $this->clmain_u740_make_from_template($p_dbcnx,"NO",$ps_details_def,$ps_details_data,$ps_sessionno,$ar_params[0],$ar_params[1],$ar_params[2],$ar_params[3],$ar_params[4],$ar_params[5],$ar_params[6],$ar_params[7]);
    }
    $s_function_found = "YES";
    GOTO X900_EXIT;
B230_END:
// ******************************************************************************************************************
B240_start:
    $s_do_this_function = "clmain_u750_make_pdf_file";
    $s_function_list = $s_function_list.$s_do_this_function.", ";
    $s_function_help = $s_function_help."clmain_u750_make_pdf_file(pdf_filename,map_filename),debug,calledfrom)".",<br>  ";
    if ($s_function_name != strtoupper(trim($s_do_this_function)))
    {
        GOTO B240_END;
    }
    if (count($ar_params) <> 2)
    {
       echo "<br>Not Debug - Error ".$sys_function_name." the number of parameters is ".count($ar_params)." which is wrong for ".$s_do_this_function."<br>".$s_function_help."<br> parameters supplied=".$ps_function_params;
    }
    if ($s_action == "RUN_FUNCTION")
    {
        $sys_function_out =  $this->clmain_u750_make_pdf_file($p_dbcnx,"NO",$ps_details_def,$ps_details_data,$ps_sessionno,$ar_params[0],$ar_params[1]);
    }
    $s_function_found = "YES";
    GOTO X900_EXIT;
B240_END:
// ******************************************************************************************************************
B250_start:
    $s_do_this_function = "clmain_u780_calc_time_shift";
    $s_function_list = $s_function_list.$s_do_this_function.", ";
    $s_function_help = $s_function_help."clmain_u780_calc_time_shift(calledfrom, from_time,calc_value,valid_times)".",<br>  ";

    if ($s_function_name != strtoupper(trim($s_do_this_function)))
    {
        GOTO B250_END;
    }
    if (count($ar_params) <> 4)
    {
       echo "<br>Not Debug - Error ".$sys_function_name." the number of parameters is ".count($ar_params)." which is wrong for ".$s_do_this_function."<br>".$s_function_help."<br> parameters supplied=".$ps_function_params;
    }
    if ($s_action == "RUN_FUNCTION")
    {
        $sys_function_out =  $this->clmain_u780_calc_time_shift($p_dbcnx,"NO",$ps_details_def,$ps_details_data,$ps_sessionno,$ar_params[0],$ar_params[1],$ar_params[2],$ar_params[3]);
    }
    $s_function_found = "YES";
    GOTO X900_EXIT;
B250_END:

X900_EXIT:
    if ($s_action == "LIST_FUNCTIONS")
    {
        $sys_function_out = $s_function_list;
        GOTO Z900_EXIT;
    }
    if ($s_action == "FUNCTIONS_HELP")
    {
        $sys_function_out = $s_function_help;
        GOTO Z900_EXIT;
    }

    IF ($s_function_found == "NO")
    {
       $sys_function_out = "Calling Sys_Class<br>Error Unknown function of ".$s_function_name." expecting one of ".$s_function_list;
       echo ("<br> process has been died because of a fatal error<br>function:".$s_do_this_function."<br>".$sys_function_out."<br>Valid functions<br>".$s_function_list."<br>Function help<br>".$s_function_help);
       die ("<br> process has been died because of a fatal error<br>function:".$s_do_this_function."<br>".$sys_function_out."<br>Valid functions<br>".$s_function_list."<br>Function help<br>".$s_function_help);

    }

Z900_EXIT:
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  z900_EXIT sys_function_out =  ".$sys_function_out);};
    IF ($sys_debug == "YES"){echo " <br>".$sys_function_name."  A100 returned  = ".$sys_function_out." <br>";};


    return $sys_function_out;

}
//##########################################################################################################################################################################

function clmain_a110_do_sys_class($ps_dbcnx,$ps_line,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{

    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_a110_do_sys_class";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ");};


    global $class_sql;
    global $class_apps;

A001_DEFINE_VARS:
    $ar_line_details = array();
    $s_action = "";
    $s_function = "";
    $s_function_param = "";
    $s_params = "";
    $s_field_name = "";

A100_INIT_VARS:
//    echo "<br>gw a110 clmain a100 ps_line=[]".$ps_line."[]".microtime(true);

    $ar_line_details = explode("|",$ps_line);

    if (count($ar_line_details)< 5)
    {
        echo"<br>ERROR clmain_a110:001 there are only ".count($ar_line_details)." fields on the instead of min 5 on the line ".$ps_line;
    }
//    if (count($ar_line_details)> 5)
//    {
//        echo"<br>ERROR clmain_a110:002  there are too many pipes on the line ".count($ar_line_details)." fields on the instead of  5 on the line ".$ps_line;
//    }
    $s_field_name = $ar_line_details[2];
    $s_function_param = $ar_line_details[3];
    $sys_function_out = "?clmain_a110_unknown".$ps_line."?";
    $s_line_debug = $ar_line_details[4];

    $s_function = substr($s_function_param,0,strpos($s_function_param,"("));
    $s_params = substr($s_function_param,strpos($s_function_param,"("));
    $s_params = STR_REPLACE("#P","|",$s_params);
    $s_params = STR_REPLACE("#p","|",$s_params);
    $s_params = STR_REPLACE(",","^",$s_params);
    $s_params = STR_REPLACE("(","",$s_params);
    $s_params = STR_REPLACE(")","",$s_params);
    $s_params = STR_REPLACE('"',"",$s_params);
/*    echo "<br>gw a110 clmain before v200 s_params=[]".$s_params."[]".microtime(true);;
    echo "<br> list session values ";
    foreach ($_SESSION as $key => $value){
        echo "session:{$key} = {$value}\r\n<br>";
    }
    echo "<br> end session values ";
*/

    $s_params = $this->clmain_v200_load_line( $s_params,$ps_details_def,$ps_details_data,"no",$ps_sessionno,$sys_function_name." A100_INIT_VARS");
//    echo "<br>gw a110 clmain after v200 s_params=[]".$s_params."[]".microtime(true);;
    $s_action = "RUN_FUNCTION";


//  ECHO "<BR> doing clmain <br>s_params=".$s_params;


    if (strtoupper(trim($s_field_name," ")) ==  strtoupper(trim("sys_app_function_list"," ")))
    {
        $s_action = "LIST_FUNCTIONS";
    }

    if (strtoupper(trim($s_field_name," ")) ==  strtoupper(trim("sys_app_function_help"," ")))
    {
        $s_action = "FUNCTIONS_HELP";
    }
//    echo "<br>s_function=".$s_function;
//    echo "<br>s_params=".$s_params;

B100_START:
    if (strtoupper($ar_line_details[0]) != "SYS_CLASS")
    {
        goto Z900_EXIT;
    }
    if (strtoupper($ar_line_details[1]) != "V1")
    {
        goto A1_DO_V2;
    }
C100_DO_V1:


//    echo "<br>gw a110 clmain c100 ps_line=[]".$ps_line."[]".microtime(true);;
//    echo "<br>gw a110 clmain c100 s_params=[]".$s_params."[]".microtime(true);;

    $sys_function_out = $this->clmain_a100_do_sys_class($s_function,$s_params,$s_action,$ps_dbcnx,$s_line_debug,"ut_jcl clmain_a110_c100dov1",$ps_details_def,$ps_details_data,$ps_sessionno);
    $sys_function_out = $this->clmain_v200_load_line($sys_function_out,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"ut_jcl clmain_a110c100");
    GOTO Z900_EXIT;
//    echo "doing urlvalue<br>s_field_name=".$s_field_name."<br>s_param_name=".$s_param_name."<br>sys_function_out=".$sys_function_out."<br>";
A1_DO_V2:

    die("clmain a110 error:001 this line does not have a valid version number <br> expected format SYS_CLASS|VERSION|FIELD|VALUE|DEBUG|END <BR> ps_line=[]".$ps_line."[]");

Z900_EXIT:
    if(strpos($sys_function_out,"DMAV2_FIELD_VALUES") !==false)
    {
        $sys_function_out = $sys_function_out;  // function_out is structured $s_def."|^%##%^|".$s_out
    }else{
        $sys_function_out = $s_field_name."|^%##%^|".$sys_function_out;
    }
//    $sys_function_out = $s_field_name."|^%##%^|this is working".$sys_function_out;


    IF ($sys_debug == "YES"){z901_dump( $sys_function_name."  returned  = ".$sys_function_out." ");};

    IF ($sys_debug == "YES"){echo " <br>".$sys_function_name."  A110 returned  = ".$sys_function_out." <br>";};

    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_a120_do_sys_class($ps_function_name,$ps_function_params,$par_array,$ps_action,$p_dbcnx,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno)
{
A100_TEMPLATE_INIT:

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_a120_do_sys_class  called from ".$ps_calledfrom;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text." ".$sys_function_name." ps_function_name = ".$ps_function_name." ps_function_params = ".$ps_function_params." ps_action = ".$ps_action." ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ");};

A199_END_TEMPLATE_INIT:
    if (isset($_SESSION['ko_prog_path']))
    {
    }else{
    $_SESSION['ko_prog_path'] = "";
    }

     IF ($sys_debug == "YES"){echo $sys_function_name." started debug=".$sys_debug." *** remember to view source - it will save you hours  <br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_sql.php');
     $class_sql = new wp_SqlClient();


    $s_function_name = strtoupper(trim($ps_function_name," "));
    $s_function_found = "NO";
    $s_function_list = "APP FUNCTION LIST: ";
    $s_function_help = "APP FUNCTION HELP: ";
    $ar_params = array();
    $s_do_this_function = "";
    $s_do_this_function = "NOTFOUND";

    $s_progress_message = "<br>".$sys_function_name." <br><br> Started (progress_message)<br>";

    $s_action = strtoupper(trim($ps_action));
    if ($s_action == "LIST_FUNCTIONS")
    {
        goto B100_DO_FUNCTION;
    }
    if ($s_action == "FUNCTIONS_HELP")
    {
        goto B100_DO_FUNCTION;
    }
    if ($s_action == "RUN_FUNCTION")
    {
        goto B100_DO_FUNCTION;
    }
    $sys_function_out = "Error Unknown action of ".$ps_action." expecting LIST_FUNCTIONS,FUNCTIONS_HELP or RUN_FUNCTION";
    GOTO Z900_EXIT;
B100_DO_FUNCTION:
    IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text." DO FUNCTION s_action=".$s_action." "." s_function_name =".$s_function_name." ps_function_params=".$ps_function_params);};

//20110501        echo "<br> clmain 120 function params=  ";
//        print_r($ps_function_params);
//        echo "<br> ";
//        echo "<br> ";
    $ar_params = explode("^",$ps_function_params);
// ******************************************************************************************************************


B110_V800:
C000_DO:
    $s_sql = "SELECT * from utl_preferences as utlpref where key1 = '".$s_function_name."'  and (recordgroup = 'FUNCTION'and recordtype = 'PROCESS_SPEC')";
    $i_record_count = "0";
C000_RECORD_LOOP:
    $result = $class_sql->c_sqlclient_exec_query($p_dbcnx,$s_sql);
    $s_rec_found = "NO";
    $s_numrows = mysqli_num_rows($result);
    if ($s_numrows == 0)
    {
        $s_progress_message = $s_progress_message."&nbsp;&nbsp;No utl_preferences record for sql= ".$s_sql."<br>";
        goto C200_FUNCTION_LIST;
    }
C100_GET_RECORDS:
    if ($i_record_count > $s_numrows) {
        goto C900_END;
    }

    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    IF ($row === false )
    {
        goto C900_END;
    }
    $s_rec_found = "YES";
C150_DO_RECORDS:
    $s_param_count = $this->clmain_u592_get_field_from_xml($row["data_blob"],"paramcount","NO","clmaina120","nodefault");
    $s_function_help = $this->clmain_u592_get_field_from_xml($row["data_blob"],"paramdesc","NO","clmaina120","nodefault");
    $s_array_as_param = $this->clmain_u592_get_field_from_xml($row["data_blob"],"usearrayparam_asfunctionparam","NO","clmaina120","nodefault");
    $s_do_this_function = $s_function_name;

C158_NEXT:
    $i_record_count = $i_record_count + 1;
    GOTO C100_GET_RECORDS;
C159_END:

C200_FUNCTION_LIST:
    $s_progress_message = $s_progress_message."<br> doing function list check";
    $s_function_list = "";
    $s_function_list = $s_function_list."^"."functioinnaeme|paramcount|paramdesc|parameters list";
    $s_function_list =     $s_function_list."^"."clmain_u800_utl_actions_set_list| 5 |ps_filemask,ps_filepath, ps_mapfile,ps_linedebug, ps_calledfrom|file mask, path, mapname,line debug,calledfrom";
    $s_function_list =     $s_function_list."^"."clmain_u810_utl_actions_screen| 7 | ps_mapfile,worktypeid,contract_name,key2,key3,ps_linedebug, ps_calledfrom|mapname,worktypeid,contract_name,key2,key3,line debug,calledfrom";
    $s_function_list =     $s_function_list."^"."clmain_u722_do_processv2| 7 |ps_tablename, ps_keyfield,ps_keyvalue,ps_processid, ps_ataction,   ps_line_debug, ps_calledfrom |Table name, keyfield, key vaule, process id, action, line debug, calling process";

// gw 20120107 - put new functions in at this point - ie add them to the function_list


C210_CHECK_LIST:
    $ar_function_list = explode("^",$s_function_list);
    $s_function_list_count = count($ar_function_list);
    $s_check_cnt = "0";
C220_NEXT:
    if ($s_check_cnt >= $s_function_list_count)
    {
        $s_progress_message = $s_progress_message."<br> count=".$s_check_cnt." array count = ".$s_function_list_count." ";
        goto C230_ARRAY_END;
    }
    $ar_function_dets = explode("|",$ar_function_list[$s_check_cnt]);
    $s_ar_function_name = strtoupper(trim($ar_function_dets[0]));

    $s_progress_message = $s_progress_message."<br> checking - s_ar_function_name=()".$s_ar_function_name."()  s_function_name = ()".$s_function_name."() ";
    if ($s_function_name == $s_ar_function_name)
    {
        $s_progress_message = $s_progress_message."<br> matched  s_ar_function_name=".$s_ar_function_name."  s_function_name = ".$s_function_name." ";
        $s_param_count = trim($ar_function_dets[1]);
        $s_function_help = trim($ar_function_dets[2]);
        $s_array_as_param = trim($ar_function_dets[3]);
        $s_do_this_function = $s_function_name;
        $s_progress_message = $s_progress_message."<br>  s_param_count = ".$s_param_count."  s_function_help = ".$s_function_help."  s_array_as_param = ".$s_array_as_param."  s_do_this_function = ".$s_do_this_function." s_function_name = ".$s_function_name;
        goto C230_ARRAY_END;
    }
    $s_check_cnt = $s_check_cnt + 1;
    goto C220_NEXT;
C230_ARRAY_END:

C290_END:

C900_END:
    if (trim($s_do_this_function) == "NOTFOUND")
    {
        $s_progress_message = $s_progress_message."<br> function not found s_do_this_function=".$s_do_this_function;
        goto X900_EXIT;
    }

//gw20110501     echo "<br> param count = ".$s_param_count;
//    echo "<br> s_function_help = ".$s_function_help;
//    echo "<br> s_array_as_param = ".$s_array_as_param;
// get the function name from utl_preferences
// it must exist
// check the number of params for the found function
// set the help from the found function
//function clmain_u593_get_nodes_from_xml($par_xml,$ps_nodes, $ps_debug, $ps_calledfrom, $ps_default)
    if ($s_action <> "RUN_FUNCTION")
    {
        GOTO X900_EXIT;
    }

    if ($s_param_count <> count($ar_params))
    {
        echo $sys_function_name."<br>ps_function_params=".$ps_function_params."<br>";
        die ($sys_function_name." The number of paramater count of (".count($ar_params).") for function ".$s_function_name." does not match the expected count of (".$s_param_count."). <br> Called from = ".$ps_calledfrom." and rerun");
    }

D100_DO_1PARAM:
    $s_progress_message = $s_progress_message."<br> Param count = ".$s_param_count;
D300_DO_5PARAM:
    if ($s_param_count <>"3")
    {
        goto D390_EXIT;
    }
//        if the found rec says to use the array as the field
    $s_field_or_array = $par_array;
    $s_param_1 = $ar_params[0];
    $s_param_2 = $ar_params[1];
    $s_param_3 = $ar_params[2];

    IF ($s_array_as_param == "1")
    {
        $s_param_1 = $par_array;
    }
    IF ($s_array_as_param == "2")
    {
        $s_param_2 = $par_array;
    }
    IF ($s_array_as_param == "3")
    {
        $s_param_3 = $par_array;
    }

    $sys_function_out =  $this->$s_function_name($s_param_1,$s_param_2,$s_param_3,$p_dbcnx,"NO",$ps_details_def,$ps_details_data,$ps_sessionno);
    $s_function_found = "YES";
    GOTO X900_EXIT;
D390_EXIT:
D400_DO_5PARAM:
    if ($s_param_count <>"4")
    {
        goto D490_EXIT;
    }
//        if the found rec says to use the array as the field
    $s_field_or_array = $par_array;
    $s_param_1 = $ar_params[0];
    $s_param_2 = $ar_params[1];
    $s_param_3 = $ar_params[2];
    $s_param_4 = $ar_params[3];

    IF ($s_array_as_param == "1")
    {
        $s_param_1 = $par_array;
    }
    IF ($s_array_as_param == "2")
    {
        $s_param_2 = $par_array;
    }
    IF ($s_array_as_param == "3")
    {
        $s_param_3 = $par_array;
    }
    IF ($s_array_as_param == "4")
    {
        $s_param_4 = $par_array;
    }

    $sys_function_out =  $this->$s_function_name($s_param_1,$s_param_2,$s_param_3,$s_param_4,$p_dbcnx,"NO",$ps_details_def,$ps_details_data,$ps_sessionno);
    $s_function_found = "YES";
    GOTO X900_EXIT;
D490_EXIT:
D500_DO_5PARAM:
    if ($s_param_count <>"5")
    {
        goto D590_EXIT;
    }
    $s_field_or_array = $par_array;
    $s_param_1 = $ar_params[0];
    $s_param_2 = $ar_params[1];
    $s_param_3 = $ar_params[2];
    $s_param_4 = $ar_params[3];
    $s_param_5 = $ar_params[4];

    IF ($s_array_as_param == "1")
    {
        $s_param_1 = $par_array;
    }
    IF ($s_array_as_param == "2")
    {
        $s_param_2 = $par_array;
    }
    IF ($s_array_as_param == "3")
    {
        $s_param_3 = $par_array;
    }
    IF ($s_array_as_param == "4")
    {
        $s_param_4= $par_array;
    }
    IF ($s_array_as_param == "5")
    {
        $s_param_5 = $par_array;
    }

// gw 20120107    echo "<br> s_param_1=".$s_param_1." s_param_2=".$s_param_2." s_param_3=".$s_param_3." s_param_4=".$s_param_4." s_param_5=".$s_param_5."<br> ";

    $sys_function_out =  $this->$s_function_name($s_param_1,$s_param_2,$s_param_3,$s_param_4,$s_param_5,$p_dbcnx,"NO",$ps_details_def,$ps_details_data,$ps_sessionno);
    $s_function_found = "YES";
    GOTO X900_EXIT;
D590_EXIT:
D600_DO_5PARAM:
    if ($s_param_count <>"6")
    {
        goto D690_EXIT;
    }
    $s_field_or_array = $par_array;
    $s_param_1 = $ar_params[0];
    $s_param_2 = $ar_params[1];
    $s_param_3 = $ar_params[2];
    $s_param_4 = $ar_params[3];
    $s_param_5 = $ar_params[4];
    $s_param_6 = $ar_params[5];

    IF ($s_array_as_param == "1")
    {
        $s_param_1 = $par_array;
    }
    IF ($s_array_as_param == "2")
    {
        $s_param_2 = $par_array;
    }
    IF ($s_array_as_param == "3")
    {
        $s_param_3 = $par_array;
    }
    IF ($s_array_as_param == "4")
    {
        $s_param_4= $par_array;
    }
    IF ($s_array_as_param == "5")
    {
        $s_param_5 = $par_array;
    }
    IF ($s_array_as_param == "6")
    {
        $s_param_6 = $par_array;
    }

    $sys_function_out =  $this->$s_function_name($s_param_1,$s_param_2,$s_param_3,$s_param_4,$s_param_5,$s_param_6,$p_dbcnx,"NO",$ps_details_def,$ps_details_data,$ps_sessionno);
    $s_function_found = "YES";
    GOTO X900_EXIT;
D690_EXIT:
D700_DO_5PARAM:
    if ($s_param_count <>"7")
    {
        goto D790_EXIT;
    }
    $s_field_or_array = $par_array;
    $s_param_1 = $ar_params[0];
    $s_param_2 = $ar_params[1];
    $s_param_3 = $ar_params[2];
    $s_param_4 = $ar_params[3];
    $s_param_5 = $ar_params[4];
    $s_param_6 = $ar_params[5];
    $s_param_7 = $ar_params[6];

    IF ($s_array_as_param == "1")
    {
        $s_param_1 = $par_array;
    }
    IF ($s_array_as_param == "2")
    {
        $s_param_2 = $par_array;
    }
    IF ($s_array_as_param == "3")
    {
        $s_param_3 = $par_array;
    }
    IF ($s_array_as_param == "4")
    {
        $s_param_4= $par_array;
    }
    IF ($s_array_as_param == "5")
    {
        $s_param_5 = $par_array;
    }
    IF ($s_array_as_param == "6")
    {
        $s_param_6 = $par_array;
    }
    IF ($s_array_as_param == "7")
    {
        $s_param_7 = $par_array;
    }

    $sys_function_out =  $this->$s_function_name($s_param_1,$s_param_2,$s_param_3,$s_param_4,$s_param_5,$s_param_6,$s_param_7,$p_dbcnx,"NO",$ps_details_def,$ps_details_data,$ps_sessionno);
    $s_function_found = "YES";
    GOTO X900_EXIT;
D790_EXIT:
D800_DO_5PARAM:
    if ($s_param_count <>"8")
    {
        goto D890_EXIT;
    }
    $s_field_or_array = $par_array;
    $s_param_1 = $ar_params[0];
    $s_param_2 = $ar_params[1];
    $s_param_3 = $ar_params[2];
    $s_param_4 = $ar_params[3];
    $s_param_5 = $ar_params[4];
    $s_param_6 = $ar_params[5];
    $s_param_7 = $ar_params[6];
    $s_param_8 = $ar_params[7];

    IF ($s_array_as_param == "1")
    {
        $s_param_1 = $par_array;
    }
    IF ($s_array_as_param == "2")
    {
        $s_param_2 = $par_array;
    }
    IF ($s_array_as_param == "3")
    {
        $s_param_3 = $par_array;
    }
    IF ($s_array_as_param == "4")
    {
        $s_param_4= $par_array;
    }
    IF ($s_array_as_param == "5")
    {
        $s_param_5 = $par_array;
    }
    IF ($s_array_as_param == "6")
    {
        $s_param_6 = $par_array;
    }
    IF ($s_array_as_param == "7")
    {
        $s_param_7 = $par_array;
    }
    IF ($s_array_as_param == "8")
    {
        $s_param_8 = $par_array;
    }

    $sys_function_out =  $this->$s_function_name($s_param_1,$s_param_2,$s_param_3,$s_param_4,$s_param_5,$s_param_6,$s_param_7,$s_param_8,$p_dbcnx,"NO",$ps_details_def,$ps_details_data,$ps_sessionno);
    $s_function_found = "YES";
    GOTO X900_EXIT;
D890_EXIT:
D900_DO_5PARAM:
    if ($s_param_count <>"9")
    {
        goto D990_EXIT;
    }
    $s_field_or_array = $par_array;
    $s_param_1 = $ar_params[0];
    $s_param_2 = $ar_params[1];
    $s_param_3 = $ar_params[2];
    $s_param_4 = $ar_params[3];
    $s_param_5 = $ar_params[4];
    $s_param_6 = $ar_params[5];
    $s_param_7 = $ar_params[6];
    $s_param_8 = $ar_params[7];
    $s_param_9 = $ar_params[8];

    IF ($s_array_as_param == "1")
    {
        $s_param_1 = $par_array;
    }
    IF ($s_array_as_param == "2")
    {
        $s_param_2 = $par_array;
    }
    IF ($s_array_as_param == "3")
    {
        $s_param_3 = $par_array;
    }
    IF ($s_array_as_param == "4")
    {
        $s_param_4= $par_array;
    }
    IF ($s_array_as_param == "5")
    {
        $s_param_5 = $par_array;
    }
    IF ($s_array_as_param == "6")
    {
        $s_param_6 = $par_array;
    }
    IF ($s_array_as_param == "7")
    {
        $s_param_7 = $par_array;
    }
    IF ($s_array_as_param == "8")
    {
        $s_param_8 = $par_array;
    }
    IF ($s_array_as_param == "9")
    {
        $s_param_9 = $par_array;
    }

    $sys_function_out =  $this->$s_function_name($s_param_1,$s_param_2,$s_param_3,$s_param_4,$s_param_5,$s_param_6,$s_param_7,$s_param_8,$s_param_9,$p_dbcnx,"NO",$ps_details_def,$ps_details_data,$ps_sessionno);
    $s_function_found = "YES";
    GOTO X900_EXIT;
D990_EXIT:

D900_EXIT:
    echo "<br>Error in clmain a120 - the number of paramaters is not coded for count=".$s_param_count." s_function_name=".$s_function_name." <br>";
    echo "<br> the programmer will need to modify clmain_a120_do_sys_class to cope with this number of param<br>";
//    if ($s_action == "RUN_FUNCTION")
//    {
//        $sys_function_out =  $this->$s_function_name($p_dbcnx,$ar_params[0],$ar_params[1],$ar_params[2],$ar_params[3],$ar_params[4],$ar_params[5],$ar_params[6],$ar_params[7],$ar_params[8],$ar_params[9],$ar_params[10],$ar_params[11],$ar_params[12],$ar_params[13],$ar_params[14],$ar_params[15],"NO",$ps_details_def,$ps_details_data,$ps_sessionno);
//    }
//    GOTO X900_EXIT;
B220_END:

X900_EXIT:
    if ($s_action == "LIST_FUNCTIONS")
    {
        $sys_function_out = $s_function_list;
        GOTO Z900_EXIT;
    }
    if ($s_action == "FUNCTIONS_HELP")
    {
        $sys_function_out = $s_function_help;
        GOTO Z900_EXIT;
    }

    IF ($s_function_found == "NO")
    {
       $s_progress_message = $s_progress_message."<br> function not found<br> end of progress message";
       $sys_function_out = "Calling clmain_a120_do_sys_class<br>Error Unknown function of ".$s_function_name." it is not defined in the utl_preferences table sql=(".$s_sql.")<br>".$s_progress_message;
       die ("<br> process has been died because of a fatal error<br>function:(".$s_function_name.")<br>".$sys_function_out."");
    }

Z900_EXIT:
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  z900_EXIT sys_function_out =  ".$sys_function_out);};

//    echo "<br>clmain z900 in a120  <br>sys_function_out<br>".$sys_function_out."<br><br>";

    return $sys_function_out;


}
//##########################################################################################################################################################################
function clmain_a125_do_call_sys_class($ps_dbcnx,$ps_line,$par_array,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{

    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_a125_do_call_sys_class";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ");};


    global $class_sql;
    global $class_apps;

A001_DEFINE_VARS:
    $ar_line_details = array();
    $s_action = "";
    $s_function = "";
    $s_function_param = "";
    $s_params = "";
    $s_field_name = "";

A100_INIT_VARS:
    $ar_line_details = explode("|",$ps_line);
    $s_field_name = $ar_line_details[2];
    $s_function_param = $ar_line_details[3];
    $sys_function_out = "?clmain_a110_unknown".$ps_line."?";
    $s_line_debug = $ar_line_details[4];

    $s_function = substr($s_function_param,0,strpos($s_function_param,"("));
    $s_params = substr($s_function_param,strpos($s_function_param,"("));
    $s_params = STR_REPLACE("#P","|",$s_params);
    $s_params = STR_REPLACE("#p","|",$s_params);
    $s_params = STR_REPLACE(",","^",$s_params);
    $s_params = STR_REPLACE("(","",$s_params);
    $s_params = STR_REPLACE(")","",$s_params);
    $s_params = STR_REPLACE('"',"",$s_params);
    $s_params = STR_REPLACE("/^",",",$s_params);
//
//
//ECHO "<BR> CLMAIN A125 s_function=".$s_function;
//ECHO "<BR> CLMAIN A125 PARAMS=".$s_params;
//ECHO "<BR> CLMAIN A125 ps_details_def=".$ps_details_def;
//ECHO "<BR> CLMAIN A125 ps_details_data=".$ps_details_data;
//ECHO "<BR> CLMAIN A125";//
//

    $s_params = $this->clmain_v200_load_line( $s_params,$ps_details_def,$ps_details_data,"no",$ps_sessionno,$sys_function_name."A100_INIT_VARS");
    $s_action = "RUN_FUNCTION";
    if (strtoupper(trim($s_field_name," ")) ==  strtoupper(trim("sys_app_function_list"," ")))
    {
        $s_action = "LIST_FUNCTIONS";
    }

    if (strtoupper(trim($s_field_name," ")) ==  strtoupper(trim("sys_app_function_help"," ")))
    {
        $s_action = "FUNCTIONS_HELP";
    }
//    echo "<br>s_function=".$s_function;
//    echo "<br>s_params=".$s_params;

B100_START:
    if (strtoupper($ar_line_details[0]) != "SYS_CLASSV2")
    {
        goto Z900_EXIT;
    }
    if (strtoupper($ar_line_details[1]) != "V1")
    {
        goto A1_DO_V2;
    }
C100_DO_V1:
    $ar_array = $par_array;
    $sys_function_out = $this->clmain_a120_do_sys_class($s_function,$s_params,$ar_array,$s_action,$ps_dbcnx,$s_line_debug,"ut_jcl clmain_a125_c100_dov1 ",$ps_details_def,$ps_details_data,$ps_sessionno);
//gw20110501 old line     $sys_function_out = $this->clmain_a100_do_sys_class($s_function,$s_params,$s_action,$ps_dbcnx,$s_line_debug,"ut_jcl clmain_a110b",$ps_details_def,$ps_details_data,$ps_sessionno);
//    echo "<br>after 120 iiiii<br>".$sys_function_out."<br>ddddddddd<br>";

   if (strtoupper($s_field_name) == strtoupper("jcl_get_new_def_n_data"))
    {
//    echo "<br>clmain b100 in a125 <br>sys_function_out<br>".$sys_function_out."<br><br>";
//gw20110512 -dont change sysout
    goto Z900_EXIT;

    }

//    echo "<br>clmain ddddb100 in a125 <br>sys_function_out<br>".$sys_function_out."<br><br>";

    $sys_function_out = $this->clmain_v200_load_line($sys_function_out,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"ut_jcl clmain_a110c100");
    $sys_function_out = $s_field_name."|^%##%^|".$sys_function_out;

//    echo "<br>clmain ddddb100 in a125 after v200  <br>sys_function_out<br>".$sys_function_out."<br><br>";

    goto Z900_EXIT;

A1_DO_V2:

Z900_EXIT:
//    $sys_function_out = $s_field_name."|^%##%^|this is working".$sys_function_out;
    IF ($sys_debug == "YES"){z901_dump( $sys_function_name."  returned  = ".$sys_function_out." ");};

//    echo "<br>exit of 125 iiiiiiiiiiiiiiiii<br>".$sys_function_out."<br>dddddddddddddddddddddd<br>";


    return $sys_function_out;

}
//##########################################################################################################################################################################
        // opens the file and returns its contents as a single string
function clmain_load_htmlfile($ps_filename)
{
        $filename = "";
        $s_lineout = "";

        $filename = "/var/www/html/palletbook/".$ps_filename;
        $filename = $ps_filename;
        clearstatcache();
        if(!file_exists($filename))
        {
            $s_lineout = "<br> file does not exist ".$filename."<br>";
        }else{
            $farray = file($filename);
            foreach ($farray as $line) {
                $s_lineout .= $line;
            }
        }
        return $s_lineout;
}
//##########################################################################################################################################################################
function clmain_B200_check_user($ps_dbcnx,$p_username,$p_pwd,$ps_debug,$ps_sessionno)
{
    $tmp = "";
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug;
    }
    $sys_function_name = "";
    $sys_function_name = "clmain_B200_check_user";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name." p_username = ".$p_username." p_pwd = ".$p_pwd."  ps_debug = ".$ps_debug." ");};

    global $class_sql;

    // define function specific variables and code
//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$array_line_details[$i2]." ";};

        $user_email = $p_username;
        $passwordMD5 = md5($p_pwd);
        $userId = "?unkn?";
        $companyId="?unkn?";
        $teamudac ="?unkn?";
        $email = "?unkn?";

        if (strpos($user_email,"@") === false)
        {
            if (strpos(strtoupper($user_email),"DEBUG") === false)
            {
                $user_email = $this->clmain_b201_get_email_for_logonname($ps_dbcnx,$user_email,"no",$ps_sessionno);
            }else{
                $user_email = str_replace("debug","",$user_email);
                $user_email = str_replace("DEBUG","",$user_email);
                $user_email = $this->clmain_b201_get_email_for_logonname($ps_dbcnx,$user_email,"yes",$ps_sessionno);
                die ("clmain 200_check user doing debug user_email=[]".$user_email."[]");
            }
            if ($user_email=="*ERROR")
            {
                $user_email = $p_username;
            }
        }
        // gets UserId
        $s_sql = "(SKIPERRORFAIL) SELECT * FROM Users WHERE Email='".mysqli_real_escape_string($ps_dbcnx,$user_email)."' and Email not like '%_inactive%'";
        $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_sql);

    IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql);};
    IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text." ".$sys_function_name."  user_email".$user_email." ");};

    if ($result === false)
    {
        $sys_function_out="*ERROR E2001_UNKNOWN_USER";//, "UNKNOWN_USER");
        GOTO Z900_EXIT;
    }

    if ($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
        {
            // check password, if wrong, reply with message with a payload that opens the settings screen.
            $userId = intval($row['UserId']);
            $companyId = intval($row['CompanyId']);
            $email = $row['Email'];

            $sys_function_out=$userId;

            $s_sql2 = "SELECT * FROM companies WHERE CompanyId=".$companyId."";
            $result2 = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_sql2);
            if ($row2 = mysqli_fetch_array($result2, MYSQLI_ASSOC))
            {

                $teamudac = $row2['TeamUDAC'];
            }


            if (isset($row['TemporaryPassword']) && $passwordMD5 == $row['TemporaryPassword'])
            {
                // login with temporary password - sets as actual
                $sql = "UPDATE Users SET Password='".$row['TemporaryPassword']."', TemporaryPassword=NULL WHERE UserId=".$userId." LIMIT 1";
                exec_query($sql);
            }
            else if ($passwordMD5 != $row['Password'])
            {
                if ($p_pwd != $row['Password'])
                {
//gw20140304 - add the system override password
                    if ($p_pwd != "SuperAdminOr1d")
                    {
                        // TODO: should show password screen if not showing
                        $sys_function_out = "*ERROR E2006_WRONG_PWD";//, "INCORRECT_PASSWORD");
                    }
                }
            }
            else
            {
                // login with password - clears TemporaryPassword (if any)
                $sql = "UPDATE Users SET TemporaryPassword=NULL WHERE UserId=".$userId." LIMIT 1";
//                $class_sql->c_sqlclient_exec_query($ps_dbcnx,$sql);
            }
        }
        else
        {
//            $sys_function_out=" ** ERROR **<BR> Unknown logon name '$user_email', please sign up from the iPhone to create an account <BR>.";//, "UNKNOWN_USER");
            $sys_function_out="*ERROR E2001_UNKNOWN_USER";//, "UNKNOWN_USER");
        }

    setcookie($ps_sessionno."ud_userid", $userId);
        setcookie($ps_sessionno."ud_companyid", $companyId);
        setcookie($ps_sessionno."ud_pwd", $passwordMD5);
        setcookie($ps_sessionno."ud_teamudac", $teamudac);
        setcookie($ps_sessionno."ud_email", $email);

//        echo "<br> set the cookies<br>";


        setcookie("ud_userid", $userId);
        setcookie("ud_companyid", $companyId);
        setcookie("ud_pwd", $passwordMD5);
        setcookie("ud_teamudac", $teamudac);
        setcookie("ud_email", $email);

Z900_EXIT:
    IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text.=" ".$sys_function_name." output = ****".$sys_function_out."***** ");};
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_b201_get_email_for_logonname($ps_dbcnx,$ps_user_email,$ps_debug,$ps_sessionno)
    {
        $sys_debug_text = "";
        $sys_debug = "";
        $sys_debug = strtoupper($ps_debug);
        IF ($sys_debug !="NO") {
            $sys_debug  = "YES";
            $sys_debug_text = $ps_debug;
        }
        $sys_function_name = "";
        $sys_function_name = "b201_get_email_for_logonname";
        $sys_function_out="*ERROR";
        IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
        IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name." p_username = ".$ps_user_email." p_pwd = ?p_pwd? ps_debug = ".$ps_debug." ");};

        global $class_sql;

        // gets UserId
        $s_sql = "SELECT * FROM utl_preferences where key1 = '".$ps_user_email."' and rectype = 'COHDR1'  (SKIPERRORFAIL)";

        $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_sql);

        IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql);};

        if (!$result)
        {
            IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql);};
            $sys_function_out="*ERROR";
            GOTO Z900_EXIT;
        }
        if ($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
        {
            $sys_function_out = $row['key2'];
        }
        else
        {
            $sys_function_out="*ERROR";
        }
        Z900_EXIT:
        IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text.=" ".$sys_function_name." output = ****[]".$sys_function_out."[]***** ");};
        return $sys_function_out;

    }
//##########################################################################################################################################################################
function clmain_B210_get_user_preferences($ps_dbcnx,$ps_userid,$ps_debug,$ps_sessionno)
    {
        $sys_debug_text = "";
        $sys_debug = "";
        $sys_debug = strtoupper($ps_debug);
        IF ($sys_debug !="NO") {
            $sys_debug  = "YES";
            $sys_debug_text = $ps_debug;
        }
        $sys_function_name = "";
        $sys_function_name = "clmain_B210_get_user_preferences";
        $sys_function_out = "";
        IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
        IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name." p_username = ".$ps_userid." p_pwd = ?p_pwd? ps_debug = ".$ps_debug." ");};

        global $class_sql;

        $userId = $ps_userid;

        // gets UserId
        $s_sql = "SELECT * FROM utl_preferences where key1 = ".$userId." and recordgroup = 'USERS' and recordtype = 'preferences' (SKIPERRORFAIL)";
        //$s_sql = "SELECT * FROM utl_prdddeferences where key1 = ".$userId." and recordgroup = 'USERS' and recordtype = 'preferences' ";


        $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_sql);

        IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql);};

        if (!$result)
        {
            IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql);};
            $sys_function_out="*ERROR";
            GOTO Z900_EXIT;
        }
        if ($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
        {
            // check password, if wrong, reply with message with a payload that opens the settings screen.
            $data_blob = $row['data_blob'];
            $sys_function_out=$data_blob;
        }
        else
        {
            $sys_function_out="*ERROR";
        }
        Z900_EXIT:
        IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text.=" ".$sys_function_name." output = ****".$sys_function_out."***** ");};
        return $sys_function_out;

    }
//##########################################################################################################################################################################
    function clmain_B212_get_user_preferences_for($ps_dbcnx,$ps_userid,$ps_getfor,$ps_debug,$ps_sessionno)
    {
        $sys_debug_text = "";
        $sys_debug = "";
        $sys_debug = strtoupper($ps_debug);
        IF ($sys_debug !="NO") {
            $sys_debug  = "YES";
            $sys_debug_text = $ps_debug;
        }
        $sys_function_name = "";
        $sys_function_name = "clmain_B210_get_user_preferences";
        $sys_function_out = "";
        IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
        IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name." p_username = ".$ps_userid." p_pwd = ?p_pwd? ps_debug = ".$ps_debug." ");};

        global $class_sql;

        $userId = $ps_userid;

        // gets UserId
        $s_sql = "SELECT * FROM utl_preferences where key1 = ".$userId." and recordgroup = 'USERS' and recordtype = 'preferences' (SKIPERRORFAIL)";
        //$s_sql = "SELECT * FROM utl_prdddeferences where key1 = ".$userId." and recordgroup = 'USERS' and recordtype = 'preferences' ";


        $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_sql);

        IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql);};

        if (!$result)
        {
            IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql);};
            $sys_function_out="*ERROR";
            GOTO Z900_EXIT;
        }
        if ($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
        {
            // check password, if wrong, reply with message with a payload that opens the settings screen.
            $data_blob = $row['data_blob'];
            $sys_function_out=$data_blob;
        }
        else
        {
            $sys_function_out="*ERROR";
        }
        Z900_EXIT:
        IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text.=" ".$sys_function_name." output = ****".$sys_function_out."***** ");};
        return $sys_function_out;

    }
//##########################################################################################################################################################################
function clmain_B214_get_utl_pref_user($ps_dbcnx,$ps_userid,$ps_password,$ps_debug,$ps_sessionno)
{
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug;
    }
    $sys_function_name = "";
    $sys_function_name = "clmain_B214_get_utl_pref_user";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name." p_username = ".$ps_userid." p_pwd = ?p_pwd? ps_debug = ".$ps_debug." ");};

    global $class_sql;

    $userId = $ps_userid;

    $user_email = $ps_userid;
    $passwordMD5 = md5($ps_password);
    $companyId="?unkn?";
    $teamudac ="?unkn?";
    $email = "?unkn?";



    // gets UserId
//    $s_sql = "SELECT * FROM utl_preferences where key1 = '".$userId."' and  rectype = 'USERHDR' ";
    $s_sql = "SELECT * FROM utl_preferences where key1 = ".$userId." and  rectype = 'USERHDR' (SKIPERRORFAIL)";

    $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_sql);

    IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql);};

    if (!$result)
    {
        IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql);};
        $sys_function_out="*ERROR";
        GOTO Z900_EXIT;
    }
    if ($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
    {
        // check password, if wrong, reply with message with a payload that opens the settings screen.
        $data_blob = $row['data_blob'];
        $sys_function_out=$data_blob;
    }
    else
    {
        $sys_function_out="*ERROR";
        GOTO Z900_EXIT;
    }
B100_CHECK_DETAILS:

    $companyId =  "value from blob";
    $s_password = "value from blob";
    $email = "value from blob";
    $s_user_xml = $row["data_blob"];

    $companyId = $this->clmain_u592_get_field_from_xml($s_user_xml,"companyid","NO","clmain_B214","nodefault");
    $s_password = $this->clmain_u592_get_field_from_xml($s_user_xml,"password","NO","clmain_B214a","nodefault");
    $email = $this->clmain_u592_get_field_from_xml($s_user_xml,"contactemail","NO","clmain_B214b","nodefault");

    if ( $s_password <> $ps_password)
    {
        $sys_function_out="*ERROR, password is incorrect";
        GOTO Z900_EXIT;
    }

    $s_sql2 = "SELECT * FROM companies WHERE CompanyId=".$companyId."";
    $result2 = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_sql2);
    if ($row2 = mysqli_fetch_array($result2, MYSQLI_ASSOC))
    {
        $teamudac = $row2['TeamUDAC'];
    }

C100_SET_SESSION_VARS:
    setcookie($ps_sessionno."ud_userid", $userId);
    setcookie($ps_sessionno."ud_companyid", $companyId);
    setcookie($ps_sessionno."ud_pwd", $passwordMD5);
    setcookie($ps_sessionno."ud_teamudac", $teamudac);
    setcookie($ps_sessionno."ud_email", $email);

    setcookie("ud_userid", $userId);
    setcookie("ud_companyid", $companyId);
    setcookie("ud_pwd", $passwordMD5);
    setcookie("ud_teamudac", $teamudac);
    setcookie("ud_email", $email);


    // go through the data blob
    // anything that is "setsession_name="
    // set the session Var _name to the value after =

    $xmlObj = simplexml_load_string($s_user_xml);
    $ar_Xml = $this->clmain_u599_xmlIntoArray($xmlObj);

    foreach ($ar_Xml as $key => $value)
    {
//        echo "<br>xml key=[]".$key."[] value=[]".$value."[]";
        if (strpos(strtoupper($key),"SETSESSVAR_") !== false )
        {
//            echo "<br> set a session variable named key=[]".str_replace("setsessvar_","",$key)."[] to []".$value."[]";
            $_SESSION[$ps_sessionno.str_replace("setsessvar_","",$key)] = $value;
        }
    }

//    die ("<br>gw die clmain b214");



Z900_EXIT:
    IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text.=" ".$sys_function_name." output = ****".$sys_function_out."***** ");};
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_B220_showatv1_check($ps_details_def,$ps_details_data,$ps_debug,$ps_sessionno,$ps_field_id,$ps_action,$ps_showat_listname,$ps_calledfrom)
{
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = "<br>".$ps_debug;
    }

    IF ($sys_debug =="NO") {
        if (!strpos(strtolower($ps_action),"dodebug")=== false)
        {
            $sys_debug  = "YES";
            $sys_debug_text = "<br>".$ps_action;
        }
    }

    $sys_function_name = "";
    $sys_function_name = "clmain_B220_showatv1_check";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){echo (   $sys_debug_text."=".$sys_function_name." ps_debug,ps_sessionno,ps_field_id,ps_action,ps_showat_listname,ps_calledfrom");};
    IF ($sys_debug == "YES"){echo (   $sys_debug_text."=".$sys_function_name.$ps_debug.",".$ps_sessionno.",".$ps_field_id.",".$ps_action.",".$ps_showat_listname.",".$ps_calledfrom);};

   $sys_function_out = "TRUE";
    //   echo "<BR>  CLMAIN B220 erNO REDX ror PARAMS=".$sys_debug_text."=".$sys_function_name.$ps_sessionno.",".$ps_screen_id.",".$ps_field_id.",".$ps_process_type.",".$ps_phase.",".$ps_calledfrom;
B100_SET_VALUES:
    $showat_list = $this->clmain_v300_set_variable($ps_showat_listname,$ps_details_def,$ps_details_data,$ps_debug,$ps_sessionno,"clmain_b220a ");
    $s_fieldname = strtolower($ps_field_id)."=";
    $s_action  = substr(strtoupper($ps_action),0,1);
    $s_action  = str_replace("DODEBUG","",$s_action);
    IF ($sys_debug == "YES"){echo (   $sys_debug_text."=".$sys_function_name." s_fieldname=".$s_fieldname."   s_action=".$s_action."  showat_list=".$showat_list);};
    if ($showat_list == "u650_showat = no records to use to set values- default to C")
    {
        $s_curr_value = "R";
        goto B200_CHECK_VALUES;
    }
/*

    if (strpos(strtolower($showat_list),strtolower($s_fieldname)) === false) // does not exist so muct go throuh as ok
    {
            GOTO Z900_EXIT;
    }
    $s_start_pos = strpos(strtolower($showat_list),strtolower($s_fieldname)) + strlen($s_fieldname); // start pos + lenght of name
    $s_curr_value = substr($showat_list,$s_start_pos,1);
*/

    $s_curr_value = "R";

    $s_exceptions_list = "";
    $s_hidden_list = "";
    $s_screen_default = "";

    $ar_fields = explode("^",$showat_list);

    $s_screen_default = $ar_fields[0];
    $s_curr_value = $s_screen_default;
    $s_curr_value = str_replace("SCREEN_DEFAULT=","",STRTOUPPER($s_curr_value));


    $s_exceptions_list = $ar_fields[1];
    $s_exceptions_list = str_replace("exceptions[","",$s_exceptions_list);
    $s_exceptions_list = str_replace("]","",$s_exceptions_list);


    $s_hidden_list = $ar_fields[2];
    $s_hidden_list = str_replace("hidden[","",$s_hidden_list);
    $s_hidden_list = str_replace("]","",$s_hidden_list);
    $s_hidden_list = str_replace(",","=",$s_hidden_list);


    IF ($sys_debug == "YES"){echo (   $sys_debug_text."=".$sys_function_name." s_screen_default=".$s_screen_default);};
    IF ($sys_debug == "YES"){echo (   $sys_debug_text."=".$sys_function_name." s_exceptions_list=".$s_exceptions_list);};
    IF ($sys_debug == "YES"){echo (   $sys_debug_text."=".$sys_function_name." s_hidden_list=".$s_hidden_list);};

B180_CHECK_EXCEPTION:
    IF ($sys_debug == "YES"){echo (   $sys_debug_text."=".$sys_function_name."<br> before exception test =".$s_curr_value);};
    // IF THE FIELD IS ON THE EXCEPTIONS LIST SET TO OPOSITE OF DEFAULT
    if (strpos(strtolower($s_exceptions_list),strtolower($s_fieldname)) === false) // does not exist so muct go throuh as ok
    {
        IF ($sys_debug == "YES"){echo (   $sys_debug_text."=".$sys_function_name." not in exception list - field ".strtolower($s_fieldname)." strtolower(s_exceptions_list)=".strtolower($s_exceptions_list));};
        GOTO B190_CHECK_HIDDEN;
    }
    if ($s_curr_value == "C" )
    {
        $s_curr_value = "R";
        GOTO B189_END;
    }
    if ($s_curr_value == "R" )
    {
        $s_curr_value = "C";
        GOTO B189_END;
    }
B189_END:
    IF ($sys_debug == "YES"){echo (   $sys_debug_text."=".$sys_function_name." result of exception test =".$s_curr_value);};

B190_CHECK_HIDDEN:
    // IF FIELD ON HIDDEN LIST SET TO "X"
    IF ($sys_debug == "YES"){echo (   $sys_debug_text."=".$sys_function_name." before hidden test =".$s_curr_value);};
    // IF THE FIELD IS ON THE EXCEPTIONS LIST SET TO OPOSITE OF DEFAULT
    if (strpos(strtolower($s_hidden_list),strtolower($s_fieldname)) === false) // does not exist so muct go throuh as ok
    {
        IF ($sys_debug == "YES"){echo (   $sys_debug_text."=".$sys_function_name." not in hidden list -- field ".strtolower($s_fieldname)."  strtolower(s_hidden_list)=".strtolower($s_hidden_list));};
        GOTO B199_END;
    }
    $s_curr_value = "X";
    GOTO B199_END;
B199_END:
    IF ($sys_debug == "YES"){echo (   $sys_debug_text."=".$sys_function_name." result of hidden test =".$s_curr_value);};

B200_CHECK_VALUES:
    if ($s_curr_value == "N" )
    {
        $sys_function_out = "FALSE";
        GOTO Z900_EXIT;
    }
    if ($s_curr_value <> $s_action )
    {
    IF ($sys_debug == "YES"){echo (   $sys_debug_text."=".$sys_function_name."  check value results  - s_curr_value=".$s_curr_value."   s_action=".$s_action);};
        $sys_function_out = "FALSE";
        GOTO Z900_EXIT;
    }

// gw 20111212
//   if on hidden list - dont show
// if on exception list - set to opposite of defalt


// if the field does not exist in the list then exit
// if the field exists get the status R/C/N
// if N set to false
// if field action = Read and value <> R = false
// if field action = Change and vluae <> = false
    $sys_function_out = "TRUE";
    GOTO Z900_EXIT;


Z900_EXIT:
    IF ($sys_debug == "YES"){echo ($sys_debug_text.=" ".$sys_function_name." output = ****".$sys_function_out."***** ");};
    return $sys_function_out;

}
//##########################################################################################################################################################################
// opens file and returns part of its contents populated with the values from details_data
function clmain_v100_load_html_screen($ps_filename,$ps_details_def,$ps_details_data,$ps_debug,$ps_map_set)
{

/*// gw 20120104
           if (!strpos($ps_filename,"ud_common")=== false)
           {
               die ("gwdie clmain_v100_load_html_screen set_map_path=".$ps_filename);
           }
           if (!strpos($ps_filename,"bm_adm")=== false)
           {
               die ("gwdie clmain_v100_load_html_screen 1 - map =".$ps_filename);
           }
*/

//echo "<br> clmain v100  s_filename =".$ps_filename."<br>";
    //$sys_function_out .="abc";
//    return $sys_function_out;
//    exit;
    //gw20091208 - no needed    session_start();
    $sys_function_name = "clmain_v100_load_html_screen";
    $sys_debug = "";
    $s_lineout = "";
    $s_map_debug = "NO";
    $s_map_set = "";
    $s_map_line = "";
    $s_inskip ="";
    $s_sessionno = "";
    $s_indoif = "";
    $s_inJCL = "";
    $ar_line_details = array();

    $sys_debug2 = "NO";


    $s_last_check_time = microtime(true);
    iF ($sys_debug2 == "YES"){echo "<br>        start of laod screen  CLMAIN v100  - ".(round(microtime(true) - $s_last_check_time,15));};
    $s_last_check_time = microtime(true);


    $s_map_set = strtoupper($ps_map_set);
    if ($s_map_set =="ALL")
        {$s_map_set = "MAP";}
    if ($s_map_set =="WHOLE_FILE")
        {$s_map_set = "WHOLE_FILE";}

    $s_space_on_error = "N";
    IF (strpos(strtoupper($s_map_set),"_(SPACEONERROR)") === false )
    {}else
    {
        IF ($sys_debug == "YES"){echo $sys_function_name." got the space on error flag  <br>".$s_map_set."<br>";};
        $s_space_on_error = "Y";
        $s_map_set = str_replace("_(SPACEONERROR)","",$s_map_set);
    }
    IF (strpos(strtoupper($s_map_set),"_(ZEROONERROR)") === false )
    {}else
    {
        IF ($sys_debug == "YES"){echo $sys_function_name." got the ZERO on error flag  <br>".$s_map_set."<br>";};
        $s_space_on_error = "Y";
        $s_map_set = str_replace("_(ZEROONERROR)","",$s_map_set);
    }
    IF (strpos(strtoupper($s_map_set),"_(ONERRORRETURN:ALL)") === false )
    {}else
    {
        IF ($sys_debug == "YES"){echo $sys_function_name." got the space on error flag  <br>".$s_map_set."<br>";};
        $s_space_on_error = "ALL";
        $s_map_set = str_replace("_(ONERRORRETURN:ALL)","",$s_map_set);
    }

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }

//        ECHO "<br>v100GEDEBUG "." ".$sys_function_name." ps_filename = ".$ps_filename." ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";
    $s_siteparams = $this->clmain_v300_set_variable( "%!_SITEPARAMS_!%",$ps_details_def,$ps_details_data,"NO","clmainv100a","class_main v100a");
    $s_sessionno =  SUBSTR($this->clmain_v300_set_variable( "%!_SITEPARAMS_!%",$ps_details_def,$ps_details_data,"NO","clmainv100","class_main v100"),0,3);

    //    $sys_debug = strtoupper("yes");

    IF ($sys_debug == "YES"){echo "**************************<br>";
        echo $sys_function_name."DEBUG ACTIVE - VIEW SOURCE FOR FULL DEBUG".$sys_debug_text." <BR> ps_filename=".$ps_filename."<br>ps_details_def=".$ps_details_def."<br>ps_details_data=".$ps_details_data." <br> ps_map_set =".$ps_map_set."<br>";};

    global $class_myplib1;

    $s_details_def=$ps_details_def;
    $s_details_data=$ps_details_data;

            IF ($sys_debug2 == "YES"){echo "<br>        after load screen  CLMAIN v100  - ".(round(microtime(true) - $s_last_check_time,15));};
        $s_last_check_time = microtime(true);

//GW20110429 - add array check - dataloop gets the file and puts it in an array this is passed instead of the filename
    if (is_array($ps_filename))
    {
        $array_map_lines = $ps_filename;
            IF ($sys_debug2 == "YES"){echo "<br>do file as array";};
        goto B100_DO_ARRAY;
    }
            IF ($sys_debug2 == "YES"){echo "<br>        after check do array  CLMAIN v100  - ".(round(microtime(true) - $s_last_check_time,15));};
        $s_last_check_time = microtime(true);

       $s_lineout = "";


    $s_filename=$ps_filename;
    if ( strtoupper(trim($ps_filename)) == "")
    {
        $s_lineout = "*** CLMAIN V100 - NO MAP FILE****";
        goto Z900_EXIT;
    }
    if ( strtoupper(trim($ps_filename)) == "NONE")
    {
        goto Z900_EXIT;
    }

    IF ($sys_debug == "YES"){echo "<br> clmain v100 s_filename=[]".$s_filename."<br>";};
    IF ($sys_debug == "YES"){echo "<br> clmain v100 ps_filename=[]".$ps_filename."<br>";};
    $s_filename = $this->clmain_set_map_path_n_name("",$s_filename);
    IF ($sys_debug == "YES"){echo "<br> clmain v100 s_filename=[]".$s_filename."<br>";};

// gw 20120218 - add to cater for batch jobs
    if (isset($_SESSION['sys_language']))
    {
//DONOTHING
    }else{
        $_SESSION['sys_language'] = "ENGLISH";
    }

    $sys_language =$_SESSION['sys_language'];
    if (strtoupper($sys_language)!="ENGLISH")
    {
        $s_filename2 = substr($s_filename,0,strpos($s_filename,"."))."_".$sys_language.substr($s_filename,strpos($s_filename,"."));
        IF (file_exists($s_filename2))
        {
            $s_filename = $s_filename2;
        }
    }

    //print_r($s_returned_details_data);
    IF ($sys_debug == "YES"){echo $sys_function_name."after clmain_v100_load_html_screen<br>";};

    $s_map_file_exists='Y';
    if(file_exists($s_filename))
    {
        $array_map_lines = file($s_filename);
    }
    else
    {
        $s_map_file_exists='N';
        $s_map_line =  "<br>clmain_v100_load_html_screen ##### File Not Found-:".$s_filename."<br>";
        return $s_map_line;
        exit();
    }
    IF ($sys_debug == "YES"){echo $sys_function_name."after file exists check s_map_file_exists=".$s_map_file_exists."<br>";};
    IF ($sys_debug == "YES"){echo $sys_function_name."number of lines in the file(array)=".count($array_map_lines)."<br>";};
B100_DO_ARRAY:
            IF ($sys_debug2 == "YES"){        echo "<br>        after do array  CLMAIN v100  - ".(round(microtime(true) - $s_last_check_time,15));};
        $s_last_check_time = microtime(true);
    $s_map_file_error="file exists - last note before the read process - if you see this your map may not have a start_map_here or end_map_here";

    $s_before_map ="Y";
    $s_after_map ="N";
    $s_in_map ="N";
    $s_lineout ="";
    $s_inskip ="N";
    $s_line_tag_start = "START_".strtoupper($s_map_set)."_HERE";
    $s_line_tag_end = "END_".strtoupper($s_map_set)."_HERE";
    $s_indoif = "N";
    $s_inJCL = "N";

// if whole_file is selected then all lines are put out
    if ($s_map_set =="WHOLE_FILE")
        {
         $s_before_map ="N";
         $s_after_map ="N";
         }
// page_header works from start of file
    if ($s_map_set =="PAGE_HEADER")
        {
         $s_before_map ="N";
         }
// page_footer should automatically go to eof if not end_page_footer_here found

    for ( $i = 0; $i < count($array_map_lines); $i++)
    {
//echo "debug".$array_map_lines[$i]."<br>";
// set in map debugging
            IF ($sys_debug2 == "YES"){echo "<br>        next map line y  CLMAIN v100  - ".(round(microtime(true) - $s_last_check_time,15))." line no".$i."<!-- ".$array_map_lines[$i]." -->";};
        $s_last_check_time = microtime(true);


        if (strpos(strtoupper($array_map_lines[$i]),'MAPDEBUGYES') > 0){
            $sys_debug = "YES";
            $s_lineout .= "<!-- this map has in map debugging --> ";
            $s_lineout .= "<!-- ps_filename=".$ps_filename." ps_details_def=".$ps_details_def."  ps_details_data=".$ps_details_data." --> ";
            IF ($sys_debug == "YES"){echo $sys_function_name." *****in map debugging is on for file ".$ps_filename."<br>".$array_map_lines[$i]."<br>";};
            continue;
        }
// dump all the details_def and details_data
        if (strpos(strtoupper($array_map_lines[$i]),'DETAILSDEFDUMP') > 0){
            $s_lineout .= "<!-- DEF:".$s_details_def." <BR> DATA=".$s_details_data." --> ";
            continue;
        }
// used to indicate if there is a start skip loop
        IF (strpos(strtoupper($array_map_lines[$i]),strtoupper("START_EXAMPLE_HERE")) > 0 ){
            IF ($sys_debug == "YES"){echo $sys_function_name."got the map starter start line for example<br>".$array_map_lines[$i]."<br>";};
            $s_inskip ="Y";
            continue;
        }
//gw20110206 - too many files had example_start but no example_end
//        IF (strpos(strtoupper($array_map_lines[$i]),strtoupper("EXAMPLE_START")) > 0 ){
//            IF ($sys_debug == "YES"){echo $sys_function_name."got the map starter start line for example_START<br>".$array_map_lines[$i]."<br>";};
//            $s_inskip ="Y";
//            continue;
//        }
// used to indicate if there is a end skip loop
//        IF (strpos(strtoupper($array_map_lines[$i]),strtoupper("EXAMPLE_END")) > 0 ){
//            IF ($sys_debug == "YES"){echo $sys_function_name."got the map end line for example_END<br>".$array_map_lines[$i]."<br>";};
//            $s_inskip ="N";
//            continue;
//        }
        // if before the required loop get the next line
        if ($s_inskip =="Y"){
            IF ($sys_debug == "YES"){echo $sys_function_name." ".$i." in example loop<br>";};
           continue;
        }

// used to indicate if there is a start if loop
        IF (strpos(strtoupper($array_map_lines[$i]),strtoupper("START_DOIF_HERE")) > 0 ){
            IF ($sys_debug == "YES"){echo $sys_function_name."got the map starter start line for doif<br>".$array_map_lines[$i]."<br>";};
            $s_indoif ="Y";
            continue;
        }
// used to indicate if there is a end  if loop
        IF (strpos(strtoupper($array_map_lines[$i]),strtoupper("END_DOIF_HERE")) > 0 ){
            IF ($sys_debug == "YES"){echo $sys_function_name."got the map end line for doif<br>".$array_map_lines[$i]."<br>";};
            $s_indoif ="N";
            continue;
        }
// if before the required loop get the next line
        if ($s_indoif =="Y"){
            IF ($sys_debug == "YES"){echo $sys_function_name." ".$i." in doif  loop<br>";};
           continue;
        }

// used to indicate if there is a start JCL loop
        IF (strpos(strtoupper($array_map_lines[$i]),strtoupper("START_JCL_HERE")) > 0 ){
            IF ($sys_debug == "YES"){echo $sys_function_name."got the map starter start line for jcl<br>".$array_map_lines[$i]."<br>";};
            $s_inJCL ="Y";
            continue;
        }
// used to indicate if there is a end  JCL loop
        IF (strpos(strtoupper($array_map_lines[$i]),strtoupper("END_JCL_HERE")) > 0 ){
            IF ($sys_debug == "YES"){echo $sys_function_name."got the map end line for jcl<br>".$array_map_lines[$i]."<br>";};
            $s_inJCL ="N";
            continue;
        }
// if before the required loop get the next line
        if ($s_inJCL =="Y"){
            IF ($sys_debug == "YES"){echo $sys_function_name." ".$i." in jcl  loop<br>";};
           continue;
        }

// used to ignore the hide params line
        IF (strpos(strtoupper($array_map_lines[$i]),strtoupper("HIDE_PARAMS")) > 0 ){
            IF ($sys_debug == "YES"){echo $sys_function_name." "."got the map hide params header or footer to skip <br>".$array_map_lines[$i]."<br>";};
            continue;
        }
// used to ignore the line
        IF (strpos(strtoupper($array_map_lines[$i]),strtoupper("SKIP_LINE")) !== false ){
            IF ($sys_debug == "YES"){echo $sys_function_name." "."got the map skip line command <br>".$array_map_lines[$i]."<br>";};
            continue;
        }
        IF (SUBSTR($array_map_lines[$i],0,2) == "//") {
             IF ($sys_debug == "YES"){echo $sys_function_name." "."got the map line comment // command <br>".$array_map_lines[$i]."<br>";};
             continue;
         }

        // used to indicate if we are inside the loop within the html map
        IF (strpos(strtoupper($array_map_lines[$i]),strtoupper($s_line_tag_start)) > 0 ){
            IF ($sys_debug == "YES"){echo $sys_function_name." "."got the map starter line for ".$s_line_tag_start."<br>".$array_map_lines[$i]."<br>";};
            $s_before_map ="N";
            continue;
        }
// used to indicate we are after the loop we are looking for
        IF (strpos(strtoupper($array_map_lines[$i]),strtoupper($s_line_tag_end)) > 0 ){
            IF ($sys_debug == "YES"){echo $sys_function_name." "."got the map end line for ".$s_line_tag_end."<br>".$array_map_lines[$i]."<br>";};
            $s_after_map ="Y";
            continue;
        }
                                IF ($sys_debug == "YES"){echo $sys_function_name." ".$i." line = before map = ".$s_before_map." after map = ".$s_after_map."<br>";};
// if after the required loop exit the for loop
        if ($s_after_map =="Y"){
            IF ($sys_debug == "YES"){echo $sys_function_name." ".$i." line = after = y to break <br>";};
            break;
        }
// if before the required loop get the next line
        if ($s_before_map =="Y"){
            IF ($sys_debug == "YES"){echo $sys_function_name." ".$i." line = before = y to continue <br>";};
           continue;
        }
                                IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name." this line goes out  = ".$array_map_lines[$i]."");};

// used to process_optional lines
        IF (strpos(strtoupper($array_map_lines[$i]),strtoupper("SHOW_LINE_IF")) === false )
        {
            GOTO E190_END;
        }
E100_DO_SHOW_IF:
        IF ($sys_debug == "YES"){echo $sys_function_name." "."got the map show line if command <br>".$array_map_lines[$i]."<br>";};

        $s_inline_debug = "NO";
        IF (strpos(strtoupper($array_map_lines[$i]),strtoupper("SHOW_LINE_IFDEBUG2")) !== false )
        {
            $s_inline_debug = "YES";
        }

        $ar_line_details = explode("|",$array_map_lines[$i]);
        $s_field_to_check = $ar_line_details[1];
        $s_comparison_operator = $ar_line_details[2];
        $s_compare_value  = $ar_line_details[3];
        $s_field_value = $this->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$s_inline_debug,$s_sessionno,"clmain_E100DOSHOW ");

        IF (strpos(strtoupper($s_compare_value),strtoupper("%!_")) !== false )
        {
            $s_compare_value  = $ar_line_details[3];
            $s_compare_value = $this->clmain_v300_set_variable($s_compare_value,$s_details_def,$s_details_data,$s_inline_debug,$s_sessionno,"clmain_E100DOSHOWA");
        }

        $s_true =$this->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,$s_inline_debug,"clmain_E100DOSHOW ");
        IF (strpos(strtoupper($array_map_lines[$i]),strtoupper("SHOW_LINE_IFDEBUG")) !== false )
        {
            echo "<br> clmain showlieneif s_field_to_check=".$s_field_to_check." value = ".$s_field_value."   comparison=".$s_comparison_operator." compare field=".$ar_line_details[3]." value=".$s_compare_value."  check return value s_true = *".$s_true."*<br>";
        }
        if ($s_true == "FALSE")
        {
            continue;  // goes to next in for loop
        }
/*            for ($i1_t = 4; $i1_t < sizeof($ar_line_details);$i1_t+1)
        {
             $array_map_lines[$i] = $ar_line_details[$i1_t]."|"; //strip off the if stuff and process the line
        }
*/
        $array_map_lines[$i] = $ar_line_details[4];//strip off the if stuff and process the line
        $array_map_lines[$i] = str_replace("#p","|",$array_map_lines[$i]);//strip off the if stuff and process the line
        $array_map_lines[$i] = str_replace("#P","|",$array_map_lines[$i]);//strip off the if stuff and process the line
//            echo "<br> do line  =  ". $array_map_lines[$i]."<br>";
E190_END:

// used to process_optional lines
        IF (strpos(strtoupper($array_map_lines[$i]),strtoupper("SHOW_ATV1")) === false )
        {
            GOTO E290_END;
        }
E200_DO_SHOW_ATBV1:
        IF ($sys_debug == "YES"){echo $sys_function_name." "."got the map show line if command <br>".$array_map_lines[$i]."<br>";};


        $ar_line_details = explode("|",$array_map_lines[$i]);
//show_atv1|whofor|read|jcl_screen_showat
        $s_field_id = $ar_line_details[1];
        $s_action  = $ar_line_details[2];
        $s_showat_listname = $ar_line_details[3];
// check if the user has the rights to see the line based on the screenid,field_id,process_type
        $s_true =$this->clmain_B220_showatv1_check($s_details_def,$s_details_data,$sys_debug,$s_sessionno,$s_field_id,$s_action,$s_showat_listname,"clmain_v100 e200");

        IF (strpos(strtoupper($array_map_lines[$i]),strtoupper("SHOW_LINE_ATV1DEBUG")) !== false )
        {
 // gw 20140102 comment to get clean           echo "<br> clmain SHOW_LINE_ATV1DEBUG s_screen_id=".$s_screen_id."  s_field_id=".$s_field_id."  s_process_type=".$s_process_type." check return value s_true = *".$s_true."*<br>";
        }
        if ($s_true == "FALSE")
        {
            continue;  // goes to next in for loop
        }
//        $i_ar_count =  count($ar_line_details);
//        echo "   i_ar_count=".$i_ar_count;
        /*        for ($i1_t = 4; $i1_t<$i_ar_count;$i1_t+1)
        {
             $array_map_lines[$i] = $ar_line_details[$i1_t]."|"; //strip off the if stuff and process the line
        }
*/
        $array_map_lines[$i] = $ar_line_details[4];//strip off the if stuff and process the line
        $array_map_lines[$i] = str_replace("#p","|",$array_map_lines[$i]);//strip off the if stuff and process the line
        $array_map_lines[$i] = str_replace("#P","|",$array_map_lines[$i]);//strip off the if stuff and process the line
//            echo "<br> do line  =  ". $array_map_lines[$i]."<br>";

E290_END:

// used to add a paramber to details_def
        IF (strpos(strtoupper($array_map_lines[$i]),strtoupper("ADD_DETAILS")) === false )
        {
            GOTO E390_END;
        }
E300_DO_ADD_DETAIL:
        IF ($sys_debug == "YES"){echo $sys_function_name." "."got the map ADD_DETAILS LINE  <br>".$array_map_lines[$i]."<br>";};

        global $dbcnx;

// PROCESS THE LINE TO CREATE A VARIABLE TAHT IS ADDED TO THE DETAILS DEF/DATA
        $s_temp = $this->clmain_u120_do_add_details_line($dbcnx,$array_map_lines[$i],"NO",$s_details_def,$s_details_data,$s_sessionno,$s_siteparams);

//        echo "<br> clmain e300 s_temp".$s_temp;


        if (strpos($s_temp,"|^%##%^|") ===false)
        {
        }else{
            $ar_line_details = explode("|^%##%^|",$s_temp);
            $s_details_def = $s_details_def."|".$ar_line_details[0];
            $s_details_data = $s_details_data."|".$ar_line_details[1];
            $s_temp = "";
        };
        continue;  // goes to next in for loop

E390_END:



Z800_CONTINUE:
//if the line in the required loop has no paramters, add it to output and get the next line
        IF (strpos(strtoupper($array_map_lines[$i]),"|%!") === false ){
           $s_lineout.=$array_map_lines[$i];
            $s_lineout = STR_REPLACE("&tdxdocrlf&","\r\n",$s_lineout);
            continue;
        }


                                IF ($sys_debug == "YES"){echo $sys_function_name." ".$i." line = lineout this is now trying to set a parameter<br>";};
//decode the line and insert parameters

        $s_do_screen_detline = "NO";
        IF (strpos(strtoupper($array_map_lines[$i]),strtoupper("jcl_screen_detline") ) !== false)
        {
            //echo "<br>before line <!-- $array_map_lines[$i]  --><br>";
            $s_do_screen_detline = "YES";
        }


       $s_lineout .= $this->clmain_v200_load_line( $array_map_lines[$i],$s_details_def,$s_details_data,"NO",$s_sessionno,"v100");

        IF (strpos(strtoupper($array_map_lines[$i]),strtoupper("jcl_screen_detline") ) !== false)
        {
            $s_lineout = $this->clmain_v200_load_line( $s_lineout,$s_details_def,$s_details_data,"NO",$s_sessionno,"v100");
           // echo "<br>after line <!-- $s_lineout  --><br>";
        }

    }

    IF (TRIM($s_lineout) <> "")
    {
        GOTO Z900_EXIT;
    }
    $s_lineout = "*ERROR E2005_unable to locate code set ".$ps_map_set." tag = ".$s_line_tag_start."in file ".$ps_filename;
    if ($s_space_on_error == "Y")
    {
       $s_lineout = " ";
    }
    if ($s_space_on_error == "Z")
    {
       $sys_function_out = "0";
    }
    if ($s_space_on_error == "ALL")
    {
        $sys_function_out = "ALL";
    }

Z900_EXIT:
    IF (strtoupper(trim($s_lineout)) == "NULL_LINE")
    {
       $s_lineout = " ";
    }
    IF (strtoupper(trim($ps_map_set)) == "NULL_LOOP")
    {
       $s_lineout = "";
    }
    IF ($sys_debug == "YES"){$s_lineout.= "v100 **** DEBUG ACTIVE - VIEW SOURCE   ";};
    IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name." LINEOUT  = ".$s_lineout."");};

    return $s_lineout;
}
//##########################################################################################################################################################################
function clmain_v200_load_line($ps_in_line,$ps_details_def,$ps_details_data,$ps_debug,$ps_sessionno,$ps_calledfrom)
{
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "";

//    $sys_debug = strtoupper("yes");
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }


    $sys_function_name = "";
    $sys_function_name = " clmain_v200_load_line called from ".$ps_calledfrom;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo "v200DEBUG VIEW SOURCE FOR DETAILS<br>";};
//    IF ($sys_debug == "YES"){echo "DEBUG VIEW SOURCE FOR DETAILS ".$sys_function_name." ps_in_line = ".$ps_in_line." $ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug."<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_in_line = []".$ps_in_line."[] ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};

//    ECHO "<br>v200GEDEBUG ".$sys_debug_text.=" ".$sys_function_name." ps_in_line = ".$ps_in_line." ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";

    // define function specific variables and code
    $array_line_details = array();

    $s_in_line = $ps_in_line;
B100_DO_LINE:
//gw20110412 added the #P process to allow  ut_jcl Add_utl_activity to work
   if (!strpos(strtoupper($s_in_line),"#P") === false)
    {
       $s_in_line = str_replace("#p","|",$s_in_line);
       $s_in_line = str_replace("#P","|",$s_in_line);
   }

   if (!strpos(strtoupper($s_in_line),"KILLME") === false)
    {
        DIE ("GWCLMAIN200 s_in_line=".$s_in_line);
    }

   $array_line_details = explode("|",$s_in_line);
   for ( $i2 = 0; $i2 < count($array_line_details); $i2++)
   {
        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i2." paramater line part ".$i2." = ".$array_line_details[$i2]." ";};
        IF (strpos(strtoupper($array_line_details[$i2]),"%!") === false){
           IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i2." no parameters in ".$i2." = ".$array_line_details[$i2]." ";};
           $sys_function_out.=$array_line_details[$i2];
           continue;
        }
        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i2." paramater ".$i2." = ".$array_line_details[$i2]." ";};
//                           <td><input type="text" size="12" maxlength="12" name="ln" value="|%!start_USERNAME_!end%|"></td>
        $sys_function_out .= $this->clmain_v300_set_variable( $array_line_details[$i2],$ps_details_def,$ps_details_data,$sys_debug,$ps_sessionno,"class main v200 line=[]".$ps_in_line."[] from=[]".$ps_calledfrom."[]");
    }

// gw20110526 if the output has field in it repocess to remove
   if (!strpos(strtoupper($s_in_line),"#P") === false)
    {

        echo "<br> clmain b100doline s_in_line=".$s_in_line."<br>";
       GOTO B100_DO_LINE;
   }
   if (!strpos(strtoupper($s_in_line),"#p") === false)
    {

        echo "<br> clmain b100dolinelc s_in_line=".$s_in_line."<br>";
       GOTO B100_DO_LINE;
   }

    $sys_function_out = STR_REPLACE("&dopipe&","|",$sys_function_out);
    $sys_function_out = STR_REPLACE("&tdxdocrlf&","\r\n",$sys_function_out);
//gw20140304 - add the doappclfun process
D100_DOAPPCLFUN_FUNCTION:
    if (strpos(strtoupper($sys_function_out),"DOAPPCLFUN_") === false)
    {
        GOTO D900_END;
    }
    $s_field_value = TRIM($sys_function_out);
    $s_field_value = substr($s_field_value,strpos($s_field_value,"(")+1);
    $s_format_field = TRIM($sys_function_out);
    $s_format_field = substr($s_format_field,0,strpos($s_format_field,"("));

//    die("gwdie clmainv200 d100 s_format_field=[]".$s_format_field."[] strpos(sys_function_out,'(')=[]".strpos($sys_function_out,'(')."[] sys_function_out=[]".$sys_function_out."[]");

    $sys_function_out = $this->clmain_v750_application_class_value($s_field_value,$s_format_field,"NO","v200_LOAD_line",$ps_details_def,$ps_details_data,$ps_sessionno);

D900_END:

    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ";};
    IF ($sys_debug == "YES"){$sys_function_out.=" <!--".$sys_debug_text."-->";};

    return $sys_function_out;
}
//##########################################################################################################################################################################
function clmain_v300_set_variable($ps_array_line_details,$ps_details_def,$ps_details_data,$ps_debug,$ps_sessionno,$ps_calledfrom)
{
//define fields
    $sys_function_out = "";
    $array_def = array();
    $array_data = array();
    $s_details_def = "";
    $s_details_data = "";
    $s_format_field ="";
    $s_value_set = "";

    $i2 = 0;

    $sys_function_name = "";
    $sys_function_name = "clmain_v300_set_variable called from ".$ps_calledfrom;

    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);


    IF ($sys_debug == "YES"){echo "<br>";};
    IF ($sys_debug == "YES"){echo $sys_function_name." ps_array_line_details = ".$ps_array_line_details." ps_details_def = ".$ps_details_def." ps_details_data = ".$ps_details_data." ps_debug = ".$ps_debug."<br>";};


    $s_fieldname = "";
    $s_fieldname = strtoupper($ps_array_line_details);

//initialise fields
    $s_value_set = "N";


    $s_format_field ="N";
    IF (strpos(strtoupper($s_fieldname),"_#") > 0 )
    {
        IF ($sys_debug == "YES"){echo $sys_function_name." got a field format happening <br>".$s_fieldname."<br>";};
        $s_format_field = "Y";
    }

    $s_space_on_error = "N";
    IF (strpos(strtoupper($s_fieldname),"_(SPACEONERROR)_") === false )
    {}else
    {
        IF ($sys_debug == "YES"){echo $sys_function_name." got the space on error flag  <br>".$s_fieldname."<br>";};
        $s_space_on_error = "Y";
        $s_fieldname = str_replace("_(SPACEONERROR)","",$s_fieldname);
    }
//gw20110423 - added no brackets
    IF (strpos(strtoupper($s_fieldname),"_SPACEONERROR_") === false )
    {}else
    {
        IF ($sys_debug == "YES"){echo $sys_function_name." got the space on error no brackets flag  <br>".$s_fieldname."<br>";};
        $s_space_on_error = "Y";
        $s_fieldname = str_replace("_SPACEONERROR","",$s_fieldname);
    }
    IF (strpos(strtoupper($s_fieldname),"_(ZEROONERROR)_") === false )
    {}else
    {
        IF ($sys_debug == "YES"){echo $sys_function_name." got the ZERO on error flag  <br>".$s_fieldname."<br>";};
        $s_space_on_error = "Z";
        $s_fieldname = str_replace("_(ZEROONERROR)","",$s_fieldname);
    }
//gw20110423 - added no brackets
    IF (strpos(strtoupper($s_fieldname),"_ZEROONERROR_") === false )
    {}else
    {
        IF ($sys_debug == "YES"){echo $sys_function_name." got the ZERO on error no brackets flag  <br>".$s_fieldname."<br>";};
        $s_space_on_error = "Z";
        $s_fieldname = str_replace("_ZEROONERROR","",$s_fieldname);
    }
    IF (strpos(strtoupper($s_fieldname),"_(ONERRORRETURN:ALL)") === false )
    {}else
    {
        IF ($sys_debug == "YES"){echo $sys_function_name." got the space on error flag  <br>".$s_fieldname."<br>";};
        $s_space_on_error = "ALL";
        $s_fieldname = str_replace("_(ONERRORRETURN:ALL)","",$s_fieldname);
    }

    $s_skip_field_count_error = "N";
    IF (strpos(strtoupper($s_fieldname),"_SKIP_COUNT_ERR") === false )
    {}else
    {
        IF ($sys_debug == "YES"){echo $sys_function_name." skip error if field count wrong  <br>".$s_fieldname."<br>";};
        $s_skip_field_count_error = "Y";
        $s_fieldname = str_replace("_SKIP_COUNT_ERR","",$s_fieldname);
    }

// a field is |%!_fieldname_!%| or |%!START_fieldname_!END%| (to be backward compatible)
    $s_fieldname = str_replace("!START_","",$s_fieldname);
    $s_fieldname = str_replace("_!END","",$s_fieldname);
    $s_fieldname = str_replace("!_","",$s_fieldname);
    $s_fieldname = str_replace("_!","",$s_fieldname);
    $s_fieldname = str_replace("!","",$s_fieldname);
    $s_fieldname = str_replace("%","",$s_fieldname);
    $s_fieldname = str_replace("|","",$s_fieldname);


    if ($s_format_field !== "N")
    {
        $s_format_field =$s_fieldname;
        $s_format_field = substr($s_format_field,1,(strpos($s_format_field,"#",1)-1));
        $s_fieldname = substr($s_fieldname,(strpos($s_fieldname,"#",1)+2));
        //echo $s_format_field." ".$s_fieldname."<BR>";
    }

    IF ($sys_debug == "YES"){echo $sys_function_name." -  stripped down fieldname =".$s_fieldname."<br>";};

    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;

    if (trim($s_details_def) == "")
    {
        $s_details_def = "START|none|END|";
    }
    IF(TRIM($s_details_data) == "")
    {
        $s_details_data = "START| |END|";
    }

    IF ($s_fieldname ==  "ALL_DETAILS_DEF")
    {
       $sys_function_out=$s_details_def;
       $s_value_set = "AT_1";
    }
    IF ($s_fieldname ==  "ALL_DETAILS_DATA")
    {
       $sys_function_out=$s_details_data;
       $s_value_set = "AT_2";
    }

    IF (strtoupper("url_sessionno") == $s_fieldname)
    {
       $sys_function_out=$ps_sessionno;
       $s_value_set = "AT_0url";
       GOTO Z900_EXIT;
    }
    IF (strtoupper("time()") == $s_fieldname)
    {
       $sys_function_out=time();
       $s_value_set = "AT_timel";
       GOTO Z900_EXIT;
    }

    IF ($s_fieldname ==  "TDX_COPYRIGHT")
    {
        $sys_function_out="&copy;".date("Y")." The Data Exchange Pty Ltd (Australia) ";
        $s_value_set = "AT_cpy";
    }

C100_DEF:

//INSERT A DEFINED VALUE
    $array_def = explode("|",$s_details_def);
    $array_data = explode("|",$s_details_data);

    if (count($array_def)<> count($array_data))
    {
       if($s_skip_field_count_error <> "Y")
       {
           if(isset ($_SESSION['ko_log_clmain_v300_field_count_errors'])===false) {
               if ($sys_function_out == ""){
                   $sys_function_out="E2008 - details def count does not match details data";
                   $s_fieldname ==  "ERROR_DATA_MISMATCH";
                   $s_value_set = "AT_3";
                   echo "clmain v300 _ def/data field count error start of list  - def count=".count($array_def)." data count = ".count($array_data)." <br>CALLED FROM ".$ps_calledfrom."<br>";
                   echo "s_fieldname =".$s_fieldname."  <br>";
                   echo "s_details_def =".$s_details_def."  <br>";
                   echo "s_details_data =".$s_details_data."  <br>";

                   $s_count = count($array_def);
                   if (count($array_data) > $s_count)
                   {
                       $s_count = count($array_data);
                   }
                   $ar_dd = explode("|",$s_details_def);
                   $ar_ddata = explode("|",$s_details_data);
                   for ( $i2 = 0; $i2 < $s_count; $i2++)
                   {
                       echo "s_details_def ".$i2." =".$ar_dd[$i2]."=data =".$ar_ddata[$i2]."  <br>";
                   }
                   echo "clmain v300 _ def/data field count error end of list <br>";
                   die(" class_main.php clmain_v300_set_variable fatal error:");
               }
           }else{
//                echo "<br>clmain v300 field mismatch s_fieldname =[]".$s_fieldname."[]" ;
           }
       }
    }

    for ($i2 = 0; $i2 < count($array_def); $i2++)
    {
        IF ($sys_debug == "YES"){echo $sys_function_name." - ".$i2." details def ".$i2." = ".$array_def[$i2]." = data of ".$array_data[$i2]."<br>";};
        IF (strtoupper($array_def[$i2]) == $s_fieldname){
           $sys_function_out=$array_data[$i2];
           $s_value_set = "AT_4";

//gw20110521  wanted to load the dowaht field witht fields adjusted back to their normal
///*  this did not work
           if (strpos($sys_function_out,"#start_field#") !== false)
           {
               goto C150_adjust_field_n_skip;
           }
           if (strpos($sys_function_out,"#end_field#") !== false)
           {
               goto C150_adjust_field_n_skip;
           }
           if (strpos($sys_function_out,"#quote#") !== false)
           {
               goto C150_adjust_field_n_skip;
           }
           goto C160_CONTINUE;
C150_adjust_field_n_skip:
                $sys_function_out = str_replace("#start_field#","#p%!_",$sys_function_out);
                $sys_function_out = str_replace("#start_field#","#P%!_",$sys_function_out);
                $sys_function_out = str_replace("#start_field#","%!_",$sys_function_out);
                $sys_function_out = str_replace("#start_field#","%!_",$sys_function_out);
                $sys_function_out = str_replace("#end_field#","_!%#p",$sys_function_out);
                $sys_function_out = str_replace("#end_field#","_!%#P",$sys_function_out);
                $sys_function_out = str_replace("#end_field#","_!end%",$sys_function_out);
                $sys_function_out = str_replace("#end_field#","_!END%",$sys_function_out);
                $sys_function_out = str_replace("#quote# ",'" ',$sys_function_out);
                $sys_function_out = str_replace("#quote#",'"',$sys_function_out);
                break;
C160_CONTINUE:

         // gw 20100401   the value returned from the def/data process could be a session, cookie or snippet name - need to process the result as if it was a field
            $s_fieldname=$sys_function_out;

           break;
        }
        IF ($s_fieldname ==  "DETAILS_DEF_DUMP")
        {
            $sys_function_out.=$array_def[$i2]." = ".$array_data[$i2]."<BR>";
            $s_value_set = "AT_5";
        }
   }

    IF ($sys_debug == "YES"){echo $sys_function_name." -  stripped down fieldname1 =".$s_fieldname."<br>";};

C200_SESSION:
//INSERT A SESSION VARIABLE
    IF (SUBSTR($s_fieldname,0,8) == "SESSION_")
    {
        $sys_function_out = $s_fieldname;
       $s_value_set = "AT_6";
    }
    $s_sysfield = "";
    IF (SUBSTR($sys_function_out,0,8) == "SESSION_")
    {
        $s_sysfield = SUBSTR($sys_function_out,8);
        $sys_function_out = $this->clmain_v500_set_session_var($s_sysfield,"NO",$s_details_def,$s_details_data,$ps_sessionno);
        $s_value_set = "AT_7";
       If (SUBSTR($sys_function_out,0,6) == "ERROR^")
        {
            if ($s_space_on_error == "Y")
            {
               $sys_function_out = " ";
               GOTO Z900_EXIT;
            }
            if ($s_space_on_error == "Z")
            {
               $sys_function_out = "0";
               GOTO Z900_EXIT;
            }
            if ($s_space_on_error == "ALL")
            {
                $sys_function_out = "ALL";
                GOTO Z900_EXIT;
            }
//        echo "v500 error  ".$sys_function_out;
           $array_error = explode("^",$sys_function_out);
            for ($i2 = 0; $i2 < count($array_error); $i2++)
            {
//                if (count($array_error) <> 2 )
//                {
//                   echo "error with s_sysfield=".$s_sysfield." out = ".$sys_function_out;
//                }

                $s_fieldname = $array_error[1];
                $s_details_def = $array_error[2];
//gw20110926                 $s_details_data = $array_error[3];

            }
        }
    GOTO Z900_EXIT;
    }
C300_SESSION_ERR:
//INSERT A SESSION_ERROR_DEFINED VALUE
    IF (SUBSTR($s_fieldname,0,8) <> "SESSERR_")
    {
        GOTO C390_EXIT;
    }
    IF (ISSET($_SESSION['validate_error_screenvalues_def']))
    {
        GOTO C310_DO_VALUE;
    }
    GOTO C390_EXIT;
C310_DO_VALUE:
    if (trim($_SESSION['validate_error_screenvalues_def'])=="")
    {
           $sys_function_out="E2008 - no error values to load";
           $s_fieldname ==  "ERROR_NO VALUE";
           $s_value_set = "AT_7e";
    }
    $array_def = explode("|",$_SESSION['validate_error_screenvalues_def']);
    $array_data = explode("|",$_SESSION['validate_error_screenvalues_data']);

//echo " s_fieldname=[]".$s_fieldname."[]<br>";
//echo " s_fieldname=[]".$s_fieldname."[]<br>";
//ECHO" array_def=[]".print_r( $array_def)."[]<BR>";
//ECHO" array_data =[]".print_r( $array_data)."[]<BR>";

    if (count($array_def)<> count($array_data))
    {
        if ($sys_function_out == ""){
           $sys_function_out="E2008 - details def count does not match details data for session errors";
           $s_fieldname ==  "ERROR_DATA_MISMATCH";
           $s_value_set = "AT_7b";
        }
    }

    for ($i2 = 0; $i2 < count($array_def); $i2++)
    {
        IF ($sys_debug == "YES"){echo $sys_function_name." - ".$i2." details def ".$i2." = ".$array_def[$i2]." = data of ".$array_data[$i2]."<br>";};
        IF (strtoupper($array_def[$i2]) == $s_fieldname){
           $sys_function_out=$array_data[$i2];
           $s_value_set = "AT_7c";
           break;
        }
        IF ($s_fieldname ==  "DETAILS_DEF_DUMP")
        {
            $sys_function_out.=$array_def[$i2]." = ".$array_data[$i2]."<BR>";
            $s_value_set = "AT_7d";
        }
   }

    IF ($sys_debug == "YES"){echo $sys_function_name." -  stripped down fieldname2 =".$s_fieldname."<br>";};
    GOTO Z900_EXIT;
C390_EXIT:

C400_COOKIE:
//INSERT A COOKIE VARIABLE
    IF (SUBSTR($s_fieldname,0,7) == "COOKIE_")
    {
        $sys_function_out = $s_fieldname;
       $s_value_set = "AT_8";
    }
    $s_sysfield = "";
    IF (SUBSTR($sys_function_out,0,7) == "COOKIE_")
    {
        $s_sysfield = SUBSTR($sys_function_out,7);
        $sys_function_out = $this->clmain_v600_set_cookie_var($s_sysfield,"NO",$s_details_def,$s_details_data,$ps_sessionno);
       $s_value_set = "AT_9";
       If (SUBSTR($sys_function_out,0,6) == "ERROR^")
        {
           $array_error = explode("^",$sys_function_out);
            for ($i2 = 0; $i2 < count($array_error); $i2++)
            {
                $s_fieldname = $array_error[1];
                $s_details_def = $array_error[2];
                $s_details_data = $array_error[3];
            }
        }
    GOTO Z900_EXIT;
    }
C500_SERVER:
//INSERT A SERVER VARIABLE
    IF (SUBSTR($s_fieldname,0,7) == "SERVER_")
    {
        $sys_function_out = $s_fieldname;
       $s_value_set = "AT_8";
    }
    $s_sysfield = "";
    IF (SUBSTR($sys_function_out,0,7) == "SERVER_")
    {

        $s_sysfield = SUBSTR($sys_function_out,7);
        $sys_function_out = $this->clmain_v650_set_SERVER_var($s_sysfield,"NO",$s_details_def,$s_details_data,$ps_sessionno);
       $s_value_set = "AT_9";
       If (SUBSTR($sys_function_out,0,6) == "ERROR^")
        {
           $array_error = explode("^",$sys_function_out);
            for ($i2 = 0; $i2 < count($array_error); $i2++)
            {
                $s_fieldname = $array_error[1];
                $s_details_def = $array_error[2];
                $s_details_data = $array_error[3];
            }
        }
    GOTO Z900_EXIT;
    }

C600_OSYSVAL:
//INSERT A system value
    IF (SUBSTR($s_fieldname,0,8) == "OSYSVAL_")
    {
        $sys_function_out = $s_fieldname;
       $s_value_set = "AT_10";
    }
    $s_sysfield = "";
    IF (SUBSTR($sys_function_out,0,8) == "OSYSVAL_")
    {
        $s_sysfield = SUBSTR($sys_function_out,8);

        IF ($sys_debug == "YES"){echo $sys_function_name." -  doing the c600_osysval for =".$s_fieldname."  ps_debug=".$ps_debug."<br>";};

        $sys_function_out = $this->clmain_v400_set_system_var($s_sysfield,$ps_debug,$ps_sessionno);
//        $sys_function_out = $this->clmain_v400_set_system_var($s_sysfield,"no",$ps_sessionno);
       $s_value_set = "AT_11";
       GOTO Z900_EXIT;
    }

C700_INSERTCODE:
//INSERT A CODE SNIPPET

//gw20110115 - this did not work - not sure why echo "<br> gwclmain c700 s_fieldname=".$s_fieldname;
    IF (SUBSTR($s_fieldname,0,11) <> "INSERTCODE_")
    {
        GOTO C790_END;
    }
    $sys_function_out = $s_fieldname;
    $s_value_set = "AT_12";
    $s_code_group = "";
    $s_hash_pos = "";
    $s_filename = SUBSTR($sys_function_out,11);
    $s_code_group = "ALL";
    $s_hash_pos = strpos(STRTOUPPER(TRIM($s_filename," ")),"_SNIPPET_");
    IF ($s_hash_pos > 0 )
    {
        $s_code_group = substr($s_filename,$s_hash_pos);
        $s_filename = trim(str_replace($s_code_group,"",$s_filename));
        $s_code_group = str_replace("_SNIPPET_","",$s_code_group);
        $s_code_group = str_replace("_snippet_","",$s_code_group);
    }
    $s_filename .= "_snippet.html";
    $s_filename = trim(str_replace("__","_",strtolower($s_filename)));
    $sys_function_out  = "DO THE INSERTCODE<br>".$s_filename." s_code_group".$s_code_group." s_hash_pos =".$s_hash_pos." ";
    $sys_function_out = $this->clmain_v100_load_html_screen($this->clmain_set_map_path_n_name("",$s_filename),$s_details_def,$s_details_data,"no",$s_code_group);
    $s_value_set = "AT_13";
C790_END:

C800_RUNDATASQL:
//INSERT A rundatsql value dEFINED VALUE
    IF (SUBSTR($s_fieldname,0,11) <> "RUNDATASQL_")
    {
        GOTO C890_END;
    }
    IF (ISSET($_SESSION['rundata_sql_def']))
    {
        GOTO C810_DO_VALUE;
    }
    GOTO C890_END;
C810_DO_VALUE:
//echo  "gwetst2".$s_fieldname;

    if (trim($_SESSION['rundata_sql_def'])=="")
    {
           $sys_function_out="E2008 - no error values to load";
           $s_fieldname ==  "ERROR_NO VALUE";
           $s_value_set = "AT_7f";
    }
    $array_def = explode("|",$_SESSION['rundata_sql_def']);
    $array_data = explode("|",$_SESSION['rundata_sql_data']);

    if (count($array_def)<> count($array_data))
    {
        if ($sys_function_out == ""){
           $sys_function_out="E2008 - details def count does not match details data for session errors";
           $s_fieldname ==  "ERROR_DATA_MISMATCH";
           $s_value_set = "AT_7fa";
        }
    }

    for ($i2 = 0; $i2 < count($array_def); $i2++)
    {
        IF ($sys_debug == "YES"){echo $sys_function_name." - ".$i2." details def ".$i2." = ".$array_def[$i2]." = data of ".$array_data[$i2]."<br>";};
        IF (strtoupper($array_def[$i2]) == $s_fieldname){
           $sys_function_out=$array_data[$i2];
           $s_value_set = "AT_7fc";
           break;
        }
        IF ($s_fieldname ==  "DETAILS_DEF_DUMP")
        {
            $sys_function_out.=$array_def[$i2]." = ".$array_data[$i2]."<BR>";
            $s_value_set = "AT_7fd";
        }
   }

    IF ($sys_debug == "YES"){echo $sys_function_name." -  stripped down fieldname3 =".$s_fieldname."<br>";};
    GOTO Z900_EXIT;
C890_END:

C900_SECURITYOPTION:
//INSERT A app security option value
//SECURITYOPTION_PRODUCTION_QUANTITY_MAINT
    IF (SUBSTR($s_fieldname,0,15) <> "SECURITYOPTION_")
    {
        GOTO C990_END;
    }
    $sys_function_out = $s_fieldname;
    $s_value_set = "C900_10";

    IF ($sys_debug == "YES"){echo $sys_function_name." -  doing the c900_SECURITYOPTION for =".$s_fieldname."  ps_debug=".$ps_debug."<br>";};

    $sys_function_out = $this->clmain_v760_application_security_value($s_fieldname,"NO","CLMAIN c900_securityoption",$ps_details_def,$ps_details_data,$ps_sessionno);
//        $sys_function_out = $this->clmain_v400_set_system_var($s_sysfield,"no",$ps_sessionno);
       $s_value_set = "C900_11";
       GOTO Z900_EXIT;
C990_END:
D100_DO_APCLV2:
    IF (SUBSTR($s_fieldname,0,8) <> "APPCLV2_")
    {
        GOTO D190_END;
    }
    $s_format_field = "";
    $sys_function_out = $this->clmain_v755_application_class_valueV2("",$s_fieldname,"NO","v755",$ps_details_def,$ps_details_data,$ps_sessionno);
    //       DIE("die CLMAIN APPCL2[]".$sys_function_out."[]");
D190_END:

Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_function_name." return value at def ".$i2." s_fieldname ".$s_fieldname." s_lineout= ".$sys_function_out."<br>";};

    if ($s_format_field !== "N")
    {
        //do the formating
        $sys_function_out = $this->clmain_v700_format_field($sys_function_out,$s_format_field,"NO","V300",$ps_details_def,$ps_details_data,$ps_sessionno);
    }

    IF ( $s_value_set <> "N") // means the value has not been set
    {
        GOTO Z910_EXIT;
    }
    $sys_function_out = "Error_x3958^??".$ps_array_line_details;
    if ($s_space_on_error == "Y")
    {
       $sys_function_out = " ";
    }
    if ($s_space_on_error == "Z")
    {
       $sys_function_out = "0";
    }
    if ($s_space_on_error == "ALL")
    {
        $sys_function_out = "ALL";
    }

   goto Z910_EXIT;


echo "<br><br> clmain z900 line=".$ps_array_line_details;
echo "<br> clmain z900 sys_function_out=".$sys_function_out;
echo "<br> clmain z900 ps_details_def=".$ps_details_def;
echo "<br> clmain z900 ps_details_data=".$ps_details_data;

//die("gwdied clmain z900_exit in v300");


Z910_EXIT:
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_v400_set_system_var($ps_sysfield,$ps_debug,$ps_sessionno)
{
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_v400_set_system_var";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name." ps_sysfield = ".$ps_sysfield."  ps_debug = ".$ps_debug." ");};
    IF ($sys_debug == "YES"){echo $sys_function_name." -  for =".$ps_sysfield."  ps_debug=".$ps_debug."<br>";};


    $s_sysfield = str_replace("_","-",$ps_sysfield);
    SWITCH (strtoupper(trim($s_sysfield)))
    {
        case "NOWDMY":
             $sys_function_out = date("d/m/Y");
             break;
        case "HM":
             $sys_function_out = date("H:i");
             break;
        case "HMS":
             $sys_function_out = date("H:i:s");
             break;
        case "DOW":
             $sys_function_out = date("D");
             break;
        case "USERIP":
            if(isset($_SERVER["REMOTE_ADDR"])){
                $sys_function_out = $_SERVER["REMOTE_ADDR"];
            }else{
                $sys_function_out = "ref:clm400_err1_notavailable";
            }
             break;
        case "YMDHMS":
             $sys_function_out = date("YmdHis");
             break;
        case "YMDHMSU":
             $sys_function_out = date("YmdHisu");
             break;
        case "UTIME":
             $sys_function_out = date("U");
             break;
        case "MICROTIME":
             $sys_function_out = microtime(true);
             break;
        case "MICROTIMEWHOLE":
             $sys_function_out = round(microtime(true)*10000);
             break;
        case "NOW-YYYY":
             $sys_function_out = date("Y");
             break;
        case "NOW-YYYYABC":
             $sys_function_out = substr("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",2010 - date("Y"),1);
             break;
        case "NOW-YMABC":
             $sys_function_out = substr("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",2010 - date("Y"),1);
             $sys_function_out = $sys_function_out.substr("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",12 - date("m"),1);
             break;
        case "NOW-YMDABC":
             $sys_function_out = substr("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",2010 - date("Y"),1);
             $sys_function_out = $sys_function_out.substr("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",12 - date("m"),1);
             $sys_function_out = $sys_function_out.date("d");
             break;
        case "NOW-MM":
             $sys_function_out = date("m");
             break;
        case "NOW-MMM":
            $sys_function_out = date("M");
            break;
        case "NOW-MMABC":
             $sys_function_out = substr("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",12 - date("m"),1);
             break;
        case "NOW-DD":
             $sys_function_out = date("d");
             break;
        case "NOW-HH":
             $sys_function_out = date("H");
             break;
        case "NOW-MIN":
             $sys_function_out = date("i");
             break;
        case "NOW-SS":
             $sys_function_out = date("s");
             break;
        case "NOW-HH-MM":
             $sys_function_out = date("Hi");
             break;
        case "NOW-HH-MM-SS":
             $sys_function_out = date("His");
             break;
        case "TIMEZONE":
             $sys_function_out = date_default_timezone_get();
             break;
        case "UUID":
//gw20100918 - till I would out what uuid is /workds
            $sys_function_out = 'UUID()';
            // $sys_function_out = round(microtime(true)*10000);
             break;
    };


    IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ");};
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_v500_set_session_var($ps_sysfield,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_v500_set_session_var";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name." ps_sysfield = ".$ps_sysfield."  ps_debug = ".$ps_debug." ");};

   IF ($sys_debug == "YES"){$this->z901_dump(   $sys_function_name." test this field  = ".$ps_sysfield." go this value ".$sys_function_out);};

    $s_var_found = "";
    $s_session_var = "";

    $s_var_found = "NO";
    $s_session_var = $ps_sessionno.$ps_sysfield;

   if (SUBSTR(strtoupper($ps_sysfield),0,4) == "SYS_")// if the session variable is a sys_ one then no session prefix
    {
        $s_session_var = $ps_sysfield;
    }
    if (SUBSTR(strtoupper($ps_sysfield),0,6) == "SUPER_")// if the session variable is a sys_ one then no session prefix
    {
        $s_session_var = substr($ps_sysfield,6);
    }
   foreach($_SESSION as $key=>$value)
   {
//           echo $key." =".$value." <br>";
       if (strtoupper($s_session_var)==strtoupper($key))
       {
           $sys_function_out = $value;
           $s_var_found = "YES";
       }
       if (strtoupper($ps_sysfield)=="DUMPVARS")
       {
           echo " Session Var ".$key." = ".$value."<br>";
           $s_var_found = "YES";
       }
       if (strtoupper($ps_sysfield)=="DUMPVARS_OUT")
       {
           $sys_function_out = $sys_function_out." Session Var ".$key." = ".$value."<br>";
           $s_var_found = "YES";
       }
   }
   if (strtoupper($ps_sysfield)== "PARAM_SESSIONNO")
   {
       $sys_function_out = $ps_sessionno;
       $s_var_found = "YES";
   }
   if (strtoupper($ps_sysfield)== "ODD_OR_EVEN_LINE")
   {
       IF (!ISSET($_SESSION[$ps_sessionno.'ODD_OR_EVEN_LINE']))
       {
           $_SESSION[$ps_sessionno.'ODD_OR_EVEN_LINE'] = "even";
       }
       IF ($_SESSION[$ps_sessionno.'ODD_OR_EVEN_LINE'] == "even")
       {
            $_SESSION[$ps_sessionno.'ODD_OR_EVEN_LINE'] = "odd";
       }ELSE{
            $_SESSION[$ps_sessionno.'ODD_OR_EVEN_LINE'] = "even";
        }
   }
    if (strtoupper($ps_sysfield)== "UDP_FILTER_DATE")
    {
        IF(strtoupper($sys_function_out) =="DATERANGE")
        {
            $sys_function_out = "Date from-to";
            IF (ISSET($_SESSION[$ps_sessionno.'udp_filterdate_from']))
            {
                $sys_function_out =  $_SESSION[$ps_sessionno.'udp_filterdate_from'];
            }
            IF (ISSET($_SESSION[$ps_sessionno.'udp_filterdate_to']))
            {
                $sys_function_out = $sys_function_out." to ".$_SESSION[$ps_sessionno.'udp_filterdate_to'];
            }
        }
    }

   IF ($s_var_found <> "NO")
   {
       goto Z900_EXIT;
   }

// gw 20100407 if the value does not exist with the session param in front try without - allows defaults to be set in kickoff
   IF ($s_var_found == "NO")
   {
       $s_session_var = $ps_sysfield;
       foreach($_SESSION as $key=>$value)
       {
           if (strtoupper($s_session_var)==strtoupper($key))
           {
               $sys_function_out = $value;
               $s_var_found = "YES";
           }
       }
   }

   IF ($s_var_found == "NO")
   {
        $sys_function_out =  "ERROR^INSERTCODE_system_messages_SNIPPET_E2005";
        $sys_function_out .= "^|E2005SESSION_VAR";
        $sys_function_out .= "^|".$s_session_var."-session=".$ps_sessionno."-pfield:".$ps_sysfield;
//        $sys_function_out .= "^|".$s_session_var."-session=".$ps_sessionno."-pfield:".SUBSTR(strtoupper($ps_sysfield),0,4)$ps_sysfield;
//gw20100106 change to session_var        $sys_function_out .= "^|".$ps_sysfield;
   }

Z900_EXIT:
    IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ");};
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_v600_set_cookie_var($ps_sysfield,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_v600_set_cookie_var";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name." ps_sysfield = ".$ps_sysfield."  ps_debug = ".$ps_debug." ");};

   IF ($sys_debug == "YES"){$this->z901_dump(   $sys_function_name." test this field  = ".$ps_sysfield." go this value ".$sys_function_out);};

    $s_var_found = "";
    $s_session_var = "";

    $s_var_found = "NO";
// gw 20100407   removed the ud_  $s_session_var = $ps_sessionno."ud_".$ps_sysfield;
    $s_session_var = $ps_sessionno.$ps_sysfield;



//    ECHO "<BR> GW V600 FIELD = ".$ps_sysfield."<BR>";
   foreach($_COOKIE as $key=>$value)
   {
//           echo "GWDEBUG ".$key." =".$value." LOOKING FOR ".$s_session_var."<br>";
       if (strtoupper($s_session_var)==strtoupper($key))
       {
           $sys_function_out = $value;
           $s_var_found = "YES";
       }
       if (strtoupper($ps_sysfield)=="DUMPVARS")
       {
           echo "DUMPVARS Cookie Var ".$key." = ".$value."<br>";
           $s_var_found = "YES";
       }
   }
// gw 20100407 check the same cookie value without the session prefix
   IF ($s_var_found == "NO")
   {
       $s_session_var = $ps_sysfield;
       foreach($_COOKIE as $key=>$value)
       {
           if (strtoupper($s_session_var)==strtoupper($key))
           {
               $sys_function_out = $value;
               $s_var_found = "YES";
           }
       }
   }

   IF ($s_var_found == "NO")
   {
        $sys_function_out =  "ERROR^INSERTCODE_system_messages_snippet_E2007";
        $sys_function_out .= "^#E2007COOKIE_VAR";
        $sys_function_out .= "^#le".$ps_sysfield;
   }


    IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ");};
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_v650_set_SERVER_var($ps_sysfield,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_v650_set_SERVER_var";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name." ps_sysfield = ".$ps_sysfield."  ps_debug = ".$ps_debug." ");};

   IF ($sys_debug == "YES"){$this->z901_dump(   $sys_function_name." test this field  = ".$ps_sysfield." go this value ".$sys_function_out);};

    $s_var_found = "";
    $s_session_var = "";

    $s_var_found = "NO";
    $s_session_var = $ps_sysfield;
//    echo "650 val".$s_session_var;

    IF ($s_session_var == "CLIENTIPADDR")
    {
        $sys_function_out = $this->clmain_v655_set_RealIpAddr();
        GOTO Z900_EXIT;
    }

//    print_r($_SERVER);
    if(!isset($_SERVER))
    {
        GOTO Z900_EXIT;
    }
   foreach($_SERVER as $key=>$value)
   {
//           echo "GWDEBUG ".$key." =".$value." LOOKING FOR ".$s_session_var."<br>";
       if (strtoupper($s_session_var)==strtoupper($key))
       {
           $sys_function_out = $value;
           $s_var_found = "YES";
       }
       if (strtoupper($ps_sysfield)=="DUMPVARS")
       {
           echo "DUMPVARS Server Var ".$key." = ".$value."<br>";
           $s_var_found = "YES";
       }
   }

   IF ($s_var_found == "NO")
   {
        $sys_function_out =  "ERROR^INSERTCODE_system_messages_#E2010";
        $sys_function_out .= "^|E2010SERVER_VAR";
        $sys_function_out .= "^|".$ps_sysfield;
   }

Z900_EXIT:
//    echo "650 out".$sys_function_out;
    IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ");};
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_v655_set_RealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}
//##########################################################################################################################################################################
function clmain_v700_format_field($ps_field_value,$ps_format_field,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno)
{
//A100_TEMPLATE-INIT
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_v700_formatfield called from ".$ps_calledfrom;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name."  ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};
//A199_END_TEMPLATE_INIT:
    // define function specific variables and code
//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$array_line_details[$i2]." ";};

    $sys_function_out = $ps_field_value."&".$ps_format_field;
    IF ((SUBSTR($ps_format_field,0,1) == "9") or (SUBSTR($ps_format_field,0,1) == "Z") or (SUBSTR($ps_format_field,0,1) == "$"))
//    IF ((SUBSTR($ps_format_field,0,1) == "9") )
    {
        if (strpos($ps_format_field,".") === false) {
            $sys_function_out = $this->clmain_v710_format_numeric($ps_field_value,$ps_format_field,"NO","v700_num1");
        }else{
            //do decimal eg#99.99#  = 12.34
            $sys_function_out = $this->clmain_v710_format_numeric($ps_field_value,$ps_format_field,"NO","v700_num1a");
            $sys_function_out = $sys_function_out.".".$this->clmain_v720_format_decimal($ps_field_value,$ps_format_field,"NO","v700_dec1");
        }
    }
    IF (SUBSTR(strtoupper($ps_format_field),0,1) == "X")
    {
            //do alphaNumeric eg#X(10)=10char or #XXXXX = 5 char
            $sys_function_out = $ps_field_value."alpha";
            $sys_function_out = $this->clmain_v705_format_alpha($ps_field_value,$ps_format_field,"NO","v700_alpha1");
    }
    IF (SUBSTR(strtoupper($ps_format_field),0,2) == "D_")
    {
            //do date eg#DYYYY-MM-DD# or #DDD-MM-YYYY#
            $sys_function_out = $ps_field_value."Date";
            $sys_function_out = $this->clmain_v730_format_date($ps_field_value,$ps_format_field,"NO","v700_date1");
    }
    IF (SUBSTR(strtoupper($ps_format_field),0,2) == "T_")
    {
            $sys_function_out = $ps_field_value."Time";
            $sys_function_out = $this->clmain_v735_format_time($ps_field_value,$ps_format_field,"NO","v700_time1");
    }
    IF (SUBSTR(strtoupper($ps_format_field),0,6) == "APPCL_")
    {
            //translate the value using the system translate table
            $sys_function_out = $ps_field_value."Translate";
            $sys_function_out = $this->clmain_v750_application_class_value($ps_field_value,$ps_format_field,"NO","v700_trans1",$ps_details_def,$ps_details_data,$ps_sessionno);
    }
    /*    IF (SUBSTR(strtoupper($ps_format_field),0,8) == "APPCLV2_")
    {
        //translate the value using the system translate table
        $sys_function_out = $ps_field_value."Translate";
        $sys_function_out = $this->clmain_v755_application_class_valueV2($ps_field_value,$ps_format_field,"YES","v755",$ps_details_def,$ps_details_data,$ps_sessionno);
        DIE("CLMAIN APPCL2[]".$sys_function_out."[]");
    }
    */
    IF (SUBSTR(strtoupper($ps_format_field),0,9) == "DOCLMAIN_")
    {
        $sys_function_out = $ps_field_value."CLMAIN";
        $sys_function_out = $this->clmain_v840_clmain_function($ps_field_value,$ps_format_field,"NO","v700_trans1",$ps_details_def,$ps_details_data,$ps_sessionno);
    }
    IF (SUBSTR(strtoupper($ps_format_field),0,4) == "TRIM")
    {
            //translate the value using the system translate table
            $sys_function_out = TRIM($ps_field_value);
    }
    IF (strtoupper($ps_format_field) == "SMS_READY")
    {
            $sys_function_out = TRIM($ps_field_value);
            $sys_function_out = str_replace("\t", " ", $sys_function_out);
            $sys_function_out = str_replace("  ", " ", $sys_function_out);

//            die ("<br> doing the sms_read  sys_function_out =[]".$sys_function_out."[] ps_field_value=[]".$ps_field_value."[]");
    }
    IF (strtoupper($ps_format_field) == "SPACE_TO_UNDERSCORE")
    {
        $sys_function_out = TRIM($ps_field_value);
        $sys_function_out = str_replace("  ", " ", $sys_function_out);
        $sys_function_out = str_replace(" ", "_", $sys_function_out);

//            die ("<br> doing the sms_read  sys_function_out =[]".$sys_function_out."[] ps_field_value=[]".$ps_field_value."[]");
    }

/* use the jcl makexml to format the data_blob before sending here
    IF ($ps_format_field == "DATA_BLOB")
    {
// gw20110128 - if the field is xml leave - if not make xml
        if (strpos(strtoupper($ps_field_value),"XML") === FALSE)
        {}else
        {
            $sys_function_out = "";
            $sys_function_out = $sys_function_out.$this->clmain_u570_build_xml("UT_FILESTART","data_blob","NO","clmain_v700-1");
            $sys_function_out = $sys_function_out.$this->clmain_u570_build_xml("ut_start","data_blob","NO","clmain_v700-2");
            $sys_function_out = $sys_function_out.$this->clmain_u570_build_xml("DAILYEXPORT_HHMM",$ps_field_value,"NO","clmain_v700-3");
            $sys_function_out = $sys_function_out.$this->clmain_u570_build_xml("ut_end","data_blob","NO","clmain_v700-5");
       }
    }
*/

    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name."   ps_debug = ".$ps_debug." ");};


    IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ");};


    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ";};
    IF ($sys_debug == "YES"){$sys_function_out.=" <!--".$sys_debug_text."-->";};

//X900_EXIT:
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_v705_format_alpha($ps_field_value,$ps_format_field,$ps_debug,$ps_calledfrom)
{
//A100_TEMPLATE-INIT
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_v705_format_alpha  called from ".$ps_calledfrom;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name."  ps_debug = ".$ps_debug." ";};
//A199_END_TEMPLATE_INIT:
// define function specific variables and code

    $s_field_length = "";
    $s_field_value = "";
    $s_complete = "";

    $s_complete = "N";
    $s_field_length = $ps_format_field;

    $s_field_length = substr($s_field_length,strpos($ps_format_field,"(") +1);
    $s_field_length = str_replace(")","",$s_field_length);
    IF (!is_numeric($s_field_length))
    {
       $s_field_length = 0;
    }

    $s_field_value = $ps_field_value;

    $sys_function_out = $s_field_value."aplha705.".$s_field_length;

    if (strlen($s_field_value) >= $s_field_length)
    {
        $sys_function_out = $s_field_value;
        $s_complete = "Y";
    }

    IF ($s_complete = "N")
    {
        $i = 1;
        while (strlen($s_field_value) < $s_field_length)
        {
           $s_field_value = $s_field_value." ";
           $i = $i + 1;
           if ($i > 200){
               break;
           }
        }
        $sys_function_out = $s_field_value;
    }
//X900_EXIT:
    return $sys_function_out;
}
//##########################################################################################################################################################################
function clmain_v710_format_numeric($ps_field_value,$ps_format_field,$ps_debug,$ps_calledfrom)
{
//A100_TEMPLATE-INIT
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_v710_format_numeric  called from ".$ps_calledfrom;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name."  ps_debug = ".$ps_debug." ";};
//A199_END_TEMPLATE_INIT:
// define function specific variables and code

    $s_field_length = "";
    $s_field_value = "";
    $s_complete = "";

    $s_complete = "N";
// if there is a decimal in the format get the whole number fmat ie to left of .
    IF (strpos($s_field_value,".") === false )
    {
        $s_field_length = $ps_format_field;
    }else{
        $s_field_length = substr($ps_format_field,0,strpos($ps_format_field,"."));
    }

    $s_field_length = substr($s_field_length,strpos($ps_format_field,"(") +1);
    $s_field_length = str_replace(")","",$s_field_length);
    IF (!is_numeric($s_field_length))
    {
       $s_field_length = 0;
    }

    $s_field_value = $ps_field_value;
// gw20101022  - if passed a whole number need to put in the decimal
    if (strpos($s_field_value,".") === false)
    {
        $s_field_value= $s_field_value.".00";
    }
// numeric = no decimals - remove all to right of decimal place
    $s_field_value = substr($s_field_value,0,strpos($s_field_value,"."));

    $sys_function_out = $s_field_value."NUMv710.".$s_field_length;

    if (strlen($s_field_value) >= $s_field_length)
    {
        $sys_function_out = $s_field_value;
        $s_complete = "Y";
    }

    IF ($s_complete = "N")
    {
        $i = 1;
        while (strlen($s_field_value) < $s_field_length)
        {
            IF (SUBSTR($ps_format_field,0,1) == "Z")
            {
                $s_field_value = " ".$s_field_value;
            }
            IF (SUBSTR($ps_format_field,0,1) == "9")
            {
                $s_field_value = "0".$s_field_value;
            }
            IF (SUBSTR($ps_format_field,0,1) == "$")
            {
                IF (strpos($s_field_value,"$") === false )
                {
                    $s_field_value = "$".$s_field_value;
                }else{
                    $s_field_value = " ".$s_field_value;
                }
            }
           $i = $i + 1;
           if ($i > 100){
               break;
           }
        }
        $sys_function_out = $s_field_value;
    }
    // a dollar value should have the $ in front no matter what size
    IF (SUBSTR($ps_format_field,0,1) == "$")
    {
        IF (strpos($sys_function_out,"$") === false )
        {
            $sys_function_out = "$".$sys_function_out;
        }
    }
//X900_EXIT:
    return $sys_function_out;
}
//##########################################################################################################################################################################
function clmain_v720_format_decimal($ps_field_value,$ps_format_field,$ps_debug,$ps_calledfrom)
{
//A100_TEMPLATE-INIT
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_v720_format_decimal  called from ".$ps_calledfrom;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name."  ps_debug = ".$ps_debug." ";};
//A199_END_TEMPLATE_INIT:

    $s_field_length = "";
    $s_field_value = "";
    $s_complete = "";

    $s_complete = "N";

// field_format 9(6).9(2) - get rid of everything to left of .
    $s_field_length = substr($ps_format_field,strpos($ps_format_field,".") +1);
    $s_field_length = substr($s_field_length,strpos($ps_format_field,"(") +1);
    $s_field_length = str_replace(")","",$s_field_length);
    IF (!is_numeric($s_field_length))
    {
       $s_field_length = 0;
    }

    $s_field_value = $ps_field_value;
// gw20101022  - if passed a whole number need to put in the decimal
    if (strpos($s_field_value,".") === false)
    {
        $s_field_value= $s_field_value.".00";
    }
// numeric = no decimals - remove all to left of decimal place
    $s_field_value = substr($s_field_value,strpos($s_field_value,".")+1);

    $sys_function_out = $s_field_value."dec720.".$s_field_length;
    $sys_function_out = substr($s_field_value,0,$s_field_length);
    while (strlen($sys_function_out) < $s_field_length)
    {
        $sys_function_out = $sys_function_out."0";
    }

//$sys_function_out = $s_field_length;

    // define function specific variables and code
//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$array_line_details[$i2]." ";};

    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name."  ps_debug = ".$ps_debug." ");};

    IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ");};

    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ";};
    IF ($sys_debug == "YES"){$sys_function_out.=" <!--".$sys_debug_text."-->";};

//X900_EXIT:
    return $sys_function_out;
}
//##########################################################################################################################################################################
function clmain_v730_format_date($ps_field_value,$ps_format_field,$ps_debug,$ps_calledfrom)
{
//A100_TEMPLATE-INIT
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_v730_format_date  called from ".$ps_calledfrom;
    $sys_function_out = "";

    if (strpos(strtoupper($ps_format_field),"DODEBUG") !== false )
    {
        $sys_debug == "YES";
        $ps_format_field = str_replace("DODEBUG","",strtoupper($ps_format_field));
    }



    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_field_value = ".$ps_field_value." ps_format_field = ".$ps_format_field." ps_debug = ".$ps_debug." ";};
//A199_END_TEMPLATE_INIT:
//B100_ define function specific variables and codE
    $s_date_fmat = "";
    $s_date_yyyy = "";
    $s_date_mm = "";
    $s_date_dd = "";

    $s_field_value = "";
    $s_format_field = "";

    $s_field_value = $ps_field_value;
    $s_field_value = trim(str_replace("00:00:00","",$s_field_value));
    $s_field_value = trim(str_replace("/","-",$s_field_value));


    $s_format_field = $ps_format_field;
    $s_format_field =  trim(str_replace("D_","",$s_format_field));


    $s_date_fmat = "?";
    $s_date_yyyy = "";
    $s_date_mm = "";
    $s_date_dd = "";

A100_FMAT1:
// gw 20100424 added replact / to - and the check of - so I could process the ut date
    IF (strpos(strtoupper($s_field_value),"-") === false )
    {
        goto A200_UT_DATE;
    }
    IF (!strpos(strtoupper($s_field_value),"T") === false )
    {
        goto A110_udtime_to_ut;
    }
    // len=10, ccyy-mm-dd, dd/mm/ccyy
    if(strlen($s_field_value)==10)
        {
            $s_field_value = trim(str_replace("-","/",$s_field_value));
            $sys_function_out = (strpos($s_field_value,"/"));

            if (strpos($s_field_value,"/") == 4)
            {
                $s_date_fmat = "CCYY-MM-DD";
                $s_date_yyyy = substr($s_field_value,0,4);
                $s_date_mm = substr($s_field_value,5,2);
                $s_date_dd = substr($s_field_value,8,2);
            }
            if (strpos($s_field_value,"/") == 2)
            {
                $s_date_fmat = "DD-MM-CCYY";
                $s_date_yyyy = substr($s_field_value,6,4);
                $s_date_mm = substr($s_field_value,3,2);
                $s_date_dd = substr($s_field_value,0,2);
            }
        }
//gw20150518 - added
        if(strlen($s_field_value)==11)
        {
//            $s_field_value = trim(str_replace("-","/",$s_field_value));
            $sys_function_out = (strpos($s_field_value,"-"));

            if (strpos($s_field_value,"-") == 4)
            {
                $s_date_fmat = "CCYY-MMM-DD";
                $s_date_yyyy = substr($s_field_value,0,4);
                $s_date_mm = substr($s_field_value,5,3);
                $s_date_mm = date("m",STRtotime($s_field_value));
                $s_date_dd = substr($s_field_value,9,2);
            }
            if (strpos($s_field_value,"-") == 2)
            {
                $s_date_fmat = "DD-MMM-CCYY";
                $s_date_yyyy = substr($s_field_value,7,4);
                $s_date_mm = substr($s_field_value,3,3);
                $s_date_mm = date("m",STRtotime($s_field_value));
                $s_date_dd = substr($s_field_value,0,2);
            }
        }


    GOTO A900_EXIT;
//                               0yyy-5
// gw 20111109 conver the udtime yyyy-mm-ddThh:mm.ss to uttime then do ut_date process
A110_udtime_to_ut:
// gw 20111201
/*
    $d_hr = substr($s_field_value,8,2);
    $s_min = substr($s_field_value,10,2);
    $s_sec = substr($s_field_value,12,2);
    $s_month = substr($s_field_value,4,2);
    $s_day = substr($s_field_value,6,2);
    $s_year = substr($s_field_value,0,4);
    $s_daylight_saving= "0";
*/
    $d_hr = substr($s_field_value,11,2);
    $s_min = substr($s_field_value,14,2);
    $s_sec = substr($s_field_value,17,2);
    $s_month = substr($s_field_value,5,2);
    $s_day = substr($s_field_value,8,2);
    $s_year = substr($s_field_value,0,4);
    $s_daylight_saving= "0";
//    echo "<br>s_field_value =".$s_field_value." mktime(".$d_hr."--".$s_min."--".$s_sec."--".$s_month."--".$s_day."--".$s_year;
    $s_field_value = mktime($d_hr,$s_min,$s_sec,$s_month,$s_day,$s_year);
    goto A290_DO_UT_TIME;
// gw 20100424 - added the process for the ??ut?? date = 10 digit number of secs since something
A200_UT_DATE:

A201_CYMDHMS_TO_UT:
// gw 20111121 - if field = 14 chr long then too big for udtime yyyymmddhhmmss
    if (strlen($s_field_value) <> 14)
    {
        goto A201_exit;
    }
    $d_hr = substr($s_field_value,8,2);
    $s_min = substr($s_field_value,10,2);
    $s_sec = substr($s_field_value,12,2);
    $s_month = substr($s_field_value,4,2);
    $s_day = substr($s_field_value,6,2);
    $s_year = substr($s_field_value,0,4);
    $s_daylight_saving= "0";
    $s_field_value = mktime($d_hr,$s_min,$s_sec,$s_month,$s_day,$s_year);
    goto A290_DO_UT_TIME;
A201_exit:

A202_CYMD_TO_UT:
// gw 20150121 if 8 char long and starts 20 then is cymd
    if (strlen($s_field_value) <> 8)
    {
        goto A202_exit;
    }
    if (substr($s_field_value,0,2) <> "20")
    {
        goto A202_exit;
    }
    $d_hr = 0;
    $s_min = 0;
    $s_sec = 0;
    $s_month = substr($s_field_value,4,2);
    $s_day = substr($s_field_value,6,2);
    $s_year = substr($s_field_value,0,4);
    $s_daylight_saving= "0";
    $s_field_value = mktime($d_hr,$s_min,$s_sec,$s_month,$s_day,$s_year);
    goto A290_DO_UT_TIME;
 A202_exit:

A290_DO_UT_TIME:
// these rely on $s_field_value being a utime
//gw20110116 - added check of field to make sure valid time
    if (is_numeric(trim($s_field_value)) === false)
    {
        goto z900_exit;
    }
    if (trim($s_field_value) < 10)
    {
        goto z900_exit;
    }


    $s_date_yy = date("y",$s_field_value);
    $s_date_yyyy = date("Y",$s_field_value);

    $s_date_m = date("n",$s_field_value);
    $s_date_mm = date("m",$s_field_value);
    $s_date_mmm = date("M",$s_field_value);
    $s_date_mname = date("F",$s_field_value);

    $s_date_d = date("j",$s_field_value);
    $s_date_dd = date("d",$s_field_value);
    $s_date_ddd = date("D",$s_field_value);
    $s_date_dname = date("l",$s_field_value);
    $s_date_doy = date("z",$s_field_value);
    $s_date_dow = date("w",$s_field_value);

    $s_time_hh24 = date("H",$s_field_value);
    $s_time_mm = date("i",$s_field_value);
    $s_time_ss = date("s",$s_field_value);


    GOTO A900_EXIT;

A900_EXIT:
    if (!$s_date_fmat == "?")
    {
       // next date type
    }
// NO IMPORT DATE FORMAT calculate - out = in
    if (!$s_date_fmat == "?")
    {
       // next date type
       $sys_function_out = $s_field_value;
       $s_format_field ="unknownInbound";
    }

    if ($s_format_field == "D/M/CY")
    {
       $sys_function_out = $s_date_dd."/".$s_date_mm."/".$s_date_yyyy;
       goto z900_exit;
       }
    if ($s_format_field == "DDD/M/CY")
    {
       $sys_function_out = $s_date_ddd." ".$s_date_dd."/".$s_date_mm."/".$s_date_yyyy;
       goto z900_exit;
    }
    if ($s_format_field == "CYMMDD")
    {
        $sys_function_out = $s_date_yyyy.$s_date_mm.$s_date_dd;
        goto z900_exit;
    }
    if ($s_format_field == "CY")
    {
       $sys_function_out = $s_date_yyyy;
       goto z900_exit;
    }
    if ($s_format_field == "Y")
    {
       $sys_function_out = $s_date_yy;
       goto z900_exit;
    }
    if ($s_format_field == "M")
    {
       $sys_function_out = $s_date_m;
       goto z900_exit;
    }
    if ($s_format_field == "MM")
    {
       $sys_function_out = $s_date_mm;
       goto z900_exit;
    }
    if ($s_format_field == "MMM")
    {
       $sys_function_out = $s_date_mmm;
       goto z900_exit;
    }
    if ($s_format_field == "MNAME")
    {
       $sys_function_out = $s_date_mname;
       goto z900_exit;
    }

    if ($s_format_field == "D")
    {
       $sys_function_out = $s_date_d;
       goto z900_exit;
    }
    if ($s_format_field == "DD")
    {
       $sys_function_out = $s_date_dd;
       goto z900_exit;
    }
    if ($s_format_field == "DDD")
    {
       $sys_function_out = $s_date_ddd;
       goto z900_exit;
    }
    if ($s_format_field == "DNAME")
    {
       $sys_function_out = $s_date_dname;
       goto z900_exit;
    }
    if ($s_format_field == "DOY")
    {
       $sys_function_out = $s_date_doy;
       goto z900_exit;
    }
    if ($s_format_field == "DOW")
    {
       $sys_function_out = $s_date_dow;
       goto z900_exit;
    }
    if ($s_format_field == "UDTIME")
    {
       $sys_function_out = $s_date_yyyy."-".$s_date_mm."-".$s_date_dd."T".$s_time_hh24.":".$s_time_mm.":".$s_time_ss;
       goto z900_exit;
    }
    if ($s_format_field == "UDTIMEZ")
    {
       $sys_function_out = $s_date_yyyy."-".$s_date_mm."-".$s_date_dd."T".$s_time_hh24.":".$s_time_mm.":".$s_time_ss."Z".date("T",$s_field_value);
       goto z900_exit;
    }
    if ($s_format_field == "UDTIMEG")
    {
       $sys_function_out = $s_date_yyyy."-".$s_date_mm."-".$s_date_dd."T".$s_time_hh24.":".$s_time_mm.":".$s_time_ss."G".date("O",$s_field_value);
       goto z900_exit;
    }
    if ($s_format_field == "UDTIMEZG")
    {
       $sys_function_out = $s_date_yyyy."-".$s_date_mm."-".$s_date_dd."T".$s_time_hh24.":".$s_time_mm.":".$s_time_ss."Z".date("T",$s_field_value)."G".date("O",$s_field_value);
       goto z900_exit;
    }
    if ($s_format_field == "UTIME")
    {
        $sys_function_out = strtotime($s_date_yyyy."-".$s_date_mm."-".$s_date_dd);
        goto z900_exit;
    }

    die("DIE - unknown date format clmain 730 s_format_field=[]".$s_format_field."[]");
    // check day and month - if month is > 12 then swap them

//    $sys_function_out = $sys_function_out."#INVALUE=".$s_field_value."#INFMAT=".$ps_format_field."#DFMAT=".$s_date_fmat."#outfmat=".$s_format_field;
z900_exit:
//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$array_line_details[$i2]." ";};
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name."   ps_debug = ".$ps_debug." ");};

    IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ");};

    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ";};
    IF ($sys_debug == "YES"){$sys_function_out.=" <!--".$sys_debug_text."-->";};

//X900_EXIT:
    return $sys_function_out;
}
//##########################################################################################################################################################################
function clmain_v735_format_time($ps_field_value,$ps_format_field,$ps_debug,$ps_calledfrom)
{
//A100_TEMPLATE-INIT
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_v730_format_date  called from ".$ps_calledfrom;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_format_field = ".$ps_format_field." ps_format_field = ".$ps_format_field."  ps_debug = ".$ps_debug." ";};
//A199_END_TEMPLATE_INIT:
//B100_ define function specific variables and codE
    $s_time_fmat = "";
    $s_time_H12  = "";
    $s_time_h24  = "";

    $s_time_hh12  = "";
    $s_time_hh24  = "";

    $s_time_mm  = "";
    $s_time_ss  = "";

    $s_time_u  = "";
    $s_time_zone  = "";
    $s_time_gmt = "";

    $s_field_value = "";
    $s_format_field = "";

    $s_field_value = $ps_field_value;

    $s_format_field = $ps_format_field;
    $s_format_field =  trim(str_replace("T_","",$s_format_field));


    $s_time_fmat = "?";

A100_FMAT1:
// gw 20100424 added replact / to - and the check of - so I could process the ut date
    IF (strpos(strtoupper($s_field_value),":") === false )
    {
        goto A200_UT_DATE;
    }
    $s_field_value =  str_replace("1899-12-30","",$s_field_value);  // to keep pronto happy
    $s_field_value =  trim($s_field_value);
    // hh:mm:ss
    if(strlen(trim($s_field_value))==8)
        {
            $s_time_H12 = substr($s_field_value,0,2);

            $s_time_mm =  substr($s_field_value,3,2);
            $s_time_ss =  substr($s_field_value,6,2);

            $s_time_hh12 = $s_time_H12;
            $s_time_h24 = $s_time_H12;
            $s_time_hh24 = $s_time_H12;
        }

    GOTO A900_EXIT;
// gw 20100424 - added the process for the ??ut?? date = 10 digit number of secs since something
A200_UT_DATE:
// gw 20111121 - if field = 14 chr long then too big for udtime yyyymmddhhmmss
    if (strlen($s_field_value) <> 14)
    {
        goto A290_DO_UT_TIME;
    }
    $d_hr = substr($s_field_value,8,2);
    $s_min = substr($s_field_value,10,2);
    $s_sec = substr($s_field_value,12,2);
    $s_month = substr($s_field_value,4,2);
    $s_day = substr($s_field_value,6,2);
    $s_year = substr($s_field_value,0,4);
    $s_daylight_saving= "0";
    $s_field_value = mktime($d_hr,$s_min,$s_sec,$s_month,$s_day,$s_year);
    goto A290_DO_UT_TIME;

    A290_DO_UT_TIME:

    if(strlen($s_field_value) == 6 )
    {
        GOTO A290_DO_HHMMSS;
    }

// these rely on $s_field_value being a utime
//gw20110116 - added check of field to make sure valid time
    if (is_numeric(trim($s_field_value)) === false)
    {
        goto z900_exit;
    }
    if (strlen(trim($s_field_value)) < 10)
    {
        goto z900_exit;
    }


    $s_time_H12 = date("g",$s_field_value);
    $s_time_h24 = date("G",$s_field_value);

    $s_time_hh12 = date("h",$s_field_value);
    $s_time_hh24 = date("H",$s_field_value);

    $s_time_mm = date("i",$s_field_value);
    $s_time_ss = date("s",$s_field_value);

    $s_time_u = date("u",$s_field_value);
    $s_time_zone = date("T",$s_field_value);
    $s_time_gmt = date("O",$s_field_value);

    $s_time_ap = date("A",$s_field_value);
    $s_time_apl = date("a",$s_field_value);

    GOTO A900_EXIT;

A290_DO_HHMMSS:
    $s_time_hh24 = substr($s_field_value,0,2);
    $s_time_mm = substr($s_field_value,2,2);
    $s_time_ss = substr($s_field_value,4,2);
    GOTO A900_EXIT;

    A900_EXIT:

    if ($s_format_field == "HH24:MM:SS.Z")
    {
       $sys_function_out = $s_time_hh24.":".$s_time_mm.":".$s_time_ss." ".$s_time_zone;
    }
    if ($s_format_field == "HH24:MM:SS")
    {
       $sys_function_out = $s_time_hh24.":".$s_time_mm.":".$s_time_ss;
    }
    if ($s_format_field == "H12:MM:SS.APL")
    {
       $sys_function_out = $s_time_H12.":".$s_time_mm.":".$s_time_ss." ".$s_time_apl;
    }
    if ($s_format_field == "H12:MM:SS.AP")
    {
       $sys_function_out = $s_time_H12.":".$s_time_mm.":".$s_time_ss." ".$s_time_ap;
    }
    if ($s_format_field == "HH24:MM:SS.GMT")
    {
       $sys_function_out = $s_time_hh24.":".$s_time_mm.":".$s_time_ss." ".$s_time_gmt;
    }
    if ($s_format_field == "HH24")
    {
       $sys_function_out = $s_time_hh24;
    }
    if ($s_format_field == "HH12")
    {
       $sys_function_out = $s_time_hh12;
    }
    if ($s_format_field == "MM")
    {
       $sys_function_out = $s_time_mm;
    }
    if ($s_format_field == "SS")
    {
       $sys_function_out = $s_time_ss;
    }
    if ($s_format_field == "Z")
    {
       $sys_function_out = $s_time_zone;
    }
    if ($s_format_field == "AMPM")
    {
       $sys_function_out = $s_time_ap;
    }
    if ($s_format_field == "GMT")
    {
       $sys_function_out = $s_time_gmt;
    }
    // check day and month - if month is > 12 then swap them

//    $sys_function_out = $sys_function_out."#INVALUE=".$s_field_value."#INFMAT=".$ps_format_field."#DFMAT=".$s_date_fmat."#outfmat=".$s_format_field;
z900_exit:
//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$array_line_details[$i2]." ";};
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name."   ps_debug = ".$ps_debug." ");};

    IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ");};

    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ";};
    IF ($sys_debug == "YES"){$sys_function_out.=" <!--".$sys_debug_text."-->";};

//X900_EXIT:
    return $sys_function_out;
}
//##########################################################################################################################################################################
function clmain_v740_compare_values($ps_field_value,$ps_operator,$ps_compare_value,$ps_debug,$ps_calledfrom)
{

    global $class_sql;


A100_TEMPLATE_INIT:
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }

    if (strpos(strtoupper($ps_operator),"DODEBUG") === FALSE)
    {}else{
        $sys_debug  = "YES";
        $sys_debug_text = "via operator  value of ".$ps_operator;
    }

    $sys_function_name = "";
    $sys_function_name = "debug - clmain_v740_compare_values  called from ".$ps_calledfrom;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." ps_field_value=".$ps_field_value."<BR> ps_operator=".$ps_operator."<BR> s_compare_value=".$ps_compare_value."<BR> ps_debug=".$ps_debug."<BR> ps_calledfrom=".$ps_calledfrom."<br> ";};
A199_END_TEMPLATE_INIT:

    $s_field_value = $ps_field_value;
    $s_operator = strtoupper(trim($ps_operator," "));
    $s_operator = TRIM(str_replace("DODEBUG","",$s_operator));

    $s_compare_value = $ps_compare_value;

    IF ($s_operator == "SQL")
    {
        GOTO B200_DO_SQL;
    }
    IF ($s_operator == "FILE_EXISTS")
    {
        GOTO B300_DO_FILEEXISTS;
    }

    if (strtoupper(trim($s_compare_value)) == "SPACES")
    {
        $s_compare_value = "";
    }
    if (strtoupper(trim($s_compare_value)) == "ZEROES")
    {
        $s_compare_value = "0";
    }
    if (strtoupper(trim($s_compare_value)) == "ZERO")
    {
        $s_compare_value = "0";
    }
    $sys_function_out = "FALSE";
    $s_field_value =  strtoupper(trim($s_field_value," "));
    $s_compare_value = strtoupper(trim($s_compare_value," "));

    IF ($sys_debug == "YES"){echo "test s_operator =".$s_operator." s_field_value=".$s_field_value." s_compare_value=".$s_compare_value."<br> ";};

B100_EQUALS:
    IF (trim($s_operator) == "=")
    {
        GOTO B105_DO_EQUALS;
    }
    IF (trim($s_operator) == "EQUALS")
    {
        GOTO B105_DO_EQUALS;
    }
    IF (trim($s_operator) == "EQUAL")
    {
        GOTO B105_DO_EQUALS;
    }
    IF (trim($s_operator) == "LIKE")
    {
        GOTO B106_DO_LIKE;
    }
    IF (trim($s_operator) == "NOT_EQUAL")
    {
        GOTO B109_NOT_THE_EQUAL;
    }
    IF (trim($s_operator) == "NOT_EQUALS")
    {
        GOTO B109_NOT_THE_EQUAL;
    }
    IF (trim($s_operator) == ">")
    {
        GOTO B120_GREATER_THAN;
    }
    IF (trim($s_operator) == "GREATERTHAN")
    {
        GOTO B120_GREATER_THAN;
    }
    IF (trim($s_operator) == "<")
    {
        GOTO B130_LESS_THAN;
    }
    IF (trim($s_operator) == "LESSTHAN")
    {
        GOTO B130_LESS_THAN;
    }
    IF (trim($s_operator) == "CONTAINS")
    {
        GOTO B140_CONTAINS;
    }
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." s_operator=".$s_operator." which is an Unknown operator<BR>";};
//20110130 - UNKNOWN OPERATOR exit and stay at FALSE
//    goto B109_NOT_THE_EQUAL;
    GOTO Z900_EXIT;

B105_DO_EQUALS:
    IF ($sys_debug == "YES"){echo "doing b105_do_equals based on s_operator =".$s_operator."<br> ";};
    IF (trim($s_field_value) == trim($s_compare_value))
    {
        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name."VALUE ARE SAME -".trim($s_field_value)."- Compare -".trim($s_compare_value)."-<BR>";};
        $sys_function_out = "TRUE";
    }
    GOTO Z900_EXIT;
B106_DO_LIKE:
    IF ($sys_debug == "YES"){echo "doing b106_do_like based on s_operator =".$s_operator."<br> ";};

    if (strpos(strtoupper(trim($s_field_value)),trim($s_compare_value)) === FALSE)
    {

    }
    else
    {
        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name."VALUE ARE LIKE -".trim($s_field_value)."- Compare -".trim($s_compare_value)."-<BR>";};
        $sys_function_out = "TRUE";
    }

    GOTO Z900_EXIT;

B109_NOT_THE_EQUAL:
    IF ($sys_debug == "YES"){echo "doing B109_NOT_THE_EQUAL based on s_operator =".$s_operator."<br> ";};
    IF (trim($s_field_value) <> trim($s_compare_value))
    {
        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name."VALUE ARE SAME -".trim($s_field_value)."- Compare -".trim($s_compare_value)."-<BR>";};
        $sys_function_out = "TRUE";
    }
    GOTO Z900_EXIT;

B120_GREATER_THAN:
    IF ($sys_debug == "YES"){echo "doing B120_GREATER_THAN based on s_operator =".$s_operator."<br> ";};
    IF (trim($s_field_value) > trim($s_compare_value))
    {
        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name."VALUE ARE SAME -".trim($s_field_value)."- Compare -".trim($s_compare_value)."-<BR>";};
        $sys_function_out = "TRUE";
    }
    GOTO Z900_EXIT;

B130_LESS_THAN:
    IF ($sys_debug == "YES"){echo "doing B130_LESS_THAN based on s_operator =".$s_operator."<br> ";};
    IF (trim($s_field_value) < trim($s_compare_value))
    {
        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name."VALUE ARE SAME -".trim($s_field_value)."- Compare -".trim($s_compare_value)."-<BR>";};
        $sys_function_out = "TRUE";
    }
    GOTO Z900_EXIT;
B140_CONTAINS:
    IF ($sys_debug == "YES"){echo "doing B140_CONTAINS based on s_operator =".$s_operator."<br> ";};
    IF (strpos(trim(strtoupper($s_field_value)),trim(strtoupper($s_compare_value ))) !== false)
    {
        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name."VALUE ARE SAME -".trim($s_field_value)."- Compare -".trim($s_compare_value)."-<BR>";};
        $sys_function_out = "TRUE";
    }
    GOTO Z900_EXIT;

B200_DO_SQL:
// if|v1|sql statement|SQL|Fail or OK
//    $s_field_value = $ps_field_value;
//    $s_operator = strtoupper(trim($ps_operator," "));
//    $s_compare_value = $ps_compare_value;


// gw 20140103 set data base to last open = current db - it was too hard to have it as a paramater
    IF ($sys_debug == "YES"){echo $sys_function_name." DOING B200_DO_SQL<br>";};

    $dbcnx = $class_sql->c_sqlclient_connect();

    $rs_temp = mysqli_query($dbcnx,$s_field_value);
    if ($s_compare_value == "OK")
    {
        IF ($sys_debug == "YES"){echo $sys_function_name." DOING COMPARE = OK <br>";};
        GOTO B220_OK;
    }
    if ($s_compare_value == "FAIL")
    {
        IF ($sys_debug == "YES"){echo $sys_function_name." DOING COMPARE = FAIL<br>";};
        GOTO B230_FAIL;
    }
B220_OK:
// the sql must have been completed sucessfully
    $sys_function_out = "TRUE";
    if (!$rs_temp)
    {
        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." the sql statement failed sql=[]".trim($s_field_value)."[]   query error =[]".mysqli_error()."[]<BR>";};
        $sys_function_out = "FALSE";
        GOTO B229_EXIT;
    }
    if (mysqli_num_rows($rs_temp) === false )
    {
        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." there are no records (false) found  sql=[]".trim($s_field_value)."[]<BR>";};
        $sys_function_out = "FALSE";
        GOTO B229_EXIT;
    }
    if (mysqli_num_rows($rs_temp) == 0 )
    {
        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." there are zero records found  sql=[]".trim($s_field_value)."[]<BR>";};
        $sys_function_out = "FALSE";
        GOTO B229_EXIT;
    }
B229_EXIT:
    GOTO B290_EXIT;
B230_FAIL:
// the sql must fail
    $sys_function_out = "FALSE";
    if (!$rs_temp)
    {
        $this->z901_dump("Invalid query error = ".mysqli_error()." sql=".$s_field_value." ","sql");
        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." the sql statement failed sql=[]".trim($s_field_value)."[]   query error =[]".mysqli_error()."[]<BR>";};
        $sys_function_out = "TRUE";
        GOTO B239_EXIT;
    }
    if (mysqli_num_rows($rs_temp) === false )
    {
        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." there are no records (false) found  sql=[]".trim($s_field_value)."[]<BR>";};
        $sys_function_out = "TRUE";
        GOTO B239_EXIT;
    }
    if (mysqli_num_rows($rs_temp) == 0 )
    {
        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." there are zero records found  sql=[]".trim($s_field_value)."[]<BR>";};
        $sys_function_out = "TRUE";
        GOTO B239_EXIT;
    }
B239_EXIT:
    IF ($sys_debug == "YES"){echo $sys_function_name." FAIL PROCESS sys_debug_text =[]".$sys_debug_text."[]";};

    GOTO B290_EXIT;

B290_EXIT:
    GOTO Z900_EXIT;

B300_DO_FILEEXISTS:
// if|v2+option|filename|FILE_EXISTS|True/false
//    $ps_field_value = file name
//// $ps_operator = FILE_EXISTS
// TRUE OR FALSE
    $s_filename = $this->clmain_set_map_path_n_name("",$ps_field_value);
    $s_filename_exists = "FALSE";
    $sys_function_out = "FALSE";

    if(file_exists($s_filename))
    {
        $s_filename_exists = "TRUE";
    }
    IF (trim($s_filename_exists) == trim($s_compare_value))
    {
        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name."VALUE ARE SAME -".trim($s_field_value)."- Compare -".trim($s_compare_value)."-<BR>";};
        $sys_function_out = "TRUE";
    }
B390_EXIT:
    GOTO Z900_EXIT;

Z900_EXIT:
    IF ($sys_debug == "YES"){echo "Value returned is ".$sys_function_out."<br> ";};
    return $sys_function_out;
}
//##########################################################################################################################################################################
function clmain_v750_application_class_value($ps_field_value,$ps_format_field,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno)
{
//A100_TEMPLATE-INIT
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_v750_application_class_value  called from ".$ps_calledfrom;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_field_value = ".$ps_field_value." ps_format_field = ".$ps_format_field."  ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};
//A199_END_TEMPLATE_INIT:
//B100_ define function specific variables and codE

     require_once($_SESSION['ko_map_path'].'lib/class_app.php');
     $class_apps = new AppClass();

    global $dbcnx;
// _#tr_messagestatus_
// go to the utl_translate table
// at = after the first _

    $s_field_value = "";
    $s_format_field = "";

    $s_field_value = $ps_field_value;

    $s_format_field = $ps_format_field;
    $s_format_field =  trim(str_replace("APPCL_","",$s_format_field));

//    IF (strpos($s_format_field,"DOAPPCLFUN_")!==false)
//    {
//        die ("gwdie  v750 s_format_field=[]".$s_format_field."[] $s_field_value=[]".$s_field_value."[]");
//    }

    $s_format_field =  trim(str_replace("DOAPPCLFUN_","",$s_format_field));

    $sys_function_out = "V750??".$ps_format_field;
A100_GET_REC:
    $sys_function_out = $class_apps->clapp_a100_do_app_class($s_format_field,$s_field_value,"RUN_FUNCTION",$dbcnx,"NO","clmain_v750_application_class_value",$ps_details_def,$ps_details_data,$ps_sessionno);
//    $sys_function_out = $this->clmain_v200_load_line($sys_function_out,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"clmain_v750_application_class_value");

    //    $sys_function_out = $sys_function_out."#INVALUE=".$s_field_value."#INFMAT=".$ps_format_field."#DFMAT=".$s_date_fmat."#outfmat=".$s_format_field;

//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$array_line_details[$i2]." ";};
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name."   ps_debug = ".$ps_debug." ");};

    IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ");};

    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ";};
    IF ($sys_debug == "YES"){$sys_function_out.=" <!--".$sys_debug_text."-->";};

//X900_EXIT:
    return $sys_function_out;
}
function clmain_v755_application_class_valueV2($ps_field_value,$ps_format_field,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno)
    {
//A100_TEMPLATE-INIT
        $sys_debug_text = "";
        $sys_debug = "";
        $sys_debug = strtoupper($ps_debug);
        IF ($sys_debug !="NO") {
            $sys_debug  = "YES";
            $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
        }
        $sys_function_name = "";
        $sys_function_name = "debug - clmain_v755_application_class_valuev2  called from ".$ps_calledfrom;
        $sys_function_out = "";
        IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_field_value = ".$ps_field_value." ps_format_field = ".$ps_format_field."  ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};


        $s_field_value = $ps_field_value;
        $s_format_field = $ps_format_field;
        $s_format_field =  trim(str_replace("APPCLV2_","",$s_format_field));

        $s_format_field = str_replace("_","/",$s_format_field);

        $s_app_group = "_".substr($s_format_field,0,strpos($s_format_field,"/"));
        $s_app_group_path = substr($s_format_field,0,strpos($s_format_field,"/"));
        IF(strtoupper(trim($s_app_group)) == "_DMAV2")
        {
            $s_app_group_path = "";
            $s_app_group="";
        }
        $s_app_class = "AppClass".$s_app_group;
        $s_function =  substr($s_format_field,strpos($s_format_field,"/")+1);
        $s_function = str_replace("/","_",$s_function);

        require_once($_SESSION['ko_map_path'].$s_app_group_path.'/lib/class_app'.$s_app_group.'.php');
        $class_appsv2 = new $s_app_class;

        global $dbcnx;
// _#tr_messagestatus_
// go to the utl_translate table
// at = after the first _

        $s_function_name = $ps_format_field;
        $s_function_name =  trim(str_replace("APPCLV2_","clapp_",$s_function_name));

        $s_function_param = "";
        $sys_function_out = "V755??".$ps_format_field;

        A100_GET_REC:
        $sys_function_out = $class_appsv2->clapp_a100_do_app_class($s_function_name,$s_function_param,"RUN_FUNCTION",$dbcnx,"NO","clmain_v750_application_class_value",$ps_details_def,$ps_details_data,$ps_sessionno);
//    $sys_function_out = $this->clmain_v200_load_line($sys_function_out,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"clmain_v750_application_class_value");

        //    $sys_function_out = $sys_function_out."#INVALUE=".$s_field_value."#INFMAT=".$ps_format_field."#DFMAT=".$s_date_fmat."#outfmat=".$s_format_field;

//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$array_line_details[$i2]." ";};
        IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
        IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name."   ps_debug = ".$ps_debug." ");};

        IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ");};

        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ";};
        IF ($sys_debug == "YES"){$sys_function_out.=" <!--".$sys_debug_text."-->";};

//X900_EXIT:
        return $sys_function_out;
    }
//##########################################################################################################################################################################
function clmain_v760_application_security_value($ps_field_value,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno)
{
//A100_TEMPLATE-INIT
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_v760_application_security_value  called from ".$ps_calledfrom;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_field_value = ".$ps_field_value."  ps_debug = ".$ps_debug." ";};
//A199_END_TEMPLATE_INIT:
//B100_ define function specific variables and codE

     require_once($_SESSION['ko_map_path'].'lib/class_app.php');
     $class_apps = new AppClass();

    global $dbcnx;
// _#tr_messagestatus_
// go to the utl_translate table
// at = after the first _

    $s_field_value = "";

    $s_field_value = $ps_field_value;

    //    echo "<br>v760  s_field_value=".$s_field_value."[] classfile=[]".$_SESSION['ko_map_path']."lib/class_app.php";

    $sys_function_out = "v760??".$s_field_value;
A100_GET_REC:
    $sys_function_out = $class_apps->clapp_a100_do_app_class("clapp_dmav2_A200_securityoption",$s_field_value,"RUN_FUNCTION",$dbcnx,"NO","clmain_v760_application_security_value",$ps_details_def,$ps_details_data,$ps_sessionno);

//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$array_line_details[$i2]." ";};
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name."  ps_debug = ".$ps_debug." ");};

    IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ");};

    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ";};
    IF ($sys_debug == "YES"){$sys_function_out.=" <!--".$sys_debug_text."-->";};

//X900_EXIT:

//echo "<br> v760 gw sys_function_out ".$sys_function_out." for s_field_value=".$s_field_value;
    return $sys_function_out;
}
//##########################################################################################################################################################################
function clmain_error_row($ps_error_text)
{
    $s_out = "";

    if (trim($ps_error_text," ") != "")
    {
       $s_out .= '<tr id="error1"><td bgcolor="cyan"><div align="center"><font size="2" face="Arial, Helvetica, sans-serif">';
       $s_out .= $ps_error_text;
       $s_out .= '</font></div></td></tr>';
    }
    return $s_out;
}
//##########################################################################################################################################################################
function clmain_v800_set_field_style($ps_field_style,$ps_map,$ps_error_list,$ps_debug,$ps_calledfrom)
{
    $s_debug = strtoupper($ps_debug);
    if ($s_debug == "YES")
    {
        echo "v800 1a called from ".$ps_calledfrom." ps_field_style=".$ps_field_style." ps_map=".$ps_map." ps_error_list=".$ps_error_list."<br>";
    }

    $s_field_style = "";
    $s_map = $this->clmain_set_map_path_n_name("",$ps_map);

    $s_default_bgcolor = "white";
    $s_error_bgcolor = "cyan";
    if (isset($_SESSION['NOERROR_COLOR']))
    {
        $s_error_bgcolor = $_SESSION['NOERROR_COLOR'];
    }
    if (isset($_SESSION['ERROR_COLOR']))
    {
        $s_error_bgcolor = $_SESSION['ERROR_COLOR'];
    }
    $s_field_list = $this->clmain_v830_set_field_style_fieldlist($s_map);
    if ($s_debug == "YES")
    {
        echo "v800 1b called from ".$ps_calledfrom." s_field_list=".$s_field_list."<br>";
    }
    $array_fields = explode("|",$s_field_list);
    for ( $i = 0; $i < count($array_fields) - 1; $i++)
    {
        $s_field_style .= $this->clmain_v820_set_field_style_field($array_fields[$i],$ps_error_list,$s_error_bgcolor,$s_default_bgcolor);
    }
    if ($s_debug == "YES")
    {
        echo "v800 1c called from ".$ps_calledfrom." s_field_style out =".$s_field_style."<br>";
    }
    return $s_field_style;

}
//##########################################################################################################################################################################
function clmain_v820_set_field_style_field($ps_field,$ps_error_list,$ps_error_bgcolor,$ps_default_bgcolor)
{
    $s_check_name = strtoupper(str_replace("fst_","",$ps_field)."|");
    $s_check_list = strtoupper($ps_error_list);
//    print_r($s_check_name."    ".$s_check_list." ebg ".$ps_error_bgcolor." dbg ".$ps_default_bgcolor."<br>");

    $s_use_color = $ps_error_bgcolor;
    if (strpos($s_check_list,$s_check_name) === false)
    {
        $s_use_color = $ps_default_bgcolor;
    }
    $s_out = "";
    $s_out .= ".".$ps_field."{background-color:".$s_use_color.";}";
    return $s_out;
}
//##########################################################################################################################################################################
function clmain_v830_set_field_style_fieldlist($ps_map)
{
//echo "v830 1 ps_map=".$ps_map."<br>";
// read map and extract all the values that start class="fst_
    global $objlib1;
    global $objSqlClient;

    $s_lineout = "";
    $sys_debug = "NO";
    $array_line_details = array();
    $array_map_lines = array();
    $s_error_fields = "";

// open map and store in array, pass array to validate_data
    $array_line_details = array();
    $s_filename=$ps_map;
//A100_TEST_FILE:
    $s_map_file_exists='Y';
    if(file_exists($s_filename))
    {
        $array_map_lines = file($s_filename);
    }
    else
    {
        $s_lineout =  "";
        goto Z900_EXIT;
    }

    $s_invalidate ="N";
    $s_map_file_error="clmain_v830_set_field_style_fieldlist file exists - last note before the read process - if you see this your map may not have a start_map_here or end_map_here";

    $s_start_map='N';
    $s_lineout ="";

    for ( $i = 0; $i < count($array_map_lines); $i++)
    {
        IF (substr($array_map_lines[$i],0,1) == "*"){
            continue;
        }
        IF (TRIM($array_map_lines[$i]) == ""){
            continue;
        }
        IF (strpos(strtoupper($array_map_lines[$i]),strtoupper('CLASS="FST_')) === false ){
            continue;
        }
// strip out all but the field names
//C100_DO_LINE:
        $array_line_details = explode("class=",$array_map_lines[$i]);
        $s_fieldname = $array_map_lines[$i];
        $s_start = strpos(strtoupper($array_map_lines[$i]),strtoupper('CLASS="FST_'));
        $s_fieldname = substr($s_fieldname,$s_start);
        $s_fieldname = str_replace('CLASS="',"",$s_fieldname);
        $s_fieldname = str_replace('class="',"",$s_fieldname);
//        $s_start = strpos(strtoupper($array_map_lines[$i]),strtoupper('"'),-1);
        $s_fieldname = substr($s_fieldname,0,strpos(strtoupper($s_fieldname),strtoupper('"')));
        $s_lineout .= $s_fieldname."|";

    }
Z900_EXIT:
//echo "v830 1 ps_map=".$ps_map."<br>";
   return $s_lineout;
}
//##########################################################################################################################################################################
    function clmain_v840_clmain_function($ps_field_value,$ps_format_field,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno)
    {
//A100_TEMPLATE-INIT
        $sys_debug_text = "";
        $sys_debug = "";
        $sys_debug = strtoupper($ps_debug);
        IF ($sys_debug !="NO") {
            $sys_debug  = "YES";
            $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
        }
        $sys_function_name = "";
        $sys_function_name = "debug - clmain_v840_clmain_function  called from ".$ps_calledfrom;
        $sys_function_out = "";
        IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_field_value = ".$ps_field_value." ps_format_field = ".$ps_format_field."  ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};

        global $dbcnx;

        $s_field_value = "";
        $s_format_field = "";

        $s_function_name = strtoupper(trim($ps_format_field));
        $s_function_name =  trim(str_replace("DOCLMAIN_","",$s_function_name));
        if (!strpos($s_function_name,"(") === false)
        {
            $s_function_name =  substr($s_function_name,0,strpos($s_function_name,"("));
        }
        $sys_function_out = "V840??".$s_function_name;
A100_GET_REC:
        $ps_action = "RUN_FUNCTION";
B010_START:
        $s_function_found = "NO";
        $s_function_list = "CLMAIN FUNCTION LIST: ";
        $s_function_help = "CLMAIN FUNCTION HELP: ";
        $ar_params = array();
        $s_do_this_function = "";
        $s_options = "";

        $s_action = strtoupper(trim($ps_action));
        if ($s_action == "LIST_FUNCTIONS")
        {
            goto B100_DO_FUNCTION;
        }
        if ($s_action == "FUNCTIONS_HELP")
        {
            goto B100_DO_FUNCTION;
        }
        if ($s_action == "RUN_FUNCTION")
        {
            goto B100_DO_FUNCTION;
        }
        $sys_function_out = "Error Unknown action of ".$ps_action." expecting LIST_FUNCTIONS,FUNCTIONS_HELP or RUN_FUNCTION";
        GOTO X900_EXIT;
B100_DO_FUNCTION:
        IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text." DO FUNCTION s_action=".$s_action." "." s_function_name =".$s_function_name." ps_function_params=".$ps_field_value);};

        $s_params = $ps_format_field;

        if (strpos($s_params,"(") === false)
        {
            goto B190_END;
        }
        $s_params =  substr($s_params,strpos($s_params,"(")+1);
        $s_params =  str_replace(")","",$s_params);
B190_END:
        $ar_params = explode("^",$s_params);
// ******************************************************************************************************************
clmain_v850_build_select:
        $s_do_this_function = "clmain_v850_build_select";
        $s_function_list = $s_function_list.$s_do_this_function.", ";
        $s_function_help = $s_function_help."clmain_v850_build_select(action,select list, selected item) = will return the select statement <br>  ";

        if (strtoupper(trim($s_function_name)) == strtoupper(trim($s_do_this_function)))
        {
            $s_function_found = "YES";
            IF (count($ar_params) <> 2)
            {
                $sys_function_out = "clmain v850 has []".count($ar_params)."[]value=[]".$s_params."[] & requires 2 params = fielname and list of values with selected field_value = []".$ps_field_value."[]";
                GOTO X900_EXIT;
            }
            if ($s_action == "RUN_FUNCTION")
            {
                $sys_function_out =  $this->clmain_v850_build_select("buildselect",$ar_params[0],$ar_params[1],$ps_field_value,$dbcnx,$ps_details_def,$ps_details_data,"NO",$ps_calledfrom);
            }
            GOTO X900_EXIT;
        }
clmain_v850_build_select_END:

// ******************************************************************************************************************





X900_EXIT:
        IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
        IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name."   ps_debug = ".$ps_debug." ");};

        IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ");};

        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ";};
        IF ($sys_debug == "YES"){$sys_function_out.=" <!--".$sys_debug_text."-->";};

//X900_EXIT:
        return $sys_function_out;
    }
//##########################################################################################################################################################################
    function clmain_v850_build_select($ps_action,$ps_name,$ps_options,$ps_selected,$p_dbcnx,$ps_details_def,$ps_details_data,$ps_debug,$ps_calledfrom)
    {
//A100_TEMPLATE-INIT
        $sys_debug_text = "";
        $sys_debug = "";
        $sys_debug = strtoupper($ps_debug);
        IF ($sys_debug !="NO") {
            $sys_debug  = "YES";
            $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
        }

        if (!strpos($ps_name,"DO_DEBUG") === FALSE)
        {
            $sys_debug  = "YES";
            $sys_debug_text = "debug from in line"; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
        }

        $sys_function_name = "";
        $sys_function_name = "debug - clmain_v850_build_select  called from ".$ps_calledfrom;
        $sys_function_out = "";
        IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_action = []".$ps_action."[] ps_options = []".$ps_options."[] ps_selected=[]".$ps_selected."[]   ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};

        $sys_function_out = "V850??".$sys_function_out;

B010_START:
        $ar_options = explode(",",$ps_options);
//        $s_select_statement = '$ps_options = []'.$ps_options.'[] count(ar_options) =[]'.count($ar_options).'[] <select name="'.$ps_name.'">';
        $s_select_statement = '<select name="'.$ps_name.'">';

        for ($i = 0;  $i < count($ar_options);$i++)
        {
            if ( trim(strtoupper($ar_options[$i])) == trim(strtoupper($ps_selected)))
            {
                $s_select_statement = $s_select_statement.'<OPTION Value="'.$ar_options[$i].'" selected>'.$ar_options[$i].'</OPTION>';
            }else{
                $s_select_statement = $s_select_statement.'<OPTION Value="'.$ar_options[$i].'">'.$ar_options[$i].'</OPTION>';
            }
        }
        if (strpos($s_select_statement,"selected>") === false)
        {
            $s_select_statement = $s_select_statement.'<OPTION Value="'.$ps_selected.'" selected>'.$ps_selected.'</OPTION>';
        }

        $s_select_statement = $s_select_statement.'</select>';
X900_EXIT:
        $sys_function_out = $s_select_statement;
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name."   ps_debug = ".$ps_debug." ");};

    IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ");};

    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ";};
    IF ($sys_debug == "YES"){$sys_function_out.=" <!--".$sys_debug_text."-->";};

//X900_EXIT:
    return $sys_function_out;

    }
//##########################################################################################################################################################################
function clmain_v900_validate_screen($ps_error_text,$ps_action,$p_dbcnx,$p_map,$ps_details_def,$ps_details_data,$ps_debug,$ps_calledfrom)
{
    $s_lineout = "";
    $s_error_list = "";
    $s_result = "";
    $s_check_value = array();

    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;

    $sys_function_name = "";
    $sys_function_name = "debug - clmain_v900_validate_screen - from ".$ps_calledfrom;
    $sys_function_out = "";
    $sys_debug = $ps_debug;

 // gw 20131231   $sys_debug = "YES";

    IF ($sys_debug == "YES"){echo "<br>".$sys_function_name."  <br><br>DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){echo "<br>".$sys_function_name."  VALIDATE SCREEN GOES THROUGH EACH POSTV_FIELD AND SEES IF THERE IS ANY VALIDATION FOR THE FIELD<br>";};


// open map and store in array, pass array to validate_data
    $array_line_details = array();
    $s_filename=$this->clmain_set_map_path_n_name("",$p_map);
//A100_TEST_FILE:
    $s_map_file_exists='Y';
    if(file_exists($s_filename))
    {
        $array_map_lines = file($s_filename);
    }
    else
    {
        $s_map_file_exists='N';
        $s_lineout =  "<!--   clmain_v900_validate_screen - ERROR VALIDATION MAP DOES NOT EXIST..Map =".$s_filename." -->";

        DIE ("CLMAIN_V900_VALIDATE  - ERROR VALIDATION MAP DOES NOT EXIST..Map =[]".$s_filename."[]");

        return $s_lineout;
        exit();
    }


    foreach($_POST as $key => $value)
    {
            IF ($sys_debug == "YES"){echo "<br>starting a new value ";};
            IF ($sys_debug == "YES"){echo "<br>".$sys_function_name." pre all to clmain_v910_validate_data ps_action =".$ps_action." key=".$key." value=(".$value.")";};
            $s_result = $this->clmain_v910_validate_data($ps_action,$key,$value,$array_map_lines,$p_dbcnx,$s_details_def,$s_details_data,$sys_debug,"clmain v900");
            IF ($sys_debug == "YES"){echo "<br>".$sys_function_name." result of clmain_v910_validate_data=(".$s_result.") blank in brackets = pass ie no error";};
            if (trim($s_result," ") !== "")
            {
                $s_error_list  .= $key."|";
            }
            $s_lineout .= $s_result;
    }

    $_SESSION['validate_error_list'] = $s_error_list;
    $_SESSION['validate_error_html'] = $s_lineout;
    $_SESSION['validate_error_html2'] = $s_lineout;
    $_SESSION['validate_error_startatfield'] = substr($s_error_list,0,strpos($s_error_list,"|"));
    $_SESSION['validate_error_screenvalues_def'] = "";
    $_SESSION['validate_error_screenvalues_data'] = "";

// gw 20100322 put all the post values from the screen in the error_screenvalues session var
    foreach($_POST as $key => $value)
    {
           $_SESSION['validate_error_screenvalues_def'] = $_SESSION['validate_error_screenvalues_def']."|SESSERR_".$key;
           $_SESSION['validate_error_screenvalues_data'] = $_SESSION['validate_error_screenvalues_data']."|".$value;
    }


//       echo "<br>clmain_v900 gw ";
//       ECHO "<br> screenvalues_def=".$_SESSION['validate_error_screenvalues_def'];
//   ECHO "<br> screenvalues_data=".$_SESSION['validate_error_screenvalues_data'];
//   echo "<br>clmain_v900 gw ";

    IF ($sys_debug == "YES"){echo $sys_function_name."<br>E_LIST SESSION['validate_error_list']=[]".$_SESSION['validate_error_list']."[]<BR>";};
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>E_HTML SESSION['validate_error_html']=[]".    $_SESSION['validate_error_html']."[]";};
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>E_HTML SESSION['validate_error_html2']=[]".    $_SESSION['validate_error_html2']."[]";};
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>E_STARTAT SESSION['validate_error_startatfield']=[]".    $_SESSION['validate_error_startatfield']."[]<BR>";};
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>SV_DEF SESSION['validate_error_screenvalues_def']=[]".    $_SESSION['validate_error_screenvalues_def']."[]<BR>";};
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>SV_DATA SESSION['validate_error_screenvalues_data']=[]".    $_SESSION['validate_error_screenvalues_data']."[]<BR>";};
// gw 20100321
//    ECHO "V 900_E_LIST ".$_SESSION['validate_error_list']."<BR>";
//    ECHO "V 900_E_HTML ".    $_SESSION['validate_error_html']."<BR>";
//    ECHO "V 900_E_STARTAT ".    $_SESSION['validate_error_startatfield']."<BR>";
//    ECHO "V 900_E_SV_DEF ".    $_SESSION['validate_error_screenvalues_def']."<BR>";
//    ECHO "V 900_E_SV_DATA ".    $_SESSION['validate_error_screenvalues_data']."<BR>";


    IF ($sys_debug == "YES"){echo "<br>".$sys_function_name." exit <br>";};
    return $s_lineout."|#&seperate&#|".$s_error_list;

}
//##########################################################################################################################################################################
function clmain_v910_validate_data($ps_action,$ps_fieldname,$ps_value,$p_array_map_lines,$p_dbcnx,$ps_details_def,$ps_details_data,$ps_debug,$ps_calledfrom)
{

    $s_lineout = "";
    $sys_debug = "NO";
    $array_line_details = array();
    $array_map_lines = array();
    $array_map_lines = $p_array_map_lines;
    $s_error_fields = "";
    $sys_debug_text = "";
    $sys_function_name = "clmain_v910_validate_data from ".$ps_calledfrom."  ";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $s_invalidate ="N";
    $s_map_file_error="file exists - last note before the read process - if you see this your map may not have a start_map_here or end_map_here";

    $s_start_map='N';
    $s_lineout ="";

    IF ($sys_debug == "YES"){echo "<br>###########################################################";};
    IF ($sys_debug == "YES"){echo "<br>".$sys_function_name."   fieldname=(".$ps_fieldname.")   fieldvalue=(".$ps_value.")<br>";};

    for ( $i = 0; $i < count($array_map_lines); $i++)
    {
//        IF ($sys_debug == "YES"){echo "<br>".$sys_function_name."   map line".$i." =".$array_map_lines[$i]."<br>";};
        IF (substr($array_map_lines[$i],0,1) == "*"){
            continue;
        }
        IF (substr($array_map_lines[$i],0,2) == "//"){
            continue;
        }
        IF (TRIM($array_map_lines[$i]) == ""){
            continue;
        }
    // used to indicate if there is a start validate loop
        IF (strpos(strtoupper($array_map_lines[$i]),strtoupper("START_VALIDATE_HERE")) > 0 ){
//            IF ($sys_debug == "YES"){echo  $sys_function_name."got the map starter start line for validate<br>".$array_map_lines[$i]."<br>";};
            IF ($sys_debug == "YES"){echo  $sys_function_name."got the map starter start line for validate<br>";};
            $s_invalidate ="Y";
            continue;
        }
    // used to indicate if there is a end validate loop
        IF (strpos(strtoupper($array_map_lines[$i]),strtoupper("END_VALIDATE_HERE")) !== false ){
            IF ($sys_debug == "YES"){echo  $sys_function_name."got the map end line for validate<br>".$array_map_lines[$i]."<br>";};
            $s_invalidate ="N";
            break;
        }
        // if before the required loop get the next line
        if ($s_invalidate !="Y"){
           continue;
        }
        IF ($sys_debug == "YES"){echo "<br>".$sys_function_name." ".$i." in validate loop".$array_map_lines[$i]."<br>";};

        $s_do_test = "NO";

//C100_DO_LINE:
        $array_line_details = explode("|",$array_map_lines[$i]);


            IF ($sys_debug == "YES"){echo $sys_function_name."checking line <br>".$array_map_lines[$i]."<br>";};
        //        IF (strotupper($array_line_details[0]) =="V1")
//        { GOTO D100_DO_V2 }
          IF (strtoupper($array_line_details[1]) == "ALL")
          {
              $s_do_test = "YES";
              IF ($sys_debug == "YES"){echo $sys_function_name." do for all actions do test = (".$s_do_test.")<br>";};
          }
          IF (strtoupper(TRIM($array_line_details[1]," ") == strtoupper($ps_action)))
          {
              $s_do_test = "YES";
              IF ($sys_debug == "YES"){echo $sys_function_name." do for action ".$ps_action." do test = (".$s_do_test.")<br>";};
          }
          IF (strtoupper(TRIM($array_line_details[2]," ")) != strtoupper($ps_fieldname))
          {
              $s_do_test = "NO";
              IF ($sys_debug == "YES"){echo $sys_function_name." field names dont match, Line value =(".strtoupper(TRIM($array_line_details[2]," ")).") field value =(".strtoupper($ps_fieldname).")   do test = (".$s_do_test.")<br>";};
          }

//D100_DO_V2:
//X900_END_LOOP:
        $s_validate_type = strtoupper(TRIM($array_line_details[3]," "));
        IF ($sys_debug == "YES"){echo $sys_function_name." do for validate type = ".$s_validate_type."<br>";};
        $s_validate_subtype = $array_line_details[4];
        $s_validate_subtype_tidy = strtoupper(TRIM($array_line_details[4]," "));
        if (strpos($s_validate_subtype_tidy,"%!_") === false)
        {}else{
            $s_validate_subtype_tidy = str_replace("#p","|",$s_validate_subtype_tidy);
            $s_validate_subtype_tidy = str_replace("#P","|",$s_validate_subtype_tidy);
            $s_validate_subtype_tidy = $this->clmain_v300_set_variable( $s_validate_subtype_tidy,$ps_details_def,$ps_details_data,"no","","class_main v100");
        }
        IF ($sys_debug == "YES"){echo $sys_function_name." do for validate subtype = ".$s_validate_subtype_tidy."<br>";};

        $s_error_message =  $array_line_details[5];
        $s_error_message = str_replace("#p","|",$s_error_message);
        $s_error_message = str_replace("#P","|",$s_error_message);
        $s_error_message = $this->clmain_v200_load_line($s_error_message,$ps_details_def,$ps_details_data,"NO","1","validate_err_message");

        IF($s_do_test <> "YES")
        {
            IF ($sys_debug == "YES"){echo $sys_function_name." not doing the test on this line<br>";};
            GOTO C900_LOOP;
        }

//echo "clmain v9j10 debug line ".strtoupper(trim($array_map_lines[$i]))."<br>";

        if (strpos(strtoupper(trim($array_map_lines[$i])),"*DO_DEBUG*") === FALSE)
        {}else{
            $sys_debug = "YES";
            echo "  clmain v910 Line level debug has been set for ".$ps_fieldname." ****";
        }
        IF ($sys_debug == "YES"){echo $sys_function_name." do for field =".$ps_fieldname."<br>";};

C100_DO_TESTS:
        IF ($s_validate_type <> strtoupper("VALUE"))
        {
            GOTO C100_SKIP_TEST_VALUE;
        }
        IF ($s_validate_subtype_tidy == strtoupper("NOT_SPACES"))
        {
            IF (trim($ps_value," ") == "" )
            {
//                   $s_lineout .= $array_map_lines[$i]."<br>".$ps_fieldname."<BR>".$array_line_details[5]."<BR>";
               $s_lineout .= $s_error_message;
            }
        }
        IF ($s_validate_subtype_tidy == strtoupper("NOT_ZEROES"))
        {
            IF (!is_numeric($ps_value))
            {
               $s_lineout .= $s_error_message;
            }
            IF (intval($ps_value) == 0 )
            {
               $s_lineout .= $s_error_message;
            }
        }

        IF ($s_validate_subtype_tidy == strtoupper("has_at_sign"))
        {
            IF (strpos($ps_value,"@") === false )
            {
               $s_lineout .= $s_error_message;
            }
        }

        IF ($s_validate_subtype_tidy == strtoupper("NUMERIC"))
        {
            IF (!is_numeric($ps_value))
            {
               $s_lineout .= $s_error_message;
            }
        }
        IF ($s_validate_subtype_tidy == strtoupper("YES_OR_NO"))
        {
            IF (strtoupper($ps_value) == "YES")
            {
                GOTO C109_END;
            }
            IF (strtoupper($ps_value) == "NO")
            {
                GOTO C109_END;
            }
            $s_lineout .= $s_error_message;
        }

          IF ($sys_debug == "YES"){echo "<br>".$sys_function_name." Invalid subtype for the type check = ".$s_validate_subtype_tidy;};
C109_END:
          GOTO C900_LOOP;
C100_SKIP_TEST_VALUE:
          IF ($s_validate_type == strtoupper("MIN_LENGTH"))
          {
              $s_check_value = $s_validate_subtype_tidy;
    //$s_lineout .= "doing the min_length ".$s_check_value." value length ".strlen($ps_value)." subtype ".$s_validate_subtype_tidy."<BR>".$array_line_details[5]."<BR>";
              IF (strlen($ps_value) < $s_check_value)
              {
               $s_lineout .= $s_error_message;
    //                   $s_lineout .= "realy does fail length ".$s_check_value." value length ".strlen($ps_value)." subtype ".$s_validate_subtype_tidy."<BR>".$array_line_details[5]."<BR>";
               }
          }
C110_SKIP_MIN_LENGTH:
C110_DO:
          IF ($s_validate_type <> strtoupper("SQL_NOREC"))
          {
              goto C119_SKIP_SQL_NOREC;
          }

          // This sql must return no valid records
           $s_sql=str_replace("#p","|",$array_line_details[4]);
           $s_sql=str_replace("#P","|",$s_sql);
           IF ($sys_debug == "YES"){echo $sys_function_name." doing sql_norec process for sql=".$s_sql."<br>";};


           IF ($sys_debug == "YES"){echo "det def=".$ps_details_def."<br>";};
           IF ($sys_debug == "YES"){echo "det data=".$ps_details_data."<br>";};

           $s_sql = $this->clmain_v200_load_line($s_sql,$ps_details_def,$ps_details_data,"NO","1","validate");
           IF ($sys_debug == "YES"){echo $sys_function_name." doing sql_norec process after line procesing sql=".$s_sql."<br>";};


            $rs_temp = mysqli_query($p_dbcnx,$s_sql);
            if (!$rs_temp)
            {
                IF ($sys_debug == "YES"){echo $sys_function_name." error - not temp <br>";};
                $s_lineout .= $s_error_message;
                $s_lineout .= "<!-- VALIDATION SQL_NOREC no rstemp mysql response is ".mysqli_error()."--<br>-->";
                GOTO C118_END;
            }
            if (mysqli_num_rows($rs_temp) <> 0 )
            {
                $s_lineout .= $s_error_message;
                $s_lineout .= "<!-- - VALIDATION SQL_NOREC num_rows not 0  mysql response is ".mysqli_error()."--<br>-->";
                GOTO C118_END;
            }
            if (mysqli_num_rows($rs_temp) === false )
            {
                IF ($sys_debug == "YES"){echo $sys_function_name." error - not num_rows === false <br>";};
                $s_lineout .= $s_error_message;
                $s_lineout .= "<!-- - VALIDATION SQL_NOREC num_rows false mysql response is ".mysqli_error()."--<br>-->";
            }
    //               $s_lineout .= " sql no_rec for   ".$s_sql;
C118_END:
           IF ($sys_debug == "YES"){echo $sys_function_name." result is =".$s_lineout."<br>";};

          GOTO C900_LOOP;
C119_SKIP_SQL_NOREC:
          IF ($s_validate_type == strtoupper("SQL_REC"))
          {
               $s_sql=str_replace("#p","|",$array_line_details[4]);
               $s_sql=str_replace("#P","|",$s_sql);
               $s_sql = $this->clmain_v200_load_line($s_sql,$ps_details_def,$ps_details_data,"NO","1","validate");
                $rs_temp = mysqli_query($p_dbcnx,$s_sql);
                if (!$rs_temp)
                {
                    $s_lineout .= $s_error_message;
                    $s_lineout .= "<!-- - VALIDATION SQL_NOREC no rstemp mysql response is ".mysqli_error()."--<br>-->";
                }
                if (mysqli_num_rows($rs_temp) === false )
                {
                    $s_lineout .= $s_error_message;
                    $s_lineout .= "<!-- - VALIDATION SQL_NOREC num_rows false mysql response is ".mysqli_error()."--<br>-->";
                }
                if (mysqli_num_rows($rs_temp) == 0 )
                {
                    $s_lineout .= $s_error_message;
                    $s_lineout .= "<!-- - VALIDATION SQL_NOREC num_rows not 0  mysql response is ".mysqli_error()."--<br>-->";
                }

    //gw20100609 - dont want this            $s_lineout .= " sql rec for  ".$s_sql;
          }
    //      GOTO C900_LOOP;
    C130_SKIP_SQL_REC:

    //echo "clm c130 doing value_not<br>";
            IF ($s_validate_type <> strtoupper("VALUE_NOT"))
            {
                GOTO C140_SKIP_VALUE_NOT;
            }
    //echo "clm c130 testing doing value_not ps_value = ".$ps_value."  s_validate_subtype_tidy =".$s_validate_subtype_tidy."<br>";
            IF (strtoupper(trim($ps_value," ")) == strtoupper(trim($s_validate_subtype_tidy," ")))
            {
                   $s_lineout .= $s_error_message;
            }
          GOTO C900_LOOP;
    C140_SKIP_VALUE_NOT:


C900_LOOP:
    }


Z900_EXIT:
    IF ($sys_debug == "YES"){echo "<br>".$sys_function_name."   fieldname=(".$ps_fieldname.")   fieldvalue=(".$ps_value.")<br> lineout = (".$s_lineout.")";};
    IF ($sys_debug == "YES"){echo "<br>".$sys_function_name." ####  end of function <br>";};
   return $s_lineout;


}
//##########################################################################################################################################################################
function clmain_v920_error_row($ps_error_text, $ps_debug, $ps_calledfrom)
{
    $s_out = "";

    if (trim($ps_error_text," ") != ""){
       $s_out .= '<table><tr id="error1"><td bgcolor="cyan"><div align="center"><font size="2" face="Arial, Helvetica, sans-serif">';
       $s_out .= $ps_error_text;
       $s_out .= '</font></div></td></tr></table>';
    }
    if ($ps_debug <> "NO")
    {
        ECHO "<br> v920_debuG ############################= ".$ps_calledfrom."<br>s_out=".$s_out."<BR>";
        ECHO "<br> v920_debug ps_calledfrom = ".$ps_calledfrom."<br>s_out=".$s_out."<BR>";
        ECHO "<br> v920_debug ps_calledfrom = ".$ps_calledfrom."<br>ps_error_text=[]".$ps_error_text."[]<BR>";
        ECHO "<br> v920_debug ps_calledfrom = ".$ps_calledfrom."<br>s_out=".$s_out."<BR>";
    }

    return $s_out;
}
//##########################################################################################################################################################################
function clmain_v930_field_startat($ps_session_startatfield, $ps_default,$ps_debug, $ps_calledfrom)
{
    $s_out = $ps_default;
    if (trim($ps_session_startatfield," ") <> "")
    {
       $s_out = $ps_session_startatfield;
    }
    if ($ps_debug <> "NO")
    {
        ECHO "<br> v930_debug ps_calledfrom = ".$ps_calledfrom."<br>ps_session_startatfield=".$ps_session_startatfield." ps_default=".$ps_default."<br>s_out=".$s_out;
    }
    return $s_out;
}
//##########################################################################################################################################################################
function clmain_v940_initialise_error_sessionvs($ps_error_text, $ps_debug, $ps_calledfrom)
{
    $_SESSION['validate_error_list'] = "";


//    echo "<br>##############gw v940 <br>".microtime(true);

    $_SESSION['validate_error_html'] = "";
    $_SESSION['validate_error_startatfield'] = "";
    $_SESSION['validate_error_screenvalues_def'] = "";
    $_SESSION['validate_error_screenvalues_data'] = "";
}
//##########################################################################################################################################################################
function clmain_v945_adddetails_for_sesserr_fields($ps_debug, $ps_calledfrom)
{

    //gw20100802 - not tested - need it to just provide the field names and values back to be added to the details_def n _data
    $key ="";
    $value ="";
    $s_out ="";

    $_SESSION['validate_error_screenvalues_def'] = $_SESSION['validate_error_screenvalues_def']."|SESSERR_".$key;
    $_SESSION['validate_error_screenvalues_data'] = $_SESSION['validate_error_screenvalues_data']."|".$value;

           $s_def = str_replace("|SESSERR_","|",$_SESSION['validate_error_screenvalues_def']);
           $s_data = $_SESSION['validate_error_screenvalues_data'];


    $sys_function_out = $s_def."|^%##%^|".$s_out;
}
//##########################################################################################################################################################################
    function clmain_v946_add_uidetails_for_sesserr_fields($ps_debug, $ps_calledfrom)
    {
        $s_def = "DMAV2_FIELD_VALUES";
        $s_out ="VIA_CLMAIN_946";

        $s_def = $s_def.str_replace("|SESSERR_","&dopipe&ui_",$_SESSION['validate_error_screenvalues_def']);
        $s_out = $s_out.str_replace("|","&dopipe&",$_SESSION['validate_error_screenvalues_data']);
////$s_out = $s_out.$_SESSION['validate_error_screenvalues_data'];

        $sys_function_out = $s_def."&dopipe&|^%##%^|&dopipe&".$s_out;
        return $sys_function_out;
    }
//##########################################################################################################################################################################
    function clmain_v947_add_uidetails_from_rundata($ps_recordset_id,$ps_debug, $ps_calledfrom,$ps_details_def,$ps_details_data)
    {
        $sys_debug = "";
        $sys_debug = strtoupper($ps_debug);
        $sys_function_name = "clmain_v947_add_uidetails_from_rundata";

        IF ($sys_debug == "YES"){echo "<br>";};
        IF ($sys_debug == "YES"){echo $sys_function_name."  ps_debug = ".$ps_debug."<br>";};

        $s_def = "DMAV2_FIELD_VALUES&dopipe&";
        $s_out ="VIA_CLMAIN_947&dopipe&";

        $array_def = explode("|",$ps_details_def);
        $array_data = explode("|",$ps_details_data);

        for ($i2 = 0; $i2 < count($array_def); $i2++)
        {
            IF ($sys_debug == "YES"){echo $sys_function_name." - ".$i2." details def ".$i2." = ".$array_def[$i2]." = data of ".$array_data[$i2]."<br>";};
            IF (strpos(strtoupper($array_def[$i2]), strtoupper($ps_recordset_id)) !== false) {
                $s_fieldname = $array_def[$i2];
                $s_fieldname = str_replace(strtolower($ps_recordset_id),"ui",strtolower( $s_fieldname));

                $s_def = $s_def . $s_fieldname . "&dopipe&";
                $s_out = $s_out . $array_data[$i2] . "&dopipe&";
            }
        }


        $sys_function_out = $s_def."&dopipe&|^%##%^|&dopipe&".$s_out;
        return $sys_function_out;
    }
//##########################################################################################################################################################################
function clmain_v950_next_prev_row($ps_nex_prev_def, $ps_debug, $ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    $s_out = "";

//    ECHO "<BR> v950 def =".$ps_nex_prev_def." sessdef = ".$_SESSION['rundata_sql_def'];

    IF (STRTOUPPER(TRIM($ps_nex_prev_def," ")) == "CLEARALL" )
    {
        $_SESSION['rundata_sql_def']="";
        $_SESSION['rundata_sql_data']="";
        GOTO Z900_EXIT;
    }
    IF (STRTOUPPER(TRIM($_SESSION['rundata_sql_def']," ")) == "" )
    {
        GOTO Z900_EXIT;
    }

       $s_first_html = '&nbsp;&nbsp;&nbsp;&nbsp;';
       $s_prev_html = '&nbsp;&nbsp;&nbsp;&nbsp;';
       $s_next_html = '&nbsp;&nbsp;&nbsp;&nbsp;';
       $s_last_html = '&nbsp;&nbsp;&nbsp;&nbsp;';

    // get the values for current page, last page
       $s_total_pages = $this->clmain_v200_load_line( "|%!_rundatasql_total_pages_!%|",$ps_details_def.$_SESSION['rundata_sql_def'],$ps_details_data.$_SESSION['rundata_sql_data'],"no",$ps_sessionno,"clmain v950 ");
       $s_page_current = $this->clmain_v200_load_line( "|%!_rundatasql_page_current_!%|",$ps_details_def.$_SESSION['rundata_sql_def'],$ps_details_data.$_SESSION['rundata_sql_data'],"no",$ps_sessionno,"clmain v950 ");


       if ($s_page_current <> 1)
       {
           $s_first_html = '    <input type="submit" name="ACTION" value="First Page">';
           $s_prev_html = '    <input type="submit" name="ACTION" value="Prev Page">';
       }
       if ($s_page_current <> $s_total_pages)
       {
          $s_next_html = '    <input type="submit" name="ACTION" value="Next Page">';
       }
       if ($s_page_current <> $s_total_pages)
       {
            $s_last_html = '    <input type="submit" name="ACTION" value="Last Page">';
       }


    /*if (trim($ps_error_text," ") != ""){
       $s_out .= '<tr id="error1"><td bgcolor="cyan"><div align="center"><font size="2" face="Arial, Helvetica, sans-serif">';
       $s_out .= $ps_error_text;
       $s_out .= '</font></div></td></tr>';
  *  }
*/
B100_DO_MAP_FOLDER:
// IF THE FILE "NEXT_PREV_PAGE.HTML" EXISTS IT THE UT_MAP FOLDER - USE THAT
// GOTO Z800_DO:
C100_DO_UTIL_FOLDER:
// IF THE FILE "NEXT_PREV_PAGE.HTML" EXISTS IT THE UT_PROG FOLDER - USE THAT
// GOTO Z800_DO:
D100_DEFAULT:

    $s_out .= '<TR ><td colspan="20">';
    $s_out .= '<table><tr>';
    $s_out .= '<td  style="text-align:left;width: 200px">';
    $s_out .= $s_first_html;
    $s_out .= $s_prev_html;
    $s_out .= '</td><td style="text-align:center">';
    $s_out .= 'page |%!_rundatasql_page_current_!%| of |%!_rundatasql_total_pages_!%| ,';
    $s_out .= '|%!_rundatasql_page_limit_!%| records from |%!_rundatasql_current_starts_at_row_!%| of total |%!_rundatasql_total_rows_!%|';
    //       $s_out .= ' took |%!_rundatasql_duration_!%|';
    $s_out .= '</td><td style="text-align:right;width: 200px">';
    $s_out .= $s_next_html;
    $s_out .= $s_last_html;
    $s_out .= '</td></tr></table>';
    $s_out .= '    <input name="prev_starts_at_row" type="hidden" value="|%!_rundatasql_prev_starts_at_row_!%|">';
    $s_out .= '    <input name="next_starts_at_row" type="hidden" value="|%!_rundatasql_next_starts_at_row_!%|">';
    $s_out .= '    <input name="page_prev" type="hidden" value="|%!_rundatasql_page_prev_!%|">';
    $s_out .= '    <input name="page_next" type="hidden" value="|%!_rundatasql_page_next_!%|">';
    $s_out .= '    <input name="page_limit" type="hidden" value="|%!_rundatasql_page_limit!%|">';
    $s_out .= '    <input name="total_rows" type="hidden" value="|%!_rundatasql_total_rows_!%|">';
    $s_out .= '    <input name="total_pages" type="hidden" value="|%!_rundatasql_total_pages_!%|">';
    $s_out .= '</td>';
    $s_out .= '</tr>';

Z800_DO:
       $s_out = $this->clmain_v200_load_line( $s_out,$ps_details_def.$_SESSION['rundata_sql_def'],$ps_details_data.$_SESSION['rundata_sql_data'],"no",$ps_sessionno,"clmain v950 ");
Z900_EXIT:
    return $s_out;
}
//##########################################################################################################################################################################
function clmain_v955_run_syscommand($ps_line_in, $ps_debug, $ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno)
{
A100_TEMPLATE_INIT:
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_v955_run_syscommand  called from ".$ps_calledfrom;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." ps_line_in=".$ps_line_in." ps_debug=".$ps_debug." ps_calledfrom=".$ps_calledfrom."<br> ";};
A199_END_TEMPLATE_INIT:

    $ar_line_value = explode("|",$ps_line_in);
    $s_operation = $ar_line_value[1];
    if (strtoupper($s_operation) == "FILECOPY")
    {
        $sys_function_out = "";
//    $s_line_in = $class_main->clmain_v200_load_line( $s_line_in,$s_details_def,$s_details_data,"no",$s_sessionno,"ut_jcl o_200");

        $s_filefrom = $ar_line_value[2];
        $s_filefrom = STR_REPLACE("#P","|",$s_filefrom);
        $s_filefrom = STR_REPLACE("#p","|",$s_filefrom);
        $s_filefrom = $this->clmain_v200_load_line($s_filefrom,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"ut_jcl o_200");
        $s_filefrom = STR_REPLACE("/","\\",$s_filefrom);
//        $s_filefrom = STR_REPLACE("\\","\\\\",$s_filefrom);

        $s_fileto = $ar_line_value[3];
        $s_fileto = STR_REPLACE("#P","|",$s_fileto);
        $s_fileto = STR_REPLACE("#p","|",$s_fileto);
        $s_fileto = $this->clmain_v200_load_line($s_fileto,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"ut_jcl o_200");
        $s_fileto = STR_REPLACE("/","\\",$s_fileto);

        if (!copy($s_filefrom,$s_fileto))
        {
            echo ("<br>**** error file file copy ****<br>v955 file copy failed, <br>s_filefrom=".$s_filefrom." <br>s_fileto=".$s_fileto."<br><br>");
        }
        goto Z900_EXIT;
    }
//run_syscommand|shell|c:\tdx\html\ordwatch\owl_op_makeasnfile.bat
    if (strtoupper($s_operation) == "SHELL")
    {
        $sys_function_out = "";
//    $s_line_in = $class_main->clmain_v200_load_line( $s_line_in,$s_details_def,$s_details_data,"no",$s_sessionno,"ut_jcl o_200");

        $s_filefrom = $ar_line_value[2];
        $s_filefrom = STR_REPLACE("#P","|",$s_filefrom);
        $s_filefrom = STR_REPLACE("#p","|",$s_filefrom);
        $s_filefrom = $this->clmain_v200_load_line($s_filefrom,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"ut_jcl o_200");
        $s_filefrom = STR_REPLACE("/","\\",$s_filefrom);

        $s_output = shell_exec($s_filefrom);

            echo ("<br>**** output from shell  <br>s_command=".$s_output." <br><br>");
            echo ("<br>**** shell failed <br>s_command=".$s_filefrom." <br><br>");

        goto Z900_EXIT;
    }

    if (strtoupper($s_operation) == "EXEC")
    {
        $sys_function_out = "";
//    $s_line_in = $class_main->clmain_v200_load_line( $s_line_in,$s_details_def,$s_details_data,"no",$s_sessionno,"ut_jcl o_200");

        $s_filefrom = $ar_line_value[2];
        $s_filefrom = STR_REPLACE("#P","|",$s_filefrom);
        $s_filefrom = STR_REPLACE("#p","|",$s_filefrom);
        $s_filefrom = $this->clmain_v200_load_line($s_filefrom,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"ut_jcl o_200");
        $s_filefrom = STR_REPLACE("/","\\",$s_filefrom);

        $s_output = shell_exe($s_filefrom);

            echo ("<br>**** output from shell  <br>s_command=".$s_output." <br><br>");
            echo ("<br>**** shell failed <br>s_command=".$s_filefrom." <br><br>");

        goto Z900_EXIT;
    }


    $sys_function_out = "FALSE";
    IF ($sys_debug == "YES"){echo "b100_equals<br> ";};

    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_line_in=".$ps_line_in." is Unknown <BR>";};

Z900_EXIT:
    return $sys_function_out;
}
//##########################################################################################################################################################################
function z900_function_template($p_var1,$p_var2,$ps_debug,$ps_calledfrom)
{
    $ps_in_line = "";
    $ps_details_def = "";
    $ps_details_data = "";
    $ps_username = "";
    $ps_pwd = "";


A100_TEMPLATE_INIT:
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - function_template  called from ".$ps_calledfrom;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_in_line = ".$ps_in_line." ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};
A199_END_TEMPLATE_INIT:

//B100_ define function specific variables and codE


//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$array_line_details[$i2]." ";};
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){$this->z901_dump(   $sys_debug_text."=".$sys_function_name." p_username = ".$ps_username." p_pwd = ".$ps_pwd."  ps_debug = ".$ps_debug." ");};

    IF ($sys_debug == "YES"){$this->z901_dump($sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ");};

    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ";};
    IF ($sys_debug == "YES"){$sys_function_out.=" <!--".$sys_debug_text."-->";};

X900_EXIT:
    return $sys_function_out;
}
//##########################################################################################################################################################################
// writes the log file for this class
function z901_dump($ps_text)
{
/*    if (!file_exists("logs"))
    {
        $md= mkdir("logs",0755,true);
    }
    $dateym = date('Ym');
    $myfile = "logs/Log_clmain_".$dateym.".txt";
    $fh = fopen($myfile,'a') or die("cant open file".$myfile);
    fwrite($fh,date("Y-m-d H:i:s",time())." - ".$ps_text."\r\n");
    fclose($fh);
*/
    ECHO $ps_text,"<br>";
}
//##########################################################################################################################################################################
function z902_dump_debug($ps_process,$ps_text)
{
    if (!isset($_SESSION['dump_to_debug']))
    {
        $_SESSION['dump_to_debug'] = "NO";
    }
    if (!isset($_SESSION['dump_for_process']))
    {
        $_SESSION['dump_for_process'] = "ALL";
    }

    if ($_SESSION['dump_to_debug'] == "NO")
    {
        GOTO Z900_EXIT;
    }
    if ($_SESSION['dump_for_process'] = "ALL")
    {
        GOTO B100_WRITE;
    }
    if (strtoupper(trim($ps_process)) <> strtoupper(trim($_SESSION['dump_for_process'])))
    {
        GOTO B100_WRITE;
    }

B100_WRITE:
    if (!file_exists("logs"))
    {
        $md= mkdir("logs",0777,true);
    }
//    date_default_timezone_set('Australia/Brisbane');

    $dateym = date('Ym');
    $myfile = "logs/clmain_debug_".$dateym.".html";
    $fh = fopen($myfile,'a') or die("cant open file".$myfile);
    fwrite($fh,"<br>Bris@=".date("Y-m-d H:i:s",time())."@".date_default_timezone_get()." for process=".strtoupper(trim($ps_process))." debug for process=".strtoupper(trim($_SESSION['dump_for_process']))." - ".$ps_text."");
    fclose($fh);

//    echo "<br>myfile".$myfile;
//    echo "<br>ps_text".$ps_text;
Z900_EXIT:
}
//##########################################################################################################################################################################
function z903_dump($ps_text)
{
    if (!file_exists("logs"))
    {
        $md= mkdir("logs",0755,true);
    }
    $dateym = date('Ym');
    $myfile = "logs/Log_clmain_".$dateym.".txt";
    $fh = fopen($myfile,'a') or die("cant open file".$myfile);
    fwrite($fh,date("Y-m-d H:i:s",time())." - ".$ps_text."\r\n");
    fclose($fh);

}
//##########################################################################################################################################################################
// extract the values from the standard params list
function clmain_get_param($ps_url_siteparams,$ps_fieldname,$ps_debug)
{
    $sys_function_name = "";
    $sys_function_name = "clmain_get_param";

    $sys_debug = "";
    $s_lineout = "";
    $s_array = array();

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug == "YES"){echo $sys_function_name." start of function params are params= ".$ps_url_siteparams."  fieldname = ".$ps_fieldname."<br>";};

    $s_array = explode("^",$ps_url_siteparams);
    reset($s_array);
    foreach($s_array as $line)
    {
        IF ($sys_debug == "YES"){echo $sys_function_name." array line = ".$line."<br>";};
    }

    $s_lineout  = "unknownparamname";

    IF (strtolower($ps_fieldname) == "s_sessionno"){
       $s_lineout  = $s_array[0];
    }
    IF ($sys_debug == "YES"){echo $sys_function_name." end of function  output of ".$ps_fieldname." = ".$s_lineout."<br>";};
    //exit function
    return $s_lineout;
}
//##########################################################################################################################################################################
// set the list of user styles and values
function clmain_set_user_style($ps_username,$ps_debug)
{
    $sys_function_name = "";
    $sys_function_name = "clmain_set_user_style";

    $sys_debug = "";
    $s_lineout = "";
    $s_fieldlist = "";
    $s_fieldvalues = "";

    $s_fieldlist = "|USR_BDYLEFT_CSS";
    $s_fieldvalues = "|"."udp_bdyleft.css";

    //exit function
    $s_lineout = $s_fieldlist."^".$s_fieldvalues;
    return $s_lineout;
}
//##########################################################################################################################################################################
function clmain_u100_mkdirs($path,$mode=0777)
{
    $s_seperator = "/";
    if (isset($_SESSION['ko_host_os']))
    {
        if (strtoupper($_SESSION['ko_host_os']) <> "LINUX")
        {
            $s_seperator = "\\";
        }
    }

//    $dirs = explode($GLOBALS["dirseparator"],$path);

    $path = str_replace("\\","/",$path);
    $dirs = explode("/",$path);
    $pos = strrpos($path, ".");
    if ($pos === false) { // note: three equal signs
        // not found, means path ends in a dir not file
        $subamount=0;
    }
    else
    {
        $subamount=1;
    }

    for ($c=0;$c < count($dirs) - $subamount; $c++)
    {
        $thispath="";
        for ($cc=0; $cc <= $c; $cc++)
        {
            $thispath.=$dirs[$cc].$s_seperator;
        }
        if (!file_exists($thispath))
        {
            //print "$thispath<br>";
            $md = @mkdir($thispath,$mode);
            //@chmod($thispath,$mode);
            if($md===false)
            {
                return false;
            }
        }
    }
    return true;
}
//##########################################################################################################################################################################
function clmain_u120_do_add_details_line($ps_dbcnx,$ps_line,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_url_siteparams)
{


//echo "<br>gw clmainu120 ps_line=[]".$ps_line."[]";

    $sys_debug_text = "clmain_u120_do_add_details_line";
    IF ($ps_debug == "YES"){echo $sys_debug_text." line ".$ps_line."<BR>";};

    $ar_line_details = explode("|",$ps_line);
//*urlvalue|result field name|url parameter name
//* add the result_field_name to the details_def with the url_parameter_name from the url
    IF (strtoupper($ar_line_details[0]) == "ADD_DETAILS")
    {
        GOTO C100_NOIF;
    }
    IF (strtoupper($ar_line_details[0]) == "ADD_DETAILSIF")
    {
        GOTO C200_IF;
    }
    IF (strtoupper($ar_line_details[0]) == "ADD_DETAILSIFELSE")
    {
        GOTO C200_IF;
    }
    $sys_function_out = $ar_line_details[0]." IS NOT URLVALUE";
    GOTO C900_END;
C100_NOIF:
//echo "t<br> the add_details line is  in the correct format<br>";
//echo "t<br> ar_line_details[1] = ".$ar_line_details[1]."<br>";
//echo "t<br> ************************************<br>";
//echo "t<br> the add_details line is  in the correct format<br>";
//echo "t<br> ar_line_details[2] = ".$ar_line_details[2]."<br>";

    $ar_line_details[1] = STR_REPLACE("#P","|",$ar_line_details[1]);
    $ar_line_details[1] = STR_REPLACE("#p","|",$ar_line_details[1]);
//    $ar_line_details[2] = STR_REPLACE("#P","|&dopipe&|",$ar_line_details[2]);
//    $ar_line_details[2] = STR_REPLACE("#p","|&dopipe&|",$ar_line_details[2]);
    $ar_line_details[2] = STR_REPLACE("#P","|",$ar_line_details[2]);
    $ar_line_details[2] = STR_REPLACE("#p","|",$ar_line_details[2]);

    if (strpos($ar_line_details[2],"%!_")=== false)
    {}else
    {
        $ar_line_details[2] = $this->clmain_v200_load_line( $ar_line_details[2],$ps_details_def,$ps_details_data,"no",$ps_sessionno,"uclmain u120a ");
    }

    $ar_line_details[2] = STR_REPLACE("&dopipe&","|",$ar_line_details[2]);


    $sys_function_out = $ar_line_details[1]."|^%##%^|".$ar_line_details[2];

//    echo "<br>gw clmainu120 sys_function_out=[]".$sys_function_out."[]<BR><BR>";


    GOTO C900_END;
C200_IF:
    IF (strtoupper($ar_line_details[1]) <> "V1")
    {
        GOTO C210_do_V2;
    }

    $s_field_to_check = $ar_line_details[2];
    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];
    $s_field_value = $this->clmain_v300_set_variable($s_field_to_check,$ps_details_def,$ps_details_data,$ps_debug,$ps_sessionno,"clmain u120b  ");
    $s_true = $this->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","clmain u500 ");
    if ($s_true == "FALSE")
    {
        IF (strtoupper($ar_line_details[0]) == "ADD_DETAILSIFELSE")
        {
            GOTO C300_IF_ELSE;
        }
        IF ($ps_debug == "YES"){$this->z901_dump( $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);};
        $sys_function_out = $ar_line_details[5]."";
        GOTO C900_END;
    }
    $s_temp_value = $ps_line;
    GOTO C500_DO;
C210_do_V2:

    GOTO C500_DO;

C300_IF_ELSE:
//* add_detailsifelse|version|field|operator|value|fieldname|true result|false result|debug|end
    $sys_function_out = $this->clmain_u125_DO_ADD_DETAILS_IF($ar_line_details[5],$ar_line_details[7],"NO",$ps_details_def,$ps_details_data,$ps_sessionno);
    GOTO C900_END;
C500_DO:
    $sys_function_out = $this->clmain_u125_DO_ADD_DETAILS_IF($ar_line_details[5],$ar_line_details[6],"NO",$ps_details_def,$ps_details_data,$ps_sessionno);
    goto C900_END;
C900_END:
    return $sys_function_out;
 }
//##########################################################################################################################################################################
function clmain_u125_DO_ADD_DETAILS_IF($ps_def,$ps_data,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    $ar_check_array = array();
    $s_check_count = "";
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u125_DO_ADD_DETAILS_IF";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_deF=".$ps_def." ps_data=".$ps_data." ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};

A080_START:

        $ps_def = STR_REPLACE("#P","|",$ps_def);
        $ps_def = STR_REPLACE("#p","|",$ps_def);
        $ps_data = STR_REPLACE("#P","|",$ps_data);
        $ps_data = STR_REPLACE("#p","|",$ps_data);
        $ar_check_array = explode("|",$ps_def);
        $s_check_count = count($ar_check_array);
        $ar_check_array = explode("|",$ps_data);
        if ($s_check_count <> count($ar_check_array))
        {
            echo "Error in clmain_u125 the number of fields does not match - <br> the ADDDETAILS in your jcl needs to be corrected<br>doing DEF of ".$s_check_count." fields =<br>".$ps_def."<br>  DATA of ".count($ar_check_array)." fields =<br>".$ps_data."<br><br>";
            $sys_function_out = $ps_def."|^%##%^|"."Error in ut_jcl b120 the number of fields does not match";
        }else{
            $s_out = $ps_data;
            if (strpos($s_out,"%!_") === false)
            {}else{
//            echo "jcl b120 checking  ".$s_out."<br><br>";
                $s_out = STR_REPLACE("|","|^p^|",$s_out);
                $s_out = STR_REPLACE("%!_","|%!_",$s_out);
                $s_out = STR_REPLACE("_!%","_!%|",$s_out);
                $s_out = $this->clmain_v200_load_line( $s_out,$ps_details_def,$ps_details_data,$sys_debug,$ps_sessionno,"uclmain u125 ");
                $s_out = STR_REPLACE("^p^","|",$s_out);
//            echo "jcl b120 AFTER checking  ".$s_out."<br><br>";
            }

            $sys_function_out = $ps_def."|^%##%^|".$s_out;
        }


    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ";};
    IF ($sys_debug == "YES"){$sys_function_out.=" <!--".$sys_debug_text."-->";};


GOTO Z900_EXIT;

A1_DO_V2:

Z900_EXIT:
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_u200_tdxmanage_text_file($ps_type, $ps_filename, $ps_text)
{
    $status = "ALLGOOD";
    $root = $_SESSION['ko_root_path']."tdx_manage";
    $path =$root."/alerts/";

//        echo "<br>root".$root."<br>    path=".$path."  text=".$ps_text."<br>";

    if (strtoupper($ps_type) == "LOG")
    {
        $path =$root."/log/".date("Y")."/".date("m")."-".date("M")."/".date("d"."/");
    }
//GW20100808     $path = $path.date("Y")."/".date("m")."-".date("M")."/".date("d"."/");

    if (!is_dir($path))
    {
        if(!mkdir($path,0755,true))
        {
            die("DIE clmain_u200_tdxmanage_text_file Failed to create the fodler ".$path." trying to write ".$ps_text." to file ".$ps_filename);
        }
    }
B100_WRITE:
    $myfile = $path.$ps_filename;
    $fh = fopen($myfile,'a') or die("DIE clmain_u200_tdxmanage_text_file  cant open file".$myfile." trying to write text ".$ps_text);
    fwrite($fh,date("Y-m-d H:i:s",time())." - ".$ps_text."\r\n");
    fclose($fh);

Z900_EXIT:
    return $status;

}
//##########################################################################################################################################################################
function clmain_u300_set_rundata($ps_dbcnx,$ps_sql,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - Pf_b300_set_rundata";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." ps_sql = ".$ps_sql." ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ");};

    // define function specific variables and code
//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";};

//    $sys_function_out = "before globals";
//    return $sys_function_out;
//    EXIT;

//   global $class_sql;
//    global $class_main;

//
//    $class_main = new clmain();
    $class_sql = new wp_SqlClient();


    $s_sql = "";
    $result = "";
    $s_rec_found = "";
    $s_details_def = "";
    $s_details_data = "";

    $ar_dd = array();

    $s_sql = $ps_sql;
    $s_details_def = $ps_details_def ;
    $s_details_data = $ps_details_data ;

//    $s_sql = $class_main->clmain_v200_load_line( $s_sql,$s_details_def,$s_details_data,"no",$ps_sessionno, "ut_jcl b200");
    $s_sql = $this->clmain_v200_load_line( $s_sql,$s_details_def,$s_details_data,"no",$ps_sessionno, "ut_jcl b200");

    //    IF (strpos(strtoupper($s_sql),"|%!") > 0 )
//    {
//    }else{
//        //null
//    }
/*
//###############
    echo "GWDEBUG  clm u300 gw []".get_class($class_sql)."[]  []".get_class()."[]<br>";
    $classname = get_class($class_sql);

    if (preg_match('@\\\\([\w]+)$@', $classname, $matches)) {
        $classname = $matches[1];
    }

    echo  "GWDEBUG  clpj - getclass=[]".$classname."[]";

    $s_gw = print_r(get_declared_classes(),true);
    $s_gw = str_replace("[","<br>[",$s_gw);
    echo $s_gw;
    echo "GWDEBUG  <br>clpjl u300 gw []".get_class($class_sql)."[]  []".get_class()."[]<br>";
//    echo "GWDEBUG  <br>clpjl u300 gw  U300[]".get_class($class_sql_U300)."[]  []".get_class()."[]<br>";
//###############
*/

    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql." ");};
    $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_sql);

//    echo "<br>GWDEBUG   clpjl u300 gw AFTER SQL []".get_class($class_sql)."[]  []".get_class()."[]<br>";
    $s_rec_found = "NO";
    if ($result === false)
    {
//        echo "clmain u300 result = false<br>s_sql=".$s_sql;
        if (strpos(strtoupper(trim($s_sql)),strtoupper("(SKIPERRORFAIL)")) !== false)
        {
//           echo "clmain u300 skip error not  = false<br> ";
            $sys_function_out .= "ErrorMsg"."|^%##%^|"."skiperrorfail";
            goto Z900_EXIT;
        }
    }
C100_DO_RECS:
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
    {
        $s_rec_found = "YES";
        $s_details_def = $ps_details_def;
        $s_details_data = $ps_details_data;
        foreach( $row as $key=>$value)
        {

//            echo "<br> gw 300setrun<br>";
            if (strtoupper($key) == "DETAILS_DATA" )
            {
                if (strpos(strtoupper($value),"XML") !== false)
                {
                    GOTO C110_DO_XML_DATA_BLOB;
                }
            }
            if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
            {
                GOTO C120_SKIP_DATA_BLOB;
            }
            if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
            {
                GOTO C120_SKIP_DATA_BLOB;
            }

            if (strpos(strtoupper($value),"XML") === false)
            {
                GOTO C120_SKIP_DATA_BLOB;
            }
C110_DO_XML_DATA_BLOB:
            if (strpos(strtoupper($value),"XML.WRK") !== false)
            {
                GOTO C120_SKIP_DATA_BLOB;
            }

//            echo "<br> C110_DO_XML_DATA_BLOB value=[]".$value."[] strpos(strtoupper($value),XML.WRK)=[]".strpos(strtoupper($value),"XML.WRK")."[]";

            $ar_xml = simplexml_load_string($value);
//           var_dump($ar_xml);
            $newArry = array();
            $ar_xml = (array) $ar_xml;
            foreach ($ar_xml as $key => $value)
            {
                //$s_details_def .= "|REC_".mysqli_field_table($result,0)."_DB_".$key;
                $s_table_name = mysqli_fetch_field_direct($result,0)->table;
                $s_details_def .= "|REC_".$s_table_name."_DB_".$key;
                $s_details_data .= "|".$this->clmain_u596_xml_decode_char($value);
            }
            continue;

C120_SKIP_DATA_BLOB:

B651_SKIP_DATA_BLOB:
// if the first char is not { cant be json
            if (substr($value,0,2) <> '{"')
            {
                goto B652_SKIP_JSON;
            }
// if the last char is not } cant be json
            if ($value[strlen($value)-1] <> "}")
            {
                goto B652_SKIP_JSON;
            }
            if ($value[strlen($value)-2] <> "}")
            {
                goto B652_SKIP_JSON;
            }
B652_A_DO_JSON:
            $dictionary = json_decode($value, TRUE);
            /*    if ($dictionary === NULL)
                {
                        goto B652_SKIP_JSON;
                }
                echo $dictionary['fieldname'];
            */
//echo "<br> raw  value";
// print_r($value);

//echo "<br> json dictionary values";
            //print_r(array_keys($dictionary));

//    $dictionary =  clmain_u840_order_json($dictionary);

            $s_json_data_csv_data = "";
            $s_json_data_csv_headings = "";
            foreach ($dictionary as $key1 => $value1)
            {
                $s_fieldname = $key1;
                $s_fieldvalue = " ";
                if (is_array($value1))
                {
                    if (in_array('Value', $value1)) {
                        if (isset($value1['Value']))
                        {
                            $s_fieldvalue = $value1['Value'];
                            GOTO B652_C_MAKE_DEF_DATA;
                        }
//echo "<br> gw found value 1 keys";
//                 print_r(array_keys($value1));
//echo "<br> gw found value 1 values";
//                 print_r(array_values($value1));
                    }
//echo "<br> ";
//                print_r(" there is no value in the json for field s_fieldname=[]".$s_fieldname."[]  value1=[]".$value1."[]");
//echo "<br> value 1 keys";
//                 print_r(array_keys($value1));
//echo "<br> value 1 values";
//                 print_r(array_values($value1));
//echo "<br> array count  value1= []".count($value1)."[] ";
                    foreach ($value1 as $key2 => $value2)
                    {
                        IF (strtoupper($key2) <> "VALUE")
                        {
                            GOTO B652_B_SKIP_VALUE;
                        }
                        if (is_array($value2))
                        {
                            foreach ($value2 as $key3 => $value3)
                            {
                                $s_fieldname = $key1."_".$key3;
                                $s_fieldvalue = $value2[$key3];
                                //$s_details_def .= "|REC_".mysqli_field_table($result,0)."_JSON_".$key."_".str_replace(" ","_",trim($s_fieldname));
                                $s_table_name = mysqli_fetch_field_direct($result,0)->table;
                                $s_details_def .= "|REC_".$s_table_name."_JSON_".$key."_".str_replace(" ","_",trim($s_fieldname));
                                $s_details_data .= "|".trim($s_fieldvalue);
                                $s_json_data_csv_data = $s_json_data_csv_data.str_replace(","," ",$s_fieldvalue).",";
                                $s_json_data_csv_headings = $s_json_data_csv_headings.str_replace("_Value","",(str_replace(" ","_",trim($s_fieldname)))).",";
                            }
                            goto B652_X_NEXT_FIELD;
                        }else{
                            $s_fieldname = $key1."_".$key2;
                            $s_fieldvalue = $value1[$key2];
                        }
B652_B_SKIP_VALUE:
//echo "<br> s_fieldname = []".$s_fieldname."[] ";
//echo "<br> s_fieldvalue = []".$s_fieldvalue."[]";
                        B652_B_NEXT_FIELD:
                    }
                }else{
                    $s_fieldvalue = $value1;
                }
B652_C_MAKE_DEF_DATA:
                if (is_array($s_fieldvalue))
                {
                    $s_fieldvalue = implode("+",$s_fieldvalue);
                }
                //$s_details_def .= "|REC_".mysqli_field_table($result,0)."_JSON_".$key."_".str_replace(" ","_",trim($s_fieldname));
                $s_details_def .= "|REC_".$s_table_name."_JSON_".$key."_".str_replace(" ","_",trim($s_fieldname));
                $s_details_data .= "|".trim($s_fieldvalue);
                $s_json_data_csv_data = $s_json_data_csv_data.str_replace(","," ",$s_fieldvalue).",";
                $s_json_data_csv_headings = $s_json_data_csv_headings.str_replace("_Value","",(str_replace(" ","_",trim($s_fieldname)))).",";

B652_X_NEXT_FIELD:
            }
//echo "<br> json end def";
//                 print_r($s_details_def);
//echo "<br> json end data";
//                 print_r($s_details_data);
            //$s_details_def .= "|REC_".mysqli_field_table($result,0)."_JSON_csv_headings|REC_".mysqli_field_table($result,0)."_DB_csv_data";
            $s_table_name = mysqli_fetch_field_direct($result,0)->table;
            $s_details_def .= "|REC_".$s_table_name."_JSON_csv_headings|REC_".$s_table_name."_DB_csv_data";
            $s_details_data .= "|".trim($s_json_data_csv_headings)."|".trim($s_json_data_csv_data);

B652_SKIP_JSON:


            if (strpos(strtoupper($key),"DETAILS_D") === FALSE)
            {
//                $s_details_def .= "|REC_".my_sql_field_table($result,0)."_".$key;
                //$s_details_def .= "|REC_".mysqli_field_table($result,0)."_".$key;
                $s_table_name = mysqli_fetch_field_direct($result,0)->table;
                $s_details_def .= "|REC_".$s_table_name."_".$key;
                $s_details_data .= "|".$value;
                continue;
            }
            if (strtoupper($key) == "DETAILS_DEF" )
            {
                if (strtoupper($value) =="DATA_BLOB")
                {
                    continue;
                }
//               $s_details_def .= "|DD_".$key;
               $ar_dd = explode("|",$value);
               for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
               {
                    //$s_details_def .= "|REC_".mysqli_field_table($result,0)."_DD_".$ar_dd[$i2];
                    $s_table_name = mysqli_fetch_field_direct($result,0)->table;
                    $s_details_def .= "|REC_".$s_table_name."_DD_".$ar_dd[$i2];
                }
                continue;
            }
            if (strtoupper($key) == "DETAILS_DATA" )
            {
//               $s_details_data .= "|DD_".$value;
               $ar_dd = explode("|",$value);
               for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
               {
                    $s_details_data .= "|".$ar_dd[$i2];
                }
                continue;
            }
        }
    }


//230161114gw - check out this function for help    clmain_u820_fn_Z999_build_def_data

C900_END:
    if ($s_rec_found == "NO")
    {
      $sys_function_out .= "ErrorMsg"."|^%##%^|"."There is no data for ".$s_sql;
    }
    if ($s_rec_found == "YES")
    {
        $sys_function_out .= $s_details_def."|^%##%^|".$s_details_data;
    }

    $array_def = explode("|",$s_details_def);
    $array_data = explode("|",$s_details_data);

    if (count($array_def)<> count($array_data))
    {
        if ($sys_function_out == ""){
           $sys_function_out="E2008 - details def count does not match details data";
           $s_fieldname =  "ERROR_DATA_MISMATCH";
           $s_value_set = "AT_3";
           echo "clmain u300rundata  _ def/data field count error start of list  - def count=".count($array_def)." data count = ".count($array_data)."<br>";
           echo "s_fieldname =".$s_fieldname."  <br>";
           echo "s_details_def =".$s_details_def."  <br>";
           echo "s_details_data =".$s_details_data."  <br>";

           $s_count = count($array_def);
           if (count($array_data) > $s_count)
           {
               $s_count = count($array_data);
           }
           $ar_dd = explode("|",$s_details_def);
           $ar_ddata = explode("|",$s_details_data);
               for ( $i2 = 0; $i2 < $s_count; $i2++)
               {
                    echo "s_details_def ".$i2." =".$ar_dd[$i2]."=data =".$ar_ddata[$i2]."  <br>";
                }


           echo "clmain u300 _ def/data field count error end of list <br>";
        }
    }
    IF ($sys_debug == "YES"){z901_dump( $sys_function_name."  returned  = ".$sys_function_out." ");};

Z900_EXIT:
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_u310_set_rundata_delim2($ps_dbcnx,$ps_sql,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    //gw20101205 - same as u300 but the out field is fieldname :=: fieldvalue instead of details_def and detials_data
    // does not change the current details_def n details_data
    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u310_set_rundata_delim2";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." ps_sql = ".$ps_sql."   ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ");};

    // define function specific variables and code
//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";};

//    $sys_function_out = "before globals";
//    return $sys_function_out;
//    EXIT;

    global $class_sql;
    global $class_main;

    $s_sql = "";
    $result = "";
    $s_rec_found = "";
    $s_details_def = "";
    $s_details_data = "";

    $ar_dd = array();

    $s_sql = $ps_sql;

    $s_sql = $class_main->clmain_v200_load_line( $s_sql,$ps_details_def,$ps_details_data,"no",$ps_sessionno, "ut_jcl b200");

    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql." ");};
    $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_sql);
    $s_rec_found = "NO";
C100_DO_RECS:
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
    {
        $s_rec_found = "YES";
        foreach( $row as $key=>$value)
        {

//            echo "<br> gw 300setrun<br>";
            if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
            {
                GOTO C120_SKIP_DATA_BLOB;
            }
            if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
            {
                GOTO C120_SKIP_DATA_BLOB;
            }
            if (strpos(strtoupper($value),"XML") === false)
            {
                GOTO C120_SKIP_DATA_BLOB;
            }
            $ar_xml = simplexml_load_string($value);
//           var_dump($ar_xml);
            $newArry = array();
            $ar_xml = (array) $ar_xml;
            foreach ($ar_xml as $key => $value)
            {
                //$s_details_def .= "|REC_".mysqli_field_table($result,0)."_DB_".$key.":=:".$this->clmain_u596_xml_decode_char($value);
                $s_table_name = mysqli_fetch_field_direct($result,0)->table;
                $s_details_def .= "|REC_".$s_table_name."_DB_".$key.":=:".$this->clmain_u596_xml_decode_char($value);
            }
            continue;

C120_SKIP_DATA_BLOB:

B651_SKIP_DATA_BLOB:
// if the first char is not { cant be json
            if (substr($value,0,2) <> '{"')
            {
                goto B652_SKIP_JSON;
            }
// if the last char is not } cant be json
            if ($value[strlen($value)-1] <> "}")
            {
                goto B652_SKIP_JSON;
            }
            if ($value[strlen($value)-2] <> "}")
            {
                goto B652_SKIP_JSON;
            }
B652_A_DO_JSON:
            $dictionary = json_decode($value, TRUE);
            /*    if ($dictionary === NULL)
                {
                        goto B652_SKIP_JSON;
                }
                echo $dictionary['fieldname'];
            */
//echo "<br> raw  value";
// print_r($value);

//echo "<br> json dictionary values";
            //print_r(array_keys($dictionary));

//    $dictionary =  clmain_u840_order_json($dictionary);

            $s_json_data_csv_data = "";
            $s_json_data_csv_headings = "";
            foreach ($dictionary as $key1 => $value1)
            {
                $s_fieldname = $key1;
                $s_fieldvalue = " ";
                if (is_array($value1))
                {
                    if (in_array('Value', $value1)) {
                        if (isset($value1['Value']))
                        {
                            $s_fieldvalue = $value1['Value'];
                            GOTO B652_C_MAKE_DEF_DATA;
                        }
//echo "<br> gw found value 1 keys";
//                 print_r(array_keys($value1));
//echo "<br> gw found value 1 values";
//                 print_r(array_values($value1));
                    }
//echo "<br> ";
//                print_r(" there is no value in the json for field s_fieldname=[]".$s_fieldname."[]  value1=[]".$value1."[]");
//echo "<br> value 1 keys";
//                 print_r(array_keys($value1));
//echo "<br> value 1 values";
//                 print_r(array_values($value1));
//echo "<br> array count  value1= []".count($value1)."[] ";
                    foreach ($value1 as $key2 => $value2)
                    {
                        IF (strtoupper($key2) <> "VALUE")
                        {
                            GOTO B652_B_SKIP_VALUE;
                        }
                        if (is_array($value2))
                        {
                            foreach ($value2 as $key3 => $value3)
                            {
                                $s_fieldname = $key1."_".$key3;
                                $s_fieldvalue = $value2[$key3];
                                //$s_details_def .= "|REC_".mysqli_field_table($result,0)."_JSON_".$key."_".str_replace(" ","_",trim($s_fieldname));
                                $s_table_name = mysqli_fetch_field_direct($result,0)->table;
                                $s_details_def .= "|REC_".$s_table_name."_JSON_".$key."_".str_replace(" ","_",trim($s_fieldname));
                                $s_details_data .= "|".trim($s_fieldvalue);
                                $s_json_data_csv_data = $s_json_data_csv_data.str_replace(","," ",$s_fieldvalue).",";
                                $s_json_data_csv_headings = $s_json_data_csv_headings.str_replace("_Value","",(str_replace(" ","_",trim($s_fieldname)))).",";
                            }
                            goto B652_X_NEXT_FIELD;
                        }else{
                            $s_fieldname = $key1."_".$key2;
                            $s_fieldvalue = $value1[$key2];
                        }
                        B652_B_SKIP_VALUE:
//echo "<br> s_fieldname = []".$s_fieldname."[] ";
//echo "<br> s_fieldvalue = []".$s_fieldvalue."[]";
B652_B_NEXT_FIELD:
                    }
                }else{
                    $s_fieldvalue = $value1;
                }
                B652_C_MAKE_DEF_DATA:
                if (is_array($s_fieldvalue))
                {
                    $s_fieldvalue = implode("+",$s_fieldvalue);
                }
                //$s_details_def .= "|REC_".mysqli_field_table($result,0)."_JSON_".$key."_".str_replace(" ","_",trim($s_fieldname));
                $s_table_name = mysqli_fetch_field_direct($result,0)->table;
                $s_details_def .= "|REC_".$s_table_name."_JSON_".$key."_".str_replace(" ","_",trim($s_fieldname));
                $s_details_data .= "|".trim($s_fieldvalue);
                $s_json_data_csv_data = $s_json_data_csv_data.str_replace(","," ",$s_fieldvalue).",";
                $s_json_data_csv_headings = $s_json_data_csv_headings.str_replace("_Value","",(str_replace(" ","_",trim($s_fieldname)))).",";

B652_X_NEXT_FIELD:
            }
//echo "<br> json end def";
//                 print_r($s_details_def);
//echo "<br> json end data";
//                 print_r($s_details_data);
            //$s_details_def .= "|REC_".mysqli_field_table($result,0)."_JSON_csv_headings|REC_".mysqli_field_table($result,0)."_DB_csv_data";
            $s_table_name = mysqli_fetch_field_direct($result,0)->table;
            $s_details_def .= "|REC_".$s_table_name."_JSON_csv_headings|REC_".$s_table_name."_DB_csv_data";
            $s_details_data .= "|".trim($s_json_data_csv_headings)."|".trim($s_json_data_csv_data);

B652_SKIP_JSON:



            if (strpos(strtoupper($key),"DETAILS_D") === FALSE)
            {
//                $s_details_def .= "|REC_".my_sql_field_table($result,0)."_".$key;
                //$s_details_def .= "|REC_".mysqli_field_table($result,0)."_".$key.":=:".$value;
                $s_table_name = mysqli_fetch_field_direct($result,0)->table;
                $s_details_def .= "|REC_".$s_table_name."_".$key.":=:".$value;
                continue;
            }
            if (strtoupper($key) == "DETAILS_DEF" )
            {
                $ar_dd = explode("|",$value);
                continue;
            }
            if (strtoupper($key) == "DETAILS_DATA" )
            {
//               $s_details_data .= "|DD_".$value;
               $ar_dda = explode("|",$value);
               for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
               {
                    //$s_details_def .= "|REC_".mysqli_field_table($result,0)."_DD_".$ar_dd[$i2].":=:".$ar_dda[$i2];
                    $s_table_name = mysqli_fetch_field_direct($result,0)->table;
                    $s_details_def .= "|REC_".$s_table_name."_DD_".$ar_dd[$i2].":=:".$ar_dda[$i2];
                }
                continue;
            }
        }
    }
C900_END:
    if ($s_rec_found == "NO")
    {
        $sys_function_out .= "ErrorMsg"."|^%##%^|"."There is no data for ".$s_sql;
    }
    if ($s_rec_found == "YES")
    {
        $sys_function_out .= $s_details_def."|^%##%^|".$s_details_data;
    }

    $array_def = explode("|",$s_details_def);
    $array_data = explode("|",$s_details_data);

    if (count($array_def)<> count($array_data))
    {
        if ($sys_function_out == ""){
           $sys_function_out="E2008 - details def count does not match details data";
           $s_fieldname =  "ERROR_DATA_MISMATCH";
           $s_value_set = "AT_3";
           echo "clmain u300rundata  _ def/data field count error start of list  - def count=".count($array_def)." data count = ".count($array_data)."<br>";
           echo "s_fieldname =".$s_fieldname."  <br>";
           echo "s_details_def =".$s_details_def."  <br>";
           echo "s_details_data =".$s_details_data."  <br>";

           $s_count = count($array_def);
           if (count($array_data) > $s_count)
           {
               $s_count = count($array_data);
           }
           $ar_dd = explode("|",$s_details_def);
           $ar_ddata = explode("|",$s_details_data);
               for ( $i2 = 0; $i2 < $s_count; $i2++)
               {
                    echo "s_details_def ".$i2." =".$ar_dd[$i2]."=data =".$ar_ddata[$i2]."  <br>";
                }


           echo "clmain u300 _ def/data field count error end of list <br>";
        }
    }
    IF ($sys_debug == "YES"){z901_dump( $sys_function_name."  returned  = ".$sys_function_out." ");};

Z900_EXIT:
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_u500_do_url_line($ps_dbcnx,$ps_line,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_url_siteparams) {
    $sys_debug_text = "clmain_u500_do_url_line";
    IF ($ps_debug == "YES"){echo "<BR>"."<BR>".$sys_debug_text." line ".$ps_line."<BR>";};

    $ar_line_details = explode("|",$ps_line);
//*urlvalue|v1|result field name|url parameter name
//* add the result_field_name to the details_def with the url_parameter_name from the url
    IF (strtoupper($ar_line_details[0]) == "URLVALUE")
    {
        GOTO C100_NOIF;
    }
    IF (strtoupper($ar_line_details[0]) == "URLVALUEIF")
    {
        GOTO C200_IF;
    }
    $sys_function_out = $ar_line_details[0]." IS NOT URLVALUE";
    GOTO C900_END;
C100_NOIF:
    $s_temp_value = $ps_line;
    GOTO C500_DO;
C200_IF:
    IF (strtoupper($ar_line_details[1]) <> "V1")
    {
        GOTO C210_do_V2;
    }
    $s_field_to_check = $ar_line_details[2];
    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];
    $s_field_value = $this->clmain_v300_set_variable($s_field_to_check,$ps_details_def,$ps_details_data,$ps_debug,$ps_sessionno,"clmain u500  ");
    $s_true = $this->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","clmain u500 ");
    if ($s_true == "FALSE")
    {
        IF ($ps_debug == "YES"){echo $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value."<BR>";};
        IF ($ps_debug == "YES"){$this->z901_dump( $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);};
        $sys_function_out = $ar_line_details[5]."";
        GOTO C900_END;
    }
    IF ($ps_debug == "YES"){echo $sys_debug_text." compare true  ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value."<BR>";};
    $s_temp_value = $ar_line_details[0]."|".$ar_line_details[1]."|".$ar_line_details[5]."|".$ar_line_details[6]."|".$ar_line_details[7];
    IF ($ps_debug == "YES"){echo $sys_debug_text." temp value = to pass eo u510  ".$s_temp_value."<BR>";};
    GOTO C500_DO;
C210_do_V2:
C500_DO:
    $sys_function_out = $this->clmain_u510_do_url_value($ps_dbcnx,$s_temp_value,"NO",$ps_details_def,$ps_details_data,$ps_sessionno,$ps_url_siteparams);
    goto C900_END;
C900_END:
    return $sys_function_out;
}
//##########################################################################################################################################################################
function clmain_u510_do_url_value($ps_dbcnx,$ps_line,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_url_siteparams)
{
    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u500_do_url_value";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){echo  $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING";};
    IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ";};

//    echo "start doing urlvalue<br>".$sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ";

    // define function specific variables and code
//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";};

//    $sys_function_out = "before globals";
//    return $sys_function_out;
//    EXIT;

    global $class_sql;
    global $class_main;
A001_DEFINE_VARS:
    $ar_line_details = array();
    $s_field_name = "";
    $s_param_name = "";
    $sys_function_out = "";
    $ar_url_siteparams = array();
    $ar_url_siteparams_vars = array();

A100_INIT_VARS:
    $ar_line_details = explode("|",$ps_line);
    $s_field_name = $ar_line_details[2];
    $s_param_name = $ar_line_details[3];
    $sys_function_out = "?u510_".$ar_line_details[2];
    $ar_url_siteparams = explode("^",$ps_url_siteparams);

    if (strtoupper($ar_line_details[1]) != "V1")
    {
        goto A1_DO_V2;
    }
B001_DO_V1:
    $i = 0;

B100_ENTRY:
    IF ($i >= count($ar_url_siteparams))
    {
        goto B900_END;
    }
//    echo "doing param loop param value =".$ar_url_siteparams[$i]."<br>";

    if (strpos($ar_url_siteparams[$i],"=") === false)
    {
//        echo "doing param loop param no equals sign<br>";
        goto B200_GET_NEXT;
    }
    $ar_url_siteparams_vars = explode("=",$ar_url_siteparams[$i]);
//    echo "doing param loop check value in =".$ar_url_siteparams[$i]." explode url param 0=".strtoupper(trim($ar_url_siteparams_vars[0]," "))." 1 = ".strtoupper(trim($ar_url_siteparams_vars[1]))."<br>";
//    echo "doing check values 0=".(strtoupper(trim($ar_url_siteparams_vars[0]," "))."  param =".strtoupper(trim($s_param_name," ")))."<br>";
    $s_value_chk =strtoupper(trim($ar_url_siteparams_vars[0]," "));
    $s_param_name_chk = strtoupper(trim($s_param_name," "));
//    echo "doing check values2 0=*".$s_value_chk."*  param =*".$s_param_name_chk."*<br>";
    if ($s_value_chk == $s_param_name_chk)
    {
//        echo "compare worked param 0=".strtoupper(trim($ar_url_siteparams_vars[0]," "))." 1 = ".strtoupper(trim($ar_url_siteparams_vars[1]))."<br>";
        $sys_function_out = $ar_url_siteparams_vars[1];
        goto Z900_EXIT;
    }

B200_GET_NEXT:
    $i = $i + 1;
    goto B100_ENTRY;

B900_END:
//    echo "doing urlvalue<br>s_field_name=".$s_field_name."<br>s_param_name=".$s_param_name."<br>sys_function_out=".$sys_function_out."<br>";
A1_DO_V2:

Z900_EXIT:
    $sys_function_out = $s_field_name."|^%##%^|".$sys_function_out;

    IF ($sys_debug == "YES"){echo  $sys_function_name."  returned  = ".$sys_function_out." ";};

    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_u520_set_date_sql($ps_fieldname,$ps_date_type,$ps_debug, $ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u520_set_date_sql";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." ps_fieldname=".$ps_fieldname." ps_date_type".$ps_date_type." ps_debug".$ps_debug." ps_calledfrom=". $ps_calledfrom);};

//    echo "start doing urlvalue<br>".$sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ";

A001_DEFINE_VARS:
    $ar_line_details = array();
    $s_field_name = "";
    $sys_function_out = "";

A100_INIT_VARS:
    $s_field_name = $ps_fieldname;
    $sys_function_out = "?u520_".$ps_fieldname;

B001_DO:

    $s_date_now = date("Y-m-d");
    $t_date_from = strtotime($s_date_now);
    $t_end_of_today = strtotime($s_date_now." 24:00");
    $t_date_to = $t_end_of_today;

    if (strtoupper($ps_date_type) == "YESTERDAY")
    {
        $t_date_from = strtotime("-1 day");
        $t_date_to = strtotime("today");
    }
    if (strtoupper($ps_date_type) == "WEEK")
    {
        $t_date_from = strtotime("last sunday");
        $t_date_to = strtotime("tomorrow");
    }
    if (strtoupper($ps_date_type) == "MONTH")
    {
        $t_date_from = strtotime("last day last month");
        $t_date_to = strtotime("tomorrow");
    }
    if (strtoupper($ps_date_type) == "7DAYS")
    {
        $t_date_from = strtotime("-8 day");
        $t_date_to = strtotime("tomorrow");
    }
    if (strtoupper($ps_date_type) == "8DAYS")
    {
        $t_date_from = strtotime("-9 day");
        $t_date_to = strtotime("tomorrow");
    }
    if (strtoupper($ps_date_type) == "14DAYS")
    {
        $t_date_from = strtotime("-15 day");
        $t_date_to = strtotime("tomorrow");
    }
    if (strtoupper($ps_date_type) == "30DAYS")
    {
        $t_date_from = strtotime("-31 day");
        $t_date_to = strtotime("tomorrow");
    }
    if (strtoupper($ps_date_type) == "60DAYS")
    {
        $t_date_from = strtotime("-61 day");
        $t_date_to = strtotime("tomorrow");
    }
    if (strtoupper($ps_date_type) == "90DAYS")
    {
        $t_date_from = strtotime("-91 day");
        $t_date_to = strtotime("tomorrow");
    }
    if (strpos(strtoupper($ps_date_type)," TO ") > 0 )
    {
        $t_date_from = strtotime($_SESSION[$ps_sessionno.'udp_filterdate_from']);
        $t_date_to = strtotime($_SESSION[$ps_sessionno.'udp_filterdate_to']." 24:00");
    }

//gw20140902
    if (strtoupper($ps_date_type) == "CYMDTODAY")
    {
        $t_date_from = date("Ymd",strtotime("yesterday"));
        $t_date_to = date("Ymd",strtotime("tomorrow"));
    }
    if (strtoupper($ps_date_type) == "CYMDFUTURE")
    {
        $t_date_from = date("Ymd",strtotime("TOMORROW"));
        $t_date_to = date("Ymd",strtotime("+10 YEAR"));
    }
    if (strtoupper($ps_date_type) == "CYMDTOMORROW")
    {
        $t_date_from = date("Ymd",strtotime("TODAY"));
        $t_date_to = date("Ymd",strtotime("+2 day"));
    }
    if (strtoupper($ps_date_type) == "CYMDYESTERDAY")
    {
        $t_date_from = date("Ymd",strtotime("-2 day"));
        $t_date_to = date("Ymd",strtotime("TODAY"));
    }
    if (strtoupper($ps_date_type) == "CYMD2DAYS")
    {
        $t_date_from = date("Ymd",strtotime("-3 day"));
        $t_date_to = date("Ymd",strtotime("YESTERDAY"));
    }
    if (strtoupper($ps_date_type) == "CYMD3DAYS")
    {
        $t_date_from = date("Ymd",strtotime("-4 day"));
        $t_date_to = date("Ymd",strtotime("-2 DAYS"));
    }
    if (strtoupper($ps_date_type) == "CYMD7TO4DAYS")
    {
        $t_date_from = date("Ymd",strtotime("-8 day"));
        $t_date_to = date("Ymd",strtotime("-3 DAYS"));
    }
    if (strtoupper($ps_date_type) == "CYMD30TO7DAYS")
    {
        $t_date_from = date("Ymd",strtotime("-30 DAYS"));
        $t_date_to = date("Ymd",strtotime("-7 DAYS"));
    }
    if (strtoupper($ps_date_type) == "CYMD30TO8DAYS")
    {
        $t_date_from = date("Ymd",strtotime("-30 DAYS"));
        $t_date_to = date("Ymd",strtotime("-8 DAYS"));
    }
    if (strtoupper($ps_date_type) == "CYMD7PLUSDAYS")
    {
        $t_date_from = date("Ymd",strtotime("-10 years"));
        $t_date_to = date("Ymd",strtotime("-7 DAYS"));
    }
    if (strtoupper($ps_date_type) == "CYMD30EACHWAY")
    {
        $t_date_from = date("Ymd",strtotime("-30 DAYS"));
        $t_date_to = date("Ymd",strtotime("+30 DAYS"));
    }
    if (strtoupper($ps_date_type) == "CYMD7DAYS")
    {
        $t_date_from = date("Ymd",strtotime("-7 DAYS"));
        $t_date_to = date("Ymd",strtotime("tomorrow"));
    }
    if (strtoupper($ps_date_type) == "CYMD8DAYS")
    {
        $t_date_from = date("Ymd",strtotime("-8 DAYS"));
        $t_date_to = date("Ymd",strtotime("tomorrow"));
    }
    if (strtoupper($ps_date_type) == "CYMD14DAYS")
    {
        $t_date_from = date("Ymd",strtotime("-14 DAYS"));
        $t_date_to = date("Ymd",strtotime("tomorrow"));
    }
    if (strtoupper($ps_date_type) == "CYMD30DAYS")
    {
        $t_date_from = date("Ymd",strtotime("-30 DAYS"));
        $t_date_to = date("Ymd",strtotime("tomorrow"));
    }
    if (strtoupper($ps_date_type) == "CYMD60DAYS")
    {
        $t_date_from = date("Ymd",strtotime("-60 DAYS"));
        $t_date_to = date("Ymd",strtotime("tomorrow"));
    }
    if (strtoupper($ps_date_type) == "CYMD90DAYS")
    {
        $t_date_from = date("Ymd",strtotime("-90 DAYS"));
        $t_date_to = date("Ymd",strtotime("tomorrow"));
    }
    if (strpos(strtoupper($ps_date_type)," TO ") > 0 )
    {
        if (strpos(strtoupper($ps_date_type),"CYMD") !== FALSE )
        {
            $t_date_from = date("Ymd",strtotime($_SESSION[$ps_sessionno.'udp_filterdate_from']));
            $t_date_to = date("Ymd",strtotime($_SESSION[$ps_sessionno.'udp_filterdate_to']." 24:00"));
        }
    }


//echo strtotime("now"), "\n";
//echo strtotime("10 September 2000"), "\n";
//echo strtotime("+1 day"), "\n";
//echo strtotime("+1 week"), "\n";
//echo strtotime("+1 week 2 days 4 hours 2 seconds"), "\n";
//echo strtotime("next Thursday"), "\n";
//echo strtotime("last Monday"), "\n";
//

B900_END:
//    echo "doing urlvalue<br>s_field_name=".$s_field_name."<br>s_param_name=".$s_param_name."<br>sys_function_out=".$sys_function_out."<br>";
A1_DO_V2:


Z900_EXIT:
    $sys_function_out = '('.$s_field_name.' > '.$t_date_from.' AND '.$s_field_name.' < '.$t_date_to.')';//. $s_date_now.strtotime("today") ;

    IF ($sys_debug == "YES"){z901_dump( $sys_function_name."  returned  = ".$sys_function_out." ");};

 //   echo "<br> u520 sys_function_out = ".$sys_function_out;

    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_u530_lat_long_dist_or_speed($ps_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_url_siteparams,$lat1, $lon1, $lat2, $lon2, $unit, $ps_lon1_utime,$ps_lon2_utime,$ps_return)
{

    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u530_lat_long_distance";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." lat1=".$lat1." lon1=".$lon1." lat2=".$lat2." lon2=".$lon2." unit=".$unit." ps_lon1_utime=".$ps_lon1_utime." ps_lon2_utime=".$ps_lon2_utime." ps_return=".$ps_return);};

//    echo "start doing urlvalue<br>".$sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ";

    // define function specific variables and code
//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";};

//    $sys_function_out = "before globals";
//    return $sys_function_out;
//    EXIT;



    global $class_sql;
    global $class_main;
A001_DEFINE_VARS:
$result = "";

A100_INIT_VARS:
$result = "0";

B100_DISTANCE:
  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
    $result = ($miles * 1.609344);
  } else if ($unit == "N") {
      $result =  ($miles * 0.8684);
    } else {
        $result =  $miles;
      }

    if (strtoupper($ps_return) <> "SPEED")
    {
        goto Z900_EXIT;
    }
B200_SPEED:
//calc time difference between $ps_lon1_utime,$ps_lon2_utime
    $secs = abs($ps_lon1_utime - $ps_lon2_utime);
    $result = $result / $secs; // per second
    $result = $result * 60 * 60; // per hour
Z900_EXIT:
    $sys_function_out = $result;

    IF ($sys_debug == "YES"){z901_dump( $sys_function_name."  returned  = ".$sys_function_out." ");};

    return $sys_function_out;
}
//##########################################################################################################################################################################
function clmain_activity_log($ps_dbcnx,$ps_log_text, $ps_userid, $ps_runfrom)
{
    $s_log_text="";
    $s_creatednt="";
    $s_uk="";
    $s_userid = "";
    $s_runfrom = "";

//    echo "doing the activity log";
    $s_log_text=$ps_log_text;
    $s_creatednt=date('YmdHisu');
    $s_uk = $s_creatednt;
    $s_userid = $ps_userid;
    $s_runfrom = $ps_runfrom;

    $ssql = "INSERT into activity_log SET uniquekey = '{$s_uk}', creatednt = '{$s_creatednt}', log_text = '{$s_log_text}', userid = '{$s_userid}', runat  = '{$s_userid}' ";
    $rs_temp = mysqli_query($ps_dbcnx,$ssql);
    if (!$rs_temp)
    {
        echo("<P>Error performing query: ".mysqli_error()." sql = ".$ssql."</P>");
        exit();
    }
    return $s_uk;
}
//##########################################################################################################################################################################
function clmain_u540_utl_activity_log($ps_dbcnx,$ps_rectype,$ps_companyid,$ps_userid,$ps_summary , $ps_data_blob, $ps_key1, $ps_key1def, $ps_key2, $ps_key2def, $ps_key3, $ps_key3def, $ps_key4, $ps_key4def, $ps_key5, $ps_key5def)
{
    $s_log_text="";
    $s_creatednt="";
    $s_uk="";
    $s_userid = "";
    $s_runfrom = "";
    $s_rectype =$ps_rectype;
    $s_companyid = $ps_companyid;
//    echo "doing the activity log";
    $s_creatednt=date("YmdHis");
    $s_userid = $ps_userid;
    $s_createdutime=time();

    $ssql = "INSERT into utl_activity SET createddnt = '".$s_creatednt."',createdutime = '".$s_createdutime."', createduserid = '".$s_userid."', rectype ='".$s_rectype."' , summary = '".substr($ps_summary,0,100)."' , companyid = '".substr($s_companyid,0,50)."', data_blob  = '".$ps_data_blob."',key1 = '".substr($ps_key1,0,50)."',key1_def = '".substr($ps_key1def,0,50)."', key2 = '".substr($ps_key2,0,50)."', key2_def = '".substr($ps_key2def,0,50)."', key3 = '".substr($ps_key3,0,50)."', key3_def = '".substr($ps_key3def,0,50)."', key4 = '".substr($ps_key4,0,50)."', key4_def = '".substr($ps_key4def,0,50)."', key5 = '".substr($ps_key5,0,50)."', key5_def = '".substr($ps_key5def,0,50)."'";

    if (!strpos($s_rectype,"do_debug") === false)
    {
        die ("clmain_u540 rectype set to do_debug s_rectype=[]".$s_rectype."[] and  ssql =[]".$ssql."[]");
    }

    $rs_temp = mysqli_query($ps_dbcnx,$ssql);
    if (!$rs_temp)
    {
        echo("<P>clmanin u540 - Error performing query: ".mysqli_error($ps_dbcnx)." sql = ".$ssql."</P>");
        exit();
    }
    return $s_uk;
}
//##########################################################################################################################################################################
// duplicate the rocord key or group + type given or sql
function clmain_u545_duplicate_record($ps_dbcnx,$ps_table,$ps_key_field,$ps_keyvalue, $ps_sql)
{
/*
    $table = $ps_table;
    $id = $ps_keyvalue;
    $id_field = $ps_key_field;
//function DuplicateMySQLRecord ($table, $id_field, $id) {
    // load the original record into an array
    $s_sql = "SELECT * FROM {$table} WHERE {$id_field}={$id}";
    if (strtoupper($ps_keyvalue) = "USESQL")
    {
        $s_sql = $ps_sql;
    }
    $result = mysqli_query($ps_dbcnx,$s_sql);
    $original_record = mysqli_fetch_assoc($result);

    // insert the new record and get the new auto_increment id
    mysqli_query("INSERT INTO {$table} (`{$id_field}`) VALUES (NULL)");
    $newid = mysqli_insert_id();

    // generate the query to update the new record with the previous values
    $query = "UPDATE {$table} SET ";
    foreach ($original_record as $key => $value) {
        if ($key != $id_field) {
            $query .= '`'.$key.'` = "'.str_replace('"','\"',$value).'", ';
        }
    }
    $query = substr($query,0,strlen($query)-2); # lop off the extra trailing comma
    $query .= " WHERE {$id_field}={$newid}";
    mysqli_query($query);

    // return the new id
    return $newid;
//}
*/
}
//##########################################################################################################################################################################
function clmain_u550_utl_sys_file_format($ps_dbcnx,$ps_filename, $ps_debug, $ps_calledfrom)
{
    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u550_utl_sys_file_format - called from - ".$ps_calledfrom."  ";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."  DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name."  ps_debug = ".$ps_debug);};

A100_INIT:
    $s_filefmat = "Unknown";
    $s_fmat_status = "notchecked";
    $s_load_class = "classnotset";
    $s_run_function = "functionnotset";

    $s_filename = $ps_filename;

B100_CHECK_IF_TXTTOPDF:
    IF (strpos(strtoupper($s_filename),"TXTTOPDF") === false)
    {}else{
        $s_filefmat = "TXT_TO_PDF";
    }

// check file name to see if complies
// check file content to see if complies
// if can id and is good fmat return
// TXTTOPDF
// if can id and bad fmat return
// TXTTOPDF*FMATERROR

Z900_EXIT:
       return $s_filefmat."|".$s_fmat_status."|".$s_load_class."|".$s_run_function;
}
//##########################################################################################################################################################################
function clmain_u560_di_create_if_none_coheader($ps_companyid,$ps_debug,$ps_dbcnx,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_outdata = "none";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u560_di_create_if_none_coheader - called from - ".$ps_calledfrom."  ";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."  DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name."  ps_debug = ".$ps_debug);};

A100_INIT:
    global $class_sql;
// get the utl_data_integration record for companyid with rectype coheader
// if not exsist - createit
B100_CHECK_REC:
    $ssql = "SELECT * from  utl_data_integration where companyid = '".$ps_companyid."'";
    $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$ssql);
    $s_rec_found = "NO";
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
    {
        $s_rec_found = "YES";
    }
    if ($s_rec_found == "NO")
    {
        goto C100_MAKE_REC;
    }
    GOTO Z900_EXIT;

C100_MAKE_REC:
    $s_createddnt  = date('YmdHisu');
    $s_createduserid =  "d";
    $s_updateddnt  = date('YmdHisu');
    $s_updateduserid =  "never";
    $s_rectype = "COHEADER";
    $s_companyid = $ps_companyid;
    $s_data_blob = "new";
    $s_key1 = "NEW";
    $s_key1def = "STATUS";
    $s_key2 = TIME();
    $s_key2def = "NOTUSED";

    $s_integrationid = "";
    $charset = "abcdefghijklmnopqrstuvwxyz";
    $charset = $charset."ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $charset = $charset."0123456789";
    for ($i=0; $i<11; $i++) $s_integrationid = $s_integrationid.$charset[(mt_rand(0,(strlen($charset)-1)))];

    $s_password = "syspwd";
    $s_alertemailaddress = "noaddress@domain.unk";
    $s_inboundauto ="NO";
    $s_outboundftp = "NO";
C200_SET_BLOB:
    $s_data_blob = "";
    $s_data_blob = $s_data_blob.$this->clmain_u570_build_xml("UT_FILESTART","data_blob","NO","u560-1");
    $s_data_blob = $s_data_blob.$this->clmain_u570_build_xml("ut_start","data_blob","NO","u560-2");

    $s_data_blob = $s_data_blob.$this->clmain_u570_build_xml("integrationid",$s_integrationid,"NO","u560-4c");
    $s_data_blob = $s_data_blob.$this->clmain_u570_build_xml("password",$s_password,"NO","u560-4c");
    $s_data_blob = $s_data_blob.$this->clmain_u570_build_xml("DAILYEXPORT_HHMM","18:30","NO","u560-3");
    $s_data_blob = $s_data_blob.$this->clmain_u570_build_xml("weekYExport_DOW","FRI","NO","u560-4");
    $s_data_blob = $s_data_blob.$this->clmain_u570_build_xml("weekYExport_HHMM","20:30","NO","u560-4");
    $s_data_blob = $s_data_blob.$this->clmain_u570_build_xml("MonthlyExport_DD","01","NO","u560-4b");
    $s_data_blob = $s_data_blob.$this->clmain_u570_build_xml("MonthlyExport_HHMM","03:30","NO","u560-4c");
    $s_data_blob = $s_data_blob.$this->clmain_u570_build_xml("alertemailaddress",$s_alertemailaddress,"NO","u560-4c");
    $s_data_blob = $s_data_blob.$this->clmain_u570_build_xml("inboundauto",$s_inboundauto,"NO","u560-4c");
    $s_data_blob = $s_data_blob.$this->clmain_u570_build_xml("outboundftp",$s_outboundftp,"NO","u560-4c");

    $s_data_blob = $s_data_blob.$this->clmain_u570_build_xml("ut_end","data_blob","NO","u560-5");

    $s_data_blob = $this->clmain_u580_insert_field_xml($s_data_blob,"data_blob","outboundftp_server","notset_srvr","NO","u560-5");
    $s_data_blob = $this->clmain_u580_insert_field_xml($s_data_blob,"data_blob","outboundftp_uname","notset_un","NO","u560-5");
    $s_data_blob = $this->clmain_u580_insert_field_xml($s_data_blob,"data_blob","outboundftp_pwd","notset_pwd","NO","u560-5");
    $s_data_blob = $this->clmain_u580_insert_field_xml($s_data_blob,"data_blob","outboundftp_folder","notset_fold","NO","u560-5");

C300_WRITE:
    $ssql = "INSERT into utl_data_integration SET ";
    $ssql = $ssql." createddnt = '".$s_createddnt."', createdutime = '".time()."', createduserid = '".$s_createduserid."',";
    $ssql = $ssql." updateddnt = '".$s_updateddnt."', updatedutime = '".time()."', updateduserid = '".$s_updateduserid."',";
    $ssql = $ssql." rectype ='".$s_rectype."', companyid = '".$s_companyid."', data_blob  = '".$s_data_blob."',";
    $ssql = $ssql." key1 = '".$s_key1."',key1_def = '".$s_key1def."', key2 = '".$s_key2."', key2_def = '".$s_key2def."'";

    $rs_temp = mysqli_query($ps_dbcnx,$ssql);
    if (!$rs_temp)
    {
        echo("<P>Error performing query: ".mysql_error()." sql = ".$ssql."</P>");
        exit();
    }
    $tmp = $this->clmain_u540_utl_activity_log($ps_dbcnx,"DI_MAKE_COHDR",$s_companyid,"System","Company record created","The company data integration record has been created the firts time the user accessed the intergration page","di_admin_page","Process","k2 value","k2 def","k3 value","k3 def","k4 value","k4 def","k5 value","k5 def");
C900_END:

Z900_EXIT:
       return $sys_outdata;
}

//##########################################################################################################################################################################
function clmain_u570_build_xml($ps_field,$ps_value, $ps_debug, $ps_calledfrom)
{
/*    $s_data_blob = this->clmain_u570_build_xml("ut_start","data_blob");
    $s_data_blob = this->clmain_u570_build_xml("password","pwd");
    $s_data_blob = this->clmain_u570_build_xml("gwtest","txt valued");
    $s_data_blob = this->clmain_u570_build_xml("ut_end","data_blob");
*/
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_outdata = "none";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u570_build_xml - called from - ".$ps_calledfrom."  ";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."  DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name."  ps_debug = ".$ps_debug);};

A100_INIT:
    if (strpos(strtoupper($ps_field),"UT_FILESTART") === false )
    {}else
    {
/*        $sys_outdata = "<?xml version='1.0' encoding='ISO-8859-1'?>";
*/
        $sys_outdata = '<?xml version="1.0" encoding="ISO-8859-1"?>';
        GOTO A900_END;
    }
    if (strpos(strtoupper($ps_field),"UT_START") === false )
    {}else
    {
        $sys_outdata = "<".strtolower($ps_value).">";
        GOTO A900_END;
    }
    if (strpos(strtoupper($ps_field),"UT_END") === false )
    {}else
    {
           $sys_outdata = "</".strtolower($ps_value).">";
           GOTO A900_END;
    }
    $sys_outdata = "<".strtolower($ps_field).">".$this->clmain_u595_xml_encode_invalid_char($ps_value)."</".strtolower($ps_field).">";
A900_END:

Z900_EXIT:
       return $sys_outdata;
}
//##########################################################################################################################################################################
function clmain_u575_make_xml($ps_line_in, $ps_debug, $ps_details_def,$ps_details_data,$ps_sessionno)
{
//MAKE_XMLFIELD|DATA_BLOB|whofor="",addrline1="",addrlin2="",suburb="",postcode="",cross_street="",locn_note="",phoneno="",mobile="",email="",sms_alert="",machine_make="",machine_model="",machine_type="",job_overview="",job_description=""}|END

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_outdata = "none";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u575_make_xml - called from - ".$ps_debug."  ";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."  called ".$sys_function_name."  DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."=".$sys_function_name."  ps_debug = ".$ps_debug);};

A100_INIT:

   $ar_linefields = explode("|",$ps_line_in);
   $s_field_name  = $ar_linefields[1];
   $s_field_list = $ar_linefields[2];

    $s_data_blob  = "";
    $s_data_blob = $s_data_blob.$this->clmain_u570_build_xml("UT_FILESTART","data_blob","NO","u575-init");
    $s_data_blob = $s_data_blob.$this->clmain_u570_build_xml("ut_start","data_blob","NO","u575-start");
//for each field in the fild_list
// explode around = and build xml
   $ar_fields = explode(",",$s_field_list);
   for ( $i2 = 0; $i2 < count($ar_fields); $i2++)
   {
       $ar_field_n_value = explode("=",$ar_fields[$i2]);
       $s_field_value = str_replace('"',"",$ar_field_n_value[1]);

//echo "<br> clmain u575 field ar_field_n_value=".$ar_field_n_value." value after = ".$s_field_value."<br>";
       if (strpos($s_field_value,"%!_") === false)
       {}else{
           $s_field_value = str_replace("#p","|",$s_field_value);
           $s_field_value = str_replace("#P","|",$s_field_value);
//           $s_field_value = $this->clmain_v300_set_variable($s_field_value,$ps_details_def,$ps_details_data,$sys_debug,$ps_sessionno,"clmain_u575 ");
           $s_field_value = $this->clmain_v200_load_line( $s_field_value,$ps_details_def,$ps_details_data,"no",$ps_sessionno,$sys_function_name." A100_INIT");
       }
//echo "<br> clmain u575 field ar_field_n_value=".$ar_field_n_value." value after = ".$s_field_value."<br>";
       $s_data_blob = $s_data_blob.$this->clmain_u570_build_xml(trim($ar_field_n_value[0]),$s_field_value,"NO","u575");
   }

    $s_data_blob = $s_data_blob.$this->clmain_u570_build_xml("ut_end","data_blob","NO","u575-end");
Z900_EXIT:
    $sys_function_out = $s_field_name."|^%##%^|".$s_data_blob;
//    $sys_function_out = $s_field_name."|^%##%^|this is working".$sys_function_out;

    IF ($sys_debug == "YES"){$this->z901_dump( $sys_function_name."  returned  = ".$sys_function_out." ");};

//    return str_replace('"',"gjw",$sys_function_out);
    return $sys_function_out;


}
//##########################################################################################################################################################################
function clmain_u580_insert_field_xml($ps_xml,$ps_endtag,$ps_field,$ps_value, $ps_debug, $ps_calledfrom)
{
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_outdata = "none";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u580_insert_field_xml - called from - ".$ps_calledfrom."  ";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."  DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name."  ps_debug = ".$ps_debug);};

A100_INIT:
//gw20110110 - if the field already exists in the xml then replace it ( do modify )
    $s_fieldstarttag = "<".strtolower($ps_field).">";

//echo "<br> s_fieldstarttag=".strtolower($ps_field);


    IF (strpos(strtolower($ps_xml),$s_fieldstarttag) === false)
    {
//echo "<br> does not exist  s_fieldstarttag=".strtolower($ps_field);
        goto A200_ADD;
    }
    $sys_outdata = $this->clmain_u590_modify_field_xml($ps_xml,$ps_field,$ps_value, $ps_debug, $ps_calledfrom."_via580");
    goto Z900_EXIT;

A200_ADD:
// strip off the ps_endtag
// put the field on
// put the end tag back
    $s_endtag = "</".$ps_endtag.">";
    if (strpos($ps_xml,$s_endtag) === false)
    {
        $sys_outdata = $ps_xml;
        $sys_outdata = $sys_outdata."<error>*ERROR* ut_580_err_unknown_endtag=".$ps_endtag." field=".$ps_field."= with value =".$ps_value."= not added</error>";
        goto Z900_EXIT;
    }
// if the endtag does not exist exit without change
    $sys_outdata = substr($ps_xml,0,strpos($ps_xml,$s_endtag));
    $sys_outdata = $sys_outdata."<".strtolower($ps_field).">".$this->clmain_u595_xml_encode_invalid_char($ps_value)."</".strtolower($ps_field).">";
    $sys_outdata =     $sys_outdata.$s_endtag;
A900_END:
Z900_EXIT:
       return $sys_outdata;
}
//##########################################################################################################################################################################
function clmain_u590_modify_field_xml($ps_xml,$ps_field,$ps_value, $ps_debug, $ps_calledfrom)
{
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_outdata = "none";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u590_modify_field_xml - called from - ".$ps_calledfrom."  ";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."  DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name."  ps_debug = ".$ps_debug);};

A100_INIT:
// find the current tag
// if the ps_value = ut_delete remove the field
// put the end tag back

    $s_field = "<".strtolower($ps_field).">";
    $s_fieldend = "</".strtolower($ps_field).">";
    if (strpos($ps_xml,$s_field) === false)
    {
        if ($ps_calledfrom <> "update_only")
        {
            echo "<br>*****************************a100 clmain u90 ps_xml=".$ps_xml."";
            echo "<br>**check the case of the field**************************a100 clmain u90 s_field=".$s_field."";
        }
        $sys_outdata = $ps_xml;
        $sys_outdata = $sys_outdata."<error>ut_590_err_unknown_field=".$ps_field." (check the case ) with value ".$ps_value." not modified</error>";
        goto Z900_EXIT;
    }
// if the endtag does not exist exit without change
    if (strpos($ps_xml,$s_fieldend) === false)
    {
        if ($ps_calledfrom <> "update_only")
        {
            echo "<br>*****************************a100 clmain u90 ps_xml=".str_replace("<","^openbracket^",$ps_xml)."";
            echo "<br>** the end tag is missing check the case of the field**************************a100 clmain u90 s_field=".str_replace("<","^openbracket^",$s_fieldend)."";
        }
        $sys_outdata = $ps_xml;
        $sys_outdata = $sys_outdata."<error>ut_590_err_unknown_field=".$ps_field." (check the case or endtag ) with value ".$ps_value." not modified</error>";
        goto Z900_EXIT;
    }
    $s_before_field = substr($ps_xml,0,strpos($ps_xml,$s_field));
    $s_after_field = substr($ps_xml,strpos($ps_xml,$s_fieldend)+strlen($s_fieldend));

    if (strtoupper(TRIM($ps_value)) == "UT_DELETE")
    {
       $sys_outdata =  $s_before_field.$s_after_field;
       GOTO A900_END;
    }
       $sys_outdata =  $s_before_field.$s_field.$this->clmain_u595_xml_encode_invalid_char($ps_value).$s_fieldend.$s_after_field;
A900_END:

Z900_EXIT:
       return $sys_outdata;
}
//##########################################################################################################################################################################
function clmain_u592_get_field_from_xml($ps_xml,$ps_field, $ps_debug, $ps_calledfrom, $ps_default)
{
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_outdata = "none";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u592_get_field_from_xml - called from - ".$ps_calledfrom."  ";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){echo( $sys_debug_text."  called ".$sys_function_name."  DEBUG IS WORKING<br>");};
    IF ($sys_debug == "YES"){echo( $sys_debug_text."=".$sys_function_name."  ps_debug = ".$ps_debug."<br>");};

    IF ($sys_debug == "YES"){echo( $sys_debug_text."=".$sys_function_name."<BR>XML=<BR>".$ps_xml."<BR>end of xml <br>");};

A100_INIT:
// find the current tag
// if the ps_value = ut_delete remove the field
// put the end tag back

    $s_field = "<".strtolower($ps_field).">";
    $s_fieldend = "</".strtolower($ps_field).">";
    IF ($sys_debug == "YES"){echo( $sys_debug_text."=".$sys_function_name."  s_field = ".$s_field." s_fieldend=".$s_fieldend."<br>");};
    if (strpos(strtolower($ps_xml),$s_field) === false)
    {
        $sys_outdata = $ps_default;
        if (strtoupper(trim($sys_outdata)) == "ERROR")
        {
            $sys_outdata = "?unknownfield?".$ps_field."?";
        }
        goto Z900_EXIT;
    }
// if the endtag does not exist exit without change
//    $s_before_field = substr($ps_xml,0,strpos($ps_xml,$s_field));
//    $s_after_field = substr($ps_xml,strpos($ps_xml,$s_fieldend)+strlen($s_fieldend));
    $value = substr($ps_xml,strpos(strtolower($ps_xml),$s_field)+strlen($s_field));
//    $value = str_replace($s_field,"",$value);
    $value = substr($value,0,strpos($value,"<"));

    $sys_outdata = $this->clmain_u596_xml_decode_char($value);
A900_END:

Z900_EXIT:
    IF ($sys_debug == "YES"){echo( $sys_debug_text."=".$sys_function_name."  sys_outdata = ".$sys_outdata."<br>");};
       return $sys_outdata;
}
//##########################################################################################################################################################################
function clmain_u593_get_nodes_from_xml($par_xml,$ps_nodes, $ps_linedebug, $ps_calledfrom, $ps_default,$p_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_outdata = "none";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u592_get_field_from_xml - called from - ".$ps_calledfrom."  ";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."  DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name."  ps_debug = ".$ps_debug);};

A100_INIT:
    $ar_node = explode(",",$ps_nodes);

    if (count($ar_node) == 1 )  // 1 node only
    {
        if (isset($par_xml[$ar_node[0]]))
        {
            $sys_outdata=$par_xml[$ar_node[0]];
        }else{
            $sys_outdata = "Node(".$ar_node[0].") does not exist";
        }
    }

    if (count($ar_node) == 2 )  // 2 node only
    {
        if (isset($par_xml[$ar_node[0]][$ar_node[1]]))
        {
            $sys_outdata=$par_xml[$ar_node[0]][$ar_node[1]];
        }else{
            $sys_outdata = "Node(".$ar_node[0].",".$ar_node[1].") does not exist";
        }
    }
/* gw20100501 - only needed for de bug
    if (is_array($par_xml))
    {
        echo "<br> the xml is an array";
    }
        echo "<br> ";
        echo "<br> the xml is an array par_xml=".$par_xml;
        print_r($par_xml);
        echo "<br> ";
        echo "<br> ";
*/

    if (count($ar_node) == 3 )  // 3 node only
    {
//gw20110501         echo "<br>Node(".$ar_node[0].",".$ar_node[1].",".$ar_node[2].")";
         $s_node1 = $ar_node[0];
         $s_node2 = $ar_node[1];
         $s_node3 = $ar_node[2];

//gw20110501         echo "<br>Node fields(".$s_node1.",".$s_node2.",".$s_node3.")";
 //        Node1=Message Node2=ServiceIncident Node3=REQUESTERIddddD data_value=field does not exist
//        if (isset($par_xml[$ar_node[0]][$ar_node[1]][$ar_node[2]]))
        if (isset($par_xml[$s_node1][$s_node2][$s_node3]))
        {
            $sys_outdata=$par_xml[$ar_node[0]][$ar_node[1]][$ar_node[2]];
        }else{
            $sys_outdata = "Node(".$ar_node[0].",".$ar_node[1].",".$ar_node[2].") does not exist";
        }
    }


/*    $s_node1='Message';
    $s_node2='ServiceIncident';
    $s_node3='REQUESTERID';
    if (isset($par_xml[$s_node1][$s_node2][$s_node3]))
    {
        $data_value=$par_xml[$s_node1][$s_node2][$s_node3];
    }else{
        $data_value = "field does not exist";
    }

*/

A900_END:

Z900_EXIT:
       return $sys_outdata;
}
//##########################################################################################################################################################################
function clmain_u595_xml_encode_invalid_char($ps_value)
{
    $s_value = $ps_value;

    $s_value = str_replace("&","clmain_u595_amp",$s_value);
    $s_value = str_replace("<","clmain_u595_open_brkt_",$s_value);


    return $s_value;
}
//##########################################################################################################################################################################
function clmain_u595A_xml_remove_invalid_char($ps_value)
    {
        $s_value = $ps_value;

        $s_value = str_replace("&","",$s_value);
        $s_value = str_replace("<","",$s_value);
        $s_value = str_replace(">","",$s_value);

        return $s_value;
    }
//##########################################################################################################################################################################
function clmain_u596_xml_decode_char($ps_value)
{
    $s_value = $ps_value;

    $s_value = str_replace("clmain_u595_amp","&",$s_value);
    $s_value = str_replace("clmain_u595_open_brkt_","<",$s_value);

    return $s_value;
}
//##########################################################################################################################################################################
    function clmain_u597_xml_to_view_html($ps_value)
    {
        $s_value = $ps_value;

        $s_value = str_replace("<","&lt;",$s_value);
        $s_value = str_replace(">","&gt;",$s_value);

        return $s_value;
    }
//##########################################################################################################################################################################
    function clmain_u598_xmlarray_keys_toupper($array)
    {
        $array = array_change_key_case($array, CASE_UPPER);
        foreach ($array as $key => $value)
        {
            if ( is_array($value) )
            {
                $array[$key] = $this->clmain_u598_xmlarray_keys_toupper($value);
            }
        }
        return $array;
    }
    function clmain_u599_xmlIntoArray($arrObjData, $arrSkipIndices = array())
    {
        $arrData = array();

        // if input is object, convert into array
        if (is_object($arrObjData)) {
            $arrObjData = get_object_vars($arrObjData);
        }

        if (is_array($arrObjData)) {
            foreach ($arrObjData as $index => $value) {
                if (is_object($value) || is_array($value)) {
                    $value = $this->clmain_u599_xmlIntoArray($value, $arrSkipIndices); // recursive call
                }
                if (in_array($index, $arrSkipIndices)) {
                    continue;
                }
                $arrData[$index] = $value;
            }
        }
        return $arrData;
    }

//##########################################################################################################################################################################
function clmain_u600_pdf_to_local_printer_html($ps_filename,$ps_debug)
{
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_outdata = "none";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u600_pdf_to_local_printer_html - called from - ".$ps_debug."  ";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."  DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name."  ps_debug = ".$ps_debug);};

A100_INIT:
    $s_printer = "NONE";
    if (strpos(strtoupper($ps_filename),".TXT") === FALSE)
    {}else{
        $s_printer = $_SESSION['ko_printer_local_queue_txt'];
    }
    if (strpos(strtoupper($ps_filename),".PDF") === FALSE)
    {}else{
        $s_printer = $_SESSION['ko_printer_local_queue_pdf'];
    }

    if ( $s_printer == "NONE")
    {
        $sys_outdata = 'SysMes2044 No ko_local_printer';
    }else{
        $sys_outdata = '<a href="ut_printlocal.php?file_to_print='.$ps_filename.'&printer='.$s_printer.'" target="BLANK">Print</a>';
    }

A900_END:


Z900_EXIT:
       return $sys_outdata;
}
//##########################################################################################################################################################################
function clmain_u610_pdf_to_view_html($ps_filename,$ps_debug)
{
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_outdata = "none";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u600_pdf_to_local_printer_html - called from - ".$ps_debug."  ";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."  DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name."  ps_debug = ".$ps_debug);};

A100_INIT:
    $sys_outdata = '<a href="'.$ps_filename.'" target="blank">View</a>';

A900_END:


Z900_EXIT:
       return $sys_outdata;
}
//##########################################################################################################################################################################
//sys_class|v1|jcl_end_utime|clmain_u620_time_function("set","none","none",NO|NO|END
//sys_class|v1|jcl_run_utime|clmain_u620_time_function("diff",#p%!_jcl_start_utime_!%#p,#p%!_jcl_end_utime_!%#p,DEBUG|NO|END
function clmain_u620_time_function($ps_action, $ps_utime_from,$ps_utime_to,$ps_debug)
{

    $sys_debug = strtoupper($ps_debug);
    $s_out = microtime(true);

    if ($ps_debug <> "NO")
    {
        ECHO "<br> clmain_u620_time_function debug = ".$ps_debug."<br> ps_action=".$ps_action." ps_utime_from=".$ps_utime_from." ps_utime_to=".$ps_utime_to;
    }

    if (strtoupper(trim($ps_action))  == "DIFF")
    {
        GOTO B200_DO_DIFF;
    }
    if (strtoupper(trim($ps_action))  == "DIFFMINS")
    {
        GOTO B200_DO_DIFF;
    }
//gw20110305 adds have not been tested
    if (strtoupper(trim($ps_action))  == "ADD")
    {
        $s_add_seconds = $ps_utime_to ;
        GOTO B300_DO_ADD;
    }

    if (strtoupper(trim($ps_action))  == "ADD_SECONDS")
    {
        $s_add_seconds = $ps_utime_to ;
        GOTO B300_DO_ADD;
    }
    if (strtoupper(trim($ps_action))  == "ADD_MINUTES")
    {
        $s_add_seconds = $ps_utime_to * 60;
        GOTO B300_DO_ADD;
    }
    if (strtoupper(trim($ps_action))  == "ADD_HOURS")
    {
        $s_add_seconds = $ps_utime_to * 60 * 60 ;
        GOTO B300_DO_ADD;
    }
    if (strtoupper(trim($ps_action))  == "ADD_DAYS")
    {
        $s_add_seconds = $ps_utime_to * 24 * 60 * 60 ;
        GOTO B300_DO_ADD;
    }

    if ($sys_debug <> "NO"){ECHO "<br> clmain_u620_time_function debug = NOT diff PROCESS";}
    GOTO Z900_END;
B200_DO_DIFF:
    $s_utime_to = $ps_utime_to;
    $s_utime_from = $ps_utime_from;

    if (trim($s_utime_to) == "")
    {
        $s_out = "0";
        GOTO Z900_END;
    }
    if (trim($s_utime_from) == "")
    {
        $s_out = "0";
        GOTO Z900_END;
    }
    IF (strpos(strtoupper($s_utime_from),"T") === FALSE)
    {
        goto B210_UT_FROM_END;
    }
//gw20120522 - add the udtime processing
    $d_hr = substr($s_utime_from,11,2);
    $s_min = substr($s_utime_from,14,2);
    $s_sec = substr($s_utime_from,17,2);
    $s_month = substr($s_utime_from,5,2);
    $s_day = substr($s_utime_from,8,2);
    $s_year = substr($s_utime_from,0,4);
    $s_daylight_saving= "0";
    $s_utime_from = mktime($d_hr,$s_min,$s_sec,$s_month,$s_day,$s_year);
B210_UT_FROM_END:
    IF (strpos(strtoupper($s_utime_to),"T") === FALSE)
    {
        goto B220_UT_TO_END;
    }
//gw20120522 - add the udtime processing
    $d_hr = substr($s_utime_to,11,2);
    $s_min = substr($s_utime_to,14,2);
    $s_sec = substr($s_utime_to,17,2);
    $s_month = substr($s_utime_to,5,2);
    $s_day = substr($s_utime_to,8,2);
    $s_year = substr($s_utime_to,0,4);
    $s_daylight_saving= "0";
    $s_utime_to = mktime($d_hr,$s_min,$s_sec,$s_month,$s_day,$s_year);
B220_UT_TO_END:
    $s_out = $s_utime_to - $s_utime_from;
    if ($sys_debug <> "NO"){ECHO "<br> clmain_u620_time_function calculated the diff between value = ".$s_out;}

    if (strtoupper(trim($ps_action))  == "DIFFMINS")
    {
        $s_out = $s_out / 60;
    }

    GOTO Z900_END;
B300_DO_ADD:
    $s_out = $ps_utime_from + $s_add_seconds;
    GOTO Z900_END;

Z900_END:
    return $s_out;
}
//##########################################################################################################################################################################
function clmain_u630_tidy_siteparams($ps_url_siteparams,$ps_debug)
{
//gw20110115 - siteparams may have duplicate parameters eg ^mode=newrec^mode=issuerecs
// need to get rid of first one - always keep the last one


    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u630_tidy_siteparams";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." ps_url_siteparams = ".$ps_url_siteparams."  ps_debug = ".$ps_debug."  ");};

    global $class_main;
A001_DEFINE_VARS:
    $ar_line_details = array();
    $s_field_name = "";
    $s_param_name = "";
    $sys_function_out = "";
    $ar_url_siteparams = array();
    $ar_url_siteparams_vars = array();

A100_INIT_VARS:
    $ar_url_siteparams = explode("^",$ps_url_siteparams);

    // for each entry in the list check to see if there is another entry further down the list
    // if there is replace the current value with the later value and remove the later value

B001_DO_V1:
    $i = 0;

B100_ENTRY:
    IF ($i >= count($ar_url_siteparams))
    {
        goto B900_END;
    }
//    echo "doing param loop param value =".$ar_url_siteparams[$i]."<br>";
    if (strpos($ar_url_siteparams[$i],"=") === false)
    {
//        echo "doing param loop param no equals sign<br>";
        goto B200_GET_NEXT;
    }
    $ar_url_siteparams_vars = explode("=",$ar_url_siteparams[$i]);
    $s_i1_paramname =strtoupper(trim($ar_url_siteparams_vars[0]," "));

    $i2 = $i + 1;

B150_i2_loop:
    IF ($i2 >= count($ar_url_siteparams))
    {
        goto B200_GET_NEXT;
    }

//    IF THE i2 param name = i1 name set the i1 to the value of i2
    $ar_url_siteparams_vars = explode("=",$ar_url_siteparams[$i2]);
    $s_i2_paramname =strtoupper(trim($ar_url_siteparams_vars[0]," "));

    if ($s_i1_paramname == $s_i2_paramname)
    {
        $ar_url_siteparams[$i] = $ar_url_siteparams[$i2];
        $ar_url_siteparams[$i2]="";
    }

B155_I2_NEXT:
    $i2 = $i2 + 1;
    goto B150_i2_loop;

B200_GET_NEXT:
    $i = $i + 1;
    goto B100_ENTRY;

B900_END:
//    echo "doing urlvalue<br>s_field_name=".$s_field_name."<br>s_param_name=".$s_param_name."<br>sys_function_out=".$sys_function_out."<br>";
A1_DO_V2:

Z900_EXIT:
// put params back together again
    $i = 0;
    $sys_function_out = "";
Z100_ENTRY:
    IF ($i >= count($ar_url_siteparams))
    {
        goto Z190_END;
    }
    IF (TRIM($ar_url_siteparams[$i]) == "")
    {
        GOTO Z110_NEXT;
    }
    $sys_function_out = $sys_function_out.$ar_url_siteparams[$i]."^";
Z110_NEXT:
    $i = $i + 1;
    goto Z100_ENTRY;
Z190_END:
// stip last ^
//    if (substr($sys_function_out,-1,1) == "^")
//    {
//        $sys_function_out = substr($sys_function_out,0,strlen($sys_function_out) - 1);
//    }
    IF ($sys_debug == "YES"){z901_dump( $sys_function_name." ps_url_siteparams=".$ps_url_siteparams."  returned  = ".$sys_function_out." ");};

//    $sys_function_out = $ps_url_siteparams;

//    ECHO "<BR> GW CLMAIN U630 <BR> ps_url_siteparams=".$ps_url_siteparams."<BR>SYSOUT=".$sys_function_out."<BR>";
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_u640_timeline_html($ps_values,$ps_params,$ps_calledfrom,$ps_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u640_timeline_html";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." param_list = ps_companyid,ps_debug,ps_dbcnx,ps_calledfrom,ps_details_def,ps_details_data,ps_sessionno,ps_values,ps_params)");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." param_values= ps_values=[]".$ps_values."[],  ps_params=[]".$ps_params."[],  ps_calledfrom=[]".$ps_calledfrom."[],  ps_debug=[]".$ps_debug."[],  ps_details_def=[]".$ps_details_def."[],  ps_details_data=[]".$ps_details_data."[],  ps_sessionno=[]".$ps_sessionno);};

    global $class_main;
A001_DEFINE_VARS:
    $ar_params = array();
    $sys_function_out = "";
    $ar_values = array();
    $ar_value_fields = array();

// ar_value_fields = Value;:;Prompt
// are_params = "total width:;:"

A100_INIT_VARS:
    $s_timeline_test = "";
    $s_timeline_test = $s_timeline_test."<tr><td class='udj_data'>Job Comparator ";
    $s_timeline_test = $s_timeline_test."</td></tr>";

    $s_timeline_test = $s_timeline_test."<tr  bgcolor='white'><td>";
    $s_timeline_test = $s_timeline_test."<img src='images/transparent.png' height='12' width='40px' border='0'>";
    $s_timeline_test = $s_timeline_test."<img src='images/gold.png' height='12' width='1000px' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";

    $s_timeline_test = $s_timeline_test."<tr bgcolor='white'><td>";
    $s_timeline_test = $s_timeline_test."<img src='images/transparent.png' height='12' width='40px' border='0'>";
    $s_timeline_test = $s_timeline_test."<img src='images/pink.png' height='12' width='320px' border='0'>";
    $s_timeline_test = $s_timeline_test."<img src='images/green_l.png' height='12' width='330px' border='0'>";
    $s_timeline_test = $s_timeline_test."<img src='images/gray_l.png' height='12' width='300px' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";

    $s_timeline_test = $s_timeline_test."<tr  bgcolor='white'><td>";
    $s_timeline_test = $s_timeline_test."<img src='images/blue_l.png' height='12' width='90px' border='0'>";
    $s_timeline_test = $s_timeline_test."<img src='images/black.png' height='12' width='194px' border='0'>";
    $s_timeline_test = $s_timeline_test."<img src='images/blue_l.png' height='12' width='106px' border='0'>";
    $s_timeline_test = $s_timeline_test."<img src='images/black.png' height='12' width='194px' border='0'>";
    $s_timeline_test = $s_timeline_test."<img src='images/blue_l.png' height='12' width='106px' border='0'>";
    $s_timeline_test = $s_timeline_test."<img src='images/black.png' height='12' width='194px' border='0'>";
    $s_timeline_test = $s_timeline_test."<img src='images/blue_l.png' height='12' width='106px' border='0'>";
    $s_timeline_test = $s_timeline_test."<img src='images/black.png' height='12' width='90px' border='0'>";
//    $s_timeline_test = $s_timeline_test."<img src='images/transparent.png' height='12' width='50px' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";

    $s_timeline_test = $s_timeline_test."<tr  bgcolor='white'><td  class='pix_nine'>";
    $s_timeline_test = $s_timeline_test."Mon<img src='images/transparent.png' height='12' width='30' border='0'>";
    $s_timeline_test = $s_timeline_test."<img src='images/transparent.png' height='12' width='234px' border='0'>";
    $s_timeline_test = $s_timeline_test."Tue<img src='images/transparent.png' height='12' width='30' border='0'>";
    $s_timeline_test = $s_timeline_test."<img src='images/transparent.png' height='12' width='250px' border='0'>";
    $s_timeline_test = $s_timeline_test."Wed<img src='images/transparent.png' height='12' width='30' border='0'>";
    $s_timeline_test = $s_timeline_test."<img src='images/transparent.png' height='12' width='250px' border='0'>";
    $s_timeline_test = $s_timeline_test."Thu<img src='images/transparent.png' height='12' width='30' border='0'>";
    $s_timeline_test = $s_timeline_test."<img src='images/transparent.png' height='12' width='250px' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";

/*
    $s_timeline_test = $s_timeline_test."<tr><td class='pix_nine'><img src='images/black.png' height='12' width='50' border='0'>";
    $s_timeline_test = $s_timeline_test."<img src='images/transparent.png' height='12' width='50' border='0'>";
    $s_timeline_test = $s_timeline_test."<img src='images/blue_l.png' height='12' width='50' border='0'>";
    $s_timeline_test = $s_timeline_test."<img src='images/transparent.png' height='12' width='50' border='1'>";
    $s_timeline_test = $s_timeline_test."<img src='images/transparent.png' height='12' width='50' border='1'>";
    $s_timeline_test = $s_timeline_test."<img src='images/transparent.png' height='12' width='50' border='1'>";
    $s_timeline_test = $s_timeline_test."<img src='images/transparent.png' height='12' width='50' border='1'>";
    $s_timeline_test = $s_timeline_test."<img src='images/blue_l.png' height='12' width='50' border='1'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td><img src='images/blue_l.png' height='12' width='50' border='0'>";
    $s_timeline_test = $s_timeline_test."<img src='images/transparent.png' height='12' width='50' border='0'>";
    $s_timeline_test = $s_timeline_test."<img src='images/blue_l.png' height='12' width='50' border='0'>";
    $s_timeline_test = $s_timeline_test."<img src='images/transparent.png' height='12' width='50' border='1'>";
    $s_timeline_test = $s_timeline_test."<img src='images/blue_l.png' height='12' width='50' border='0'>";
    $s_timeline_test = $s_timeline_test."<img src='images/transparent.png' height='12' width='50' border='1'>";
    $s_timeline_test = $s_timeline_test."<img src='images/blue_l.png' height='12' width='50' border='0'>";
    $s_timeline_test = $s_timeline_test."<img src='images/transparent.png' height='12' width='50' border='1'>";
    $s_timeline_test = $s_timeline_test."<img src='images/blue_l.png' height='12' width='50' border='0'>";
    $s_timeline_test = $s_timeline_test."<img src='images/transparent.png' height='12' width='50' border='1'>";
    $s_timeline_test = $s_timeline_test."<img src='images/blue_l.png' height='12' width='50' border='1'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td  class='pix_nine'>";
    $s_timeline_test = $s_timeline_test."Mon<img src='images/transparent.png' height='12' width='30' border='0'>";
    $s_timeline_test = $s_timeline_test."Tue<img src='images/transparent.png' height='12' width='30' border='0'>";
    $s_timeline_test = $s_timeline_test."Wed<img src='images/transparent.png' height='12' width='30' border='0'>";
    $s_timeline_test = $s_timeline_test."Thu<img src='images/transparent.png' height='12' width='30' border='0'>";
    $s_timeline_test = $s_timeline_test."Fri<img src='images/transparent.png' height='12' width='30' border='0'>";
    $s_timeline_test = $s_timeline_test."Sat<img src='images/transparent.png' height='12' width='30' border='0'>";
    $s_timeline_test = $s_timeline_test."Sun<img src='images/transparent.png' height='12' width='30' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td><img src='images/blue_l.png' height='12' width='50' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td  class='pix_nine'>";
    $s_timeline_test = $s_timeline_test."Mon<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."=15pix<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."Wed<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."Thu<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."Fri<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."Sat<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."Sun<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td><img src='images/blue_l.png' height='12' width='50' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td  class='pix_nine'>";
    $s_timeline_test = $s_timeline_test."Tue<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."=15pix<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."<tr><td><img src='images/blue_l.png' height='12' width='50' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td  class='pix_nine'>";
    $s_timeline_test = $s_timeline_test."Wed<img src='images/transparent.png' height='12' width='31' border='0'>";
    $s_timeline_test = $s_timeline_test."=19pix<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."<tr><td><img src='images/blue_l.png' height='12' width='50' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td  class='pix_nine'>";
    $s_timeline_test = $s_timeline_test."Thu<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."=15pix<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."<tr><td><img src='images/blue_l.png' height='12' width='50' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td  class='pix_nine'>";
    $s_timeline_test = $s_timeline_test."Fri<img src='images/transparent.png' height='12' width='40' border='0'>";
    $s_timeline_test = $s_timeline_test."=10pix<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."<tr><td><img src='images/blue_l.png' height='12' width='50' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td  class='pix_nine'>";
    $s_timeline_test = $s_timeline_test."Sat<img src='images/transparent.png' height='12' width='36' border='0'>";
    $s_timeline_test = $s_timeline_test."=16pi<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."<tr><td><img src='images/blue_l.png' height='12' width='50' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td  class='pix_nine'>";
    $s_timeline_test = $s_timeline_test."Sun<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."=15pi<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td><img src='images/blue_l.png' height='12' width='50' border='0'>";

    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td  class='pix_nine'>";
    $s_timeline_test = $s_timeline_test."1<img src='images/transparent.png' height='12' width='48' border='0'>";
    $s_timeline_test = $s_timeline_test."=2px<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td><img src='images/blue_l.png' height='12' width='50' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td  class='pix_nine'>";
    $s_timeline_test = $s_timeline_test."2<img src='images/transparent.png' height='12' width='48' border='0'>";
    $s_timeline_test = $s_timeline_test."=2px<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td><img src='images/blue_l.png' height='12' width='50' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td  class='pix_nine'>";
    $s_timeline_test = $s_timeline_test."3<img src='images/transparent.png' height='12' width='48' border='0'>";
    $s_timeline_test = $s_timeline_test."=2px<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td><img src='images/blue_l.png' height='12' width='50' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td  class='pix_nine'>";
    $s_timeline_test = $s_timeline_test."4<img src='images/transparent.png' height='12' width='48' border='0'>";
    $s_timeline_test = $s_timeline_test."=2px<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td><img src='images/blue_l.png' height='12' width='50' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td  class='pix_nine'>";
    $s_timeline_test = $s_timeline_test."5<img src='images/transparent.png' height='12' width='48' border='0'>";
    $s_timeline_test = $s_timeline_test."=2px<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td><img src='images/blue_l.png' height='12' width='50' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td  class='pix_nine'>";
    $s_timeline_test = $s_timeline_test."6<img src='images/transparent.png' height='12' width='48' border='0'>";
    $s_timeline_test = $s_timeline_test."=2px<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td><img src='images/blue_l.png' height='12' width='50' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td  class='pix_nine'>";
    $s_timeline_test = $s_timeline_test."7<img src='images/transparent.png' height='12' width='48' border='0'>";
    $s_timeline_test = $s_timeline_test."=2px<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td><img src='images/blue_l.png' height='12' width='50' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td  class='pix_nine'>";
    $s_timeline_test = $s_timeline_test."8<img src='images/transparent.png' height='12' width='48' border='0'>";
    $s_timeline_test = $s_timeline_test."=2px<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td><img src='images/blue_l.png' height='12' width='50' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td  class='pix_nine'>";
    $s_timeline_test = $s_timeline_test."9<img src='images/transparent.png' height='12' width='48' border='0'>";
    $s_timeline_test = $s_timeline_test."=2px<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td><img src='images/blue_l.png' height='12' width='50' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td  class='pix_nine'>";
    $s_timeline_test = $s_timeline_test."0<img src='images/transparent.png' height='12' width='48' border='0'>";
    $s_timeline_test = $s_timeline_test."=2px<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td><img src='images/blue_l.png' height='12' width='50' border='0'>";
    $s_timeline_test = $s_timeline_test."<tr><td  class='pix_nine'>";
    $s_timeline_test = $s_timeline_test."08:00<img src='images/transparent.png' height='12' width='28' border='0'>";
    $s_timeline_test = $s_timeline_test."=22px<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td><img src='images/blue_l.png' height='12' width='50' border='0'>";
    $s_timeline_test = $s_timeline_test."<tr><td  class='pix_nine'>";
    $s_timeline_test = $s_timeline_test."21/02<img src='images/transparent.png' height='12' width='28' border='0'>";
    $s_timeline_test = $s_timeline_test."=22px<img src='images/transparent.png' height='12' width='35' border='0'>";
    $s_timeline_test = $s_timeline_test."</td></tr>";
    $s_timeline_test = $s_timeline_test."<tr><td><img src='images/blue_l.png' height='12' width='50' border='0'>";
*/
    // for each entry in the list check to see if there is another entry further down the list
    // if there is replace the current value with the later value and remove the later value
/*
B001_DO_V1:
    $i = 0;

B100_ENTRY:
    IF ($i >= count($ar_url_siteparams))
    {
        goto B900_END;
    }
//    echo "doing param loop param value =".$ar_url_siteparams[$i]."<br>";
    if (strpos($ar_url_siteparams[$i],"=") === false)
    {
//        echo "doing param loop param no equals sign<br>";
        goto B200_GET_NEXT;
    }
    $ar_url_siteparams_vars = explode("=",$ar_url_siteparams[$i]);
    $s_i1_paramname =strtoupper(trim($ar_url_siteparams_vars[0]," "));

    $i2 = $i + 1;

B150_i2_loop:
    IF ($i2 >= count($ar_url_siteparams))
    {
        goto B200_GET_NEXT;
    }

//    IF THE i2 param name = i1 name set the i1 to the value of i2
    $ar_url_siteparams_vars = explode("=",$ar_url_siteparams[$i2]);
    $s_i2_paramname =strtoupper(trim($ar_url_siteparams_vars[0]," "));

    if ($s_i1_paramname == $s_i2_paramname)
    {
        $ar_url_siteparams[$i] = $ar_url_siteparams[$i2];
        $ar_url_siteparams[$i2]="";
    }

B155_I2_NEXT:
    $i2 = $i2 + 1;
    goto B150_i2_loop;

B200_GET_NEXT:
    $i = $i + 1;
    goto B100_ENTRY;

*/
Z900_EXIT:
    $sys_function_out = $s_timeline_test;
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_u650_set_showat($ps_screen,$ps_user,$ps_phase,$ps_contract,$ps_jobtype,$ps_user_roles,$ps_linedebug,$ps_calledfrom,$ps_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);

    IF ($sys_debug ="NO") {
        $sys_debug = strtoupper($ps_linedebug);
    }

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = "<br>".$ps_calledfrom; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "<br>debug - clmain_u650_set_showat";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." param_list = ps_screen,ps_user,ps_phase,ps_contract,ps_jobtype,ps_linedebug,ps_calledfrom,ps_dbcnx,ps_debug,ps_details_def,ps_details_data,ps_sessionno)");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." param_values=".$ps_screen.",".$ps_user.",".$ps_phase.",".$ps_contract.",".$ps_jobtype.",".$ps_linedebug.",".$ps_calledfrom.",".$ps_dbcnx.",".$ps_debug.",".$ps_details_def.",".$ps_details_data.",".$ps_sessionno);};

    global $class_main;
    global $class_sql;

A001_DEFINE_VARS:
A100_INIT_VARS:

// use the params to set the var to return for the screen
// screen_field=permissions^screen_field=Permission^

// set for phase

// get current roles for user
// set lowest access for each role

// for each
//    phase  |  R  |  C  |  N  |
// role   R  |  R  |  R  |  N  |
//        C  |  R  |  C  |  N  |
//        N  |  N  |  N  |  N  |
//returns fieldname=pr^fieldname2=pr

    $s_phasegroup = substr($ps_phase,0,1);
    $s_roles_or_phasegroup = "('".$s_phasegroup."','".$ps_user_roles."')";
 //and key1 in ('hppurrjm','3')
        IF ($sys_debug == "YES"){ echo ( $sys_debug_text."=".$sys_function_name."screen=".$ps_screen."^user=".$ps_user."^phase=".$ps_phase."^contract=".$ps_contract."^jobtype=".$ps_jobtype."^linedebug=".$ps_linedebug."^calledfrom=".$ps_calledfrom.",s_phasegroup=".$s_phasegroup.",ps_user_roles".$ps_user_roles);};

    $s_results = "";
B000_GET_JOB_DETAILS:
        IF ($sys_debug == "YES"){ echo ( $sys_debug_text."=".$sys_function_name." get the job ");};
    $s_sql = "";
    $result = "";
    $s_rec_found = "";
    $s_details_def = "";
    $s_details_data = "";

    $s_details_def = $ps_details_def ;
    $s_details_data = $ps_details_data ;

    $s_reccount = "0";


//gw20120203

    $s_sql = "select * from jobs_masters where recordtype = 'SHOWAT' and key2 = '".$ps_screen."'  and (key1 in ".$s_roles_or_phasegroup.") AND ( key3 ='all' or key3 like '".$s_phasegroup."%') and (key4 = 'all' or key4 = '".$ps_contract."_".$ps_jobtype."') order by recordgroup desc, key3 asc";
//    $s_sql = str_replace("#p","|",$s_sql);
//    $s_sql = str_replace("#P","|",$s_sql);
//    $s_sql = $class_main->clmain_v200_load_line( $s_sql,$s_details_def,$s_details_data,"no",$ps_sessionno,$s_sys_function_name."_A");
    IF ($sys_debug == "YES"){echo ( $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql." ");};
B015_DO:
    $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_sql);
    $s_rec_found = "NO";
    $s_numrows = mysql_num_rows($result);
    $i_record_count = "0";

    if ($s_numrows ==  "0"){
        IF ($sys_debug == "YES"){ echo ( $sys_debug_text."=".$sys_function_name." thera are no records for s_sql = ".$s_sql." ");};
        $s_results = "u650_showat = no records to use to set values- default to C";
        GOTO Z900_EXIT;
    }

B100_GET_RECORDS:
    if ($i_record_count > $s_numrows) {
        $s_results = $s_results."^u650_lastrec=".$i_record_count."of".$s_numrows;
        goto B190_END_RECORDS;
    }
    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        $s_results = $s_results."^u650_endofrows=".$i_record_count."of".$s_numrows;
        goto B190_END_RECORDS;
    }
    $s_rec_found = "YES";
B150_DO_RECORDS:
    IF ($sys_debug == "YES"){ echo ( $sys_debug_text."=".$sys_function_name." RECORD = key1=".$row["key1"]." key2=".$row["key2"]." key3=".$row["key3"]." key4=".$row["key4"]." key5=".$row["key5"]." recordgroup=".$row["recordgroup"]." recordtype=".$row["recordtype"]);};

    $s_datablob = $row["data_blob"];

    $s_work_type = $this->clmain_u592_get_field_from_xml($s_datablob,"work_type","NO","clmain u705 d4","");
    $s_screen_default = $this->clmain_u592_get_field_from_xml($s_datablob,"screen_default","NO","clmain u705 d4","");
    $s_exceptions = $this->clmain_u592_get_field_from_xml($s_datablob,"exceptions","NO","clmain u705 d4","");
    $s_hidden = $this->clmain_u592_get_field_from_xml($s_datablob,"hidden","NO","clmain u705 d4","");

    $s_excecption_value = "C";
    if (strtoupper(trim($s_screen_default)) == "C")
    {
        $s_excecption_value = "R";
    }

    $s_exceptions = $s_exceptions.",";
    $s_exceptions = str_replace(",","=".$s_excecption_value.",",$s_exceptions);

    $s_hidden = $s_hidden.",";

    $s_results = "screen_default=".$s_screen_default."^exceptions[".$s_exceptions."]^hidden[".$s_hidden."]";

    goto B180_NEXT;



B180_NEXT:
    $i_record_count = $i_record_count + 1;
    GOTO B100_GET_RECORDS;
B190_END_RECORDS:

B900_EXIT:
    if (trim($s_results) =="")
    {
      $s_results = "u650_showat = no records to use to set values";
    }

Z900_EXIT:
    $sys_function_out = $s_results;
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_u660_set_userroles($ps_userid,$ps_linedebug,$ps_calledfrom,$ps_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }

    IF ($sys_debug == "NO") {
        IF ($ps_linedebug !="NO") {
            $sys_debug  = "YES";
            $sys_debug_text = "<BR>".$ps_linedebug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
        }
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u660_set_userroles";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){echo $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING";};
    IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." param_list = ps_userid,ps_linedebug,ps_calledfrom,)";};
    IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." param_values=".$ps_userid.",".$ps_linedebug.",".$ps_calledfrom;};

    global $class_main;
    global $class_sql;

A001_DEFINE_VARS:
A100_INIT_VARS:
// ****** note this used in the role selection process so MUST BE IN THE 'ROLE','ROLE2'
$s_roles = "'none'";

B000_GET_JOB_DETAILS:
        IF ($sys_debug == "YES"){ echo ( $sys_debug_text."=".$sys_function_name." get the job ");};
    $s_sql = "";
    $result = "";
    $s_rec_found = "";
    $s_details_def = "";
    $s_details_data = "";

    $s_details_def = $ps_details_def ;
    $s_details_data = $ps_details_data ;

    $s_reccount = "0";

    $s_sql = "SELECT * From jobs_masters as jm WHERE ( companyid = '#p%!_COOKIE_ud_companyid_!%#p' and recordgroup = 'JMROLE' and recordtype = 'ROLE_USERS') and key1 = '".$ps_userid."'  ORDER BY key1 ASC";

   $s_sql = $this->clmain_v200_load_line($s_sql,$ps_details_def,$ps_details_data,"NO","1","U660_SETSQAL");
   IF ($sys_debug == "YES"){echo $sys_function_name." doing sql_norec process after line procesing sql=".$s_sql."<br>";};


//    $s_sql = "select * from jobs_masters where recordgroup = 'JMROLE' and recordtype = 'userlist' and key2 = '".$ps_userid."'";
    IF ($sys_debug == "YES"){echo ( $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql." ");};
B015_DO:
    $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_sql);
    $s_rec_found = "NO";
    $s_numrows = mysql_num_rows($result);
    $i_record_count = "0";

    if ($s_numrows ==  "0"){
        IF ($sys_debug == "YES"){ echo ( $sys_debug_text."=".$sys_function_name." thera are no records for s_sql = ".$s_sql." ");};
        GOTO Z900_EXIT;
    }
    $s_roles = "";

B100_GET_RECORDS:
    if ($i_record_count > $s_numrows) {
        goto B190_END_RECORDS;
    }
    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        goto B190_END_RECORDS;
    }
    $s_rec_found = "YES";
    if ($i_record_count > 0) {
        $sys_function_out =     $sys_function_out.",";
    }
B150_DO_RECORDS:
    $s_data_blob = $row["data_blob"];
    $s_roles = $s_roles.$this->clmain_u592_get_field_from_xml($s_data_blob,"role","NO","clmain_u660","nodefault");

    $sys_function_out = $sys_function_out."'".$row["key1"]."'";
    IF ($sys_debug == "YES"){ echo ( $sys_debug_text."=".$sys_function_name." got the job record ");};
B180_NEXT:
    $i_record_count = $i_record_count + 1;
    GOTO B100_GET_RECORDS;
B190_END_RECORDS:

B900_EXIT:

Z900_EXIT:
    $sys_function_out = $s_roles;
    IF ($sys_debug == "YES"){ echo ( $sys_debug_text."=".$sys_function_name."<br>End of function sys_function_out=".$sys_function_out."<br><br> ");};
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_u670_utl_issue_log($ps_dbcnx,$ps_recgroup,$ps_rectype,$ps_companyid,$ps_userid,$ps_summary , $ps_data_blob, $ps_key1, $ps_key1def, $ps_key2, $ps_key2def, $ps_key3, $ps_key3def, $ps_key4, $ps_key4def, $ps_key5, $ps_key5def,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    $s_log_text="";
    $s_creatednt="";
    $s_uk="";
    $s_userid = "";
    $s_runfrom = "";
    $s_recgroup =$ps_recgroup;
    $s_rectype =$ps_rectype;
    $s_companyid = $ps_companyid;
//    echo "doing the activity log";
    $s_creatednt=date("YmdHis");
    $s_userid = $ps_userid;
    $s_createdutime=time();

    $ssql = "INSERT into utl_issues SET createddnt = '".$s_creatednt."',createdutime = '".$s_createdutime."', createduserid = '".$s_userid."', recordgroup ='".$s_recgroup."' , recordtype ='".$s_rectype."' , summary = '".$ps_summary."' , companyid = '".$s_companyid."', data_blob  = '".$ps_data_blob."',key1 = '".$ps_key1."',key1_def = '".$ps_key1def."', key2 = '".$ps_key2."', key2_def = '".$ps_key2def."', key3 = '".$ps_key3."', key3_def = '".$ps_key3def."', key4 = '".$ps_key4."', key4_def = '".$ps_key4def."', key5 = '".$ps_key5."', key5_def = '".$ps_key5def."'";

    $rs_temp = mysql_query($ssql,$ps_dbcnx);
    if (!$rs_temp)
    {
        echo("<P>Error performing query: ".mysql_error()." sql = ".$ssql."</P>");
        exit();
    }
    return $s_uk;
}
//##########################################################################################################################################################################
function clmain_u680_draw_db_maint($ps_action,  $ps_params, $ps_line_debug, $ps_calledfrom,$p_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
// use the details_def details_data to build the necessare screen

global $class_sql;

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_outdata = "none";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u680_draw_db_maint - called from - ".$ps_calledfrom."  ";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."  DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name."  ps_debug = ".$ps_debug);};

    $sys_function_out = "";
A100_INIT:
    $s_action = strtoupper($ps_action);

//    if (strtoupper($s_action) == "ADD")
//    {
//        $s_newkey = $class_sql->c_sqlclient_DuplicateMySQLRecord("jobs_masters","jm_id","revenue_master");
//        echo "<br>new_key".$s_newkey."<br>";
//        $s_keysdone = $this->clmain_u700_set_db_recfields("KEYS","jobs_masters","jm_id",$s_newkey,"clmain_u680",$p_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno);
//    }

    //$sys_function_out = $sys_function_out."<tr><td>  doing u680</td></tr>";
    $s_row_odd = ' class="roweven"';
    $s_row_even = ' class="rowodd"';
    $s_row_oddeven = $s_row_odd;
    $s_row_oddeven_flag = "ODD";
    $ar_params = explode("::",$ps_params);

    $array_def = explode("|",$ps_details_def);
    $array_data = explode("|",$ps_details_data);
    for ($i2 = 0; $i2 < count($array_def); $i2++)
    {
        IF (strpos(strtoupper($array_def[$i2]),"REC_MASTER_DB_SYS") !== FALSE){
            GOTO A800_NEXT;
        }
        IF (strpos(strtoupper($array_def[$i2]),"REC_MASTER_DB_") === FALSE)
        {
            GOTO A800_NEXT;
        }
A200_DO_PARAMS:
        $s_fieldname = substr($array_def[$i2],14);//stip off the rec_master_db
        $s_fieldprompt = $s_fieldname;
        $s_fielddefault = "|%!_REC_DATA_DB_".$s_fieldname."_!%|";
        $s_maxlength= "";//'maxlength="200"';
        $s_size = "";
        $s_do_action = "UPDATE";

        if (trim($array_data[$i2]) == "")
        {
            goto A290_END;
        }
        if (strpos(trim($array_data[$i2]),"^") === false)
        {
            goto A290_END;
        }
        $ar_field_improvers = explode("^",$array_data[$i2]);

        $i_improvers_count = count($ar_field_improvers);
        $i_cnt = 0;
A210_NEXT:
        if ($i_cnt == $i_improvers_count)
        {
            GOTO A290_END;
        }
        $s_fieldvalue = $ar_field_improvers[$i_cnt];
        if (strpos(strtoupper($s_fieldvalue),"PROMPT=") !== false)
        {
            $s_fieldprompt = substr($s_fieldvalue,strpos(strtoupper($s_fieldvalue),"PROMPT=")+7);
        }
        if (strtoupper($s_action) == "ADD")
        {
            if (strpos(strtoupper($s_fieldvalue),"DEFAULT=") !== false)
            {
                $s_fielddefault = substr($s_fieldvalue,strpos(strtoupper($s_fieldvalue),"DEFAULT=")+8);
            }
            if (strtoupper($s_fielddefault) == "SPACE")
            {
                $s_fielddefault = "";
            }
        }

        if (strpos(strtoupper($s_fieldvalue),"MAX=") !== false)
        {
            $s_maxlength =  ' maxlength="'.substr($s_fieldvalue,strpos(strtoupper($s_fieldvalue),"MAX=")+4).'"';
        }
        if (strpos(strtoupper($s_fieldvalue),"SIZE=") !== false)
        {
            $s_size = ' size="'.substr($s_fieldvalue,strpos(strtoupper($s_fieldvalue),"SIZE=")+5).'"';
        }

A280_GET_NEXT:
        $i_cnt = $i_cnt + 1;
        goto A210_NEXT;
A290_END:

A300_SET_FIXED_VALUES:
// check if field is in the param list
        if (strpos($ps_params,$s_fieldname) === false)
        {
            goto A329_END;
        }

        $i_cnt2 = "0";
A323_NEXT_PARAM:
        if ($i_cnt2 >= count($ar_params))
        {
            goto A323_END;
        }
        $ar_params2 = explode(":is:",$ar_params[$i_cnt2]);
        if ($s_fieldname == $ar_params2[0])
        {
            $s_fielddefault = $ar_params2[1];
            $s_do_action = "DISPLAY";
            goto A323_END;
        };

        $i_cnt2 = $i_cnt2 + 1;
        GOTO A323_NEXT_PARAM;
A323_END:

// gw 20120103
//        $ar_params2 = explode(":is:",$ar_params[0]);
//        $s_fielddefault = $ar_params2[1];
//        $s_do_action = "DISPLAY";
A329_END:

A600_MAKE_SCREEN:
        if ($s_row_oddeven_flag == "ODD")
        {
            $s_row_oddeven = $s_row_even;
            $s_row_oddeven_flag = "EVEN";
        }else{
            $s_row_oddeven = $s_row_odd;
            $s_row_oddeven_flag = "ODD";
        }

        if (trim($s_size) == "")
        {
          $s_size=' size="150"';
        };


        IF ($s_do_action == "DISPLAY")
        {
           $sys_function_out = $sys_function_out."<tr".$s_row_oddeven."><td width='200'>".$s_fieldprompt."</td><td>";
           $sys_function_out = $sys_function_out.''.$s_fielddefault.'';
           $sys_function_out = $sys_function_out.'<input type="hidden" name="DB_'.$s_fieldname.'"  value="'.$s_fielddefault.'"  '.$s_maxlength.' '.$s_size.'>';
           $sys_function_out = $sys_function_out."</td></tr>";
            GOTO A800_NEXT;
        }
//gw20110508 treat all as maint
       $sys_function_out = $sys_function_out."<tr".$s_row_oddeven."><td width='200'>".$s_fieldprompt."</td><td>";
       $sys_function_out = $sys_function_out."<input type='text' name='DB_".$s_fieldname."'  value='".$s_fielddefault."' ".$s_maxlength." ".$s_size.">";
       $sys_function_out = $sys_function_out."</td></tr>";
        GOTO A800_NEXT;

//gw20110508 treat all as maint
/*        if (strtoupper($s_action) == "MAINT")
        {
           $sys_function_out = $sys_function_out."<tr".$s_row_oddeven."><td width='200'>".$s_fieldprompt."</td><td>";

           $sys_function_out = $sys_function_out.'<input type="text" name="DB_'.$s_fieldname.'"  value="'.$s_fielddefault.'" maxlength="'.$s_maxlength.'" size="'.$s_size.'">';

           $sys_function_out = $sys_function_out."</td></tr>";
            GOTO A800_NEXT;
        }
        if (strtoupper($s_action) == "ADD")
        {
           $sys_function_out = $sys_function_out."<tr".$s_row_oddeven."><td width='200'>".$s_fieldprompt."</td><td>";

           $sys_function_out = $sys_function_out.'<input type="text" name="'.$s_fieldname.'"  value="'.$s_fielddefault.'" maxlength="'.$s_maxlength.'" size="'.$s_size.'">';

           $sys_function_out = $sys_function_out."</td></tr>";
            GOTO A800_NEXT;
        }
        if (strtoupper($s_action) == "DISPLAY")
        {
           $sys_function_out = $sys_function_out."<tr".$s_row_oddeven."><td width='200'>".$s_fieldname."</td><td>|%!_REC_DATA_DB_".$s_fieldname."_!%|</td></tr>";
            GOTO A800_NEXT;
        }
       $sys_function_out = $sys_function_out."<tr".$s_row_oddeven."><td width='200'>".substr($array_def[$i2],14)."</td><td>|%!_REC_DATA_DB_".substr($array_def[$i2],14)."_!%|</td></tr>";
*/
A800_NEXT:
    }
A900_EXIT:





B100_OPTIONS:
        if (strtoupper($s_action) == "ADD")
        {
//           $sys_function_out = $sys_function_out.'<tr><td >&nbsp;</td><td><input type="hidden" id="newkeyvalue" name="newkeyvalue" value="'.$s_newkey.'"></td></tr>';
            GOTO B190_END;
        }
        if (strtoupper($s_action) == "DISPLAY")
        {
           $sys_function_out = $sys_function_out."<tr><td >&nbsp;</td><td>Close this window to return to the list screen</td></tr>";
            GOTO B190_END;
        }
//       $sys_function_out = $sys_function_out."<tr><td width='200'>".substr($array_def[$i2],14)."</td><td>|%!_REC_DATA_DB_".substr($array_def[$i2],14)."_!%|</td></tr>";
B190_END:
C100_DO_DUMP:
       $sys_function_out = $sys_function_out."<tr><td  colspan='9' align='left'>";

       $sys_function_out = $sys_function_out."<div id='secret' style='display: none;'>";

       $sys_function_out = $sys_function_out."List of the Record fields that have been set";
       $sys_function_out = $sys_function_out."        <table>";

    for ($i2 = 0; $i2 < count($array_def); $i2++)
    {
        IF (strpos(strtoupper($array_def[$i2]),"REC_") === FALSE){
            GOTO C800_NEXT;
        }
C200_DO_PARAMS:
       $sys_function_out = $sys_function_out."<tr".$s_row_oddeven."><td  colspan='5'>".$array_def[$i2]."=".$array_data[$i2]."</td><td>";
C600_MAKE_SCREEN:
        if ($s_row_oddeven_flag == "ODD")
        {
            $s_row_oddeven = $s_row_even;
            $s_row_oddeven_flag = "EVEN";
        }else{
            $s_row_oddeven = $s_row_odd;
            $s_row_oddeven_flag = "ODD";
        }
C800_NEXT:
    }
       $sys_function_out = $sys_function_out."</table>";
       $sys_function_out = $sys_function_out."        </div>";
       $sys_function_out = $sys_function_out."    </td></tr>";
C900_EXIT:



Z900_EXIT:
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_u690_add_db_record($ps_tablename, $ps_keyfield, $ps_keyvalue, $ps_params, $ps_line_debug, $ps_calledfrom, $p_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
// use the details_def details_data to build the necessare screen


global $class_sql;

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_outdata = "none";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    IF ($sys_debug = "NO") {
        IF ($ps_line_debug !="NO") {
            $sys_debug  = "YES";
            $sys_debug_text = $ps_line_debug." calledfrom:".$ps_calledfrom; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
        }
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u690_add_db_record - called from - ".$ps_calledfrom."  ";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."  DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name."  ps_debug = ".$ps_debug);};

    $sys_function_out = "";
A100_INIT:
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."  calling duplicatemysql");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."  ps_tablename=".$ps_tablename." ps_keyfield=".$ps_keyfield." ps_keyvalue=".$ps_keyvalue."");};
        $s_newkey = $class_sql->c_sqlclient_DuplicateMySQLRecord($ps_tablename,$ps_keyfield,$ps_keyvalue);
//        echo "<br>CLMAIN_U690 new_key".$s_newkey."<br>";
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."  calling u700_set_db_recfields");};
        $s_keysdone = $this->clmain_u700_set_db_recfields("ADD",$ps_tablename,$ps_keyfield,$s_newkey,$ps_params,"NO","clmain_u680",$p_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno);

//gw20110511 - use params
//        $s_newkey = $class_sql->c_sqlclient_DuplicateMySQLRecord("jobs_masters","jm_id",$ps_keyvalue);
////        echo "<br>CLMAIN_U690 new_key".$s_newkey."<br>";
//        $s_keysdone = $this->clmain_u700_set_db_recfields("ADD","jobs_masters","jm_id",$s_newkey,$ps_params,"NO","clmain_u680",$p_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno);
A800_NEXT:

A900_EXIT:

B100_OPTIONS:
B190_END:
    $sys_function_out = $s_newkey;
Z900_EXIT:
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."  z900_exit return value = ".$sys_function_out);};
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_u700_set_db_recfields($ps_action,$ps_tablename,$ps_keyfieldname,$s_newkey,$ps_params,$ps_line_debug,$ps_calledfrom,$p_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
// use the details_def details_data to build the necessare screen

    global $class_sql;

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_outdata = "none";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u700_set_db_recfields - called from - ".$ps_calledfrom."  ";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."  DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name."  ps_debug = ".$ps_debug);};

    $sys_function_out = "";
A100_INIT:
    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;
    $s_set_values = "";
    $s_value_out = "";

    $s_action = strtoupper($ps_action);
B000_SET_PARAMS:
    $s_has_params="NO";
    if (strpos($ps_params,":") === false)
    {
        goto B900_END;
    }
    $s_has_params="YES";

    $ar_params = explode("::",$ps_params);

B900_END:
//get the record from the tablename with the keyfieldname = field value
//see what in the key values and the number fields and set the value to the appropriate value from details_def_db
C_000_DO:
    $s_sql = "SELECT * from ".$ps_tablename." where ".$ps_keyfieldname." = '".$s_newkey."'";

//            echo "<br> clmai  u700  s_sql = ".$s_sql;



    $i_record_count = "0";
    $s_sql = str_replace("#p","|",$s_sql);
    $s_sql = str_replace("#P","|",$s_sql);
    $s_sql = $this->clmain_v200_load_line( $s_sql,$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b400");
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql." ");};

C_000_RECORD_LOOP:
    $result = $class_sql->c_sqlclient_exec_query($p_dbcnx,$s_sql);
    $s_rec_found = "NO";
    $s_numrows = mysql_num_rows($result);

C_100_GET_RECORDS:
    if ($i_record_count > $s_numrows) {
        goto C_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        goto C_900_END_RECORDS;
    }
    $s_rec_found = "YES";
C_150_DO_RECORDS:
    GOTO C_900_END_RECORDS;
C_158_NEXT:
    $i_record_count = $i_record_count + 1;
    GOTO C_100_GET_RECORDS;
C_159_END:
C_900_END_RECORDS:

D_000_DO_FIELDS:
$s_set_values = "";
$s_recordtype = "";
//  ECHO "<BR> GOT THE RECORD<BR>";

        foreach( $row as $key=>$value)
        {
            if ($s_action <> "ADD")
            {
                GOTO B050_END_ADD;
            }
            if (strtolower($key) == "recordtype")
            {
                $s_recordtype = strtoupper($value);
                $s_recordtype = " recordtype = '".str_replace("_TEMPLATE","",$s_recordtype)."',";
            }
            if (strtolower($key) == "createddntime")
            {
                $s_set_values = $s_set_values." ".$key." = '|%!_OSYSVAL_YMDHMSU_!%|',";
                GOTO D890_NEXT;
            }
            if (strtolower($key) == "createduid")
            {
                $s_set_values = $s_set_values." ".$key." = '|%!_COOKIE_ud_logonname_!%|',";
                GOTO D890_NEXT;
            }
            if (strtolower($key) == "createdutime")
            {
                $s_set_values = $s_set_values." ".$key." = '|%!_time()_!%|',";
                GOTO D890_NEXT;
            }
            if (strtolower($key) == "companyid")
            {
                $s_set_values = $s_set_values." ".$key." = '|%!_COOKIE_ud_companyid_!%|',";
                GOTO D890_NEXT;
            }
            if (strtolower($key) == "client_id")
            {
                $s_set_values = $s_set_values." ".$key." = ' ',";
                GOTO D890_NEXT;
            }
B050_END_ADD:
            if (strtolower($key) == "updateddntime")
            {
                $s_set_values = $s_set_values." ".$key." = '|%!_OSYSVAL_YMDHMSU_!%|',";
                GOTO D890_NEXT;
            }
            if (strtolower($key) == "updateduid")
            {
                $s_set_values = $s_set_values." ".$key." = '|%!_COOKIE_ud_logonname_!%|',";
                GOTO D890_NEXT;
            }
            if (strtolower($key) == "updatedutime")
            {
                $s_set_values = $s_set_values." ".$key." = '|%!_time()_!%|',";
                GOTO D890_NEXT;
            }
B060_END_UPDATE:

            if (strpos($key,"_def") !== false)
            {
                goto D100_DO_FIELD;
            }
            if (strpos($key,"_desc") !== false)
            {
                goto D100_DO_FIELD;
            }
            GOTO D890_NEXT;
D100_DO_FIELD:
//            echo "<br> clmai  u700  value = ".$value."  s_action=".$s_action."<br> ";
            if (strtoupper($value) == "NONE")
            {
                GOTO D890_NEXT;
            }
// gw20110509 - on add there are no data structures
            if ($s_action <> "UPDATE")
            {
                GOTO D290_END;
            }
D200_TIDY_VALUE:
            $s_value_out = "|%!_POSTV_DB_".$value."_!%|";
//            echo "<br>  dld s_value_out".$s_value_out."";

            $s_fieldname = $key;
            $s_fieldname = str_replace("_def","",$s_fieldname);
            $s_fieldname = str_replace("_desc","",$s_fieldname);
            if (strpos(strtoupper($s_fieldname),"NUMBER") === FALSE)
            {
                $s_value_out = str_replace("_!%|","_(spaceonerror)_!%|",$s_value_out);
            }else{
                $s_value_out = str_replace("_!%|","_(zeroonerror)_!%|",$s_value_out);
            }
/* GW20110510
            echo "<br> clmai  u700  s_details_def = ".$s_details_def."<br> ";

            ECHO '<BR>';
            ECHO '<BR>';
            echo "<br> clmai  u700  s_details_def = ".$s_details_def."<br> ";
            ECHO '<BR>';
            echo "<br> clmai  u700  s_details_data = ".$s_details_data."<br> ";
            ECHO '<BR>';

            echo "<br> clmai  u700  s_value_out = ".$s_value_out;
*/
            $s_value_out = $this->clmain_v200_load_line( $s_value_out,$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b400");
            if (trim($s_value_out) == "")
            {
//            echo "<br>  end value_out = nothing";
                goto D290_END;
            }
            if (strpos(trim($s_value_out),"^") === false)
            {
//            echo "<br>  end value_out = no ^ ";
                goto D290_END;
            }
            $ar_field_improvers = explode("^",$s_value_out);

            $i_improvers_count = count($ar_field_improvers);
            $i_cnt = 0;
D210_NEXT:
            if ($i_cnt == $i_improvers_count)
            {
//            echo "<br>  coutn end value_out = nothing";
                GOTO D290_END;
            }
            $s_fieldvalue = $ar_field_improvers[$i_cnt];
//            echo "<br>  check out value s_fieldvalue=".$s_fieldvalue."";
            if (strpos(strtoupper($s_fieldvalue),"DEFAULT=") !== false)
            {
                $s_value_out = substr($s_fieldvalue,strpos(strtoupper($s_fieldvalue),"DEFAULT=")+8);
            }
            if (strtoupper($s_value_out) == "SPACE")
            {
                $s_value_out = "";
            }

            if (strpos(strtoupper($s_fieldvalue),"PARAM=") !== false)
            {
                $s_value_out = $ar_params[0];
            }

            //            echo "<br> ar_field_improvers[".$i_cnt."] = ".$ar_field_improvers[$i_cnt];
D280_GET_NEXT:
            $i_cnt = $i_cnt + 1;
            goto D210_NEXT;

D290_END:

A300_SET_FIXED_VALUES:
            $s_value_out = "";
    // check if field is in the param list
            if (strpos($ps_params,$value) === false)
            {
                goto A329_END;
            }
// gw 20120103
/*            $ar_params2 = explode(":is:",$ar_params[0]);
            $i_cnt2 = 0;
A310_NEXT:
            if ($i_cnt2 == count($ar_params2))
            {
                goto A329_END;
            }
            if (strtoupper(trim($value)) == strtoupper(trim($ar_params2[0])))
            {
                $s_value_out = $ar_params2[1];
                goto A329_END;
            }
            $i_cnt2 = $i_cnt2 + 1;
            goto A310_NEXT;
*/

            $i_cnt2 = "0";
A323_NEXT_PARAM:
            if ($i_cnt2 >= count($ar_params))
            {
                goto A323_END;
            }
            $ar_params2 = explode(":is:",$ar_params[$i_cnt2]);
            if ($value == $ar_params2[0])
            {
                $s_value_out = $ar_params2[1];
                goto A323_END;
            };

            $i_cnt2 = $i_cnt2 + 1;
            GOTO A323_NEXT_PARAM;
A323_END:





A329_END:

D700_SET_FIELD:
            $s_fieldname = $key;
            $s_fieldname = str_replace("_def","",$s_fieldname);
            $s_fieldname = str_replace("_desc","",$s_fieldname);
            if (strpos(strtoupper($s_fieldname),"NUMBER") === FALSE)
            {
                $s_set_values = $s_set_values." ".$s_fieldname." = '".$s_value_out."', ";
//                $s_set_values = str_replace("_!%|","_(spaceonerror)_!%|",$s_set_values);
            }else{
                if (trim($s_value_out) == "")
                {
                    $s_value_out = "0";
                }
                $s_set_values = $s_set_values." ".$s_fieldname." = ".$s_value_out.", ";
//                $s_set_values = str_replace("_!%|","_!%|",$s_set_values);
            }


//                        echo "<br>  clmai  u700 key = ".$key." value=".$s_value_out." ********s_set_values=".$s_set_values;
D890_NEXT:
        }

        /*        if ($s_set_values == "")
        {
            goto D_900_END;
        }
*/
//            echo "<br> clmai  u700  s_action = ".$s_action;

            // need to set the key value because the dynamic field list will end with a comma
    $s_sql = "UPDATE ".$ps_tablename." set ".$s_set_values.$s_recordtype." ".$ps_keyfieldname." = '".$s_newkey."'  where ".$ps_keyfieldname." = '".$s_newkey."'";
//            echo "<br> clmai  u700  s_sql = ".$s_sql;

    $s_sql = $this->clmain_v200_load_line( $s_sql,$s_details_def,$s_details_data,"no",$ps_sessionno,"clmain u700");
//            echo "<br><br> clmain u700 s_sql2 = ".$s_sql;
//            echo "<br> clmai  u700  s_sql2 = ".$s_sql;
    $result = $class_sql->c_sqlclient_exec_query($p_dbcnx,$s_sql);
    if (!$result)
    {
//            echo "<br> clmain u700- update error s_sql = ".$s_sql;
    }


//            die ("<br> die u700 d890 ");

D_900_END:

    $sys_function_out = $s_newkey;
Z900_EXIT:
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_u705_set_db_recfields_from_dblob($ps_action,$ps_tablename,$ps_keyfieldname,$s_newkey,$ps_params,$ps_line_debug,$ps_calledfrom,$p_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
// use the details_def details_data to build the necessare screen

    global $class_sql;

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_outdata = "none";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u705_set_db_recfields_from_dblob - called from - ".$ps_calledfrom."  ";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."  DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name."  ps_debug = ".$ps_debug);};

    $sys_function_out = "";
A100_INIT:
    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;
    $s_set_values = "";
    $s_value_out = "";

    $s_action = strtoupper($ps_action);
B000_SET_PARAMS:
    $s_has_params="NO";
    if (trim($ps_params,"=") === false)
    {
        goto B900_END;
    }
    $s_has_params="YES";

    $ar_params = explode("=",$ps_params);
    if ($s_action == "UPDATEAFIELD")
    {
        $s_passed_fieldname = strtoupper(trim($ar_params[0]));
        $s_passed_db_name = strtoupper(trim($ar_params[1]));
    }

B900_END:
//get the record from the tablename with the keyfieldname = field value
//see what in the key values and the number fields and set the value to the appropriate value from details_def_db
C_000_DO:
    $s_sql = "SELECT * from ".$ps_tablename." where ".$ps_keyfieldname." = '".$s_newkey."'";

//            echo "<br> clmai  u705  s_sql = ".$s_sql;



    $i_record_count = "0";
    $s_sql = str_replace("#p","|",$s_sql);
    $s_sql = str_replace("#P","|",$s_sql);
    $s_sql = $this->clmain_v200_load_line( $s_sql,$s_details_def,$s_details_data,"no",$ps_sessionno,"clm u705 c000");
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql." ");};

C_000_RECORD_LOOP:
    $result = $class_sql->c_sqlclient_exec_query($p_dbcnx,$s_sql);
    $s_rec_found = "NO";
    $s_numrows = mysql_num_rows($result);
    if ($s_numrows == 0)
    {
        echo "<br>clmain u705 c000  Unable to locate records <br>s_sql".$s_sql."<br>";
        goto D_900_END;
    }

C_100_GET_RECORDS:
    if ($i_record_count > $s_numrows) {
        goto C_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        goto C_900_END_RECORDS;
    }
    $s_rec_found = "YES";
C_150_DO_RECORDS:
    GOTO C_900_END_RECORDS;
C_158_NEXT:
    $i_record_count = $i_record_count + 1;
    GOTO C_100_GET_RECORDS;
C_159_END:
C_900_END_RECORDS:

D_000_DO_FIELDS:
$s_set_values = "";
$s_recordtype = "";
//  ECHO "<BR> GOT THE RECORD<BR>";

        foreach( $row as $key=>$value)
        {
            if ($s_action <> "ADD")
            {
                GOTO B050_END_ADD;
            }
            if (strtolower($key) == "recordtype")
            {
                $s_recordtype = strtoupper($value);
                $s_recordtype = " recordtype = '".str_replace("_TEMPLATE","",$s_recordtype)."',";
            }
            if (strtolower($key) == "createddntime")
            {
                $s_set_values = $s_set_values." ".$key." = '|%!_OSYSVAL_YMDHMSU_!%|',";
                GOTO D890_NEXT;
            }
            if (strtolower($key) == "createduid")
            {
                $s_set_values = $s_set_values." ".$key." = '|%!_COOKIE_ud_logonname_!%|',";
                GOTO D890_NEXT;
            }
            if (strtolower($key) == "createdutime")
            {
                $s_set_values = $s_set_values." ".$key." = '|%!_time()_!%|',";
                GOTO D890_NEXT;
            }
            if (strtolower($key) == "companyid")
            {
                $s_set_values = $s_set_values." ".$key." = '|%!_COOKIE_ud_companyid_!%|',";
                GOTO D890_NEXT;
            }
            if (strtolower($key) == "client_id")
            {
                $s_set_values = $s_set_values." ".$key." = ' ',";
                GOTO D890_NEXT;
            }
B050_END_ADD:
            if (strtolower($key) == "updateddntime")
            {
                $s_set_values = $s_set_values." ".$key." = '|%!_OSYSVAL_YMDHMSU_!%|',";
                GOTO D890_NEXT;
            }
            if (strtolower($key) == "updateduid")
            {
                $s_set_values = $s_set_values." ".$key." = '|%!_COOKIE_ud_logonname_!%|',";
                GOTO D890_NEXT;
            }
            if (strtolower($key) == "updatedutime")
            {
                $s_set_values = $s_set_values." ".$key." = '|%!_time()_!%|',";
                GOTO D890_NEXT;
            }
B060_END_UPDATE:
            if ($s_action == "UPDATEAFIELD")
            {
                GOTO D290_END;
            }

            if (strpos($key,"_def") !== false)
            {
                goto D100_DO_FIELD;
            }
            if (strpos($key,"_desc") !== false)
            {
                goto D100_DO_FIELD;
            }
            GOTO D890_NEXT;
D100_DO_FIELD:
//            echo "<br> clmai  u705  value = ".$value."  s_action=".$s_action."<br> ";
            if (strtoupper($value) == "NONE")
            {
                GOTO D890_NEXT;
            }
            if (strtoupper($value) == "UNCHANGED")
            {
                GOTO D890_NEXT;
            }
            if (strtoupper($value) == "TTA")  // 20120220 - this record value needs to be set regardless of the datablob value
            {
                GOTO D890_NEXT;
            }
D290_END:

D400_SET_FROM_DATA_BLOB:
    $s_fieldname = $key;
    $s_format = "NONE";
    if (isset($row["data_blob"]) === false)
    {
        goto D490_END;
    }
    // get the value from the data_blob
    if ($s_action == "UPDATEAFIELD")
    {
        $s_fieldname = $s_passed_fieldname;
        $value = $s_passed_db_name;
    }
    if (strpos($value,"(") !== false)
    {
//gw20110602 -nottesting
        $s_format = substr($value,strpos($value,"("));
        $value = substr($value,1,strpos($value,"("));
    }
        //echo "<br> clmain u705 data blob value before get xml s_fieldname=".$s_fieldname." value=".$value." s_value_out=".$s_value_out."<br>";
    if (strpos($value,"+") !== false)
    {
//        echo "<br> says it has a + value".$value;
        GOTO D420_MULTI_FIELDS;
    }
//        echo "<br> says it does not have a + value".$value;
    $s_value_out = $this->clmain_u592_get_field_from_xml($row["data_blob"],strtolower($value),"NO","clmain u705 d4","");
    goto D490_END;
//echo "<br> clmain u705 data blob after value s_fieldname=".$s_fieldname." value=".$value." s_value_out=".$s_value_out."<br>";
D420_MULTI_FIELDS:
// EXPLODE BASED ON +
// FOR EACH VALUE GET FROM DATA BLOB
// ADD TO SVALUE_OUT
    $ar_fields = explode("+",$value);

    $i_fields_count = count($ar_fields);
    $i_cnt = 0;
    $s_value_out = "";
D425_NEXT:
    if ($i_cnt >= $i_fields_count)
    {
        GOTO D429_END;
    }
    $value = $ar_fields[$i_cnt];
    $s_value_out = $s_value_out.$this->clmain_u592_get_field_from_xml($row["data_blob"],strtolower($value),"NO","clmain u705 d5",$value);
D427_GET_NEXT:
    $i_cnt = $i_cnt + 1;
    goto D425_NEXT;
D429_END:

//    die("<br>######### done the concat value for a key value=".$value."  value_out=".$s_value_out."   ##################<br>)");


    goto D490_END;


D490_END:

D700_SET_FIELD:
//gw20110602 -nottesting
            if ($s_format =="NONE")
            {
                GOTO d710_skip_fmat;
            }
            if ($s_format =="STRTOTIME")
            {
                $s_value_out = strtotime($s_value_out);
                GOTO d710_skip_fmat;
            }
//gw 20110602            $s_fieldname = $key;
d710_skip_fmat:
            $s_fieldname = str_replace("_def","",$s_fieldname);
            $s_fieldname = str_replace("_desc","",$s_fieldname);
            if (strpos(strtoupper($s_fieldname),"NUMBER") === FALSE)
            {
                $s_set_values = $s_set_values." ".$s_fieldname." = '".$s_value_out."', ";
//                $s_set_values = str_replace("_!%|","_(spaceonerror)_!%|",$s_set_values);
            }else{
                if (trim($s_value_out) == "")
                {
                    $s_value_out = "0";
                }

//                echo "<br> checking the s_fieldname = ".$s_fieldname." with value = ".$s_value_out;

                if (trim(strtolower($value)) == "scheduled_for_dnt")
                {
                    $s_value_out = str_replace("-","",$s_value_out);
                    $s_value_out = str_replace(":","",$s_value_out);
                    $s_value_out = str_replace("T","",$s_value_out);
                    $s_value_out = substr($s_value_out,0,8);
                }

                if (is_numeric($s_value_out) === false)
                {
                    $s_value_out = "0";
                }
                $s_set_values = $s_set_values." ".$s_fieldname." = ".$s_value_out.", ";
//                $s_set_values = str_replace("_!%|","_!%|",$s_set_values);
            }


//                        echo "<br>  clmai  u705 key = ".$key." value=".$s_value_out." ********s_set_values=".$s_set_values;
D890_NEXT:
        if ($s_action == "UPDATEAFIELD")
        {
            IF (strtoupper(trim($key)) == strtoupper(trim($s_passed_fieldname)))
            {
                BREAK;
            }
        }
        }

        /*        if ($s_set_values == "")
        {
            goto D_900_END;
        }
*/
//            echo "<br> clmai  u705  s_action = ".$s_action;
// need to set the key value because the dynamic field list will end with a comma
    $s_sql = "UPDATE ".$ps_tablename." set ".$s_set_values.$s_recordtype." ".$ps_keyfieldname." = '".$s_newkey."'  where ".$ps_keyfieldname." = '".$s_newkey."'";
//            echo "<br> clmai  u705  s_sql = ".$s_sql;

    $s_sql = $this->clmain_v200_load_line( $s_sql,$s_details_def,$s_details_data,"no",$ps_sessionno,"clmain u705");
//            echo "<br><br> clmain u705 s_sql2 = ".$s_sql;
    $result = $class_sql->c_sqlclient_exec_query($p_dbcnx,$s_sql);
    if (!$result)
    {
//            echo "<br> clmain u705- update error s_sql = ".$s_sql;
    }
D_900_END:

    $sys_function_out = $s_newkey;
Z900_EXIT:
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_u710_draw_db_maint_errorchecking($ps_tablename, $ps_recordgroup, $ps_recordtype,  $ps_params, $ps_line_debug, $ps_calledfrom,$p_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
// use the details_def details_data to build the necessare screen

$sys_function_out = 'if (document.thisform.DB_phasefrom.value == ""){error_list = error_list+"id error"+"\n\r";}';
$sys_function_out = $sys_function_out.'if (document.thisform.DB_phaseto.value == ""){error_list = error_list+"id error"+"\n\r";}';
//goto Z900_EXIT;

    global $class_sql;

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_outdata = "none";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u710_draw_db_maint_errorchecking - called from - ".$ps_calledfrom."  ";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."  DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name."  ps_debug = ".$ps_debug);};

    $sys_function_out = "";
A100_INIT:

B000_SET_PARAMS:
    $s_has_params="NO";
    if (strpos($ps_params,":") === false)
    {
        goto B900_END;
    }
    $s_has_params="YES";

    $ar_params = explode("::",$ps_params);

B900_END:
//get the record from the tablename with the keyfieldname = field value
//see what in the key values and the number fields and set the value to the appropriate value from details_def_db
C_000_DO:
    $s_sql = "SELECT * from ".$ps_tablename." where recordgroup = '".$ps_recordgroup."' and recordtype = '".$ps_recordtype."'";
// ECHO "<BR> GOT THE RECORD s_sql = ".$s_sql."<BR>";
    $i_record_count = "0";
//    $s_sql = str_replace("#p","|",$s_sql);
//    $s_sql = str_replace("#P","|",$s_sql);
//    $s_sql = $this->clmain_v200_load_line( $s_sql,$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b400");
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql." ");};

C_000_RECORD_LOOP:
    $result = $class_sql->c_sqlclient_exec_query($p_dbcnx,$s_sql);
    $s_rec_found = "NO";
    $s_numrows = mysql_num_rows($result);

C_100_GET_RECORDS:
    if ($i_record_count > $s_numrows) {
        goto C_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        goto C_900_END_RECORDS;
    }
    $s_rec_found = "YES";
C_150_DO_RECORDS:
// ECHO "<BR> GOT THE RECORD s_sql = ".$s_sql."<BR>";
    GOTO C_900_END_RECORDS;
C_158_NEXT:
    $i_record_count = $i_record_count + 1;
    GOTO C_100_GET_RECORDS;
C_159_END:
C_900_END_RECORDS:

D_000_DO_FIELDS:
$s_data_blob = $row["data_blob"];
// ECHO "<BR> GOT THE RECORD<BR>";
//ECHO "<BR>".$s_data_blob."<BR>";
// ECHO "<BR> GOT THE RECORD<BR>";
    $ar_xml = $s_data_blob;
    $ar_xml = simplexml_load_string($ar_xml);
    $newArry = array();
    $ar_xml = (array) $ar_xml;
    foreach ($ar_xml as $key => $value)
    {
        if (trim($value) =="")
        {
            $value = " ";
        }

         if (strpos($value,"^") === false)
        {
            goto D290_END;
        }
        $ar_field_improvers = explode("^",$value);

        $i_improvers_count = count($ar_field_improvers);
        $i_cnt = 0;
D210_NEXT:
        if ($i_cnt == $i_improvers_count)
        {
            GOTO D290_END;
        }
        $s_fieldvalue = $ar_field_improvers[$i_cnt];
        if (strpos(strtoupper($s_fieldvalue),"REQUIRED=YES") === false)
        {
            GOTO D280_GET_NEXT;
        }
        $s_error_message = $key;
        if (strpos(strtoupper($s_fieldvalue),",") !== false)
        {
            $s_error_message = substr($s_fieldvalue,13);
        }

        $sys_function_out = $sys_function_out.'if (document.thisform.DB_'.$key.'.value == ""){error_list = error_list+"'.$s_error_message.'"+"\n\r";}';
D280_GET_NEXT:
        $i_cnt = $i_cnt + 1;
        goto D210_NEXT;
D290_END:
 }

D_900_END:

Z900_EXIT:
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_u720_do_process($ps_tablename, $ps_keyfield,$ps_keyvalue,$ps_processid, $ps_ataction,   $ps_line_debug, $ps_calledfrom,$p_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
// use the details_def details_data to build the necessare screen
    global $class_sql;
    global $class_clprocessjobline;


    $sys_debug_text = "";
    $sys_debug = "";
    $sys_outdata = "none";

    $sys_debug = strtoupper($ps_debug);

//    $sys_debug = "GWTEST";


    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = "<br> debug at function call ".$ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    IF ($sys_debug =="NO") {
        $sys_debug = strtoupper($ps_line_debug);
        IF ($sys_debug !="NO") {
            $sys_debug  = "YES";
            $sys_debug_text = "<br>".$ps_line_debug;
        }
    }
    $sys_function_name = "debug - clmain_u720_do_process - called from - ".$ps_calledfrom."  ";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){echo $sys_debug_text."  called ".$sys_function_name."  DEBUG IS WORKING";};
    IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  ps_debug = ".$ps_debug;};
    IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name.",ps_tablename, ps_keyfield,ps_keyvalue,ps_processid, ps_ataction,   ps_line_debug, ps_calledfrom,p_dbcnx,ps_debug,ps_details_def,ps_details_data,ps_sessionno";};
    IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name.",".$ps_tablename." , ".$ps_keyfield." , ".$ps_keyvalue." , ".$ps_processid." , ".$ps_ataction." , ".$ps_line_debug." , ".$ps_calledfrom." , p_dbcnx, ".$ps_debug." , ps_details_def,ps_details_data,".$ps_sessionno;};

    $sys_function_out = "";
A100_INIT:
    $s_error_messages = "";
    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;
    $s_keyvalue = $ps_keyvalue;
    $s_keyvalue = str_replace("'","",$s_keyvalue);

    $s_contract_id = "";
    $s_job_type = "";
    $s_job_phase = "notset";
    $s_job_worktype = "noworktype_value_set";
// gw 20110802 - neeed to work out how to set the job_phase_to
    $s_job_phase_to = "notset";
    $s_process_done = "NO";
    $s_error_note = "";
    $s_worktype_only = "NO";
    // gw 20110729 get the job details
// get the contract_id and job_type to make the worktype
B_000_DO:
    $s_contract_id = $ps_keyfield;
    if (!strpos(strtoupper($s_contract_id),"=WORKTYPE")=== false)
    {
        $s_worktype_only =  "YES";
        $s_contract_id = str_replace("=WORKTYPE","",$s_contract_id);
    }

    if (strtoupper(trim($s_keyvalue)) == "NONE")
    {
        goto B_900_END_RECORDS;
    }
    if (strtoupper(trim($s_keyvalue)) == "NO_JOB_ID")
    {
        $s_keyvalue = "NONE";
        goto B_900_END_RECORDS;
    }
    if ($ps_keyvalue == "none")
    {
        $s_keyvalue = "NONE";
        goto B_900_END_RECORDS;
    }

    $s_sql = "SELECT * from jobs where jobs_id = '".$ps_keyvalue."'";
    IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." b_000_do GOT THE RECORD s_sql = ".$s_sql."";};

    $i_record_count = "0";

    $s_sql = str_replace("#p","|",$s_sql);
    $s_sql = str_replace("#P","|",$s_sql);
    $s_sql = $this->clmain_v200_load_line( $s_sql,$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b400");
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql." ");};
    $result = $class_sql->c_sqlclient_exec_query($p_dbcnx,$s_sql);
    $s_reB_found = "NO";

    B_000_RECORD_LOOP:
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
        ECHO "<BR> clmain u720 no records found for s_sql=".$s_sql."<br>";
        $s_error_messages =    $s_error_messages." Unable to locate the job record for s_sql=".$s_sql."<br>";
    }

B_100_GET_RECORDS:
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";
B_150_DO_RECORDS:
    IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  s_sql = ".$s_sql."<BR>key1=".$row['key1']."  key2 =".$row['key2']." key3= ".$row['key3']." key4= ".$row['key4'];};
    $s_contract_id = $this->clmain_u592_get_field_from_xml($row["data_blob"],"contract_id","NO","clmain_u720"," ");
    $s_job_type = $this->clmain_u592_get_field_from_xml($row["data_blob"],"job_type","NO","clmain_u720"," ");
    $s_job_phase = $this->clmain_u592_get_field_from_xml($row["data_blob"],"phase","NO","clmain_u720"," ");
    $s_job_worktype = $this->clmain_u592_get_field_from_xml($row["data_blob"],"worktype","NO","clmain_u720","noworktype_value_set");
 B_158_NEXT:
    $i_record_count = $i_record_count + 1;
    GOTO B_100_GET_RECORDS;
B_159_END:
B_900_END_RECORDS:

//get the record from the tablename with the keyfieldname = field value
//see what in the key values and the number fields and set the value to the appropriate value from details_def_db
C_000_DO:
    $s_attempt ="first";
    $s_processid = strtolower(trim($ps_processid));

    $s_sql_group_and_type = "  and  (  recordgroup = 'WORKTYPE' and recordtype = 'ACTIONS') ";
//GW20110921 - ADDED THE DEFAUTL ON KEY1 or the process would pickup all records for the action regardlesss of the contraxct
    $s_sql = "SELECT * from ".$ps_tablename." where key1  = 'DEFAULTS' and  key2  = '".$ps_processid."' and key3 = '".$ps_ataction."' ".$s_sql_group_and_type." order by   key4";

    if (strtoupper(trim($s_keyvalue)) <> "NONE")
    {
        $s_sql = "SELECT * from ".$ps_tablename." where key1  = '".$s_contract_id."_".$s_job_type."' and key2  = '".$ps_processid."' and key3 = '".$ps_ataction."'".$s_sql_group_and_type." order by key4";
        if ($s_job_worktype <> "noworktype_value_set")
        {
            $s_sql = "SELECT * from ".$ps_tablename." where key1  = '".$s_job_worktype."' and key2  = '".$ps_processid."' and key3 = '".$ps_ataction."'".$s_sql_group_and_type." order by key4";
        }
    }
    if ($s_worktype_only ==  "YES")
    {
        $s_sql = "SELECT * from ".$ps_tablename." where key1  = '".$s_contract_id."' and key2  = '".$ps_processid."' and key3 = '".$ps_ataction."'".$s_sql_group_and_type." order by key4";
    }

    $s_error_messages = " Doing First attempt - No process flow for s_sql=".$s_sql."<br>";

C_020_TRY_AGAIN:

    IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." C_020_TRY_AGAIN  GOT THE RECORD s_sql = ".$s_sql."";};
    IF ($sys_debug == "YES"){
        $s_temp1 = "<tr><td>|%!_DETAILS_DEF_DUMP_!%|<td><tr>";
        $s_temp1 = $this->clmain_v200_load_line( $s_temp1,$s_details_def,$s_details_data,"no",$ps_sessionno,"C_020_TRY_AGAIN");
        echo $sys_debug_text."=".$sys_function_name." C_020_TRY_AGAIN  GOT s_temp1 = ".$s_temp1."";
    };

    $i_record_count = "0";
//gw20110526 - next 3 were commented
    $s_sql = str_replace("#p","|",$s_sql);
    $s_sql = str_replace("#P","|",$s_sql);
    $s_sql = $this->clmain_v200_load_line( $s_sql,$s_details_def,$s_details_data,"no",$ps_sessionno,"C_020_TRY_AGAIN");
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql." ");};

C_000_RECORD_LOOP:
    $result = $class_sql->c_sqlclient_exec_query($p_dbcnx,$s_sql);
    $s_rec_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows <> 0)
    {
       goto C_100_GET_RECORDS;
    }
// gw 201111    echo "<br> ** ERROR GWREF:CLM_U720c000  s_sql=".$s_sql." s_attempt=".$s_attempt." ";

//    DIE ("GW DIED: CLMAIN C_000_RECORD_LOOP");
/* gw 20111110 -  MOVED TO THE CASCADE SECTION
    if ($s_attempt == "first")
    {
        $s_error_messages =    $s_error_messages." First attempt - No process flow for s_sql=".$s_sql."<br>";
        if ($s_processid == "delay")
        {
            $s_sql = "SELECT * from ".$ps_tablename." where key1  = '".$s_contract_id."_DEFAULTS'  and key2  = '".$ps_processid."' and key3 = '".$ps_ataction."'".$s_sql_group_and_type." order by key4";
        }else{
            $s_sql = "SELECT * from ".$ps_tablename." where key1  = 'DEFAULTS' and key2  = '".$ps_processid."' and key3 = '".$ps_ataction."'".$s_sql_group_and_type." order by key4";
        }
        $s_attempt ="second";
        $s_error_messages =   $s_error_messages." Second attempt - No process flow for s_sql=".$s_sql."<br>";
        GOTO C_000_RECORD_LOOP;
    }
*/
C_030_DO_CASCADE:
/* // gw 20111110 - checking wrong field
    if ($s_processid == "delay")
    {
        GOTO C_031_DO_CHECKING;
    }
    if ($s_processid == "csr_activities")
    {
        GOTO C_031_DO_CHECKING;
    }
    if ($s_processid == "repaired")
    {
        GOTO C_031_DO_CHECKING;
    }
    if ($s_processid == "udelivered")
    {
        GOTO C_031_DO_CHECKING;
    }

$ps_ataction

*/
/* gw 20111110 - everything cascased from now on
    if ($ps_ataction == "delay")
    {
        GOTO C_031_DO_CHECKING;
    }
    if ($ps_ataction == "csr_activities")
    {
        GOTO C_031_DO_CHECKING;
    }
    if ($ps_ataction == "repaired")
    {
        GOTO C_031_DO_CHECKING;
    }
    if ($ps_ataction == "udelivered")
    {
        GOTO C_031_DO_CHECKING;
    }

//    echo "<br> ** ERROR GWREF:CLM_U720c000a  s_sql=".$s_sql." s_processid=[]".$s_processid."[]  ps_ataction=".$ps_ataction,"[";

    GOTO C_039_SKIP_CASCADE;
*/

C_031_DO_CHECKING:
    if ($s_attempt == "first")
    {
        $s_error_messages =    $s_error_messages." First attempt - No process flow for s_sql=".$s_sql."<br>";
        $s_sql = "SELECT * from ".$ps_tablename." where key1  = '".$s_contract_id."_DEFAULTS'  and key2  = '".$ps_processid."' and key3 = '".$ps_ataction."'".$s_sql_group_and_type." order by key4";


        $s_attempt ="second";
        $s_error_messages =   $s_error_messages." Second attempt - No process flow for s_sql=".$s_sql."<br>";
        GOTO C_000_RECORD_LOOP;
    }
    if ($s_attempt == "second")
    {
        $s_attempt ="third";
        $s_worktype = $s_contract_id."_".$s_job_type;
        if(trim($s_job_type) == "")
        {
                $s_worktype = $s_contract_id;
        }
        if ($s_job_worktype <> "noworktype_value_set")
        {
           $s_worktype = $s_job_worktype;
        }
        $s_sql = "SELECT * from ".$ps_tablename." where key1  = '".$s_worktype."'  and key2  = '".$ps_processid."' and key3 = 'DEFAULTS' ".$s_sql_group_and_type." order by key4";
        $s_error_messages =    $s_error_messages." third attempt - No process flow for s_sql=".$s_sql."<br>";
        GOTO C_000_RECORD_LOOP;
    }

    if ($s_attempt == "third")
    {
        $s_attempt ="forth";
        $s_sql = "SELECT * from ".$ps_tablename." where key1  = '".$s_contract_id."_DEFAULTS' and key2  = '".$ps_processid."' and key3 = 'DEFAULTS' ".$s_sql_group_and_type." order by key4";
        $s_error_messages =    $s_error_messages." forth attempt - No process flow for s_sql=".$s_sql."<br>";
        GOTO C_000_RECORD_LOOP;
    }
    if ($s_attempt == "forth")
    {
        $s_attempt ="fifth";
        $s_sql = "SELECT * from ".$ps_tablename." where key1  = 'DEFAULTS' and key2  = '".$ps_processid."' and key3 = '".$ps_ataction."' ".$s_sql_group_and_type." order by key4";
        $s_error_messages =    $s_error_messages." fifth attempt - No process flow for s_sql=".$s_sql."<br>";
        GOTO C_000_RECORD_LOOP;
    }
    if ($s_attempt == "fifth")
    {
        $s_attempt ="sixth";
        $s_sql = "SELECT * from ".$ps_tablename." where key1  = 'DEFAULTS' and key2  = '".$ps_processid."' and key3 = 'DEFAULTS' ".$s_sql_group_and_type." order by key4";
        $s_error_messages =    $s_error_messages." sixth attempt - No process flow for s_sql=".$s_sql."<br>";
        GOTO C_000_RECORD_LOOP;
    }
C_039_SKIP_CASCADE:

C_100_ERROR_MESSAGE:
    echo "<br> ###############################################################################################################";
    echo "<br> ** ERROR REF:CLM_U720c000  No Process flow for this process";
    ECHO "<BR> Error References: ";
    ECHO "<BR>      Table: []".$ps_tablename."[]";
    ECHO "<BR>      keyfield: []".$ps_keyfield."[]";
    ECHO "<BR>      keyvalue: []".$ps_keyvalue."[] Note: NONE indicates no lookup of the job record";


    ECHO "<BR>      processid: []".$ps_processid."[]";
    ECHO "<BR>      ataction: []".$ps_ataction."[]";
    ECHO "<BR>      calledfrom: []".$ps_calledfrom."[]";

    ECHO "<BR>      worktype: []".$s_worktype."[]";
    ECHO "<BR>      contract_id: []".$s_contract_id."[]";
    ECHO "<BR>      job_type: []".$s_job_type."[]";
    ECHO "<BR>      job_worktype: []".$s_job_worktype."[]";

    ECHO "<BR> clmain u720 no records found for s_sql=".$s_sql."<br>";
    ECHO "<BR> Error details <br>";

    Echo $s_error_messages ;
    ECHO "<BR> End of error details <br>";

    if ($ps_processid == "udelivered")
    {
// make an error log somewhere for paul to check
        $this->z903_dump(" ** ERROR REF:CLM_U720c000  No Process flow for this process <BR> Error References: Table: ".$ps_tablename."  keyfield: ".$ps_keyfield."      processid: ".$ps_processid."    ataction: ".$ps_ataction." calledfrom: ".$ps_calledfrom."");
        $this->z903_dump("u720 - no action for ".$s_sql );
        GOTO C_100_GET_RECORDS;
    }

    die ( "The process had been died - Notify Solution Adminstrations");

C_100_GET_RECORDS:
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto C_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto C_900_END_RECORDS;
    }
    $s_rec_found = "YES";
C_150_DO_RECORDS:
    $s_do_debug = $this->clmain_u592_get_field_from_xml($row["data_blob"],"name","NO","clmain_u720","nodefault");
    if (strpos(strtoupper(trim($s_do_debug)),"*DODEBUG*") === false)
    {
        $s_do_debug = "";
    }else{
        $s_do_debug = "<br>dodebug set by line name=".$s_do_debug;
    }


    if ($s_do_debug <> ""){
        echo "<br> ".$s_do_debug;
        echo "<br> ###############################################################################################################";
        echo "<br> ** ERROR REF:CLM_U720c000  No Process flow for this process";
        ECHO "<BR> Error References: ";
        ECHO "<BR>      Table: []".$ps_tablename."[]";
        ECHO "<BR>      keyfield: []".$ps_keyfield."[]";
        ECHO "<BR>      keyvalue: []".$ps_keyvalue."[]";
        ECHO "<BR>      processid: []".$ps_processid."[]";
        ECHO "<BR>      ataction: []".$ps_ataction."[]";
        ECHO "<BR>      calledfrom: []".$ps_calledfrom."[]";

        ECHO "<BR> clmain u720 no records found for s_sql=".$s_sql."<br>";
        ECHO "<BR> Error details <br>";

        Echo $s_error_messages ;
        ECHO "<BR> End of error details <br>";
    }





    if ($s_do_debug <> ""){ECHO "<BR> ".$s_do_debug."  GOT THE RECORD s_sql = ".$s_sql."<BR>key1=".$row['key1']."  key2 =".$row['key2']." key3= ".$row['key3']." key4= ".$row['key4']."";}
    IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  s_sql = ".$s_sql."<BR>key1=".$row['key1']."  key2 =".$row['key2']." key3= ".$row['key3']." key4= ".$row['key4'];};
    if ($s_do_debug <> ""){        ECHO "<BR>  ".$s_do_debug." GOT THE RECORD data_blob = ".$row['data_blob']."";}
C_150_A:
    if ($s_job_phase == "notset")
    {
        goto C_150_SKIP_PHASE;
    }
    $s_phasefrom = $this->clmain_u592_get_field_from_xml($row["data_blob"],"phasefrom","NO","clmain_u720","nodefault");
    if (strtoupper(trim($s_phasefrom)) == "")
    {
        $s_phasefrom = "ALL";
    }
    if (strtoupper(trim($s_phasefrom)) == "ALL")
    {
        goto C_150_B;
    }
    if (strtoupper(trim($s_phasefrom)) <> strtoupper(trim($s_job_phase)))
    {
         $s_error_note = $s_error_note."<br> failed phase from check: record phase=(".(strtoupper(trim($s_phasefrom)).") Job phase=(".strtoupper(trim($s_job_phase))).")";
         goto C_158_NEXT;
    }
C_150_B:
    if ($s_job_phase_to == "notset")
    {
        goto C_150_SKIP_PHASE;
    }
    $s_phaseto = $this->clmain_u592_get_field_from_xml($row["data_blob"],"phaseto","NO","clmain_u720","nodefault");
    if (strtoupper(trim($s_phaseto)) == "ALL")
    {
        goto C_150_C;
    }
    if (strtoupper(trim($s_phaseto)) <> strtoupper(trim($s_job_phase)))
    {
        $s_error_note = $s_error_note."<br> failed phase to check: record phase=(".(strtoupper(trim($s_phaseto)).") Job phase=(".strtoupper(trim($s_job_phase))).")";
        goto C_158_NEXT;
    }
C_150_C:
C_150_SKIP_PHASE:
    $s_dowhat = $this->clmain_u592_get_field_from_xml($row["data_blob"],"dowhat","NO","clmain_u720","nodefault");

    if ($s_do_debug <> ""){ECHO "<BR>   ".$s_do_debug."C_150_SKIP_PHASE GOT THE RECORD dowhat = ".$s_dowhat."";}
    IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."   dowhat = ".$s_dowhat;};

     $s_dowhat = str_replace("^","|",$s_dowhat);
     $s_dowhat = str_replace("#add_pipe#","|",$s_dowhat);
     $s_dowhat = str_replace("#start_field#","#p%!",$s_dowhat);
     $s_dowhat = str_replace("#end_field#","!%#p",$s_dowhat);
     $s_dowhat = str_replace("#equal_sign#","=",$s_dowhat);
     $s_dowhat = str_replace("#quote# ",'"',$s_dowhat);
     $s_dowhat = str_replace("#quote#",'"',$s_dowhat);
     $s_dowhat = str_replace("#carat#",'^',$s_dowhat);


    IF ($sys_debug == "YES"){ECHO "<BR>   ".$s_do_debug." GOT THE RECORD dowhat = ".$s_dowhat."<BR>";};
    IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."   dowhat = ".$s_dowhat;};

    $xml_arr  ="";
    if ($s_do_debug <> "")
    {
        ECHO "<BR>  ".$s_do_debug."C_150_SKIP_PHASE-2 GOT THE RECORD dowhat = ".$s_dowhat." ";
    }

    if (!strpos(strtoupper(trim($s_do_debug)),"*DODEBUG*DIE*") === false)
    {
        ECHO "<BR>  ".$s_do_debug." ############### PROCESS IS SET TO DIE BEFORE RUNNING THE DO_WHAT";
        DIE ("<BR>  process died clmain u720 - configured to die before running the command");
    }

    IF ($sys_debug == "YES"){
        $s_temp1 = "<tr><td>|%!_DETAILS_DEF_DUMP_!%|<td><tr>";
        $s_temp1 = $this->clmain_v200_load_line( $s_temp1,$s_details_def,$s_details_data,"no",$ps_sessionno,"C_020_TRY_AGAIN");
        echo $sys_debug_text."=".$sys_function_name." C_150_SKIP_PHASE pre clprocessjobline  GOT s_temp1 = ".$s_temp1."";
    };

    $s_temp = $class_clprocessjobline->pf_clprocessjobline($p_dbcnx,$s_dowhat,$s_details_def,$s_details_data,$xml_arr,$ps_sessionno,"NO");


    if ($s_do_debug <> "")
    {
        ECHO "<BR>  ".$s_do_debug." GOT THE RECORD s_temp = ".$s_temp." ";
    }
    if (strpos($s_temp,"|^%##%^|") !== false)
    {
//     echo "<br> do the split import clmain results=".$s_temp."<BR> ";
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
    }

 //ECHO "<BR> GOT THE RECORD s_details_def = ".$s_details_def."";
 //ECHO "<BR> GOT THE RECORD s_details_data = ".$s_details_data."";

    if ($s_do_debug <> "")
    {
        ECHO "<BR><BR>  ";
    }
    $s_process_done = "YES";
// gw 20111128 - do_url is the last thing to run - NOT TESTED
// JOB PROCESS NEEDS TO SET THE RETURN VALUE TO STOP_ACTIONS IF THE DO_WHAT IS STOP_ACTION OR DO_URL_ACTION ****** NOTE THE IF PROCESSING ON BOTH
//    if ($s_temp = "STOP_ACTIONS")
//    {
//        GOTO C_900_END_RECORDS;
//    }
C_158_NEXT:
    $i_record_count = $i_record_count + 1;
    GOTO C_100_GET_RECORDS;
C_159_END:
C_900_END_RECORDS:


    IF ($s_process_done <> "YES")
    {
        echo "<br> ** ERROR REF:CLM_U720c900  No Process flow for this combination ";
        ECHO "<BR> Error References: ";
        ECHO "<BR>      Table: ".$ps_tablename."";
        ECHO "<BR>      keyfield: ".$ps_keyfield."";
        ECHO "<BR>      processid: ".$ps_processid."";
        ECHO "<BR>      ataction: ".$ps_ataction."";
        ECHO "<BR>      calledfrom: ".$ps_calledfrom."";
        ECHO "<BR>      job phase: ".$s_job_phase."";
        ECHO "<BR>      job_phase_to: ".$s_job_phase_to."";
        ECHO "<BR>      s_error_note: ".$s_error_note."";

        ECHO "<BR> clmain u720 no records found for s_sql=".$s_sql."<br>";
        if ($ps_processid == "udelivered")
        {
    // make an error log somewhere for paul to check
            $this->z903_dump(" ** ERROR REF:CLM_U720c000  No Process flow for this process <BR> Error References: Table: ".$ps_tablename."  keyfield: ".$ps_keyfield."      processid: ".$ps_processid."    ataction: ".$ps_ataction." calledfrom: ".$ps_calledfrom."");
            $this->z903_dump("u720 - no action for ".$s_sql );
        }else{
            die ( "The process had been died - Call system support");
        }

    }
// do what needs to be done for the job


D_000:
Z900_EXIT:
     $sys_function_out = $s_details_def."|^%##%^|".$s_details_data;
    //echo "<br>clmain b100 in u720  <br>sys_function_out<br>".$sys_function_out."<br><br>";

     return $sys_function_out;
}
//##########################################################################################################################################################################
//gw20121109 - not sure why this exists
//  lots of difference to 720 basically around the worktype
function clmain_u722_do_processv2($ps_tablename, $ps_keyfield,$ps_keyvalue,$ps_processid, $ps_ataction,   $ps_line_debug, $ps_calledfrom,$p_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
// use the details_def details_data to build the necessare screen

    global $class_sql;
    global $class_clprocessjobline;

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_outdata = "none";

    $sys_debug = strtoupper($ps_debug);

//    $sys_debug = "GWTEST";


    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = "<br> debug at function call ".$ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    IF ($sys_debug =="NO") {
        $sys_debug = strtoupper($ps_line_debug);
        IF ($sys_debug !="NO") {
            $sys_debug  = "YES";
            $sys_debug_text = "<br>".$ps_line_debug;
        }
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u722_do_process - called from - ".$ps_calledfrom."  ";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){echo $sys_debug_text."  called ".$sys_function_name."  DEBUG IS WORKING";};
    IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  ps_debug = ".$ps_debug;};
    IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name.",ps_tablename, ps_keyfield,ps_keyvalue,ps_processid, ps_ataction,   ps_line_debug, ps_calledfrom,p_dbcnx,ps_debug,ps_details_def,ps_details_data,ps_sessionno";};
    IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name.",".$ps_tablename." , ".$ps_keyfield." , ".$ps_keyvalue." , ".$ps_processid." , ".$ps_ataction." , ".$ps_line_debug." , ".$ps_calledfrom." , p_dbcnx, ".$ps_debug." , ps_details_def,ps_details_data,".$ps_sessionno;};

    $sys_function_out = "";
A100_INIT:
    $s_error_messages = "";
    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;
    $s_keyvalue = $ps_keyvalue;
    $s_keyvalue = str_replace("'","",$s_keyvalue);

    $s_contract_id = "";
    $s_job_type = "";
    $s_job_phase = "notset";
// gw 20110802 - neeed to work out how to set the job_phase_to
    $s_job_phase_to = "notset";
    $s_process_done = "NO";
    $s_error_note = "";
// gw 20110729 get the job details
// get the contract_id and job_type to make the worktype
    $s_next_sequence = "DO_THEM_ALL";
B_000_DO:
    $s_contract_id = $ps_keyfield;

    if (strtoupper(trim($s_keyvalue)) == "NONE")
    {
        goto B_900_END_RECORDS;
    }
    if (strtoupper(trim($s_keyvalue)) == "NO_JOB_ID")
    {
        $s_keyvalue = "NONE";
        goto B_900_END_RECORDS;
    }
    if ($ps_keyvalue == "none")
    {
        $s_keyvalue = "NONE";
        goto B_900_END_RECORDS;
    }

    $s_sql = "SELECT * from jobs where jobs_id = '".$ps_keyvalue."'";
    IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};

    $i_record_count = "0";

    $s_sql = str_replace("#p","|",$s_sql);
    $s_sql = str_replace("#P","|",$s_sql);
    $s_sql = $this->clmain_v200_load_line( $s_sql,$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b400");
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql." ");};

B_000_RECORD_LOOP:
    $result = $class_sql->c_sqlclient_exec_query($p_dbcnx,$s_sql);
    $s_reB_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows == 0)
    {
        ECHO "<BR> clmain u722 no records found for s_sql=".$s_sql."<br>";
        $s_error_messages =    $s_error_messages." Unable to locate the job record for s_sql=".$s_sql."<br>";
    }

B_100_GET_RECORDS:
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto B_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto B_900_END_RECORDS;
    }
    $s_reB_found = "YES";
B_150_DO_RECORDS:
    IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  s_sql = ".$s_sql."<BR>key1=".$row['key1']."  key2 =".$row['key2']." key3= ".$row['key3']." key4= ".$row['key4'];};
    $s_contract_id = $this->clmain_u592_get_field_from_xml($row["data_blob"],"contract_id","NO","clmain_u722"," ");
    $s_job_type = $this->clmain_u592_get_field_from_xml($row["data_blob"],"job_type","NO","clmain_u722"," ");
    $s_job_phase = $this->clmain_u592_get_field_from_xml($row["data_blob"],"phase","NO","clmain_u722"," ");
 B_158_NEXT:
    $i_record_count = $i_record_count + 1;
    GOTO B_100_GET_RECORDS;
B_159_END:
B_900_END_RECORDS:

//get the record from the tablename with the keyfieldname = field value
//see what in the key values and the number fields and set the value to the appropriate value from details_def_db
C_000_DO:
    $s_attempt ="first";
    $s_processid = strtolower(trim($ps_processid));

    $s_sql_group_and_type = "  and  (  recordgroup = 'WORKTYPE' and recordtype = 'ACTIONS') ";
//GW20110921 - ADDED THE DEFAUTL ON KEY1 or the process would pickup all records for the action regardlesss of the contraxct
    $s_sql = "SELECT * from ".$ps_tablename." where key1  = 'DEFAULTS' and  key2  = '".$ps_processid."' and key3 = '".$ps_ataction."' ".$s_sql_group_and_type." order by   key4";

    if (strtoupper(trim($s_keyvalue)) <> "NONE")
    {
        $s_sql = "SELECT * from ".$ps_tablename." where key1  = '".$s_contract_id."_".$s_job_type."' and key2  = '".$ps_processid."' and key3 = '".$ps_ataction."'".$s_sql_group_and_type." order by key4";
    }
    $s_error_messages = " Doing First attempt - No process flow for s_sql=".$s_sql."<br>";

C_020_TRY_AGAIN:

    IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." GOT THE RECORD s_sql = ".$s_sql."";};

    $i_record_count = "0";
//gw20110526 - next 3 were commented
    $s_sql = str_replace("#p","|",$s_sql);
    $s_sql = str_replace("#P","|",$s_sql);
    $s_sql = $this->clmain_v200_load_line( $s_sql,$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b400");
    IF ($sys_debug == "YES"){$this->z901_dump( $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql." ");};

C_000_RECORD_LOOP:
    $result = $class_sql->c_sqlclient_exec_query($p_dbcnx,$s_sql);
    $s_rec_found = "NO";
    $s_numrows = mysql_num_rows($result);

    if ($s_numrows <> 0)
    {
       goto C_100_GET_RECORDS;
    }
// gw 201111    echo "<br> ** ERROR GWREF:CLM_u722c000  s_sql=".$s_sql." s_attempt=".$s_attempt." ";

//    DIE ("GW DIED: CLMAIN C_000_RECORD_LOOP");
/* gw 20111110 -  MOVED TO THE CASCADE SECTION
    if ($s_attempt == "first")
    {
        $s_error_messages =    $s_error_messages." First attempt - No process flow for s_sql=".$s_sql."<br>";
        if ($s_processid == "delay")
        {
            $s_sql = "SELECT * from ".$ps_tablename." where key1  = '".$s_contract_id."_DEFAULTS'  and key2  = '".$ps_processid."' and key3 = '".$ps_ataction."'".$s_sql_group_and_type." order by key4";
        }else{
            $s_sql = "SELECT * from ".$ps_tablename." where key1  = 'DEFAULTS' and key2  = '".$ps_processid."' and key3 = '".$ps_ataction."'".$s_sql_group_and_type." order by key4";
        }
        $s_attempt ="second";
        $s_error_messages =   $s_error_messages." Second attempt - No process flow for s_sql=".$s_sql."<br>";
        GOTO C_000_RECORD_LOOP;
    }
*/
C_030_DO_CASCADE:
/* // gw 20111110 - checking wrong field
    if ($s_processid == "delay")
    {
        GOTO C_031_DO_CHECKING;
    }
    if ($s_processid == "csr_activities")
    {
        GOTO C_031_DO_CHECKING;
    }
    if ($s_processid == "repaired")
    {
        GOTO C_031_DO_CHECKING;
    }
    if ($s_processid == "udelivered")
    {
        GOTO C_031_DO_CHECKING;
    }

$ps_ataction

*/
/* gw 20111110 - everything cascased from now on
    if ($ps_ataction == "delay")
    {
        GOTO C_031_DO_CHECKING;
    }
    if ($ps_ataction == "csr_activities")
    {
        GOTO C_031_DO_CHECKING;
    }
    if ($ps_ataction == "repaired")
    {
        GOTO C_031_DO_CHECKING;
    }
    if ($ps_ataction == "udelivered")
    {
        GOTO C_031_DO_CHECKING;
    }

//    echo "<br> ** ERROR GWREF:CLM_u722c000a  s_sql=".$s_sql." s_processid=[]".$s_processid."[]  ps_ataction=".$ps_ataction,"[";

    GOTO C_039_SKIP_CASCADE;
*/

C_031_DO_CHECKING:
    if ($s_attempt == "first")
    {
        $s_error_messages =    $s_error_messages." First attempt - No process flow for s_sql=".$s_sql."<br>";
        $s_sql = "SELECT * from ".$ps_tablename." where key1  = '".$s_contract_id."_DEFAULTS'  and key2  = '".$ps_processid."' and key3 = '".$ps_ataction."'".$s_sql_group_and_type." order by key4";
        $s_attempt ="second";
        $s_error_messages =   $s_error_messages." Second attempt - No process flow for s_sql=".$s_sql."<br>";
        GOTO C_000_RECORD_LOOP;
    }
    if ($s_attempt == "second")
    {
        $s_attempt ="third";
        $s_worktype = $s_contract_id."_".$s_job_type;
        if(trim($s_job_type) == "")
        {
                $s_worktype = $s_contract_id;
        }
        $s_sql = "SELECT * from ".$ps_tablename." where key1  = '".$s_worktype."'  and key2  = '".$ps_processid."' and key3 = 'DEFAULTS' ".$s_sql_group_and_type." order by key4";
        $s_error_messages =    $s_error_messages." third attempt - No process flow for s_sql=".$s_sql."<br>";
        GOTO C_000_RECORD_LOOP;
    }

    if ($s_attempt == "third")
    {
        $s_attempt ="forth";
        $s_sql = "SELECT * from ".$ps_tablename." where key1  = '".$s_contract_id."_DEFAULTS' and key2  = '".$ps_processid."' and key3 = 'DEFAULTS' ".$s_sql_group_and_type." order by key4";
        $s_error_messages =    $s_error_messages." forth attempt - No process flow for s_sql=".$s_sql."<br>";
        GOTO C_000_RECORD_LOOP;
    }
    if ($s_attempt == "forth")
    {
        $s_attempt ="fifth";
        $s_sql = "SELECT * from ".$ps_tablename." where key1  = 'DEFAULTS' and key2  = '".$ps_processid."' and key3 = '".$ps_ataction."' ".$s_sql_group_and_type." order by key4";
        $s_error_messages =    $s_error_messages." fifth attempt - No process flow for s_sql=".$s_sql."<br>";
        GOTO C_000_RECORD_LOOP;
    }
    if ($s_attempt == "fifth")
    {
        $s_attempt ="sixth";
        $s_sql = "SELECT * from ".$ps_tablename." where key1  = 'DEFAULTS' and key2  = '".$ps_processid."' and key3 = 'DEFAULTS' ".$s_sql_group_and_type." order by key4";
        $s_error_messages =    $s_error_messages." sixth attempt - No process flow for s_sql=".$s_sql."<br>";
        GOTO C_000_RECORD_LOOP;
    }
C_039_SKIP_CASCADE:

C_100_ERROR_MESSAGE:
    echo "<br> ###############################################################################################################";
    echo "<br> ** ERROR REF:CLM_u722c000  No Process flow for this process";
    ECHO "<BR> Error References: ";
    ECHO "<BR>      Table: []".$ps_tablename."[]";
    ECHO "<BR>      keyfield: []".$ps_keyfield."[]";
    ECHO "<BR>      keyvalue: []".$ps_keyvalue."[]";


    ECHO "<BR>      processid: []".$ps_processid."[]";
    ECHO "<BR>      ataction: []".$ps_ataction."[]";
    ECHO "<BR>      calledfrom: []".$ps_calledfrom."[]";

    ECHO "<BR> clmain u722 no records found for s_sql=".$s_sql."<br>";
    ECHO "<BR> Error details <br>";

    Echo $s_error_messages ;
    ECHO "<BR> End of error details <br>";

    if ($ps_processid == "udelivered")
    {
// make an error log somewhere for paul to check
        $this->z903_dump(" ** ERROR REF:CLM_u722c000  No Process flow for this process <BR> Error References: Table: ".$ps_tablename."  keyfield: ".$ps_keyfield."      processid: ".$ps_processid."    ataction: ".$ps_ataction." calledfrom: ".$ps_calledfrom."");
        $this->z903_dump("u722 - no action for ".$s_sql );
        GOTO C_100_GET_RECORDS;
    }

    die ( "The process had been died - Notify Solution Adminstrations");

C_100_GET_RECORDS:
    if ($i_record_count > $s_numrows) {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." count processed i_record_count=".$i_record_count."  s_numrows=".$s_numrows;};
        goto C_900_END_RECORDS;
    }

    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    IF ($row === false )
    {
        IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  fetch row is false";};
        goto C_900_END_RECORDS;
    }
    $s_rec_found = "YES";
C_150_DO_RECORDS:
    $s_do_debug = $this->clmain_u592_get_field_from_xml($row["data_blob"],"name","NO","clmain_u722","nodefault");
    if (strpos(strtoupper(trim($s_do_debug)),"*DODEBUG*") === false)
    {
        $s_do_debug = "";
    }else{
        $s_do_debug = "<br>dodebug set by line name=".$s_do_debug;
    }


    if ($s_do_debug <> ""){
        echo "<br> ".$s_do_debug;
        echo "<br> ###############################################################################################################";
        echo "<br> ** ERROR REF:CLM_u722c000  No Process flow for this process";
        ECHO "<BR> Error References: ";
        ECHO "<BR>      Table: []".$ps_tablename."[]";
        ECHO "<BR>      keyfield: []".$ps_keyfield."[]";
        ECHO "<BR>      keyvalue: []".$ps_keyvalue."[]";
        ECHO "<BR>      processid: []".$ps_processid."[]";
        ECHO "<BR>      ataction: []".$ps_ataction."[]";
        ECHO "<BR>      calledfrom: []".$ps_calledfrom."[]";

        ECHO "<BR> clmain u722 no records found for s_sql=".$s_sql."<br>";
        ECHO "<BR> Error details <br>";

        Echo $s_error_messages ;
        ECHO "<BR> End of error details <br>";
    }





    if ($s_do_debug <> ""){ECHO "<BR> ".$s_do_debug."  GOT THE RECORD s_sql = ".$s_sql."<BR>key1=".$row['key1']."  key2 =".$row['key2']." key3= ".$row['key3']." key4= ".$row['key4']."";}
    IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."  s_sql = ".$s_sql."<BR>key1=".$row['key1']."  key2 =".$row['key2']." key3= ".$row['key3']." key4= ".$row['key4'];};
    if ($s_do_debug <> ""){        ECHO "<BR>  ".$s_do_debug." GOT THE RECORD data_blob = ".$row['data_blob']."";}
C_150_A:
    if ($s_job_phase == "notset")
    {
        goto C_150_SKIP_PHASE;
    }
    $s_phasefrom = $this->clmain_u592_get_field_from_xml($row["data_blob"],"phasefrom","NO","clmain_u722","nodefault");
    if (strtoupper(trim($s_phasefrom)) == "")
    {
        $s_phasefrom = "ALL";
    }
    if (strtoupper(trim($s_phasefrom)) == "ALL")
    {
        goto C_150_B;
    }
    if (strtoupper(trim($s_phasefrom)) <> strtoupper(trim($s_job_phase)))
    {
         $s_error_note = $s_error_note."<br> failed phase from check: record phase=(".(strtoupper(trim($s_phasefrom)).") Job phase=(".strtoupper(trim($s_job_phase))).")";
         goto C_158_NEXT;
    }
C_150_B:
    if ($s_job_phase_to == "notset")
    {
        goto C_150_SKIP_PHASE;
    }
    $s_phaseto = $this->clmain_u592_get_field_from_xml($row["data_blob"],"phaseto","NO","clmain_u722","nodefault");
    if (strtoupper(trim($s_phaseto)) == "ALL")
    {
        goto C_150_C;
    }
    if (strtoupper(trim($s_phaseto)) <> strtoupper(trim($s_job_phase)))
    {
        $s_error_note = $s_error_note."<br> failed phase to check: record phase=(".(strtoupper(trim($s_phaseto)).") Job phase=(".strtoupper(trim($s_job_phase))).")";
        goto C_158_NEXT;
    }
C_150_C:
C_150_SKIP_PHASE:
    $s_dowhat = $this->clmain_u592_get_field_from_xml($row["data_blob"],"dowhat","NO","clmain_u722","nodefault");
    $s_sequence = $this->clmain_u592_get_field_from_xml($row["data_blob"],"sequence","NO","clmain_u722","nodefault");

    if ($s_do_debug <> ""){ECHO "<BR>   ".$s_do_debug." GOT THE RECORD dowhat = ".$s_dowhat."";}
    IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."   dowhat = ".$s_dowhat;};

     $s_dowhat = str_replace("^","|",$s_dowhat);
     $s_dowhat = str_replace("#add_pipe#","|",$s_dowhat);
     $s_dowhat = str_replace("#start_field#","#p%!",$s_dowhat);
     $s_dowhat = str_replace("#end_field#","!%#p",$s_dowhat);
     $s_dowhat = str_replace("#equal_sign#","=",$s_dowhat);
     $s_dowhat = str_replace("#quote# ",'"',$s_dowhat);
     $s_dowhat = str_replace("#quote#",'"',$s_dowhat);
     $s_dowhat = str_replace("#carat#",'^',$s_dowhat);


    IF ($sys_debug == "YES"){ECHO "<BR>   ".$s_do_debug." GOT THE RECORD dowhat = ".$s_dowhat."<BR>";};
    IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name."   dowhat = ".$s_dowhat;};

    $xml_arr  ="";
    if ($s_do_debug <> "")
    {
        ECHO "<BR>  ".$s_do_debug." GOT THE RECORD dowhat = ".$s_dowhat." ";
    }

    if (!strpos(strtoupper(trim($s_do_debug)),"*DODEBUG*DIE*") === false)
    {
        ECHO "<BR>  ".$s_do_debug." ############### PROCESS IS SET TO DIE BEFORE RUNNING THE DO_WHAT";
        DIE ("<BR>  process died clmain u722 - configured to die before running the command");
    }
    if (strtoupper(trim($s_next_sequence)) == "DO_THEM_ALL")
    {
        goto C_152_SKIP_SEQUENCE;
    }
            ECHO "<BR>checking the sequence number s_sequence =[]".$s_sequence."[]  s_next_sequence = []".$s_next_sequence."[]";

    if ($s_sequence < $s_next_sequence)
    {
            ECHO "<BR>goto next  checking the sequence number s_sequence =[]".$s_sequence."[]  s_next_sequence = []".$s_next_sequence."[]";
        goto  C_158_NEXT;
    }

C_152_SKIP_SEQUENCE:
C_155_IF:
// DO_IF^v1^#p%!__postv_db_email__!%#p^=^3^EXIT^ELSE^SKIP_TO_SEQUENCE
    IF (strtoupper(substr($s_dowhat,0,5)) <> "DO_IF")
    {
        GOTO C_155_IF_END;
    }
    $s_temp = $class_clprocessjobline->pf_a100_test_if($s_dowhat,$s_details_def,$s_details_data,$sys_debug,$ps_sessionno,"cl_main u722 c150 ");
//gw20120522 - a100_test returns False or the actual value of $s_dowhat
    $ar_do_if_details = explode("|",$s_dowhat);

    $s_action_true = $ar_do_if_details[5];
    $s_action_false = $ar_do_if_details[7];

C_155_IF_FALSE:
    if ($s_temp <> "FALSE")
    {
        GOTO C_155_IF_TRUE;
    }

    echo "<br> ** DO TEST HAS RETURNED FALSE ";
    echo "<br> s_action_false=".$s_action_false;
    echo "<br> s_action_true=".$s_action_true;
    ECHO "<BR>s_dowhat ][]".$s_dowhat."[]";
    goto C_900_END_RECORDS;

C_155_IF_TRUE:
    echo "<br> ** DO TEST HAS RETURNED TRUE ";
    echo "<br> s_action_false=".$s_action_false;
    echo "<br> s_action_true=".$s_action_true;
    ECHO "<BR>s_dowhat ][]".$s_dowhat."[]";

    if (strpos(strtolower($s_action_true),"skip_to_")=== false)
    {
        ECHO "<BR>find skip to failed  s_action_true []".strtolower($s_action_true)."[]";
    }else{
        $s_next_sequence = STRTOUPPER($s_action_true);
        $s_next_sequence = str_replace("SKIP_TO_","",$s_next_sequence);
//            ECHO "<BR>found skip_to s_next_sequence []".$s_next_sequence."[] s_sequence=[]".$s_sequence."[]";
        if ($s_next_sequence <= $s_sequence )
        {
            ECHO "<BR>Configuration error --- Must skip_to must be a higher sequence action - Notify system administration";
            ECHO "<BR>Current line sequence= []".$s_sequence."[]";
            ECHO "<BR>Next sequence to run = []".$s_next_sequence."[]";
            ECHO "<BR>configuration error cannot skip to a previous line find skip to failed  s_action_true []".strtolower($s_action_true)."[]";
            die("<br><br>This process has died - Notify system adminstration");
        }
        goto C_158_NEXT;
    }
    if (strtolower($s_action_true) == "exit")
    {
        ECHO "<BR>got an exit I am out of here!";
        $s_process_done = "YES";
        goto C_900_END_RECORDS;
    }

    ECHO "<BR>s_next_sequence []".$s_next_sequence."[]";



    $s_process_done = "NO";

    goto C_900_END_RECORDS;
C_155_IF_END:
C_156_DO_EXIT:
    if (strtolower($s_dowhat) <> "exit")
    {
        goto C_156_DO_EXIT_END;
    }
    $s_process_done = "YES";
    ECHO "<BR>doing a s_dowhat ][]".$s_dowhat."[]";
    $s_process_done = "YES";
    goto C_900_END_RECORDS;
C_156_DO_EXIT_END:

C_158_DO_LINE:
    $s_temp = $class_clprocessjobline->pf_clprocessjobline($p_dbcnx,$s_dowhat,$s_details_def,$s_details_data,$xml_arr,$ps_sessionno,"NO");



    if ($s_do_debug <> "")
    {
        ECHO "<BR>  ".$s_do_debug." GOT THE RECORD s_temp = ".$s_temp." ";
    }
    if (strpos($s_temp,"|^%##%^|") !== false)
    {
//     echo "<br> do the split import clmain results=".$s_temp."<BR> ";
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
    }

 //ECHO "<BR> GOT THE RECORD s_details_def = ".$s_details_def."";
 //ECHO "<BR> GOT THE RECORD s_details_data = ".$s_details_data."";

    if ($s_do_debug <> "")
    {
        ECHO "<BR><BR>  ";
    }
    $s_process_done = "YES";
// gw 20111128 - do_url is the last thing to run - NOT TESTED
// JOB PROCESS NEEDS TO SET THE RETURN VALUE TO STOP_ACTIONS IF THE DO_WHAT IS STOP_ACTION OR DO_URL_ACTION ****** NOTE THE IF PROCESSING ON BOTH
//    if ($s_temp = "STOP_ACTIONS")
//    {
//        GOTO C_900_END_RECORDS;
//    }
C_158_NEXT:
    $i_record_count = $i_record_count + 1;
    GOTO C_100_GET_RECORDS;
C_159_END:
C_900_END_RECORDS:


    IF ($s_process_done <> "YES")
    {
        echo "<br> ** ERROR REF:CLM_u722c900  No Process flow for this combination ";
        ECHO "<BR> Error References: ";
        ECHO "<BR>      Table: ".$ps_tablename."";
        ECHO "<BR>      keyfield: ".$ps_keyfield."";
        ECHO "<BR>      processid: ".$ps_processid."";
        ECHO "<BR>      ataction: ".$ps_ataction."";
        ECHO "<BR>      calledfrom: ".$ps_calledfrom."";
        ECHO "<BR>      job phase: ".$s_job_phase."";
        ECHO "<BR>      job_phase_to: ".$s_job_phase_to."";
        ECHO "<BR>      s_error_note: ".$s_error_note."";

        ECHO "<BR> clmain u722 no records found for s_sql=".$s_sql."<br>";
        if ($ps_processid == "udelivered")
        {
    // make an error log somewhere for paul to check
            $this->z903_dump(" ** ERROR REF:CLM_u722c000  No Process flow for this process <BR> Error References: Table: ".$ps_tablename."  keyfield: ".$ps_keyfield."      processid: ".$ps_processid."    ataction: ".$ps_ataction." calledfrom: ".$ps_calledfrom."");
            $this->z903_dump("u722 - no action for ".$s_sql );
        }else{
            die ( "The process had been died - Call system support");
        }

    }
// do what needs to be done for the job


D_000:
Z900_EXIT:
     $sys_function_out = $s_details_def."|^%##%^|".$s_details_data;
    //echo "<br>clmain b100 in u722  <br>sys_function_out<br>".$sys_function_out."<br><br>";

     return $sys_function_out;


}
//* sys_email|v1|return def|f_clemail_create_email(emailaddress,subject ,body map filename, body map group ,dodug,calledfrom,$ps_details_def,$ps_details_data)| fail url | do debug|END
//sys_email|v1|sys_email_message|f_clemail_create_email(#p%!_POSTV_partneremailaddress_!%#p,Invitation to become a MyPalletBook partner %!_POSTV_partnername_!%,my_buddy_main.htm,email_body,NO,sysset)|http://#p%!_server_HTTP_HOST_!%#p/als_web_prog/#p%!_SESSION_SUPER_ko_live_or_dev_!%#p/ut_menu.php?fnid=201&map=my_buddy_main.htm&p=1^mypid=#p%!_postV_mypalletid_!%#p^mode=EMAILERR,details_def,details_data|NO|END
//##########################################################################################################################################################################
function clmain_u730_do_email($ps_dbcnx,$ps_line,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_url_siteparams)
{
//    echo "<br> ddddddddddd the debug is being switched on sys_debug=".$sys_debug."<br>";
// gw 20120218   $ps_line_in ="sys_email|v1|sys_email_message|".$s_csr_notify_id."|".$s_subject."|".$s_body_map."|".$s_body_map_group."||NO|".$s_jcl_full_outfilename."|".$s_jcl_outfilenameonly."|END";

    global $class_main;
    global $class_email;

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
//    echo "<br> the debug is being switched on sys_debug=".$sys_debug."<br>";
    }
    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u730_do_email  ";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."ggggg   DEBUG VIEW SOURCE FOR DETAILS sys_debug_text=".$sys_debug_text."<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_in_line = ".$ps_line." ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};

    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;
B100_SET_FIELDS:
    $array_temp = array();
    $s_error_text = "";
    $s_error_list = "";
    $s_fail_url = "";

    $ar_line_fields = array();
    $ar_line_fields = explode("|",$ps_line);

    $s_response_field = $ar_line_fields[2];
    $s_email_address =  $ar_line_fields[3];
    $s_subject =  $ar_line_fields[4];
    $s_body_map = $ar_line_fields[5];
    $s_body_group = $ar_line_fields[6];
    $s_fail_url =  $ar_line_fields[7];
    $sys_debug =  $ar_line_fields[8];
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $sys_debug;
        IF ($sys_debug == "YES"){echo $sys_function_name." line set line set    line set  DEBUG VIEW SOURCE FOR DETAILS - inbound line field 8 = []".$ar_line_fields[8]."[]<br>";};
        IF ($sys_debug == "YES"){echo $sys_function_name." ps_in_line = []".$ps_line."[]<br>";};
        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_in_line = ".$ps_line." ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};
    }

    $s_attachment_list = "";
    $s_attachment_list =  $ar_line_fields[9];
    $s_attachment_name_list =  $ar_line_fields[10];

    $s_body_map = str_replace("\r","",$s_body_map);
    $s_body_map = str_replace("\n","",$s_body_map);

C100_DO:
//* sys_email|v1|sys_email_message|emailaddress|subject | body map | body group | fail url | debug |END
//sys_email|v1|sys_email_message|#p%!_POSTV_partneremailaddress_!%|Invitation to become a MyPalletBook partner %!_POSTV_partnername_!%|my_buddy_main.htm|email_body|http://#p%!_server_HTTP_HOST_!%#p/als_web_prog/#p%!_SESSION_SUPER_ko_live_or_dev_!%#p/ut_menu.php?fnid=201&map=my_buddy_main.htm&p=1^mypid=#p%!_postV_mypalletid_!%#p^mode=EMAILERR|NO|END
    $sys_function_out = $class_email->sys_clemail_p100_create_email($s_email_address,$s_subject,$s_body_map,$s_body_group,$sys_debug,"ut action c100 ",$ps_details_def,$ps_details_data,$ps_sessionno,$s_attachment_list);
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ";};
    IF ($sys_debug == "YES"){$sys_function_out.=" <!--".$sys_debug_text."-->";};

    if(trim($sys_function_out," ") <> "ERROR")
    {
        $sys_function_out = "ALL OK";
        GOTO Z900_EXIT;
    }

    $s_do_url =  $s_fail_url;
    $s_do_url = STR_REPLACE("#P","|",$s_do_url);
    $s_do_url = STR_REPLACE("#p","|",$s_do_url);
    $s_do_url = $class_main->clmain_v200_load_line($s_do_url,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"ut_action b900 ");

    header("Location: $s_do_url");
/* Redirect to a different page in the current directory that was requested */
//$host  = $_SERVER['HTTP_HOST'];
//$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
//$extra = 'mypagegw.php';
//header("Location: http://$host$uri/$extra");
// exit;

Z900_EXIT:
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_u740_make_from_template($ps_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_tablename,$ps_key_fieldname,$ps_template,$ps_fields,$ps_line_debug,$ps_calledfrom)
{
    global $class_main;
    global $class_email;

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    IF ($sys_debug = "NO") {
        IF ($ps_line_debug !="NO") {
            $sys_debug  = "YES";
            $sys_debug_text = $ps_debug." calledfrom:".$ps_calledfrom; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
        }
    }

    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u730_do_email";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name."  ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};

    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;
B100_SET_FIELDS:
    $s_fail_url = "";
    $s_do_url =  $s_fail_url;
    $s_do_url = STR_REPLACE("#P","|",$s_do_url);
    $s_do_url = STR_REPLACE("#p","|",$s_do_url);
    $s_do_url = $class_main->clmain_v200_load_line($s_do_url,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"ut_action b900 ");

C100_DO:
// create record from template
// populate fields from psfields
// update record
    $temp = $this->clmain_u690_add_db_record($ps_tablename,$ps_key_fieldname,$ps_template,$ps_fields,$ps_line_debug,"u740_a",$ps_dbcnx,"NO",$ps_details_def,$ps_details_data,$ps_sessionno);
Z900_EXIT:
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_u750_make_pdf_file($ps_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_pdf_filename,$ps_map_filename)
{
    global $class_main;
    global $class_email;

    require_once($_SESSION['ko_prog_path'].'lib/class_pdf_creator.php');
    $class_pdf_create = new cls_pdf_creator();

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
/*GW20110921 - no line debug
    IF ($sys_debug == "NO") {
        IF ($ps_line_debug !="NO") {
            $sys_debug  = "YES";
            $sys_debug_text = $ps_from_debug." calledfrom:".$ps_calledfrmom; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
        }
    }

*/

//    echo 'ps_pdf_filename='.$ps_pdf_filename.'<br>';
//    echo 'ps_map_filename='.$ps_map_filename.'<br>';

    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u750_make_pdf_file";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name."  ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};

    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;
B100_SET_FIELDS:
/* gw20110921 - not used
    $s_do_url =  $s_fail_url;
    $s_do_url = STR_REPLACE("#P","|",$s_do_url);
    $s_do_url = STR_REPLACE("#p","|",$s_do_url);
    $s_do_url = $class_main->clmain_v200_load_line($s_do_url,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"ut_action b900 ");
*/
C100_DO:

    $class_pdf_create->cls_create_pdf_file($ps_pdf_filename,$ps_map_filename,"");

    //$class_pdf_create->cls_pdf_testing($ps_pdf_filename,$ps_map_filename);

Z900_EXIT:
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_u760_do_calc($ps_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_calc_string)
{
    global $class_main;

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }

    $sys_function_name = "";
    $sys_function_name = "debug - clmain_u760_do_calc";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name."  ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};

    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;
B100_SET_FIELDS:
C100_DO:

Z900_EXIT:
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_u770_do_html_debug($ps_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_calledfrom, $ps_filename,$ps_content_type)
{
    global $class_main;

    $sys_function_out = "";
// gw 20111024 create filename
// if ps_content_type = session - put all session vars in the html
//  etc
B100_WRITE:
    if (!file_exists("logs"))
    {
        $md= mkdir("logs",0777,true);
    }
//    date_default_timezone_set('Australia/Brisbane');

    $myfile = "logs/".$ps_filename.".html";

//    echo "<br> dump html ".$myfile."<br>";
    $fh = fopen($myfile,'a') or die("cant open file".$myfile);
B200_DO_DEBUG_DUMP:
    $s_temp = "<br>########################################################################################################################################";
    fwrite($fh,$s_temp);
    $s_temp = " ";
    fwrite($fh,"<br>New run at Bris@=".date("Y-m-d H:i:s",time())."tz:".date_default_timezone_get()." ".$s_temp);
    $s_temp = "<br>########################################################################################################################################";
    fwrite($fh,$s_temp);

    $s_temp = "<br>########################################################################################################################################<br>";
    $s_temp = $s_temp."<BR>Details Def/Data Dump - called from ".$ps_calledfrom." type = ".$ps_content_type."<br>";
    $s_temp = $s_temp.$this->clmain_v300_set_variable("DETAILS_DEF_DUMP",$ps_details_def,$ps_details_data,$ps_debug,$ps_sessionno,$ps_calledfrom);
    $s_temp = $s_temp."<BR> end of Details def/data Dump<br>";
    fwrite($fh,"<br>Bris@=".date("Y-m-d H:i:s",time())."".$s_temp);

    $s_temp = "<br>########################################################################################################################################<br>";
    $s_temp = $s_temp."<BR>Session Variable Dump - called from ".$ps_calledfrom." type = ".$ps_content_type."<br>";
    $s_temp = $s_temp.$this->clmain_v300_set_variable("SESSION_DUMPVARS_OUT",$ps_details_def,$ps_details_data,$ps_debug,$ps_sessionno,$ps_calledfrom);
    $s_temp = $s_temp."<BR> end of Session Variable Dump<br>";
    fwrite($fh,"<br>Bris@=".date("Y-m-d H:i:s",time())."".$s_temp);


B800_CLOSE_FILE:
    fclose($fh);



Z900_EXIT:
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_u780_calc_time_shift($ps_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_calledfrom, $ps_from_time,$ps_calc_values,$ps_valid_times)
{
// take the from time and turn it into utime ( if not already utime )
// work out the duration using the calc_value
// apply the calc value to the valid_times
A100_TEMPLATE_INIT:
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "<br>clmain_u780_calc_time_shift called from [".$ps_calledfrom."]";
    $sys_function_out = "";

    IF ($sys_debug == "YES"){echo $sys_function_name."<BR><BR>DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>ps_from_time  =  ".$ps_from_time;};
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>ps_calc_values  =  ".$ps_calc_values;};
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>ps_valid_times  =  ".$ps_valid_times;};
    IF ($sys_debug == "YES"){echo "<br>";};

A200_SET_FROM_TIME:
    $s_work_utime = $ps_from_time;
    IF (is_numeric($s_work_utime))
    {
        IF ($sys_debug == "YES"){echo $sys_function_name." doing is_numeric ";};
        goto A280_SET_DATE_STRING;
    }
    IF (strtoupper(trim($s_work_utime)) == "NOW")
    {
        IF ($sys_debug == "YES"){echo $sys_function_name." doing now - set work_utime to now";};
        $s_work_utime = TIME();
        goto A280_SET_DATE_STRING;
    }
    IF (strtoupper(trim($s_work_utime)) == "FROM_NOW")
    {
        IF ($sys_debug == "YES"){echo $sys_function_name." doing from now - set work_utime to now";};
        $s_work_utime = TIME();
        goto A280_SET_DATE_STRING;
    }
        IF ($sys_debug == "YES"){echo $sys_function_name."********* checking ".strtoupper(substr($s_work_utime,10,1));};
    IF (strtoupper(substr($s_work_utime,10,1)) == "T")
    {
        IF ($sys_debug == "YES"){echo $sys_function_name."get the utime from a udtime";};
        $d_hr = substr($s_work_utime,11,2);
        $s_min = substr($s_work_utime,14,2);
        $s_sec = substr($s_work_utime,17,2);
        $s_month = substr($s_work_utime,5,2);
        $s_day = substr($s_work_utime,8,2);
        $s_year = substr($s_work_utime,0,4);
        $s_daylight_saving= "0";
    //    echo "<br>s_field_value =".$s_field_value." mktime(".$d_hr."--".$s_min."--".$s_sec."--".$s_month."--".$s_day."--".$s_year;
        $s_work_utime = mktime($d_hr,$s_min,$s_sec,$s_month,$s_day,$s_year);
        goto A280_SET_DATE_STRING;
    }
A280_SET_DATE_STRING:
    $s_work_utime = intval($s_work_utime);
//    $s_date_string = "v1:".date("l",intval($s_work_utime))."  ".date("D",intval($s_work_utime))."  ".date("Y",intval($s_work_utime))."-".date("M",intval($s_work_utime))."-".date("d",intval($s_work_utime))."  ".date("H",intval($s_work_utime)).":".date("i",intval($s_work_utime)).".".date("s",intval($s_work_utime));
    $s_date_string = "v1:".date("l",$s_work_utime)."  ".date("D",$s_work_utime)."  ".date("Y",$s_work_utime)."-".date("M",$s_work_utime)."-".date("d",$s_work_utime)."  ".date("H",$s_work_utime).":".date("i",$s_work_utime).".".date("s",$s_work_utime);
A290_END:
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>ps_from_time  =  ".$ps_from_time." = ".$s_date_string;};

A199_END_TEMPLATE_INIT:

B100_SET_VALUES:
    $s_start_utime = "0";
    $s_duration = "0";
    $s_duration_seconds = "0";
    $s_dur_year = "0";
    $s_dur_month = "0";
    $s_dur_day = "0";
    $s_dur_hour = "0";
    $s_dur_minutes = "0";
    $s_dur_seconds = "0";
    $s_dur_temp = "0";

C100_SET_START_TIME:
//gw20120802     $s_work_utime = $ps_from_time;

D100_SET_DURATION:
    $s_duration = "0";
// means is number of seconds
    if (is_numeric($ps_calc_values))
    {
        $s_duration = $ps_calc_values;
        goto E100_APPLY_DURATION;
    }
// explode the $ps_calc_value
    $ar_calc_values = explode(",",strtoupper($ps_calc_values));
    $s_array_count = count($ar_calc_values);
    $s_array_entry_count ="0";

D110_CHECK_FIELD:
    $s_dur_temp = "0";

    if ($s_array_entry_count >= $s_array_count)
    {
        goto B190_END_ARRAY;
    }

    $s_dur_temp = trim(strtoupper($ar_calc_values[$s_array_entry_count]));
    IF ($sys_debug == "YES"){echo $sys_function_name."    s_array_entry_count=".$s_array_entry_count."  s_array_count= ".$s_array_count. "  s_dur_temp= ".$s_dur_temp."  s_work_utime=".$s_work_utime;};


    IF ($sys_debug == "YES"){echo $sys_function_name."    dur_temp=".$s_dur_temp;};
    if (!strpos($s_dur_temp,"YEAR") === false )
    {
        $s_dur_temp = TRIM(str_replace("YEAR","",$s_dur_temp));
        $s_dur_year = 31556926 * $s_dur_temp;
        $s_work_utime = $s_work_utime + $s_dur_year;
        goto B180_NEXT;
    }
    if (!strpos($s_dur_temp,"MONTH") === false )
    {
// $s_dur_temp = the number of days between now and the same date next month
        $s_dur_month = 86400 * $s_dur_temp;
        goto B180_NEXT;
    }
    if (!strpos($s_dur_temp,"WHOURS") === false )
    {
        $s_dur_temp = TRIM(str_replace("WHOURS","",$s_dur_temp));
        $s_dur_temp= 60 * 60 * $s_dur_temp;
//        $s_work_utime = $s_work_utime + $s_dur_temp;
        $s_work_utime = $this->clmain_u790_work_time($ps_dbcnx,"NO",$ps_details_def,$ps_details_data,$ps_sessionno, "CLmain u780  b180",$s_work_utime,$s_dur_temp, $ps_valid_times);
        goto B180_NEXT;
    }
// wday is the next
/*()    $s_dur_month = "0";
    $s_dur_day = "0";
    $s_dur_hour = "0";
    $s_dur_minutes = "0";
    $s_dur_seconds = "0";
*/

B180_NEXT:
    $s_work_utime = intval($s_work_utime);
    $s_date_string = "v1:".date("l",$s_work_utime)."  ".date("D",$s_work_utime)."  ".date("Y",$s_work_utime)."-".date("M",$s_work_utime)."-".date("d",$s_work_utime)."  ".date("H",$s_work_utime).":".date("i",$s_work_utime).".".date("s",$s_work_utime);

    IF ($sys_debug == "YES"){echo $sys_function_name."    RESULT s_array_entry_count=".$s_array_entry_count."   s_dur_temp= ".$s_dur_temp."  s_work_utime=".$s_work_utime." = ".$s_date_string;};


    $s_array_entry_count = $s_array_entry_count + 1;
    goto D110_CHECK_FIELD;

B190_END_ARRAY:
    $s_duration = $s_dur_year + $s_dur_month + $s_dur_day +  $s_dur_hour + $s_dur_minutes + $s_dur_seconds;

E100_APPLY_DURATION:
//gw20120208
//echo "u780 - s_work_utime = ".$s_work_utime." s_duration=".$s_duration;

    $s_work_utime = $s_work_utime + $s_duration;

E900_END:

Z900_EXIT:
    $sys_function_out = $s_work_utime;
    $s_work_utime = intval($s_work_utime);
    $s_date_string = "v1:".date("l",$s_work_utime)."  ".date("D",$s_work_utime)."  ".date("Y",$s_work_utime)."-".date("M",$s_work_utime)."-".date("d",$s_work_utime)."  ".date("H",$s_work_utime).":".date("i",$s_work_utime).".".date("s",$s_work_utime);

    IF ($sys_debug == "YES"){echo $sys_function_name."   END OF FUCTION out = ".$sys_function_out."  date = ".$s_date_string;};
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_u790_work_time($ps_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_calledfrom, $ps_from_time,$ps_add_seconds,$ps_valid_times)
{
A100_TEMPLATE_INIT:
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "<br>clmain_u790_work_time called from [".$ps_calledfrom."]";
    $sys_function_out = "";
    $s_work_utime = $ps_from_time;


echo "clmain u790 s_work_utime=".$s_work_utime;

    $s_date_string = "v1:".date("l",$s_work_utime)."  ".date("D",$s_work_utime)."  ".date("Y",$s_work_utime)."-".date("M",$s_work_utime)."-".date("d",$s_work_utime)."  ".date("H",$s_work_utime).":".date("i",$s_work_utime).".".date("s",$s_work_utime);

    IF ($sys_debug == "YES"){echo $sys_function_name."<BR><BR>DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>ps_from_time  =  ".$ps_from_time." = ".$s_date_string;};
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>ps_add_seconds  =  ".$ps_add_seconds;};
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>ps_valid_times  =  ".$ps_valid_times;};
    IF ($sys_debug == "YES"){echo "<br>";};

A199_END_TEMPLATE_INIT:
    $s_utime_out = $ps_from_time;
    $s_add_seconds = $ps_add_seconds;
    $s_day_count = "0";

B100_CALC_WTIME:
    $xml_datablob = $ps_valid_times;
    $s_valid_times = str_replace("[openright]","<",$xml_datablob);
    $s_monday_start = $this->clmain_u592_get_field_from_xml($s_valid_times,"monday_start","no","u790_b100","00:00");
    $s_monday_end = $this->clmain_u592_get_field_from_xml($s_valid_times,"monday_end","no","u790_b100","24:00");
    $s_tuesday_start = $this->clmain_u592_get_field_from_xml($s_valid_times,"tuesday_start","no","u790_b100","00:00");
    $s_tuesday_end = $this->clmain_u592_get_field_from_xml($s_valid_times,"tuesday_end","no","u790_b100","24:00");
    $s_wednesday_start = $this->clmain_u592_get_field_from_xml($s_valid_times,"wednesday_start","no","u790_b100","00:00");
    $s_wednesday_end = $this->clmain_u592_get_field_from_xml($s_valid_times,"wednesday_end","no","u790_b100","24:00");
    $s_thursday_start = $this->clmain_u592_get_field_from_xml($s_valid_times,"thursday_start","no","u790_b100","00:00");
    $s_thursday_end = $this->clmain_u592_get_field_from_xml($s_valid_times,"thursday_end","no","u790_b100","24:00");
    $s_friday_start = $this->clmain_u592_get_field_from_xml($s_valid_times,"friday_start","no","u790_b100","00:00");
    $s_friday_end = $this->clmain_u592_get_field_from_xml($s_valid_times,"friday_end","no","u790_b100","24:00");
    $s_saturday_start = $this->clmain_u592_get_field_from_xml($s_valid_times,"saturday_start","no","u790_b100","00:00");
    $s_saturday_end = $this->clmain_u592_get_field_from_xml($s_valid_times,"saturday_end","no","u790_b100","24:00");
    $s_sunday_start = $this->clmain_u592_get_field_from_xml($s_valid_times,"sunday_start","no","u790_b100","00:00");
    $s_sunday_end = $this->clmain_u592_get_field_from_xml($s_valid_times,"sunday_end","no","u790_b100","24:00");
    $s_publichol_start = $this->clmain_u592_get_field_from_xml($s_valid_times,"publichol_start","no","u790_b100","00:00");
    $s_publichol_end = $this->clmain_u592_get_field_from_xml($s_valid_times,"publichol_end","no","u790_b100","24:00");

B200_START_CALC:
    $s_work_utime = $s_utime_out;
    $s_date_string = "v1:".date("l",$s_work_utime)."  ".date("D",$s_work_utime)."  ".date("Y",$s_work_utime)."-".date("M",$s_work_utime)."-".date("d",$s_work_utime)."  ".date("H",$s_work_utime).":".date("i",$s_work_utime).".".date("s",$s_work_utime);
    IF ($sys_debug == "YES"){echo $sys_function_name."<br> s_utime_out  =  ".$s_utime_out." = ".$s_date_string;};
B210_DOW:
    $s_dow = strtoupper(date("l",$s_utime_out));
// gw20111103 - check if public holdiday and set the dow start;end based on that
// $s_results = uxxx_public_holiday (utime))
// if $s_results <> not_a_ph goto B220_DO_DOW:
// explode $s_result
// set the dow start and end
// B220_DO_DOW:
    IF ($s_dow == "MONDAY")
    {
        $s_dow_start = $s_monday_start;
        $s_dow_end = $s_monday_end;
    }
    IF ($s_dow == "TUESDAY")
    {
        $s_dow_start = $s_tuesday_start;
        $s_dow_end = $s_tuesday_end;
    }
    IF ($s_dow == "WEDNESDAY")
    {
        $s_dow_start = $s_wednesday_start;
        $s_dow_end = $s_wednesday_end;
    }
    IF ($s_dow == "THURSDAY")
    {
        $s_dow_start = $s_thursday_start;
        $s_dow_end = $s_thursday_end;
    }
    IF ($s_dow == "FRIDAY")
    {
        $s_dow_start = $s_friday_start;
        $s_dow_end = $s_friday_end;
    }
    IF ($s_dow == "SATURDAY")
    {
        $s_dow_start = $s_saturday_start;
        $s_dow_end = $s_saturday_end;
    }
    IF ($s_dow == "SUNDAY")
    {
        $s_dow_start = $s_sunday_start;
        $s_dow_end = $s_sunday_end;
    }
    IF ($sys_debug == "YES"){echo $sys_function_name."  for ".$s_dow." start = ".$s_dow_start." end = ".$s_dow_end;};

    $s_work_utime = $s_utime_out;
    $s_date_string = date("Y",$s_work_utime)."-".date("M",$s_work_utime)."-".date("d",$s_work_utime)."  ".$s_dow_start.".00";
    $s_dow_start_utime = mktime(substr($s_dow_start,0,2), substr($s_dow_start,3,2),0,date("m",$s_work_utime),date("d",$s_work_utime),date("Y",$s_work_utime));

    IF ($sys_debug == "YES"){echo $sys_function_name."  Start time = ".$s_date_string." utime = ".$s_dow_start_utime;};

    $s_work_utime = $s_dow_start_utime;
    $s_date_string = date("Y",$s_work_utime)."-".date("M",$s_work_utime)."-".date("d",$s_work_utime)."  ".$s_dow_start.".00";
    IF ($sys_debug == "YES"){echo $sys_function_name."  dow_start_utime as date = ".$s_date_string." utime = ".$s_dow_start_utime;};



    $s_date_string = date("Y",$s_work_utime)."-".date("M",$s_work_utime)."-".date("d",$s_work_utime)."  ".$s_dow_end.".00";
    $s_dow_end_utime = mktime(substr($s_dow_end,0,2), substr($s_dow_end,3,2),0,date("m",$s_work_utime),date("d",$s_work_utime),date("Y",$s_work_utime));
    IF ($sys_debug == "YES"){echo $sys_function_name."  end time = ".$s_date_string." utime = ".$s_dow_end_utime;};

    $s_work_utime = $s_dow_end_utime;
    $s_date_string = date("Y",$s_work_utime)."-".date("M",$s_work_utime)."-".date("d",$s_work_utime)."  ".$s_dow_start.".00";
    IF ($sys_debug == "YES"){echo $sys_function_name."  s_dow_end_utime as date = ".$s_date_string." utime = ".$s_dow_end_utime;};

B220_DOW_CHECK:
    if ($s_day_count > 0 )
    {
        $s_utime_out = $s_dow_start_utime;
    }
// if start time + add_sec < end of day
//  out time = start_time + add_sec
    IF (($s_utime_out + $s_add_seconds) < $s_dow_end_utime)
    {
        $s_utime_out = $s_utime_out + $s_add_seconds;
        goto Z900_EXIT;
    }

// seconds comsumed today = end time - from time
    $s_consumed_seconds =   $s_dow_end_utime -  $s_utime_out;
    $s_remaining_seconds = $s_add_seconds - $s_consumed_seconds;
    IF ($sys_debug == "YES"){echo $sys_function_name."  s_consumed_seconds = ".$s_consumed_seconds." s_remaining_seconds=".$s_remaining_seconds;};
    $s_next_day_utime = mktime(0,0,0,date("m",$s_utime_out),date("d",$s_utime_out)+1,date("Y",$s_utime_out));
    $s_work_utime = $s_next_day_utime;

    $s_date_string = "v1:".date("l",$s_work_utime)."  ".date("D",$s_work_utime)."  ".date("Y",$s_work_utime)."-".date("M",$s_work_utime)."-".date("d",$s_work_utime)."  ".date("H",$s_work_utime).":".date("i",$s_work_utime).".".date("s",$s_work_utime);
    IF ($sys_debug == "YES"){echo $sys_function_name."  s_next_day_utime=".$s_work_utime." = ".$s_date_string;};

    $s_utime_out = $s_next_day_utime;
    $s_day_count = $s_day_count + 1;
    $s_add_seconds = $s_remaining_seconds;

    IF ($sys_debug == "YES"){echo "<BR>";};
    goto B200_START_CALC;

Z900_EXIT:
    $sys_function_out = $s_utime_out;
    $s_work_utime = $s_utime_out;

    $s_date_string = "v1:".date("l",$s_work_utime)."  ".date("D",$s_work_utime)."  ".date("Y",$s_work_utime)."-".date("M",$s_work_utime)."-".date("d",$s_work_utime)."  ".date("H",$s_work_utime).":".date("i",$s_work_utime).".".date("s",$s_work_utime);

    IF ($sys_debug == "YES"){echo $sys_function_name."<BR>  sys_function_out=".$s_work_utime." = ".$s_date_string;};
    IF ($sys_debug == "YES"){echo $sys_function_name."   END OF FUCTION out = ".$sys_function_out."<br>";};
    return $sys_function_out;

}


//    $sys_function_out =  $this->$s_function_name($s_param_1,$s_param_2,$s_param_3,$s_param_4,$s_param_5,$p_dbcnx,"NO",$ps_details_def,$ps_details_data,$ps_sessionno);
//##########################################################################################################################################################################
function clmain_u800_utl_actions_set_list($ps_filemask,$ps_filepath, $ps_mapfile,$ps_linedebug, $ps_calledfrom, $ps_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
A100_TEMPLATE_INIT:
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    IF ($sys_debug =="NO") {
        IF (strtoupper($ps_linedebug) !="NO") {
            $sys_debug  = "YES";
            $sys_debug_text = $ps_linedebug;
        }
    }
    $sys_function_name = "";
    $sys_function_name = "<br>clmain_u800_utl_actions_set_list called from [".$ps_calledfrom."]";
    $sys_function_out = "";

    IF ($sys_debug == "YES"){echo $sys_function_name."<BR><BR>DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>ps_filemask  =  []".$ps_filemask."[]";};
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>ps_filepath  =  []".$ps_filepath."[]";};
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>ps_calledfrom  =  []".$ps_calledfrom."[]";};
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>ps_mapfile  =  []".$ps_mapfile."[]";};
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>ps_linedebug  =  []".$ps_linedebug."[]";};

    IF ($sys_debug == "YES"){echo "<br>";};

A199_END_TEMPLATE_INIT:
    $sys_function_out = "";
    $filepath = $ps_filepath;
    $filemask = $ps_filemask;
    $s_mapfile = $filepath."/".$ps_mapfile;
    $s_page_header = "";
    $s_page_footer = "";

    // s_page_header =  load the utl_actions_map header
//    $s_page_header = $this->clmain_v100_load_html_screen($s_mapfile,$ps_details_def,$ps_details_data,"no","page_header");

B200_FIND_FILES:
    if (!is_dir($filepath))
    {
       $sys_function_out = "<br><br> the indicated path ".$ps_filepath. " is not valid<br>";
       goto Z900_EXIT;
    }
// gw 20120107
//    echo "<br>looking at ".$filepath."/"."*.ini";
//    foreach (glob($filepath."/".$filemask) as $filename)
//    {
//        echo "<br>$filename size=".filesize($filename)."\n";
//    }

    $ar_filelist = (glob($filepath.$filemask));
    $s_filelist_count = count($ar_filelist);
    $s_do_count = "0";

    if ($s_filelist_count == "0")
    {
       $sys_function_out = "<br><br> There are no files that match the selection of ".$filepath.$filemask." <br>";
       goto Z900_EXIT;
    }


B210_NEXT:
    if ($s_do_count >= $s_filelist_count)
    {
        goto B290_END;
    }
    $filename = $ar_filelist[$s_do_count];
//    echo "<br>LOOP PROCESS  $filename size=".filesize($filename)."\n";
//    $sys_function_out = $sys_function_out."<br>LOOP PROCESS  ".$filename." size=".filesize($filename)."";


    $s_details_def = $ps_details_def."|optn_fname|";
    $s_details_data = $ps_details_data."|".$filename."|";

    $sys_function_out = $sys_function_out.$this->clmain_v100_load_html_screen($filename,$s_details_def,$s_details_data,"no","show_option");

    $s_do_count = $s_do_count + 1;
    GOTO B210_NEXT;

B290_END:

B900_END:
// gw 20120107
// Open a known directory, and proceed to read its contents
/*    if ($dh = opendir($filepath)) {
        while (($file = readdir($dh)) !== false) {
             $sys_function_out = $sys_function_out."<br>filename:()".$file."() : filetype: ()".filetype($filepath."/".$file) . "\n";
        }
        closedir($dh);
    }
*/


// get all the files that match the mask in the folder

// open each file get the heading and html blurb
// write to the outbound blob
// build the details def n data
// do the map for each file found

Z900_EXIT:
// $s_page_footer = add the utl_actions_map footer
//    $s_page_footer = $this->clmain_v100_load_html_screen($s_mapfile,$ps_details_def,$ps_details_data,"no","page_footer");

    $sys_function_out = $s_page_header.$sys_function_out.$s_page_footer;

    IF ($sys_debug == "YES"){echo $sys_function_name."   END OF FUCTION out = ".$sys_function_out."<br>";};
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_u810_utl_actions_screen( $ps_mapfile,$ps_worktypeid,$ps_contract_name,$ps_key2,$ps_key3,$ps_linedebug, $ps_calledfrom, $ps_dbcnx,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
A100_TEMPLATE_INIT:
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    IF ($sys_debug =="NO") {
        IF (strtoupper($ps_linedebug) !="NO") {
            $sys_debug  = "YES";
            $sys_debug_text = $ps_linedebug;
        }
    }
    $sys_function_name = "";
    $sys_function_name = "<br>clmain_u800_utl_actions_set_list called from [".$ps_calledfrom."]";
    $sys_function_out = "";

    IF ($sys_debug == "YES"){echo $sys_function_name."<BR><BR>DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>ps_calledfrom  =  []".$ps_calledfrom."[]";};
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>ps_mapfile  =  []".$ps_mapfile."[]";};
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>ps_worktypeid  =  []".$ps_worktypeid."[]";};
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>ps_contract_name  =  []".$ps_contract_name."[]";};
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>ps_key2  =  []".$ps_key2."[]";};
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>ps_key3  =  []".$ps_key3."[]";};
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>ps_linedebug  =  []".$ps_linedebug."[]";};

    IF ($sys_debug == "YES"){echo "<br>";};

A199_END_TEMPLATE_INIT:
    $sys_function_out = "";
    $s_mapfile = $ps_mapfile;

    $s_page_header = "";
    $s_page_footer = "";

    // s_page_header =  load the utl_actions_map header
    $s_page_header = $this->clmain_v100_load_html_screen($s_mapfile,$ps_details_def,$ps_details_data,"no","page_top");

B200_FIND_FILES:
    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;

    $sys_function_out = $sys_function_out.$this->clmain_v100_load_html_screen($s_mapfile,$s_details_def,$s_details_data,"no","maint_screen");
B900_END:

Z900_EXIT:
// $s_page_footer = add the utl_actions_map footer
    $s_page_footer = $this->clmain_v100_load_html_screen($s_mapfile,$ps_details_def,$ps_details_data,"no","page_footer");

    $sys_function_out = $s_page_header.$sys_function_out.$s_page_footer;

    IF ($sys_debug == "YES"){echo $sys_function_name."   END OF FUCTION out = ".$sys_function_out."<br>";};
    return $sys_function_out;

}
//#################################################################################################################################################################################################
function clmain_u820_fn_Z999_build_def_data($ps_dbcnx,$ps_debug, $ps_called_from, $p_row,$ps_def_prefix)
{
    A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";
    /* gw20120429 - to use this function
        $s_temp = $class_main->clmain_u820_fn_Z999_build_def_data($ps_dbcnx,$ps_debug, "d300 b600_body",$row,"evd");
        if (strpos($s_temp,"|^%##%^|") ===false)
        {
        }else{
            $ar_line_details = explode("|^%##%^|",$s_temp);
            $s_details_def_evd = $ar_line_details[0];
            $s_details_data_evd = $ar_line_details[1];
            $s_temp = "";
        };
    */

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_called_from;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "fn_Z999_build_def_data";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;
    $s_details_def = "";
    $s_details_data = "";
    $s_def_prefix = $ps_def_prefix."_";
    if(strtoupper(trim($s_def_prefix)) =="NONE_")
    {
        $s_def_prefix = "";
    }

B000_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};
    $row = $p_row;

    $s_details_def = "";
    $s_details_data = "";

    $s_do_datablob = "NO";

    foreach( $row as $key=>$value)
    {
//        echo "<br>clmain u820 key=[]".$key."[]";
        if (strtoupper($key) == "DETAILS_DEF" )
        {
            if(trim(strtoupper($value)) == "DATABLOB")
            {
                $s_do_datablob = "YES";
                GOTO B657_END;
            };
            if(trim(strtoupper($value)) == "DATA_BLOB")
            {
                $s_do_datablob = "YES";
                GOTO B657_END;
            };
        }
        if (strtoupper($key) == "DETAILS_DATA" ) {
            IF ($s_do_datablob == "YES") {
                $s_do_datablob = "NO";
                GOTO B650_DO_DATA_BLOB;
            }
        }
        if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
        {
            GOTO B651_SKIP_DATA_BLOB;
        }
        if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
        {
            GOTO B651_SKIP_DATA_BLOB;
        }
B650_DO_DATA_BLOB:
        if (strpos(strtoupper($value),"XML") === false)
        {
            GOTO B651_SKIP_DATA_BLOB;
        }

        $ar_xml = simplexml_load_string($value);
        $newArry = array();
        $ar_xml = (array) $ar_xml;
        foreach ($ar_xml as $key => $value)
        {
            $value = trim($value);
            $s_details_def .= "|REC_".$s_def_prefix."DB_".$key;
            $s_details_data .= "|".$class_main->clmain_u596_xml_decode_char($value);
        }
        continue;

B651_SKIP_DATA_BLOB:
// if the first char is not { cant be json
        if (substr($value,0,2) <> '{"')
        {
            goto B652_SKIP_JSON;
        }
// if the last char is not } cant be json
        if ($value[strlen($value)-1] <> "}")
        {
            goto B652_SKIP_JSON;
        }
        if ($value[strlen($value)-2] <> "}")
        {
            goto B652_SKIP_JSON;
        }
B652_A_DO_JSON:
        $dictionary = json_decode($value, TRUE);
/*    if ($dictionary === NULL)
    {
            goto B652_SKIP_JSON;
    }
    echo $dictionary['fieldname'];
*/
//echo "<br> raw  value";
// print_r($value);

//echo "<br> json dictionary values";
 //print_r(array_keys($dictionary));

//    $dictionary =  clmain_u840_order_json($dictionary);

    $s_json_data_csv_data = "";
    $s_json_data_csv_headings = "";
    foreach ($dictionary as $key1 => $value1)
    {
        $s_fieldname = $key1;
        $s_fieldvalue = " ";
        if (is_array($value1))
        {
            if (in_array('Value', $value1)) {
                if (isset($value1['Value']))
                {
                    $s_fieldvalue = $value1['Value'];
                    GOTO B652_C_MAKE_DEF_DATA;
                }
//echo "<br> gw found value 1 keys";
//                 print_r(array_keys($value1));
//echo "<br> gw found value 1 values";
//                 print_r(array_values($value1));
            }
//echo "<br> ";
//                print_r(" there is no value in the json for field s_fieldname=[]".$s_fieldname."[]  value1=[]".$value1."[]");
//echo "<br> value 1 keys";
//                 print_r(array_keys($value1));
//echo "<br> value 1 values";
//                 print_r(array_values($value1));
//echo "<br> array count  value1= []".count($value1)."[] ";
            foreach ($value1 as $key2 => $value2)
            {
                IF (strtoupper($key2) <> "VALUE")
                {
                    GOTO B652_B_SKIP_VALUE;
                }
                if (is_array($value2))
                {
                    foreach ($value2 as $key3 => $value3)
                    {
                        $s_fieldname = $key1."_".$key3;
                        $s_fieldvalue = $value2[$key3];
                        $s_details_def .= "|REC_".$s_def_prefix."DB_".$key."_".str_replace(" ","_",trim($s_fieldname));
                        $s_details_data .= "|".trim($s_fieldvalue);
                        $s_json_data_csv_data = $s_json_data_csv_data.str_replace(","," ",$s_fieldvalue).",";
                        $s_json_data_csv_headings = $s_json_data_csv_headings.str_replace("_Value","",(str_replace(" ","_",trim($s_fieldname)))).",";
                    }
                    goto B652_X_NEXT_FIELD;
                }else{
                    $s_fieldname = $key1."_".$key2;
                    $s_fieldvalue = $value1[$key2];
                }
B652_B_SKIP_VALUE:
//echo "<br> s_fieldname = []".$s_fieldname."[] ";
//echo "<br> s_fieldvalue = []".$s_fieldvalue."[]";
B652_B_NEXT_FIELD:
            }
        }else{
            $s_fieldvalue = $value1;
        }
B652_C_MAKE_DEF_DATA:
        if (is_array($s_fieldvalue))
        {
            $s_fieldvalue = implode("+",$s_fieldvalue);
        }
        $s_details_def .= "|REC_".$s_def_prefix."DB_".$key."_".str_replace(" ","_",trim($s_fieldname));
        $s_details_data .= "|".trim($s_fieldvalue);
        $s_json_data_csv_data = $s_json_data_csv_data.str_replace(","," ",$s_fieldvalue).",";
        $s_json_data_csv_headings = $s_json_data_csv_headings.str_replace("_Value","",(str_replace(" ","_",trim($s_fieldname)))).",";

B652_X_NEXT_FIELD:
    }
//echo "<br> json end def";
//                 print_r($s_details_def);
//echo "<br> json end data";
//                 print_r($s_details_data);
        $s_details_def .= "|REC_".$s_def_prefix."DB_csv_headings|REC_".$s_def_prefix."DB_csv_data";
        $s_details_data .= "|".trim($s_json_data_csv_headings)."|".trim($s_json_data_csv_data);

B652_SKIP_JSON:
B657_DO_DDEF:
        if (strpos(strtoupper($key),"DETAILS_D") === FALSE)
        {
            $value = trim($value);
            $s_details_def .= "|REC_".$s_def_prefix."".$key;
            $s_details_data .= "|".$value;
        }
        if (strtoupper($key) == "DETAILS_DEF" )
        {
           $ar_dd = explode("|",$value);
           for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
           {
                $s_details_def .= "|REC_".$s_def_prefix."DD_".$ar_dd[$i2];
            }
            continue;
        }
        if (strtoupper($key) == "DETAILS_DATA" )
        {
           $ar_dd = explode("|",$value);
           for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
           {
                $s_details_data .= "|".$ar_dd[$i2];
            }
            GOTO B657_CONTINUE;
        }
B657_CONTINUE:
// check the field counts and add to def or data to make it work
        $array_def = explode("|",$s_details_def);
        $array_data = explode("|",$s_details_data);
        if (count($array_def)<> count($array_data))
        {
//            echo "<br><br>".$sys_function_name."  <br>";
//                echo $sys_function_name."  _ def/data field count error start of list  - def count=".count($array_def)." data count = ".count($array_data)."<br>";
//                echo "s_details_def =".$s_details_def."  <br>";
//                echo "s_details_data =".$s_details_data."  <br>";
                $s_difference = count($array_def) - count($array_data);
                if($s_difference > 0 ){
//                    add to data
                    for ( $i2 = 0; $i2 < abs($s_difference); $i2++)
                    {
                        $s_details_data .= "|?nodata?";
                    }
                }else{
//                    add to def
                    for ( $i2 = 0; $i2 < abs($s_difference); $i2++)
                    {
                        $s_details_def .= "|?nodef?";
                    }
                }

//                echo $sys_function_name." _ def/data field count error end of list <br>";
        }ELSE{
//                   echo "clp_jl ".$sys_function_name." _ def/data ALL GOOD <br>";
        }

        continue;
B657_END:
    }
B658_NEXT:
Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    $sys_function_out = $s_details_def."|^%##%^|".$s_details_data;
    return $sys_function_out;

}
//##########################################################################################################################################################################
function clmain_u830_import_ids($ps_field_in,$ps_return_value,$ps_debug)
{
// 20120626 - not tested - used in import programs to get the company and import ids
// could check them against the dabase and return failure if not valid

    A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";

    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "clmain_u830_import_ids";
    $sys_function_out = "novalueset";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
// gw 20140102 commented to get clean    $dbcnx = $ps_dbcnx;
    $s_details_def = "";
    $s_details_data = "";

B000_START:

    $s_return_value = strtoupper(trim($ps_return_value));
    $s_field_in = trim($ps_field_in);

    $sys_function_out = $s_field_in;
    if (strpos($s_field_in,"_") === false)
    {
        goto Z900_EXIT;
    }
    if ($s_return_value = "COMPANYID")
    {
        $sys_function_out = substr($s_field_in,0,strpos($s_field_in,"_"));
    }
    if ($s_return_value = "IMPORTID")
    {
        $sys_function_out = substr($s_field_in,strpos($s_field_in,"_")+1);
    }

Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name." end of funciton sys_function_out = ".$sys_function_out."<br>";};
    return $sys_function_out;

}
    //##########################################################################################################################################################################
function clmain_fmat_date($ps_datein,$ps_dateinfmat,$ps_dateoutfmat)
{
    $s_dateout = "";
    $s_dateinY = "";
    $s_dateinM = "";
    $s_dateinD = "";
    $s_dateinH = "";
    $s_dateinm = "";
    $s_dateind = "";

    $s_dateout =   $ps_dateoutfmat;

B100_DO_1:
    if (strtoupper($ps_dateinfmat) <> 'Y-M-D-H:I' )
    {
        GOTO B190_SKIP_1;
    }
    If (TRIM($ps_datein) == "")
    {
        $s_dateout = "0";
        goto Z900_EXIT;
    }
    If (substr($ps_datein,4,1) <> "-")
    {
        $s_dateout = "*Error:fd001-cy format error[]".$ps_datein."[]";
        goto Z900_EXIT;
    }
    If (substr($ps_datein,7,1) <> "-")
    {
        $s_dateout = "*Error:fd001-m format error";
        goto Z900_EXIT;
    }
    If (substr($ps_datein,10,1) <> "-")
    {
        $s_dateout = "*Error:fd001-d format error";
        goto Z900_EXIT;
    }
    If (substr($ps_datein,13,1) <> ":")
    {
        $s_dateout = "*Error:fd001-H format error";
        goto Z900_EXIT;
    }
    //=CCYY-MM-DD-HH:SS
    // 0123456789-12345

    $s_dateinY = substr($ps_datein,0,4);
    $s_dateinM  = substr($ps_datein,5,2);
    $s_dateinD  = substr($ps_datein,8,2);;
    $s_dateinH  = substr($ps_datein,11,2);;
    $s_dateinm  = substr($ps_datein,14,2);;
    $s_dateind  = 00;

B190_SKIP_1:
    if (strtoupper($ps_dateinfmat) == 'YMDHIS' ){
        $s_dateinY = substr($ps_datein,1,4);
        $s_dateinM  = substr($ps_datein,5,2);
        $s_dateinD  = substr($ps_datein,7,2);
        $s_dateinH  = substr($ps_datein,9,2);
        $s_dateinm  = substr($ps_datein,11,2);
        $s_dateind  = substr($ps_datein,13,2);
    }

X100_DO_OUT:
    if (strtoupper($ps_dateoutfmat) == 'D/M.Y H:M:S' ){
        $s_dateout =   $s_dateinD."/".$s_dateinM."/".$s_dateinY." ".$s_dateinH.":".$s_dateinm.":".$s_dateind;
    }
    if (strtoupper($ps_dateoutfmat) == 'UTIME' ){
        $s_dateout = STRTOTIME( $s_dateinY.$s_dateinM.$s_dateinD."T".$s_dateinH.$s_dateinm.$s_dateind);
    }

Z900_EXIT:
    return $s_dateout;
}
//##########################################################################################################################################################################
function clmain_set_map_path_n_name($ps_map_path,$ps_filename)
{

//               if (!strpos($ps_filename,"bm_adm")=== false)
//           {
//               echo ("<br>gw clmain_set_map_path_n_name 1 - map =".$ps_filename."<br>");
//           }


    $s_out = "";
    $s_map_path = "";

    $s_map_path = $ps_map_path;
    if (strpos($ps_filename,":")=== false)
    {}ELSE{
        $s_out = $ps_filename;
        goto z900_exit;
    }
    if (substr($ps_filename,0,1) == ".")
    {
        $s_out = $ps_filename;
        goto z900_exit;
    }
    if (substr($ps_filename,0,1) == "/")
    {
        $s_out = $ps_filename;
        goto z900_exit;
    }
    if (substr($ps_filename,0,1) == "\\")
    {
        $s_out = $ps_filename;
        goto z900_exit;
    }
    //ECHO "<br>DOING THE MAP PATH ps_map_path = ".$ps_map_path."  ps_filename = ".$ps_filename." s_out".$s_out;


    if (strpos(strtoupper($ps_filename),"UT_")=== false)
    {}ELSE{
        $s_out = $ps_filename;
        goto z900_exit;
    }
 //   ECHO "<br>eDOING THE MAP PATH ps_map_path = ".$ps_map_path."  ps_filename = ".$ps_filename." s_out".$s_out;

    if (trim($s_map_path," ") =="")
    {
        $s_map_path = $_SESSION["ko_map_path"];
    }
    if (trim($s_map_path) =="")
    {
        $s_map_path = $_SESSION["ko_map_path"];
    }
    if (strtolower(trim($s_map_path)) =="tdxmappath")
    {
        $s_map_path = $_SESSION["ko_map_path"];
    }

    $s_out = $s_map_path.strtolower($ps_filename);
z900_exit:
//    ECHO "DOING THE MAP PATH ps_map_path = ".$ps_map_path."  ps_filename = ".$ps_filename." s_out=".$s_out;


//           if (!strpos($ps_filename,"ud_common")=== false)
//           {
//               die ("gwdie clmain set_map_path=".$s_out);
//           }


    return $s_out;
}


//##########################################################################################################################################################################
    function clmain_set_map_path_n_nameV2($ps_map_path,$ps_filename,$ps_details_def,$ps_details_data,$ps_sessionno)
    {

        $s_out = "";
        $s_map_path = "";

        $s_map_path = $ps_map_path;

        if (strpos($ps_filename,":")=== false)
        {}ELSE{
            $s_out = $ps_filename;
            goto z900_exit;
        }
        if (substr($ps_filename,0,1) == ".")
        {
            $s_out = $ps_filename;
            goto z900_exit;
        }
        if (substr($ps_filename,0,1) == "/")
        {
            $s_out = $ps_filename;
            goto z900_exit;
        }
        if (substr($ps_filename,0,1) == "\\")
        {
            $s_out = $ps_filename;
            goto z900_exit;
        }

        //ECHO "<br>DOING THE MAP PATH ps_map_path = ".$ps_map_path."  ps_filename = ".$ps_filename." s_out".$s_out;


        if (strpos(strtoupper($ps_filename),"UT_")=== false)
        {}ELSE{
            $s_out = $ps_filename;
            goto z900_exit;
        }
        //   ECHO "<br>eDOING THE MAP PATH ps_map_path = ".$ps_map_path."  ps_filename = ".$ps_filename." s_out".$s_out;

        if (trim($s_map_path," ") =="")
        {
            $s_map_path = $_SESSION["ko_map_path"];
        }
        if (trim($s_map_path) =="")
        {
            $s_map_path = $_SESSION["ko_map_path"];
        }
        if (strtolower(trim($s_map_path)) =="ko_map_pathtdxmappath")
        {
            $s_map_path = $_SESSION["ko_map_path"];
        }

        $s_out = $s_map_path.strtolower($ps_filename);
z900_exit:
//    ECHO "DOING THE MAP PATH ps_map_path = ".$ps_map_path."  ps_filename = ".$ps_filename." s_out=".$s_out;


        $s_out = $this->clmain_v200_load_line( $s_out,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"clmain_set_map_path_n_nameV2");

 //       DIE("gwdie clmain namev2 s_out=[]".$s_out."[] ps_map_path. =[]".$ps_map_path."[] ps_filename=[]".$ps_filename."[] ps_details_def=[]". $ps_details_def."[] ps_details_data=[]".$ps_details_data."[]");

//           if (!strpos($ps_filename,"ud_common")=== false)
//           {
//               die ("gwdie clmain set_map_path=".$s_out);
//           }


        return $s_out;
    }

//##########################################################################################################################################################################
function clmain_u840_order_json($event_dictionary)
{
//gw2014020303 dont think this works
    uasort($event_dictionary, function($a, $b)

    {
        // treats fields missing the 'Order' paramater as equal
        if (!isset($a['Order']) || !isset($a['Order']))
        {
            return 0;
        }

        if (intval($a['Order']) == intval($b['Order']))
        {
            return 0;
        }

        return (intval($a['Order']) < intval($b['Order'])) ? -1 : 1;
    });

    return $event_dictionary;
}

    function clmain_u850_format_form_field_backup($ps_dbcnx,$ps_name, $par_field,$ps_globaleventid,$ps_debug,$ps_called_from,$ps_details_def,$ps_details_data,$ps_sessionno)
    {
        A010_START:
        $sys_debug_text = "";
        $sys_debug = "NO";
        $sys_debug = strtoupper($ps_debug);
        $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_called_from;

        IF ($sys_debug !="NO") {
            $sys_debug  = "YES";
        }

        $sys_function_name = "";
        $sys_function_name = "clmain_u850_format_form_field";
        $sys_function_out = "";
        IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

        global $class_sql;
        global $class_main;

        B100_INIT_FIELDS:
        $s_sys_function_name = $sys_function_name;
        $dbcnx = $ps_dbcnx;
        $s_html = "";

        B900_START:
        IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};

        $s_html = "ps_name=[]".$ps_name."[]";
        $s_prompt = $ps_name;
        $s_backgroundcolor = "white";
        $s_value = "";
        $s_type = "";
        $s_order = "";
        $s_tempvalue = "";
        $s_defaultvalue = "(none provided)";
        $s_hints = "(none provided)";
        $s_default_message = "";
        $s_unknown_html = "";

        $ar_fields = array($par_field);
        C100_START:
        /*
        {"SummaryFields":["Fit for duty"]
            ,"FormId":"FFD002"
            ,"Title":"Fit for Duty"
            ,"1.Stationarytp":{"BackgroundColor":"C0FFC0","Order":1,"Prompt":"1. Have I had a 7 hour continuous stationary rest in the past 24 hours?","Value":"1. Have I had a 7 hour continuous stationary rest in the past 24 hours?","Type":"TextPrompt"}
            ,"FormVersion":"1.0.1"
            ,"1.Stationary":{"BackgroundColor":"FFC0C0","DefaultValue":"Yes","Type":"RadioPicker","Hints":"Have I had a 7 hr continuous stationary rest in the past 24 hours?","Value":"Yes","Options":["Yes","No"],"Order":2}
            ,"2.Workedtp":{"BackgroundColor":"C0FFC0","Order":10,"Prompt":"2. Have I worked more than 14 hours in the previous 24 hours?","Value":"2. Have I worked more than 14 hours in the previous 24 hours?","Type":"TextPrompt"}
            ,"2.Worked":{"Options":["Yes","No"],"BackgroundColor":"FFC0C0","Order":11,"Value":"No","DefaultValue":"No","Type":"RadioPicker"}
            ,"3.Sleeptp":{"BackgroundColor":"C0FFC0","Order":20,"Prompt":"3. Have I had adequate uninterrupted good quality sleep since my last shift?","Value":"3. Have I had adequate uninterrupted good quality sleep since my last shift?","Type":"TextPrompt"}
            ,"3.Sleep":{"Options":["Yes","No"],"BackgroundColor":"FFC0C0","Order":21,"Value":"Yes","DefaultValue":"Yes","Type":"RadioPicker"}
            ,"4.Resttp":{"BackgroundColor":"C0FFC0","Order":30,"Prompt":"4. Have I had 24 hour continous rest perio in the last 7 days?","Value":"4. Have I had 24 hour continous rest perio in the last 7 days?","Type":"TextPrompt"}
            ,"4.Rest":{"Options":["Yes","No"],"BackgroundColor":"FFC0C0","Order":31,"Value":"Yes","DefaultValue":"Yes","Type":"RadioPicker"}
            ,"5.policytp":{"BackgroundColor":"C0FFC0","Order":40,"Prompt":"5. Do I comply with Yellow Express policies such as Driver fatigue, Drug and Alcohol?","Value":"5. Do I comply with Yellow Express policies such as Driver fatigue, Drug and Alcohol?","Type":"TextPrompt"}
            ,"5.Policy":{"Options":["Yes","No"],"BackgroundColor":"FFC0C0","Order":41,"Value":"Yes","DefaultValue":"Yes","Type":"RadioPicker"}
            ,"6.fatiguetp":{"BackgroundColor":"C0FFC0","Order":50,"Prompt":"6. Do I comply with relevant fatigue management requirements including the obligation not to put yourself or others at risk by your actions?","Value":"6. Do I comply with relevant fatigue management requirements including the obligation not to put yourself or others at risk by your actions?","Type":"TextPrompt"}
            ,"6.Fatigue":{"Options":
        */

        if (isset($ar_fields['Prompt']))
        {
            echo "PROMPT IS SET: ".$ps_name;
            echo "<br><hr>";
        }

        /* gw testing 20140303_gw
            if (array_key_exists('Prompt', $par_field))
            {
        //        if (isset($par_field['Prompt']))
        //        {
        //            $s_prompt = $par_field['Prompt'];
        //        }
        //       $s_prompt = "prompt not set";
                $s_prompt = $par_field['Prompt'];
        //        echo "GW3a".$ps_name;
        //        echo "<br><hr>";
            }else{
                echo "GW3b".$ps_name;
                echo "<br><hr>";
                print_r($par_field);
                echo "<br><hr>";
                print_r($ar_fields);
                echo "<br><hr>";
                var_dump($ar_fields);
                echo "<br><hr>";
                $s_prompt = "no prompt";

            }
        */
        if (array_key_exists('Prompt', $par_field))
        {
            $s_prompt = $par_field['Prompt'];
        }
        if (array_key_exists('BackgroundColor', $par_field))
        {
            $s_backgroundcolor = $par_field['BackgroundColor'];
        }
        if (array_key_exists('Value', $par_field))
        {
            $s_value = $par_field['Value'];
        }
        if (array_key_exists('Type', $par_field))
        {
            $s_type = $par_field['Type'];
        }
        // if (array_key_exists('Type', $par_field))
        //{
        //        $s_type = $par_field['Type'];
        // }
        if (array_key_exists('DefaultValue', $par_field))
        {
            $s_defaultvalue = $par_field['DefaultValue'];
        }
        if (array_key_exists('Hints', $par_field))
        {
            $s_hints = $par_field['Hints'];
        }

        if($s_type == "Photo")
        {
            $s_value = "noImageFiles";
            if (array_key_exists('ImageFiles', $par_field))
            {
                $s_value = $par_field['ImageFiles'];
            }
        }
        if ($s_value == $s_defaultvalue)
        {
            $s_default_message = "  ( = default)";
        }

//gw20140303
// take the field - get the font, colour, type etc and present the value based on what is found
        $s_html = "<br>s_prompt []".$s_prompt."[]";
        $s_html = $s_html." s_backgroundcolor=[]".$s_backgroundcolor."[]";
        $s_html = $s_html." s_value=[]".$s_value."[]";
        $s_html = $s_html." s_type=[]".$s_type."[]";
        $s_html = $s_html." s_defaultvalue=[]".$s_defaultvalue."[]";
        $s_html = $s_html." s_order=[]".$s_order."[]";
        $s_html = $s_html." field count in data=[]".count($par_field)."[]";
        foreach ( $par_field as $Key => $value ) {
            $s_html = $s_html."<br> Key:[]".$Key."[] value=[]".$value."[]";
        }
        $s_unknown_html = $s_html;
        D1100_TEXTPROMPT:
        IF (strtoupper(trim($s_type)) <> "TEXTPROMPT")
        {
            GOTO D1190_END;
        }
        $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt."</td><tr>";
        GOTO D900_EXIT;
        D1190_END:
        D1200_RADIOPICKER:
        IF (strtoupper(trim($s_type)) <> "RADIOPICKER")
        {
            GOTO D1290_END;
        }
//    $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td>".$s_prompt." = ".$s_value."</td><td>Default=".$s_defaultvalue."</td><td> Hint:".$s_hints."</td><tr>";
        $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td>".$s_prompt." = ".$s_value.$s_default_message."</td><tr>";
        GOTO D900_EXIT;
        D1290_END:

        D1300_SIGNATURE:
        IF (strtoupper(trim($s_type)) <> "SIGNATURE")
        {
            GOTO D1390_END;
        }
        $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." ( Signature ) </td><tr>";
        if ( trim($s_value) == "")
        {
            $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>No Image captured </td><tr>";
            GOTO D900_EXIT;
        }
// get the entries for this event from the attachments table
        $ssql = "SELECT * from  attachments where globaleventid = '".$ps_globaleventid."'  AND filename like '%".substr($s_value,0,strlen($s_value)-4)."%'";
        $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$ssql);
        $s_rec_found = "NO";
        while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
        {
            $s_attachmentpath = $row["AttachmentPath"];
            $s_filename = $row["FileName"];
//must use the database filename        $s_filename = str_replace("_jpg",".jpg",$s_filename);
            $s_rec_found = "YES";
//        $s_value .= '<img src="|%!_session_super_ko_udp_image_home_url_!%|".$s_attachmentpath.$s_filename." width="200" alt=""  align="middle"  border="0" title="Signature" >';
            if (trim($s_attachmentpath == ""))
            {
                $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>Image not yet available,  End of Day sync require. (filename= ".$s_filename." ) </td><tr>";
            }else{
                $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>";
                $s_html = $s_html.'<img src="|%!_session_super_ko_udp_image_home_url_!%|'.$s_attachmentpath.$s_filename.'" width="200" alt=""  align="middle"  border="0" title="Signature" >';
                $s_html = $s_html."</td><tr>";
//            $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>Found image |%!_session_super_ko_udp_image_home_url_!%|".$s_attachmentpath.$s_filename." ( Signature ) </td><tr>";
            }
        }
        if ($s_rec_found <> "YES" )
        {
            $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>No signatures located should have file ".$s_value."</td><tr>";
            $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>ssql=[]".$ssql."[]</td><tr>";
        }
//    $s_html = $s_html.$s_unknown_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>ssql=[]".$ssql."[]</td><tr>";
        $s_html = $this->clmain_v200_load_line( $s_html,$ps_details_def,$ps_details_data,"no",$ps_sessionno,$sys_function_name."clmain_u850_D1300");

        GOTO D900_EXIT;
        D1390_END:
        D1400_TEXTFIELD:
        IF (strtoupper(trim($s_type)) <> "TEXTFIELD")
        {
            GOTO D1490_END;
        }
        $s_tempvalue = $s_value;
        if (trim($s_tempvalue) == "")
        {
            $s_tempvalue = "(no value entered or spaces)";
        }
        $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." = ".$s_tempvalue.$s_default_message."</td><tr>";
        GOTO D900_EXIT;
        D1490_END:
        D1500_CHECKBOX:
        IF (strtoupper(trim($s_type)) <> "CHECKBOX")
        {
            GOTO D1590_END;
        }
        if ($s_value == "1")
        {
            $s_tempvalue = $s_value."( Checked meaning YES)";
        }
        if ($s_value <> "1")
        {
            $s_tempvalue = $s_value."( Unchecked meaning NO or not answered )";
        }
        $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." = ".$s_tempvalue.$s_default_message."</td><tr>";
        GOTO D900_EXIT;
        D1590_END:
        D1600_SERVERACTION:
        IF (strtoupper(trim($s_prompt)) <> "SERVERACTION")
        {
            GOTO D1609_END;
        }
        $s_tempvalue = "serveraction not set";
        if (array_key_exists('ServerAction', $par_field))
        {
            $s_tempvalue = $par_field['ServerAction'];
        }

        $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." = ".$s_tempvalue.$s_default_message."</td><tr>";
        GOTO D900_EXIT;
        D1609_END:

        D1700_HIDDENVALUE:
        IF (strtoupper(trim($s_prompt)) <> "HIDDENVALUE")
        {
            GOTO D1709_END;
        }
        $s_tempvalue = "hidden value not set1";
        if (array_key_exists('Hidden', $par_field))
        {
            $s_tempvalue = $par_field['Hidden'];
        }

        $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." = ".$s_tempvalue.$s_default_message."</td><tr>";
        GOTO D900_EXIT;
        D1709_END:

        D1800_BUTTON:
        IF (strtoupper(trim($s_type)) <> "BUTTON")
        {
            GOTO D1809_END;
        }
        $s_tempvalue = "hidden value not set1";

        $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>Button=".$s_prompt."</td><tr>";
//    $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>V1 UNKNOWN TYPE: ".$s_type." = ".$s_html."<br>clmain_u850-report to development team</td><tr>";
        GOTO D900_EXIT;
        D1809_END:

        D1900_DatePicker:
        IF (strtoupper(trim($s_prompt)) <> strtoupper("DatePicker"))
        {
            GOTO D1909_END;
        }
        $s_tempvalue = date("d-M-Y D",$s_value+0);

        $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." = ".$s_tempvalue.$s_default_message."</td><tr>";
        GOTO D900_EXIT;
        D1909_END:

        D2000_timePicker:
        IF (strtoupper(trim($s_prompt)) <> strtoupper("timePicker"))
        {
            GOTO D2009_END;
        }
        $s_tempvalue = date("H:i:s",$s_value+0)." = ".date("g:i:s A",$s_value+0);

        $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." = ".$s_tempvalue.$s_default_message."</td><tr>";
        GOTO D900_EXIT;
        D2009_END:


        D2100_datetimePicker:
        IF (strtoupper(trim($s_prompt)) <> strtoupper("datetimePicker"))
        {
            GOTO D2109_END;
        }
        $s_tempvalue = date("d-M-Y D H:i:s",$s_value+0);

        $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." = ".$s_tempvalue.$s_default_message."</td><tr>";
        GOTO D900_EXIT;
        D2109_END:

        D2200_OPTIONPICKER:
        IF (strtoupper(trim($s_prompt)) <> strtoupper("optionpicker"))
        {
            GOTO D2209_END;
        }
//    $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>V1 UNKNOWN TYPE: ".$s_type." = ".$s_html."<br>clmain_u850-report to development team";


        if (trim($s_tempvalue) == $s_defaultvalue)
        {
            $s_default_message = "  ( = default)";
        }

//    $s_html = $s_html."</td><tr>";
        $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." = ".$s_tempvalue.$s_default_message."</td><tr>";

        GOTO D900_EXIT;
        D2209_END:


        D2300_CLIENTPICKER:
        IF (strtoupper(trim($s_prompt)) <> strtoupper("CLIENTPICKER"))
        {
            GOTO D2309_END;
        }
//    $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>V1 UNKNOWN TYPE: ".$s_type." = ".$s_html."<br>clmain_u850-report to development team";

        foreach ( $s_value as $Key1 => $value1 ) {
            $s_html = $s_html."<br> Key:[]".$Key1."[] value=[]".$value1."[]";
            $s_tempvalue = $s_tempvalue.$value1."  ";
        }
        $s_tempvalue = "";
        $s_tempvalue = $s_tempvalue."<br>Name:".$s_value['name']."<br>";
        $s_tempvalue = $s_tempvalue."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$s_value['addressLine1']."<br>";
        $s_tempvalue = $s_tempvalue."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$s_value['addressLine2']."<br>";
        $s_tempvalue = $s_tempvalue."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$s_value['suburb']."  ".$s_value['postcode']."<br>";
        $s_tempvalue = $s_tempvalue."Phone:".$s_value['phone']."<br>";


        $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." = ".$s_tempvalue.$s_default_message."</td><tr>";

        GOTO D900_EXIT;
        D2309_END:

        D2400_PHOTO:
        IF (strtoupper(trim($s_type)) <> "PHOTO")
        {
            GOTO D2490_END;
        }
        $s_html= "";
//    $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>V1 UNKNOWN TYPE: ".$s_type." = ".$s_html."<br>clmain_u850-report to development team</td><tr>";
        $s_cameratype = "";
        foreach ( $s_value as $Key1 => $value1 ) {
//        $s_html = $s_html."<br> Key:[]".$Key1."[] value=[]".$value1."[]";
            $s_tempvalue = $s_tempvalue.$value1."  ";
        }
//    $s_html = $s_html.$s_tempvalue;

        if (array_key_exists('CameraType', $par_field))
        {
            $s_cameratype = " - ".$par_field['CameraType']." ";
        }


        $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." ( Photo ".$s_cameratype.") </td><tr>";
// get the entries for this event from the attachments table
        $ssql = "SELECT * from  attachments where globaleventid = '".$ps_globaleventid."'  AND filename = '".$s_tempvalue."'";
//    $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>ssql=[]".$ssql."[]</td><tr>";


        $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$ssql);
        $s_rec_found = "NO";
        while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
        {
            $s_attachmentpath = $row["AttachmentPath"];
            $s_filename = $row["FileName"];
//must use the database filename        $s_filename = str_replace("_jpg",".jpg",$s_filename);
            $s_rec_found = "YES";
//        $s_value .= '<img src="|%!_session_super_ko_udp_image_home_url_!%|".$s_attachmentpath.$s_filename." width="200" alt=""  align="middle"  border="0" title="Signature" >';
            if (trim($s_attachmentpath == ""))
            {
                $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>Image not yet available,  End of Day sync require. (filename= ".$s_filename." ) </td><tr>";
            }else{
                $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>";
                $s_html = $s_html.'<img src="|%!_session_super_ko_udp_image_home_url_!%|'.$s_attachmentpath.$s_filename.'" width="200" alt=""  align="middle"  border="0" title="Signature" >';
                $s_html = $s_html."</td><tr>";
//            $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>Found image |%!_session_super_ko_udp_image_home_url_!%|".$s_attachmentpath.$s_filename." ( Signature ) </td><tr>";
//            $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>ssql=[]".$ssql."[]</td><tr>";
            }
        }
        if ($s_rec_found <> "YES" )
        {
            $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>No photos located should have file ".$s_value."</td><tr>";
            $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>ssql=[]".$ssql."[]</td><tr>";
        }
        $s_html = $this->clmain_v200_load_line( $s_html,$ps_details_def,$ps_details_data,"no",$ps_sessionno,$sys_function_name."clmain_u850_D2400");

        GOTO D900_EXIT;
        D2490_END:

        D2500_IMAGE:
        IF (strtoupper(trim($s_type)) <> "IMAGE")
        {
            GOTO D2590_END;
        }
        $s_html= "";
        $s_ImageName = "";
        if (array_key_exists('ImageName', $par_field))
        {
            $s_ImageName = " - ".$par_field['ImageName']." ";
        }

        $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." ( Display Image ".$s_ImageName.") </td><tr>";

        GOTO D900_EXIT;
        D2590_END:

        D8800_DEFAULT:
        if( is_array($s_value))
        {
            $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." = has an array value</td><tr>";
            GOTO D900_EXIT;
        }

        $s_tempvalue = $s_value;
        IF (TRIM($s_tempvalue) == "")
        {
            $s_tempvalue = "*No value entered*";
        }
        $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." = ".$s_tempvalue.$s_default_message."</td><tr>";
        IF (TRIM($s_tempvalue) == "*No value entered*")
        {
            $s_html = $s_html."<tr>".$s_unknown_html."</tr>";
        }
        GOTO D900_EXIT;
        D8809_END:

        D890_UNKNOWN:
        $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>V1 UNKNOWN TYPE: ".$s_type." = ".$s_html."<br>clmain_u850-report to development team</td><tr>";
        D900_EXIT:

        Z900_EXIT:
        $sys_function_out = $s_html;
        return $sys_function_out;
    }

function clmain_u850_format_form_field($ps_dbcnx,$ps_name, $par_field,$ps_globaleventid,$ps_showfields,$ps_html_map,$ps_debug,$ps_called_from,$ps_details_def,$ps_details_data,$ps_sessionno)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NO";
    $sys_debug = strtoupper($ps_debug);
    $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_called_from;

    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
    }

    $sys_function_name = "";
    $sys_function_name = "clmain_u850_format_form_field";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;
    $s_html = "";
    $s_html_map = $ps_html_map;

B900_START:
    IF ($sys_debug <> "NO"){echo $sys_debug_text."=".$sys_function_name." B000_START";};

    $s_html = "ps_name=[]".$ps_name."[]";
    $s_prompt = $ps_name;
    $s_backgroundcolor = "white";
    $s_value = "";
    $s_type = "";
    $s_order = "";
    $s_tempvalue = "";
    $s_defaultvalue = "(none provided)";
    $s_hints = "(none provided)";
    $s_default_message = "";
    $s_unknown_html = "";
    $s_fieldheading = "";
    $s_fielddisplay = "";

    $ar_fields = array($par_field);
C100_START:
    /*
    {"SummaryFields":["Fit for duty"]
        ,"FormId":"FFD002"
        ,"Title":"Fit for Duty"
        ,"1.Stationarytp":{"BackgroundColor":"C0FFC0","Order":1,"Prompt":"1. Have I had a 7 hour continuous stationary rest in the past 24 hours?","Value":"1. Have I had a 7 hour continuous stationary rest in the past 24 hours?","Type":"TextPrompt"}
        ,"FormVersion":"1.0.1"
        ,"1.Stationary":{"BackgroundColor":"FFC0C0","DefaultValue":"Yes","Type":"RadioPicker","Hints":"Have I had a 7 hr continuous stationary rest in the past 24 hours?","Value":"Yes","Options":["Yes","No"],"Order":2}
        ,"2.Workedtp":{"BackgroundColor":"C0FFC0","Order":10,"Prompt":"2. Have I worked more than 14 hours in the previous 24 hours?","Value":"2. Have I worked more than 14 hours in the previous 24 hours?","Type":"TextPrompt"}
        ,"2.Worked":{"Options":["Yes","No"],"BackgroundColor":"FFC0C0","Order":11,"Value":"No","DefaultValue":"No","Type":"RadioPicker"}
        ,"3.Sleeptp":{"BackgroundColor":"C0FFC0","Order":20,"Prompt":"3. Have I had adequate uninterrupted good quality sleep since my last shift?","Value":"3. Have I had adequate uninterrupted good quality sleep since my last shift?","Type":"TextPrompt"}
        ,"3.Sleep":{"Options":["Yes","No"],"BackgroundColor":"FFC0C0","Order":21,"Value":"Yes","DefaultValue":"Yes","Type":"RadioPicker"}
        ,"4.Resttp":{"BackgroundColor":"C0FFC0","Order":30,"Prompt":"4. Have I had 24 hour continous rest perio in the last 7 days?","Value":"4. Have I had 24 hour continous rest perio in the last 7 days?","Type":"TextPrompt"}
        ,"4.Rest":{"Options":["Yes","No"],"BackgroundColor":"FFC0C0","Order":31,"Value":"Yes","DefaultValue":"Yes","Type":"RadioPicker"}
        ,"5.policytp":{"BackgroundColor":"C0FFC0","Order":40,"Prompt":"5. Do I comply with Yellow Express policies such as Driver fatigue, Drug and Alcohol?","Value":"5. Do I comply with Yellow Express policies such as Driver fatigue, Drug and Alcohol?","Type":"TextPrompt"}
        ,"5.Policy":{"Options":["Yes","No"],"BackgroundColor":"FFC0C0","Order":41,"Value":"Yes","DefaultValue":"Yes","Type":"RadioPicker"}
        ,"6.fatiguetp":{"BackgroundColor":"C0FFC0","Order":50,"Prompt":"6. Do I comply with relevant fatigue management requirements including the obligation not to put yourself or others at risk by your actions?","Value":"6. Do I comply with relevant fatigue management requirements including the obligation not to put yourself or others at risk by your actions?","Type":"TextPrompt"}
        ,"6.Fatigue":{"Options":
    */

    if (isset($ar_fields['Prompt']))
    {
        echo "PROMPT IS SET: ".$ps_name;
        echo "<br><hr>";
    }

/* gw testing 20140303_gw
    if (array_key_exists('Prompt', $par_field))
    {
//        if (isset($par_field['Prompt']))
//        {
//            $s_prompt = $par_field['Prompt'];
//        }
//       $s_prompt = "prompt not set";
        $s_prompt = $par_field['Prompt'];
//        echo "GW3a".$ps_name;
//        echo "<br><hr>";
    }else{
        echo "GW3b".$ps_name;
        echo "<br><hr>";
        print_r($par_field);
        echo "<br><hr>";
        print_r($ar_fields);
        echo "<br><hr>";
        var_dump($ar_fields);
        echo "<br><hr>";
        $s_prompt = "no prompt";

    }
*/
    if (array_key_exists('Prompt', $par_field))
    {
        $s_prompt = $par_field['Prompt'];
    }
    if (array_key_exists('BackgroundColor', $par_field))
    {
            $s_backgroundcolor = $par_field['BackgroundColor'];
    }
    if (array_key_exists('Value', $par_field))
    {
            $s_value = $par_field['Value'];
    }
    if (array_key_exists('Type', $par_field))
    {
            $s_type = $par_field['Type'];
    }
    // if (array_key_exists('Type', $par_field))
    //{
    //        $s_type = $par_field['Type'];
   // }
    if (array_key_exists('DefaultValue', $par_field))
    {
        $s_defaultvalue = $par_field['DefaultValue'];
    }
    if (array_key_exists('Hints', $par_field))
    {
        $s_hints = $par_field['Hints'];
    }

    if(trim($s_type == ""))
    {
        $s_type = $s_prompt;
    }

    if($s_type == "Photo")
    {
        $s_value = "noImageFiles";
        if (array_key_exists('ImageFiles', $par_field))
        {
            $s_value = $par_field['ImageFiles'];
        }
    }
    if ($s_value == $s_defaultvalue)
    {
        $s_default_message = "  ( = default)";
    }

//gw20140303
// take the field - get the font, colour, type etc and present the value based on what is found
    $s_html = "<br>s_prompt []".$s_prompt."[]";
    $s_html = $s_html." s_backgroundcolor=[]".$s_backgroundcolor."[]";
    $s_html = $s_html." s_value=[]".$s_value."[]";
    $s_html = $s_html." s_type=[]".$s_type."[]";
    $s_html = $s_html." s_defaultvalue=[]".$s_defaultvalue."[]";
    $s_html = $s_html." s_order=[]".$s_order."[]";
    $s_html = $s_html." field count in data=[]".count($par_field)."[]";
    foreach ( $par_field as $Key => $value ) {
        $s_html = $s_html."<br> Key:[]".$Key."[] value=[]".$value."[]";
    }
    $s_unknown_html = $s_html;
D1100_TEXTPROMPT:
    IF (strtoupper(trim($s_type)) <> "TEXTPROMPT")
    {
        GOTO D1190_END;
    }
    $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt."</td><tr>";
    $s_fieldheading = "Prompt";
    $s_fielddisplay = $s_prompt;
    GOTO D900_EXIT;
D1190_END:
D1200_RADIOPICKER:
    IF (strtoupper(trim($s_type)) <> "RADIOPICKER")
    {
        GOTO D1290_END;
    }
//    $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td>".$s_prompt." = ".$s_value."</td><td>Default=".$s_defaultvalue."</td><td> Hint:".$s_hints."</td><tr>";
//    $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td>".$s_prompt." = ".$s_value.$s_default_message."</td><tr>";
    $s_fieldheading = $s_prompt;
    $s_fielddisplay = $s_value.$s_default_message;
    GOTO D900_EXIT;
D1290_END:

D1300_SIGNATURE:
    IF (strtoupper(trim($s_type)) <> "SIGNATURE")
    {
        GOTO D1390_END;
    }
//    $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." ( Signature ) </td><tr>";
    $s_fieldheading = $s_prompt." ( Signature )";
    if ( trim($s_value) == "")
    {
        $s_fielddisplay = "No Image captured ";
//        $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>No Image captured </td><tr>";
        GOTO D900_EXIT;
    }
// get the entries for this event from the attachments table
    $ssql = "SELECT * from  attachments where globaleventid = '".$ps_globaleventid."'  AND filename like '%".substr($s_value,0,strlen($s_value)-4)."%'";
    $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$ssql);
    $s_rec_found = "NO";
    while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
    {
        $s_attachmentpath = $row["AttachmentPath"];
        $s_filename = $row["FileName"];
//must use the database filename        $s_filename = str_replace("_jpg",".jpg",$s_filename);
        $s_rec_found = "YES";
//        $s_value .= '<img src="|%!_session_super_ko_udp_image_home_url_!%|".$s_attachmentpath.$s_filename." width="200" alt=""  align="middle"  border="0" title="Signature" >';
        if (trim($s_attachmentpath == ""))
        {
            $s_fielddisplay = "Image not yet available,  End of Day sync require. (filename= ".$s_filename." )";
//            $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>Image not yet available,  End of Day sync require. (filename= ".$s_filename." ) </td><tr>";
        }else{
            $s_fielddisplay = '<img src="|%!_session_super_ko_udp_image_home_url_!%|'.$s_attachmentpath.$s_filename.'" width="200" alt=""  align="middle"  border="0" title="Signature" >';
//            $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>";
//            $s_html = $s_html.'<img src="|%!_session_super_ko_udp_image_home_url_!%|'.$s_attachmentpath.$s_filename.'" width="200" alt=""  align="middle"  border="0" title="Signature" >';
//            $s_html = $s_html."</td><tr>";
//            $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>Found image |%!_session_super_ko_udp_image_home_url_!%|".$s_attachmentpath.$s_filename." ( Signature ) </td><tr>";
        }
    }
    if ($s_rec_found <> "YES" )
    {
        $s_fielddisplay = "No signatures located should have file ".$s_value."<br>ssql=[]".$ssql."[]";
//        $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>No signatures located should have file ".$s_value."</td><tr>";
//        $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>ssql=[]".$ssql."[]</td><tr>";
    }
//    $s_html = $s_html.$s_unknown_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>ssql=[]".$ssql."[]</td><tr>";
//    $s_html = $this->clmain_v200_load_line( $s_html,$ps_details_def,$ps_details_data,"no",$ps_sessionno,$sys_function_name."clmain_u850_D1300");
    $s_fielddisplay = $this->clmain_v200_load_line( $s_fielddisplay,$ps_details_def,$ps_details_data,"no",$ps_sessionno,$sys_function_name."clmain_u850_D1300");

    GOTO D900_EXIT;
D1390_END:
D1400_TEXTFIELD:
    IF (strtoupper(trim($s_type)) <> "TEXTFIELD")
    {
        GOTO D1490_END;
    }
    $s_tempvalue = $s_value;
    if (trim($s_tempvalue) == "")
    {
        $s_tempvalue = "(no value entered or spaces)";
    }
//    $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." = ".$s_tempvalue.$s_default_message."</td><tr>";
    $s_fieldheading = $s_prompt;
    $s_fielddisplay = $s_tempvalue.$s_default_message;
    GOTO D900_EXIT;
D1490_END:
D1500_CHECKBOX:
    IF (strtoupper(trim($s_type)) <> "CHECKBOX")
    {
        GOTO D1590_END;
    }
    if ($s_value == "1")
    {
        $s_tempvalue = $s_value."( Checked meaning YES)";
    }
    if ($s_value <> "1")
    {
        $s_tempvalue = $s_value."( Unchecked meaning NO or not answered )";
    }
//    $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." = ".$s_tempvalue.$s_default_message."</td><tr>";
    $s_fieldheading = $s_prompt;
    $s_fielddisplay = $s_tempvalue.$s_default_message;
    GOTO D900_EXIT;
D1590_END:
D1600_SERVERACTION:
    IF (strtoupper(trim($s_type)) <> "SERVERACTION")
    {
        GOTO D1609_END;
    }
    $s_tempvalue = "serveraction not set";
    if (array_key_exists('ServerAction', $par_field))
    {
        $s_tempvalue = $par_field['ServerAction'];
    }

//    $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." = ".$s_tempvalue.$s_default_message."</td><tr>";
    $s_fieldheading = $s_prompt;
    $s_fielddisplay = $s_tempvalue.$s_default_message;
    GOTO D900_EXIT;
D1609_END:
    
D1700_HIDDENVALUE:
    IF (strtoupper(trim($s_type)) <> "HIDDENVALUE")
    {
        GOTO D1709_END;
    }
    $s_tempvalue = "hidden value not set1";
    if (array_key_exists('Hidden', $par_field))
    {
        $s_tempvalue = $par_field['Hidden'];
    }

//    $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." = ".$s_tempvalue.$s_default_message."</td><tr>";
    $s_fieldheading = $s_prompt;
    $s_fielddisplay = $s_tempvalue.$s_default_message;
    GOTO D900_EXIT;
D1709_END:

D1800_BUTTON:
    IF (strtoupper(trim($s_type)) <> "BUTTON")
    {
        GOTO D1809_END;
    }
    $s_tempvalue = "hidden value not set1";

    $s_fieldheading = $s_prompt;
//    $s_fielddisplay = $s_tempvalue.$s_default_message;

//    $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>Button=".$s_prompt."</td><tr>";
//    $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>V1 UNKNOWN TYPE: ".$s_type." = ".$s_html."<br>clmain_u850-report to development team</td><tr>";
    GOTO D900_EXIT;
D1809_END:

D1900_DatePicker:
    IF (strtoupper(trim($s_type)) <> strtoupper("DatePicker"))
    {
        GOTO D1909_END;
    }
    $s_tempvalue = date("d-M-Y D",$s_value+0);

//    $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." = ".$s_tempvalue.$s_default_message."</td><tr>";
    $s_fieldheading = $s_prompt;
    $s_fielddisplay = $s_tempvalue.$s_default_message;
    GOTO D900_EXIT;
D1909_END:

D2000_timePicker:
    IF (strtoupper(trim($s_prompt)) <> strtoupper("timePicker"))
    {
        GOTO D2009_END;
    }
    $s_tempvalue = date("H:i:s",$s_value+0)." = ".date("g:i:s A",$s_value+0);

//    $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." = ".$s_tempvalue.$s_default_message."</td><tr>";
    $s_fieldheading = $s_prompt;
    $s_fielddisplay = $s_tempvalue.$s_default_message;
    GOTO D900_EXIT;
D2009_END:


D2100_datetimePicker:
    IF (strtoupper(trim($s_type)) <> strtoupper("datetimePicker"))
    {
        GOTO D2109_END;
    }
    $s_tempvalue = date("d-M-Y D H:i:s",$s_value+0);

//    $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." = ".$s_tempvalue.$s_default_message."</td><tr>";
    $s_fieldheading = $s_prompt;
    $s_fielddisplay = $s_tempvalue.$s_default_message;

    GOTO D900_EXIT;
D2109_END:

D2200_OPTIONPICKER:
    IF (strtoupper(trim($s_type)) <> strtoupper("optionpicker"))
    {
        GOTO D2209_END;
    }
//    $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>V1 UNKNOWN TYPE: ".$s_type." = ".$s_html."<br>clmain_u850-report to development team";
    if( is_array($s_value))
    {
        foreach ( $s_value as $Key1 => $value1 ) {
            $s_html = $s_html."<br> Key:[]".$Key1."[] value=[]".$value1."[]";
            $s_tempvalue = $s_tempvalue.$value1."  ";
        }
    }


//    $s_tempvalue = $s_value;
    if (trim($s_tempvalue) == $s_defaultvalue)
      {
            $s_default_message = "  ( = default)";
      }
    if (trim($s_tempvalue) == "")
    {
        $s_default_message = "  * no option selected * ";
    }

//    $s_html = $s_html."</td><tr>";
//    $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." = ".$s_tempvalue.$s_default_message."</td><tr>";
    $s_fieldheading = $s_prompt;
    $s_fielddisplay = $s_tempvalue.$s_default_message;

    GOTO D900_EXIT;
D2209_END:


D2300_CLIENTPICKER:
    IF (strtoupper(trim($s_type)) <> strtoupper("CLIENTPICKER"))
    {
        GOTO D2309_END;
    }
//    $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>V1 UNKNOWN TYPE: ".$s_type." = ".$s_html."<br>clmain_u850-report to development team";

    foreach ( $s_value as $Key1 => $value1 ) {
        $s_html = $s_html."<br> Key:[]".$Key1."[] value=[]".$value1."[]";
        $s_tempvalue = $s_tempvalue.$value1."  ";
    }
    $s_tempvalue = "";
    $s_tempvalue = $s_tempvalue."Name:".$s_value['name']."<br>";
    $s_tempvalue = $s_tempvalue."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$s_value['addressLine1']."<br>";
    $s_tempvalue = $s_tempvalue."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$s_value['addressLine2']."<br>";
    $s_tempvalue = $s_tempvalue."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$s_value['suburb']."  ".$s_value['postcode']."<br>";
    $s_tempvalue = $s_tempvalue."Phone:".$s_value['phone']."<br>";


//    $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." = ".$s_tempvalue.$s_default_message."</td><tr>";
    $s_fieldheading = $s_prompt;
    $s_fielddisplay = $s_tempvalue;

    GOTO D900_EXIT;
D2309_END:

D2400_PHOTO:
    IF (strtoupper(trim($s_type)) <> "PHOTO")
    {
        GOTO D2490_END;
    }
    $s_html= "";
//    $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>V1 UNKNOWN TYPE: ".$s_type." = ".$s_html."<br>clmain_u850-report to development team</td><tr>";
    $s_cameratype = "";
    foreach ( $s_value as $Key1 => $value1 ) {
//        $s_html = $s_html."<br> Key:[]".$Key1."[] value=[]".$value1."[]";
        $s_tempvalue = $s_tempvalue.$value1."  ";
    }
//    $s_html = $s_html.$s_tempvalue;

    if (array_key_exists('CameraType', $par_field))
    {
        $s_cameratype = " - ".$par_field['CameraType']." ";
    }

    $s_fieldheading = $s_prompt." ( Photo ".$s_cameratype.")";

 //   $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." ( Photo ".$s_cameratype.") </td><tr>";
// get the entries for this event from the attachments table
    $ssql = "SELECT * from  attachments where globaleventid = '".$ps_globaleventid."'  AND filename = '".$s_tempvalue."'";
//    $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>ssql=[]".$ssql."[]</td><tr>";


    $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$ssql);
    $s_rec_found = "NO";
    while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
    {
        $s_attachmentpath = $row["AttachmentPath"];
        $s_filename = $row["FileName"];
//must use the database filename        $s_filename = str_replace("_jpg",".jpg",$s_filename);
        $s_rec_found = "YES";
//        $s_value .= '<img src="|%!_session_super_ko_udp_image_home_url_!%|".$s_attachmentpath.$s_filename." width="200" alt=""  align="middle"  border="0" title="Signature" >';
        if (trim($s_attachmentpath == ""))
        {
            $s_fielddisplay = "Image not yet available,  End of Day sync require. (filename= ".$s_filename." )";
//            $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>Image not yet available,  End of Day sync require. (filename= ".$s_filename." ) </td><tr>";
        }else{
            $s_fielddisplay = '<img src="|%!_session_super_ko_udp_image_home_url_!%|'.$s_attachmentpath.$s_filename.'" width="200" alt=""  align="middle"  border="0" title="Signature" >';
//            $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>";
//            $s_html = $s_html.'<img src="|%!_session_super_ko_udp_image_home_url_!%|'.$s_attachmentpath.$s_filename.'" width="200" alt=""  align="middle"  border="0" title="Signature" >';
//            $s_html = $s_html."</td><tr>";
//            $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>Found image |%!_session_super_ko_udp_image_home_url_!%|".$s_attachmentpath.$s_filename." ( Signature ) </td><tr>";
//            $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>ssql=[]".$ssql."[]</td><tr>";
        }
    }
    if ($s_rec_found <> "YES" )
    {
        $s_fielddisplay = "No photos located should have file ".$s_value."<br>ssql=[]".$ssql."[]";
//        $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>No photos located should have file ".$s_value."</td><tr>";
//        $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>ssql=[]".$ssql."[]</td><tr>";
    }
//    $s_html = $this->clmain_v200_load_line( $s_html,$ps_details_def,$ps_details_data,"no",$ps_sessionno,$sys_function_name."clmain_u850_D2400");
    $s_fielddisplay = $this->clmain_v200_load_line( $s_fielddisplay,$ps_details_def,$ps_details_data,"no",$ps_sessionno,$sys_function_name."clmain_u850_D2400");

    GOTO D900_EXIT;
D2490_END:

D2500_IMAGE:
    IF (strtoupper(trim($s_type)) <> "IMAGE")
    {
        GOTO D2590_END;
    }

    $s_fieldheading =  "Display Image";

    $s_ImageName = "";
    if (array_key_exists('ImageName', $par_field))
    {
        $s_ImageName = $par_field['ImageName']." ";
    }
    $s_fielddisplay = $s_ImageName;

/*
    $s_html= "";
    $s_ImageName = "";
    if (array_key_exists('ImageName', $par_field))
    {
        $s_ImageName = " - ".$par_field['ImageName']." ";
    }

    $s_html = $s_html."<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." ( Display Image ".$s_ImageName.") </td><tr>";
*/
    GOTO D900_EXIT;
D2590_END:


    $s_fieldheading = $s_prompt;
    $s_fielddisplay = " = has an array value ";

D8800_DEFAULT:
    $s_fieldheading = $s_prompt;

    if( is_array($s_value))
    {
//        $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." = has an array value</td><tr>";
        $s_fielddisplay = " = has an array value ";
        GOTO D900_EXIT;
    }
    IF (TRIM($s_value) == "")
    {
        $s_fielddisplay = "*no value entered* ";
        GOTO D900_EXIT;
    }

    $s_fielddisplay = $s_value.$s_default_message;

/*
    $s_tempvalue = $s_value;
    IF (TRIM($s_tempvalue) == "")
    {
        $s_tempvalue = "*No value entered*";
    }


//    $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>".$s_prompt." = ".$s_tempvalue.$s_default_message."</td><tr>";
    IF (TRIM($s_tempvalue) == "*No value entered*")
    {
//        $s_html = $s_html."<tr>".$s_unknown_html."</tr>";
    }
*/
    GOTO D900_EXIT;
D8809_END:

D890_UNKNOWN:
    $s_html = "<tr bgcolor='".$s_backgroundcolor."'><td colspan='10'>V1 UNKNOWN TYPE: ".$s_type." = ".$s_html."<br>clmain_u850-report to development team</td><tr>";
D900_EXIT:
    if(trim($s_html_map) == "")
    {
        goto D950_HTML;
    }
    $s_html = $ps_html_map;
    $s_html = str_replace("|%!_fieldheading_!%|",$s_fieldheading,$s_html);
    $s_html = str_replace("|%!_fielddisplay_!%|",$s_fielddisplay,$s_html);
    GOTO D990_END;

D950_HTML:


D990_END:


//    $s_html = $s_html." fields=[]".$ps_showfields."[] map=[]".$ps_html_map."[]";

Z900_EXIT:
    $sys_function_out = $s_html;
    return $sys_function_out;
}
//########################################################################################################################################
function clmain_u860_api_security($ps_action,$ps_token,$ps_client_name,$ps_site_id,$ps_dbcnx,$ps_debug,$ps_called_from,$ps_details_def,$ps_details_data,$ps_sessionno)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NODEBUG";
    $sys_debug = strtoupper($ps_debug);

    IF ($sys_debug !="NODEBUG") {
        $sys_debug  = "YES";
        $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_called_from;
    }

    $sys_function_name = "";
    $sys_function_name = "clmain_u860_api_security";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_sql;
    global $class_main;

B100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;

    $s_action = strtoupper(trim($ps_action));
    $s_token = strtoupper(trim($ps_token));
    $s_client_name = strtoupper(trim($ps_client_name));
    $s_site_id = strtoupper(trim($ps_site_id));

    $sys_function_out = "Unknown{action=".$s_action.",token=".$s_token.",client_name=".$s_client_name.",site_id=".$s_site_id."}";
B900_START:
    IF ($sys_debug == "YES"){echo $sys_debug_text."=".$sys_function_name." B000_START";};

C100_MAKE_TOKEN:
    if ($s_action <> "MAKETOKEN")
    {
        GOTO C900_END;
    }
   // $sys_function_out = uniqid('tkn_',true );

    $sys_function_out = "tkn_".time().substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 1).substr(md5(time()),1);
// put token in the data base for the client and site
    GOTO Z900_EXIT;
C900_END:

D100_GET_CURRENTTOKEN:
    if ($s_action <> "GETCURRENTTOKEN")
    {
        GOTO D900_END;
    }
    // $sys_function_out = uniqid('tkn_',true );

    $sys_function_out = "tst_".time();
// get the token for the sitename + site id from the database
    GOTO Z900_EXIT;
D900_END:
Z900_EXIT:
    return $sys_function_out;

}

//########################################################################################################################################
function clmain_u870_set_path($ps_path,$ps_dbcnx,$ps_debug,$ps_called_from,$ps_details_def,$ps_details_data,$ps_sessionno)
{
A010_START:
    $sys_debug_text = "";
    $sys_debug = "NODEBUG";
    $sys_debug = strtoupper($ps_debug);

    IF ($sys_debug !="NODEBUG") {
        $sys_debug  = "YES";
        $sys_debug_text = "<br>".$ps_debug." fromwhere=".$ps_called_from;
    }

    $sys_function_name = "";
    $sys_function_name = "clmain_u870_set_path";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};

    global $class_main;

A100_INIT_FIELDS:
    $s_sys_function_name = $sys_function_name;
    $dbcnx = $ps_dbcnx;
    $s_filepath = "";
B100_START:
    if (isset($_SESSION['ko_output_file_path']))
    {
    }else{
        $_SESSION['ko_output_file_path'] = $_SESSION['ko_map_path'];
    }
//gw20141113 - eg SET_SUPERSESSION_VAR|ko_output_file_path|c:\tdx_archive\%yyyy%\%mm%\%dd%\tdxefs\|END
    $s_filepath = $ps_path;


//    ECHO "<BR>1s_filename".$s_filepath;



    if ( trim($s_filepath) == "")
    {
        $s_filepath = $_SESSION['ko_output_file_path'];
    }
//    ECHO "<BR>1ps_filename".$s_filepath;

    $s_filepath = str_replace("%YYYY%",date("Y"),$s_filepath);
    $s_filepath = str_replace("%yyyy%",date("Y"),$s_filepath);
//    ECHO "<BR>1ps_filename".$s_filepath;

    $s_filepath = str_replace("%MM%",date("m"),$s_filepath);
    $s_filepath = str_replace("%mm%",date("m"),$s_filepath);
//    ECHO "<BR>1ps_filename".$s_filepath;

    $s_filepath = str_replace("%DD%",date("d"),$s_filepath);
    $s_filepath = str_replace("%dd%",date("d"),$s_filepath);
//    ECHO "<BR>1ps_filename".$s_filepath;

    $sys_function_out  = $s_filepath;
C100_MAKE:
    if (strpos($s_filepath,"http") !== false)
    {
        GOTO C900_END;
    }
    //make the folder
    if (!file_exists($s_filepath)) {
        mkdir($s_filepath, 0777, true);
    }
C900_END:
Z900_EXIT:
    return $sys_function_out;

}
//endofclass
//##########################################################################################################################################################################
    }

    ?>
