<?php

class cl_dma_api2
{

    function cl_dmaapi2_a100_get_carrier_rate($ps_request_xml,$ps_dbcnx,$ps_debug,$ps_calledfrom,$ps_details_def,$ps_details_data,$ps_sessionno)
    {
        A100_INIT:

        require_once($_SESSION['web_prog_path'].'lib/class_cl_sql.php');

        global $class_main;
        $class_sql = new clSqlClient();

        B100_PROCESS:

        //echo $ps_request_xml;
        //echo '<br>';

        $xml_data = $ps_request_xml;
        $xml_data=str_replace("'","",$xml_data);
        $xml_data=str_replace("&","",$xml_data);
        $xml_data=str_replace(", "," ",$xml_data);

        if($xml_data===false)
        {
            $tmp = $class_main->clmain_u540_utl_activity_log($ps_dbcnx,"dmaapi_carrier_csemail","SYSMON","DMAAPI","carrier_csemail","Error - unable to process the xml_data - xml_data is []".$xml_data."[]",$s_taskrequestid,"TASKREQUESTID","k2 value","k2 def","k3 value","k3 def","k4 value","k4 def","k5 value","k5 def");
            $sys_function_out = "Error cl_dmaapi2_100_x1472 - xml-data is false";
            $s_response_status = "ERROR";
            $s_response_message = "Ref:dmaapi100_1000 - Unable to process the xml data.";
            GOTO Z900_EXIT;
        }

        C100_PROCESS_XML:

        $xmlObj = simplexml_load_string($xml_data);
        $arrXml = $class_main->clmain_u599_xmlIntoArray($xmlObj);
        $xml_arr=$class_main->clmain_u598_xmlarray_keys_toupper($arrXml);

        $s_tasktype=trim($xml_arr['TASKDETAILS']['TASKTYPE']);
        $s_mailto=trim($xml_arr['TASKDETAILS']['TASKEMAILTO']);
        $s_requestid=trim($xml_arr['TASKDETAILS']['TASKREQUEST_ID']);
        $s_requesttime=trim($xml_arr['TASKDETAILS']['TASKREQUEST_TIMELOCAL']);
        $s_requesttimegmt=trim($xml_arr['TASKDETAILS']['TASKREQUEST_TIMEGMT']);
        $s_token=trim($xml_arr['TASKDETAILS']['TASKTOKEN']);
        $s_client_name=trim($xml_arr['TASKDETAILS']['TASKCLIENT_NAME']);
        $s_site_id=trim($xml_arr['TASKDETAILS']['TASKSITE_ID']);
        $s_site_software=trim($xml_arr['TASKDETAILS']['TASKSITE_SOFTWARE']);
        $s_site_version=trim($xml_arr['TASKDETAILS']['TASKSITE_VERSION']);

        if (trim($xml_arr['CN_CARRIERS']['CN_NUMBER_DETAILS']['CN_NUMBER'])<>'')
        {
            goto C200_PROCESS_SINGLE_CARRIER;
        }
        if (trim($xml_arr['CN_CARRIERS']['CN_NUMBER_DETAILS'][0]['CN_NUMBER'])<>'')
        {
            goto C300_PROCESS_MULTI_CARRIER;
        }

        goto Z900_EXIT;

        C200_PROCESS_SINGLE_CARRIER:

        $s_cn_number=trim($xml_arr['CN_CARRIERS']['CN_NUMBER_DETAILS']['CN_NUMBER']);
        $s_company_id=trim($xml_arr['CN_CARRIERS']['CN_NUMBER_DETAILS']['COMPANY_ID']);
        $s_tco=trim($xml_arr['CN_CARRIERS']['CN_NUMBER_DETAILS']['TCO']);
        $s_cn_uk=trim($xml_arr['CN_CARRIERS']['CN_NUMBER_DETAILS']['CN_UK']);
        $s_process_id=trim($xml_arr['CN_CARRIERS']['CN_NUMBER_DETAILS']['PROCESS_ID']);
        $s_type=trim($xml_arr['CN_CARRIERS']['CN_NUMBER_DETAILS']['TYPE']);
        $s_selection_type=trim($xml_arr['CN_CARRIERS']['CN_NUMBER_DETAILS']['SELECTION_TYPE']);

        $s_url=$_SESSION['dma_prog_url']."ratebot.php?site_path=".$_SESSION['site_path']."&cn_number=".$s_cn_number."&company_id=".$s_company_id."&tco=".$s_tco."&cn_uk=".$s_cn_uk."&process_id=".$s_process_id."&type=".$s_type;

        if ($s_tasktype=='CHEAPEST')
        {
            goto D100_GET_CHEAPEST_CARRIER;
        }

        goto Z900_EXIT;

        C300_PROCESS_MULTI_CARRIER:

        $i=0;
        while (trim($xml_arr['CN_CARRIERS']['CN_NUMBER_DETAILS'][$i]['CN_NUMBER'])<>'')
        {
            $s_cn_number=trim($xml_arr['CN_CARRIERS']['CN_NUMBER_DETAILS'][$i]['CN_NUMBER']);
            $s_company_id=trim($xml_arr['CN_CARRIERS']['CN_NUMBER_DETAILS'][$i]['COMPANY_ID']);
            $s_tco=trim($xml_arr['CN_CARRIERS']['CN_NUMBER_DETAILS'][$i]['TCO']);
            $s_cn_uk=trim($xml_arr['CN_CARRIERS']['CN_NUMBER_DETAILS'][$i]['CN_UK']);
            $s_process_id=trim($xml_arr['CN_CARRIERS']['CN_NUMBER_DETAILS'][$i]['PROCESS_ID']);
            $s_type=trim($xml_arr['CN_CARRIERS']['CN_NUMBER_DETAILS'][$i]['TYPE']);
            $s_selection_type=trim($xml_arr['CN_CARRIERS']['CN_NUMBER_DETAILS'][$i]['SELECTION_TYPE']);

            $s_url=$_SESSION['dma_prog_url']."ratebot.php?site_path=".$_SESSION['site_path']."&cn_number=".$s_cn_number."&company_id=".$s_company_id."&tco=".$s_tco."&cn_uk=".$s_cn_uk."&process_id=".$s_process_id."&type=".$s_type;

            //echo 's_url='.$s_url.'<br>';
            //exit();

            $ch = curl_init($s_url);
            curl_setopt($ch, CURLOPT_TIMEOUT, 50);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_exec($ch);
            curl_close($ch);

            $i++;
        }

        if ($s_tasktype=='CHEAPEST')
        {
            goto D100_GET_CHEAPEST_CARRIER;
        }

        goto Z900_EXIT;

        D100_GET_CHEAPEST_CARRIER:

        $s_cn_schedcharge = '';
        $s_transport_company = '';
        $s_cheapest_carrier = '';

        D150_CHECK_ZERO_CARRIER_COST:

        $s_zero_cost_carriers='0';
        $s_zero_carrier_cost_sql="select count(*) as zero_cost_carriers from dmcntrk where key1 = '{$s_cn_number}' and trackgroup = 'SCHEDCHARGE' and key3 = '0' ";
        //echo 's_zero_carrier_cost_sql='.$s_zero_carrier_cost_sql.'<br>';
        //exit();
        $s_zero_carrier_cost_result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_zero_carrier_cost_sql);
        while($s_zero_carrier_cost_row = mysql_fetch_array($s_zero_carrier_cost_result, MYSQL_ASSOC))
        {
            $s_zero_cost_carriers = $s_zero_carrier_cost_row['zero_cost_carriers'];
        }

        if ($s_zero_cost_carriers==0)
        {
            goto D160_DO_CHEAPEST_CARRIER;
        }

        $s_first_rec='Y';
        $s_cheapest_carrier_sql="select * from dmcntrk where key1 = '{$s_cn_number}' and trackgroup = 'SCHEDCHARGE' order by Key3";
        //echo 's_cheapest_carrier_sql='.$s_cheapest_carrier_sql.'<br>';
        //exit();
        $s_cheapest_carrier_result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_cheapest_carrier_sql);
        while($s_cheapest_carrier_row = mysql_fetch_array($s_cheapest_carrier_result, MYSQL_ASSOC))
        {
            $s_cn_schedcharge = $s_cheapest_carrier_row['Key3'];
            $s_transport_company = $s_cheapest_carrier_row['transport_company'];

            if ($s_first_rec=='Y')
            {
                $s_first_rec='N';
                $s_cheapest_carrier='ZERO_CARRIERS|'.$s_transport_company.'='.$s_cn_schedcharge;
            }
            else
            {
                $s_cheapest_carrier=$s_cheapest_carrier.'|'.$s_transport_company.'='.$s_cn_schedcharge;
            }
        }

        echo $s_cheapest_carrier;

        goto Z900_EXIT;

        D160_DO_CHEAPEST_CARRIER:

        $s_cheapest_carrier_sql="select * from dmcntrk where key1 = '{$s_cn_number}' and trackgroup = 'SCHEDCHARGE' order by Key3 ASC LIMIT 1 ";
        //echo 's_cheapest_carrier_sql='.$s_cheapest_carrier_sql.'<br>';
        //exit();
        $s_cheapest_carrier_result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_cheapest_carrier_sql);
        while($s_cheapest_carrier_row = mysql_fetch_array($s_cheapest_carrier_result, MYSQL_ASSOC))
        {
            $s_cn_schedcharge = $s_cheapest_carrier_row['Key3'];
            $s_transport_company = $s_cheapest_carrier_row['transport_company'];
        }

        $s_cheapest_carrier=$s_transport_company.'|'.$s_cn_schedcharge;

        echo $s_cheapest_carrier;

        goto Z900_EXIT;

        Z900_EXIT:
    }

}
?>
