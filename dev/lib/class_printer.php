<?php

	//************************************************************************************************
	//
	//	Class: Printer
	//
	//
	//************************************************************************************************


	class cls_printer
	{
        function pf_print_pdf_file($ps_printer_name,$ps_printer_filename,$ps_printer_orientation=false,$ps_printer_copies=false)
        {
            $s_print_cmd='';
            $s_output='';
            $s_return='';
            
            if (trim($ps_printer_orientation)=='')
            {
                // pkn 20110201 - 1 = portrait, 2 = landscapes
                $ps_printer_orientation='1';
            }
            if (trim($ps_printer_copies)=='')
            {
                $ps_printer_copies='1';
            }            
            
            //$s_print_cmd = "C:\\PKNPC\\pdfprint_cmd\\pdfprint -printer \\\pknlaptop\canon_MP610_series_printer C:\\PKNPC\\WebDev\\dma-cs-dev\\test.pdf";
            //$s_print_cmd = "C:\\tdx\\pdfprint_cmd\\pdfprint -printer ".$ps_printer_name." ".$ps_printer_filename;
            //$s_print_cmd = 'C:\\tdx\\pdfprint_cmd\\pdfprint -printer '.$ps_printer_name.' -paper "210x295mm" -orient '.$ps_printer_orientation.' -copies '.$ps_printer_copies.' '.$ps_printer_filename;
		$s_print_cmd = 'C:\\tdx\\pdfprint_cmd\\pdfprint -printer '.$ps_printer_name.' -paper "pdf" -orient '.$ps_printer_orientation.' -copies '.$ps_printer_copies.' '.$ps_printer_filename;



            exec($s_print_cmd,$s_output,$s_return);
            //echo "<pre>";
            //print_r($s_output);
            //print_r($s_ret);
            //echo "</pre>";            
            
            $s_printer_message=trim($s_output[0]);
            $s_printer_message=strtoupper($s_output[0]);
            if (substr($s_printer_message,0,9)=='THANK YOU')
            {
                return 'Printer Message: File=: '.$ps_printer_filename.' successfully sent to tdxtest printer-: '.$ps_printer_name;    
            }
            else
            {
                return 'Printer Message: Error, unable to find printer-: '.$ps_printer_name.'.  Please check before printing again.';    
            }
            
            
        }

        function pf_print_text_file($ps_printer_name,$ps_printer_filename)
        {
            //$printer = "\\\\pknlaptop\\canon_MP610_series_printer";
            //$printer = "\\\\server04\\SHARP MX-2300N B&W";
            //$printer = "\\\\tdxapache2-dev\\cutepdfwriter";
            //$printer = "\\\\pknlaptop\\cutepdf_writer";

            if($ph = printer_open($ps_printer_name))
            {
                // Get file contents
                $fh = fopen($ps_printer_filename, "rb");
                $s_file_content = fread($fh, filesize($ps_printer_filename));
                fclose($fh);
               
                printer_set_option($ph, PRINTER_MODE, "TEXT");
                printer_write($ph, $s_file_content);
                printer_close($ph);            
                return 'Printer Message: File=: '.$ps_printer_filename.' successfully sent to printer-: '.$ps_printer_name;
            }
            else
            {
                return 'Printer Message: Error, unable to find printer-: '.$ps_printer_name.'.  Please check before printing again.';
            }
            
        }        

        function pf_show_printers()
        {            
            ?>
                <table width="100%" border="0" cellpadding="5" cellspacing="0">
                    <tr bgcolor="#EEEEEE">
                        <td><table width="100%"  border="0" cellspacing="0" cellpadding="2" id="body-text">
                        <tr bgcolor="#EEEEEE">
                            <td colspan="3"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                        <tr bgcolor="#EEEEEE">
                            <td><strong>Select Printer to print to</strong></td>
                            <td align="right" valign="center"><strong><a href="javascript:;" onclick="getHelp('<?=$help_id?>');"><img src="images/q.gif" height="12" width="12" border="0"></a></strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                </table><hr width="100%" size="1"><br><?=$page_desc?><br><br></td>
                    </tr>        
            <?

            $s_print_cmd = "C:\\tdx\\pdfprint_cmd\\pdfprint -listprinter ";
            exec($s_print_cmd,$s_printer_list,$s_return);    
            //echo "<pre>";
            //print_r($s_printer_list);
            //echo "</pre>";                
                        
            foreach ($s_printer_list as $s_show_printer_list)
            {
                $s_printer = $s_show_printer_list;
                $s_printer_queue = str_replace(' ','^',$s_printer);
                ?>     
                    <tr bgcolor="#EEEEEE">                    
                        <td width="25%"><?=$s_printer?></td>
                        <td width="25%" align="center"><input type=checkbox name=chkprinter[<?=$s_printer_queue?>] size=100 maxlength=100 value="Y"></td>
                        <td width="50%">&nbsp;</td>
                    </tr>              
                <? 
            }
    
            ?>
                <tr bgcolor="#EEEEEE">
                    <td colspan="3">
                    <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                        <tr bgcolor="#EEEEEE">
                            <td width="100%">
                                <input type="submit" name="btnSelectPrinter" value="Print to Selected Printer">
                                </td>
                            <td width="25%">&nbsp;</td>
                            <td width="50%">&nbsp;</td>
                        </tr>
                    </table>
                    </td>
                </tr>
                </table>
                </td>
                </tr>
                </table>        
            <?
        }
        
        function pf_show_printers_php()
        {            
            ?>
                <table width="100%" border="0" cellpadding="5" cellspacing="0">
                    <tr bgcolor="#EEEEEE">
                        <td><table width="100%"  border="0" cellspacing="0" cellpadding="2" id="body-text">
                        <tr bgcolor="#EEEEEE">
                            <td colspan="3"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                        <tr bgcolor="#EEEEEE">
                            <td><strong>Select Printer to print to</strong></td>
                            <td align="right" valign="center"><strong><a href="javascript:;" onclick="getHelp('<?=$help_id?>');"><img src="images/q.gif" height="12" width="12" border="0"></a></strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                </table><hr width="100%" size="1"><br><?=$page_desc?><br><br></td>
                    </tr>        
            <?

            /*
            $local_printer_list = printer_list(PRINTER_ENUM_LOCAL);                
            foreach ($local_printer_list as $local_printer)
            {
                $printer = $local_printer["NAME"];        
                $printer = str_replace(' ','^',$printer);
                ?>     
                    <tr bgcolor="#EEEEEE">                    
                        <td width="25%"><?=$printer?></td>
                        <td width="25%" align="center"><input type=checkbox name=chkprinter[<?=$printer?>] size=100 maxlength=100 value="Y"></td>
                        <td width="50%">&nbsp;</td>
                    </tr>              
                <? 
            }
            */

            //pkn 20101207 - works fine on my laptop
            //$domain_printer_list = printer_list(PRINTER_ENUM_NETWORK);    
            //pkn 20101207 - works fine on a domain
            $domain_printer_list = printer_list(PRINTER_ENUM_LOCAL| PRINTER_ENUM_SHARED);
            foreach ($domain_printer_list as $domain_printer)
            {
                $printer = $domain_printer["NAME"];
                $printer_queue = str_replace(' ','^',$printer);
                ?>     
                    <tr bgcolor="#EEEEEE">                    
                        <td width="25%"><?=$printer?></td>
                        <td width="25%" align="center"><input type=checkbox name=chkprinter[<?=$printer_queue?>] size=100 maxlength=100 value="Y"></td>
                        <td width="50%">&nbsp;</td>
                    </tr>              
                <? 
            }
    
            ?>
                <tr bgcolor="#EEEEEE">
                    <td colspan="3">
                    <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                        <tr bgcolor="#EEEEEE">
                            <td width="100%">
                                <input type="submit" name="btnSelectPrinter" value="Print to Selected Printer">
                                </td>
                            <td width="25%">&nbsp;</td>
                            <td width="50%">&nbsp;</td>
                        </tr>
                    </table>
                    </td>
                </tr>
                </table>
                </td>
                </tr>
                </table>        
            <?
        }
	}
?>
