<?php

//DIE("GWDIE UT_JCL 000");


function ut_jcl_pf_customError($errno,$errstr,$errfile,$errline,$errcontext)
  {
// NOTE **** THIS ONLY CATCHES NON-FATAL ERRORS
//  echo "<b>Error gw12:</b> [$errno] $errstr $errfile $errline $errcontext.<br>";
// gw 20100315       switch ($error_level) {
/*
       switch ($errno) {
        case E_USER_ERROR:
            echo "<b>My ERROR</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            echo "  Fatal error on line $errline in file $errfile";
            echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
            echo "Aborting...<br />\n";
            exit(1);
            break;

        case E_USER_WARNING:
            echo "<b>My WARNING</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        case E_USER_NOTICE:
            echo "<b>My NOTICE</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        default:
            echo "<BR>Unknown error type: [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;
        }
*/
    /* Don't execute PHP internal error handler */
//    return true;
    echo "<br>ut_jcl.php v1 -    custom error<br> errno =[]".$errno,"[]<br>  errstr=[]".$errstr."[]<br>  errfile=[]".$errfile."[]<br>  errline=[]".$errline.'[]<br>  errcontext=[]'.$errcontext."[]<br> ";
    echo "SESSION['ko_prog_path']=[]".$_SESSION['ko_prog_path']."[]";
    echo "DUMP all session vars:<br>";
    echo '<pre>' . print_r($_SESSION, TRUE) . '</pre>';
    echo "<br>end dump";
    echo "DUMP all SERVER vars:<br>";
    echo '<pre>' . print_r($_SESSION, TRUE) . '</pre>';
    echo "<br>end dump";
}


// DIE("GWDIE UT_JCL 2000");

    $ko_map_path="";
    IF(SESSION_ID() == ""){
        session_start();
    }

A000_SET_RUN:
// DIE("GWDIE UT_JCL 2500");
     //set error handler
// pkn 20190730 - as this is the public version of jcl processor skip error checking	 
    //if(isset ($_SESSION['dma_to_wp_n_dmav2'])===false){
    //    set_error_handler("ut_jcl_pf_customError", E_ALL);
    //}
date_default_timezone_set('Australia/Brisbane');

A0000_DO_DMA_TO_DMAV2_LINK:

pf_set_sessionid('tlm_adm1');

$GLOBALS["program_name"]=basename(__FILE__, '.php');
$GLOBALS["skip_session_start"]='Y';
$_GET['site_path']="/tdx/html/dma-triangle/v20180501/";

require_once($_SESSION['dma_prog_path'].'/tdx/html/dma_prog/v20180501/lib/comm_session_init_public.php');
require_once($_SESSION['dma_prog_path'].'/tdx/html/dma_prog/v20180501/lib/class_main.php');

$objSql = new SqlClient();

$_SESSION['ko_prog_path']='/tdx/web_prog/dev/';

echo 'session variables='.'<br>';
print_r($_SESSION);
exit();

/*

echo "<br>gw utjcl a0000";
echo "<br> list session values ";
foreach ($_SESSION as $key => $value){
    echo "session:{$key} = {$value}\r\n<br>";
}
echo "<br> end session values ";



echo "<br> before test the kickoff done is set to []".$_SESSION['ut_kickoff_has_been_done']."[] <br>";
*/

//'20161105gw added so ut_kickoff not runn more than once per session'
    if(!isset($_SESSION['ut_kickoff_has_been_done']))
    {

//        echo "<br> the value is not set and ut_kickoff is <br>";
        $_from_ut_jcl = "YES";

        require_once('ut_kickoff.php');
        $_SESSION['ut_kickoff_has_been_done'] = "doneat_".microtime(true);
    }else{
//        echo "<br> the kickoff done is set to []".$_SESSION['ut_kickoff_has_been_done']."[] <br>";

    }

A9000_END:

// DIE("GWDIE UT_JCL 3001");

    IF(SESSION_ID() == ""){
        session_start();
    }
    if (isset($_SESSION['ko_prog_path']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
    }else{
    $_SESSION['ko_prog_path'] = "";
    }
    if (isset($_SESSION['ko_dbase_to_connectto']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
    }else{
    $_SESSION['ko_dbase_to_connectto'] = "tdxpryda";
    }

    if (isset($_SESSION['ko_map_path']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
        $ko_map_path = $_SESSION['ko_map_path'];
    }else{
    $_SESSION['ko_map_path'] = "";
    }

A000_DEFINE_VARIABLES:
    $sys_prog_name = "WEBPROG UT_JCL.PHP ";
    $sys_debug = '';
    $sys_debug_text = $sys_prog_name;
    $s_file_exists = '';
    $s_filename="";
    $map = "";
    $tmp_array = array();
    $sys_function_out = "";
    $s_temp_value = "";

    $s_sessionno = "";
    $s_helplocation = "";
    $function_id = "";
    $get_userid = "";
    $s_details_def="";
    $s_details_data="";
    $s_siteparams = "";
    $s_url_siteparams = "";
    $s_usr_style_list = "";
    $s_usr_style_values= "";
    $username = "";
    $s_inJCL ="";
    $s_temp = "";
    $results = "";
    $s_MultiLevelloop1 = "";
    $s_MultiLevelloop2 = "";
    $s_MultiLevelloop3 = "";
    $s_no_jcl_in_file = "";

    $s_output_to_file = "";
    $s_output_to_filename = "";
    $s_output_to_atend = "";
    $s_output_to_atend_do="";

A200_LOAD_LIBRARIES:
    $sys_debug = strtoupper("NO");
//   $sys_debug = strtoupper("yes");

     IF ($sys_debug == "YES"){echo $sys_prog_name." started debug=".$sys_debug." *** remember to view source - it will save you hours  <br>";};
     IF ($sys_debug == "YES"){echo $sys_prog_name." debug - ".$sys_debug." before require of class_sql.php from ko_prog_path on next line - if no next line then no ko_prog_path<br>";};
     IF ($sys_debug == "YES"){echo $sys_prog_name." debug - ".$sys_debug." ko_prog_path = []".$_SESSION['ko_prog_path']."[] check if this is the right path to the web prog folder!<br>";};
    IF(!file_exists($_SESSION['ko_prog_path'].'lib/class_sql.php')){
        die("ut_jcl Err:001 process stopped.  <br> This file does not exist file=[]".$_SESSION['ko_prog_path'].'lib/class_sql.php'."[]");
    }
    IF ($sys_debug == "YES"){echo $sys_prog_name." before class_sql file=[]".$_SESSION['ko_prog_path'].'lib/class_sql.php'."[]";};

    $s_temp1 = $_SESSION['ko_prog_path'].'lib/class_sql.php';
    require_once($s_temp1);
    $s_temp = 'wp_SqlClient';
    if (!class_exists($s_temp)) {
        die("<br><br>  ut_jcl Err:002 process stopped.  <br> This class does not exist class=[]".$s_temp."[]<br>Recently loaded library = []".$s_temp1."[] <br>Check the class exists in the library and that the correct library has been used<br> ");
    }
    $class_sql = new wp_SqlClient();

     IF ($sys_debug == "YES"){echo $sys_prog_name." after class_sql<br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_main.php');
     $class_main = new clmain();

     IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_main <br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_process_jobline.php');
     $class_clprocessjobline = new clprocessjobline();
     IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_process <br>";};
//     require_once($_SESSION['ko_prog_path'].'lib/class_myplib1.php5');
//     $class_myplib1 = new myplib1();
//     IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_myplib1<br>";};

//print_r ($_SESSION);
//gw20160519 - this is now done by the dmav2 process so no generic class_app
//     require_once($_SESSION['ko_map_path'].'lib/class_app.php');
//     $class_apps = new AppClass();

     IF ($sys_debug == "YES"){echo "_get=";print_r($_GET);echo "<br>";};
     IF ($sys_debug == "YES"){echo "_post=";print_r($_POST);echo "<br>";};


//     echo     "<br> gw full line params ie all _get=";print_r($_GET);echo "<br>";

A300_CONNECT_TODBASE:
     $dbcnx = $class_sql->c_sqlclient_connect();


A400_GET_PARAMETERS:
    if(!isset($s_wp_ut_jcl_runtype)){
        goto A410_DO_URL;
    }
    if($s_wp_ut_jcl_runtype <> "INPROGV1")
    {
        goto A410_DO_URL;
    }
    $map = $s_wp_ut_jcl_filename;
    $s_url_siteparams = "902^utjcl_sitep2";
    $s_url_siteparams = $s_url_siteparams."^".$s_wp_ut_jcl_runparams;
    if(isset ($_SESSION['dma_to_wp_jcl_mappath'])!==false){
        $ko_map_path = $_SESSION['dma_to_wp_jcl_mappath'];
    }
    IF(file_exists($ko_map_path.$_SESSION['ko_dbase_to_connectto']."/".$s_wp_ut_jcl_filename))
    {
        $map = $ko_map_path.$_SESSION['ko_dbase_to_connectto']."/".$s_wp_ut_jcl_filename;
        GOTO A450_NEXT;
    }

    IF(file_exists($ko_map_path.$s_wp_ut_jcl_filename))
    {
        $map = $ko_map_path.$s_wp_ut_jcl_filename;
        GOTO A450_NEXT;
    }

    GOTO A450_NEXT;

A410_DO_URL:
     if (isset($_GET['map'])) $map = $_GET["map"];
     if (isset($_GET['p'])) $s_url_siteparams = $_GET["p"];

//gw201606701
    if(trim($s_url_siteparams) == "")
    {
        $s_url_siteparams = "903^utjcl_dmamby";
    }

A450_NEXT:
     $s_url_siteparams = $class_main->clmain_u630_tidy_siteparams($s_url_siteparams, "NO");
//gw20110206 - first 2 params become system parms - the rest are in site params
     $ar_url_params = explode("^",$s_url_siteparams);
     $s_url_systemparams = $ar_url_params[0]."^".$ar_url_params[1]."";

A500_SET_VALUES:
    $ar_line_details = array();
    $s_MultiLevelloop1 = "NONE";
    $s_MultiLevelloop2 = "NONE";
    $s_MultiLevelloop3 = "NONE";
    $s_output_to_file = "NO";
    $s_output_to_filename = "";

//echo "<br>utjcl a500 map =".$map;

    $map = $class_main->clmain_set_map_path_n_name($ko_map_path,$map);
    $map = str_replace("\\\\","\\", $map);
    $map = str_replace("\\\\","\\", $map);
    $map = str_replace("\\\\","\\", $map);
    $map = str_replace("\\\\","\\", $map);


// echo "<br>utjcl a5002 map =".$map;

    $sys_prog_name = "ut_jcl.php";
    $sys_function_name = $sys_prog_name;
    IF ($sys_debug == "YES"){echo $sys_prog_name." ";};

    $s_file_exists='Y';
    if(file_exists($map))
    {
        $array_lines = file($map);
    }
    else
    {
        $s_file_exists='N';
        $sys_function_out =  "<br>".$sys_prog_name."E:utjcl_200 ##### File Not Found-:".$map."<br>";
        z901_dump("ERROR", $sys_prog_name."E:utjcl_200  ##### File Not Found-:[]".$map."[]");
        GOTO Z900_EXIT;
    }
    $s_no_jcl_in_file = "The file ".$map." has no jcl code";


    IF ($sys_debug == "YES"){echo $sys_prog_name." after file exists check s_map_file_exists=".$s_file_exists."<br>";};
    IF ($sys_debug == "YES"){echo $sys_prog_name." number of lines in the file(array)=".count($array_lines)."<br>";};

if (strpos($s_url_siteparams,"%!") > 0 )
    {
           $s_sessionno = "utmenu1";
            $_SESSION[$s_sessionno.'username'] = $username;
    }else{
            $s_sessionno = $class_main->clmain_get_param($s_url_siteparams,"s_sessionno","no");
    }
//    echo "after file open<br>";
//gw20100127       $s_sessionno = $class_main->clmain_get_param($s_url_siteparams,"s_sessionno","no");
//gw20110313 - added user timezone
    if (isset($_SESSION[$s_sessionno.'ut_logon_timezone']))
    {
        $timezone=$_SESSION[$s_sessionno.'ut_logon_timezone'];
        date_default_timezone_set($timezone);
//        echo "<br> ************88  set the timezone to ".$timezone."<br>";
    }else{
//        echo "<br> ************88 not setup  set the timezone to ".$timezone."<br>";
    }

    if (isset($_SESSION[$s_sessionno.'username']))
    {
        $username = $_SESSION[$s_sessionno.'username'];
    }else{
        $username = "Unknown ";
    }
    $s_details_def='START|USERNAME';
    $s_details_data='START|'.$username;
    $s_details_def.="|SITEPARAMS";
    $s_details_data.='|'.$s_url_siteparams;
    $s_details_def.="|SYSTEMPARAMS";
    $s_details_data.='|'.$s_url_systemparams;

    if (isset($_SESSION[$s_sessionno.'session_def']))
    {
        $s_details_def.=$_SESSION[$s_sessionno.'session_def'];
        $s_details_data.=$_SESSION[$s_sessionno.'session_data'];
    }
    $s_details_def.='|END';
    $s_details_data.='|END';
    $s_inJCL ="N";

    $i = 0;
    $s_line_in = "";
B100_GET_REC:
    IF ($i >= count($array_lines))
    {
        goto Z700_END;
    }

    $s_line_in = $array_lines[$i];

    IF (substr($s_line_in,0,1) == "*")
    {
        goto Z800_GET_NEXT;
    }
    IF (substr($s_line_in,0,2) == "//")
    {
        goto Z800_GET_NEXT;
    }
    IF (trim($s_line_in) == "")
    {
        goto Z800_GET_NEXT;
    }
//'    IF (trim($s_line_in,0,2) == "//")
//'    {
//        goto Z800_GET_NEXT;
//    }

    IF ($sys_debug == "YES"){echo $sys_function_name."  processing line ".$i."<br>".$s_line_in."<br>";};
// used to ignore the line
    IF (strpos(strtoupper($s_line_in),strtoupper("SKIP_LINE")) === false )
    {}else{
        IF ($sys_debug == "YES"){echo $sys_function_name." "."got the map skip line command <br>".$s_line_in."<br>";};
        goto Z800_GET_NEXT;
    }
// used to indicate if there is a start JCL loop
    IF (strpos(strtoupper($s_line_in),strtoupper("START_JCL_HERE"))  === false )
    {}else{
        IF ($sys_debug == "YES"){echo $sys_function_name."got the map starter start line for jcl<br>".$s_line_in."<br>";};
        $s_inJCL ="Y";
        goto Z800_GET_NEXT;
    }
// used to indicate if there is a end  JCL loop
    IF (strpos(strtoupper($s_line_in),strtoupper("END_JCL_HERE"))  === false  )
    {}else{
        IF ($sys_debug == "YES"){echo $sys_function_name."got the map end line for jcl<br>".$s_line_in."<br>";};
        $s_inJCL ="N";
        goto Z700_END;
    }
// if before the required loop get the next line
    if ($s_inJCL =="N")
    {
        IF ($sys_debug == "YES"){echo $sys_function_name." ".$i." in jcl  loop<br>";};
        goto Z800_GET_NEXT;
    }

    $s_no_jcl_in_file = "";
    $ar_line_details = explode("|",$s_line_in);

    IF ($sys_debug == "YES"){echo $sys_function_name." line <br>".$s_line_in."<br>";};

    IF (strtoupper($ar_line_details[0]) == "DUMPTABLELISTTOSQLLOG")
    {
        $temp = $class_sql->c_sqlclient_list_tables_to_log($dbcnx);
        goto Z800_GET_NEXT;
    }
A00_START:
//gw20160212 - done inc class_process_jobline
/*
goto A900_END;

    IF (strtoupper($ar_line_details[0]) == "DRAW")
    {
        $map = $ar_line_details[1];
//        echo "<br> utjcl a00_start map=".$map;
        IF (!strpos(strtoupper($map),strtoupper("%!"))  === false  )
        {
            $map = STR_REPLACE("#P","|",$map);
            $map = STR_REPLACE("#p","|",$map);
            $map = $class_main->clmain_v200_load_line( $map,$s_details_def,$s_details_data,"NO",$s_sessionno," ut_jcl a00_start draw  ");
        }
        $map = $class_main->clmain_set_map_path_n_name($ko_map_path,$map);
//        echo "<br> utjcl 3 a00_start map=".$map;
        $sys_function_out .= pf_b100_DO_DRAW_HTML($map,$ar_line_details[2],"NO",$s_details_def,$s_details_data);
        goto Z800_GET_NEXT;
    }


//* DrawIF|v1|field|operator|value|map_name|loop|END
//DrawIF|v1|url_MODE|=|WELCOME|my_buddy_main.htm|welcome_map|END
    IF (strtoupper($ar_line_details[0]) == "DRAWIF")
    {
        $sys_function_out .= pf_b110_DO_DRAWIF_HTML($s_line_in,"NO",$s_details_def,$s_details_data,$s_sessionno);
        goto Z800_GET_NEXT;
    }
*/
A900_END:
B000_START:

// NOT gww20110526 - moved to class_process_jobline
//    GOTO B900_END;

//dataloop|map|loop|Select * from devices |END
//* dataloop|owl_bdymain.htm|detail_loop|SELECT * FROM sales_order  as S_O where (so_order_status <> "90" and so_order_status <> "99" and so_order_status <> "91"and so_order_status <> "75") AND so_whse_code = "3" order by so_order_no desc LIMIT 0 , 50 |END
//* dataloop|v1|url_MODE|=|INITSCRN|owl_bdymain.htm|detail_loop|SELECT * FROM sales_order  as S_O where (so_order_status <> "90" and so_order_status <> "99" and so_order_status <> "91"and so_order_status <> "75") AND so_whse_code = "3" order by so_order_no desc LIMIT 0 , 50 |END

    IF (strtoupper($ar_line_details[0]) == "DATALOOP")
    {
        GOTO B100_NOIF;
    }
    IF (strtoupper($ar_line_details[0]) == "DATALOOPIF")
    {
        GOTO B200_IF;
    }
    GOTO B900_END;
B100_NOIF:
    $s_temp_value = $ar_line_details[3];
    $s_map  = $ar_line_details[1];
    IF (!strpos(strtoupper($s_map),strtoupper("%!"))  === false  )
    {
        $s_map = STR_REPLACE("#P","|",$map);
        $s_map = STR_REPLACE("#p","|",$s_map);
        $s_map = $class_main->clmain_v200_load_line( $s_map,$s_details_def,$s_details_data,"NO",$s_sessionno," ut_jcl a00_start draw  ");
    }
    $s_group  = $ar_line_details[2];
    GOTO B500_DO;
B200_IF:
    IF (strtoupper($ar_line_details[1]) <> "V1")
    {
        GOTO B210_do_V2;
    }
    $s_field_to_check = $ar_line_details[2];
    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];
    $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl B200 ");
    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"NO","utjcl_B200 ");
    if ($s_true == "FALSE")
    {
        z901_dump($sys_debug, $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);
// gw 20100327  dont do this it erases all prev drawing        $sys_function_out = "";
        GOTO Z800_GET_NEXT;
    }
    $s_map  = $ar_line_details[5];
    IF (!strpos(strtoupper($s_map),strtoupper("%!"))  === false  )
    {
        $s_map = STR_REPLACE("#P","|",$map);
        $s_map = STR_REPLACE("#p","|",$s_map);
        $s_map = $class_main->clmain_v200_load_line( $s_map,$s_details_def,$s_details_data,"NO",$s_sessionno," ut_jcl a00_start draw  ");
    }
    $s_group  = $ar_line_details[6];
    $s_temp_value = $ar_line_details[7];
    GOTO B500_DO;
B210_do_V2:
B500_DO:


    $s_page_current = "1";
    $s_page_limit = "35";
    if (isset($_SESSION['ko_display_page_limit']))
    {
        $s_page_limit = $_SESSION['ko_display_page_limit'];
    }

    IF (strpos($s_url_siteparams,"^spage") === false)
    {
        GOTO B520_DO;
    }
//        urlvalue|v1|url_mode|mode|end
    $s_temp = "urlvalue|v1|not needed|spage|end";
    $s_page_current = $class_main->clmain_u500_do_url_line($dbcnx,$s_temp,"NO",$s_details_def,$s_details_data,$s_sessionno,$s_url_siteparams);
    if (strpos($s_page_current,"|^%##%^|") ===false)
    {
    }else{
        $ar_line_details = explode("|^%##%^|",$s_page_current);
//junk        $s_page_current = $ar_line_details[0];
        $s_page_current = $ar_line_details[1];
        $s_temp = "";
        GOTO B520_DO;
    };

// set the current page and page limit
#p%!_postv_page_prev_!%#p
B520_DO:
    //        $sys_function_out .= "BEFORE the dataloop<bR>";
    $s_temp_value = STR_REPLACE("#P","|",$s_temp_value);
    $s_temp_value = STR_REPLACE("#p","|",$s_temp_value);
    $sys_function_out .= Pf_b200_do_dataloop($dbcnx,$s_temp_value,$class_main->clmain_set_map_path_n_name($ko_map_path,$s_map),$s_group,"no",$s_details_def,$s_details_data,$s_sessionno,$s_page_current,$s_page_limit);
//            $sys_function_out .= "AFTER the dataloop<bR>";
        goto Z800_GET_NEXT;
B900_END:

C000_START:
//*urlvalue|v1|result field name|url parameter name
//* add the result_field_name to the details_def with the url_parameter_name from the url

/*
    IF (strtoupper(substr($s_line_in,0,8)) <> "URLVALUE")
    {
        GOTO C900_END;
    }
    $s_temp = $class_main->clmain_u500_do_url_line($dbcnx,$s_line_in,"no",$s_details_def,$s_details_data,$s_sessionno,$s_url_siteparams);
    if (strpos($s_temp,"|^%##%^|") ===false)
    {
    }else{
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
        $s_temp = "";
    };
        goto Z800_GET_NEXT;
*/
        C900_END:
D100_DO_APP_CLASS:
/*  20110511 - moved to class_process_jobline
//*app_class|v1|result field name|application class function and paramaters
//* add the result_field_name to details_def after running the function in the app_class
    IF (strtoupper($ar_line_details[0]) == "APP_CLASS")
    {
        $s_temp = Pf_b600_do_app_class($dbcnx,$s_line_in,"no",$s_details_def,$s_details_data,$s_sessionno);
        if(strtoupper(trim($ar_line_details[4]," ")) == 'YES')
        {
           echo "<br>doing APP_CLASS - LINE ".$s_line_in."<br>".$s_temp."<br>";
        }
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
        $s_temp = "";
        goto Z800_GET_NEXT;
    }

//*app_class|v1|result field name|application class function and paramaters
//app_classIF|v1|url_MODE|=|ADDITEMNEXT|b170_result|clapp_B170_pickssccitems(#p%!_url_sscc_no_!%#p, NO)|NO|END
    IF (strtoupper($ar_line_details[0]) <> "APP_CLASSIF")
    {
        GOTO D900_END;
    }

    $s_field_to_check = $ar_line_details[2];
    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];
    $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl d100 ");
    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_D100 ");

    if ($s_true == "FALSE")
    {
        z901_dump($sys_debug, $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);};
        $sys_function_out = "";
        GOTO Z800_GET_NEXT;
    }

//    DIE ("<BR>GW DIR UT JCL D100 Dl  s_true".$s_true."  s_line_in=".$s_line_in);

    $s_line_in = "APP_CLASS|V1|".$ar_line_details[5]."|".$ar_line_details[6]."|".$ar_line_details[7];
    $s_temp = Pf_b600_do_app_class($dbcnx,$s_line_in,"no",$s_details_def,$s_details_data,$s_sessionno);
    if(strtoupper(trim($ar_line_details[7]," ")) == 'YES')
    {
       echo "<br>doing APP_CLASSif - LINE ".$s_line_in."<br>".$s_temp."<br>";
    }

    $ar_line_details = explode("|^%##%^|",$s_temp);
    $s_details_def = $s_details_def."|".$ar_line_details[0];
    $s_details_data = $s_details_data."|".$ar_line_details[1];
    $s_temp = "";
    goto Z800_GET_NEXT;
*/
D900_END:

 GOTO E000_START;
/* 20160212 gw - moved to class_process_jobline
    // sys_class|version|fieldname|class function and parameters|end
//* sys_class|v1|field_style|clmain_v800_set_field_style(field_style,my_buddy_main.htm,"")|END
    IF (strtoupper($ar_line_details[0]) == "SYS_CLASS")
    {
//        echo "<br>GWDEBUG  doing SYS_CLASS - LINE ".$s_line_in."<br>".$s_temp."<br>";

        $s_temp = $class_main->clmain_a110_do_sys_class($dbcnx,$s_line_in,"no",$s_details_def,$s_details_data,$s_sessionno);
        if(strtoupper(trim($ar_line_details[4]," ")) == 'YES')
        {
           echo "<br>doing SYS_CLASS - LINE ".$s_line_in."<br>".$s_temp."<br>";
        }
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
        $s_temp = "";
        goto Z800_GET_NEXT;
    }
    IF (strtoupper($ar_line_details[0]) == "SYS_CLASSIF")
    {
        $s_temp = Pf_b655_do_sys_classif($dbcnx,$s_line_in,"no",$s_details_def,$s_details_data,$s_sessionno);
        if(strtoupper(trim($ar_line_details[4]," ")) == 'YES')
        {
           echo "<br>doing SYS_CLASS - LINE ".$s_line_in."<br>".$s_temp."<br>";
        }
        if ($s_temp == "") // the if test failed
        {
            goto Z800_GET_NEXT;
        }
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
        $s_temp = "";
        goto Z800_GET_NEXT;
    }
*/
E000_START:
//gw20110511 - moved to class_process_jobline
/*
//    IF (strtoupper(substr($s_line_in,0,11)) <> "ADD_DETAILS")
    IF (strtoupper(substr($s_line_in,0,11)) == "ADD_DETAILS")
    {
        GOTO E900_END;
    }
    IF (strtoupper(substr($s_line_in,0,11)) <> "ADD_DETAILS")
    {
        GOTO E900_END;
    }

//echo "<br> e00start ut_Jcl before the add_details s_line_in=".$s_line_in;


    $s_temp = $class_main->clmain_u120_do_add_details_line($dbcnx,$s_line_in,"NO",$s_details_def,$s_details_data,$s_sessionno,$s_url_siteparams);
    if (strpos($s_temp,"|^%##%^|") ===false)
    {
    }else{
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
        $s_temp = "";
    };
        goto Z800_GET_NEXT;

        E900_END:
*/
F000_RUNDATA:
//rundata|SELECT * from sales_order where so_order_no = 544854|END
// adds the results of the sql to the end of run wide details def/data

//gw20160212 - moved to class_process_jobline.php
/*
    IF (strtoupper($ar_line_details[0]) == "RUNDATA")
    {
        GOTO F100_DO_RUNDATA;
    }
    IF (strtoupper($ar_line_details[0]) == "RUNDATAIF")
    {
        GOTO F200_DO_RUNDATAIF;
    }
    GOTO F900_END;
F100_DO_RUNDATA:
    $s_temp_value = $ar_line_details[1];
    GOTO F400_RUNDATA;
F200_DO_RUNDATAIF:
    IF (strtoupper($ar_line_details[1]) <> "V1")
    {
        GOTO F210_do_rundataif_v2;
    }
    $s_field_to_check = $ar_line_details[2];
    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];
    $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl F200 RUNDATA sys_debug=".$sys_debug);
    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_F200 ");
    if ($s_true == "FALSE")
    {
        z901_dump($sys_debug, $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);
        $sys_function_out = "";
        GOTO Z800_GET_NEXT;
    }

    $s_temp_value = $ar_line_details[5];
    GOTO F400_RUNDATA;
F210_do_rundataif_v2:
F400_RUNDATA:
    $s_temp_value = STR_REPLACE("#P","|",$s_temp_value);
    $s_temp_value = STR_REPLACE("#p","|",$s_temp_value);
    $s_temp = $class_main->clmain_u300_set_rundata($dbcnx,$s_temp_value,"no",$s_details_def,$s_details_data,$s_sessionno);
    $ar_line_details = explode("|^%##%^|",$s_temp);
    $s_details_def = $ar_line_details[0];
    $s_details_data = $ar_line_details[1];
    $s_temp = "";

//    DIE ( "<br><br>DIE RUNDATA<br>".$s_details_def."<br>".$s_details_data);


    goto Z800_GET_NEXT;
F900_END:
*/

// gw20110525 - moved to process_job_line
// gw 20110802 moved back to here because process_job_line was not passing back the file and link
G000_START:
//    outtofileonly|filename=jobfileno.txt|fileonly=yes|end
//echo "<br> doing the ouptput to file".strtoupper($ar_line_details[0])." <br>";
    IF (strtoupper($ar_line_details[0]) <> "OUTPUTTOTEXT")
    {
        GOTO G500_SAVEOUTPUT;
    }

//echo   "<br>utjcl  doing the ouptput to file <br>";


    $s_output_to_file = "YES";
    $s_output_to_filename = $ar_line_details[1];
    $s_output_to_filename =  substr($s_output_to_filename,strpos($s_output_to_filename,"=") + 1);
    $s_output_to_filename = STR_REPLACE("#P","|",$s_output_to_filename);
    $s_output_to_filename = STR_REPLACE("#p","|",$s_output_to_filename);
    $s_output_to_filename = $class_main->clmain_v200_load_line( $s_output_to_filename,$s_details_def,$s_details_data,"NO",$s_sessionno," ut_jcl g900a  ");

    $s_output_to_fileonly = $ar_line_details[2];
    $s_output_to_fileonly =  substr($s_output_to_fileonly,strpos($s_output_to_fileonly,"=") + 1);

    $s_output_to_atend = $ar_line_details[3];
    $s_output_to_atend =  substr($s_output_to_atend,strpos($s_output_to_atend,"=") + 1);
    $s_output_to_atend = STR_REPLACE("#P","|",$s_output_to_atend);
    $s_output_to_atend = STR_REPLACE("#p","|",$s_output_to_atend);
    $s_output_to_atend = $class_main->clmain_v200_load_line( $s_output_to_atend,$s_details_def,$s_details_data,"NO",$s_sessionno," ut_jcl g900b  ");
    $s_output_to_atend_do =  $s_output_to_atend;
    $s_output_to_atend =  substr($s_output_to_atend,0,strpos($s_output_to_atend,":"));
    $s_output_to_atend_do =  substr($s_output_to_atend_do,strpos($s_output_to_atend_do,":") + 1);
G500_SAVEOUTPUT:
    IF (strtoupper($ar_line_details[0]) <> "SAVEOUTPUTTOTEXT")
    {
        GOTO G509_END;
    }
//20170106GW - COPY OF THE END OF jcl file process below
    // PUT THIS IS A FILE
    if (trim($s_output_to_file) <> "YES")
    {
        GOTO G509_END;
    }
    IF (trim($s_output_to_filename) == "")
    {
        $s_output_to_filename = "ut_jcl_output_to_file.txt";
    }

    $s_folder_name = str_replace("/","\\",$s_output_to_filename);
    $s_folder_name = substr($s_folder_name,0,strrpos($s_folder_name,'\\'));

    //gw20110130    $s_folder_name = substr($s_output_to_filename,0,strrpos($s_output_to_filename,'\\'));
    if (!is_dir($s_folder_name)){
        IF (!mkdir($s_folder_name,0,true)){
            die ("Failed to create the folder ".$s_folder_name." for file ".$s_output_to_filename);
        }
    }
    $fh = fopen($s_output_to_filename,'a') or die("cant open file".$s_output_to_filename);
    //    fwrite($fh,date("Y-m-d H:i:s",time())."\r\n");
    fwrite($fh,$sys_function_out."\r\n");
    fclose($fh);
G509_END:


G900_END:

    IF (trim(strtoupper($ar_line_details[0])," ") == "MULTILEVEL_1JC")
    {
        $s_multilevel_all_loops = $array_lines[$i];
        goto P_900_END;
    }
    IF (trim(strtoupper($ar_line_details[0])," ") == "MULTILEVEL_1JCIF")
    {
        $s_multilevel_all_loops = $array_lines[$i];
        goto P_900_END;
    }
    IF (trim(strtoupper($ar_line_details[0])," ") == "MULTILEVEL_2JCIF")
    {
        $s_multilevel_all_loops = $s_multilevel_all_loops."~~~".$array_lines[$i];
        goto P_900_END;
    }
    IF (trim(strtoupper($ar_line_details[0])," ") == "MULTILEVEL_2JC")
    {
        $s_multilevel_all_loops = $s_multilevel_all_loops."~~~".$array_lines[$i];
        goto P_900_END;
    }
    IF (trim(strtoupper($ar_line_details[0])," ") == "MULTILEVEL_3JC")
    {
        $s_multilevel_all_loops = $s_multilevel_all_loops."~~~".$array_lines[$i];
        goto P_900_END;
    }
    IF (trim(strtoupper($ar_line_details[0])," ") == "MULTILEVEL_3JCIF")
    {
        $s_multilevel_all_loops = $s_multilevel_all_loops."~~~".$array_lines[$i];
        goto P_900_END;
    }
    IF (trim(strtoupper($ar_line_details[0])," ") == "DO_MULTILEVELJCIF")
    {
        $s_multilevel_all_loops = $array_lines[$i]."~~~".$s_multilevel_all_loops;
        $s_line_in = $s_multilevel_all_loops;
        goto P_900_END;
    }
    IF (trim(strtoupper($ar_line_details[0])," ") == "DO_MULTILEVELJC")
    {
        $s_multilevel_all_loops = $array_lines[$i]."~~~".$s_multilevel_all_loops;
        $s_line_in = $s_multilevel_all_loops;
        goto P_900_END;
    }

/*H100_DO_MULTILEVEL:
    IF (trim(strtoupper($ar_line_details[0])," ") == "MULTILEVEL_1")
    {
        $s_MultiLevelloop1 = $array_lines[$i];
        goto Z800_GET_NEXT;
    }
    IF (trim(strtoupper($ar_line_details[0])," ") == "MULTILEVEL_2")
    {
        $s_MultiLevelloop2 = $array_lines[$i];
        goto Z800_GET_NEXT;
    }
    IF (trim(strtoupper($ar_line_details[0])," ") == "MULTILEVEL_3")
    {
        $s_MultiLevelloop3 = $array_lines[$i];
        goto Z800_GET_NEXT;
    }
    IF (trim(strtoupper($ar_line_details[0])," ") == "DO_MULTILEVEL")
    {
        $sys_function_out .= Pf_b400_do_multilevelloop($dbcnx,$s_MultiLevelloop1,$s_MultiLevelloop2,$s_MultiLevelloop3,"NO",$s_details_def,$s_details_data,$s_sessionno,"ut_jcl.php DO_MULTILEVEL",$ko_map_path);
        goto Z800_GET_NEXT;
    }
H900_END:
*/
H1_000_MULTILEVEL:

    IF (trim(strtoupper($ar_line_details[0])," ") == "MULTILEVEL_1")
    {
        GOTO H1_100_DO;
    }
    IF (trim(strtoupper($ar_line_details[0])," ") == "MULTILEVEL_1IF")
    {
        GOTO H1_200_DOIF;
    }
    GOTO H1_900_END;
H1_100_DO:
    $s_temp_value = $array_lines[$i];
    GOTO H1_400_DO;
H1_200_DOIF:
    IF (strtoupper($ar_line_details[1]) <> "V1")
    {
        GOTO H1_210_do_If_v2;
    }
    $s_field_to_check = $ar_line_details[2];
    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];
    $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl h1_200 ");
    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_h1_200 ");
    if ($s_true == "FALSE")
    {
        z901_dump($sys_debug, $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);
//        $sys_function_out = "";
        GOTO Z800_GET_NEXT;
    }
// gw 20100502  normal multilevel  MULTILEVEL_2|udp_bm_issues.htm|detail_loop|udp_bm_issues.htm|null_loop|*noerror* SELECT *  FROM events as L2_ev where (`CompanyId` =  #p%!_COOKIE_ud_companyid_!%#p and L2_ev.GlobalEventTypeId = "#p%!_REC_L1_et_GlobalEventTypeId_!%#p")  and #p%!_date_selection_!%#p |MLL2_NUMROWS|NONE|NOne|None|NONE|END
// gw 20100502  MULTILEVEL_2if|v1|session_udp_active_device|equal|all|udp_bm_issues.htm|detail_loop|udp_bm_issues.htm|null_loop|*noerror* SELECT *  FROM events as L2_ev where (`CompanyId` =  #p%!_COOKIE_ud_companyid_!%#p and L2_ev.GlobalEventTypeId = "#p%!_REC_L1_et_GlobalEventTypeId_!%#p")  and #p%!_date_selection_!%#p |MLL2_NUMROWS|NONE|NOne|None|NONE|END

    $s_temp_value = $ar_line_details[0]."|".$ar_line_details[5]."|".$ar_line_details[6]."|".$ar_line_details[7]."|".$ar_line_details[8]."|".$ar_line_details[9]."|".$ar_line_details[10]."|".$ar_line_details[11]."|".$ar_line_details[12]."|".$ar_line_details[13]."|".$ar_line_details[14]."|".$ar_line_details[15];
    GOTO H1_400_DO;
H1_210_do_If_v2:
H1_400_DO:
    $s_MultiLevelloop1 = $s_temp_value;

    goto Z800_GET_NEXT;
H1_900_END:

H2_000_MULTILEVEL:
    IF (trim(strtoupper($ar_line_details[0])," ") == "MULTILEVEL_2")
    {
        GOTO H2_100_DO;
    }
    IF (trim(strtoupper($ar_line_details[0])," ") == "MULTILEVEL_2IF")
    {
        GOTO H2_200_DOIF;
    }
    GOTO H2_900_END;
H2_100_DO:
    $s_temp_value = $array_lines[$i];
    GOTO H2_400_DO;
H2_200_DOIF:
    IF (strtoupper($ar_line_details[1]) <> "V1")
    {
        GOTO H2_210_do_If_v2;
    }
    $s_field_to_check = $ar_line_details[2];
    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];
    $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl h2_200 ");
    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_h2_200 ");
    if ($s_true == "FALSE")
    {
        z901_dump($sys_debug, $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);
//        $sys_function_out = "";
        GOTO Z800_GET_NEXT;
    }

    $s_temp_value =  "MULTILEVEL_2"."|".$ar_line_details[5]."|".$ar_line_details[6]."|".$ar_line_details[7]."|".$ar_line_details[8]."|".$ar_line_details[9]."|".$ar_line_details[10]."|".$ar_line_details[11]."|".$ar_line_details[12]."|".$ar_line_details[13]."|".$ar_line_details[14]."|".$ar_line_details[15];

    GOTO H2_400_DO;
H2_210_do_If_v2:
H2_400_DO:
        $s_MultiLevelloop2 = $s_temp_value;
    goto Z800_GET_NEXT;
H2_900_END:

H3_000_MULTILEVEL:
    IF (trim(strtoupper($ar_line_details[0])," ") == "MULTILEVEL_3")
    {
        GOTO H3_100_DO;
    }
    IF (trim(strtoupper($ar_line_details[0])," ") == "MULTILEVEL_3IF")
    {
        GOTO H3_200_DOIF;
    }
    GOTO H3_900_END;
H3_100_DO:
    $s_temp_value = $array_lines[$i];
    GOTO H3_400_DO;
H3_200_DOIF:
    IF (strtoupper($ar_line_details[1]) <> "V1")
    {
        GOTO H3_210_do_If_v2;
    }
    $s_field_to_check = $ar_line_details[2];
    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];
    $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl h3_200 ");
    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_h3_200 ");
    if ($s_true == "FALSE")
    {
        z901_dump($sys_debug, $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);
//        $sys_function_out = "";
        GOTO Z800_GET_NEXT;
    }

    $s_temp_value = $ar_line_details[0]."|".$ar_line_details[5]."|".$ar_line_details[6]."|".$ar_line_details[7]."|".$ar_line_details[8]."|".$ar_line_details[9]."|".$ar_line_details[10]."|".$ar_line_details[11]."|".$ar_line_details[12]."|".$ar_line_details[13]."|".$ar_line_details[14]."|".$ar_line_details[15];
    GOTO H3_400_DO;
H3_210_do_If_v2:
H3_400_DO:
        $s_MultiLevelloop3 = $s_temp_value;
    goto Z800_GET_NEXT;
H3_900_END:
//GW20110714 -Added the if check
//H4_DO:
//    IF (trim(strtoupper($ar_line_details[0])," ") == "DO_MULTILEVEL")
//    {
////
////    echo "<br>s_MultiLevelloop1=".$s_MultiLevelloop1."<br>";
////    echo "<br>s_MultiLevelloop2=".$s_MultiLevelloop2."<br>";
////    echo "<br>s_MultiLevelloop3=".$s_MultiLevelloop3."<br>";
//
//        $sys_function_out .= Pf_b400_do_multilevelloop($dbcnx,$s_MultiLevelloop1,$s_MultiLevelloop2,$s_MultiLevelloop3,"NO",$s_details_def,$s_details_data,$s_sessionno,"ut_jcl.php DO_MULTILEVEL",$ko_map_path);
//        goto Z800_GET_NEXT;
//    }
//H4_900_EXIT:

H4_DO:
    IF (trim(strtoupper($ar_line_details[0])," ") == "DO_MULTILEVEL")
    {
        GOTO H4_100_DO;
    }
    IF (trim(strtoupper($ar_line_details[0])," ") == "DO_MULTILEVELIF")
    {
        GOTO H4_200_DOIF;
    }
    GOTO H4_900_END;
H4_100_DO:
    $s_temp_value = $array_lines[$i];
    GOTO H4_400_DO;
H4_200_DOIF:
    IF (strtoupper($ar_line_details[1]) <> "V1")
    {
        GOTO H4_210_do_If_v2;
    }
    $s_field_to_check = $ar_line_details[2];
    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];
    $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl H4_200 ");
    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_H4_200 ");
    if ($s_true == "FALSE")
    {
        z901_dump($sys_debug, $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);
//        $sys_function_out = "";
        GOTO Z800_GET_NEXT;
    }

    $s_temp_value = $ar_line_details[0]."|".$ar_line_details[5];
    GOTO H4_400_DO;
H4_210_do_If_v2:
H4_400_DO:
    $sys_function_out .= Pf_b400_do_multilevelloop($dbcnx,$s_MultiLevelloop1,$s_MultiLevelloop2,$s_MultiLevelloop3,"NO",$s_details_def,$s_details_data,$s_sessionno,"ut_jcl.php DO_MULTILEVEL",$ko_map_path);
    goto Z800_GET_NEXT;
H4_900_END:

H999_skip_multi:

I_000_DO_FUNCTION:
//DOFUNCTION|Functionname|details_def_name|parameter|END
// dofunction|countfolderfiles|folder_cnt|folder_cnt_1|end
// adds the results of the sql to the end of run wide details def/data
    IF (strtoupper($ar_line_details[0]) == "DOFUNCTION")
    {
        GOTO I_100_DO;
    }
    IF (strtoupper($ar_line_details[0]) == "DOFUNCTIONIF")
    {
        GOTO I_200_DO_IF;
    }
    GOTO I_900_END;
I_100_DO:
    $s_function = $ar_line_details[1];
    $s_def_name = $ar_line_details[2];
    $s_paramater = $ar_line_details[3];
    GOTO I_400_RUN;
I_200_DO_IF:
    IF (strtoupper($ar_line_details[1]) <> "V1")
    {
        GOTO I_210_do_if_v2;
    }
    $s_field_to_check = $ar_line_details[2];
    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];
    $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl i200 ");
    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_i200 ");
    if ($s_true == "FALSE")
    {
        z901_dump($sys_debug, $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);
        $sys_function_out = "";
        GOTO Z800_GET_NEXT;
    }

    $s_function = $ar_line_details[5];
    $s_def_name = $ar_line_details[6];
    $s_paramater = $ar_line_details[7];
    GOTO I_400_RUN;
I_210_do_if_v2:
I_400_RUN:
    IF (trim(strtoupper($ar_line_details[1])," ") == "COUNTFOLDERFILES")
    {
        $filecount = count(glob("".$s_paramater));
        $s_details_def = $s_details_def."|".$s_def_name;
        $s_details_data = $ar_line_details[1]."|".$filecount;
        goto Z800_GET_NEXT;
    }

    goto Z800_GET_NEXT;
I_900_END:

J_000_SUSPEND_REFRESH:
//suspend_refresh|from=16:19|to=16:25|map=owl_bdymain_suspend.htm|loop=suspended_loop|end
    IF (strtoupper($ar_line_details[0]) == STRTOUPPER("suspend_refresh"))
    {
        GOTO J_100_DO;
    }
    IF (strtoupper($ar_line_details[0]) == STRTOUPPER("suspend_refreshif"))
    {
        GOTO J_200_DO_IF;
    }
    GOTO J_900_END;
J_100_DO:
    $s_suspend_from = $ar_line_details[1];
    $s_suspend_to = $ar_line_details[2];
    $s_map = $ar_line_details[3];
    $s_loop  = $ar_line_details[4];
    $s_refresh  = $ar_line_details[5];
    GOTO J_400_RUN;
J_200_DO_IF:
    IF (strtoupper($ar_line_details[1]) <> "V1")
    {
        GOTO J_210_do_if_v2;
    }
    $s_field_to_check = $ar_line_details[2];
    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];
    $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl J200 ");
    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_J200 ");
    if ($s_true == "FALSE")
    {
        z901_dump($sys_debug, $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);
        $sys_function_out = "";
        GOTO Z800_GET_NEXT;
    }

    $s_suspend_from = $ar_line_details[5];
    $s_suspend_to = $ar_line_details[6];
    $s_map = $ar_line_details[7];
    $s_loop  = $ar_line_details[8];
    $s_refresh  = $ar_line_details[9];
    GOTO J_400_RUN;
J_210_do_if_v2:
J_400_RUN:
    $s_suspend_from = str_replace("from=","",$s_suspend_from);
    $s_suspend_to = str_replace("to=","",$s_suspend_to);
    $s_map = str_replace("map=","",$s_map);
    $s_loop  = str_replace("loop=","",$s_loop);

    $s_suspend_from = str_replace("FROM=","",$s_suspend_from);
    $s_suspend_to = str_replace("TO=","",$s_suspend_to);
    $s_map = str_replace("MAP=","",$s_map);
    $s_loop  = str_replace("LOOP=","",$s_loop);

    $s_refresh  =  substr($s_refresh,strpos($s_refresh,"=")+1);

    $s_suspend_from_chk = str_replace(":","",$s_suspend_from)."00";
    $s_suspend_to_chk = str_replace(":","",$s_suspend_to."00");

    $now_hm = date("His");
    $s_details_def = $s_details_def."|suspend_from|suspend_to|suspend_now|suspend_from_chk|suspend_to_chk|suspend_secs|suspend_now";
    $s_details_data = $s_details_data."|".$s_suspend_from."|".$s_suspend_to."|".$now_hm."|".$s_suspend_from_chk."|".$s_suspend_to_chk."|".$s_refresh."|".$now_hm;

    $s_suspend_from = str_replace(":","",$s_suspend_from);
    $s_suspend_to = str_replace(":","",$s_suspend_to);

    if ($now_hm > $s_suspend_from_chk)
    {
        if ($now_hm < $s_suspend_to_chk)
        {
            goto J_500_DO_SUSPEND;
        }
    }
    goto  J_900_END;
J_500_DO_SUSPEND:
    $sys_function_out .= pf_b100_DO_DRAW_HTML($class_main->clmain_set_map_path_n_name($ko_map_path,$s_map), $s_loop,"NO",$s_details_def,$s_details_data);
    goto Z700_END;
J_900_END:

K_000_SQLCHECK:
//sqlcheck|stopjcl=yes|sql=Select * from log where type = heartbeat and created time = last 20 mins|onfailmap=owl_bdymain_noheartbeat.htm|onfailloop=noheatbeat_loop|refresh_secs=700|end

    IF (strtoupper($ar_line_details[0]) == STRTOUPPER("SQLCHECK"))
    {
        GOTO K_100_DO;
    }
    IF (strtoupper($ar_line_details[0]) == STRTOUPPER("sqlcheckif"))
    {
        GOTO K_200_DO_IF;
    }
    GOTO K_900_END;
K_100_DO:
    $s_stop_jcl =$ar_line_details[1];
    $s_sql= $ar_line_details[2];
    $s_map = $ar_line_details[3];
    $s_loop  = $ar_line_details[4];
    $s_refresh  = $ar_line_details[5];
    GOTO K_400_RUN;
K_200_DO_IF:
    IF (strtoupper($ar_line_details[1]) <> "V1")
    {
        GOTO K_210_do_if_v2;
    }
    $s_field_to_check = $ar_line_details[2];
    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];
    $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl k200 ");
    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_k200 ");
    if ($s_true == "FALSE")
    {
        z901_dump($sys_debug, $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);
        $sys_function_out = "";
        GOTO Z800_GET_NEXT;
    }

    $s_stop_jcl =$ar_line_details[5];
    $s_sql= $ar_line_details[6];
    $s_map = $ar_line_details[7];
    $s_loop  = $ar_line_details[8];
    $s_refresh  = $ar_line_details[9];
    GOTO K_400_RUN;
K_210_do_if_v2:
K_400_RUN:
    $s_stop_jcl =  substr($s_stop_jcl,strpos($s_stop_jcl,"=")+1);
    $s_sql = substr($s_sql,strpos($s_sql,"=")+1);
    $s_map = substr($s_map,strpos($s_map,"=")+1);
    $s_loop  = substr($s_loop,strpos($s_loop,"=")+1);
    $s_refresh  =  substr($s_refresh,strpos($s_refresh,"=")+1);

    $s_details_def = $s_details_def."|s_stop_jcl|s_sql";
    $s_details_data = $s_details_data."|".$s_stop_jcl."|".$s_sql;


    // run the sql
    // if got records goto end

    // draw html
    $sys_function_out .= pf_b100_DO_DRAW_HTML($class_main->clmain_set_map_path_n_name($ko_map_path,$s_map), $s_loop,"NO",$s_details_def,$s_details_data);
    // if fatal goto z900_end
    if ($s_stop_jcl == "YES") {
        goto Z900_EXIT;
    }
    // else goto k900_end
    goto  K_900_END;
K_500_DO_SUSPEND:
    $sys_function_out .= pf_b100_DO_DRAW_HTML($class_main->clmain_set_map_path_n_name($ko_map_path,$s_map), $s_loop,"NO",$s_details_def,$s_details_data);
    goto Z700_END;
K_900_END:

L_000_UPDATE_RECORD:
//GW20110511 - MOVED TO CLASS_PROCESS_JOBLINE
/*  20110511 - moved to class_process_jobline


//"update_record|V1|tablename|uk_fieldname|uk_value|DEBUG|set statement|end";
//"update_recordif|v1|session_udp_active_device|equal|all|tablename|uk_fieldname|uk_value|DEBUG|set statement|end";
    IF (strtoupper($ar_line_details[0]) == STRTOUPPER("UPDATE_RECORD"))
    {
        GOTO L_100_DO;
    }
    IF (strtoupper($ar_line_details[0]) == STRTOUPPER("UPDATE_RECORDif"))
    {
        GOTO L_200_DO_IF;
    }
    GOTO L_900_END;
L_100_DO:
// gw20101222 - no change to the inbound line     $s_line_in = $s_line_in;
    GOTO L_400_RUN;
L_200_DO_IF:
    IF (strtoupper($ar_line_details[1]) <> "V1")
    {
        GOTO L_210_do_if_v2;
    }
    $s_field_to_check = $ar_line_details[2];
    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];
    $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl l200 ");
    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_l200 ");
    if ($s_true == "FALSE")
    {
        z901_dump($sys_debug, $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);
        $sys_function_out = "";
        GOTO Z800_GET_NEXT;
    }
//GW20101222 - ****************  NOT TESTED ******************
// gw20101222 - build s_line_in with out the if stuff
    $s_line_in = "UPDATE_RECORD|V1|".$ar_line_details[5]."|".$ar_line_details[6]."|".$ar_line_details[7]."|".$ar_line_details[8]."|".$ar_line_details[9]."|".$ar_line_details[10];
    GOTO L_400_RUN;
L_210_do_if_v2:
L_400_RUN:
// GW20110116 - DOES NOT APPLY TO UTJCL YET - SEE UT_ACTION FOR THE PROCESS TO MAKE THIS WORK
/*
    IF (strpos(strtoupper($s_line_In), "UPDATE_DATA_BLOB_WITH_DB_FIELDS") === false)
    {
        goto L_500_SKIP_DATA_BLOB;
    }

    $s_data_blob_fields = "";
L_410_build_data_blob_fields:
// IF LINE HAS UPDATE_ALL_DATA_BLOB
// GET ALL THE POSTV VALUES WITH _DB_ IN THEM
    $array_def = explode("|",$s_details_def);
    $array_data = explode("|",$s_details_data);

    if (count($array_def)<> count($array_data))
    {
// do nothting yet gw20110116
    }
    for ($i2 = 0; $i2 < count($array_def); $i2++)
    {
        IF (strpos($array_def[$i2],"_DB_") === false)
        {}else
        {
            $s_tmp = $array_def[$i2];
            $s_tmp = substr($s_tmp,strpos($s_tmp,"_DB_"));
            $s_tmp = str_replace("_DB_","",$s_tmp);
            $s_data_blob_fields = $s_data_blob_fields."|".$s_tmp."=".$array_data[$i2];
        }
   }
l_420_get_original_data_blob:
// GET THE RECORD FOR THE KEY

// UPDATE THE RECORD_DATA_BLOB VALUE WITH THE SCREEN VALUES

//    FOR EACH ENTRY CHECK_FIELD
//        DO THE CLMAIN_INSERT XML  -=WHICH WILL MODIFY THE VALUE IF IT EXITS
//    E

    $s_upd_data_blob = ' DATA_BLOB = "'.$s_data_blob.'"';
    $s_line_in = "UPDATE_RECORD|V1|".$ar_line_details[2]."|".$ar_line_details[3]."|".$ar_line_details[4]."|".$ar_line_details[5]."|".$s_upd_data_blob."|".$ar_line_details[10];
L_500_SKIP_DATA_BLOB:


    $s_temp = $class_sql->sql_a200_update_record($dbcnx,$s_line_in,"no",$s_details_def,$s_details_data,$s_sessionno);
    IF (STRPOS($s_temp,"|^%##%^|")  === FALSE)
    {}ELSE{
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
        $s_temp = "";
    }
    goto Z800_GET_NEXT;
*/
L_900_END:

M_000_ADD_RECORD:
    IF (strtoupper($ar_line_details[0]) == STRTOUPPER("ADD_RECORD"))
    {
        GOTO M_100_DO;
    }
    IF (strtoupper($ar_line_details[0]) == STRTOUPPER("ADD_RECORDif"))
    {
        GOTO M_200_DO_IF;
    }
    GOTO M_900_END;
M_100_DO:
// gw20101222 - no change to the inbound line     $s_line_in = $s_line_in;
    GOTO M_400_RUN;
M_200_DO_IF:
    IF (strtoupper($ar_line_details[1]) <> "V1")
    {
        GOTO M_210_do_if_v2;
    }
    $s_field_to_check = $ar_line_details[2];
    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];

    if (strpos(strtoupper($s_comparison_operator),"DOSYSDEBUG") === FALSE)
    {}else{
        $sys_debug  = "YES";
        $sys_debug_text = "via operator  value of ".$s_comparison_operator;
    }

    IF ($sys_debug == "YES"){echo $sys_debug_text." s_field_to_check=[]".$s_field_to_check."[]";};
    IF ($sys_debug == "YES"){echo $sys_debug_text." s_comparison_operator =[]".$s_comparison_operator."[]";};
    IF ($sys_debug == "YES"){echo $sys_debug_text." s_compare_value =[]".$s_compare_value."[]";};

// gw 20140106 - if the field to check has #p then it may have more than 1 field to change
    if (!strpos($s_field_to_check,"#p") === false)
    {
        $s_field_to_check = str_replace("#p","|",$s_field_to_check);
        $s_field_to_check = str_replace("#P","|",$s_field_to_check);
        $s_field_to_check = str_replace("__","_",$s_field_to_check);
        IF ($sys_debug == "YES"){echo $sys_debug_text." pre  v300  s_field_to_check=[]".$s_field_to_check."[]";};
        $s_field_value = $class_main->clmain_v200_load_line($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl m200");
    }else{
        $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl m200");
    }
    IF ($sys_debug == "YES"){echo $sys_debug_text." post  v300  s_field_to_check=[]".$s_field_to_check."[]";};

    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","ut_jcl_m200 ");
    if ($s_true == "FALSE")
    {
        z901_dump($sys_debug, $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);
        $sys_function_out = "";
        GOTO Z800_GET_NEXT;
    }
    $s_line_in = "ADD_RECORD|V1|".$ar_line_details[5]."|".$ar_line_details[6]."|".$ar_line_details[7]."|".$ar_line_details[8]."|".$ar_line_details[9]."|".$ar_line_details[10];
    GOTO M_400_RUN;
M_210_do_if_v2:
M_400_RUN:
    $s_temp = $class_sql->sql_a100_add_record($dbcnx,$s_line_in,"no",$s_details_def,$s_details_data,$s_sessionno);
    IF (STRPOS($s_temp,"|^%##%^|")  === FALSE)
    {}ELSE{
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
        $s_temp = "";
    }
    goto Z800_GET_NEXT;
M_900_END:
//MAKE_XMLFIELD|DATA_BLOB|whofor="",addrline1="",addrlin2="",suburb="",postcode="",cross_street="",locn_note="",phoneno="",mobile="",email="",sms_alert="",machine_make="",machine_model="",machine_type="",job_overview="",job_description=""}|END
N_000_MAKE_XML:
    IF (strtoupper($ar_line_details[0]) == STRTOUPPER("MAKE_XMLFIELD"))
    {
        GOTO N_100_DO;
    }
    IF (strtoupper($ar_line_details[0]) == STRTOUPPER("MAKE_XMLFIELDIF"))
    {
        GOTO N_200_DO_IF;
    }
    GOTO N_900_END;
N_100_DO:
// gw20101222 - no change to the inbound line     $s_line_in = $s_line_in;
    GOTO N_400_RUN;
N_200_DO_IF:
    IF (strtoupper($ar_line_details[1]) <> "V1")
    {
        GOTO N_210_do_if_v2;
    }
    $s_field_to_check = $ar_line_details[2];
    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];
    $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl n200 ");
    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcN_n200 ");
    if ($s_true == "FALSE")
    {
        z901_dump($sys_debug, $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);
        $sys_function_out = "";
        GOTO Z800_GET_NEXT;
    }
//GW20101222 - ****************  NOT TESTED ******************
// gw20101222 - build s_line_in with out the if stuff
    $s_line_in = "MAKE_XML|".$ar_line_details[5]."|".$ar_line_details[6];
    GOTO N_400_RUN;
N_210_do_if_v2:
N_400_RUN:
    $s_temp = $class_main->clmain_u575_make_xml($s_line_in,"no",$s_details_def,$s_details_data,$s_sessionno);
    IF (STRPOS($s_temp,"|^%##%^|")  === FALSE)
    {}ELSE{
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
        $s_temp = "";
    }
    goto Z800_GET_NEXT;
N_900_END:
//\command|filecopy|(from filename)#p%!_SESSION_ko_map_path_!%#p#p%!_jcl_outfilenameonly_!%#p|(tofilename)z:\#p%!_jcl_outfilename_!%#p
O_000_RUN_SYSCOMAND:
    IF (strtoupper($ar_line_details[0]) == STRTOUPPER("RUN_SYSCOMMAND"))
    {
        GOTO O_100_DO;
    }
    IF (strtoupper($ar_line_details[0]) == STRTOUPPER("RUN_SYSCOMMANDIF"))
    {
        GOTO O_200_DO_IF;
    }
    GOTO O_900_END;
O_100_DO:
    GOTO O_400_RUN;
O_200_DO_IF:
    IF (strtoupper($ar_line_details[1]) <> "V1")
    {
        GOTO O_210_do_if_v2;
    }
    $s_field_to_check = $ar_line_details[2];
    $s_comparisoO_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];
    $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl o200 ");
    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparisoO_operator,$s_compare_value,"no","utjcO_o200 ");
    if ($s_true == "FALSE")
    {
        z901_dump($sys_debug, $sys_debug_text." failed value ".$s_field_value." ".$s_comparisoO_operator." ".$s_compare_value);
        $sys_functioO_out = "";
        GOTO Z800_GET_NEXT;
    }
//GW20101222 - ****************  NOT TESTED ******************
// gw20101222 - build s_line_in with out the if stuff
    $s_line_in = "RUN_SYSCOMMAND|".$ar_line_details[5]."|".$ar_line_details[6]."|".$ar_line_details[7];
    GOTO O_400_RUN;
O_210_do_if_v2:
O_400_RUN:
    $s_temp = $class_main->clmain_v955_run_syscommand($s_line_in,"no","jcl_o400",$s_details_def,$s_details_data,$s_sessionno);
    IF (STRPOS($s_temp,"|^%##%^|")  === FALSE)
    {}ELSE{
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
        $s_temp = "";
    }
    goto Z800_GET_NEXT;
O_900_END:
//Add_utl_activity - see the u540_utl_activty explaination for paramaters
/*    parameters = <br>
    clmain_u540_utl_activity_log($ps_dbcnx,<br>
    $ps_rectype,<br>
    $ps_companyid,<br>
    $ps_userid,<br>
    $ps_summary ,<br>
    $ps_data_blob,<br>
    $ps_key1, and  $ps_key1def, <br>
    $ps_key2, $ps_key2def, <br>
    $ps_key3, $ps_key3def, <br>
    $ps_key4, $ps_key4def,<br>
    $ps_key5, $ps_key5def<br>
*/
P_000_ADD_ACTIVITY:
    IF (strtoupper($ar_line_details[0]) == STRTOUPPER("ADD_UTL_ACTIVITY"))
    {
        GOTO P_100_DO;
    }
    IF (strtoupper($ar_line_details[0]) == STRTOUPPER("ADD_UTL_ACTIVITYIF"))
    {
        GOTO P_200_DP_IF;
    }
    GOTO P_900_END;
P_100_DO:
    GOTO P_400_RUN;
P_200_DP_IF:
    IF (strtoupper($ar_line_details[1]) <> "V1")
    {
        GOTO P_210_dP_if_v2;
    }
    $s_field_to_check = $ar_line_details[2];
    $s_comparisoo_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];
    $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl o200 ");
    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparisoo_operator,$s_compare_value,"no","utjcP_o200 ");
    if ($s_true == "FALSE")
    {
        z901_dump($sys_debug, $sys_debug_text." failed value ".$s_field_value." ".$s_comparisoo_operator." ".$s_compare_value);
        $sys_function_out = "";
        GOTO Z800_GET_NEXT;
    }

    $ar_line_details[6] = $class_main->clmain_v200_load_line($ar_line_details[6],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4d ");
    $ar_line_details[7] = $class_main->clmain_v200_load_line($ar_line_details[7],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4e ");
    $ar_line_details[8] = $class_main->clmain_v200_load_line($ar_line_details[8],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4b ");
    $ar_line_details[9] = $class_main->clmain_v200_load_line($ar_line_details[9],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4b ");
    $ar_line_details[10] = $class_main->clmain_v200_load_line($ar_line_details[10],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4h ");
    $ar_line_details[12] = $class_main->clmain_v200_load_line($ar_line_details[12],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4j ");
    $ar_line_details[14] = $class_main->clmain_v200_load_line($ar_line_details[14],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4l ");
    $ar_line_details[16] = $class_main->clmain_v200_load_line($ar_line_details[16],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4n ");
    $ar_line_details[18] = $class_main->clmain_v200_load_line($ar_line_details[18],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4a ");

/*GW20110412
    $ar_line_details[6] = $class_main->clmain_v300_set_variable($ar_line_details[6],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4d ");
    $ar_line_details[7] = $class_main->clmain_v300_set_variable($ar_line_details[7],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4e ");
    $ar_line_details[10] = $class_main->clmain_v300_set_variable($ar_line_details[10],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4h ");
    $ar_line_details[12] = $class_main->clmain_v300_set_variable($ar_line_details[12],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4j ");
    $ar_line_details[14] = $class_main->clmain_v300_set_variable($ar_line_details[14],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4l ");
    $ar_line_details[16] = $class_main->clmain_v300_set_variable($ar_line_details[16],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4n ");
    $ar_line_details[18] = $class_main->clmain_v300_set_variable($ar_line_details[18],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4a ");


*/
    $tmp = $class_main->clmain_u540_utl_activity_log($dbcnx,$ar_line_details[5],$ar_line_details[6],$ar_line_details[7],$ar_line_details[8],$ar_line_details[9],$ar_line_details[10],$ar_line_details[11],$ar_line_details[12],$ar_line_details[13],$ar_line_details[14],$ar_line_details[15],$ar_line_details[16],$ar_line_details[17],$ar_line_details[18],$ar_line_details[19]);
    goto Z800_GET_NEXT;
P_210_dP_if_v2:
P_400_RUN:

// change the values on the lines
    $ar_line_details[3] = $class_main->clmain_v200_load_line($ar_line_details[3],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4a ");
    $ar_line_details[4] = $class_main->clmain_v200_load_line($ar_line_details[4],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4b ");
    $ar_line_details[5] = $class_main->clmain_v200_load_line($ar_line_details[5],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4b ");
    $ar_line_details[6] = $class_main->clmain_v200_load_line($ar_line_details[6],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4b ");
    $ar_line_details[7] = $class_main->clmain_v200_load_line($ar_line_details[7],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4e ");
    $ar_line_details[9] = $class_main->clmain_v200_load_line($ar_line_details[9],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4g ");
    $ar_line_details[11] = $class_main->clmain_v200_load_line($ar_line_details[11],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4i ");
    $ar_line_details[13] = $class_main->clmain_v200_load_line($ar_line_details[13],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4k ");
    $ar_line_details[15] = $class_main->clmain_v200_load_line($ar_line_details[15],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4m ");

/* GW20110412
// change the values on the lines
//    $ar_line_details[2] = $class_main->clmain_v300_set_variable($ar_line_details[2],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p_400 ");
    $ar_line_details[3] = $class_main->clmain_v300_set_variable($ar_line_details[3],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4a ");
    $ar_line_details[4] = $class_main->clmain_v300_set_variable($ar_line_details[4],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4b ");
//    $ar_line_details[5] = $class_main->clmain_v300_set_variable($ar_line_details[5],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4c ");
//    $ar_line_details[6] = $class_main->clmain_v300_set_variable($ar_line_details[6],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4d ");
    $ar_line_details[7] = $class_main->clmain_v300_set_variable($ar_line_details[7],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4e ");
//    $ar_line_details[8] = $class_main->clmain_v300_set_variable($ar_line_details[8],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4f ");
    $ar_line_details[9] = $class_main->clmain_v300_set_variable($ar_line_details[9],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4g ");
//    $ar_line_details[10] = $class_main->clmain_v300_set_variable($ar_line_details[10],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4h ");
    $ar_line_details[11] = $class_main->clmain_v300_set_variable($ar_line_details[11],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4i ");
//    $ar_line_details[12] = $class_main->clmain_v300_set_variable($ar_line_details[12],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4j ");
    $ar_line_details[13] = $class_main->clmain_v300_set_variable($ar_line_details[13],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4k ");
//   $ar_line_details[14] = $class_main->clmain_v300_set_variable($ar_line_details[14],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4l ");
    $ar_line_details[15] = $class_main->clmain_v300_set_variable($ar_line_details[15],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4m ");
//    $ar_line_details[16] = $class_main->clmain_v300_set_variable($ar_line_details[16],$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl p4n ");
*/
    $tmp = $class_main->clmain_u540_utl_activity_log($dbcnx,$ar_line_details[2],$ar_line_details[3],$ar_line_details[4],$ar_line_details[5],$ar_line_details[6],$ar_line_details[7],$ar_line_details[8],$ar_line_details[9],$ar_line_details[10],$ar_line_details[11],$ar_line_details[12],$ar_line_details[13],$ar_line_details[14],$ar_line_details[15],$ar_line_details[16]);
    goto Z800_GET_NEXT;
P_900_END:
// goto Z800_GET_NEXT;
//    IF (strtoupper(substr($s_line_in,0,11)) == "ADD_DETAILS")
//    {
//     echo "<br>**************************************************";
//        echo "<br> test of add_details<br>".$s_line_in."<br>";
//    }

//        echo "<br>ut jcl debug testing  test of line= <br>".$s_line_in."<br>";


    // gw20110502 - added this process to use the class
    $xml_arr  ="";


//DIE("GWDIE UT_JCL 3011 s_line_in=[]".$s_line_in."[]");


    $s_temp = $class_clprocessjobline->pf_clprocessjobline($dbcnx,$s_line_in,$s_details_def,$s_details_data,$xml_arr,$s_sessionno,"NO");
    if (strpos($s_temp,"|^%##%^|") !== false)
    {
       if (strpos(strtoupper($s_line_in),strtoupper("|jcl_get_new_def_n_data|")) !== false)
        {
        //    echo "<br>clmain b100 in a125 <br>sys_function_out<br>".$sys_function_out."<br><br>";
        //gw20110512 -dont change sysout
            $ar_line_details = explode("|^%##%^|",$s_temp);
            $s_details_def = $ar_line_details[0];
            $s_details_data = $ar_line_details[1];
            goto Z800_GET_NEXT;
        }
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
        goto P_950_NO_DRAW;
    }
/*
//    ECHO "<br>gwdebug ".$s_line_in." sys_class at []".strpos(strtoupper($s_line_in),"SYS_CLASS")."[]";
    IF (strpos(strtoupper($s_line_in),"SYS_CLASS") !== false)
    {
        goto P_950_NO_DRAW;
    }
    IF (strpos(strtoupper($s_line_in),"ADD_DETAILS") !== false)
    {
        goto P_950_NO_DRAW;
    }
    IF (strpos(strtoupper($s_line_in),"url_mode") !== false)
    {
        goto P_950_NO_DRAW;
    }
*/
    $sys_function_out = $sys_function_out.$s_temp;
    GOTO Z800_GET_NEXT;
P_950_NO_DRAW:

//    DIE ("<BR> UT_JCL PRE Z800_UNKNOWN LINE <BR>The jcl file ".$map." has an unknown line =".$s_line_in);
Z800_GET_NEXT:
    $i = $i + 1;
    goto B100_GET_REC;

Z700_END:
    if (trim($s_no_jcl_in_file," ") == "")
    { }else{
        $sys_function_out .= "<BR><BR>ERROR - from ut.jcl.php **".$s_no_jcl_in_file."**<br>";
    }
    if (trim($s_no_jcl_in_file," ") <> "")
    {
        $sys_function_out .= "<BR><BR>ERROR - from ut.jcl.php **".$s_no_jcl_in_file."**<br>";
    }
Z900_EXIT:
//echo "<br>GWDEBUG  end utjcl  s_output_to_file=[]".$s_output_to_file."[] sys_function_out=[]".$sys_function_out."[]";
//echo "<br>utjcl 1565 do   s_output_to_file=[]".$s_output_to_file."[] s_output_to_filename=[]".$s_output_to_filename."[] s_output_to_atend=[]".$s_output_to_atend."[] s_output_to_fileonly=[]".$s_output_to_fileonly."[]";


// PUT THIS IS A FILE
    if (trim($s_output_to_file) <> "YES")
    {
        GOTO Z950_SKIP_FILE;
    }
    IF (trim($s_output_to_filename) == "")
    {
        $s_output_to_filename = "ut_jcl_output_to_file.txt";
    }

    $s_folder_name = str_replace("/","\\",$s_output_to_filename);
    $s_folder_name = substr($s_folder_name,0,strrpos($s_folder_name,'\\'));

//gw20110130    $s_folder_name = substr($s_output_to_filename,0,strrpos($s_output_to_filename,'\\'));
    if (!is_dir($s_folder_name)){
        IF (!mkdir($s_folder_name,0,true)){
            die ("Failed to create the folder ".$s_folder_name." for file ".$s_output_to_filename);
        }
    }
    $fh = fopen($s_output_to_filename,'a') or die("cant open file".$s_output_to_filename);
//    fwrite($fh,date("Y-m-d H:i:s",time())."\r\n");
    fwrite($fh,$sys_function_out."\r\n");
    fclose($fh);
/*
echo "<br>GWDEBUG  end utjcl>";

    echo "<br> now=".date("Y-m-d H:i:s",time());
    echo "<br> s_output_to_file=".$s_output_to_file;
echo "<br> s_output_to_filename=".$s_output_to_filename;
echo "<br> s_output_to_atend=".$s_output_to_atend;
echo "<br> s_output_to_atend_do=".$s_output_to_atend_do;
*/
 //die ("gwdead");
    if (strtoupper($s_output_to_atend) == "OPEN")
    {
        GOTO Z905_OPEN_TEXT_FILE;
    }
    if (strtoupper($s_output_to_atend) == "DOMAP")
    {
        GOTO Z910_DO_MAP;
    }
    if(trim(strtoupper($s_output_to_fileonly)) =="YES"){
        GOTO Z990_EXIT;
    }
    GOTO Z950_SKIP_FILE;

Z905_OPEN_TEXT_FILE:
    $sys_function_out = "";
    $sys_function_out .= '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
    $sys_function_out .= '<html>';
    $sys_function_out .= '<head>';
//    $sys_function_out .= '<meta HTTP-EQUIV="REFRESH" content="0; '.$s_output_to_atend_do.'">';
    $sys_function_out .= '<br>This process requires you to open <a href='.$s_output_to_atend_do.'>this file</a>';
//    $sys_function_out .= '<br><a href='.$s_output_to_atend_do.'>map file</a>';
    $sys_function_out .= '</head>';
    $sys_function_out .= '</html>';
    GOTO Z950_SKIP_FILE;

Z910_DO_MAP:
    $sys_function_out = "";
    $sys_function_out .= '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
    $sys_function_out .= '<html>';
    $sys_function_out .= '<head>';
//    $sys_function_out .= '<meta HTTP-EQUIV="REFRESH" content="0; '.$s_output_to_atend_do.'">';
    $sys_function_out .= '<br>DO MAP PROCESS This process requires you to open <a href='.$s_output_to_atend_do.'>this file</a>';
//    $sys_function_out .= '<br><a href='.$s_output_to_atend_do.'>map file</a>';
    $sys_function_out .= '</head>';
    $sys_function_out .= '</html>';

    GOTO Z950_SKIP_FILE;
Z950_SKIP_FILE:

     ECHO $sys_function_out;

Z990_EXIT:


?>


<?php

// ***********************************************************************************************************************************
// start functions

function pf_b100_DO_DRAW_HTML($ps_filename,$ps_group,$ps_debug,$ps_details_def,$ps_details_data)
{


//gw20160212    global $class_main;
    $class_main = new clmain();

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - pf_b100_DO_DRAW_HTML";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_filename = ".$ps_filename." ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};

    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;

//    $sys_function_out .= "gwdebug ut_jcl.php load ".$ps_group." from file ".$ps_filename."<br>";

    $sys_function_out .= $class_main->clmain_v100_load_html_screen($ps_filename,$s_details_def,$s_details_data,"no",$ps_group);

    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ";};
    IF ($sys_debug == "YES"){$sys_function_out.=" <!--".$sys_debug_text."-->";};


    return $sys_function_out;

}
//##########################################################################################################################################################################

function pf_b110_DO_DRAWIF_HTML($ps_line_in,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    global $class_main;
//* DrawIF|v1|field|operator|value|map_name|loop|debug|END

//DrawIF|v1|url_MODE|=|WELCOME|my_buddy_main.htm|welcome_map|no|END

    $s_line = $ps_line_in;
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }

    if (strpos(strtoupper( $s_line),"**DODEBUG**") === false )
    {}else
    {
        $sys_debug  = "YES";
        $sys_debug_text = "Inline debug";
        $s_line = str_replace("**dodebug**","",$s_line);
        $s_line = str_replace("**DODEBUG**","",$s_line);
        $s_line = str_replace("**DoDebug**","",$s_line);
    }

    $sys_function_name = "";
    $sys_function_name = "debug - pf_b100_DO_DRAW_HTML";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo "<br>".$sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){echo "<br>".$sys_debug_text.=" ".$sys_function_name." ps_in_line = ".$ps_line_in." processing line = ".$s_line." ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};

A080_START:
    $ar_line_details = explode("|",$s_line);
    if (strtoupper($ar_line_details[0]) != "DRAWIF")
    {
        goto Z900_EXIT;
    }
    if (strtoupper($ar_line_details[1]) != "V1")
    {
        goto A1_DO_V2;
    }

A100_INIT_VARS:
    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;


    $s_field_to_check = $ar_line_details[2];
    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];

// gw 20120108    $s_map_name = $class_main->clmain_set_map_path_n_name("",$ar_line_details[5]);
    $s_map_name = $ar_line_details[5];

    IF (!strpos(strtoupper($s_map_name),strtoupper("%!"))  === false  )
    {
        $s_map_name = STR_REPLACE("#P","|",$s_map_name);
        $s_map_name = STR_REPLACE("#p","|",$s_map_name);
        $s_map_name = $class_main->clmain_v200_load_line( $s_map_name,$s_details_def,$s_details_data,"NO",$ps_sessionno," ut_jcl b110 a100");
    }
    $s_map_name = $class_main->clmain_set_map_path_n_name("",$s_map_name);

    $s_map_group = $ar_line_details[6];
    $s_line_debug = $ar_line_details[7];

B100_START:
    $sys_function_out = "?b110_unknown".$ps_line_in."?";
    $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b1100");
    IF ($sys_debug == "YES"){echo "<br>".$sys_debug_text.=" ".$sys_function_name." s_field_to_check = ".$s_field_to_check." s_field_value = ".$s_field_value;};

C100_DO_V1:
    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_b110");
    if ($s_true == "FALSE")
    {
        z901_dump($sys_debug, $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);
        $sys_function_out = "";
        GOTO Z900_EXIT;
    }



    $sys_function_out = $class_main->clmain_v100_load_html_screen($s_map_name,$s_details_def,$s_details_data,"NO",$s_map_group);

    IF ($sys_debug == "YES"){echo "<br>".$sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ";};
    IF ($sys_debug == "YES"){echo "<br>".$sys_function_out.=" <!--".$sys_debug_text."-->";};


GOTO Z900_EXIT;

A1_DO_V2:

Z900_EXIT:
    return $sys_function_out;

}
//##########################################################################################################################################################################
function moved_to_clmain_pf_b120_DO_ADD_DETAILS_IF($ps_line_in,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    global $class_main;
//* ADD_DETAILSIF|VERSION|FIELD|OPERATOR|VALUE|FIELDNAMES|VALUES|DEBUG|END

    $ar_check_array = array();
    $s_check_count = "";
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - pf_b120_DO_ADD_DETAILS_IF";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_in_line = ".$ps_line_in." ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};

A080_START:
    $ar_line_details = explode("|",$ps_line_in);
    if (strtoupper($ar_line_details[0]) != "ADD_DETAILSIF")
    {
        goto Z900_EXIT;
    }
    if (strtoupper($ar_line_details[1]) != "V1")
    {
        goto A1_DO_V2;
    }

A100_INIT_VARS:
    $s_field_to_check = $ar_line_details[2];
    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];

    $s_line_debug = $ar_line_details[7];

    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;

    IF (strtoupper($s_line_debug) == "YES")
    {
        $sys_debug = "YES";
        IF ($sys_debug == "YES"){echo $sys_function_name." *** IN LINE DEBUG VIEW SOURCE FOR DETAILS<br>";};
        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_in_line = ".$ps_line_in." ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};
    }


B100_START:
    $sys_function_out = "?b120_unknown".$ps_line_in."?";
    $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$ps_sessionno,"ut_jcl b120");

C100_DO_V1:
    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_b120B");
    if ($s_true == "FALSE")
    {
        z901_dump($sys_debug, $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);
        $sys_function_out = "";
        GOTO Z900_EXIT;
    }

        $ar_line_details[5] = STR_REPLACE("#P","|",$ar_line_details[5]);
        $ar_line_details[5] = STR_REPLACE("#p","|",$ar_line_details[5]);
        $ar_line_details[6] = STR_REPLACE("#P","|",$ar_line_details[6]);
        $ar_line_details[6] = STR_REPLACE("#p","|",$ar_line_details[6]);
        $ar_check_array = explode("|",$ar_line_details[5]);
        $s_check_count = count($ar_check_array);
        $ar_check_array = explode("|",$ar_line_details[6]);
        if ($s_check_count <> count($ar_check_array))
        {
            echo "Error in ut_jcl b120 the number of fields does not match - doing line ".$ps_line_in."<br><br>";
            $sys_function_out = $ar_line_details[5]."|^%##%^|"."Error in ut_jcl b120 the number of fields does not match";
        }else{
            $s_out = $ar_line_details[6];
            if (strpos($s_out,"%!_") === false)
            {}else{
//            echo "jcl b120 checking  ".$s_out."<br><br>";
                $s_out = STR_REPLACE("|","|^p|",$s_out);
                $s_out = $class_main->clmain_v200_load_line( $s_out,$s_details_def,$s_details_data,$sys_debug,$ps_sessionno,"ut_jcl b120out");
                $s_out = STR_REPLACE("^p","|",$s_out);
//            echo "jcl b120 AFTER checking  ".$s_out."<br><br>";
            }

            $sys_function_out = $ar_line_details[5]."|^%##%^|".$s_out;
        }


    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ";};
    IF ($sys_debug == "YES"){$sys_function_out.=" <!--".$sys_debug_text."-->";};


GOTO Z900_EXIT;

A1_DO_V2:

Z900_EXIT:
    return $sys_function_out;

}
//##########################################################################################################################################################################
function Pf_b200_do_dataloop($ps_dbcnx,$ps_sql,$ps_filename,$ps_group,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno, $ps_page_current,$ps_page_limit)
{
    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }

    if(strpos($ps_sql,"*DODEBUG_DATALOOP*") <> false)
    {
        $ps_sql = str_replace("*DODEBUG_DATALOOP*","",$ps_sql);
        $sys_debug  = "YES";
        $sys_debug_text = "sql debug:"; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }

    $sys_function_name = "";
    $sys_function_name = "debug - Pf_b200_do_dataloop";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    z901_dump($sys_debug, $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");
    z901_dump($sys_debug, $sys_debug_text."=".$sys_function_name." ps_sql = ".$ps_sql." ps_filename = ".$ps_filename."  ps_group = ".$ps_group."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ");

    // define function specific variables and code
//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";};

//    $sys_function_out = "before globals";
//    return $sys_function_out;
//    EXIT;

//gw20160212    global $class_sql;
    $class_sql = new wp_SqlClient();
//gw20160212    global $class_main;
    $class_main = new clmain();


    $s_sql = "";
    $result = "";
    $s_rec_found = "";
    $s_details_def = "";
    $s_details_data = "";
    $s_dataloop_type = "";
    $s_dataloop_type = "newlist";
    $s_dataloop_action = "";

    $ar_dd = array();

    $s_sql = $ps_sql;
    $s_details_def = $ps_details_def ;
    $s_details_data = $ps_details_data ;

    $s_sql = $class_main->clmain_v200_load_line( $s_sql,$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b200");

//    IF (strpos(strtoupper($s_sql),"|%!") > 0 )
//    {
//    }else{
//        //null
//    }

    $sys_debug2 = "NO";
//    $sys_debug2 = "YES";
    z901_dump($sys_debug, $sys_debug_text."   ".$sys_function_name." s_sql after being loaded []".$s_sql."[]");

    $s_page_current = $ps_page_current; // 20150126 - replaced below
    $s_page_limit = $ps_page_limit;

    if (isset($_SESSION[$ps_sessionno.'udp_page_limit']))
    {
        $s_page_limit = $_SESSION[$ps_sessionno.'udp_page_limit'];
//        die("die cl_proc_jobline e19 page=[]".$s_page_limit."[] session var ".$ps_sessionno.'udp_page_limit');
    }


    if(strpos($s_sql,"***newlist***") <> false)
    {
        $s_sql = str_replace("***newlist***","",$s_sql);
        $s_dataloop_type = "newlist";
    }
    if(strpos($s_sql,"***nextpage***") <> false)
    {
        $s_dataloop_type = "pages";
        $s_dataloop_action = "nextpage";
        $s_sql = str_replace("***nextpage***","",$s_sql);
    }
    if(strpos($s_sql,"***prevpage***") <> false)
    {
        $s_dataloop_type = "pages";
        $s_dataloop_action = "prevpage";
        $s_sql = str_replace("***prevpage***","",$s_sql);
    }
    if(strpos($s_sql,"***firstpage***") <> false)
    {
        $s_dataloop_type = "pages";
        $s_dataloop_action = "firstpage";
        $s_sql = str_replace("***firstpage***","",$s_sql);
    }
    if(strpos($s_sql,"***lastpage***") <> false)
    {
        $s_dataloop_type = "pages";
        $s_dataloop_action = "lastpage";
        $s_sql = str_replace("***lastpage***","",$s_sql);
    }
    $s_sql_start = microtime(true);

    z901_dump($sys_debug, $sys_debug_text."   ".$sys_function_name." s_dataloop_type []".$s_dataloop_type."[]");
A120_NEWLIST:
    IF(    $s_dataloop_type <> "newlist")
    {
     GOTO A129_END;
    }
//gw the totals for the full data set
            $s_last_check_time = microtime(true);
            IF ($sys_debug2 == "YES"){echo "<br>before exec_query gwutjcl b190 - ".(round(microtime(true) - $s_last_check_time,12));};
            $s_last_check_time = microtime(true);
    $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_sql);
            IF ($sys_debug2 == "YES"){echo "<br>after exec_query gwutjcl b190 - ".(round(microtime(true) - $s_last_check_time,15));};
            $s_last_check_time = microtime(true);

    $s_total_rows = mysqli_affected_rows($ps_dbcnx);
            IF ($sys_debug2 == "YES"){echo "<br>after affected rows  gwutjcl b190 - ".(round(microtime(true) - $s_last_check_time,15));};
            $s_last_check_time = microtime(true);

    z901_dump($sys_debug, $sys_debug_text."=".$sys_function_name." a120 s_total_rows = []".$s_total_rows."[]  s_page_limit = []".$s_page_limit."[] ");

    if(strtoupper(trim($s_page_limit)) =="ALL")
    {
        $s_total_pages = "1";
        GOTO A129_END;
    }
    $s_total_pages = ceil($s_total_rows/$s_page_limit);
A129_END:

A130_PAGES:
    IF($s_dataloop_type <> "pages")
    {
        GOTO A139_END;
    }
//gw the totals from the session var
    $s_total_rows = $class_main->clmain_v300_set_variable( "rundatasql_total_rows",$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b200a" );

    IF ($sys_debug2 == "YES"){echo "<br>after affected rows  gwutjcl b190 - ".(round(microtime(true) - $s_last_check_time,15));};
    $s_last_check_time = microtime(true);
    $s_total_pages = $class_main->clmain_v300_set_variable( "rundatasql_total_pages",$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b200b" );
    $s_page_current = $class_main->clmain_v300_set_variable( "rundatasql_page_current",$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b200c" );
    if($s_dataloop_action == "nextpage")
    {
        $s_page_current = $s_page_current + 1;
    };
    if($s_dataloop_action == "prevpage")
    {
        $s_page_current = $s_page_current - 1;
    };
    if($s_dataloop_action == "firstpage")
    {
        $s_page_current = 1;
    };
    if($s_dataloop_action == "lastpage")
    {
        $s_page_current = $s_total_pages;
    };
A139_END:

    $s_next_starts_at_row = $s_page_current * $s_page_limit;

    $s_page_next = $s_page_current + 1;
    if ($s_page_next > $s_total_pages)
    {
        $s_page_next = $s_total_pages;
    }
//gw20150125     $s_next_starts_at_row = $s_page_next * $s_page_limit;

    $s_page_prev = $s_page_current -1;
    if ($s_page_prev < 1)
    {
        $s_page_prev = 1 ;
    }
//    $s_prev_starts_at_row = ($s_page_prev * $s_page_limit) - $s_page_limit;
    $s_prev_starts_at_row = $s_next_starts_at_row  - $s_page_limit;
    if($s_prev_starts_at_row< 0)
    {
        $s_prev_starts_at_row = 0;
    }
    $s_current_starts_at_row =  ($s_page_current -1) * $s_page_limit;

    $s_sql = $class_sql->c_sqlclient_add_select_limit($s_sql,$s_page_current,$s_page_limit);

A800_do_display_sql:
            z901_dump($sys_debug, $sys_debug_text."=".$sys_function_name." a800 s_sql = ".$s_sql." ");
    $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_sql);
    $s_rec_found = "NO";


//20160115gw     ECHO "UTJCL A800 - AFTER SQL";

//20160115gw     if(!$result )
//20160115gw     {
//20160115gw         z901_dump($sys_debug, $sys_debug_text."=".$sys_function_name." a800 no result for s_sql = ".$s_sql." ");
//20160115gw         ECHO "UTJCL A800 - BAD RESULT L";
//20160115gw     }ELSE{
//20160115gw         z901_dump($sys_debug, $sys_debug_text."=".$sys_function_name." a800 GOT A result for s_sql = ".$s_sql." ");
//20160115gw         ECHO "UTJCL A800 - GOOD SQL";
//20160115gw     }

            $S_sql_end =  microtime(true);
            $s_sql_duration = round($S_sql_end - $s_sql_start,5);
            IF ($sys_debug2 == "YES"){echo "<br>sql duration  gwutjcl b190".$s_sql_duration;};
            $s_last_check_time = microtime(true);

    $s_page_def = "rundatasql_upd_affected_rows|rundatasql_duration|rundatasql_total_rows|rundatasql_page_current|rundatasql_current_starts_at_row|rundatasql_page_limit|rundatasql_total_pages|rundatasql_page_next|rundatasql_next_starts_at_row|rundatasql_page_prev|rundatasql_prev_starts_at_row|rundatasql_sql";
    $s_page_data = mysqli_affected_rows($ps_dbcnx)."|".$s_sql_duration." secs|".$s_total_rows."|".$s_page_current."|".$s_current_starts_at_row."|".$s_page_limit."|".$s_total_pages."|".$s_page_next."|".$s_next_starts_at_row."|".$s_page_prev."|".$s_prev_starts_at_row."|".$s_sql;

    $_SESSION['rundata_sql_def'] = $s_page_def;
    $_SESSION['rundata_sql_data'] = $s_page_data;

    $s_next_prev_row = $class_main->clmain_v950_next_prev_row($_SESSION['rundata_sql_def'],"NO","ut_jcl rundata",$ps_details_def,$ps_details_data,$ps_sessionno);

//    die ("gw utjcl b200 s_dataloop_typedddd".$s_dataloop_type);
    $s_details_def = $ps_details_def."|SYS_RD_NEXT_PREV_ROW|".$s_page_def;
    $s_details_data = $ps_details_data."|".$s_next_prev_row."|".$s_page_data;
    $sys_function_out .= $class_main->clmain_v100_load_html_screen($ps_filename,$s_details_def,$s_details_data,"no",$ps_group."_header_(spaceonerror)");

//        echo "gwetst ".$s_page_def."   data = ".$s_page_data;
B100_LOAD_FILE_TO_ARRAY:
//20160115gw     ECHO "UTJCL B100 LAD ARRAR A800 - GOOD SQL";

    $s_filename=$ps_filename;
    $s_filename = $class_main->clmain_set_map_path_n_name("",$s_filename);

    $sys_language =$_SESSION['sys_language'];
    if (strtoupper($sys_language)!="ENGLISH")
    {
        $s_filename2 = substr($s_filename,0,strpos($s_filename,"."))."_".$sys_language.substr($s_filename,strpos($s_filename,"."));
        IF (file_exists($s_filename2))
        {
            $s_filename = $s_filename2;
        }
    }

    //print_r($s_returned_details_data);
    IF ($sys_debug == "YES"){echo $sys_function_name."after clmain_v100_load_html_screen<br>";};

    $s_map_file_exists='Y';
    if(file_exists($s_filename))
    {
        $array_map_lines = file($s_filename);
        $s_filename = $array_map_lines;
    }
    else
    {
        $s_map_file_exists='N';
        $s_map_line =  "<br>clmain_v100_load_html_screenE:utjcl_201  ##### File Not Found-:".$s_filename."<br>";
        z901_dump("ERROR", "WB_UT_JCL.PHPE:utjcl_201  ##### File Not Found-:[]".$s_filename."[]");
        return $s_map_line;
        exit();
    }
B190_END:
//20160115gw     ECHO "<BR>UTJCL B190 - GOOD SQL";
        $s_last_check_time2 = microtime(true);
    $s_dataloop_reccount = 0;
//20160115gw     ECHO "<BR>UTJCL B190-2 - GOOD SQL";

//20160115gw     if(!$result )
//20160115gw     {
//20160115gw         ECHO "<BR>UTJCL B190 - BAD RESULT L";
//20160115gw     }ELSE{
//20160115gw         ECHO "<BR>UTJCL B190 - GOOD SQL";
//20160115gw     }

//20160115gw     ECHO "<BR>UTJCL B190 - RESULT =[]".$result."[]";
//20160115gw     ECHO "<BR>UTJCL B190 - RESULT COUNT =[]".count($result)."[]";
//20160115gw     ECHO "<BR>UTJCL B190 - RESULT ROWS =[]".@mysqli_num_rows( $result )."[]";

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
    {
            IF ($sys_debug2 == "YES"){echo "<br>next record gwutjcl b190 - ".(round(microtime(true) - $s_last_check_time2,15));};
        $s_last_check_time2 = microtime(true);
        $s_rec_found = "YES";
        $s_details_def = $ps_details_def;
        $s_details_data = $ps_details_data;

        $s_dataloop_reccount = $s_dataloop_reccount + 1;

        $_SESSION[$ps_sessionno.'DATALOOP_RECCOUNT'] = $s_dataloop_reccount;

        $s_details_def = $s_details_def."|"."DATALOOP_RECCOUNT";
        $s_details_data = $s_details_data."|".$s_dataloop_reccount;

        $s_table_name = mysqli_fetch_field_direct($result,0)->table;

        $s_temp = $class_main->clmain_u820_fn_Z999_build_def_data($ps_dbcnx,$ps_debug, "ut_jcl_b200dataloop",$row,$s_table_name);
        if (strpos($s_temp,"|^%##%^|") ===false)
        {
        }else{
            $ar_line_details = explode("|^%##%^|",$s_temp);
            $s_details_def = $s_details_def.$ar_line_details[0];
            $s_details_data = $s_details_data.$ar_line_details[1];
            $s_temp = "";
        };

        require_once($_SESSION['ko_prog_path'].'lib/class_process_jobline.php');
        $class_clprocessjobline = new clprocessjobline();


//20170320gw - need to introduce a filter on the details_def
        if( $class_clprocessjobline->z910_check_dd_filter (  $ps_dbcnx,"NODEBUG",$s_details_def,$s_details_data,$ps_sessionno, "utjcl b200"   ) <> "allgood"){
            $s_dataloop_reccount = $s_dataloop_reccount - 1;
//            $s_rec_dd_skipped = "YES";
            goto C900_WHILE_LOOP;
        }





          /*
                foreach( $row as $key=>$value)
                {
                    if (strtoupper($key) == "DATA_BLOB" )
                    {
                        if (strpos(strtoupper($value),"XML") === false)
                        {}else
                        {
                            $ar_xml = $value;
                            $ar_xml = simplexml_load_string($ar_xml);
                //           var_dump($ar_xml);
                            $newArry = array();
                            $ar_xml = (array) $ar_xml;
                            foreach ($ar_xml as $key => $value)
                            {
                                //$s_details_def .= "|REC_".mysql_field_table($result,0)."_DB_".$key;
                                $s_table_name = mysqli_fetch_field_direct($result,0)->table;
                                $s_details_def .= "|REC_".$s_table_name."_DB_".$key;
                                $s_details_data .= "|".$class_main->clmain_u596_xml_decode_char($value);
                            }
                        }
                        continue;
                    }
                    if (strpos(strtoupper($key),"DETAILS_D") === FALSE)
                    {
                        GOTO C120_SKIP_DATA_BLOB;
                    }
                    if (strtoupper($key) == "DETAILS_DEF" )
                    {
        //               $s_details_def .= "|DD_".$key;
                       $ar_dd = explode("|",$value);
                       for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
                       {
                            $s_details_def .= "|DD_gw1".$ar_dd[$i2];
        //20160115GW - added the rec_ prefix to make it clearer, left above for backward compatability
                            //$s_details_def .= "|REC_".mysql_field_table($result,0)."_DD_".$ar_dd[$i2];
                            $s_table_name = mysqli_fetch_field_direct($result,0)->table;
                            $s_details_def .= "|REC_".$s_table_name."_DD_".$ar_dd[$i2];
                        }
                        continue;
                    }
                    if (strtoupper($key) == "DETAILS_DATA" )
                    {
        //               $s_details_data .= "|DD_".$value;
                       $ar_dd = explode("|",$value);
                       for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
                       {
                            $s_details_data .= "|".$ar_dd[$i2];
                            $s_details_data .= "|".$ar_dd[$i2];
                        }
                        continue;
                    }
        C120_SKIP_DATA_BLOB:

        B651_SKIP_DATA_BLOB:
        //            DIE ("GW DIE UT_JCL B200 C120190 ");


        // if the first char is not { cant be json
                    if (substr($value,0,2) <> '{"')
                    {
                        goto B652_SKIP_JSON;
                    }
        // if the last char is not } cant be json
                    if ($value[strlen($value)-1] <> "}")
                    {
                        goto B652_SKIP_JSON;
                    }
                    if ($value[strlen($value)-2] <> "}")
                    {
                        goto B652_SKIP_JSON;
                    }

        //            ECHO "<BR> IS JSON:".$key;
        B652_A_DO_JSON:
                    $dictionary = json_decode($value, TRUE);
                    /*    if ($dictionary === NULL)
                        {
                                goto B652_SKIP_JSON;
                        }
                        echo $dictionary['fieldname'];
                    */
//echo "<br> raw  value";
// print_r($value);

//echo "<br> json dictionary values";
            //print_r(array_keys($dictionary));

//    $dictionary =  clmain_u840_order_json($dictionary);
/*
            $s_json_data_csv_data = "";
            $s_json_data_csv_headings = "";
            foreach ($dictionary as $key1 => $value1)
            {
                $s_fieldname = $key1;
                $s_fieldvalue = " ";
                if (is_array($value1))
                {
                    if (in_array('Value', $value1)) {
                        if (isset($value1['Value']))
                        {
                            $s_fieldvalue = $value1['Value'];
                            GOTO B652_C_MAKE_DEF_DATA;
                        }
//echo "<br> gw found value 1 keys";
//                 print_r(array_keys($value1));
//echo "<br> gw found value 1 values";
//                 print_r(array_values($value1));
                    }
//echo "<br> ";
//                print_r(" there is no value in the json for field s_fieldname=[]".$s_fieldname."[]  value1=[]".$value1."[]");
//echo "<br> value 1 keys";
//                 print_r(array_keys($value1));
//echo "<br> value 1 values";
//                 print_r(array_values($value1));
//echo "<br> array count  value1= []".count($value1)."[] ";
                    foreach ($value1 as $key2 => $value2)
                    {
                        IF (strtoupper($key2) <> "VALUE")
                        {
                            GOTO B652_B_SKIP_VALUE;
                        }
                        if (is_array($value2))
                        {
                            foreach ($value2 as $key3 => $value3)
                            {
                                $s_fieldname = $key1."_".$key3;
                                $s_fieldvalue = $value2[$key3];
                                //$s_details_def .= "|REC_".mysql_field_table($result,0)."_JSON_".$key."_".str_replace(" ","_",trim($s_fieldname));
                                $s_table_name = mysqli_fetch_field_direct($result,0)->table;
                                $s_details_def .= "|REC_".$s_table_name."_JSON_".$key."_".str_replace(" ","_",trim($s_fieldname));
                                $s_details_data .= "|".trim($s_fieldvalue);
                                $s_json_data_csv_data = $s_json_data_csv_data.str_replace(","," ",$s_fieldvalue).",";
                                $s_json_data_csv_headings = $s_json_data_csv_headings.str_replace("_Value","",(str_replace(" ","_",trim($s_fieldname)))).",";
                            }
                            goto B652_X_NEXT_FIELD;
                        }else{
                            $s_fieldname = $key1."_".$key2;
                            $s_fieldvalue = $value1[$key2];
                        }
B652_B_SKIP_VALUE:
//echo "<br> s_fieldname = []".$s_fieldname."[] ";
//echo "<br> s_fieldvalue = []".$s_fieldvalue."[]";
B652_B_NEXT_FIELD:
                    }
                }else{
                    $s_fieldvalue = $value1;
                }
B652_C_MAKE_DEF_DATA:
                if (is_array($s_fieldvalue))
                {
                    $s_fieldvalue = implode("+",$s_fieldvalue);
                }
                //$s_details_def .= "|REC_".mysql_field_table($result,0)."_JSON_".$key."_".str_replace(" ","_",trim($s_fieldname));
                $s_table_name = mysqli_fetch_field_direct($result,0)->table;
                $s_details_def .= "|REC_".$s_table_name."_JSON_".$key."_".str_replace(" ","_",trim($s_fieldname));
                $s_details_data .= "|".trim($s_fieldvalue);
                $s_json_data_csv_data = $s_json_data_csv_data.str_replace(","," ",$s_fieldvalue).",";
                $s_json_data_csv_headings = $s_json_data_csv_headings.str_replace("_Value","",(str_replace(" ","_",trim($s_fieldname)))).",";

//        ECHO "<BR>def".$s_details_def;
//        ECHO "<BR>data".$s_details_data;
//        ECHO "<BR>csvdata".$s_json_data_csv_data;
//        ECHO "<BR>csvhead".$s_json_data_csv_headings;

B652_X_NEXT_FIELD:
            }
//echo "<br> json end def";
//                 print_r($s_details_def);
//echo "<br> json end data";
//                 print_r($s_details_data);
            //$s_details_def .= "|REC_".mysql_field_table($result,0)."_JSON_csv_headings|REC_".mysql_field_table($result,0)."_DB_csv_data";
            $s_table_name = mysqli_fetch_field_direct($result,0)->table;
            $s_details_def .= "|REC_".$s_table_name."_JSON_csv_headings|REC_".$s_table_name."_DB_csv_data";
            $s_details_data .= "|".trim($s_json_data_csv_headings)."|".trim($s_json_data_csv_data);
            continue;

B652_SKIP_JSON:

C190_SKIP_JSON:

//                $s_details_def .= "|REC_".my_sql_field_table($result,0)."_".$key;
            //$s_details_def .= "|REC_".mysql_field_table($result,0)."_".$key;
            $s_table_name = mysqli_fetch_field_direct($result,0)->table;
            $s_details_def .= "|REC_".$s_table_name."_".$key;
            IF (STRPOS($value,"|") === false)
            {
                $s_details_data .= "|".$value;
            }else{
                $s_details_data .= "|".str_replace("|","#*pipe*#",$value);
                $array_data = explode("|",$value);
//                    echo "ut_jcl b200 - there is a pipe in the record data for def |REC_".mysql_field_table($result,0)."_".$key." value=".$value."<br>";
                for ( $i2 = 0; $i2 < count($array_data); $i2++)
                {
                    //$s_details_def .= "|REC_".mysql_field_table($result,0)."_".$key."_".$i2;
                    $s_table_name = mysqli_fetch_field_direct($result,0)->table;
                    $s_details_def .= "|REC_".$s_table_name."_".$key."_".$i2;
                    $s_details_data .= "|".$array_data[$i2];
                }
            }

            continue;

C600_FOR_EACH_END:
        }

        */


C500_CHECK_DEF:

    $array_def = explode("|",$s_details_def);
    $array_data = explode("|",$s_details_data);

        if (count($array_def)<> count($array_data))
        {
            if(isset ($_SESSION['ko_log_clmain_v300_field_count_errors'])===false) {
               echo "clmain Pf_b200_do_dataloop  _ def/data field count error start of list  - def count=".count($array_def)." data count = ".count($array_data)."<br>";
               echo "s_fieldname =".$ps_filename."  <br>";
               echo "s_details_def =".$s_details_def."  <br>";
               echo "s_details_data =".$s_details_data."  <br>";

               $s_count = count($array_def);
               if (count($array_data) > $s_count)
               {
                   $s_count = count($array_data);
               }
               $ar_dd = explode("|",$s_details_def);
               $ar_ddata = explode("|",$s_details_data);
                   for ( $i2 = 0; $i2 < $s_count; $i2++)
                   {
                        echo "s_details_def ".$i2." =".$ar_dd[$i2]."=data =".$ar_ddata[$i2]."  <br>";
                    }

               echo "ut_jcl  Pf_b200_do_dataloop _ def/data field count error end of list <br>";
        }else{
//            echo "<br>process jobline b200 field mismatch " ;
        }
    }ELSE{
//                   echo "clmain Pf_b200_do_dataloop _ def/data ALL GOOD <br>";
    }

C590_END:
            IF ($sys_debug2 == "YES"){echo "<br>before load screen gwutjcl b190 - ".(round(microtime(true) - $s_last_check_time,15));};
        $s_last_check_time = microtime(true);
        $sys_function_out .= $class_main->clmain_v100_load_html_screen($s_filename,$s_details_def,$s_details_data,"no",$ps_group);

            IF ($sys_debug2 == "YES"){echo "<br>after load screen  gwutjcl b190 - ".(round(microtime(true) - $s_last_check_time,15));};
        $s_last_check_time = microtime(true);
C900_WHILE_LOOP:
    }

            IF ($sys_debug2 == "YES"){echo "<br>after c900  gwutjcl b190 - ".(round(microtime(true) - $s_last_check_time,15));};
        $s_last_check_time = microtime(true);

    if ($s_rec_found == "NO")
    {
// gw 20100502         $sys_function_out .= "There is no data for your selection ".$sys_function_name."<br>".$s_sql;
        $sys_function_out .= $class_main->clmain_v100_load_html_screen($ps_filename,$s_details_def,$s_details_data,"no",$ps_group."_nodata");
        if (strpos(strtoupper($sys_function_out),"*ERROR") === false)
        {}else
        {
           $sys_function_out = "There is no data for your selection and the group ".$ps_group."_nodata is not in the map.  ".$sys_function_name."<br>".$s_sql;
        }
    }

    $s_details_def = $ps_details_def."|SYS_RD_NEXT_PREV_ROW|".$s_page_def;
    $s_details_data = $ps_details_data."|".$s_next_prev_row."|".$s_page_data;
    $sys_function_out .= $class_main->clmain_v100_load_html_screen($ps_filename,$s_details_def,$s_details_data,"no",$ps_group."_footer_(spaceonerror)");


    z901_dump($sys_debug, $sys_function_name."  returned  = ".$sys_function_out." ");

            IF ($sys_debug2 == "YES"){echo "<br>end of data loop gwutjcl b190 - ".(round(microtime(true) - $s_last_check_time,15));};
        $s_last_check_time = microtime(true);
    return $sys_function_out;

}
//##########################################################################################################################################################################
    function moved_to_clmain_Pf_b300_set_rundata($ps_dbcnx,$ps_sql,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
    {
        $sys_debug_text = "";
        $sys_debug = "";

        $sys_debug = strtoupper($ps_debug);
        IF ($sys_debug !="NO") {
            $sys_debug  = "YES";
            $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
        }
        $sys_function_name = "";
        $sys_function_name = "debug - Pf_b300_set_rundata";
        $sys_function_out = "";
        IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
        z901_dump($sys_debug, $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");
    //    z901_dump($sys_debug, $sys_debug_text."=".$sys_function_name." ps_sql = ".$ps_sql." ps_filename = ".$ps_filename."  ps_group = ".$ps_group."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ");

        // define function specific variables and code
    //EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";};

    //    $sys_function_out = "before globals";
    //    return $sys_function_out;
    //    EXIT;

        global $class_sql;
        global $class_main;

        $s_sql = "";
        $result = "";
        $s_rec_found = "";
        $s_details_def = "";
        $s_details_data = "";

        $ar_dd = array();

        $s_sql = $ps_sql;
        $s_details_def = $ps_details_def ;
        $s_details_data = $ps_details_data ;

        $s_sql = $class_main->clmain_v200_load_line( $s_sql,$s_details_def,$s_details_data,"no",$ps_sessionno, "ut_jcl b200");

        //    IF (strpos(strtoupper($s_sql),"|%!") > 0 )
    //    {
    //    }else{
    //        //null
    //    }


        z901_dump($sys_debug, $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql." ");
        $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_sql);
        $s_rec_found = "NO";

        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
        {
            $s_rec_found = "YES";
            $s_details_def = $ps_details_def;
            $s_details_data = $ps_details_data;
            foreach( $row as $key=>$value)
            {
                if (strpos(strtoupper($key),"DETAILS_D") === FALSE)
                {
    //                $s_details_def .= "|REC_".my_sql_field_table($result,0)."_".$key;
                    //$s_details_def .= "|REC_".mysql_field_table($result,0)."_".$key;
                    $s_table_name = mysqli_fetch_field_direct($result,0)->table;
                    $s_details_def .= "|REC_".$s_table_name."_".$key;
                    $s_details_data .= "|".$value;
                    continue;
                }
                if (strtoupper($key) == "DETAILS_DEF" )
                {
    //               $s_details_def .= "|DD_".$key;
                   $ar_dd = explode("|",$value);
                   for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
                   {
                        $s_details_def .= "|DD_".$ar_dd[$i2];
                    }
                    continue;
                }
                if (strtoupper($key) == "DETAILS_DATA" )
                {
    //               $s_details_data .= "|DD_".$value;
                   $ar_dd = explode("|",$value);
                   for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
                   {
                        $s_details_data .= "|".$ar_dd[$i2];
                    }
                    continue;
                }
            }
        }
        if ($s_rec_found == "NO")
        {
            $sys_function_out .= "<br>There is no data in ".$sys_function_name."for ".$s_sql;
        }
        if ($s_rec_found == "YES")
        {
            $sys_function_out .= $s_details_def."|^%##%^|".$s_details_data;
        }

        z901_dump($sys_debug, $sys_function_name."  returned  = ".$sys_function_out." ");

        return $sys_function_out;

    }
    //##########################################################################################################################################################################
    // B400, B410 AND B420 are very, very, very similar.
    function Pf_b400_do_multilevelloop($ps_dbcnx,$ps_MultiLevelloop1,$ps_MultiLevelloop2,$ps_MultiLevelloop3,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_calledfrom,$ps_ko_map_path)
    {
    // Update checklist
    // if you want to copy b400 down then after you copy do the following
    // a. remove all reference to multileveloop1
    // b. change all mll1 to mll2
    // c. change all _b400_ to _b410_
    // d. c

        $sys_debug_text = "";
        $sys_debug = "";

        $sys_debug = strtoupper($ps_debug);
        IF ($sys_debug !="NO") {
            $sys_debug  = "YES";
            $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
        }
        $sys_function_name = "";
        $sys_function_name = "debug - Pf_b400_do_multilevelloop called from ".$ps_calledfrom." ";
        $sys_function_out = "";

        $s_skip_error = "";
        $s_skip_error = "NO";

        IF ($sys_debug == "YES"){echo "<br>".$sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
        z901_dump($sys_debug, $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");
        z901_dump($sys_debug,"Parameters= s_MultiLevelloop1,s_MultiLevelloop2,s_MultiLevelloop3,ps_debug,s_details_def,s_details_data,s_sessionno,ps_calledfrom");
        z901_dump($sys_debug,"Values= ".$ps_MultiLevelloop1.",".$ps_MultiLevelloop2.",".$ps_MultiLevelloop3.",".$ps_debug.",".$ps_details_def.",".$ps_details_data.",".$ps_sessionno.",".$ps_calledfrom);
        // define function specific variables and code
        //EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";

        IF ($ps_MultiLevelloop1 == "NONE")
        {
            IF ($ps_MultiLevelloop2 == "NONE")
            {
                IF ($ps_MultiLevelloop3 == "NONE")
                {
                    $sys_function_out .= "E2009 - There is no multilevel loops defined ".$sys_function_name;
                    z901_dump($sys_function_out);
                    GOTO Z900_EXIT;
                }
            }
        }

        IF ($ps_MultiLevelloop1 == "NONE")
        {
            IF ($ps_MultiLevelloop2 <> "NONE")
            {
                    $sys_function_out .= "E2009 - There is a 2nd level multilevel without a first level ".$sys_function_name;
                    z901_dump($sys_function_out);
                    GOTO Z900_EXIT;
            }
        }


        z901_dump($sys_debug,"got past the multilevel loop test");

        global $class_sql;
        global $class_main;


        //DECLARE
        $s_sql = "";
        $result = "";
        $s_rec_found = "";
        $s_details_def = "";
        $s_details_data = "";
        $s_reccount = "";
        $s_numrows = "";
        $s_field_value = "";
        $row = array();
        $ar_dd = array();
        $ar_line_details = array();
        $ar_keys = array_keys($row);
        $ar_values = array_values($row);
        $key = "";
        $value = "";

        $s_mll_preloop_map = "";
        $s_mll_preloop_group = "";
        $s_mll_postloop_map = "";
        $s_mll_postloop_group = "";
        $s_mll_tally1_field = "";
        $s_mll_tally1_value = "";
        $s_mll_tally2_field = "";
        $s_mll_tally2_value = "";
        $s_mll_tally3_field = "";
        $s_mll_tally3_value = "";
        $s_mll_tally4_field = "";
        $s_mll_tally4_value = "";
        $s_mll_tally5_field = "";
        $s_mll_tally5_value = "";

    //INITIALISE
        $s_details_def = $ps_details_def ;
        $s_details_data = $ps_details_data ;


        $ar_line_details = explode("|",$ps_MultiLevelloop1);
        $s_mll_preloop_map = strtoupper(TRIM($ar_line_details[1],""));
        $s_mll_preloop_group = TRIM($ar_line_details[2],"");
        $s_mll_postloop_map = strtoupper(TRIM($ar_line_details[3],""));
        $s_mll_postloop_group = TRIM($ar_line_details[4],"");
        $s_sql = TRIM($ar_line_details[5],"");
        $s_mll_tally1_field = TRIM($ar_line_details[6],"");
        $s_mll_tally2_field = TRIM($ar_line_details[7],"");
        $s_mll_tally3_field = TRIM($ar_line_details[8],"");
        $s_mll_tally4_field = TRIM($ar_line_details[9],"");
        $s_mll_tally5_field = TRIM($ar_line_details[10],"");
        $s_reccount = "0";
        $s_mll_tally1_value = "0";
        $s_mll_tally2_value = "0";
        $s_mll_tally3_value = "0";
        $s_mll_tally4_value = "0";
        $s_mll_tally5_value = "0";

        if (strpos(strtoupper($s_sql),"SKIP_ERROR") === FALSE )
        {    }else
        {
            $s_skip_error = "YES";
            $s_sql = str_replace("SKIP_ERROR"," ",$s_sql);
            $s_sql = str_replace("skip_error"," ",$s_sql);
        }
        if (strtoupper(substr($s_sql,0,9)) =="*NOERROR*")
        {
            $s_skip_error = "YES";
            $s_sql = substr($s_sql,9);
        }


        $s_sql = str_replace("#p","|",$s_sql);
        $s_sql = str_replace("#P","|",$s_sql);
        $s_sql = $class_main->clmain_v200_load_line( $s_sql,$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b400");
        z901_dump($sys_debug, $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql." ");


    C000_RECORD_LOOP:
        $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_sql);
        $s_rec_found = "NO";
        $s_numrows = mysqli_num_rows($result);

C100_DO_RECORDS:
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        IF ($row === false )
        {
            goto C900_END_RECORDS;
        }
        if ($s_numrows==0)
        {
            goto C900_END_RECORDS;
        }
        if (empty($row))
        {
            goto C900_END_RECORDS;
        }

        $s_rec_found = "YES";
        $s_reccount = $s_reccount + 1;
        $s_details_def = $ps_details_def;
        $s_details_data = $ps_details_data;
        z901_dump($sys_debug, $sys_debug_text."=".$sys_function_name."   in while loops = recs found");


        $s_DB_IBD_def="";
        $s_DB_IDB_data="";
        $s_inbounddata_heading = "";

        $i_c150 = 0;
        $ar_keys = array_keys($row);
        $ar_values = array_values($row);
        //$s_table_name = mysql_field_table($result,0);
        $s_table_name = mysqli_fetch_field_direct($result,0)->table;
    //    PRINT_R(mysql_field_table($result,0));
        //    print_r($row);
    C150_BUILD_DATA:
        if ($i_c150 == count($row))
        {
            goto C159_END;
        }
        $key = $ar_keys[$i_c150];
        $value = $ar_values[$i_c150];
    //     z901_dump( "field ".$i_c150." count row=".count($row)." key = ".$key. " value = ".$value." count keys=".count($ar_keys)." count values = ".count($ar_values));
        //    print_r($row);


    //            echo "<br> gw 300setrun<br>";
        if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
        {
            GOTO C152_SKIP_DATA_BLOB;
        }
        $ar_xml = simplexml_load_string($value);
    //           var_dump($ar_xml);
        $newArry = array();
        $ar_xml = (array) $ar_xml;
        foreach ($ar_xml as $key => $value)
        {
            //$s_details_def .= "|REC_".mysql_field_table($result,0)."_DB_".$key;
            $s_table_name = mysqli_fetch_field_direct($result,0)->table;
            $s_details_def .= "|REC_".$s_table_name."_DB_".$key;
            $s_details_data .= "|".$class_main->clmain_u596_xml_decode_char($value);
/*
            if(trim(strtoupper($key)) == trim(strtoupper("inbounddata_heading"))){
                $ar_dd = explode("#PIPE#",$value);
                for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
                {
                    $s_DB_IBD_def .= "|REC_".$s_table_name."_DB_"."IBD_".$ar_dd[$i2];
                }
            }
            if (trim(strtoupper($key)) == trim(strtoupper("inbounddata_line")) )
            {
                $ar_dd = explode("#PIPE#",$value);
                for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
                {
                    $s_DB_IDB_data .= "|".$ar_dd[$i2];
                }
                $s_details_def .= $s_DB_IBD_def;
                $s_details_data .= $s_DB_IDB_data;
            }
*/
            if(trim(strtoupper($key)) == trim(strtoupper("inbounddata_heading"))){
                $s_inbounddata_heading = $value;

            }
            if (trim(strtoupper($key)) == trim(strtoupper("inbounddata_line")) )
            {
                $ar_dd = explode("#PIPE#",$value);
                $ar_temp = explode("#PIPE#",$s_inbounddata_heading);

                for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
                {
                    $s_DB_IDB_data .= "|".$ar_dd[$i2];
                    if(count($ar_dd) == count($ar_temp))
                    {
                        $s_DB_IBD_def .= "|REC_".$s_table_name."_DB_"."IBD_".$ar_dd[$i2];
                    }else{
                        $s_DB_IBD_def .= "|REC_".$s_table_name."_DB_"."IBD_".$i2;
                    }
                }
                $s_details_def .= $s_DB_IBD_def;
                $s_details_data .= $s_DB_IDB_data;
            }

        }
        GOTO C158_NEXT;

    C152_SKIP_DATA_BLOB:
        if (strpos(strtoupper($key),"DETAILS_D") === FALSE)
        {
            $s_details_def .= "|REC_".$s_table_name."_".$key;
            $s_details_data .= "|".$value;
            GOTO C158_NEXT;
        }
        if (strtoupper($key) == "DETAILS_DEF" )
        {
           $ar_dd = explode("|",$value);
           for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
           {
               IF (strtoupper($ar_dd[$i2]) <> "END")
               {
                   $s_details_def .= "|DD_".$s_table_name.$ar_dd[$i2];
               }
            }
            GOTO C158_NEXT;
        }
        if (strtoupper($key) == "DETAILS_DATA" )
        {
           $ar_dd = explode("|",$value);
           for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
           {
               IF (strtoupper($ar_dd[$i2]) <> "END")
               {
                   $s_details_data .= "|".$ar_dd[$i2];
               }
            }
            GOTO C158_NEXT;
        }

    C158_NEXT:
        $i_c150 = $i_c150 + 1;
        GOTO C150_BUILD_DATA;
    C159_END:

        $s_details_def .= "|MLL1_RECCOUNT|MLL1_NUMROWS|";
        $s_details_data .= "|".$s_reccount."|".$s_numrows."|";
    C200_DO_TALLY:
        if (strtoupper($s_mll_tally1_field) != "NONE")
        {
            $s_field_value = $class_main->clmain_v300_set_variable( "|%!_".$s_mll_tally1_field."_!%|",$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b400_T1" );
            $s_mll_tally1_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally1_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"ut_jcl b400_T1");
        }
        if (strtoupper($s_mll_tally2_field) != "NONE")
        {
            $s_field_value = $class_main->clmain_v300_set_variable( "%!_".$s_mll_tally2_field."_!%",$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b400_T2" );
            $s_mll_tally2_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally2_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"ut_jcl b400_T2");
        }
        if (strtoupper($s_mll_tally3_field) != "NONE")
        {
            $s_field_value = $class_main->clmain_v300_set_variable( "%!_".$s_mll_tally3_field."_!%",$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b400 t3" );
            $s_mll_tally3_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally3_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"ut_jcl b400_T3");
        }
        if (strtoupper($s_mll_tally4_field) != "NONE")
        {
            $s_field_value = $class_main->clmain_v300_set_variable( "%!_".$s_mll_tally4_field."_!%",$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b400 t4" );
            $s_mll_tally4_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally4_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"ut_jcl b400_T4");
        }

        if (strtoupper($s_mll_tally5_field) != "NONE")
        {
            $s_field_value = $class_main->clmain_v300_set_variable( "%!_".$s_mll_tally5_field."_!%",$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b400 t5" );
            $s_mll_tally5_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally5_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"ut_jcl b400_T5");
        }

        $s_details_def .= "|MLL1_TALLY1|MLL1_TALLY2|MLL1_TALLY3|MLL1_TALLY4|MLL1_TALLY5|";
        $s_details_data .= "|".$s_mll_tally1_value."|".$s_mll_tally2_value."|".$s_mll_tally3_value."|".$s_mll_tally4_value."|".$s_mll_tally5_value."|";
    C299_END_DO_TALLY:

        if ($s_mll_preloop_map!="NONE"){
            $sys_function_out .= $class_main->clmain_v100_load_html_screen($class_main->clmain_set_map_path_n_name($ps_ko_map_path,$s_mll_preloop_map),$s_details_def,$s_details_data,"no",$s_mll_preloop_group);
        }
    C300_DO_NEXT_LEVEL:
        $sys_function_out .= Pf_b410_do_multilevelloop2($ps_dbcnx,$ps_MultiLevelloop2,$ps_MultiLevelloop3,"NO",$s_details_def,$s_details_data,$ps_sessionno,"ut_jcl B410_mll2",$ps_ko_map_path);
    //at level 2        $sys_function_out .= Pf_b420_do_multilevelloop3($ps_dbcnx,$ps_MultiLevelloop3,"NO",$s_details_def,$s_details_data,$ps_sessionno,"ut_jcl b400_mll2");
    // at level 3  nothing happes

        if ($s_mll_postloop_map!="NONE"){
            $sys_function_out .= $class_main->clmain_v100_load_html_screen($class_main->clmain_set_map_path_n_name($ps_ko_map_path,$s_mll_postloop_map),$s_details_def,$s_details_data,"no",$s_mll_postloop_group);
        }
    C800_NEXT_REC:
        GOTO C100_DO_RECORDS;



    C900_END_RECORDS:
        if ($s_rec_found == "NO")
        {
            if ($s_skip_error == "YES")
            {
                $sys_function_out .= "";
            }else
            {
    //gw20101105 - replace with nodata loop            $sys_function_out .= "<br>There is no data in ".$sys_function_name."for ".$s_sql;
                $sys_function_out .= $class_main->clmain_v100_load_html_screen($s_mll_preloop_map,$s_details_def,$s_details_data,"no",$s_mll_preloop_group."_nodata");
                if (strpos(strtoupper($sys_function_out),"*ERROR") === false)
                {}else
                {
                    $sys_function_out = "There is no data for your selection and the group ".$s_mll_preloop_group."_nodata is not in the map.  ".$s_mll_preloop_map."<br>".$s_sql;
                }
            }
        }

        z901_dump($sys_debug, $sys_function_name."  returned  = ".$sys_function_out." ");
    Z900_EXIT:
        return $sys_function_out;
    }
    //##########################################################################################################################################################################
    function Pf_b410_do_multilevelloop2($ps_dbcnx,$ps_MultiLevelloop2,$ps_MultiLevelloop3,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_calledfrom,$ps_ko_map_path)
    {
    // Update checklist
    // if you want to copy b400 down then after you copy do the following
    // a. remove all reference to multileveloop1
    // b. change all mll1 to mll2
    // c. change all _b400_ to _b410_

        $sys_debug_text = "";
        $sys_debug = "";

        $sys_debug = strtoupper($ps_debug);
        IF ($sys_debug !="NO") {
            $sys_debug  = "YES";
            $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
        }


        if (strpos(strtoupper( $ps_MultiLevelloop2),"**DODEBUG**") === false )
        {}else
        {
            $sys_debug  = "YES";
            $sys_debug_text = "Inline debug";
            $ps_MultiLevelloop2 = str_replace("**dodebug**","",$ps_MultiLevelloop2);
            $ps_MultiLevelloop2 = str_replace("**DODEBUG**","",$ps_MultiLevelloop2);
            $ps_MultiLevelloop2 = str_replace("**DoDebug**","",$ps_MultiLevelloop2);
        }

        $sys_function_name = "";
        $sys_function_name = "debug - Pf_B410_do_multilevelloop called from ".$ps_calledfrom." ";
        $sys_function_out = "";
        IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
        z901_dump($sys_debug, $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");
        z901_dump($sys_debug,"Parameters= s_MultiLevelloop2,s_MultiLevelloop3,ps_debug,s_details_def,s_details_data,s_sessionno,ps_calledfrom");
        z901_dump($sys_debug,"Values= ".$ps_MultiLevelloop2.",".$ps_MultiLevelloop3.",".$ps_debug.",".$ps_details_def.",".$ps_details_data.",".$ps_sessionno.",".$ps_calledfrom);
        // define function specific variables and code
        //EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";

            IF ($ps_MultiLevelloop2 == "NONE")
            {
                IF ($ps_MultiLevelloop3 == "NONE")
                {
                    $sys_function_out .= "E2009 - There is no multilevel loops defined ".$sys_function_name."<br>";
                    z901_dump($sys_function_out);
                    GOTO Z900_EXIT;
                }
            }

        z901_dump($sys_debug,"got past the multilevel loop test");

        global $class_sql;
        global $class_main;
    //DECLARE
        $s_sql = "";
        $result = "";
        $s_rec_found = "";
        $s_details_def = "";
        $s_details_data = "";
        $s_reccount = "";
        $s_numrows = "";
        $s_field_value = "";
        $row = array();
        $ar_dd = array();
        $ar_line_details = array();
        $ar_keys = array_keys($row);
        $ar_values = array_values($row);
        $key = "";
        $value = "";

        $s_mll_preloop_map = "";
        $s_mll_preloop_group = "";
        $s_mll_postloop_map = "";
        $s_mll_postloop_group = "";
        $s_mll_tally1_field = "";
        $s_mll_tally1_value = "";
        $s_mll_tally2_field = "";
        $s_mll_tally2_value = "";
        $s_mll_tally3_field = "";
        $s_mll_tally3_value = "";
        $s_mll_tally4_field = "";
        $s_mll_tally4_value = "";
        $s_mll_tally5_field = "";
        $s_mll_tally5_value = "";

    //INITIALISE
        $s_details_def = $ps_details_def ;
        $s_details_data = $ps_details_data ;

        $ar_line_details = explode("|",$ps_MultiLevelloop2);
        $s_mll_preloop_map = strtoupper(TRIM($ar_line_details[1],""));
        $s_mll_preloop_group = TRIM($ar_line_details[2],"");
        $s_mll_postloop_map = strtoupper(TRIM($ar_line_details[3],""));
        $s_mll_postloop_group = TRIM($ar_line_details[4],"");
        $s_sql = TRIM($ar_line_details[5],"");
        $s_mll_tally1_field = TRIM($ar_line_details[6],"");
        $s_mll_tally2_field = TRIM($ar_line_details[7],"");
        $s_mll_tally3_field = TRIM($ar_line_details[8],"");
        $s_mll_tally4_field = TRIM($ar_line_details[9],"");
        $s_mll_tally5_field = TRIM($ar_line_details[10],"");
        $s_reccount = "0";
        $s_mll_tally1_value = "0";
        $s_mll_tally2_value = "0";
        $s_mll_tally3_value = "0";
        $s_mll_tally4_value = "0";
        $s_mll_tally5_value = "0";

        $s_sql_noerror = "";
        $s_sql_noerror = "NO";
        if (strtoupper(substr($s_sql,0,9)) =="*NOERROR*")
        {
            $s_sql_noerror = "YES";
            $s_sql = substr($s_sql,9);
        }

        $s_sql = str_replace("#p","|",$s_sql);
        $s_sql = str_replace("#P","|",$s_sql);
        $s_sql = $class_main->clmain_v200_load_line( $s_sql,$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b400");

        z901_dump($sys_debug, $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql." ");


    C000_RECORD_LOOP:
        $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_sql);
        $s_rec_found = "NO";
        $s_numrows = mysqli_num_rows($result);

        C100_DO_RECORDS:
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        IF ($row === false )
        {
            goto C900_END_RECORDS;
        }
        if ($s_numrows==0)
        {
            goto C900_END_RECORDS;
        }
        if (empty($row))
        {
            goto C900_END_RECORDS;
        }

        $s_rec_found = "YES";
        $s_reccount = $s_reccount + 1;
        $s_details_def = $ps_details_def;
        $s_details_data = $ps_details_data;
        z901_dump($sys_debug, $sys_debug_text."=".$sys_function_name."   in while loops = recs found");


        $i_c150 = 0;
        $s_DB_IBD_def="";
        $s_DB_IDB_data="";
        $ar_keys = array_keys($row);
        $ar_values = array_values($row);
        //$s_table_name = mysql_field_table($result,0);
        $s_table_name = mysqli_fetch_field_direct($result,0)->table;
    //    PRINT_R(mysql_field_table($result,0));
        //    print_r($row);
    C150_BUILD_DATA:
        if ($i_c150 == count($row))
        {
            goto C159_END;
        }
        $key = $ar_keys[$i_c150];
        $value = $ar_values[$i_c150];
    //     z901_dump( "field ".$i_c150." count row=".count($row)." key = ".$key. " value = ".$value." count keys=".count($ar_keys)." count values = ".count($ar_values));
        //    print_r($row);
    //            echo "<br> gw 300setrun<br>";
        if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
        {
            GOTO C152_SKIP_DATA_BLOB;
        }
        $ar_xml = simplexml_load_string($value);
    //           var_dump($ar_xml);
        $newArry = array();
        $ar_xml = (array) $ar_xml;
        foreach ($ar_xml as $key => $value)
        {
            //$s_details_def .= "|REC_".mysql_field_table($result,0)."_DB_".$key;
            $s_table_name = mysqli_fetch_field_direct($result,0)->table;
            $s_details_def .= "|REC_".$s_table_name."_DB_".$key;
            $s_details_data .= "|".$class_main->clmain_u596_xml_decode_char($value);

            if(trim(strtoupper($key)) == trim(strtoupper("inbounddata_heading"))){
                $ar_dd = explode("#PIPE#",$value);
                for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
                {
                    $s_DB_IBD_def .= "|REC_".$s_table_name."_DB_"."IBD_".$ar_dd[$i2];
                }
            }
            if (trim(strtoupper($key)) == trim(strtoupper("inbounddata_line")) )
            {
                $ar_dd = explode("#PIPE#",$value);
                for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
                {
                    $s_DB_IDB_data .= "|".$ar_dd[$i2];
                }
                $s_details_def .= $s_DB_IBD_def;
                $s_details_data .= $s_DB_IDB_data;
            }


        }
        GOTO C158_NEXT;

    C152_SKIP_DATA_BLOB:
        if (strpos(strtoupper($key),"DETAILS_D") === FALSE)
        {
            z901_dump($sys_debug, $sys_debug_text."=".$sys_function_name."  found a field = "."|REC_".$s_table_name."_".$key." with value ".$value);

            $s_details_def .= "|REC_".$s_table_name."_".$key;
            $s_details_data .= "|".$value;
            GOTO C158_NEXT;
        }
        if (strtoupper($key) == "DETAILS_DEF" )
        {
           $ar_dd = explode("|",$value);
           for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
           {
               IF (strtoupper($ar_dd[$i2]) <> "END")
               {
                   $s_details_def .= "|DD_".$s_table_name.$ar_dd[$i2];
               }
            }
            GOTO C158_NEXT;
        }
        if (strtoupper($key) == "DETAILS_DATA" )
        {
           $ar_dd = explode("|",$value);
           for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
           {
               IF (strtoupper($ar_dd[$i2]) <> "END")
               {
                   $s_details_data .= "|".$ar_dd[$i2];
               }
            }
            GOTO C158_NEXT;
        }

    C158_NEXT:
        $i_c150 = $i_c150 + 1;
        GOTO C150_BUILD_DATA;
    C159_END:

        $s_details_def .= "|MLL2_RECCOUNT|MLL2_NUMROWS|";
        $s_details_data .= "|".$s_reccount."|".$s_numrows."|";
    C200_DO_TALLY:
        if (strtoupper($s_mll_tally1_field) != "NONE")
        {
            $s_field_value = $class_main->clmain_v300_set_variable( "|%!_".$s_mll_tally1_field."_!%|",$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl B410_T1" );
            $s_mll_tally1_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally1_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"ut_jcl B410_T1");
        }
        if (strtoupper($s_mll_tally2_field) != "NONE")
        {
            $s_field_value = $class_main->clmain_v300_set_variable( "%!_".$s_mll_tally2_field."_!%",$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl B410_T2" );
            $s_mll_tally2_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally2_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"ut_jcl B410_T2");
        }
        if (strtoupper($s_mll_tally3_field) != "NONE")
        {
            $s_field_value = $class_main->clmain_v300_set_variable( "%!_".$s_mll_tally3_field."_!%",$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b400 t3" );
            $s_mll_tally3_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally3_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"ut_jcl B410_T3");
        }
        if (strtoupper($s_mll_tally4_field) != "NONE")
        {
            $s_field_value = $class_main->clmain_v300_set_variable( "%!_".$s_mll_tally4_field."_!%",$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b400 t4" );
            $s_mll_tally4_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally4_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"ut_jcl B410_T4");
        }

        if (strtoupper($s_mll_tally5_field) != "NONE")
        {
            $s_field_value = $class_main->clmain_v300_set_variable( "%!_".$s_mll_tally5_field."_!%",$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b400 t5" );
            $s_mll_tally5_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally5_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"ut_jcl B410_T5");
        }

        $s_details_def .= "|MLL2_TALLY1|MLL2_TALLY2|MLL2_TALLY3|MLL2_TALLY4|MLL2_TALLY5|";
        $s_details_data .= "|".$s_mll_tally1_value."|".$s_mll_tally2_value."|".$s_mll_tally3_value."|".$s_mll_tally4_value."|".$s_mll_tally5_value."|";
    C299_END_DO_TALLY:

        if ($s_mll_preloop_map!="NONE"){
            $sys_function_out .= $class_main->clmain_v100_load_html_screen($class_main->clmain_set_map_path_n_name($ps_ko_map_path,$s_mll_preloop_map),$s_details_def,$s_details_data,"no",$s_mll_preloop_group);
        }
    C300_DO_NEXT_LEVEL:
    //at level 1    $sys_function_out .= Pf_b410_do_multilevelloop2($ps_dbcnx,$ps_MultiLevelloop2,$ps_MultiLevelloop3,"NO",$s_details_def,$s_details_data,$ps_sessionno,"ut_jcl B410_mll2");
    //at level 2

    //// gw 20100420 - added the only do if not none
        IF ($ps_MultiLevelloop3 <> "NONE")
        {
            $sys_function_out .= Pf_b420_do_multilevelloop3($ps_dbcnx,$ps_MultiLevelloop3,"NO",$s_details_def,$s_details_data,$ps_sessionno,"ut_jcl b410_mll2",$ps_ko_map_path);
        }
    // gw20100911 - added the eolonly process
        if ($s_mll_postloop_map!="NONE"){
            if ( strpos(strtoupper($s_mll_postloop_group),"EOLONLY") === false)
            {

    //        die("gwdied it loop map = ".$s_mll_postloop_map);
                $sys_function_out .= $class_main->clmain_v100_load_html_screen($class_main->clmain_set_map_path_n_name($ps_ko_map_path,$s_mll_postloop_map),$s_details_def,$s_details_data,"no",$s_mll_postloop_group);
            }
        }
    C800_NEXT_REC:
        GOTO C100_DO_RECORDS;

    C900_END_RECORDS:
    //gw120100911 - this may need to move to just before z900_exit so that the sql checks etc are done first
        if ($s_mll_postloop_map!="NONE"){
            if (!strpos(strtoupper($s_mll_postloop_group),"EOLONLY") === false)
            {
                $s_details_def .= "|MLL2_TALLY1|MLL2_TALLY2|MLL2_TALLY3|MLL2_TALLY4|MLL2_TALLY5|";
                $s_details_data .= "|".$s_mll_tally1_value."|".$s_mll_tally2_value."|".$s_mll_tally3_value."|".$s_mll_tally4_value."|".$s_mll_tally5_value."|";
                $sys_function_out .= $class_main->clmain_v100_load_html_screen($class_main->clmain_set_map_path_n_name($ps_ko_map_path,$s_mll_postloop_map),$s_details_def,$s_details_data,"no",$s_mll_postloop_group);
            }
        }

    /* GW 20100911 - restructured into z810
        if ($s_rec_found == "NO")
        {
            if($s_sql_noerror = "YES")
            {
                $sys_function_out .= "";
            }else{
                $sys_function_out .= "<br>There is no data in ".$sys_function_name."for ".$s_sql;
            }
        }
    */

Z810_EXIT:
    if ($s_rec_found <> "NO")
    {
        GOTO Z900_EXIT;
    }
    if($s_sql_noerror = "YES")
    {
        $sys_function_out .= "";
        GOTO Z900_EXIT;
    }else{
//gw20101105 - replace with nodata loop         $sys_function_out .= "<br>There is no data in ".$sys_function_name."for ".$s_sql;
            $sys_function_out .= $class_main->clmain_v100_load_html_screen($s_mll_preloop_map,$s_details_def,$s_details_data,"no",$s_mll_preloop_group."_nodata");
            if (strpos(strtoupper($sys_function_out),"*ERROR") === false)
            {}else
            {
                $sys_function_out = "There is no data for your selection and the group ".$s_mll_preloop_group."_nodata is not in the map.  ".$s_mll_preloop_map."<br>".$s_sql;
            }
    }
Z900_EXIT:
    z901_dump($sys_debug,"dump of details_def=".$s_details_def);
    z901_dump($sys_debug,"dump of details_data=".$s_details_data);
    z901_dump($sys_debug, $sys_function_name."  returned  = ".$sys_function_out." ");
    return $sys_function_out;
}
//##########################################################################################################################################################################
function Pf_b420_do_multilevelloop3($ps_dbcnx,$ps_MultiLevelloop3,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_calledfrom,$ps_ko_map_path)
{
// Update checklist
// if you want to copy b400 down then after you copy do the following
// a. remove all reference to multileveloop1
// b. change all mll1 to mll3
// c. change all _b400_ to _b420_

    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - Pf_b420_do_multilevelloop called from ".$ps_calledfrom." ";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    z901_dump($sys_debug, $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");
    z901_dump($sys_debug,"Parameters= s_MultiLevelloop3,ps_debug,s_details_def,s_details_data,s_sessionno,ps_calledfrom");
    z901_dump($sys_debug,"Values= ".$ps_MultiLevelloop3.",".$ps_debug.",".$ps_details_def.",".$ps_details_data.",".$ps_sessionno.",".$ps_calledfrom);
    // define function specific variables and code
    //EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";

            IF ($ps_MultiLevelloop3 == "NONE")
            {
                $sys_function_out .= "E2009 - There is no multilevel loops defined ".$sys_function_name;
                z901_dump($sys_function_out);
                GOTO Z900_EXIT;
            }

    z901_dump($sys_debug,"got past the multilevel loop test");

    global $class_sql;
    global $class_main;
//DECLARE
    $s_sql = "";
    $result = "";
    $s_rec_found = "";
    $s_details_def = "";
    $s_details_data = "";
    $s_reccount = "";
    $s_numrows = "";
    $s_field_value = "";
    $row = array();
    $ar_dd = array();
    $ar_line_details = array();
    $ar_keys = array_keys($row);
    $ar_values = array_values($row);
    $key = "";
    $value = "";

    $s_mll_preloop_map = "";
    $s_mll_preloop_group = "";
    $s_mll_postloop_map = "";
    $s_mll_postloop_group = "";
    $s_mll_tally1_field = "";
    $s_mll_tally1_value = "";
    $s_mll_tally2_field = "";
    $s_mll_tally2_value = "";
    $s_mll_tally3_field = "";
    $s_mll_tally3_value = "";
    $s_mll_tally4_field = "";
    $s_mll_tally4_value = "";
    $s_mll_tally5_field = "";
    $s_mll_tally5_value = "";

//INITIALISE
    $s_details_def = $ps_details_def ;
    $s_details_data = $ps_details_data ;

    $ar_line_details = explode("|",$ps_MultiLevelloop3);
    $s_mll_preloop_map = strtoupper(TRIM($ar_line_details[1],""));
    $s_mll_preloop_group = TRIM($ar_line_details[2],"");
    $s_mll_postloop_map = strtoupper(TRIM($ar_line_details[3],""));
    $s_mll_postloop_group = TRIM($ar_line_details[4],"");
    $s_sql = TRIM($ar_line_details[5],"");
    $s_mll_tally1_field = TRIM($ar_line_details[6],"");
    $s_mll_tally2_field = TRIM($ar_line_details[7],"");
    $s_mll_tally3_field = TRIM($ar_line_details[8],"");
    $s_mll_tally4_field = TRIM($ar_line_details[9],"");
    $s_mll_tally5_field = TRIM($ar_line_details[10],"");
    $s_reccount = "0";
    $s_mll_tally1_value = "0";
    $s_mll_tally2_value = "0";
    $s_mll_tally3_value = "0";
    $s_mll_tally4_value = "0";
    $s_mll_tally5_value = "0";


    $s_sql = str_replace("#p","|",$s_sql);
    $s_sql = str_replace("#P","|",$s_sql);
    $s_sql = $class_main->clmain_v200_load_line( $s_sql,$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b400");
    z901_dump($sys_debug, $sys_debug_text."=".$sys_function_name." s_sql = ".$s_sql." ");


C000_RECORD_LOOP:
    $result = $class_sql->c_sqlclient_exec_query($ps_dbcnx,$s_sql);
    $s_rec_found = "NO";
    $s_numrows = mysqli_num_rows($result);

    C100_DO_RECORDS:
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    IF ($row === false )
    {
        goto C900_END_RECORDS;
    }
    if ($s_numrows==0)
    {
        goto C900_END_RECORDS;
    }
    if (empty($row))
    {
        goto C900_END_RECORDS;
    }

    $s_rec_found = "YES";
    $s_reccount = $s_reccount + 1;
    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;
    z901_dump($sys_debug, $sys_debug_text."=".$sys_function_name."   in while loops = recs found");


    $i_c150 = 0;
    $s_DB_IBD_def="";
    $s_DB_IDB_data="";

    $ar_keys = array_keys($row);
    $ar_values = array_values($row);
    //$s_table_name = mysql_field_table($result,0);
    $s_table_name = mysqli_fetch_field_direct($result,0)->table;
    //PRINT_R(mysql_field_table($result,0));
    //    print_r($row);
C150_BUILD_DATA:
    if ($i_c150 == count($row))
    {
        goto C159_END;
    }
    $key = $ar_keys[$i_c150];
    $value = $ar_values[$i_c150];
//     z901_dump( "field ".$i_c150." count row=".count($row)." key = ".$key. " value = ".$value." count keys=".count($ar_keys)." count values = ".count($ar_values));
    //    print_r($row);
    if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
    {
        GOTO C152_SKIP_DATA_BLOB;
    }
    $ar_xml = simplexml_load_string($value);
//           var_dump($ar_xml);
    $newArry = array();
    $ar_xml = (array) $ar_xml;
    foreach ($ar_xml as $key => $value)
    {
        //$s_details_def .= "|REC_".mysql_field_table($result,0)."_DB_".$key;
        $s_table_name = mysqli_fetch_field_direct($result,0)->table;
        $s_details_def .= "|REC_".$s_table_name."_DB_".$key;
        $s_details_data .= "|".$class_main->clmain_u596_xml_decode_char($value);

        if(trim(strtoupper($key)) == trim(strtoupper("inbounddata_heading"))){
            $ar_dd = explode("#PIPE#",$value);
            for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
            {
                $s_DB_IBD_def .= "|REC_".$s_table_name."_DB_"."IBD_".$ar_dd[$i2];
            }
        }
        if (trim(strtoupper($key)) == trim(strtoupper("inbounddata_line")) )
        {
            $ar_dd = explode("#PIPE#",$value);
            for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
            {
                $s_DB_IDB_data .= "|".$ar_dd[$i2];
            }
            $s_details_def .= $s_DB_IBD_def;
            $s_details_data .= $s_DB_IDB_data;
        }

    }
    GOTO C158_NEXT;

C152_SKIP_DATA_BLOB:
    if (strpos(strtoupper($key),"DETAILS_D") === FALSE)
    {
        $s_details_def .= "|REC_".$s_table_name."_".$key;
        $s_details_data .= "|".$value;
        GOTO C158_NEXT;
    }
    if (strtoupper($key) == "DETAILS_DEF" )
    {
       $ar_dd = explode("|",$value);
       for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
       {
           IF (strtoupper($ar_dd[$i2]) <> "END")
           {
               $s_details_def .= "|DD_".$s_table_name.$ar_dd[$i2];
           }
        }
        GOTO C158_NEXT;
    }
    if (strtoupper($key) == "DETAILS_DATA" )
    {
       $ar_dd = explode("|",$value);
       for ( $i2 = 0; $i2 < count($ar_dd); $i2++)
       {
           IF (strtoupper($ar_dd[$i2]) <> "END")
           {
               $s_details_data .= "|".$ar_dd[$i2];
           }
        }
        GOTO C158_NEXT;
    }

C158_NEXT:
    $i_c150 = $i_c150 + 1;
    GOTO C150_BUILD_DATA;
C159_END:

    $s_details_def .= "|mll3_RECCOUNT|mll3_NUMROWS|";
    $s_details_data .= "|".$s_reccount."|".$s_numrows."|";
C200_DO_TALLY:
    if (strtoupper($s_mll_tally1_field) != "NONE")
    {
        $s_field_value = $class_main->clmain_v300_set_variable( "|%!_".$s_mll_tally1_field."_!%|",$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl B410_T1" );
        $s_mll_tally1_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally1_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"ut_jcl B410_T1");
    }
    if (strtoupper($s_mll_tally2_field) != "NONE")
    {
        $s_field_value = $class_main->clmain_v300_set_variable( "%!_".$s_mll_tally2_field."_!%",$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl B410_T2" );
        $s_mll_tally2_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally2_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"ut_jcl B410_T2");
    }
    if (strtoupper($s_mll_tally3_field) != "NONE")
    {
        $s_field_value = $class_main->clmain_v300_set_variable( "%!_".$s_mll_tally3_field."_!%",$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b400 t3" );
        $s_mll_tally3_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally3_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"ut_jcl B410_T3");
    }
    if (strtoupper($s_mll_tally4_field) != "NONE")
    {
        $s_field_value = $class_main->clmain_v300_set_variable( "%!_".$s_mll_tally4_field."_!%",$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b400 t4" );
        $s_mll_tally4_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally4_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"ut_jcl B410_T4");
    }

    if (strtoupper($s_mll_tally5_field) != "NONE")
    {
        $s_field_value = $class_main->clmain_v300_set_variable( "%!_".$s_mll_tally5_field."_!%",$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b400 t5" );
        $s_mll_tally5_value = pf_b490_set_value($ps_dbcnx,$s_mll_tally5_value,$s_field_value,"no",$s_details_def,$s_details_data,$ps_sessionno,"ut_jcl B410_T5");
    }

    $s_details_def .= "|mll3_TALLY1|mll3_TALLY2|mll3_TALLY3|mll3_TALLY4|mll3_TALLY5|";
    $s_details_data .= "|".$s_mll_tally1_value."|".$s_mll_tally2_value."|".$s_mll_tally3_value."|".$s_mll_tally4_value."|".$s_mll_tally5_value."|";
C299_END_DO_TALLY:

    if ($s_mll_preloop_map!="NONE"){
        $sys_function_out .= $class_main->clmain_v100_load_html_screen($class_main->clmain_set_map_path_n_name($ps_ko_map_path,$s_mll_preloop_map),$s_details_def,$s_details_data,"no",$s_mll_preloop_group);
    }
C300_DO_NEXT_LEVEL:
//at level 1    $sys_function_out .= Pf_b420_do_multilevelloop2($ps_dbcnx,$ps_MultiLevelloop2,$ps_MultiLevelloop3,"NO",$s_details_def,$s_details_data,$ps_sessionno,"ut_jcl B410_mll3");
//at level 2    $sys_function_out .= Pf_b420_do_multilevelloop3($ps_dbcnx,$ps_MultiLevelloop3,"NO",$s_details_def,$s_details_data,$ps_sessionno,"ut_jcl b410_mll3");
// at level 3  nothing happes
    if ($s_mll_postloop_map!="NONE"){
        $sys_function_out .= $class_main->clmain_v100_load_html_screen($class_main->clmain_set_map_path_n_name($ps_ko_map_path,$s_mll_postloop_map),$s_details_def,$s_details_data,"no",$s_mll_postloop_group);
    }
C800_NEXT_REC:
    GOTO C100_DO_RECORDS;



C900_END_RECORDS:
    if ($s_rec_found == "NO")
    {
//gw20101105 - replace with nodata loop         $sys_function_out .= "<br>There is no data in ".$sys_function_name."for ".$s_sql;
        $sys_function_out .= $class_main->clmain_v100_load_html_screen($s_mll_preloop_map,$s_details_def,$s_details_data,"no",$s_mll_preloop_group."_nodata");
        if (strpos(strtoupper($sys_function_out),"*ERROR") === false)
        {}else
        {
           $sys_function_out = "There is no data for your selection and the group ".$s_mll_preloop_group."_nodata is not in the map.  ".$s_mll_preloop_map."<br>".$s_sql;
        }
    }

    z901_dump($sys_debug, $sys_function_name."  returned  = ".$sys_function_out." ");
Z900_EXIT:
    return $sys_function_out;
}
//##########################################################################################################################################################################
function pf_b490_set_value($ps_dbcnx,$ps_mll_tally_value,$ps_field_value,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_calledfrom)
{
//A100_TEMPLATE-INIT
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - pf_b490_set_value  called from ".$ps_calledfrom;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_mll_tally_value = ".$ps_mll_tally_value." ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};
//A199_END_TEMPLATE_INIT:

//B100_ define function specific variables and codE

    if (trim($ps_field_value,"") == "")
    {
        $ps_field_value =  "0";
    }
    if (!is_numeric($ps_field_value))
    {
       if (strpos($ps_field_value,"%!_") === false)
       {
           $ps_field_value =  "0";
       }
       $sys_function_out =  $ps_field_value;
    }else{
        $sys_function_out = $ps_mll_tally_value + $ps_field_value;
    }

//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";};

//X900_EXIT:
    return $sys_function_out;
}
//##########################################################################################################################################################################
/*
function moved_to_clmain_Pf_b500_do_url_value($ps_dbcnx,$ps_line,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_url_siteparams)
{

 *
     $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - Pf_b500_do_url_value";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    z901_dump($sys_debug, $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");
    z901_dump($sys_debug, $sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ");

//    echo "start doing urlvalue<br>".$sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ";

    // define function specific variables and code
//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";

//    $sys_function_out = "before globals";
//    return $sys_function_out;
//    EXIT;

    global $class_sql;
    global $class_main;
A001_DEFINE_VARS:
    $ar_line_details = array();
    $s_field_name = "";
    $s_param_name = "";
    $sys_function_out = "";
    $ar_url_siteparams = array();
    $ar_url_siteparams_vars = array();

A100_INIT_VARS:
    $ar_line_details = explode("|",$ps_line);
    $s_field_name = $ar_line_details[2];
    $s_param_name = $ar_line_details[3];
    $sys_function_out = "?b500_".$ar_line_details[2];
    $ar_url_siteparams = explode("^",$ps_url_siteparams);

    if (strtoupper($ar_line_details[0]) != "URLVALUE")
    {
        goto Z900_EXIT;
    }
    if (strtoupper($ar_line_details[1]) != "V1")
    {
        goto A1_DO_V2;
    }
B001_DO_V1:
    $i = 0;

B100_ENTRY:
    IF ($i >= count($ar_url_siteparams))
    {
        goto B900_END;
    }
//    echo "doing param loop param value =".$ar_url_siteparams[$i]."<br>";

    if (strpos($ar_url_siteparams[$i],"=") === false)
    {
//        echo "doing param loop param no equals sign<br>";
        goto B200_GET_NEXT;
    }
    $ar_url_siteparams_vars = explode("=",$ar_url_siteparams[$i]);
//    echo "doing param loop check value in =".$ar_url_siteparams[$i]." explode url param 0=".strtoupper(trim($ar_url_siteparams_vars[0]," "))." 1 = ".strtoupper(trim($ar_url_siteparams_vars[1]))."<br>";
//    echo "doing check values 0=".(strtoupper(trim($ar_url_siteparams_vars[0]," "))."  param =".strtoupper(trim($s_param_name," ")))."<br>";
    $s_value_chk =strtoupper(trim($ar_url_siteparams_vars[0]," "));
    $s_param_name_chk = strtoupper(trim($s_param_name," "));
//    echo "doing check values2 0=*".$s_value_chk."*  param =*".$s_param_name_chk."*<br>";
    if ($s_value_chk == $s_param_name_chk)
    {
//        echo "compare worked param 0=".strtoupper(trim($ar_url_siteparams_vars[0]," "))." 1 = ".strtoupper(trim($ar_url_siteparams_vars[1]))."<br>";
        $sys_function_out = $ar_url_siteparams_vars[1];
        goto Z900_EXIT;
    }

B200_GET_NEXT:
    $i = $i + 1;
    goto B100_ENTRY;

B900_END:
//    echo "doing urlvalue<br>s_field_name=".$s_field_name."<br>s_param_name=".$s_param_name."<br>sys_function_out=".$sys_function_out."<br>";
A1_DO_V2:

Z900_EXIT:
    $sys_function_out = $s_field_name."|^%##%^|".$sys_function_out;

    z901_dump($sys_debug, $sys_function_name."  returned  = ".$sys_function_out." ");

    return $sys_function_out;
}
*/

//##########################################################################################################################################################################
/*
function Pf_b600_do_app_class($ps_dbcnx,$ps_line,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - Pf_b600_do_app_class";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    z901_dump($sys_debug, $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");
    z901_dump($sys_debug, $sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ");

//    echo "start doing urlvalue<br>".$sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ";

    // define function specific variables and code
//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";};

//    $sys_function_out = "before globals";
//    return $sys_function_out;
//    EXIT;

    global $class_sql;
    global $class_main;
    global $class_apps;

A001_DEFINE_VARS:
    $ar_line_details = array();
    $s_action = "";
    $s_function = "";
    $s_function_param = "";
    $s_params = "";
    $s_field_name = "";

A100_INIT_VARS:
    $ar_line_details = explode("|",$ps_line);
    $s_field_name = $ar_line_details[2];
    $s_function_param = $ar_line_details[3];
    $sys_function_out = "?b600_unknown".$ps_line."?";
    $s_line_debug = $ar_line_details[4];

    $s_function = substr($s_function_param,0,strpos($s_function_param,"("));
    $s_params = substr($s_function_param,strpos($s_function_param,"("));
    $s_params = STR_REPLACE("#P","|",$s_params);
    $s_params = STR_REPLACE("#p","|",$s_params);
    $s_params = STR_REPLACE(",","^PARAM^",$s_params);
    $s_params = STR_REPLACE("(","",$s_params);
    $s_params = STR_REPLACE(")","",$s_params);
    $s_params = STR_REPLACE('#"',"^^#^",$s_params);
    $s_params = STR_REPLACE('"',"",$s_params);
    $s_params = STR_REPLACE("^^#^",'"',$s_params);
    $s_params = $class_main->clmain_v200_load_line( $s_params,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"ut_jcl b600a");
    $s_action = "RUN_FUNCTION";
    if (strtoupper(trim($s_field_name," ")) ==  strtoupper(trim("sys_app_function_list"," ")))
    {
        $s_action = "LIST_FUNCTIONS";
    }

    if (strtoupper(trim($s_field_name," ")) ==  strtoupper(trim("sys_app_function_help"," ")))
    {
        $s_action = "FUNCTIONS_HELP";
    }
//    echo "<br>s_function=".$s_function;
//    echo "<br>s_params=".$s_params;

B100_START:
    if (strtoupper($ar_line_details[0]) != "APP_CLASS")
    {
        goto Z900_EXIT;
    }
    if (strtoupper($ar_line_details[1]) != "V1")
    {
        goto A1_DO_V2;
    }
C100_DO_V1:
    $sys_function_out = $class_apps->clapp_a100_do_app_class($s_function,$s_params,$s_action,$ps_dbcnx,$s_line_debug,"ut_jcl b600b",$ps_details_def,$ps_details_data,$ps_sessionno);

//        echo "<br>utjcl ssss sys_function_out = +".$sys_function_out."+ <br>";

    if (strpos($sys_function_out,"|^%##%^|") ===false)
    {

    echo "<br><br> utjcl app_class sys_function_out=".$sys_function_out."<br>";
    die ("<br>utjcl<br>");
        $sys_function_out = $class_main->clmain_v200_load_line($sys_function_out,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"ut_jcl b600c100");
    }else{
        $ar_line_details = explode("|^%##%^|",$sys_function_out);
        $s_field_name = $ar_line_details[0];
        $sys_function_out = $ar_line_details[1];

    echo "<br><br> eeeutjcl app_class sys_function_out=".$sys_function_out."<br>";
    die ("<br>utjcl<br>");

    };



//    echo "doing urlvalue<br>s_field_name=".$s_field_name."<br>s_param_name=".$s_param_name."<br>sys_function_out=".$sys_function_out."<br>";
A1_DO_V2:

Z900_EXIT:
    $sys_function_out = $s_field_name."|^%##%^|".$sys_function_out;
//    $sys_function_out = $s_field_name."|^%##%^|this is working".$sys_function_out;

    z901_dump($sys_debug, $sys_function_name."  returned  = ".$sys_function_out." ");

 //       echo "<br>utjcl z900 a sys_function_out = +".$sys_function_out."+ <br>";
    return $sys_function_out;

}
*/
//##########################################################################################################################################################################
function moved_to_clmain_Pf_b650_do_sys_class($ps_dbcnx,$ps_line,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
/*
    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - Pf_b650_do_sys_class";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    z901_dump($sys_debug, $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");
    z901_dump($sys_debug, $sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ");

//    echo "start doing urlvalue<br>".$sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ";

    // define function specific variables and code
//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";};

//    $sys_function_out = "before globals";
//    return $sys_function_out;
//    EXIT;

    global $class_sql;
    global $class_main;
    global $class_apps;

A001_DEFINE_VARS:
    $ar_line_details = array();
    $s_action = "";
    $s_function = "";
    $s_function_param = "";
    $s_params = "";
    $s_field_name = "";

A100_INIT_VARS:
    $ar_line_details = explode("|",$ps_line);
    $s_field_name = $ar_line_details[2];
    $s_function_param = $ar_line_details[3];
    $sys_function_out = "?b650_unknown".$ps_line."?";
    $s_line_debug = $ar_line_details[4];

    $s_function = substr($s_function_param,0,strpos($s_function_param,"("));
    $s_params = substr($s_function_param,strpos($s_function_param,"("));
    $s_params = STR_REPLACE("#P","|",$s_params);
    $s_params = STR_REPLACE("#p","|",$s_params);
    $s_params = STR_REPLACE(",","^",$s_params);
    $s_params = STR_REPLACE("(","",$s_params);
    $s_params = STR_REPLACE(")","",$s_params);
    $s_params = STR_REPLACE('"',"",$s_params);
    $s_params = $class_main->clmain_v200_load_line( $s_params,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"ut_jcl b600a");
    $s_action = "RUN_FUNCTION";
    if (strtoupper(trim($s_field_name," ")) ==  strtoupper(trim("sys_app_function_list"," ")))
    {
        $s_action = "LIST_FUNCTIONS";
    }

    if (strtoupper(trim($s_field_name," ")) ==  strtoupper(trim("sys_app_function_help"," ")))
    {
        $s_action = "FUNCTIONS_HELP";
    }
//    echo "<br>s_function=".$s_function;
//    echo "<br>s_params=".$s_params;

B100_START:
    if (strtoupper($ar_line_details[0]) != "SYS_CLASS")
    {
        goto Z900_EXIT;
    }
    if (strtoupper($ar_line_details[1]) != "V1")
    {
        goto A1_DO_V2;
    }
C100_DO_V1:
    $sys_function_out = $class_main->clmain_a100_do_sys_class($s_function,$s_params,$s_action,$ps_dbcnx,$s_line_debug,"ut_jcl b650b",$ps_details_def,$ps_details_data,$ps_sessionno);
    $sys_function_out = $class_main->clmain_v200_load_line($sys_function_out,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"ut_jcl b650c100");

//    echo "doing urlvalue<br>s_field_name=".$s_field_name."<br>s_param_name=".$s_param_name."<br>sys_function_out=".$sys_function_out."<br>";
A1_DO_V2:

Z900_EXIT:
    $sys_function_out = $s_field_name."|^%##%^|".$sys_function_out;
//    $sys_function_out = $s_field_name."|^%##%^|this is working".$sys_function_out;

    z901_dump($sys_debug, $sys_function_name."  returned  = ".$sys_function_out." ");

    return $sys_function_out;
*/
}
//##########################################################################################################################################################################
//sys_classif|v1|url_MODE|=|NEWREC|NULL|clmain_v940_initialise_error_sessionvs(#p%!_SESSION_SUPER_validate_error_html_!%#p,NO,sysset)|NO|END
function Pf_b655_do_sys_classif($ps_dbcnx,$ps_line,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{

    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - Pf_b655_do_sys_classif";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    z901_dump($sys_debug, $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");
    z901_dump($sys_debug, $sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ");


    global $class_sql;
    global $class_main;
    global $class_apps;

A001_DO_IF:
    $ar_line_details = array();
    $s_action = "";
    $s_function = "";
    $s_function_param = "";
    $s_params = "";
    $s_field_name = "";

    $ar_line_details = explode("|",$ps_line);
    if (strtoupper($ar_line_details[0]) != "SYS_CLASSIF")
    {
        goto Z900_EXIT;
    }
    if (strtoupper($ar_line_details[1]) != "V1")
    {
        goto A1_DO_V2;
    }

    if (count($ar_line_details) <> 9 )
    {
        echo "<br> ****  ut_jcl - sys_classif *** the number of fields is not correct";
        echo "<br> ****  ".count($ar_line_details)." instead of the expected 9 <br>";

    }

    $s_field_to_check = $ar_line_details[2];
    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];

    $s_field_name = $ar_line_details[5];
    $s_function_param = $ar_line_details[6];
    $s_line_debug = $ar_line_details[7];

    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;

B100_START:
    $sys_function_out = "?b110_unknown".$ps_line."?";
    $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_jcl b655");

C100_DO_V1:
    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_b655a");
    if ($s_true == "FALSE")
    {
        z901_dump($sys_debug, $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);
        $sys_function_out = "";
        $s_field_name = $s_field_name."_if_".$s_compare_value."_failed";
        GOTO Z900_EXIT;
    }



A100_INIT_VARS:
    $sys_function_out = "?b650_unknown".$ps_line."?";

    $s_function = substr($s_function_param,0,strpos($s_function_param,"("));
    $s_params = substr($s_function_param,strpos($s_function_param,"("));
    $s_params = STR_REPLACE("#P","|",$s_params);
    $s_params = STR_REPLACE("#p","|",$s_params);
    $s_params = STR_REPLACE(",","^",$s_params);
    $s_params = STR_REPLACE("(","",$s_params);
    $s_params = STR_REPLACE(")","",$s_params);
    $s_params = STR_REPLACE('"',"",$s_params);

    $s_params = $class_main->clmain_v200_load_line( $s_params,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"ut_jcl b655b ");
    $s_action = "RUN_FUNCTION";
    if (strtoupper(trim($s_field_name," ")) ==  strtoupper(trim("sys_app_function_list"," ")))
    {
        $s_action = "LIST_FUNCTIONS";
    }

    if (strtoupper(trim($s_field_name," ")) ==  strtoupper(trim("sys_app_function_help"," ")))
    {
        $s_action = "FUNCTIONS_HELP";
    }

C100_DO_V1a:
    $sys_function_out = $class_main->clmain_a100_do_sys_class($s_function,$s_params,$s_action,$ps_dbcnx,$s_line_debug,"ut_jcl b655c ",$ps_details_def,$ps_details_data,$ps_sessionno);
    $sys_function_out = $class_main->clmain_v200_load_line($sys_function_out,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"ut_jcl b655d ");

//    echo "doing urlvalue<br>s_field_name=".$s_field_name."<br>s_param_name=".$s_param_name."<br>sys_function_out=".$sys_function_out."<br>";
A1_DO_V2:

Z900_EXIT:
    $sys_function_out = $s_field_name."|^%##%^|".$sys_function_out;
//    $sys_function_out = $s_field_name."|^%##%^|this is working".$sys_function_out;

    z901_dump($sys_debug, $sys_function_name."  returned  = ".$sys_function_out." ");

    return $sys_function_out;

}

function pf_set_sessionid($ps_loginname)
{
    $sys_debug = '';
    $sys_prog_name = "logon1";

    $loginname = "tlm_adm1";
    $password = "tlm_adm1";
    $s_sessionno = "";
    $action = "";
    $s_params = "";
    $s_sys_language = "";
    $s_action_type = "";
    $s_ret_map = "";
    $s_error_map = "";
    $s_ok_map = "";

// A100_INITIALISE_VARIABLES
    $action = "ERROR";
    $s_ret_map = "ut_logon_screen.html";
    $s_error_map = "ut_logon_error_snippet";
    $s_ok_map = "udp_p1.htm";
    $s_user_status_or_id = "";

//A200_LOAD_LIBRARIES
    $sys_debug = strtoupper("NO");
//    $sys_debug = strtoupper("yes");

    IF ($sys_debug == "YES") {
        echo $sys_prog_name . " started debug=" . $sys_debug . "<br>";
    };
    require_once('/tdx/web_prog/dev/lib/class_sql.php');
    $class_sql = new wp_SqlClient();
    IF ($sys_debug == "YES") {
        echo $sys_prog_name . " after class_sql<br>";
    };
    require_once('/tdx/web_prog/dev/lib/class_main.php');
    $class_main = new clmain();
    IF ($sys_debug == "YES") {
        echo $sys_prog_name . " after class_main <br>";
    };
    require_once('/tdx/web_prog/dev/lib/class_myplib1.php5');
    $class_myplib1 = new myplib1();
    IF ($sys_debug == "YES") {
        echo $sys_prog_name . " after class_myplib1<br>";
    };
    IF ($sys_debug == "YES") {
        echo "_get=";
        print_r($_GET);
        echo "<br>";
    };
    IF ($sys_debug == "YES") {
        echo "_post=";
        print_r($_POST);
        echo "<br>";
    };

//A300_CONNECT_TODBASE
    //$dbcnx = $class_sql->c_sqlclient_connect();

//A400_GET_PARAMETERS
    if (isset($_POST['ln'])) $loginname = $_POST["ln"];
    if (isset($_POST['pw'])) $password = $_POST["pw"];
    if (isset($_POST['params'])) $s_params = $_POST["params"];
    if (isset($_POST['sys_language'])) $s_sys_language = $_POST["sys_language"];
    if (isset($_POST['action_type'])) $s_action_type = $_POST["action_type"];
    if (isset($_POST['ret_map'])) $s_ret_map = $_POST["ret_map"];
    if (isset($_POST['error_map'])) $s_error_map = $_POST["error_map"];
    if (isset($_POST['ok_map'])) $s_ok_map = $_POST["ok_map"];

    IF ($sys_debug == "YES") {
        print_r($sys_prog_name . "  " . $_POST);
    };


//A500_SET_VALUES
// if no values in params yet it will only contain ^
//    if (strlen($s_params) < 2 ){
// if the st char is a ^ then no session id has been set

    if (strpos($s_params, "^") == 0) {
        IF ($sys_debug == "YES") {
            print_r($sys_prog_name . "getting new session id");
        };
        $s_sessionno = pf_set_sessionid($loginname);
    } else {
        $s_sessionno = substr($s_params, 0, strpos($s_params, "^"));
    }
    IF ($sys_debug == "YES") {
        print_r($sys_prog_name . "session id = " . $s_sessionno);
    };


    $username = $loginname;
    $s_error_message = "noerror";
    if (TRIM($password) == "") {
        $s_error_message = "INSERTCODE_" . $s_error_map . "_E2003";
    }
    if (TRIM($username) == "") {
        $s_error_message = "INSERTCODE_" . $s_error_map . "_E2002";
    }

    if ($s_action_type == "SETLANG") {
        $s_error_message = "INSERTCODE_" . $s_error_map . "_E2004";
        setcookie("ud_sys_language", $s_sys_language, time() + 60 * 60 * 24 * 30);
    }


//A600_CHECK_USERNAME
    if ($s_error_message <> "noerror") {
        goto A700_SET_SESSION_VALUES;
    }

    setcookie('ud_logonname', $username, time() + 60 * 60 * 24 * 30);
    $s_user_status_or_id = $class_main->clmain_B200_check_user($dbcnx, $username, $password, "NO", $s_sessionno);
    IF ($sys_debug == "YES") {
        echo "<br>USER STATUS = " . $s_user_status_or_id . "<br>";
    };

//     if (strpos(strtoupper($s_user_status_or_id), "ERROR" ) > 0)
    if (strpos(strtoupper($s_user_status_or_id), "*ERROR") === false) {
        GOTO A610_USER_GOOD;
    }

    A605_CHECK_UTL_SYSTEM_USER:
    $s_user_status_or_id = $class_main->clmain_B214_get_utl_pref_user($dbcnx, $username, $password, "NO", $s_sessionno);
    IF ($sys_debug == "YES") {
        echo "<br>USER STATUS = " . $s_user_status_or_id . "<br>";
    };
    if (strpos(strtoupper($s_user_status_or_id), "*ERROR") === false) {
        GOTO A610_USER_GOOD;
    }

    A605_CHECK_DMA:
//  check the users table instead of the user table
// if exists goto a610_user_good
    $s_sql2 = "SELECT * FROM user WHERE id='" . $username . "'(SKIPERRORFAIL)";
    $result2 = $class_sql->c_sqlclient_exec_query($dbcnx, $s_sql2);
    if ($result2 === false) {
        GOTO A620_USER_ERROR;
    }
    if ($row2 = mysqli_fetch_array($result2, MYSQLI_ASSOC)) {
        $s_user_status_or_id = "Possible password error (dma)";
        if ($row2['Password'] == $password) {
            $s_user_status_or_id = "DMA";
            GOTO A610_USER_GOOD;
        }
    }

    GOTO A620_USER_ERROR;

    A610_USER_GOOD:
    IF ($sys_debug == "YES") {
        echo "<br>USER GOOD STATUS = " . $s_user_status_or_id . "<br>";
    };
    setcookie('ud_logonname', $username, time() + 60 * 60 * 24 * 30);
    $action = "MAINPAGE";
    IF ($sys_debug == "YES") {
        echo "BEFORE PREFERENCES <br>";
    };
    $user_preferences = pf_set_user_preferences($dbcnx, $s_sessionno, $s_user_status_or_id);
    IF ($sys_debug == "YES") {
        echo "<br>AFTER PREFERENCES user_preferences=" . $user_preferences . "<br>";
    };

//echo "<br>".print_r(str_replace("<","openbracket^",$user_preferences));
//ECHO "<BR>utmenu user_preferences end ";


    if (strpos(strtoupper($user_preferences), "*ERROR") !== false) {
        IF ($sys_debug == "YES") {
            echo "<br>doing preference error <br>";
        };
        GOTO A700_SET_SESSION_VALUES;
    }
//     $s_ok_map = s
// user status will return the xml datablob
    $xml = new SimpleXMLElement($user_preferences);


//echo "<br>".print_r(str_replace("<","openbracket^",$xml));
//ECHO "<BR>utmenu setpref end ";

//ECHO "<BR>ut_logon user perferences start user_preference  ";
//echo "<br>".print_r($_SESSION);
//ECHO "<BR>ut_logon user perferencesend user_preference  ";


    IF ($sys_debug == "YES") {
        echo "<br>AFTER SIMPLE XML<br>";
    };
    if (isset($xml->logon_initpage[0])) {
        $s_ok_map = $xml->logon_initpage[0];
//echo "<br>".print_r(str_replace("<","openbracket^",$s_ok_map));
//ECHO "<BR>utmenu $s_ok_map end ";

    }
    IF ($sys_debug == "YES") {
        "<br>s_ok_map = " . $s_ok_map . "";
    };

    GOTO A700_SET_SESSION_VALUES;

    A620_USER_ERROR:
    $s_error_message = TRIM($s_user_status_or_id);
    $s_error_id = str_replace("*", "", $s_user_status_or_id);
    $s_error_id = str_replace("ERROR ", "", $s_error_id);
    $s_error_message = $s_error_id;
    IF (strtoUpper(TRIM($s_error_id)) == "E2001_UNKNOWN_USER") {
        $s_error_message = "INSERTCODE_" . $s_error_map . "_E2001";//, "UNKNOWN_USER");
    }
    IF (strtoUpper(TRIM($s_error_id)) == "E2006_WRONG_PWD") {
        $s_error_message = "INSERTCODE_" . $s_error_map . "_E2006";//, "UNKNOWN_USER");
    }
    GOTO A700_SET_SESSION_VALUES;

    A700_SET_SESSION_VALUES:
    IF ($sys_debug == "YES") {
        echo "<br>A700_SET_SESSION_VALUES s_user_status_or_id=[]" . $s_user_status_or_id . "[]<br>";
    };
    $s_siteparams = "" . $s_sessionno . "^";
    $_SESSION[$s_sessionno . 'username'] = $username;
    $s_siteparams = "" . $s_sessionno . "^" . "logonid" . "^";

    if ($s_user_status_or_id <> "DMA") {
        IF ($sys_debug == "YES") {
            echo "<br>A700_SET_SESSION_VALUES skip the dma settings s_user_status_or_id=[]" . $s_user_status_or_id . "[]<br>";
        };
        goto A750_SKIP_DMA;
    };
    $_SESSION[$s_sessionno . 'dma_logon_name'] = $row2['Name'];
    $_SESSION[$s_sessionno . 'dma_logon_company'] = $row2['Company'];
    $_SESSION[$s_sessionno . 'dma_logon_email'] = $row2['Email'];
    $_SESSION[$s_sessionno . 'dma_logon_accounts_access'] = $row2['Accounts_access'];
    $_SESSION[$s_sessionno . 'dma_logon_role'] = $row2['role'];
    $_SESSION[$s_sessionno . 'dma_logon_transport_company'] = $row2['transport_company'];

    $_SESSION[$s_sessionno . 'dma_logon_company_id_sql'] = "and company_id in ('" . $row2['Accounts_access'] . "')";
    $_SESSION[$s_sessionno . 'dma_logon_company_id_sql'] = str_replace(",", "','", $_SESSION[$s_sessionno . 'dma_logon_company_id_sql']);
    if (strtoupper(trim($row2['Accounts_access'])) == "ALL") {
        $_SESSION[$s_sessionno . 'dma_logon_company_id_sql'] = " ";
        GOTO A710_COMPANY_ID_DONE;
    }
    if (strpos(trim($row2['Accounts_access']), "%") > 0) {
        $_SESSION[$s_sessionno . 'dma_logon_company_id_sql'] = "and company_id like '" . $row2['Accounts_access'] . "'";
        GOTO A710_COMPANY_ID_DONE;
    }

    $s_logon_company_id_sql = "and ('" . $row2['Accounts_access'] . "')";
    $s_logon_company_id_sql = str_replace(",", "','", $s_logon_company_id_sql);
    $s_logon_company_id_sql = str_replace("('", "(company_id = '", $s_logon_company_id_sql);
    $s_logon_company_id_sql = str_replace(",", " or company_id = ", $s_logon_company_id_sql);

    $_SESSION[$s_sessionno . 'dma_logon_company_id_sql'] = $s_logon_company_id_sql;
    IF ($sys_debug == "YES") {
        echo "<br>A700_SET_SESSION_VALUES doing set of session vars s_logon_company_id_sql=[]" . $s_logon_company_id_sql . "[]<br>";
    };

    A710_COMPANY_ID_DONE:

    A750_SKIP_DMA:

    A790_END:
    IF ($sys_debug == "YES") {
        die ("<br><br>utlogon die in debug - action = " . $action . " s_ok_map=" . $s_ok_map . " s_ret_map=" . $s_ret_map . ' s_siteparams=' . $s_siteparams . ") <br>");
    };


//    echo "<br> action  = ".$action."<br>";
//    echo "<br> s_ok_map  = ".$s_ok_map."<br>";

//die ("<br> gwdie sessionno=.".$ps_sessionno);

//B100_ACTION
    if (trim($action) == "ERROR") {
        $s_details_def = "|ERROR_MESSAGE";
        $s_details_data = "|" . $s_error_message;
        $_SESSION[$s_sessionno . 'session_def'] = $s_details_def;
        $_SESSION[$s_sessionno . 'session_data'] = $s_details_data;
        echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
        echo '<html>';
        echo '<head>';
        echo '<meta HTTP-EQUIV="REFRESH" content="0; url=ut_menu.php?fnid=101&map=' . $s_ret_map . '&p=' . $s_siteparams . '">';
        echo '</head>';
        echo '</html>';
        exit;
    }

    if (trim($action) == "MAINPAGE") {
        echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
        echo '<html>';
        echo '<head>';
        echo '<meta HTTP-EQUIV="REFRESH" content="0; url=ut_menu.php?fnid=101&map=' . $s_ok_map . '&p=' . $s_siteparams . '&setupuserid=' . $s_sessionno . '|' . $s_user_status_or_id . '">';
        echo '</head>';
        echo '</html>';
        exit;
    }

//    echo '<meta HTTP-EQUIV="REFRESH" content="0; url=ut_menu.php?fnid=101&map='.$s_ok_map.'&p='.$s_siteparams.'&setupuserid='.$s_sessionno.'|'.$s_user_status_or_id.'&dieme=yes">';

    echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
    echo '<html>';
    echo '<head>';
    echo '</head>';
    echo '<body> There is no option for this funtion ' . $function_id . '<BR> USERNAME=' . $username . '<BR> PWD=' . $password . '<BR> SESSIONID=' . $s_sessionno . '</body>';
// echo "<br><br><br>".print_r($_SESSION);
    echo '</html>';
    exit;
}

//########################################################################################################################################################################################


function pf_set_user_preferences($ps_dbcnx,$ps_sessionno,$ps_userid)
{
// get the utl_users_preferences record if the table exists
// set the preferences based on the values in utl_users_preferences.data_blob
    global $class_main;
    /* gw20110415 done in utmenu
        $s_deviceframe_width = "";
        $_SESSION[$ps_sessionno.'udp_site_css'] = "udp_site.css";
        $_SESSION[$ps_sessionno.'udp_bdyleft_css'] = "udp_bdyleft.css";
        $_SESSION[$ps_sessionno.'udp_navleft_css'] = "udp_navleft.css";
        $_SESSION[$ps_sessionno.'udp_bdymain_css'] = "udp_bydmain.css";

        $_SESSION[$ps_sessionno.'udj_site_css'] = "udj_site.css";

        $s_deviceframe_width = "150";
        $_SESSION[$ps_sessionno.'udp_deviceframe_width'] = $s_deviceframe_width;
        $_SESSION[$ps_sessionno.'udp_deviceframe_tabwidth'] = $s_deviceframe_width - 20;
        $_SESSION[$ps_sessionno.'udp_devicedesc'] = "User";

        $_SESSION[$ps_sessionno.'userbunit'] = "JOBMANAGEMENT";
        $_SESSION[$ps_sessionno.'ut_logon_timezone'] = "Australia/Sydney";

        $_SESSION['gwut_logon_timezone'] = "Australia/Sydney";

    */
    $sys_out = "";
    $sys_out = $sys_out.'<?xml version="1.0" encoding="ISO-8859-1"?><data_blob>';
    $sys_out = $sys_out.'<no_preferences>NoPreferences</no_preferences>';
    $sys_out = $sys_out.'</data_blob>';

    A100_GET_USER:
    $s_user_preferences = $class_main->clmain_B210_get_user_preferences($ps_dbcnx,$ps_userid,"NO",$ps_sessionno);
    $sys_out= $s_user_preferences;
    B100_SET_SESSION_VARS:

//echo "<br>".print_r(str_replace("<","openbracket^",$s_user_preferences));
//ECHO "<BR>utmenu setpref end ";

//ECHO "<BR>ut_logon user perferences start user_preference  ";
//echo "<br>".print_r($_SESSION);
//ECHO "<BR>ut_logon user perferencesend user_preference  ";
//die ("<br> gwdie sessionno=.".$ps_sessionno);
    if (strpos(strtoupper($s_user_preferences),"*ERROR") !== false )
    {
        GOTO B900_END;
    }
    $xml = new SimpleXMLElement($s_user_preferences);
    if (isset($xml->userbunit[0])){
        $_SESSION[$ps_sessionno.'userbunit'] = (string)$xml->userbunit[0];
// gw 201111        $_SESSION[$ps_sessionno.'userbunit'] = "ut_logon_gwtst";
    }
    if (isset($xml->timezone[0])){

// gw 201111        echo "ut_logon<br>";
        $s_temp = (string)$xml->timezone[0];
// gw 201111        echo "s_temp = ".$s_temp."<br>";
// gw 201111        echo "timezone[0] = ".$xml->timezone[0]."<br>";
        $_SESSION[$ps_sessionno.'ut_logon_timezone'] = $s_temp ;
// gw 201111        echo "session  timezone = ".$_SESSION[$ps_sessionno.'ut_logon_timezone']."<br>";
        // gw 201111IF (is_string($s_temp))
// gw 201111        {
// gw 201111               echo "IS A STRING = ".$s_temp."<br>";
// gw 201111        }else{
// gw 201111               echo "IS not A STRING = ".$s_temp." is ".gettype($s_temp)."<br>";
// gw 201111        }

        // gw 20111108 causes an error on windows so hardcoded this      $_SESSION[$ps_sessionno.'ut_logon_timezone'] = 'Australia/Brisbane';

// gw 201111     die ("gwdied set time zone session var   $ps_sessionno ".$_SESSION[$ps_sessionno.'ut_logon_timezone']);
    }



    B900_END:

    Z900_EXIT:
    return $sys_out;
}

//##########################################################################################################################################################################
function z901_dump($ps_do_debug,$ps_text)
{
    IF (trim(strtoupper($ps_do_debug)) == "NO")
    {
        goto Z900_EXIT;
    }

    $file_path = "/tdx_archive";
    if (!file_exists($file_path)) {
//        echo "create []".$file_path."[]";
        mkdir($file_path);
    }
    $file_path = $file_path."/".date("Ym",time());
    if (!file_exists($file_path)) {
//        echo "create []".$file_path."[]";
        mkdir($file_path);
    }
    $file_path = $file_path."/debug";
    if (!file_exists($file_path)) {
//        echo "create []".$file_path."[]";
        mkdir($file_path);
    }
    $file_path = $file_path."/".date("d",time());
    if (!file_exists($file_path)) {
//        echo "create []".$file_path."[]";
        mkdir($file_path);
    }
//

    $dateym = date('Ym');
    $myfile = $file_path."/ut_jcl_".$dateym.".html";
    $fh = fopen($myfile,'a') or die("cant open file".$myfile);
    fwrite($fh,"".date("Y-m-d H:i:s",time())." - ".$ps_text."\r\n<br>");
    fclose($fh);
    //   die("gwdieds1ss: filepath=[]".$file_path."[] at ".date("Y-m-d H:i:s",time()));

    Z900_EXIT:

}

?>
