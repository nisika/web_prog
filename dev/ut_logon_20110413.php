<?php
function pf_menu_customError($error_level,$error_message,$error_file,$error_line,$error_context)
  {
  echo "<b>Error gw12:</b> [$error_level] $error_message $error_file $error_line $error_context.<br>";
exit;
  }

 //set error handler
set_error_handler("pf_menu_customError");
date_default_timezone_set('Australia/Brisbane');



function pf_set_sessionid($ps_loginname)
{
// print_r($_SESSION);
    $sSessionid = "100";
    $sCheckSid = $sSessionid."udSid";
    while (isset($_SESSION[$sCheckSid])):
        $sSessionid = $sSessionid + 1;
        $sCheckSid = $sSessionid."udSid";
        IF ($sSessionid > 8 ) // want to keep the session no at a single digit
        {
//            echo "error setting id";
            break;
        }
        IF ($sSessionid > 120 )
        {
            echo "way too many  exit ";
            exit;
        }
    endwhile;
    $_SESSION[$sCheckSid] = $sCheckSid;
    return $sSessionid;
}

// A000_DEFINE_VARIABLES
    session_start();

    $sys_debug = '';
    $sys_prog_name = "logon1";

    $loginname = "";
    $password = "";
    $s_sessionno = "";
    $action = "";
    $s_params = "";
    $s_sys_language = "";
    $s_action_type = "";
    $s_ret_map = "";
    $s_error_map = "";
    $s_ok_map="";

// A100_INITIALISE_VARIABLES
    $action = "ERROR";
    $s_ret_map = "ut_logon_screen.html";
    $s_error_map = "ut_logon_error_snippet";
    $s_ok_map="udp_p1.htm";

//A200_LOAD_LIBRARIES
    $sys_debug = strtoupper("NO");
     IF ($sys_debug == "YES"){echo $s_prog_name." started debug=".$sys_debug."<br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_sql.php');
     $class_sql = new wp_SqlClient();
     IF ($sys_debug == "YES"){echo $s_prog_name." after class_sql<br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_main.php');
     $class_main = new clmain();
     IF ($sys_debug == "YES"){echo  $s_prog_name." after class_main <br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_myplib1.php5');
     $class_myplib1 = new myplib1();
     IF ($sys_debug == "YES"){echo  $s_prog_name." after class_myplib1<br>";};
     IF ($sys_debug == "YES"){echo "_get=";print_r($_GET);echo "<br>";};
     IF ($sys_debug == "YES"){echo "_post=";print_r($_POST);echo "<br>";};

//A300_CONNECT_TODBASE
     $dbcnx = $class_sql->c_sqlclient_connect();

     //A400_GET_PARAMETERS
     if (isset($_POST['ln'])) $loginname = $_POST["ln"];
     if (isset($_POST['pw'])) $password = $_POST["pw"];
     if (isset($_POST['params'])) $s_params = $_POST["params"];
     if (isset($_POST['sys_language'])) $s_sys_language = $_POST["sys_language"];
     if (isset($_POST['action_type'])) $s_action_type = $_POST["action_type"];
     if (isset($_POST['ret_map'])) $s_ret_map = $_POST["ret_map"];
     if (isset($_POST['error_map'])) $s_error_map = $_POST["error_map"];
     if (isset($_POST['ok_map'])) $s_ok_map = $_POST["ok_map"];

     IF ($sys_debug == "YES"){print_r($sys_prog_name."  ".$_POST);};


//A500_SET_VALUES
// if no values in params yet it will only contain ^
//    if (strlen($s_params) < 2 ){
// if the st char is a ^ then no session id has been set
    if (strpos($s_params,"^") == 0) {
        IF ($sys_debug == "YES"){print_r($sys_prog_name."getting new session id");};
        $s_sessionno = pf_set_sessionid($loginname);
    }else{
        $s_sessionno = substr($s_params,0,strpos($s_params,"^"));
    }
    IF ($sys_debug == "YES"){print_r($sys_prog_name."session id = ".$s_sessionno);};


    $username = $loginname;
    $s_error_message = "noerror";
    if (TRIM($password) == ""){
        $s_error_message="INSERTCODE_".$s_error_map."_E2003";
     }
    if (TRIM($username) == ""){
        $s_error_message="INSERTCODE_".$s_error_map."_E2002";
     }

     if ($s_action_type =="SETLANG")
     {
        $s_error_message="INSERTCODE_".$s_error_map."_E2004";
        setcookie("ud_sys_language",$s_sys_language,time()+60*60*24*30);
     }


//A600_CHECK_USERNAME
    if ($s_error_message == "noerror")
    {
         $s_User_status = $class_main->clmain_B200_check_user($dbcnx,$username,$password,"NO",$s_sessionno);
         IF ($sys_debug == "YES"){echo "USER STATUS = ".$s_User_status."<br>";};

    //     if (strpos(strtoupper($s_User_status), "ERROR" ) > 0)
         if (strpos(strtoupper($s_User_status),"ERROR") === false )
          {
             setcookie('ud_logonname',$username,time()+60*60*24*30);
             $action = "MAINPAGE";
             pf_set_user_preferences($s_sessionno,$username);
          }else{
             $s_error_message = TRIM($s_User_status);
             $s_error_id = str_replace("*","",$s_User_status);
             $s_error_id = str_replace("ERROR ","",$s_error_id);
             $s_error_message = $s_error_id;
             IF (strtoUpper(TRIM($s_error_id)) == "E2001_UNKNOWN_USER")
             {
                $s_error_message="INSERTCODE_".$s_error_map."_E2001";//, "UNKNOWN_USER");
             }
             IF (strtoUpper(TRIM($s_error_id)) == "E2006_WRONG_PWD")
             {
                $s_error_message="INSERTCODE_".$s_error_map."_E2006";//, "UNKNOWN_USER");
             }
          }
    }
//A700_SET_SESSION_VALUES
    $s_siteparams = "".$s_sessionno."^";
    $_SESSION[$s_sessionno.'username'] = $username;
    $s_siteparams = "".$s_sessionno."^"."logonid"."^";

    //B100_ACTION
  if (trim($action) == "ERROR"){
    $s_details_def = "|ERROR_MESSAGE";
    $s_details_data = "|".$s_error_message;
    $_SESSION[$s_sessionno.'session_def'] = $s_details_def;
    $_SESSION[$s_sessionno.'session_data'] = $s_details_data;
    echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
    echo '<html>';
    echo '<head>';
    echo '<meta HTTP-EQUIV="REFRESH" content="0; url=ut_menu.php?fnid=101&map='.$s_ret_map.'&p='.$s_siteparams.'">';
    echo '</head>';
    echo '</html>';
    exit;
  }

  if (trim($action) == "MAINPAGE"){
    echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
    echo '<html>';
    echo '<head>';
    echo '<meta HTTP-EQUIV="REFRESH" content="0; url=ut_menu.php?fnid=101&map='.$s_ok_map.'&p='.$s_siteparams.'">';
    echo '</head>';
    echo '</html>';
    exit;
  }


    echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
    echo '<html>';
    echo '<head>';
    echo '</head>';
    echo '<body> There is no option for this funtion '.$function_id.'<BR> USERNAME='.$username.'<BR> PWD='.$password.'<BR> SESSIONID='.$s_sessionno.'</body>';
    echo '</html>';
    exit;

function pf_set_user_preferences($ps_sessionno,$ps_username)
{
$s_deviceframe_width = "";


// get user master record and preferences
    $_SESSION[$ps_sessionno.'udp_site_css'] = "udp_site.css";
    $_SESSION[$ps_sessionno.'udp_bdyleft_css'] = "udp_bdyleft.css";
    $_SESSION[$ps_sessionno.'udp_navleft_css'] = "udp_navleft.css";
    $_SESSION[$ps_sessionno.'udp_bdymain_css'] = "udp_bydmain.css";

    $_SESSION[$ps_sessionno.'udj_site_css'] = "udj_site.css";

    $s_deviceframe_width = "150";
    $_SESSION[$ps_sessionno.'udp_deviceframe_width'] = $s_deviceframe_width;
    $_SESSION[$ps_sessionno.'udp_deviceframe_tabwidth'] = $s_deviceframe_width - 20;
    $_SESSION[$ps_sessionno.'udp_devicedesc'] = "User";

    $_SESSION[$ps_sessionno.'userbunit'] = "JOBMANAGEMENT";
    $_SESSION[$ps_sessionno.'ut_logon_timezone'] = "Australia/Sydney";
//gw20110313 - added user timezone
/*    if (isset($_SESSION[$s_sessionno.'ut_logon_timezone']))
    {
        $timezone=$_SESSION[$s_sessionno.'ut_logon_timezone'];
        date_default_timezone_set($timezone);
    }else{
    }
*/
    // can change this to the users requirements

}
  ?>
