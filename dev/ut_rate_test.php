<!--
history  ut_rate_test.php
gjw  20110312 - created from my_help from mypallet bok
-->
<?php

function pf_customError($errno,$errstr,$errfile,$errline,$errcontext)
  {
// NOTE **** THIS ONLY CATCHES NON-FATAL ERRORS
//  echo "<b>Error gw12:</b> [$errno] $errstr $errfile $errline $errcontext.<br>";
// gw 20100315       switch ($error_level) {
       switch ($errno) {
        case E_USER_ERROR:
            echo "<b>My ERROR</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            echo "  Fatal error on line $errline in file $errfile";
            echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
            echo "Aborting...<br />\n";
            exit(1);
            break;

        case E_USER_WARNING:
            echo "<b>My WARNING</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        case E_USER_NOTICE:
            echo "<b>My NOTICE</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        default:
            echo "<BR>Unknown error type: [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;
        }

    /* Don't execute PHP internal error handler */
    return true;

  }

A000_SET_RUN:
     //set error handler
    set_error_handler("pf_customError", E_ALL);
    date_default_timezone_set('Australia/Brisbane');
    session_start();


    $sys_debug="";
    $sys_debug = strtoupper("NO");
//    $sys_debug = strtoupper("yes");

     IF ($sys_debug == "YES"){echo $sys_prog_name." started debug=".$sys_debug." *** remember to view source - it will save you hours  <br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_sql.php');
     $class_sql = new wp_SqlClient();
     IF ($sys_debug == "YES"){echo $sys_prog_name." after class_sql<br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_main.php');
     $class_main = new clmain();
     IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_main <br>";};

     require_once($_SESSION['ko_prog_path'].'lib/class_rate.php');
     $class_rate = new clrate();
     IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_rate <br>";};

     $ar_line_details = array();

A300_CONNECT_TODBASE:
     $dbcnx = $class_sql->c_sqlclient_connect();


     $get_runfrom = "notset";
     if (isset($_GET['runfrom']))
     {
         $get_runfrom = $_GET['runfrom'];
     }
     if (isset($_GET['p']))
     {
          $s_url_siteparams = $_GET["p"];
          $s_url_siteparams = $class_main->clmain_u630_tidy_siteparams($s_url_siteparams, "NO");
          $s_sessionno = $class_main->clmain_get_param($s_url_siteparams,"s_sessionno","no");
     }

     if (isset($_POST['rate']))
     {
         $get_runfrom = $_POST['rate'];
     }

     if (isset($_POST['sessionno']))
     {
         $s_sessionno = $_POST['sessionno'];
     }

     $get_runfrom = strtoupper(trim($get_runfrom));
B100_START:
     if ($get_runfrom <> "START")
     {
         GOTO B190_SKIP_START;
     }
    $s_filename=$_SESSION['ko_prog_path'].'ut_rate_test_map.html';
    $s_details_def='START|runtime|sessionno|END';
    $s_details_data='START|'.$get_runfrom."|".$s_sessionno.'|END';
    echo $class_main->clmain_v100_load_html_screen($s_filename,$s_details_def,$s_details_data,"NO","WHOLE_FILE");
    GOTO B900_END;

B190_SKIP_START:
B200_RATE:
     if ($get_runfrom <> "RATE")
     {
         GOTO B290_SKIP_RATE;
     }

    $s_key1 = $_POST['key1'];
    $s_key2 = $_POST['key2'];
    $s_key3 = $_POST['key3'];
    $s_key4 = $_POST['key4'];
    $s_key5 = $_POST['key5'];
    $s_options = $_POST['params'];;
    $s_details_def='START|runtime|key1|key2|key3|key4|key5';
    $s_details_def=$s_details_def."|END";
    $s_details_data='START|'.$get_runfrom."|".$s_key1."|".$s_key2."|".$s_key3."|".$s_key4."|".$s_key5.'|END';

    $s_calledfrom = "";

    $s_rate_result = $class_rate->clrate_a100_rate_this($dbcnx,$sys_debug,$s_calledfrom,$s_details_def,$s_details_data,$s_sessionno,$s_key1,$s_key2,$s_key3,$s_key4,$s_key5,$s_options);

// break the results down and add to details_def/data
    if (!strpos($s_rate_result,"|^%##%^|") ===false)
    {
        $ar_line_details = explode("|^%##%^|",$s_rate_result);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
        $s_rate_result = "";
    };

//    echo "<br>s_rate_result = ".$s_rate_result."<br>";

    $s_filename=$_SESSION['ko_prog_path'].'ut_rate_test_result_map.html';
    echo $class_main->clmain_v100_load_html_screen($s_filename,$s_details_def,$s_details_data,"NO","WHOLE_FILE");
    GOTO B900_END;
B290_SKIP_RATE:

B300_RATE:
     if ($get_runfrom <> "RATETCO")
     {
         GOTO B390_SKIP_RATE;
     }

    $s_options = $_POST['params'];;
    $s_details_def='START|runtime|key1|key2|key3|key4|key5';
    $s_details_def=$s_details_def."|END";
    $s_details_data='START|'.$get_runfrom."|s_key1|s_key2|s_key3|s_key4|s_key5|END";
    $s_calledfrom = "";

    $s_rate_result = $class_rate->clrate_c100_rate_this_transport($dbcnx,$sys_debug,$s_calledfrom,$s_details_def,$s_details_data,$s_sessionno,$s_options);

// break the results down and add to details_def/data
    if (!strpos($s_rate_result,"|^%##%^|") ===false)
    {
        $ar_line_details = explode("|^%##%^|",$s_rate_result);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
        $s_rate_result = "";
    };

//    echo "<br>s_rate_result = ".$s_rate_result."<br>";

    $s_filename=$_SESSION['ko_prog_path'].'ut_rate_test_result_map.html';
    echo $class_main->clmain_v100_load_html_screen($s_filename,$s_details_def,$s_details_data,"NO","WHOLE_FILE");
    GOTO B900_END;
B390_SKIP_RATE:

    echo "<br>Not valid get_runfrom option (".$get_runfrom.") <br>";
    IF ($sys_debug == "YES"){echo "after main<br>";};
B900_END:

Z900_EXIT:

?>
