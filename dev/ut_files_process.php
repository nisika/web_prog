<?php
/*
Name:ut_files_proessing

*/
function pf_customError($errno,$errstr,$errfile,$errline,$errcontext)
  {
// NOTE **** THIS ONLY CATCHES NON-FATAL ERRORS
//  echo "<b>Error gw12:</b> [$errno] $errstr $errfile $errline $errcontext.<br>";
// gw 20100315       switch ($error_level) {
       switch ($errno) {
        case E_USER_ERROR:
            echo "<b>My ERROR</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            echo "  Fatal error on line $errline in file $errfile";
            echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
            echo "Aborting...<br />\n";
            exit(1);
            break;

        case E_USER_WARNING:
            echo "<b>My WARNING</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        case E_USER_NOTICE:
            echo "<b>My NOTICE</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        default:
            echo "<BR>Unknown error type: [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;
        }

    /* Don't execute PHP internal error handler */
    return true;

  }

//    $sys_debug = strtoupper("yes");
//    $_SESSION['ko_prog_path'] = 'c:\\tdx\\web_prog\\dev\\';

A000_SET_RUN:
     //set error handler
    set_error_handler("pf_customError", E_ALL);
    date_default_timezone_set('Australia/Brisbane');
    session_start();

//gw20110921 - added user timezone
    if (isset($_SESSION[$s_sessionno.'ut_logon_timezone']))
    {
        $timezone=$_SESSION[$s_sessionno.'ut_logon_timezone'];
        date_default_timezone_set($timezone);
    }else{
    }


    $s_job_start = microtime(true);

    if (isset($_SESSION['ko_prog_path']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
    }else{
    $_SESSION['ko_prog_path'] = "";
    }
    if (isset($_SESSION['ko_dbase_to_connectto']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
    }else{
    $_SESSION['ko_dbase_to_connectto'] = "tdxpryda";
    }

    if (isset($_SESSION['ko_map_path']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
        $ko_map_path = $_SESSION['ko_map_path'];
    }else{
    $_SESSION['ko_map_path'] = "";
    }

A000_DEFINE_VARIABLES:
    $sys_prog_name = "";
    $sys_debug = '';
    $s_file_exists = '';
    $s_filename="";
    $map = "";
    $tmp_array = array();
    $sys_function_out = "";
    $s_temp_value = "";

    $array_fields = array();
    $array_lines = array();
    $s_tmp_array = array();
    $s_import_folder = "**** import_folder_notset";
    $s_valid_file_function = "**** valid_file_function_notset";
    $s_run_process_function = "**** import_records_function_notset";
    $s_archive_folder = "**** archive_folder_notset";
    $s_unknown_file_action = " **** no action set for unknown file";

    $s_sessionno = "";

    $s_file_count = "0";
    $s_file_count_error = "0";
    $s_file_count_skip = "0";
    $s_file_count_unknown = "0";
    $s_file_count_unknown_leave = "0";
    $s_file_action = "notset";
    $s_file_archive = "notset";


A200_LOAD_LIBRARIES:
    $sys_debug = strtoupper("NO");

     IF ($sys_debug == "YES"){echo $sys_prog_name." started debug=".$sys_debug." *** remember to view source - it will save you hours  <br>";};
     require_once($_SESSION['ko_prog_path'].'lib\\class_sql.php');
     $class_sql = new wp_SqlClient();
     IF ($sys_debug == "YES"){echo $sys_prog_name." after class_sql<br>";};
     require_once($_SESSION['ko_prog_path'].'lib\\class_main.php');
     $class_main = new clmain();
     IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_main <br>";};

//     require_once($_SESSION['ko_prog_path'].'lib/class_myplib1.php5');
//     $class_myplib1 = new myplib1();
//     IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_myplib1<br>";};

     require_once($_SESSION['ko_map_path'].'lib/class_app.php');
     $class_apps = new AppClass();

     IF ($sys_debug == "YES"){echo "_get=";print_r($_GET);echo "<br>";};
     IF ($sys_debug == "YES"){echo "_post=";print_r($_POST);echo "<br>";};

A300_CONNECT_TODBASE:
     $dbcnx = $class_sql->c_sqlclient_connect();

A400_GET_PARAMETERS:
     if (isset($_GET['process_jcl'])) $import_jcl = $_GET["process_jcl"];
     if (isset($_GET['p'])) $s_url_siteparams = $_GET["p"];
//get the jcl name from the url
    $import_jcl_map = $class_main->clmain_set_map_path_n_name($ko_map_path,$import_jcl);
    $sys_prog_name = "ut_file_import.php";
    $sys_function_name = $sys_prog_name;
    IF ($sys_debug == "YES"){echo $sys_prog_name." ";};

    $s_file_exists='Y';
    if(file_exists($import_jcl_map))
    {
        $array_lines = file($import_jcl_map);
    }
    else
    {
        $s_file_exists='N';
        $sys_function_out =  "<br>".$sys_prog_name." ##### File Not Found-:".$import_jcl_map."<br>";
        GOTO Z900_EXIT;
    }

    $tmp = $class_main->clmain_u540_utl_activity_log($dbcnx,"HEARTBEAT","SYSTEM","ut_file_process is running","ut_file_process.php","test blob",$import_jcl,"Process","k2 value","k2 def","k3 value","k3 def","k4 value","k4 def","k5 value","k5 def");


A410_DO_LINES:
    $i = 0;
    $s_line_in = "";
A420_GET_REC:
    IF ($i >= count($array_lines))
    {
        goto Z499_EXIT;
    }

    $s_line_in = $array_lines[$i];
    IF (substr($s_line_in,0,1) == "*")
    {
        goto Z490_GET_NEXT;
    }
    $s_line_in = $array_lines[$i];
    $array_fields = explode("=",$s_line_in);
    $s_var_type = strtoupper(trim($array_fields[0],""));
    if ($s_var_type == "ARCHIVE_GROUP")
    {
     $s_archive_folder = $array_fields[1];
    }
    if ($s_var_type == "IMPORT_FOLDER")
    {
     $s_import_folder = $array_fields[1];
    }
    if ($s_var_type == "VALID_FILE_FUNCTION")
    {
     $s_valid_file_function = $array_fields[1];
    }
    if ($s_var_type == "IMPORT_RECORDS_FUNCTION")
    {
     $s_run_process_function = $array_fields[1];
    }
    if ($s_var_type == "UNKNOWN_FILE_ACTION")
    {
     $s_unknown_file_action = $array_fields[1];
    }
Z490_GET_NEXT:
    $i = $i + 1;
    goto A420_GET_REC;

Z499_EXIT:
     if (   $s_archive_folder == "**** archive_folder_notset")
     {
         $s_archive_folder = $import_jcl;
     }

//'open map jcl and get
//
//' valid_file_function
// use this to get the valid_file_description
// run the finction for each file
// import_records_function

A500_SET_VALUES:
    // open this directory
    IF (TRIM($s_import_folder," ") == "")
    {
        $s_import_folder = "DEFAULT";
    }

    $path =$ko_map_path."load\\";
    if (strtoupper(trim($s_import_folder," ")) <> "DEFAULT")
    {
        $path = $s_import_folder;
    }
    $s_archive_path = "c:\\tdx_archive\\".date("Y")."\\".date("m")."-".date("M")."\\".date("d")."\\".$s_archive_folder."\\";

    if (!is_dir($s_archive_path))
    {
        if(!mkdir($s_archive_path,0,true))
        {
            die("Failed to create the archive fodler ".$s_archive_path);
        }
    }

    if (!is_dir($path))
    {
            die("ut_file_import.php <br>  The import folder ".$path." does not exist.  Using jcl = ".$import_jcl_map." which has a import_folder value of ".$s_import_folder." which tests to --".strtoupper(trim($s_import_folder))."--");
    }

B100_LIST_FILES:
    //    $path =$ko_map_path;
    $file_path = opendir($path);

    // get each entry
    while($entryName = readdir($file_path)) {
        $dirArray[] = $entryName;
    }

    // close directory
    closedir($file_path);

    //    count elements in array
    $indexCount    = count($dirArray);
//    Print ("$indexCount files<br>\n");
    // sort 'em
    sort($dirArray);

    // print 'em
    $sys_function_out = $sys_function_out."<TABLE border=1 cellpadding=5 cellspacing=0 class=whitelinks>";
    $sys_function_out = $sys_function_out."<TR><td colspan='5' bgcolor='cyan'>There are ".$indexCount." files in the ".$path." folder - including system folders";
    $sys_function_out = $sys_function_out."<br> files will be archived to ".$s_archive_path."</td></TR>";
    $sys_function_out = $sys_function_out."<TR><td colspan='5' bgcolor='green'>Files in ";
    $sys_function_out = $sys_function_out."<br>Yellow will not be processed";
    $sys_function_out = $sys_function_out."<br> Red are files that have had a problem";
    $sys_function_out = $sys_function_out."<br> White will be processed";
    $sys_function_out = $sys_function_out."<br> Orange are unknown fmat and will be skipped";
    $sys_function_out = $sys_function_out."<br> Grey are unknown files to be left alone";
    $sys_function_out = $sys_function_out."</td></TR>";

    $sys_function_out = $sys_function_out."<TR><td colspan='5' bgcolor='pink'>";
    $sys_function_out = $sys_function_out."jcl processing values:<br>";
    $sys_function_out = $sys_function_out."Import folder = ".$s_import_folder."<br>";
    $sys_function_out = $sys_function_out."valid_file_function = ".$s_valid_file_function."<br>";
    $sys_function_out = $sys_function_out."import_records_function = ".$s_run_process_function."<br>";
    $sys_function_out = $sys_function_out."archive_folder = ".$s_archive_folder." ( if matches the jcl name then it has not been set in the jcl)<br>";
    $sys_function_out = $sys_function_out."unknown file action  = ".$s_unknown_file_action." ( will move the file to filename.unknown if not set to 'leave')<br>";

    $sys_function_out = $sys_function_out."</td></TR>";

    $sys_function_out = $sys_function_out."<TR><TH>Filename</TH><th>Filetype</th><th>Filesize</th><th>Modified</th><th>Format/Fmat checked/load class/run function</th></TR>";

B200_CONTENTS_LOOP:
    // loop through the array of files and print them all
    for($index=0; $index < $indexCount; $index++)
    {
        if (substr("$dirArray[$index]", 0, 1) == "."){  // don't list hidden files
            GOTO B290_SKIP_LINE;
        }
        if (filetype($path.$dirArray[$index]) == "dir"){  // don't list directories
            GOTO B290_SKIP_LINE;
        }

        $filename = $dirArray[$index];
        $s_bgcolor="bgcolor='white'";
        $s_display_count = "na";

        $s_filefmat = pf_b300_set_filefmat($dbcnx,$path,$filename,"no",$s_valid_file_function);
        $s_tmp_array = explode("|",$s_filefmat);
        $s_filefmat = $s_tmp_array[0];
        $s_filefmatchecked = $s_tmp_array[1];
        $s_filefmatclass = $s_tmp_array[2];
        $s_filefmatfunction = $s_tmp_array[3];

        $s_bgcolor = pf_b200_set_process_colour($path,$filename,"nousedyet","notusedyet","no",$s_filefmat,$s_unknown_file_action);
        if ($s_bgcolor=="bgcolor='yellow'")
        {
            $s_file_count_skip = $s_file_count_skip + 1;
            $s_display_count = $s_file_count_skip;
        }
        if ($s_bgcolor=="bgcolor='red'")
        {
            $s_file_count_error = $s_file_count_error + 1;
            $s_display_count = $s_file_count_error;
        }
        if ($s_bgcolor == "bgcolor='white'")
        {
            $s_file_count = $s_file_count + 1;
            $s_display_count = $s_file_count;
        }
        if ($s_bgcolor=="bgcolor='orange'")
        {
            $s_file_count_unknown = $s_file_count_unknown + 1;
            $s_display_count = $s_file_count_unknown;
        }
        if ($s_bgcolor=="bgcolor='gray'")
        {
            $s_file_count_unknown_leave = $s_file_count_unknown_leave + 1;
            $s_display_count = $s_file_count_unknown_leave;
        }

B100_DO_FILE:
        $sys_function_out = $sys_function_out."<TR ".$s_bgcolor."><TD>".$s_display_count." = $dirArray[$index]</a></td>";
        $sys_function_out = $sys_function_out."<td>";
        $sys_function_out = $sys_function_out.filetype($path.$filename);
        $sys_function_out = $sys_function_out."</td>";
        $sys_function_out = $sys_function_out."<td>";
        $sys_function_out = $sys_function_out.filesize($path.$filename);
        $sys_function_out = $sys_function_out."</td>";
        $sys_function_out = $sys_function_out."<td>";
        $sys_function_out = $sys_function_out.filemtime($path.$filename);
        $sys_function_out = $sys_function_out." - ";
        $sys_function_out = $sys_function_out.date("Y-m-d H:i:s",filemtime($path.$filename));
        $sys_function_out = $sys_function_out."</td>";
        $sys_function_out = $sys_function_out."<td>";
        $sys_function_out = $sys_function_out.$s_filefmat;
        $sys_function_out = $sys_function_out."</td>";

        $sys_function_out = $sys_function_out."</TR>";
B290_SKIP_LINE:
    }
B900_EXIT:
    $sys_function_out = $sys_function_out."<TR bgcolor='green'><td colspan='5'>There are";
    $sys_function_out = $sys_function_out."<br> ".$s_file_count." files to be processed";
    $sys_function_out = $sys_function_out."<br>".$s_file_count_skip." are to be skipped";
    $sys_function_out = $sys_function_out."<br>".$s_file_count_error." have had a previous error";
    $sys_function_out = $sys_function_out."<br>".$s_file_count_unknown." are unknown format files";
    $sys_function_out = $sys_function_out."<br>".$s_file_count_unknown_leave." are unknown format to be left alone";

    $sys_function_out = $sys_function_out."</td></TR>";
    $sys_function_out = $sys_function_out."</TABLE>";
    ECHO $sys_function_out;
B999_END_OF_LIST_FILES:

C010_DO_FILE_PROCESS:
    u900_write_text_file("LOG",  $s_archive_folder."_".date("YM").".TXT", "There are".$s_file_count." files to be processed ".$s_file_count_skip." are to be skipped ".$s_file_count_error." have had a previous error ".$s_file_count_unknown." are unknown format files",$s_archive_folder);

    $sys_function_out = "";
    $sys_function_out = $sys_function_out."<br>Processing of files started at ".date("Y-m-d H:i:s");
    ECHO $sys_function_out;
    $sys_function_out = "";
    $f_fprocess_start = microtime(true);

    $s_file_count_error = "0";
    $s_file_count_skip = "0";
    $s_file_count_now = "0";
C100_PROCESS_FILES:
    for($index=0; $index < $indexCount; $index++)
    {
        $f_dofile_start = microtime(true);
        if (substr("$dirArray[$index]", 0, 1) == "."){  // don't list hidden files
            GOTO C290_SKIP_LINE;
        }
        if (filetype($path.$dirArray[$index]) == "dir"){  // don't list directories
            GOTO C290_SKIP_LINE;
        }
        $filename = $dirArray[$index];
        $s_bgcolor="bgcolor='white'";
        $s_filefmat = pf_b300_set_filefmat($dbcnx,$path,$filename,"no",$s_valid_file_function);
        $s_tmp_array = explode("|",$s_filefmat);
        $s_filefmat = $s_tmp_array[0];
        $s_filefmatchecked = $s_tmp_array[1];
        $s_filefmatclass = $s_tmp_array[2];
        $s_filefmatfunction = $s_tmp_array[3];

        $s_bgcolor = pf_b200_set_process_colour($path,$filename,"nousedyet","notusedyet","no",$s_filefmat,$s_unknown_file_action);

C100_DO_FILE:
        $sys_function_out = "";
        $sys_function_out = $sys_function_out."<br>".date("Y-m-d H:i:s")."  File Processing started ".$path.$dirArray[$index];
        ECHO $sys_function_out;
        $sys_function_out = "";
        if ($s_bgcolor=="bgcolor='yellow'")
        {
         //       pf_b300_move_to_alert($dbcnx, $path,$dirArray[$index],"c100_do_file",$s_archive_path);
            GOTO C200_END_FILE;
        }
        if ($s_bgcolor=="bgcolor='red'")
        {
            $s_file_action = "File is .working = has had a problem in a previous run  ";
            u900_write_text_file("ALERT", $s_archive_folder."_IN_ERROR_FAILED_FILE.TXT", "The file .".$path.$filename.". has failed in a previous run or is being process while this process is trying to run",$s_archive_folder);
            GOTO C200_END_FILE;
        }
        if ($s_bgcolor=="bgcolor='gray'")
        {
            $s_file_action = "File has a been left in place ";
            GOTO C200_END_FILE;
        }
        if ($s_bgcolor=="bgcolor='orange'")
        {
            $s_file_action = "File is unknown ";
            $s_file_archive = $s_archive_path.$filename."_".date("YmdHis").".unknown";
            u900_write_text_file("LOG", $s_archive_folder.date("YM").".TXT", "The file .".$path.$filename.". is an unknown format has has been moved to ".$s_file_archive,$s_archive_folder);
            u900_write_text_file("ALERT", "ERROR_".$s_archive_folder."_IS_UNKNOWN_FILE.TXT", "The file .".$path.$filename.". is an unknown format has has been moved to ".$s_file_archive,$s_archive_folder);
            IF ($sys_debug == "YES"){echo $sys_function_name."  unknown file  ".$path.$filename. " started by renaming it to ".$s_file_archive;};
            rename($path.$filename,$s_file_archive);
            IF ($sys_debug == "YES"){echo $sys_function_name."  unknown file after the rename on file = ".$s_file_archive;};
            GOTO C200_END_FILE;
        }
        if ($s_bgcolor == "bgcolor='white'")
        {
            $s_file_action = "File has been processed ";
            $s_file_count_now = $s_file_count_now +1;
            $sys_function_out = "";
            $sys_function_out = $sys_function_out."<br>File ".$s_file_count_now." of ".$s_file_count." Processing started ".$path.$dirArray[$index]." at ".date("Y-m-d H:i:s");
            ECHO $sys_function_out;

            pf_b100_process_file($dbcnx, $path,$dirArray[$index],"no",$s_archive_path,$s_filefmat,$s_filefmatchecked,$s_filefmatclass, $s_filefmatfunction);

        }
C200_END_FILE:
        $f_dofile_end = microtime(true);
        $duration= round($f_dofile_end - $f_dofile_start,4);
        $sys_function_out = "";
        $sys_function_out = $sys_function_out."<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".date("Y-m-d H:i:s")." in ".$duration." seconds ".$s_file_action.".  Process ended ".$path.$dirArray[$index];
        if (strtoupper(trim($s_file_archive)) <> "NOTSET")
        {
            $sys_function_out = $sys_function_out."<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; and archived to ".$s_file_archive;
        }
        ECHO $sys_function_out;
        $sys_function_out = "";

C290_SKIP_LINE:
    }

C999_END_FILE_PROCESS:

Z900_EXIT:
     $sys_function_out = $sys_function_out."<br>Process is complete at ".date("Y-m-d H:i:s");
     ECHO $sys_function_out;

//END OF PRIMARY RUN
?>

<?php
// ***********************************************************************************************************************************
// start functions

function pf_b100_process_file($ps_dbcnx,$ps_path,$ps_filename,$ps_debug,$ps_archive_path,$ps_filefmat,$ps_filefmatchecked,$ps_filefmatclass, $ps_filefmatfunction )
{

    global $class_main;
    global $class_sql;

    $dbcnx = $ps_dbcnx;
    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "<br>debug - pf_b100_process_file - from=".$sys_debug_text;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>DEBUG VIEW SOURCE FOR DETAILS#######################################################<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_filename = ".$ps_filename." ps_debug = ".$ps_debug." ";};


    $s_file_in = $ps_path.$ps_filename;
    $s_file_work = $ps_path.$ps_filename.date("his").".working";
    $s_file_archive = $ps_archive_path.$ps_filename."_".date("YmdHis").".done";

    IF ($sys_debug == "YES"){echo $sys_function_name."  I'm doing ".$s_file_in. " started by renaming it to ".$s_file_work;};
    rename($s_file_in,$s_file_work);
    IF ($sys_debug == "YES"){echo $sys_function_name."  Starting to process after the rename on file = ".$s_file_work;};

A100_INIT:
    u900_write_text_file("LOG", "ut_file_process.log", "The file ".$ps_filename.". ps_filefmat=".$ps_filefmat." ps_filefmatchecked=".$ps_filefmatchecked." ps_filefmatclass=".$ps_filefmatclass." ps_filefmatfunction=".$ps_filefmatfunction." ","file_process");
    $sys_function_out = "";
    $sys_function_out = $sys_function_out."<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The file .".$ps_filename.". ps_filefmat=".$ps_filefmat." ps_filefmatchecked=".$ps_filefmatchecked." ps_filefmatclass=".$ps_filefmatclass." ps_filefmatfunction=".$ps_filefmatfunction." ";
    ECHO $sys_function_out;

C100_PROCESS_FILE_FMAT:
    IF (strpos(strtoupper($ps_filefmatclass),"CLAPP_") === false )
    {} else
    {
         require_once($_SESSION['ko_map_path'].'lib\\'.$ps_filefmatclass.'.php');
         $class_apps_import = new $ps_filefmatclass();
         $stmp = $class_apps_import->clapp_fp_file_process($dbcnx,"no","ut_file_process pf_b100 c100 ",$s_file_work,$ps_filefmatfunction);
    }

ZZ900_EXIT:

Z900_EXIT:
    IF ($sys_debug == "YES"){echo $sys_function_name."  I'm doing anarchive of the file ".$s_file_work." to ".$s_file_archive;};
    rename($s_file_work,$s_file_archive);
    IF ($sys_debug == "YES"){echo $sys_function_name."  the archive did not fail";};

    return $sys_function_out;

}
// ***********************************************************************************************************************************
function pf_b200_set_process_colour($ps_path,$ps_filename,$ps_no_proccess_list,$ps_error_list,$ps_debug,$ps_filefmat,$ps_unknown_file_action)
{
// need to modify to check the values against the error_list and no_process_lists
    $s_filename = $ps_filename;
    $s_bgcolor="bgcolor='white'";
    if (strtoupper($ps_filefmat) == "UNKNOWN")
    {
        $s_bgcolor="bgcolor='orange'";
        if (strtoupper(trim($ps_unknown_file_action)) == "LEAVE")
        {
            $s_bgcolor="bgcolor='gray'";
        }
        GOTO Z900_EXIT;
    }
B100_DO_NO_PROCESS:
        if (strpos($s_filename,".done") === false)
        {
        }else{
            $s_bgcolor="bgcolor='yellow'";
            goto Z900_EXIT;
        }
C100_DO_ERROR_LIST:
        if (strpos($s_filename,".working") === false)
        {
            GOTO Z900_EXIT;
        }else{
            $s_bgcolor="bgcolor='red'";
        }
Z900_EXIT:
    return $s_bgcolor;
}
// ***********************************************************************************************************************************
function pf_b300_set_filefmat($ps_dbcnx,$ps_path,$ps_filename,$ps_debug,$ps_valid_file_function)
{
    global $class_main;
    global $class_sql;
    global $class_apps;


    $s_valid_file_function = $ps_valid_file_function;
    $s_filename = $ps_filename;
    $dbcnx = $ps_dbcnx;

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "<br>debug - pf_b300_set_filefmat - from=".$sys_debug_text;
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."<br>DEBUG VIEW SOURCE FOR DETAILS#######################################################<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_filename = ".$ps_filename." ps_debug = ".$ps_debug." ";};

    $s_filefmat = "Unknown";
    $s_action = "RUN_FUNCTION";
    $s_filefmat = "Unknown";

A100_USE_APP_CLASS:
    IF (strpos(strtoupper($s_valid_file_function),"CLAPP_") === false)
    {}else{
        goto B100_DO_APP_CLASS;
    }
    IF (strpos(strtoupper($s_valid_file_function),"UT_SYS_FILE") === false)
    {}else{
        goto C100_DO_UT_SYS_FILE;
    }

A900_EXIT:
    GOTO Z900_EXIT;


B100_DO_APP_CLASS:
    IF (strpos(strtoupper($s_valid_file_function),"UT_SYS_FILE+") === false)
    {}else{
        $s_filefmat = $class_main->clmain_u550_utl_sys_file_format($dbcnx,$s_filename,"no","UT_FP_B100");
    }


    if (substr($s_filefmat,0,7) <> "Unknown")
    {
        goto B900_EXIT;
    }
    $s_valid_file_function = substr($s_valid_file_function,12);
    $s_filefmat = $class_apps->clapp_a100_do_app_class($s_valid_file_function,$s_filename,$s_action,$dbcnx,"no","ut_file_import b300","","","");
B900_EXIT:
    GOTO X100_TRANSLATE_FMAT;

C100_DO_UT_SYS_FILE:
    $s_filefmat = $class_main->clmain_u550_utl_sys_file_format($dbcnx,$s_filename,"no","UT_FP_c100");

C900_EXIT:
    GOTO X100_TRANSLATE_FMAT;

X100_TRANSLATE_FMAT:

Z900_EXIT:
    return $s_filefmat;
}
function Pf_b600_do_app_class($ps_dbcnx,$ps_line,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{

A001_DEFINE_VARS:
    $ar_line_details = array();
    $s_action = "";
    $s_function = "";
    $s_function_param = "";
    $s_params = "";
    $s_field_name = "";

A100_INIT_VARS:
    $ar_line_details = explode("|",$ps_line);
    $s_field_name = $ar_line_details[2];
    $s_function_param = $ar_line_details[3];
    $sys_function_out = "?b600_unknown".$ps_line."?";
    $s_line_debug = $ar_line_details[4];

    $s_function = substr($s_function_param,0,strpos($s_function_param,"("));
    $s_params = substr($s_function_param,strpos($s_function_param,"("));
    $s_params = STR_REPLACE("#P","|",$s_params);
    $s_params = STR_REPLACE("#p","|",$s_params);
    $s_params = STR_REPLACE(",","^PARAM^",$s_params);
    $s_params = STR_REPLACE("(","",$s_params);
    $s_params = STR_REPLACE(")","",$s_params);
    $s_params = STR_REPLACE('#"',"^^#^",$s_params);
    $s_params = STR_REPLACE('"',"",$s_params);
    $s_params = STR_REPLACE("^^#^",'"',$s_params);
    $s_params = $class_main->clmain_v200_load_line( $s_params,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"ut_jcl b600a");
    $s_action = "RUN_FUNCTION";
    if (strtoupper(trim($s_field_name," ")) ==  strtoupper(trim("sys_app_function_list"," ")))
    {
        $s_action = "LIST_FUNCTIONS";
    }

    if (strtoupper(trim($s_field_name," ")) ==  strtoupper(trim("sys_app_function_help"," ")))
    {
        $s_action = "FUNCTIONS_HELP";
    }
//    echo "<br>s_function=".$s_function;
//    echo "<br>s_params=".$s_params;

B100_START:
    if (strtoupper($ar_line_details[0]) != "APP_CLASS")
    {
        goto Z900_EXIT;
    }
    if (strtoupper($ar_line_details[1]) != "V1")
    {
        goto A1_DO_V2;
    }
C100_DO_V1:
    $sys_function_out = $class_apps->clapp_a100_do_app_class($s_function,$s_params,$s_action,$ps_dbcnx,$s_line_debug,"ut_jcl b600b",$ps_details_def,$ps_details_data,$ps_sessionno);
    $sys_function_out = $class_main->clmain_v200_load_line($sys_function_out,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"ut_jcl b600c100");

//    echo "doing urlvalue<br>s_field_name=".$s_field_name."<br>s_param_name=".$s_param_name."<br>sys_function_out=".$sys_function_out."<br>";
A1_DO_V2:

Z900_EXIT:
    $sys_function_out = $s_field_name."|^%##%^|".$sys_function_out;
//    $sys_function_out = $s_field_name."|^%##%^|this is working".$sys_function_out;

    IF ($sys_debug == "YES"){z901_dump( $sys_function_name."  returned  = ".$sys_function_out." ");};

    return $sys_function_out;

}
//##########################################################################################################################################################################

function u900_write_text_file($ps_type, $ps_filename, $ps_text,$ps_archive_folder)
{
    $status = "ALLGOOD";
    $root = "/tdx_manage";
    $path =$root."/alerts/";

    if (strtoupper($ps_type) == "LOG")
    {
      $path = "c:\\tdx_archive\\".date("Y")."\\".date("m")."-".date("M")."\\".date("d")."\\".$ps_archive_folder."\\";
//      $path =$root."/log/".date("Y")."/".date("m")."-".date("M")."/".date("d"."/");
    }
//GW20100808     $path = $path.date("Y")."/".date("m")."-".date("M")."/".date("d"."/");

    if (!is_dir($path))
    {
        if(!mkdir($path,0,true))
        {
            die(" u900_write_text_file Failed to create the fodler ".$path." trying to write ".$ps_text." to file ".$ps_filename);
        }
    }
B100_WRITE:
    $myfile = $path.$ps_filename;
    $fh = fopen($myfile,'a') or die("cant open file".$myfile." trying to write text ".$ps_text);
    fwrite($fh,date("Y-m-d H:i:s",time())." - ".$ps_text."\r\n");
    fclose($fh);

Z900_EXIT:
    return $status;

}

?>
