<?php
function pf_customError($errno,$errstr,$errfile,$errline,$errcontext)
  {
// NOTE **** THIS ONLY CATCHES NON-FATAL ERRORS
//  echo "<b>Error gw12:</b> [$errno] $errstr $errfile $errline $errcontext.<br>";
        switch ($errno) {
        case E_USER_ERROR:
            echo "<b>My ERROR</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            echo "  Fatal error on line $errline in file $errfile";
            echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
            echo "Aborting...<br />\n";
            exit(1);
            break;

        case E_USER_WARNING:
            echo "<b>My WARNING</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        case E_USER_NOTICE:
            echo "<b>My NOTICE</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        default:
            echo "<BR>Unknown error type: [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;
        }

    /* Don't execute PHP internal error handler */
    return true;

  }


$ko_map_path="";

A000_SET_RUN:
     //set error handler
    set_error_handler("pf_customError", E_ALL);
    date_default_timezone_set('Australia/Brisbane');
    session_start();


    if (isset($_SESSION['ko_prog_path']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
    }else{
    $_SESSION['ko_prog_path'] = "";
    }
    if (isset($_SESSION['ko_dbase_to_connectto']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
    }else{
    $_SESSION['ko_dbase_to_connectto'] = "tdxpryda";
    }

    if (isset($_SESSION['ko_map_path']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
        $ko_map_path = $_SESSION['ko_map_path'];
    }else{
    $_SESSION['ko_map_path'] = "";
    }



A000_DEFINE_VARIABLES:
    $sys_prog_name = "";
    $sys_debug = '';
    $s_file_exists = '';
    $s_filename="";
    $get_map = "";
    $tmp_array = array();
    $sys_function_out = "";
    $s_post_action = "";

    $s_sessionno = "";
    $s_helplocation = "";
    $function_id = "";
    $get_userid = "";
    $s_details_def="";
    $s_details_data="";
    $s_siteparams = "";
    $s_url_siteparams = "";
    $s_usr_style_list = "";
    $s_usr_style_values= "";
    $username = "";
    $s_inaction ="";
    $s_temp = "";
    $results = "";
    $s_MultiLevelloop1 = "";
    $s_MultiLevelloop2 = "";
    $s_MultiLevelloop3 = "";
    $s_no_action_in_file = "";

    $sys_function_name = "";
    $sys_function_name = "main part of ut_action.php ";

A200_LOAD_LIBRARIES:
    $sys_debug = strtoupper("NO");
 //$sys_debug = strtoupper("yes");

     IF ($sys_debug == "YES"){echo $sys_prog_name." started debug=".$sys_debug."<br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_sql.php');
     $class_sql = new wp_SqlClient();
     IF ($sys_debug == "YES"){echo $sys_prog_name." after class_sql<br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_main.php');
     $class_main = new clmain();
     IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_main <br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_myplib1.php5');
     $class_myplib1 = new myplib1();
     IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_myplib1<br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_email.php');
     $class_email = new sys_clemail();
     IF ($sys_debug == "YES"){echo  $sys_prog_name." after clemail<br>";};

     require_once($_SESSION['ko_prog_path'].'lib/class_process_jobline.php');
     $class_clprocessjobline = new clprocessjobline();
     IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_process <br>";};


     require_once($_SESSION['ko_map_path'].'lib/class_app.php');
     $class_apps = new AppClass();
     IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_appclass<br>";};

     IF ($sys_debug == "YES"){echo "DEBUG IS ACTIVE - CHECK THE SCREEN SOURCE FOR FULL DETAILS or you will spend ages looking for stuff that is hidden<br><br>";};
     IF ($sys_debug == "YES"){echo "_get=";print_r($_GET);echo "<br>";};
     IF ($sys_debug == "YES"){echo "_post=";print_r($_POST);echo "<br>";};

A300_CONNECT_TODBASE:
     $dbcnx = $class_sql->c_sqlclient_connect();


A400_GET_PARAMETERS:
     if (isset($_GET['map'])) $get_map = $_GET["map"];
     if (isset($_GET['p'])) $s_url_siteparams = $_GET["p"];
     if (isset($_POST['ACTION'])) $s_post_action = $_POST["ACTION"];
     if (isset($_POST['params'])) $s_url_siteparams = $_POST["params"];
// gw 20100424 added the post params - this php should only be called as the action from a form so get wont work
//gw20110314 - added the action_alt
    if (isset($_POST['action_alt'])) $s_post_action = $_POST["action_alt"];

// gw 20131231    echo "<br>ut_action.php  POST VALUES<br>";
// gw 20131231    print_r($_POST);
// gw 20131231    echo "<br>ut_action.php  GET VALUES<br>";
// gw 20131231    print_r($_GET);
// gw 20131231    echo "<br>ut_action.php <br>";

$s_post_action = str_replace(" ","_",$s_post_action);
//gw20110206 - first 2 params become system parms - the rest are in site params
     $s_url_systemparams = $s_url_siteparams;
     $ar_url_params = explode("^",$s_url_siteparams);
     IF (count($ar_url_params) > 1)
     {
          $s_url_systemparams = $ar_url_params[0]."^".$ar_url_params[1]."";
     }

A500_SET_VALUES:
    $ar_line_details = array();
    $s_MultiLevelloop1 = "NONE";
    $s_MultiLevelloop2 = "NONE";
    $s_MultiLevelloop3 = "NONE";

    $get_map = $class_main->clmain_set_map_path_n_name($ko_map_path,$get_map);
    $sys_prog_name = "ut_action.php";
    IF ($sys_debug == "YES"){echo $sys_prog_name." ";};

    $s_file_exists='Y';
    if(file_exists($get_map))
    {
        $array_lines = file($get_map);
    }
    else
    {
        $s_file_exists='N';
        $sys_function_out =  "<br>".$sys_prog_name." ##### File Not Found-:".$get_map."<br>";
        GOTO Z900_EXIT;
    }
    $s_no_action_in_file = "The file ".$get_map." has no action code";


    IF ($sys_debug == "YES"){echo $sys_prog_name." after file exists check s_map_file_exists=".$s_file_exists."<br>";};
    IF ($sys_debug == "YES"){echo $sys_prog_name." number of lines in the file(array)=".count($array_lines)."<br>";};

    if (strpos($s_url_siteparams,"%!") > 0 )
    {
           $s_sessionno = "utmenu1";
            $_SESSION[$s_sessionno.'username'] = $username;
    }else{
            $s_sessionno = $class_main->clmain_get_param($s_url_siteparams,"s_sessionno","no");
    }
    //    echo "after file open<br>";
//gw20110313 - added user timezone
    if (isset($_SESSION[$s_sessionno.'ut_logon_timezone']))
    {
        $timezone=$_SESSION[$s_sessionno.'ut_logon_timezone'];
        date_default_timezone_set($timezone);
    }else{
    }


    //gw20100127       $s_sessionno = $class_main->clmain_get_param($s_url_siteparams,"s_sessionno","no");
    if (isset($_SESSION[$s_sessionno.'username']))
    {
        $username = $_SESSION[$s_sessionno.'username'];
    }else{
        $username = "Unknown ";
    }
    $s_details_def='START|USERNAME';
    $s_details_data='START|'.$username;
    $s_details_def.="|SITEPARAMS";
    $s_details_data.='|'.$s_url_siteparams;
    $s_details_def.="|SYSTEMPARAMS";
    $s_details_data.='|'.$s_url_systemparams;


    if (isset($_SESSION[$s_sessionno.'session_def']))
    {
        $s_details_def.=$_SESSION[$s_sessionno.'session_def'];
        $s_details_data.=$_SESSION[$s_sessionno.'session_data'];
    }
    $s_details_def.='|END';
    $s_details_data.='|END';
    $s_inaction ="N";

B090_ADD_POST_VALUES:
    foreach($_POST as $key => $value)
    {
        $s_details_def= $s_details_def."|POSTV_".$key;
        $s_details_data= $s_details_data."|".$value;
//    echo "<br>utaction postv=".$key." value ".$value;
    }
B099_END:

    $i = 0;
    $s_line_in = "";

B100_GET_REC:
    IF ($i >= count($array_lines))
    {
        goto Z900_END;
    }

    $s_line_in = $array_lines[$i];
    IF (substr($s_line_in,0,1) == "*")
    {
        goto Z800_GET_NEXT;
    }
    IF (substr($s_line_in,0,2) == "//")
    {
        goto Z800_GET_NEXT;
    }
    IF (trim($s_line_in) == "")
    {
//        echo "<br> utaction b100a blank line s_line_in=*".$s_line_in."*";
        goto Z800_GET_NEXT;
    }


    //gw20110211 - if not action in the post variable use the DEFAULT_ACTION from the script
    if (trim($s_post_action) == "" )
    {
//    echo "<br> utaction b100 s_line_in=*".$s_line_in."*";

        IF (strpos(strtoupper($s_line_in),strtoupper("DEFAULT_ACTION")) === false )
        {}else
        {
            IF ($sys_debug == "YES"){echo $sys_function_name." ".$i.".got the the default action line  ".$s_line_in."<br>";};
            $s_post_action = substr($s_line_in,strpos($s_line_in,"=")+1);
            $s_post_action = trim(str_replace("-","",$s_post_action));
            $s_post_action = trim(str_replace(">","",$s_post_action));
            goto Z800_GET_NEXT;
        }
    }
//gw20160203 - add default button option
    if (trim(strtolower($s_post_action)) == "default_action" )
    {
        IF (strpos(strtoupper($s_line_in),strtoupper("DEFAULT_ACTION")) === false )
        {}else
        {
            IF ($sys_debug == "YES"){echo $sys_function_name." ".$i.".got the the default action line v2  ".$s_line_in."<br>";};
            $s_post_action = substr($s_line_in,strpos($s_line_in,"=")+1);
            $s_post_action = trim(str_replace("-","",$s_post_action));
            $s_post_action = trim(str_replace(">","",$s_post_action));
            IF ($sys_debug == "YES"){echo $sys_function_name." ".$i.". action is now []".$s_post_action."[]<br>";};
            goto Z800_GET_NEXT;
        }
    }
// used to ignore the line
    IF (strpos(strtoupper($s_line_in),strtoupper("SKIP_LINE")) === false )
    {}else
    {
        IF ($sys_debug == "YES"){echo $sys_function_name." ".$i.".got the map skip line command ".$s_line_in."<br>";};
        goto Z800_GET_NEXT;
    }
    IF (substr(strtoupper($s_line_in),1,2) == "//")
    {
        IF ($sys_debug == "YES"){echo $sys_function_name." ".$i.".got the map skip line command ".$s_line_in."<br>";};
        goto Z800_GET_NEXT;
    }
// used to indicate if there is a start ACTION loop
    IF (strpos(strtoupper($s_line_in),strtoupper("START_ACTION_".$s_post_action."_HERE"))  === false )
    {}else
    {
        IF ($sys_debug == "YES"){echo $sys_function_name." ".$i.".got the map starter start line for action  ".$s_line_in."<br>";};
        $s_inaction ="Y";
//        echo "utaction start action s_post_action=[]".$s_post_action."[]";
        goto Z800_GET_NEXT;
    }
// used to indicate if there is a end  ACTION loop
    IF (strpos(strtoupper($s_line_in),strtoupper("END_ACTION_".$s_post_action."_HERE"))  === false  )
    {}else
    {
        IF ($sys_debug == "YES"){echo $sys_function_name." ".$i.".got the map end line for action  ".$s_line_in."<br>";};
        $s_inaction ="N";
//gw20150202 - exit the script at endofaction rather than keep reading                goto Z800_GET_NEXT;
        goto Z900_END;
    }
// if before the required loop get the next line
    if ($s_inaction =="N")
    {
// gw 20100315        IF ($sys_debug == "YES"){echo $sys_function_name." ".$i." not in action loop ".$s_line_in." <br>";};
        goto Z800_GET_NEXT;
    }
    $s_no_action_in_file = "";

    IF ($sys_debug == "YES"){echo $sys_function_name." ".$i." in action loop ".$s_line_in." <br>";};

    $ar_line_details = explode("|",$s_line_in);

    IF (strtoupper($ar_line_details[0]) == "DUMPTABLELISTTOSQLLOG")
    {
        $temp = $class_sql->c_sqlclient_list_tables_to_log($dbcnx);
        goto Z800_GET_NEXT;
    }
//***********************************************************************************************************************************************************
A100_START:

// gw 20131231 echo "gwut_action_ $ar_line_details[0]=[]".$ar_line_details[0]."[]<br>";

//*urlvalue|v1|result field name|url parameter name
//* add the result_field_name to the details_def with the url_parameter_name from the url
    IF (strtoupper($ar_line_details[0]) == "URLVALUE")
    {
        $s_temp = Pf_b500_do_url_value($dbcnx,$s_line_in,"no",$s_details_def,$s_details_data,$s_sessionno,$s_url_siteparams);
//        echo "doing urlvalue<br>".$s_temp."<br>";
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
        $s_temp = "";
        goto Z800_GET_NEXT;
    }
//***********************************************************************************************************************************************************
A200_START:
    IF (strtoupper($ar_line_details[0]) == "POSTVALUE")
    {
        $s_temp = Pf_b550_do_post_value($dbcnx,$s_line_in,"no",$s_details_def,$s_details_data,$s_sessionno,$s_url_siteparams);
//        echo "doing urlvalue<br>".$s_temp."<br>";
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
        $s_temp = "";
        goto Z800_GET_NEXT;
    }
//***********************************************************************************************************************************************************
//    * add_record |version|table name|ukeyname|=ukvalue or field name of the return uniquekey |debug|end
A300_START:
    IF (strtoupper($ar_line_details[0]) <> "ADD_RECORD")
    {
        GOTO A390_END;
    }
    $s_temp = $class_sql->sql_a100_add_record($dbcnx,$s_line_in,"no",$s_details_def,$s_details_data,$s_sessionno);
    IF (STRPOS($s_temp,"|^%##%^|")  === FALSE)
    {}ELSE{
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
        $s_temp = "";
    }
    goto Z800_GET_NEXT;
A390_END:
    //***********************************************************************************************************************************************************

//    * add_record |version|table name|ukeyname|=ukvalue or field name of the return uniquekey |debug|end
A3100_START:
    IF (strtoupper($ar_line_details[0]) <> "ADD_RECORD_AUTOKEY")
    {
        GOTO A3190_END;
    }
    $s_temp = $class_sql->sql_a110_add_record_autokey($dbcnx,$s_line_in,"no",$s_details_def,$s_details_data,$s_sessionno);
    IF (STRPOS($s_temp,"|^%##%^|")  === FALSE)
    {}ELSE{
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
        $s_temp = "";
    }
    goto Z800_GET_NEXT;
A3190_END:
    //***********************************************************************************************************************************************************

A400_START:

    IF (strtoupper($ar_line_details[0]) == "DO_URL")
    {
//    ECHO" utaction s_line_in   ".$s_line_in."<br>";
        $s_do_url =  $ar_line_details[1];
        $s_do_url = STR_REPLACE("#P","|",$s_do_url);
        $s_do_url = STR_REPLACE("#p","|",$s_do_url);
//    ECHO" utaction  do_url   ".$s_do_url."<br>";
        $s_do_url = $class_main->clmain_v200_load_line($s_do_url,$s_details_def,$s_details_data,"no",$s_sessionno,"ut_action do url");
//    ECHO" utaction  do_url   ".$s_do_url."<br>";
//    ECHO" utaction  do_url   ".$s_do_url."<br>";
//        die ("DEBUG_DIE  UT_ACTION_  a400start in utaction s_do_url=[]".$s_do_url."[]");
//
    if (!strpos(strtoupper($s_do_url),"BUGANDDIE")  === FALSE )
    {
    die ("DEBUG_DIE  UT_ACTION_  a400start in utaction s_do_url=[]".$s_do_url."[]");
    }

//          echo "<!-- pageheader!start -->";
        echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
        echo '<html>';
        echo '<head>';
        echo '<meta HTTP-EQUIV="REFRESH" content="0; url='.$s_do_url.'">';
        echo '</head>';
        echo '</html>';
        exit;

        goto Z800_GET_NEXT;
    }



A410_START:
    IF (strtoupper($ar_line_details[0]) <> "DO_URLIF")
    {
        GOTO A490_END;
    }

    IF (strtoupper($ar_line_details[1]) <> "V1")
    {
        GOTO A420_IFV2;
    }
    $s_field_to_check = $ar_line_details[2];
    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];
    $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl F200 ");
    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_F200 ");
    if ($s_true == "FALSE")
    {
        IF ($sys_debug == "YES"){z901_dump( $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);};
        $sys_function_out = "";
        GOTO Z800_GET_NEXT;
    }

    $s_temp_value = $ar_line_details[5];
    GOTO A430_DO;
A420_IFV2:
A430_DO:
//    ECHO" utaction s_line_in   ".$s_line_in."<br>";
        $s_do_url =  $ar_line_details[5];
        $s_do_url = STR_REPLACE("#P","|",$s_do_url);
        $s_do_url = STR_REPLACE("#p","|",$s_do_url);
//    ECHO" utaction  do_url   ".$s_do_url."<br>";
        $s_do_url = $class_main->clmain_v200_load_line($s_do_url,$s_details_def,$s_details_data,"no",$s_sessionno,"ut_action do url");
        echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
        echo '<html>';
        echo '<head>';
        echo '<meta HTTP-EQUIV="REFRESH" content="0; url='.$s_do_url.'">';
        echo '</head>';
        echo '</html>';
        exit;

        goto Z800_GET_NEXT;
A490_END:

//***********************************************************************************************************************************************************
A500_START:
// gw 20131231  echo "gwutaction a500_start  strtoupper(ar_line_details[0])=[]".strtoupper($ar_line_details[0])."[]";

IF (strtoupper($ar_line_details[0]) == "VALIDATE")
    {
//gw20130729        ECHO" validate 1 ".$s_line_in."<br>";
//gw20130729//gw20130729        echo " s_details_def=[]".$s_details_def."[]<br>";
//gw20130729        echo " s_details_data=[]".$s_details_data."[]<br>";

    $s_temp = pf_b900_validate_screen($dbcnx,$s_line_in,"no",$s_details_def,$s_details_data,$s_sessionno,$s_url_siteparams);

//gw20130729     ECHO" validate 2  temp ".$s_temp."<br>";

// gw 20131231    die ("gwut_action201");






    IF (STRPOS($s_temp,"|^%##%^|")  === FALSE)
        {}ELSE{
            $ar_line_details = explode("|^%##%^|",$s_temp);
            $s_details_def = $s_details_def."|".$ar_line_details[0];
            $s_details_data = $s_details_data."|".$ar_line_details[1];
            $s_temp = "";
//    ECHO" ADD_REC1 details_def ".$s_details_def."<br>";
//    ECHO" ADD_REC1 s_details_data ".$s_details_data."<br>";
        }
        goto Z800_GET_NEXT;
    }


// gw 20131231 die ("not validate at a500 start gwut_action201  strtoupper(ar_line_details[0])=[]".strtoupper($ar_line_details[0])."[]");




A600_START:
    IF (strtoupper($ar_line_details[0]) <> "SYS_EMAIL")
    {
        GOTO A690_END;
    }
A650_DO:
//gw20101002    ECHO" sysemail 1 ".$s_line_in."<br>";
        $s_temp = pf_c100_do_email($dbcnx,$s_line_in,"no",$s_details_def,$s_details_data,$s_sessionno,$s_url_siteparams);
//gw20101002    ECHO" sysemail 2  temp ".$s_temp."<br>";
        IF (STRPOS($s_temp,"|^%##%^|")  === FALSE)
        {}ELSE{
            $ar_line_details = explode("|^%##%^|",$s_temp);
            $s_details_def = $s_details_def."|".$ar_line_details[0];
            $s_details_data = $s_details_data."|".$ar_line_details[1];
            $s_temp = "";
        }

//gw20101002        die ("gw died me in a650 ut_action");
    goto Z800_GET_NEXT;

A690_END:
//***********************************************************************************************************************************************************
A700_START:

        GOTO A790_END;

//gw201105018 - move to process_jobline
/*
    IF (strtoupper($ar_line_details[0]) <> "UPDATE_RECORD")
    {
        GOTO A790_END;
    }

//    echo "<br>l_400_run<br>";
//    echo "<br>$s_line_in<br>";

    IF (strpos(strtoupper($s_line_in), "UPDATE_DATA_BLOB_WITH_DB_FIELDS") === false)
    {
        goto A750_SKIP_DATA_BLOB;
    }

//    echo "<br>l_400_run DUMP of def<br>";
//    echo $s_details_def."<br>";
//    echo "<br>l_400_run DUMP of data<br>";
//    echo $s_details_data."<br>";
//    echo "<br>l_400_run end of data <br>";

    $s_data_blob_fields = "";
A710_build_data_blob_fields:
//update_record|V1|jobs|jobs_id|%!_postv_jobs_id_!%|NO| UPDATE_DATA_BLOB_WITH_DB_FIELDS |end

// IF LINE HAS UPDATE_ALL_DATA_BLOB
// GET ALL THE POSTV VALUES WITH _DB_ IN THEM
    $array_def = explode("|",$s_details_def);
    $array_data = explode("|",$s_details_data);

    if (count($array_def)<> count($array_data))
    {
// do nothting yet gw20110116
    }
//    echo "<br>l_400_run list of posted fields and values <br>";
    for ($i2 = 0; $i2 < count($array_def); $i2++)
    {
        IF (strtoupper(substr($array_def[$i2],0,9)) == "POSTV_DB_")
        {
            $s_tmp = $array_def[$i2];
            $s_tmp = substr($s_tmp,strpos($s_tmp,"_DB_"));
            $s_tmp = str_replace("_DB_","",$s_tmp);
            $s_data_blob_fields = $s_data_blob_fields."|".$s_tmp."=".$array_data[$i2];
//            echo "<br>utaction l_400_run array_def[$i2]=".$array_def[$i2]."=".$array_data[$i2]."";
        }
    }
//    echo "<br>l_400_run end of list of posted fields and values <br>";

//    echo "<br>utaction l_400_run dump of datablob fields from the screen<br>";
//    echo $s_data_blob_fields."<br>";
//   echo "<br>l_400_run end of field dump<br>";


A720_get_original_data_blob:
// GET THE RECORD FOR THE KEY
//F400_RUNDATA:
    $s_orig_data_blob = "";
    $s_sql = "SELECt DATA_BLOB FROM  ".$ar_line_details[2]." where ".$ar_line_details[3]." = '".$ar_line_details[4]."'";
    $s_sql = STR_REPLACE("#P","|",$s_sql);
    $s_sql = STR_REPLACE("#p","|",$s_sql);
    $s_sql = $class_main->clmain_v200_load_line( $s_sql,$s_details_def,$s_details_data,"no",$s_sessionno, "ut_ACTION A720");
    $result = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql);
    $s_rec_found = "NO";
A725_DO_RECS:
    while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
    {
        foreach( $row as $key=>$value)
        {
            if (strpos(strtoupper($key),"DATA_BLOB") === FALSE)
            {}else
            {
                $s_orig_data_blob = $value;
                $s_rec_found = "YES";
            }
        }
    }

    if ($s_rec_found == "NO")
    {
       echo "<br>720_run no record found  s_sql".$s_sql."<br>";

    }
//    echo "<br>720_run s_orig_data_blob xml <br>";
//    echo $s_orig_data_blob."<br>";
//    echo "<br>720_run end of original datablob xml<br>";

// UPDATE THE RECORD_DATA_BLOB VALUE WITH THE SCREEN VALUES
A730_UPDATE_DATA_BLOB:
//    FOR EACH ENTRY CHECK_FIELD
//        DO THE CLMAIN_INSERT XML  -=WHICH WILL MODIFY THE VALUE IF IT EXITS
//    E
    $s_data_blob = $s_orig_data_blob;
//    echo "<br>720_run start of update of the data  blob";

    $array_data = explode("|",$s_data_blob_fields);
    for ($i2 = 0; $i2 < count($array_data); $i2++)
    {
        IF (strpos($array_data[$i2],"=") === false)
        {}else
        {
            $array_data2 = explode("=",$array_data[$i2]);
//            echo "<br>a730 = array_data2[0]".$array_data2[0]." array_data2[1]=".$array_data2[1]."";
            $s_data_blob = $class_main->clmain_u580_insert_field_xml($s_data_blob,"data_blob",$array_data2[0],$array_data2[1],"NO","uaction_730");
        }
    }
    $s_data_blob = str_replace("'","''",$s_data_blob);
    $s_upd_data_blob = " DATA_BLOB = '".$s_data_blob."'";
    $s_line_in = "UPDATE_RECORD|V1|".$ar_line_details[2]."|".$ar_line_details[3]."|".$ar_line_details[4]."|".$ar_line_details[5]."|".$s_upd_data_blob."|".$ar_line_details[7];
//    echo "<br>720_run end of update of the data  blob";
//    echo "<br>720_run s_upd_data_blob<br>";
//    echo $s_upd_data_blob."<br>";
//    echo "<br>720_run<br>";

//    die ("die in utACTION 720_run");

A750_SKIP_DATA_BLOB:
    $s_temp = $class_sql->sql_a200_update_record($dbcnx,$s_line_in,"no",$s_details_def,$s_details_data,$s_sessionno);
    IF (STRPOS($s_temp,"|^%##%^|")  === FALSE)
    {}ELSE{
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
        $s_temp = "";
    }
    goto Z800_GET_NEXT;
*/
A790_END:

A800_DO_APP_CLASS:
//*app_class|v1|result field name|application class function and paramaters
//* add the result_field_name to details_def after running the function in the app_class

//gw20161027 - move to process_jobline
//    IF (strtoupper($ar_line_details[0]) <> "APP_CLASS")
//    {
        GOTO A890_END;
//    }


/*
//die ( "gw die a800 doing app_class");


    $s_temp = Pf_b600_do_app_class($dbcnx,$s_line_in,"NO",$s_details_def,$s_details_data,$s_sessionno);
    if(strtoupper(trim($ar_line_details[4]," ")) == 'YES')
    {
       echo "<br>ut_action doing APP_CLASS - LINE ".$s_line_in."<br>".$s_temp."<br>";
    }
    $ar_line_details = explode("|^%##%^|",$s_temp);
    $s_details_def = $s_details_def."|".$ar_line_details[0];
    $s_details_data = $s_details_data."|".$ar_line_details[1];
    $s_temp = "";
    goto Z800_GET_NEXT;
*/
A890_END:

A900_DO_APP_CLASS:
//*app_class|v1|result field name|application class function and paramaters
//* add the result_field_name to details_def after running the function in the app_class
    IF (strtoupper($ar_line_details[0]) <> "SYS_CLASS")
    {
        GOTO A990_END;
    }

DIE("GWDIE UTACTION A900 DO SYS_CLASS - should be using class_process_jobline");


    $s_temp = $class_main->clmain_a110_do_sys_class($dbcnx,$s_line_in,"no",$s_details_def,$s_details_data,$s_sessionno);
    if(strtoupper(trim($ar_line_details[4]," ")) == 'YES')
    {
       echo "<br>doing SYS_CLASS - LINE ".$s_line_in."<br>".$s_temp."<br>";
    }
    $ar_line_details = explode("|^%##%^|",$s_temp);
    $s_details_def = $s_details_def."|".$ar_line_details[0];
    $s_details_data = $s_details_data."|".$ar_line_details[1];
    $s_temp = "";
    goto Z800_GET_NEXT;
A990_END:


F000_RUNDATA:
//rundata|SELECT * from sales_order where so_order_no = 544854|END
// adds the results of the sql to the end of run wide details def/data
    IF (strtoupper($ar_line_details[0]) == "RUNDATA")
    {
        GOTO F100_DO_RUNDATA;
    }
    IF (strtoupper($ar_line_details[0]) == "RUNDATAIF")
    {
        GOTO F200_DO_RUNDATAIF;
    }
    GOTO F900_END;
F100_DO_RUNDATA:
    $s_temp_value = $ar_line_details[1];
    GOTO F400_RUNDATA;
F200_DO_RUNDATAIF:
    IF (strtoupper($ar_line_details[1]) <> "V1")
    {
        GOTO F210_do_rundataif_v2;
    }
    $s_field_to_check = $ar_line_details[2];
    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];
    $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,$sys_debug,$s_sessionno,"ut_jcl F200 ");
    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utjcl_F200 ");
    if ($s_true == "FALSE")
    {
        IF ($sys_debug == "YES"){z901_dump( $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);};
        $sys_function_out = "";
        GOTO Z800_GET_NEXT;
    }

    $s_temp_value = $ar_line_details[5];
    GOTO F400_RUNDATA;
F210_do_rundataif_v2:
F400_RUNDATA:
    $s_temp_value = STR_REPLACE("#P","|",$s_temp_value);
    $s_temp_value = STR_REPLACE("#p","|",$s_temp_value);
    $s_temp = $class_main->clmain_u300_set_rundata($dbcnx,$s_temp_value,"no",$s_details_def,$s_details_data,$s_sessionno);
    $ar_line_details = explode("|^%##%^|",$s_temp);
    $s_details_def = $ar_line_details[0];
    $s_details_data = $ar_line_details[1];
    $s_temp = "";
    goto Z800_GET_NEXT;

F900_END:

    $s_line_in = str_replace("_CLPJL","",$s_line_in);

// gw20110502 - added this process to use the class
    $xml_arr  ="";
    $s_temp = $class_clprocessjobline->pf_clprocessjobline($dbcnx,$s_line_in,$s_details_def,$s_details_data,$xml_arr,$s_sessionno,"NO");
    if (strpos($s_temp,"|^%##%^|") !== false)
    {
//     echo "<br> do the split import clmain results=".$s_temp;
//  DIE ( "DIE UT_ACTION.PHP  NOT FALSE CALLING CLPROCESS_JOBLINE s_temp=[]".$s_temp."[]");
        $ar_line_details = explode("|^%##%^|",$s_temp);
        $s_details_def = $s_details_def."|".$ar_line_details[0];
        $s_details_data = $s_details_data."|".$ar_line_details[1];
    }
//  DIE ( "DIE UT_ACTION.PHP FALSE   CALLING CLPROCESS_JOBLINE s_temp=[]".$s_temp."[]");


/*
    IF (strtoupper($ar_line_details[0]) == "DRAW")
    {
        $sys_function_out .= pf_b100_DO_DRAW_HTML($class_main->clmain_set_map_path_n_name($ko_map_path,$ar_line_details[1]),$ar_line_details[2],"NO",$s_details_def,$s_details_data);
        goto Z800_GET_NEXT;
    }

//* DrawIF|v1|field|operator|value|map_name|loop|END
//DrawIF|v1|url_MODE|=|WELCOME|my_buddy_main.htm|welcome_map|END
    IF (strtoupper($ar_line_details[0]) == "DRAWIF")
    {
        $sys_function_out .= pf_b110_DO_DRAWIF_HTML($s_line_in,"NO",$s_details_def,$s_details_data,$s_sessionno);
        goto Z800_GET_NEXT;
    }

//dataloop|map|loop|Select * from devices |END
//* dataloop|owl_bdymain.htm|detail_loop|SELECT * FROM sales_order  as S_O where (so_order_status <> "90" and so_order_status <> "99" and so_order_status <> "91"and so_order_status <> "75") AND so_whse_code = "3" order by so_order_no desc LIMIT 0 , 50 |END
    IF (strtoupper($ar_line_details[0]) == "DATALOOP")
    {
//        $sys_function_out .= "BEFORE the dataloop<bR>";
        $ar_line_details[3] = STR_REPLACE("#P","|",$ar_line_details[3]);
        $ar_line_details[3] = STR_REPLACE("#p","|",$ar_line_details[3]);
        $sys_function_out .= Pf_b200_do_dataloop($dbcnx,$ar_line_details[3],$class_main->clmain_set_map_path_n_name($ko_map_path,$ar_line_details[1]),$ar_line_details[2],"no",$s_details_def,$s_details_data,$s_sessionno);
//            $sys_function_out .= "AFTER the dataloop<bR>";
        goto Z800_GET_NEXT;
    }



*
*/


//die ("<br> die ut_action unkown action line = ".$s_line_in);

Z800_GET_NEXT:
    $i = $i + 1;
    goto B100_GET_REC;

Z900_END:
    if (trim($s_no_action_in_file," ") <> "")
    {
        $sys_function_out .= "<BR><BR>ERROR - from ut.action.php **".$s_no_action_in_file." for action (".$s_post_action.")**<br>";
    }
Z900_EXIT:
     ECHO $sys_function_out;

?>


<?php
// ***********************************************************************************************************************************
// start functions
function pf_b100_DO_DRAW_HTML($ps_filename,$ps_group,$ps_debug,$ps_details_def,$ps_details_data)
{

    global $class_main;

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - pf_b100_DO_DRAW_HTML";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
//    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_in_line = ".$ps_in_line." ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};

    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;

//    $sys_function_out .= "gwdebug ut_action.php load ".$ps_group." from file ".$ps_filename."<br>";

    $sys_function_out .= $class_main->clmain_v100_load_html_screen($ps_filename,$s_details_def,$s_details_data,"no",$ps_group);

    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ";};
    IF ($sys_debug == "YES"){$sys_function_out.=" <!--".$sys_debug_text."-->";};


    return $sys_function_out;

}
//##########################################################################################################################################################################
function pf_b110_DO_DRAWIF_HTML($ps_line_in,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    global $class_main;
//* DrawIF|v1|field|operator|value|map_name|loop|debug|END

//DrawIF|v1|url_MODE|=|WELCOME|my_buddy_main.htm|welcome_map|no|END

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - pf_b100_DO_DRAW_HTML";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_in_line = ".$ps_line_in." ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};

A080_START:
    $ar_line_details = explode("|",$ps_line_in);
    if (strtoupper($ar_line_details[0]) != "DRAWIF")
    {
        goto Z900_EXIT;
    }
    if (strtoupper($ar_line_details[1]) != "V1")
    {
        goto A1_DO_V2;
    }

A100_INIT_VARS:
    $s_field_to_check = $ar_line_details[2];
    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];

    $s_map_name = $class_main->clmain_set_map_path_n_name("",$ar_line_details[5]);
    $s_map_group = $ar_line_details[6];
    $s_line_debug = $ar_line_details[7];

    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;

B100_START:
    $sys_function_out = "?b110_unknown".$ps_line_in."?";
    $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_action b1100");

C100_DO_V1:
    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utaction_b110");
    if ($s_true == "FALSE")
    {
        IF ($sys_debug == "YES"){z901_dump( $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);};
        $sys_function_out = "";
        GOTO Z900_EXIT;
    }

    $sys_function_out = $class_main->clmain_v100_load_html_screen($s_map_name,$s_details_def,$s_details_data,"NO",$s_map_group);

    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ";};
    IF ($sys_debug == "YES"){$sys_function_out.=" <!--".$sys_debug_text."-->";};


GOTO Z900_EXIT;

A1_DO_V2:

Z900_EXIT:
    return $sys_function_out;

}
//##########################################################################################################################################################################
function pf_b120_DO_ADD_DETAILS_IF($ps_line_in,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    global $class_main;
//* ADD_DETAILSIF|VERSION|FIELD|OPERATOR|VALUE|FIELDNAMES|VALUES|DEBUG|END


    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - pf_b120_DO_ADD_DETAILS_IF";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_in_line = ".$ps_line_in." ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};

A080_START:
    $ar_line_details = explode("|",$ps_line_in);
    if (strtoupper($ar_line_details[0]) != "ADD_DETAILSIF")
    {
        goto Z900_EXIT;
    }
    if (strtoupper($ar_line_details[1]) != "V1")
    {
        goto A1_DO_V2;
    }

A100_INIT_VARS:
    $s_field_to_check = $ar_line_details[2];
    $s_comparison_operator = $ar_line_details[3];
    $s_compare_value  = $ar_line_details[4];

    $s_line_debug = $ar_line_details[7];

    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;

B100_START:
    $sys_function_out = "?b120_unknown".$ps_line_in."?";
    $s_field_value = $class_main->clmain_v300_set_variable($s_field_to_check,$s_details_def,$s_details_data,"no",$ps_sessionno,"ut_action b120");

C100_DO_V1:
    $s_true =$class_main->clmain_v740_compare_values($s_field_value,$s_comparison_operator,$s_compare_value,"no","utaction_b120B");
    if ($s_true == "FALSE")
    {
        IF ($sys_debug == "YES"){z901_dump( $sys_debug_text." failed value ".$s_field_value." ".$s_comparison_operator." ".$s_compare_value);};
        $sys_function_out = "";
        GOTO Z900_EXIT;
    }

        $ar_line_details[5] = STR_REPLACE("#P","|",$ar_line_details[5]);
        $ar_line_details[5] = STR_REPLACE("#p","|",$ar_line_details[5]);
        $ar_line_details[6] = STR_REPLACE("#P","|",$ar_line_details[6]);
        $ar_line_details[6] = STR_REPLACE("#p","|",$ar_line_details[6]);
        $sys_function_out = $ar_line_details[5]."|^%##%^|".$ar_line_details[6];

    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ";};
    IF ($sys_debug == "YES"){$sys_function_out.=" <!--".$sys_debug_text."-->";};


GOTO Z900_EXIT;

A1_DO_V2:

Z900_EXIT:
    return $sys_function_out;

}
//##########################################################################################################################################################################
function Pf_b500_do_url_value($ps_dbcnx,$ps_line,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_url_siteparams)
{
    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - Pf_b500_do_url_value";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ");};

//    echo "start doing urlvalue<br>".$sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ";

    // define function specific variables and code
//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";};

//    $sys_function_out = "before globals";
//    return $sys_function_out;
//    EXIT;

    global $class_sql;
    global $class_main;
A001_DEFINE_VARS:
    $ar_line_details = array();
    $s_field_name = "";
    $s_param_name = "";
    $sys_function_out = "";
    $ar_url_siteparams = array();
    $ar_url_siteparams_vars = array();

A100_INIT_VARS:
    $ar_line_details = explode("|",$ps_line);
    $s_field_name = $ar_line_details[2];
    $s_param_name = $ar_line_details[3];
    $sys_function_out = "?b500_unknown?";
    $ar_url_siteparams = explode("^",$ps_url_siteparams);

    if (strtoupper($ar_line_details[0]) != "URLVALUE")
    {
        goto Z900_EXIT;
    }
    if (strtoupper($ar_line_details[1]) != "V1")
    {
        goto A1_DO_V2;
    }
B001_DO_V1:
    $i = 0;

B100_ENTRY:
    IF ($i >= count($ar_url_siteparams))
    {
        goto B900_END;
    }
//    echo "doing param loop param value =".$ar_url_siteparams[$i]."<br>";

    if (strpos($ar_url_siteparams[$i],"=") === false)
    {
//        echo "doing param loop param no equals sign<br>";
        goto B200_GET_NEXT;
    }
    $ar_url_siteparams_vars = explode("=",$ar_url_siteparams[$i]);
//    echo "doing param loop check value in =".$ar_url_siteparams[$i]." explode url param 0=".strtoupper(trim($ar_url_siteparams_vars[0]," "))." 1 = ".strtoupper(trim($ar_url_siteparams_vars[1]))."<br>";
//    echo "doing check values 0=".(strtoupper(trim($ar_url_siteparams_vars[0]," "))."  param =".strtoupper(trim($s_param_name," ")))."<br>";
    $s_value_chk =strtoupper(trim($ar_url_siteparams_vars[0]," "));
    $s_param_name_chk = strtoupper(trim($s_param_name," "));
//    echo "doing check values2 0=*".$s_value_chk."*  param =*".$s_param_name_chk."*<br>";
    if ($s_value_chk == $s_param_name_chk)
    {
//        echo "compare worked param 0=".strtoupper(trim($ar_url_siteparams_vars[0]," "))." 1 = ".strtoupper(trim($ar_url_siteparams_vars[1]))."<br>";
        $sys_function_out = $ar_url_siteparams_vars[1];
        goto Z900_EXIT;
    }

B200_GET_NEXT:
    $i = $i + 1;
    goto B100_ENTRY;

B900_END:
//    echo "doing urlvalue<br>s_field_name=".$s_field_name."<br>s_param_name=".$s_param_name."<br>sys_function_out=".$sys_function_out."<br>";
A1_DO_V2:

Z900_EXIT:
    $sys_function_out = $s_field_name."|^%##%^|".$sys_function_out;

    IF ($sys_debug == "YES"){z901_dump( $sys_function_name."  returned  = ".$sys_function_out." ");};

    return $sys_function_out;

}
//##########################################################################################################################################################################
function Pf_b550_do_post_value($ps_dbcnx,$ps_line,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_url_siteparams)
{
    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - Pf_b550_do_post_value";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ");};

//    echo "start doing urlvalue<br>".$sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ";

    // define function specific variables and code
//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";};

//    $sys_function_out = "before globals";
//    return $sys_function_out;
//    EXIT;

    global $class_sql;
    global $class_main;
A001_DEFINE_VARS:
    $ar_line_details = array();
    $s_field_name = "";
    $s_param_name = "";
    $sys_function_out = "";

A100_INIT_VARS:
    $ar_line_details = explode("|",$ps_line);
    $s_field_name = $ar_line_details[2];
    $s_param_name = $ar_line_details[3];
    $sys_function_out = "?b500_unknown?";

    if (strtoupper($ar_line_details[0]) != "POSTVALUE")
    {
        goto Z900_EXIT;
    }
    if (strtoupper($ar_line_details[1]) != "V1")
    {
        goto A1_DO_V2;
    }
B001_DO_V1:
    foreach($_POST as $key => $value)
    {
            if(strtoupper(trim($key," ")) == strtoupper(trim($s_param_name," ")))
            {
               $sys_function_out = $value;
            }
    }

B900_END:
//    echo "doing postvalue<br>s_field_name=".$s_field_name."<br>s_param_name=".$s_param_name."<br>sys_function_out=".$sys_function_out."<br>";
A1_DO_V2:

Z900_EXIT:
    $sys_function_out = $s_field_name."|^%##%^|".$sys_function_out;

    IF ($sys_debug == "YES"){z901_dump( $sys_function_name."  returned  = ".$sys_function_out." ");};

    return $sys_function_out;

}
//##########################################################################################################################################################################
// from ut_jcl 20100918
function Pf_b600_do_app_class($ps_dbcnx,$ps_line,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno)
{
    $sys_debug_text = "";
    $sys_debug = "";

    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - Pf_b600_do_app_class";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_debug_text." ".$sys_function_name."DEBUG VIEW SOURCE or log file FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."  called ".$sys_function_name."DEBUG IS WORKING");};
    IF ($sys_debug == "YES"){z901_dump( $sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ");};

//    echo "start doing urlvalue<br>".$sys_debug_text."=".$sys_function_name." ps_line = ".$ps_line."  ps_debug = ".$ps_debug."  ps_details_def = ".$ps_details_def."  ps_details_data = ".$ps_details_data." ";

    // define function specific variables and code
//EXAMPLE DEBUG LINE        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ".$i." paramater ".$i2." = ".$ar_line_details[$i2]." ";};

//    $sys_function_out = "before globals";
//    return $sys_function_out;
//    EXIT;

    global $class_sql;
    global $class_main;
    global $class_apps;

A001_DEFINE_VARS:
    $ar_line_details = array();
    $s_action = "";
    $s_function = "";
    $s_function_param = "";
    $s_params = "";
    $s_field_name = "";

A100_INIT_VARS:
    $ar_line_details = explode("|",$ps_line);
    $s_field_name = $ar_line_details[2];
    $s_function_param = $ar_line_details[3];
    $sys_function_out = "?b600_unknown".$ps_line."?";
    $s_line_debug = $ar_line_details[4];

    $s_function = substr($s_function_param,0,strpos($s_function_param,"("));
    $s_params = substr($s_function_param,strpos($s_function_param,"("));
    $s_params = STR_REPLACE("#P","|",$s_params);
    $s_params = STR_REPLACE("#p","|",$s_params);
    $s_params = STR_REPLACE(",","^PARAM^",$s_params);
    $s_params = STR_REPLACE("(","",$s_params);
    $s_params = STR_REPLACE(")","",$s_params);
    $s_params = STR_REPLACE('#"',"^^#^",$s_params);
    $s_params = STR_REPLACE('"',"",$s_params);
    $s_params = STR_REPLACE("^^#^",'"',$s_params);
    $s_params = $class_main->clmain_v200_load_line( $s_params,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"ut_jcl b600a");
    $s_action = "RUN_FUNCTION";
    if (strtoupper(trim($s_field_name," ")) ==  strtoupper(trim("sys_app_function_list"," ")))
    {
        $s_action = "LIST_FUNCTIONS";
    }

    if (strtoupper(trim($s_field_name," ")) ==  strtoupper(trim("sys_app_function_help"," ")))
    {
        $s_action = "FUNCTIONS_HELP";
    }
//    echo "<br>s_function=".$s_function;
//    echo "<br>s_params=".$s_params;

B100_START:
    if (strtoupper($ar_line_details[0]) != "APP_CLASS")
    {
        goto Z900_EXIT;
    }
    if (strtoupper($ar_line_details[1]) != "V1")
    {
        goto A1_DO_V2;
    }
C100_DO_V1:
    $sys_function_out = $class_apps->clapp_a100_do_app_class($s_function,$s_params,$s_action,$ps_dbcnx,$s_line_debug,"ut_action C100_DO_V1",$ps_details_def,$ps_details_data,$ps_sessionno);
    $sys_function_out = $class_main->clmain_v200_load_line($sys_function_out,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"ut_action C100_DO_V1");
//    echo "<br> ut_action  c100_d0_v1doapp=".$sys_function_out."<br>";

//    echo "doing urlvalue<br>s_field_name=".$s_field_name."<br>s_param_name=".$s_param_name."<br>sys_function_out=".$sys_function_out."<br>";
A1_DO_V2:

Z900_EXIT:
    $sys_function_out = $s_field_name."|^%##%^|".$sys_function_out;
//    $sys_function_out = $s_field_name."|^%##%^|this is working".$sys_function_out;

    IF ($sys_debug == "YES"){z901_dump( $sys_function_name."  returned  = ".$sys_function_out." ");};

    return $sys_function_out;

}
//##########################################################################################################################################################################
function pf_b900_validate_screen($p_dbcnx,$ps_line,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_url_siteparams)
{

    global $class_main;

    $class_main->clmain_v940_initialise_error_sessionvs("","NO","UT_ACTION B900");

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - ut_action.php pf_b900_validate_screen";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_in_line = ".$ps_line." ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};

    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;

//gw20130729   echo "pf_b900_validate_screen<br>";
//gw20130729    echo " ps_details_def=[]".$s_details_def."[]<br>";
//gw20130729    echo " ps_details_data=[]".$s_details_data."[]<br>";


B100_SET_FIELDS:
    $array_temp = array();
    $s_error_text = "";
    $s_error_list = "";
    $s_fail_url = "";

    $ar_line_fields = array();
    $ar_line_fields = explode("|",$ps_line);

    $s_action =  $ar_line_fields[1];
    $s_map = $ar_line_fields[2];
    $s_fail_url =  $ar_line_fields[3];
    $sys_debug =  strtoupper($ar_line_fields[4]);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        IF ($sys_debug == "YES"){echo $sys_function_name." validate pf_b900 line set DEBUG VIEW SOURCE FOR DETAILS<br>";};
        IF ($sys_debug == "YES"){echo $sys_debug_text.=" ".$sys_function_name." <br>ps_in_line = ".$ps_line." <br>ps_details_def = ".$ps_details_def."<br> ps_details_data ".$ps_details_data." <br>ps_debug = ".$ps_debug." ";};
    }

C100_DO_VALIDATE:
    IF ($sys_debug == "YES"){echo "<br>".$sys_function_name." prior to call  clmain_v900_validate_screen ";};
    $sys_function_out = $class_main->clmain_v900_validate_screen(" ",$s_action,$p_dbcnx,$s_map,$ps_details_def,$ps_details_data,$sys_debug,"ut_action b900");
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ";};
    IF ($sys_debug == "YES"){$sys_function_out.=" <!--".$sys_debug_text."-->";};
    IF ($sys_debug == "YES"){echo "<br> ".$sys_function_name." sys_function_out=".$sys_function_out;};

//gw20130729    echo "<br>ut_action v900 c100 sys_function_out".$sys_function_out;
//gw20130729    echo "pf_b900_validate_screen<br>";
//gw20130729    echo " ps_details_def=[]".$ps_details_def."[]<br>";
//gw20130729    echo " ps_details_data=[]".$ps_details_data."[]<br>";



    $array_temp = explode("|#&seperate&#|",$sys_function_out);
    $s_error_text = $array_temp[0];
    $s_error_list = $array_temp[1];
    if(trim($s_error_text," ") == "")
    {
        $sys_function_out = "ALL VALIDATED OK";
        IF ($sys_debug == "YES"){echo "<br> ".$sys_function_name."error_text is blank so sys_function_out".$sys_function_out;};
        GOTO Z900_EXIT;
    }

    $s_do_url =  $s_fail_url;
    $s_do_url = STR_REPLACE("#P","|",$s_do_url);
    $s_do_url = STR_REPLACE("#p","|",$s_do_url);

//gw20130729    echo "pf_b900_validate_screen  pre v200  <br>";
//gw20130729    echo " ps_details_def=[]".$ps_details_def."[]<br>";
//gw20130729    echo " ps_details_data=[]".$ps_details_data."[]<br>";
// gw 20131231    echo '<pre>' . print_r($_SESSION, TRUE) . '</pre>';

// gw 20131231    die ("gw_utaction b900 validate pre run url for error ");

    $s_do_url = $class_main->clmain_v200_load_line($s_do_url,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"ut_action b900 ");

echo "<br>gwutaction b900 SESSION['validate_error_html']=[]".$_SESSION['validate_error_html']."[]";



    IF ($sys_debug == "YES")
    {
        echo "<br> ".$sys_function_name." this process would now do url=".$s_do_url;
        goto Z900_EXIT;
    };

    header("Location: $s_do_url");
/* Redirect to a different page in the current directory that was requested */
//$host  = $_SERVER['HTTP_HOST'];
//$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
//$extra = 'mypagegw.php';
//header("Location: http://$host$uri/$extra");
exit;

Z900_EXIT:
    IF ($sys_debug == "YES")
    {
        echo "<br><br><br> ".$sys_function_name." validate debug is on so process has to die <br>";
        die;
    };

    return $sys_function_out;

}


//* sys_email|v1|return def|f_clemail_create_email(emailaddress,subject ,body map filename, body map group ,dodug,calledfrom,$ps_details_def,$ps_details_data)| fail url | do debug|END
//sys_email|v1|sys_email_message|f_clemail_create_email(#p%!_POSTV_partneremailaddress_!%#p,Invitation to become a MyPalletBook partner %!_POSTV_partnername_!%,my_buddy_main.htm,email_body,NO,sysset)|http://#p%!_server_HTTP_HOST_!%#p/als_web_prog/#p%!_SESSION_SUPER_ko_live_or_dev_!%#p/ut_menu.php?fnid=201&map=my_buddy_main.htm&p=1^mypid=#p%!_postV_mypalletid_!%#p^mode=EMAILERR,details_def,details_data|NO|END
//##########################################################################################################################################################################
function pf_c100_do_email($p_dbcnx,$ps_line,$ps_debug,$ps_details_def,$ps_details_data,$ps_sessionno,$ps_url_siteparams)
{

    global $class_main;
    global $class_email;

    $sys_debug_text = "";
    $sys_debug = "";
    $sys_debug = strtoupper($ps_debug);
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        $sys_debug_text = $ps_debug; // PS_DEBUG SHOULD CONTAIN THE NAME OF THE FUNTION CALLING THE DEBUG
    }
    $sys_function_name = "";
    $sys_function_name = "debug - pf_c100_do_email";
    $sys_function_out = "";
    IF ($sys_debug == "YES"){echo $sys_function_name."DEBUG VIEW SOURCE FOR DETAILS<br>";};
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_in_line = ".$ps_line." ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};

    $s_details_def = $ps_details_def;
    $s_details_data = $ps_details_data;
B100_SET_FIELDS:
    $array_temp = array();
    $s_error_text = "";
    $s_error_list = "";
    $s_fail_url = "";

    $ar_line_fields = array();
    $ar_line_fields = explode("|",$ps_line);

    $s_response_field = $ar_line_fields[2];
    $s_email_address =  $ar_line_fields[3];
    $s_subject =  $ar_line_fields[4];
    $s_body_map = $ar_line_fields[5];
    $s_body_group = $ar_line_fields[6];
    $s_fail_url =  $ar_line_fields[7];
    $sys_debug =  $ar_line_fields[8];
    IF ($sys_debug !="NO") {
        $sys_debug  = "YES";
        IF ($sys_debug == "YES"){echo $sys_function_name." line set DEBUG VIEW SOURCE FOR DETAILS<br>";};
        IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." ps_in_line = ".$ps_line." ps_details_def = ".$ps_details_def." ps_details_data ".$ps_details_data." ps_debug = ".$ps_debug." ";};
    }

    $s_attachment_list = "";
C100_DO:
//* sys_email|v1|sys_email_message|emailaddress|subject | body map | body group | fail url | debug |END
//sys_email|v1|sys_email_message|#p%!_POSTV_partneremailaddress_!%|Invitation to become a MyPalletBook partner %!_POSTV_partnername_!%|my_buddy_main.htm|email_body|http://#p%!_server_HTTP_HOST_!%#p/als_web_prog/#p%!_SESSION_SUPER_ko_live_or_dev_!%#p/ut_menu.php?fnid=201&map=my_buddy_main.htm&p=1^mypid=#p%!_postV_mypalletid_!%#p^mode=EMAILERR|NO|END
    $sys_function_out = $class_email->sys_clemail_p100_create_email($s_email_address,$s_subject,$s_body_map,$s_body_group,$sys_debug,"ut action c100 ",$ps_details_def,$ps_details_data,$ps_sessionno,$s_attachment_list);
    IF ($sys_debug == "YES"){$sys_debug_text.=" ".$sys_function_name." output = ".$sys_function_out." ";};
    IF ($sys_debug == "YES"){$sys_function_out.=" <!--".$sys_debug_text."-->";};

    if(trim($sys_function_out," ") <> "ERROR")
    {
        $sys_function_out = "ALL OK";
        GOTO Z900_EXIT;
    }

    $s_do_url =  $s_fail_url;
    $s_do_url = STR_REPLACE("#P","|",$s_do_url);
    $s_do_url = STR_REPLACE("#p","|",$s_do_url);
    $s_do_url = $class_main->clmain_v200_load_line($s_do_url,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"ut_action b900 ");

    header("Location: $s_do_url");
/* Redirect to a different page in the current directory that was requested */
//$host  = $_SERVER['HTTP_HOST'];
//$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
//$extra = 'mypagegw.php';
//header("Location: http://$host$uri/$extra");
exit;

Z900_EXIT:
    return $sys_function_out;

}

//##########################################################################################################################################################################
function z901_dump($ps_text)
{

       ECHO "ut_action_dump:".$ps_text."<br>";
/* gw 20110906 - no permissionbs
    $file_path  = "";
    if (isset($_SESSION['ko_map_path']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
        $file_path = $_SESSION['ko_map_path'];
    }

    $file_path = $file_path."logs";
    if (!file_exists($file_path))
    {
        $md= mkdir($file_path);
    }

    $dateym = date('Ym');
    $myfile = $file_path."\ut_action_".$dateym.".txt";
    $fh = fopen($myfile,'a') or die("cant open file".$myfile);
    fwrite($fh,date("Y-m-d H:i:s",time())." - ".$ps_text."\r\n");
    fclose($fh);
*/
}

?>
