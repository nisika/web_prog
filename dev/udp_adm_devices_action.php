<?php

function pf_customError($errno,$errstr,$errfile,$errline,$errcontext)
  {
// NOTE **** THIS ONLY CATCHES NON-FATAL ERRORS
//  echo "<b>Error gw12:</b> [$errno] $errstr $errfile $errline $errcontext.<br>";
// gw 20100315       switch ($error_level) {
       switch ($errno) {
        case E_USER_ERROR:
            echo "<b>My ERROR</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            echo "  Fatal error on line $errline in file $errfile";
            echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
            echo "Aborting...<br />\n";
            exit(1);
            break;

        case E_USER_WARNING:
            echo "<b>My WARNING</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        case E_USER_NOTICE:
            echo "<b>My NOTICE</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        default:
            echo "<BR>Unknown error type: [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;
        }

    /* Don't execute PHP internal error handler */
    return true;

  }

echo "<br>prog=[]".$_SESSION['ko_prog_path']."[]<br><br>";
print_r($_SESSION);
echo "<br>map=[]".$_SESSION['ko_map_path']."[]<br><br>";


$ko_map_path="./";
$ko_prog_path="./";

A000_SET_RUN:
     //set error handler
    set_error_handler("pf_customError", E_ALL);
    date_default_timezone_set('Australia/Brisbane');
    session_start();
echo "<br>3prog=[]".$_SESSION['ko_prog_path']."[]<br><br>";
print_r($_SESSION);
echo "<br>3map=[]".$_SESSION['ko_map_path']."[]<br><br>";


    if (isset($_SESSION['ko_prog_path']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
    }else{
    $_SESSION['ko_prog_path'] = "";
    }
    if (isset($_SESSION['ko_dbase_to_connectto']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
    }else{
    $_SESSION['ko_dbase_to_connectto'] = "tdxpryda";
    }

    if (isset($_SESSION['ko_map_path']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
        $ko_map_path = $_SESSION['ko_map_path'];
    }else{
    $_SESSION['ko_map_path'] = "";
    }

A000_DEFINE_VARIABLES:
    $sys_prog_name = "";
    $sys_debug = '';
    $s_file_exists = '';
    $s_filename="";
    $map = "";
    $tmp_array = array();
    $sys_function_out = "";
    $s_temp_value = "";

A200_LOAD_LIBRARIES:
    $sys_debug = strtoupper("NO");
//    $sys_debug = strtoupper("yes");

//gw20130809
$ko_map_path="./";

// /als_web_prog/dev/


echo "<br>prog=[]".$_SESSION['ko_prog_path']."[]<br><br>";
print_r($_SESSION);
echo "<br>map=[]".$_SESSION['ko_map_path']."[]<br><br>";
echo "<br>ko_map_path=[]".$ko_map_path."[]<br><br>";


     IF ($sys_debug == "YES"){echo $sys_prog_name." started debug=".$sys_debug." *** remember to view source - it will save you hours  <br>";};
     require_once($ko_map_path.'lib/class_sql.php');
     $class_sql = new wp_SqlClient();
     IF ($sys_debug == "YES"){echo $sys_prog_name." after class_sql<br>";};
     require_once($ko_map_path.'lib/class_main.php');
     $class_main = new clmain();
     IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_main <br>";};

     require_once($_SESSION['ko_map_path'].'lib/class_app.php');
     $class_apps = new AppClass();

     IF ($sys_debug == "YES"){echo "_get=";print_r($_GET);echo "<br>";};
     IF ($sys_debug == "YES"){echo "_post=";print_r($_POST);echo "<br>";};

A300_CONNECT_TODBASE:
     $dbcnx = $class_sql->c_sqlclient_connect();

    $s_action = "";
    $deviceId = "";

A400_GET_PARAMETERS:
     if (isset($_GET['action'])) $s_action = $_GET["action"];
     if (isset($_GET['DeviceId'])) $deviceId = $_GET["DeviceId"];

//     echo "<br>deviceId=".$deviceId;
//     echo "<br>s_action".$s_action;

A500_SET_VALUES:
    $s_action = strtoupper(trim($s_action," "));
    $deviceId = intval(trim($deviceId," "));

//     echo "<br>deviceId a=".$deviceId;
//     echo "<br>s_action a ".$s_action;
B100_PROCESS:
        if ($deviceId <= 0)
        {
            die("udp_adm_devices_action = invalid deviceId $deviceId");
        }

        if ($s_action == "APPROVE")
        {
            $newStatus = 'active';
            goto B200_UPDATE;
        }
        if ($s_action == "DEACTIVATE")
        {
            $newStatus = 'deactive';
            goto B200_UPDATE;
        }
        if ($s_action == "REACTIVATE")
        {
            $newStatus = 'active';
            goto B200_UPDATE;
        }
        if ($s_action == "LOST")
        {
            $newStatus = 'lost';
            goto B200_UPDATE;
        }
        if ($s_action == "FOUND")
        {
            $newStatus = 'active';
            goto B200_UPDATE;
        }
        if ($s_action == "RESENDJOBS")
        {
            // resets deliveries that are with the driver
            $sql = "UPDATE Deliveries SET DeliveryStatus='scheduled' WHERE AssignedToDeviceId=".$deviceId." AND (DeliveryStatus = 'withdriver' OR DeliveryStatus = 'senttodriver')";
            $result = mysql_query($sql,$dbcnx);
            if (!$result) {
                die('udp_adm_devices_action.php UPDATE FAILD - Invalid query: ' . mysql_error()."<br>sql=".$sql);
            }

            // marks delivered entries as delivered-end-of-day
            $sql = "UPDATE Deliveries SET DeliveryStatus='delivered_endofday' WHERE AssignedToDeviceId=".$deviceId." AND (DeliveryStatus = 'delivered')";
            $result = mysql_query($sql,$dbcnx);
            if (!$result) {
                die('udp_adm_devices_action.php UPDATE FAILD - Invalid query: ' . mysql_error()."<br>sql=".$sql);
            }

            // marks cancel pending entries as cancelled
            $sql = "UPDATE Deliveries SET DeliveryStatus='cancelled_confirmed' WHERE AssignedToDeviceId=".$deviceId." AND (DeliveryStatus = 'withdriver_cancelpending')";
            $result = mysql_query($sql,$dbcnx);
            if (!$result) {
                die('udp_adm_devices_action.php UPDATE FAILD - Invalid query: ' . mysql_error()."<br>sql=".$sql);
            }
            goto B200_UPDATE;
        }


        die("udp_adm_devices_action = unknown action of ".$s_action);

B200_UPDATE:
// gw 20100506   dont need company id     $sql = "SELECT Status FROM Devices WHERE CompanyId=".$companyid." AND DeviceId=".$deviceId;
        $sql = "SELECT Status FROM Devices WHERE DeviceId=".$deviceId;
//        $result = mysql_query($sql,$dbcnx);

            $result = mysql_query($sql,$dbcnx);
            if (!$result) {
                die('udp_adm_devices_action.php  - Invalid query: ' . mysql_error()."<br>sql=".$sql);
            }

        if ($deviceRecord = mysql_fetch_array($result, MYSQL_ASSOC))
        {
// gw 20100506   dont need company id              $sql = "UPDATE Devices SET Status='$newStatus'  WHERE CompanyId=".$companyid." AND DeviceId=".$deviceId." LIMIT 1";
            $sql = "UPDATE Devices SET Status='$newStatus' WHERE  DeviceId=".$deviceId." LIMIT 1";
//            mysql_query($sql,$dbcnx);

            $result = mysql_query($sql,$dbcnx);
            if (!$result) {
                die('udp_adm_devices_action.php UPDATE FAILD - Invalid query: ' . mysql_error()."<br>sql=".$sql);
            }


        }
        else
        {
            die("udp_adm_devices_action = no results for sql =".$sql);
            // no can do
        }

A900_END:
B000_START:


Z800_GET_NEXT:

Z700_END:
Z900_EXIT:

    $s_return_html = "";
    $s_return_html = $s_return_html.'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">';
    $s_return_html = $s_return_html.'<html><head>';
    $s_return_html = $s_return_html.'<meta http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT">';
    $s_return_html = $s_return_html.'<meta http-equiv="Pragma" content="no-cache">';
    $s_return_html = $s_return_html.'<meta http-equiv="Cache-Control" content="no-cache">';
    $s_return_html = $s_return_html.'<me//ta http-equiv="Content-Type" content="text/html; charset=utf-8">';
    $s_return_html = $s_return_html.'<meta http-equiv="Lang" content="en">';
    $s_return_html = $s_return_html.'<meta name="author" content="">';
    $s_return_html = $s_return_html.'<meta http-equiv="Reply-to" content="@.com">';
    $s_return_html = $s_return_html.'<meta name="generator" content="PhpED 5.8">';
    $s_return_html = $s_return_html.'<meta name="description" content="">';
    $s_return_html = $s_return_html.'<meta name="keywords" content="">';
    $s_return_html = $s_return_html.'<meta name="creation-date" content="01/01/2009">';
    $s_return_html = $s_return_html.'<meta name="revisit-after" content="15 days">';
    $s_return_html = $s_return_html.'<title>uDelivered Confirmed</title>';
    $s_return_html = $s_return_html.'<link rel="stylesheet" type="text/css" href="my.css">';
    $s_return_html = $s_return_html.'</head>';
    $s_return_html = $s_return_html.'<body>';
    $s_return_html = $s_return_html.'Task Confirmation<br> The task has been completed - please close this window';
    $s_return_html = $s_return_html.'</body>';
    $s_return_html = $s_return_html.'</html>';

    echo $s_return_html;

        // redirect to self to remove get variables
//gw20120719        header("Location: http://".$_SERVER['HTTP_HOST']."ut_menu.php?fnid=208&p=|%!start_SITEPARAMS_!end%|^mode=newrec");
        exit;
    ?>