<?php
function ut_kickoff_pf_customError($errno,$errstr,$errfile,$errline,$errcontext)
  {
// NOTE **** THIS ONLY CATCHES NON-FATAL ERRORS
//  echo "<b>Error gw12:</b> [$errno] $errstr $errfile $errline $errcontext.<br>";
        switch ($errno) {
//        case E_USER_ERROR://256
        case 256:
            echo "<b>My ERROR</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            echo "  Fatal error on line $errline in file $errfile";
            echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
            echo "Aborting...<br />\n";
            exit(1);
            break;

//        case E_USER_WARNING://512
        case 512:
            echo "<b>My WARNING</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

//        case E_USER_NOTICE://1024
        case 1024:
            echo "<b>My NOTICE</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        default:
            echo "Unknown error type: [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;
        }
    /* Don't execute PHP internal error handler */
    return true;

  }

  function pf_set_sessionid($ps_loginname)
{
// print_r($_SESSION);
    $sSessionid = "501";
    $sCheckSid = $sSessionid."udSid";
    while (isset($_SESSION[$sCheckSid])):
        $sSessionid = $sSessionid + 1;
        $sCheckSid = $sSessionid."udSid";
        IF ($sSessionid > 508 ) // want to keep the session no at a single digit
        {
//            echo "error setting id";
            break;
        }
        IF ($sSessionid > 520 )
        {
            echo "way too many  exit ";
            exit;
        }
    endwhile;
    $_SESSION[$sCheckSid] = $sCheckSid;
    return $sSessionid;
}


  function pf_load_dma_vars($ps_junk)
  {
//      echo $_SESSION["ko_from_dma_link_done"];
//      die ("gw utkickoff pf load dma");


      if(!isset($_SESSION["ko_from_dma_link_done"]))
      {
          GOTO B100_SET;
      }
      if($_SESSION["ko_from_dma_link_done"] == "YES_".$_SESSION["valid_user"]);
      {
          GOTO z900_exit;
      }
B100_SET:
      $_SESSION["ko_from_dma_link_done"] = "YES_".$_SESSION["valid_user"];
      foreach($_SESSION as $key=>$value)
      {
          if(strpos($key,"ko_") === false)
          {
              $_SESSION["ko_from_dma_".$key] = $value;
          }
      }
 z900_exit:
  }



  //ko_prog_path = "als_web_prog/dev/";

  A000_SET_RUN:
     //set error handler
  if(isset ($_SESSION['dma_to_wp_n_dmav2'])===false){
      set_error_handler("ut_kickoff_pf_customError", E_ALL);
      date_default_timezone_set('Australia/Brisbane');
  }

    IF(SESSION_ID() == ""){
        session_start();
    }

A000_DEFINE_VARIABLES:
    $s_kickoff_ini = "";
    $ar_line_details = array();
    $run_path = "";
    $init_page = "";
    $init_process = "";
    $s_curr_path = "";
    $s_url_siteparams = "";
    $s_url_siteparams = "";
    $s_sessionno = "";
    $s_siteparams = "";
    $sys_function_out = "";
    $ini_file = "";
    $class_library = "";
    $function_name = "";
    $class_name = "";
    $stmp = "";

 A200_LOAD_LIBRARIES:
    $sys_debug = strtoupper("NO");
    if(file_exists("ut_kickoff_do_debug.yes"))
    {
        $sys_debug = strtoupper("YES");
    }
    $sys_prog_name = "ut_kickoff.php";

/*
     IF ($sys_debug == "YES"){echo $sys_prog_name." started debug=".$sys_debug."<br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_sql.php');
     $class_sql = new wp_SqlClient();
     IF ($sys_debug == "YES"){echo $sys_prog_name." after class_sql<br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_main.php');
     $class_main = new clmain();
     IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_main <br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_myplib1.php5');
     $class_myplib1 = new myplib1();
     IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_myplib1<br>";};
     IF ($sys_debug == "YES"){echo "_get=";print_r($_GET);echo "<br>";};
     IF ($sys_debug == "YES"){echo "_post=";print_r($_POST);echo "<br>";};

A300_CONNECT_TODBASE:
     $dbcnx = $class_sql->c_sqlclient_connect();
*/
A400_GET_PARAMETERS:
  $ini_file = "kickoff.ini";
// the dma_to_wp ( dmav2 ) values have already been set
     if(isset ($_SESSION['dma_to_wp_n_dmav2'])){
        if ($_SESSION['dma_to_wp_n_dmav2'] == "YES") {
            if(strpos($_SESSION['dma_to_wp_ini_file'],"/") !==false){
                $ini_file = $_SESSION['dma_to_wp_ini_file'];
            }else{
                $run_path = $_SESSION['dma_to_wp_sitepath'];
                $ini_file = $_SESSION['dma_to_wp_ini_file'];
            }
//
//            echo "<BR>1".$_SESSION['dma_to_wp_n_dmav2'] ;
//            echo "<BR>2".$_SESSION['dma_to_wp_path'];
//            echo "<BR>3".$_SESSION['dma_to_wp_sitepath'] ;
//            echo "<BR>4".$_SESSION['dma_to_wp_ini_file'] ;
//            echo "<BR>5".$_SESSION['dma_to_wp_jcl_mappath'] ;
//            echo "<BR>6".$_SESSION['dma_prog_path'] ;

            goto A500_SET_VALUES;
        }
     }
/*
  echo "<h3> PHP List All Session Variables</h3>";
  foreach ($_SESSION as $key=>$val)
      echo $key."=[] ".$val."[]<br/>";

  echo "<br> doing the dmave_ini process";


// the dma_to_wp ( dmav2 ) values are required but not set so need to be set from the siteini file
*/
  $s_do_init_dmav2 = "NO";
  if (isset($_GET['tv2']))
      {
//          ECHO "<BR> todmav2 is set=[]".$_GET['tv2']."[]";
          $s_do_init_dmav2 = "YES";
      }
  //if (isset($_from_ut_jcl))
 // {
 //     ECHO "<BR> _from_ut_jcl is set=[]".$_from_ut_jcl."[]";
 //     $s_do_init_dmav2 = "YES";
 // }

  if ($s_do_init_dmav2 == "NO")
  {
      GOTO A450_USE_GETS;
  }
// read the site ini file and set the dma_to_wp values
//          echo "<br>doing the dmave_ini process";

  $file_name="site-details/site_config.ini";
  $file_name= $_SESSION['site_path']."site-details/site_config.ini";
  if(file_exists($file_name))
  {
      $ini = file($file_name);
  }
  else
  {
      echo "<br><br> ut_kickoff  File Not Found dma site_details site_config.ini file_name=[]".$file_name."[]<br><br>*****";
  }

  IF(SESSION_ID() == ""){
      session_start();
  }

  foreach($ini as $key=>$val)
  {
      if(!strpos($val, "|")===false)
      {
          $line = explode("|",$val);
          $type = $line[1];
          $type = trim(str_replace("*","",$type));
//          echo "<br><br> ut_kickoff  val =[]".$val."[] type=[]".$type."[]*****";
          switch(strtoupper($type)) {
              case "DMA_TO_WEBPROG_YN" :
                  $_SESSION['dma_to_wp_n_dmav2'] = trim($line[2]);
                  break;
              case "DMA_TO_WEBPROG_PATH" :
                  $_SESSION['dma_to_wp_path'] = trim($line[2]);
                  break;
              case "DMA_TO_WEBPROG_SITEPATH" :
                  $_SESSION['dma_to_wp_sitepath'] = trim($line[2]);
                  break;
              case "DMA_TO_WEBPROG_SITEINI" :
                  $_SESSION['dma_to_wp_ini_file'] = trim($line[2]);
                  break;
              case "DMA_TO_WEBPROG_JCL_MAPPATH" :
                  $_SESSION['dma_to_wp_jcl_mappath'] = trim($line[2]);
                  break;
  //            case "DMA_PROG_PATH" :
  //                $_SESSION['dma_prog_path'] = trim($line[2]);
  //                break;
          }
      }
  }
/*
  echo "<h3> PHP List All Session Variables</h3>";
  foreach ($_SESSION as $key=>$val)
      echo $key."=[] ".$val."[]<br/>";
*/

  $run_path = $_SESSION['dma_to_wp_sitepath'];
  $ini_file = $_SESSION['dma_to_wp_ini_file'];

  goto A500_SET_VALUES;

A450_USE_GETS:
     if (isset($_GET['path'])) $run_path = $_GET["path"];

     if (isset($_GET['inifile'])) $ini_file = $_GET["inifile"];
     if (isset($_GET['init'])) $init_page = $_GET["init"];
     if (isset($_GET['p'])) $s_url_siteparams = $_GET["p"];
     if (isset($_GET['init_process'])) $init_process = $_GET["init_process"];
     if (isset($_GET['debug'])) $sys_debug = strtoupper($_GET["debug"]);
     if (isset($_GET['class_library'])) $class_library = $_GET["class_library"];
     if (isset($_GET['class_name'])) $class_name = $_GET["class_name"];
     if (isset($_GET['function_name'])) $function_name = $_GET["function_name"];

     IF ($sys_debug == "YES"){echo $sys_prog_name." run_path =".$run_path."----<br>";};
     IF ($sys_debug == "YES"){echo $sys_prog_name." init_page =".$init_page."----<br>";};
     IF ($sys_debug == "YES"){echo $sys_prog_name." s_url_siteparams =".$s_url_siteparams."----<br>";};
     IF ($sys_debug == "YES"){echo $sys_prog_name." init_process =".$init_process."----<br>";};
      IF ($sys_debug == "YES"){echo $sys_prog_name." ini_file =".$ini_file."----<br>";};
      IF ($sys_debug == "YES"){echo $sys_prog_name." class_library =".$class_library."----<br>";};
      IF ($sys_debug == "YES"){echo $sys_prog_name." class_name =".$class_name."----<br>";};
      IF ($sys_debug == "YES"){echo $sys_prog_name." function_name =".$function_name."----<br>";};

//   DIE ("DEBUG DIE IN UT_KICKOFF");

A500_SET_VALUES:
    IF ($sys_debug == "YES"){echo $sys_prog_name."  Doing debug <BR>";};

    $s_kickoff_ini = $run_path.$ini_file;

 // die("gwdie utkickoff s_kickoff_ini=[]".$s_kickoff_ini."[]");


    $s_curr_path = getcwd();

    $s_file_exists='Y';
    if(file_exists($s_kickoff_ini))
    {
        $array_lines = file($s_kickoff_ini);
        IF ($sys_debug == "YES"){echo $sys_prog_name." a500_ Processing file = ".$s_kickoff_ini."<br>";};
    }
    else
    {
        $s_file_exists='N';
        IF ($sys_debug == "YES"){echo $sys_prog_name." a500_ cannot file file  = ".$s_kickoff_ini."<br>";};
//        echo "<br>gwdebug ut_kickoff not kickoff_ini=[]".$s_kickoff_ini."[]";
//if no kickoff file keep working and wait for something else file fail which it may not
//        $sys_function_out =  "<br>".$sys_prog_name." ##### File Not Found-:_curr_path=".$s_curr_path." looking for file=".$s_kickoff_ini."<br>";
        GOTO Z900_EXIT;
    }
    IF ($sys_debug == "YES"){echo $sys_prog_name." after file exists check ini exists=".$s_file_exists."<br>";};
    IF ($sys_debug == "YES"){echo $sys_prog_name." number of lines in the file(array)=".count($array_lines)."<br>";};


    if (strpos($s_url_siteparams,"%!") > 0 )
    {
           $s_sessionno = "1";
            GOTO A600_SKIP;
    }
    if(!isset($_SESSION["ko_from_dma_link_done"]))
    {
      GOTO A550_SET;
    }
    if($_SESSION["ko_from_dma_link_done"] == "YES_".$_SESSION["valid_user"]);
    {
      GOTO A600_SKIP;
    }
A550_SET:

    $s_sessionno = pf_set_sessionid("utkickoff");
A600_SKIP:
//gw20110921 - added user timezone
    if (isset($_SESSION[$s_sessionno.'ut_logon_timezone']))
    {
        $timezone=$_SESSION[$s_sessionno.'ut_logon_timezone'];
        date_default_timezone_set($timezone);
    }else{
    }


    $i = 0;
    $s_line_in = "";

B100_GET_REC:
    IF ($i >= count($array_lines))
    {
        goto B900_END;
    }
    $s_line_in = $array_lines[$i];
    IF (substr($s_line_in,0,1) == "*")
    {
        goto B200_GET_NEXT;
    }
// set site session variables
    $ar_line_details = explode("|",$array_lines[$i]);
    IF ($sys_debug == "YES"){echo $sys_prog_name."b100_ Processing line ".$i." of ".count($array_lines)." = ".$array_lines[$i]."<br>";};
//SET_SUPERSESSION_VAR|ko_dbase_userid|dma_session_varname_db_username|END

    IF (strtoupper($ar_line_details[0]) == "SET_SESSION_VAR")
    {
//        $_SESSION[$s_sessionno.$ar_line_details[1]] = $ar_line_details[2];
//            IF ($sys_debug == "YES"){echo $sys_prog_name." set_session_var = ".$ar_line_details[1]." to ".$ar_line_details[2]."<br>";};
//        goto B200_GET_NEXT;
        if(strpos(($ar_line_details[2]),"dma_session_varname_") === false)
        {
            $_SESSION[$s_sessionno.$ar_line_details[1]] = $ar_line_details[2];
            goto B200_GET_NEXT;
        }
//        echo "<br><br>ar_line_details[2])[]".$ar_line_details[2]."[]";
//        echo "<br>ar_)[]".str_replace("dma_session_varname_","",$ar_line_details[2])."[]";
        $_SESSION[$s_sessionno.$ar_line_details[1]] = $_SESSION[str_replace("dma_session_varname_","",$ar_line_details[2])];
        goto B200_GET_NEXT;
    }
    IF (strtoupper($ar_line_details[0]) == "SET_SUPERSESSION_VAR")
    {
        if(strpos(($ar_line_details[2]),"dma_session_varname_") === false)
        {
            $_SESSION[$ar_line_details[1]] = $ar_line_details[2];
            goto B200_GET_NEXT;
        }
//        echo "<br><br>ar_line_details[2])[]".$ar_line_details[2]."[]";
//        echo "<br>ar_)[]".str_replace("dma_session_varname_","",$ar_line_details[2])."[]";

        if(!isset($_SESSION[str_replace("dma_session_varname_","",$ar_line_details[2])]))
        {
            $s_die_message = "";
            $s_die_message = $s_die_message."<br>GWDIE - ut_kickoff.php set dma session varname";
            $s_die_message = $s_die_message."<br>Cannot set ".str_replace("dma_session_varname_","",$ar_line_details[2]);
            $s_die_message = $s_die_message."<br><br>Available Session vars";

            foreach ($_SESSION as $key=>$val)
                $s_die_message = $s_die_message."<br> ".$key." = []".$val."[]";

            $s_die_message = $s_die_message."<br><br>tdX - The End";
            die($s_die_message);
// dump all session vars
        }
        $_SESSION[$ar_line_details[1]] = $_SESSION[str_replace("dma_session_varname_","",$ar_line_details[2])];
        goto B200_GET_NEXT;
    }
    IF (strtoupper($ar_line_details[0]) == "SET_SUPERCOOKIE_VAR")
    {
       setcookie($ar_line_details[1],$ar_line_details[2]);
        goto B200_GET_NEXT;
    }
B200_GET_NEXT:
    $i = $i + 1;
    goto B100_GET_REC;

B900_END:
  IF ($sys_debug == "YES"){echo $sys_prog_name."<br>b900 end of file processing ";};

  if(isset ($_SESSION['dma_to_wp_n_dmav2'])) {
      if ($_SESSION['dma_to_wp_n_dmav2'] == "YES") {
          IF ($sys_debug == "YES"){echo $sys_prog_name."<br>b900 goto z900";};
          goto Z900_EXIT;
//   dont run any web page just exit and return to cl0100_comm_session_init.php
      }
      IF ($sys_debug == "YES"){echo $sys_prog_name."<br>b900dma_to_wp_n_dmav2 not YES";};
  }else{
      IF ($sys_debug == "YES"){echo $sys_prog_name."<br>b900dma_to_wp_n_dmav2 is not set";};
  }

C100_DO_WEBPAGE:

    IF ($sys_debug == "YES"){echo $sys_prog_name."C100_DO_WEBPAGE "."<br>";};

    $init_page = str_replace("@amp@","&",$init_page);

    IF ($sys_debug == "YES"){echo $sys_prog_name."C100 init_page ".$init_page."<br>";};
//gw20100627 - changed t ocheck for the siteparams exist
//    if (strpos($init_page,"&") === false)

    $s_siteparam_exists = "NO";
    IF ($sys_debug == "YES"){echo $sys_prog_name." siteparam exists pre tests ".$s_siteparam_exists."<br>";};
    if (strpos($init_page,"&p=") === false)
    {}ELSE
    {$s_siteparam_exists = "YES";}
    if (strpos($init_page,"?p=") === false)
    {}ELSE
    {$s_siteparam_exists = "YES";}

    IF ($s_siteparam_exists <> "YES")
//    if (strpos($init_page,"&p=") === false)
    {
        $s_siteparams = 'p='.$s_sessionno."^";
        IF ($sys_debug == "YES"){echo $sys_prog_name."C100 no siteparams  IN ".$init_page."<br>";};
    }else{
//gw20100627        $s_siteparams = '';
        IF ($sys_debug == "YES"){echo $sys_prog_name."C100 has sitparams  IN ".$init_page."<br>";};
        IF ($sys_debug == "YES"){echo $sys_prog_name."C100 has sitparams  start at ".strpos($init_page,"&p=")."<br>";};
        $s_siteparams = substr($init_page,strpos($init_page,"&p=")+3);
        IF ($sys_debug == "YES"){echo $sys_prog_name."C100 has sitparams  OF  ".$s_siteparams."<br>";};
        $s_siteparams = 'p='.$s_sessionno."^".substr($init_page,strpos($init_page,"&p=")+3);
        $init_page = substr($init_page,0,strpos($init_page,"&p="));
    }

//gw20100630 - are there any parameters at all - put ? or & in accordingly
    if (strpos($init_page,"?") === false)
    {
        $s_siteparams = '?'.$s_siteparams;
        IF ($sys_debug == "YES"){echo $sys_prog_name."C101 siteparams are the only parameters IN ".$init_page."<br>";};
    }else{
        $s_siteparams = '&'.$s_siteparams;
        IF ($sys_debug == "YES"){echo $sys_prog_name."C101 siteparams are additionaly parameters IN ".$init_page."<br>";};
    }



    IF ($sys_debug == "YES"){echo $sys_prog_name." init_process=".$init_process."<br>";};
    IF ($sys_debug == "YES"){echo $sys_prog_name." init_page=".$init_page."<br>";};
    IF ($sys_debug == "YES"){echo $sys_prog_name." s_siteparams=".$s_siteparams."<br>";};

    IF ($sys_debug == "YES"){echo " Debug causes the ut_kickoff to die before going to the init page.<br>";DIE;};


    IF (STRTOUPPER(TRIM($init_process," "))!="WEBPAGE")
    {
        GOTO C900_END;
    }
    $sys_function_out = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">';
    $sys_function_out .= '<html>';
    $sys_function_out .= '<head>';
    $sys_function_out .= '<title>loading kickoff</title>';
    $sys_function_out .= '<meta http-equiv="REFRESH" content="0;url='.$init_page.$s_siteparams.'"></HEAD>';
    $sys_function_out .= '<BODY>';
    $sys_function_out .= 'utk-Redirecting';
    $sys_function_out .= '</BODY>';
    $sys_function_out .= '</HTML>';
    goto Z900_EXIT;
C900_END:

    IF (STRTOUPPER(TRIM($init_process," "))!="RUNCLASS")
    {
        GOTO D900_END;
    }

     IF ($sys_debug == "YES"){echo $sys_prog_name." started debug=".$sys_debug." *** remember to view source - it will save you hours  <br>";};
     require_once($_SESSION['ko_prog_path'].'lib\\class_sql.php');
     $class_sql = new wp_SqlClient();
     IF ($sys_debug == "YES"){echo $sys_prog_name." after class_sql<br>";};
     require_once($_SESSION['ko_prog_path'].'lib\\class_main.php');
     $class_main = new clmain();
     IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_main <br>";};
     require_once($_SESSION['ko_map_path'].'lib\\class_app.php');
     $class_apps = new AppClass();


//    echo "<br> class_library = ".$class_library." class_name = ".$class_name." function_name=".$function_name;
    // load the library
     IF ($sys_debug == "YES"){echo $sys_prog_name." started debug=".$sys_debug." *** remember to view source - it will save you hours  <br>";};
//     echo "<br> pre required<br>";
     require_once($_SESSION['ko_map_path'].'lib/'.$class_library) ;
//     echo "<br> pre nerw<br>";
     $class_kickoff = new class_kickoff();
//     echo "<br> pre function<br>";
// run the class
    $result = $class_kickoff->$function_name();
//     echo "<br> finished<br>";
     die ("gw utkickoff");
D900_END:
  IF (STRTOUPPER(TRIM($init_process," "))!="DMALINK")
  {
      GOTO E100_END;
  }

  $stmp = pf_load_dma_vars($stmp);

  $sys_function_out = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">';
  $sys_function_out .= '<html>';
  $sys_function_out .= '<head>';
  $sys_function_out .= '<title>dmalink</title>';
  $sys_function_out .= '<meta http-equiv="REFRESH" content="0;url='.$init_page.$s_siteparams.'"></HEAD>';
  $sys_function_out .= '<BODY>';
  $sys_function_out .= 'utk-Redirecting';
  $sys_function_out .= '</BODY>';
  $sys_function_out .= '</HTML>';
  goto Z900_EXIT;

E100_END:
  IF (STRTOUPPER(TRIM($init_process," "))!="DMALINKSDUMP")
  {
      GOTO E200_END;
  }

  $stmp = pf_load_dma_vars($stmp);

E110_SKIP1:
  $sys_function_out = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">';
  $sys_function_out .= '<html>';
  $sys_function_out .= '<head>';
  $sys_function_out .= '<title>DMALINKSDUMP</title>';
  $sys_function_out .= '<meta http-equiv="REFRESH" content="0;url='.$init_page.$s_siteparams.'"></HEAD>';
  $sys_function_out .= '<BODY>';

  echo " START DMALINKSDUMP <br>";
  foreach($_SESSION as $key=>$value)
  {
        echo " Session Var ".$key." = ".$value."<br>";
  }

  $sys_function_out .= 'utk-Redirecting';
  $sys_function_out .= '</BODY>';
  $sys_function_out .= '</HTML>';
  echo " END DMALINKSDUMP <br>";
  die ("gw utkickoff");

  goto Z900_EXIT;

E200_END:


  foreach($_SESSION as $key=>$value)
  {
//           echo $key." =".$value." <br>";
      if (strtoupper($s_session_var)==strtoupper($key))
      {
          $sys_function_out = $value;
          $s_var_found = "YES";
      }
      if (strtoupper($ps_sysfield)=="DUMPVARS")
      {
          echo " Session Var ".$key." = ".$value."<br>";
          $s_var_found = "YES";
      }
      if (strtoupper($ps_sysfield)=="DUMPVARS_OUT")
      {
          $sys_function_out = $sys_function_out." Session Var ".$key." = ".$value."<br>";
          $s_var_found = "YES";
      }
  }


  X800_NO_ACTION:
  $sys_function_out = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">';
  $sys_function_out .= '<html>';
  $sys_function_out .= '<head>';
  $sys_function_out .= '<title>?'.$init_process.'</title>';
  $sys_function_out .= '<BODY>';
  $sys_function_out .= 'Unknown Initial process of '.(STRTOUPPER(TRIM($init_process," ")));
  $sys_function_out .= '</BODY>';
  $sys_function_out .= '</HTML>';
  ECHO $sys_function_out;
  die ("gw utkickoff");

  Z900_EXIT:
//print_r($sys_function_out);
     ECHO $sys_function_out;

?>
