<?php

// includes
require_once('DBFunctions.php');
require_once('TemplateFunctions.inc.php');

// user auth
require_once('DoUserAuth.php');

if (isset($_POST['action']))
{
	if ($_POST['action'] = 'ADDDELIVERY')
	{
		// adds entry
		$queryValueArray = array(
								 'CompanyId'=>$companyId,
								 'CreatedByUserId'=>$userId,
								 'DeliveryStatus'=>'scheduled',
								 'Version'=>time(),
								 'ScheduledForDate'=>time(),
								 'DateAdded'=>time(),
								 'Source'=>'website',
								 'CNNO'=>''.$_POST['CNNO'],
								 'Receiver'=>''.$_POST['Receiver'],
								 'PhoneNo'=>''.$_POST['PhoneNo'],
								 'AddrLine1'=>''.$_POST['AddrLine1'],
								 'AddrLine2'=>''.$_POST['AddrLine2'],
								 'Suburb'=>''.$_POST['Suburb'],
								 'Postcode'=>''.$_POST['Postcode'],
								 'Note'=>''.$_POST['Note'],
								 'Items'=>''.$_POST['Items'],
 								 'Timezone'=>36000 // TODO: set to company timezone
							 );

		// if a device was specified, validates it, and adds
		if (intval($_POST['AssignToDevice']) != -1)
		{
			$sql = "SELECT DeviceId From Devices WHERE CompanyId=$companyId AND DeviceId=".intval($_POST['AssignToDevice'])." AND Status='active'";
			$result = exec_query($sql);
			if (mysql_num_rows($result) == 1)
			{
				$queryValueArray['AssignedToDeviceId'] = intval($_POST['AssignToDevice']);
			}
			else
			{
				die("invalid DeviceId: ".intval($_POST['AssignToDevice']));
			}
			
			$queryValueArray['DeliveryStatus'] = 'scheduled';
		}
		else
		{
			$queryValueArray['DeliveryStatus'] = 'unassigned';	
		}
		
				
		$queryNames = "";
		$queryValues = "";
		foreach ($queryValueArray as $key => $value)
		{
			if ($queryNames != "")
				$queryNames .= ', ';
			$queryNames .= '`'.$key.'`';
			
			if ($queryValues != "")
				$queryValues .= ', ';
				
			if (is_string($value))
			{
				$queryValues .= '\''.mysql_real_escape_string($value).'\'';
			}
			else
			{
				$queryValues .= $value;	
			}
		}
			
		$sql = "INSERT INTO Deliveries ($queryNames) VALUES ($queryValues)"; 
		exec_query($sql);
		$globalDeliveryId = mysql_insert_id();	
		
		if ($globalDeliveryId <= 0)
		{
			die("invalid globalDeliveryId".$globalDeliveryId);
		}
		
		echo "Delivery Added.<br /><br />";
	}
}

template_header("Add Delivery");
?>

<form name="adddelivery" action="?" method="post">
<input type="hidden" name="action" value="ADDDELIVERY">

CNNO: <input type="text" name="CNNO" /><br />
Assign to Device:
<select name="AssignToDevice">
<?php
$sql = "SELECT COUNT(DeviceId) From Devices WHERE CompanyId=$companyId AND Status='active'";
$result = exec_query($sql);
$activeDeviceCount = 0;
$notYetSelected = true;

if ($row = mysql_fetch_array($result, MYSQL_NUM))
{
	$activeDeviceCount = $row[0];
}

if ($activeDeviceCount != 1)
{
	echo '<option value="-1" selected>(none)</option>';
	$notYetSelected = false;
}
else
{
	echo '<option value="-1">(none)</option>';	
}
			
?>
<?php
$sql = "SELECT * From Devices WHERE CompanyId=$companyId AND Status='active'";
$result = exec_query($sql);
while ($deviceRecord = mysql_fetch_array($result, MYSQL_ASSOC))
{
	$selectedText = "";
	if ($notYetSelected)
	{
		$selectedText = " selected";
		$notYetSelected = true;
	}
	
	echo '<option value="'.$deviceRecord['DeviceId'].'"'.$selectedText.'>'.htmlspecialchars($deviceRecord['DeviceName']).'</option>';
}
?>
</select>
 (if a device does not appear in this list it may not have been approved)<br />
Receiver: <input type="text" name="Receiver" /><br />
PhoneNo: <input type="text" name="PhoneNo" /><br />
AddrLine1: <input type="text" name="AddrLine1" /><br />
AddrLine2: <input type="text" name="AddrLine2" /><br />
Suburb: <input type="text" name="Suburb" /><br />
Postcode: <input type="text" name="Postcode" /><br />
Item Description: <input type="text" name="Items" /><br />
Note:<br/ >
<textarea cols="50" rows="4" name="Note"></textarea><br/ >
<br/ >
<input type="submit" value="Submit" />
</form>

<br />
<br />
<a href=".">Back to Manage Me main screen</a>

<?php

template_footer();