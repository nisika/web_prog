<?php

// quick check to see if the user is authenticate them - if so, redirects to manageme.php
require_once("DBFunctions.php");
require_once('AuthFunctions.php');
if (authCookieExists())
{
	redirectToRelativeURLAndExit("manageme.php");
}

// if the user access this page from their iPhone, checks if they have sent login info
if (isset($_GET['action']) && $_GET['action'] == "IPLOGIN")
{
	require('DoUserAuth.php');
}


?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>udelivered.com - take your work to the world </title>
<link href="udset.css" rel="stylesheet" type="text/css">
<meta name="description" content="uDelivered the best mobile solutions">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
<body bgcolor="#FFFFFF" text="#333333" link="#0099CC" vlink="#3399CC" alink="#FF0000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" height="100%"  border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
  <tr>
    <td><div align="center">
    <table width="800" border="0" cellspacing="0" cellpadding="0">
          <tr height="50"><td>&nbsp;</td></tr>
          <tr>
          <td width="450" align="center" class="heading">People have talked for years about being road warriors and being able to work outside the office.<br>
          <br>
          With the iPhone the talk can stop.<br>
          <br>
          With uDelivered the shouting can begin!</td>
          <td width="50" class="heading">&nbsp;<br></td>
          <td width="300" class="text" align="center"><img src="images/udelivered_proofb.jpg"></td>
          </tr>
          <tr>
          <td colspan="3" valign="top" bgcolor="#FFFFFF">
                <form method='post' action='manageme.php'>
                <input type="hidden" name="action" value="LOGIN">
                <input type="hidden" name="postloginredirect" value="<?php echo $_GET['postloginredirect']; ?>">
                <table width="100%" border="0" cellspacing="0" cellpadding="20">
                 <tr valign="top" >
                    <td align="left"  width= "600" class="text"> <a href="udp_index.html"><img src="images/udp_proof_btn.png"></a><br>
                      <span class="headingproof">
                    <img src="images/ud_12.png">As a person on the road having to deliver a service or some goods you are the ultimate road warrior.<br>
                    <img src="images/ud_12.png">Until now the cost and hassle of providing immediate and irrefutable proof of your work has been expensive and complicated.<br>
                    <img src="images/ud_12.png">The iPhone and uDelivered combine to create the most cost effective and powerful solution for your on the road requirements.<br>
                    <img src="images/ud_12.png">Download job details from the web portal or enter them directly on the iPhone.<br>
                    <img src="images/ud_12.png">Enter the Proof you have delivered, capture the signature, allocate the work to a client, upload to the web, immediate customer service and billing recording.<br>
                    <img src="images/ud_12.png">All details, events, activty, asset details are immediatly pushed to the web for your office to manage and perhaps your clients to see.<br>
                    <img src="images/ud_12.png">That's just the beginning of the features of uDelivered.Proof, <a href="udp_index.html">check out the full feature list</a><br>
                    </span></td>
                    <td align="right"  width= "200" class="text">
                    <table>
                         <tr>
                            <td align="right" class="text">Email Address:</td>
                            <td><input type="text" size="18" maxlength="128" name="user_email"></td>
                        </tr>
                          <tr>
                            <td align="right" class="text">Password:</td>
                            <td><input type="password" size="18" maxlength="128" name="password"></td>
                          </tr>
                          <tr>
                            <td align="right" class="text">&nbsp;</td>
                            <td><input type='submit' value='LOGON' name='Submit'><br /></td>
                          </tr>
                  </table>
                  </td>
                  </tr>
                </table>
                </form>

          </td>
          </tr>
          <tr>
          <td height="25" >
          <table width="100%" border="0" cellspacing="0" cellpadding="10">
              <tr>
                <td><div align="center"><font class="text" style="color:#6fbf49;">Copyright &copy; 2009 udelivered - designed by tdX</font></div></td>
              </tr>
            </table></td>
          </tr>
        </table>
        </div>
        </td>
    <td valign="top" class="righttd"><div class="righttd_bg">&nbsp;</div></td>
  </tr>
</table>
<map name="header"><area shape="rect" coords="10,71,223,186" href="http://www.austworkplacemgt.com.au/" target="_blank">
</map>
</body>
</html>