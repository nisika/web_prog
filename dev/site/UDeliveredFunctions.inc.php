<?php

require_once('DBFunctions.php');


function getStatusDescriptionForStatus($status)
{
	if ($status == "unassigned")
	{
		return "Unassigned";
	}
	else if ($status == "scheduled")
	{
		return "Scheduled";
	}
	else if ($status == "senttodriver")
	{
		return "Sent to Driver (unconfirmed)";
	}
	else if ($status == "withdriver")
	{
		return "With Driver (confirmed)";
	}
	else if ($status == "delivered")
	{
		return "Delivered";
	}
	else if ($status == "delivered_endofday")
	{
		return "Delivered, record locked.";
	}
	else if ($status == "withdriver_cancelpending")
	{
		return "Cancel&nbsp;Pending (with Driver)";
	}
	else if ($status == "cancelled_confirmed")
	{
		return "Cancelled";
	}
}

function getDescriptionForDeviceStatus($status)
{
	if ($status == "pending_approval")
	{
		return "Pending Approval";
	}
	else if ($status == "active")
	{
		return "Active";
	}
	else if ($status == "deactive")
	{
		return "Deactive";
	}
	else if ($status == "lost")
	{
		return "Reported Lost";
	}
	else if ($status == "reassigned")
	{
		return "Reassigned";
	}
}

function getStatusColorForStatus($status)
{
	
	
}