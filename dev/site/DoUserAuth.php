<?php

/*
 * Includes
 */
require_once('DBFunctions.php');
require_once('AuthFunctions.php');


$userId = -1;
$companyId = -1;

// Login related processing
if (isset($_POST['action']) || isset($_GET['action']))
{
	if ($_POST['action'] == "LOGIN" || $_GET['action'] == "IPLOGIN")
	{
		$user_email = $_POST['user_email'];
		$passwordMD5 = md5($_POST['password']);
		
		if ($_GET['action'] == "IPLOGIN")
		{
			$user_email = $_GET['user_email'];
			$passwordMD5 = strtolower($_GET['ph']);
		}
		
		// gets UserId
		$result = exec_query("SELECT * FROM Users WHERE Email='".mysql_real_escape_string($user_email)."'");
		
		if ($row = mysql_fetch_array($result, MYSQL_ASSOC))
		{
			// check password, if wrong, reply with message with a payload that opens the settings screen.
			$userId = intval($row['UserId']);
			$companyId = intval($row['CompanyId']);
			
			if (isset($row['TemporaryPassword']) && $passwordMD5 == $row['TemporaryPassword'])
			{
				// login with temporary password - sets as actual
				$sql = "UPDATE Users SET Password='".$row['TemporaryPassword']."', TemporaryPassword=NULL WHERE UserId=".$userId." LIMIT 1";
				exec_query($sql);	
			}
			else if ($passwordMD5 != $row['Password'])
			{
				// TODO: should show password screen if not showing
				die("Incorrect password for '$user_email'.");//, "INCORRECT_PASSWORD");
			}
			else
			{
				// login with password - clears TemporaryPassword (if any)
				$sql = "UPDATE Users SET TemporaryPassword=NULL WHERE UserId=".$userId." LIMIT 1";
				exec_query($sql);	
			}
		}
		else
		{
			die("Unknown email address '$user_email', please sign up from the App, or <a href=\"index.php\">try a different email address</a>.");//, "UNKNOWN_USER");
		}
		
		
		setcookie("LoginUserId", $userId);
		setcookie("LoginPwd", $passwordMD5);
		
		// redirects to self
		if ($_GET['action'] == "IPLOGIN")
		{
			redirectToRelativeURLAndExit("manageme.php");
		}
		else
		{
			if (isset($_POST['postloginredirect']) && $_POST['postloginredirect'] != "")
			{
				redirectAndExit("http://".$_SERVER['HTTP_HOST'].$_POST['postloginredirect']);
			}
			else
			{
				redirectToRelativeURLAndExit("manageme.php");
			}
		}
	}
	
	if ($_POST['action'] == "FORGOTPASSWORD")
	{
		$email = $_POST['email'];
		$password = $_POST['password'];
		
	}
}


// Session authentication
if ($userId <= 0 && isset($_COOKIE['LoginUserId']))
{
	$testUserId = $_COOKIE['LoginUserId'];
	$passwordmd5 = $_COOKIE['LoginPwd'];
	
	// gets UserId
	$result = exec_query("SELECT * FROM Users WHERE UserId=".intval($testUserId)."");
	
	if ($row = mysql_fetch_array($result, MYSQL_ASSOC))
	{
		// check password, if wrong, reply with message with a payload that opens the settings screen.
		$userId = intval($row['UserId']);
		$companyId = intval($row['CompanyId']);
		
		if ($passwordmd5 != $row['Password'])
		{
			setcookie("LoginUserId","",time() - 60*60);
			setcookie("LoginPwd","",time() - 60*60);
			
			die("Bad Session: Incorrect password for '$user_email'. Please <a href=\"index.php\">try again</a>.");//, "INCORRECT_PASSWORD");
		}
	}
	else
	{
		setcookie("LoginUserId","",time() - 60*60);
		setcookie("LoginPwd","",time() - 60*60);
		
		die("Bad Session: Unknown email address '$user_email', please <a href=\"index.php\">sign up</a>.");//, "UNKNOWN_USER");
	}	
}

// if user is not authenticated, redirects them to the authentication page (saving the current page for return)
if ($userId <= 0)
{
	redirectToRelativeURLAndExit("index.php?postloginredirect=".urlencode($_SERVER['REQUEST_URI']));
}
	
if ($companyId <= 0)
{
	die("Error 0x80001003: invalid $companyId: ".$companyId);
}


