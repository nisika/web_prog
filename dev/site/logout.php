<?php

// includes
require_once('DBFunctions.php');
require_once('TemplateFunctions.inc.php');

// user auth
require_once('DoUserAuth.php');

setcookie("LoginUserId","",time() - 60*60);
setcookie("LoginPwd","",time() - 60*60);


template_header("Log out");
?>

You are now logged out.

<?php

template_footer();