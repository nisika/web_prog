<?php

// includes
require_once('DBFunctions.php');
require_once('TemplateFunctions.inc.php');
require_once('UDeliveredFunctions.inc.php');

// user auth
require_once('DoUserAuth.php');


if (isset($_GET['action']))
{
	if ($_GET['action'] == 'CANCEL' || $_GET['action'] == 'UNCANCEL')
	{
		$globalEntryId = intval($_GET['GlobalDeliveryId']);
		if ($globalEntryId <= 0)
		{
			die("invalid globalEntryId $globalEntryId");
		}
		
		$sql = "SELECT GlobalDeliveryId, DeliveryStatus, AssignedToDeviceId FROM Deliveries WHERE CompanyId=$companyId AND GlobalDeliveryId=$globalEntryId";
		$result = exec_query($sql);
		
		if ($deliveryRecord = mysql_fetch_array($result, MYSQL_ASSOC))
		{
			$newStatus = NULL;
			
			if ($_GET['action'] == 'CANCEL')
			{
				if ($deliveryRecord['DeliveryStatus'] == 'senttodriver' || $deliveryRecord['DeliveryStatus'] == 'withdriver')
				{
					// it doesn't matter if the driver never actually got the original entry (i.e. it's stuck on senttodriver) because they will download the cancelled entry anyway...
					$newStatus = 'withdriver_cancelpending';
				}
				else if ($deliveryRecord['DeliveryStatus'] == 'unassigned' || $deliveryRecord['DeliveryStatus'] == 'scheduled')
				{
					$newStatus = 'cancelled_confirmed';
				}
				else
				{
					die("cannot cancel in status ".$deliveryRecord['DeliveryStatus']);
				}
			}
			if ($_GET['action'] == 'UNCANCEL')
			{
				if ($deliveryRecord['DeliveryStatus'] == 'withdriver_cancelpending')
				{
					$newStatus = 'senttodriver';  // marked as sent-to-driver, because the ACK status is unknown
				}
				else if ($deliveryRecord['DeliveryStatus'] == 'cancelled_confirmed')
				{
					if (isset($deliveryRecord['AssignedToDeviceId']))
					{
						$newStatus = 'scheduled';
					}
					else
					{
						$newStatus = 'unassigned';
					}
				}
				else
				{
					die("cannot uncancel in status ".$deliveryRecord['DeliveryStatus']);
				}
			}

			if ($newStatus != NULL)
			{
				$sql = "UPDATE Deliveries SET DeliveryStatus='$newStatus', Version=".time()." WHERE CompanyId=$companyId AND GlobalDeliveryId=$globalEntryId LIMIT 1";
				exec_query($sql);
			}
		}
		else
		{
			// if GlobalDeliveryId was given, it should be correct!
			
			die("invalid record index supplied '$globalEntryId'");
		}
	}
	else if ($_GET['action'] == "ASSIGN")
	{
		$globalEntryId = intval($_GET['GlobalDeliveryId']);
		if ($globalEntryId <= 0)
		{
			die("invalid globalEntryId $globalEntryId");
		}
		$deviceId = intval($_GET['DeviceId']);
		if ($deviceId == 0)
		{
			die("invalid deviceId $deviceId");
		}
		
		$sql = "SELECT GlobalDeliveryId, DeliveryStatus, Source FROM Deliveries WHERE CompanyId=$companyId AND GlobalDeliveryId=$globalEntryId";
		$result = exec_query($sql);
		
		if ($deliveryRecord = mysql_fetch_array($result, MYSQL_ASSOC))
		{
			$invalid = !($deliveryRecord['DeliveryStatus'] == "unassigned" || $deliveryRecord['DeliveryStatus'] == "scheduled" || $deliveryRecord['DeliveryStatus'] == "cancelled_confirmed");
			$invalid |= $deliveryRecord['Source'] != "website";
						   
			if (!$invalid)
			{
				if ($deviceId == -1)
				{
					$sql = "UPDATE Deliveries SET DeliveryStatus='unassigned', Version=".time().", AssignedToDeviceId=NULL WHERE CompanyId=$companyId AND GlobalDeliveryId=$globalEntryId LIMIT 1";
					exec_query($sql);
				}
				else
				{
					$sql = "SELECT DeviceId From Devices WHERE CompanyId=$companyId AND DeviceId=$deviceId";
					$result = exec_query($sql);
					if ($deliveryRecord = mysql_fetch_array($result, MYSQL_ASSOC))
					{
						$sql = "UPDATE Deliveries SET DeliveryStatus='scheduled', Version=".time().", AssignedToDeviceId=$deviceId WHERE CompanyId=$companyId AND GlobalDeliveryId=$globalEntryId LIMIT 1";
						exec_query($sql);
					}
					else
					{
						$invalid = true;
					}
				}
			}

			if ($invalid)
			{
				die("invalid");
			// no can do  
			}
						   
		}
		else
	    {
					die("invalid");
			   // no can do
	    }
						   
	}

	// redirect to self to remove get variables
	header("Location: http://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);

	echo "Action complete";
	
	exit;
}
	

template_header("Manage Me");

// gets the device info, exits if not active
$result = exec_query("SELECT DeviceId FROM Devices WHERE CompanyId=$companyId AND Status = 'pending_approval'");
if (mysql_num_rows($result) > 0)
{
	echo "<span class=\"newdevices\">Note: Your company has newly registered devices that need <a href=\"devices.php\">your approval</a>.</span>";
	echo "<br /><br />";
}
		
	
	
?>

<h2>Menu</h2>

<a href="devices.php">Devices</a><br />
<a href="adddelivery.php">Add Delivery</a><br />
<a href="logout.php">Logout</a><br />
<br /><br />


<h2>Your Deliveries</h2>

<table>
    <tr class="deliverytable">
    	<td>
		</td>
        <td class="tableheading">
            CNNO
		</td>
		<td class="tableheading">
        	Status
		</td>
		<td class="tableheading">
			Device
		</td>
		<td class="tableheading">
        	Deliver To
		</td>
		<td class="tableheading">
        	Note
		</td>
		<td class="tableheading">
        	Recieved By
		</td>
		<td class="tableheading">
        	Delivery Proof
		</td>
        <td class="tableheading">
        	Actions
		</td>
	</tr>

<?php


$sql = "SELECT * From Deliveries INNER JOIN Companies ON Deliveries.CompanyId=Companies.CompanyId LEFT OUTER JOIN Devices ON Deliveries.AssignedToDeviceId=Devices.DeviceId WHERE Deliveries.CompanyId=$companyId";
$result = exec_query($sql);

while ($deliveryRecord = mysql_fetch_array($result, MYSQL_ASSOC))
{
	$globalDeliveryId = $deliveryRecord['GlobalDeliveryId'];

	$imageURL = "NoImage.png";
	
	if ($deliveryRecord['DeliveryStatus'] == "cancelled_confirmed")
	{
		$imageURL = "Cancelled.png";
	}
	else
	{
		$sql = "SELECT * FROM Attachments WHERE GlobalDeliveryId=$globalDeliveryId AND ContentType='image/jpeg'";
		$result2 = exec_query($sql);

		if ($attachments = mysql_fetch_array($result2, MYSQL_ASSOC))
		{
			$filename = "usrdata/".$deliveryRecord['CompanyId']."/".$attachments['FileName'];
			if (isset($attachments['FileName']))
			{
				$imageURL = $filename;
			}
			
			// ok
		}
		else
		{
			//die("no attachments");
		}
		mysql_free_result($result2);
	}	
	
	?>
    <tr class="deliverytable deliveryrow">
    	<td class="deliverygeneric deliveryall">
			<a href="<?php echo $imageURL; ?>"><img class="deliverydetailimage" src="<?php echo $imageURL; ?>" /></a>
		</td>
        <td class="deliverycnno deliveryall">
            <?php echo $deliveryRecord['CNNO']; ?>
		</td>
		<td class="deliverystatus deliveryall">
        <?php
			echo getStatusDescriptionForStatus($deliveryRecord['DeliveryStatus']);
		?>
		</td>
		<td class="deliveryall">
			<?php
			if (isset($deliveryRecord['AssignedToDeviceId']))
			{
				echo $deliveryRecord['DeviceName'];
			}
			else
			{
				echo "<i>None</i>";
			}
			?>
			<?php
			if ($deliveryRecord['Source'] == "website" && $deliveryRecord['DeliveryStatus'] == "unassigned" || $deliveryRecord['DeliveryStatus'] == "scheduled" || $deliveryRecord['DeliveryStatus'] == "cancelled_confirmed")
			{
				echo "<br /><br />";
				if ($deliveryRecord['DeliveryStatus'] == "unassigned")
				{
					echo "Assign to device:<br />";
				}
				else
				{
					echo "Reassign to device:<br />";
				}
				?>
				<form name="frmAssign<?php echo $deliveryRecord['GlobalDeliveryId']; ?>" action="?" method="GET">
				<input type="hidden" name="action" value="ASSIGN">
				<input type="hidden" name="GlobalDeliveryId" value="<?php echo $deliveryRecord['GlobalDeliveryId']; ?>">
				<select name="DeviceId" onChange="frmAssign<?php echo $deliveryRecord['GlobalDeliveryId']; ?>.submit();">
				<option value="-2" selected>Choose One</option>
				<option value="-1">(none)</option>
				<?php
				$sql = "SELECT * From Devices WHERE CompanyId=$companyId AND Status='active'";
				$result2 = exec_query($sql);
				while ($deviceRecord = mysql_fetch_array($result2, MYSQL_ASSOC))
				{
					if ($deliveryRecord['AssignedToDeviceId'] != $deviceRecord['DeviceId'])
					{
						echo '<option value="'.$deviceRecord['DeviceId'].'"'.$selectedText.'>'.htmlspecialchars($deviceRecord['DeviceName']).'</option>';
					}
				}
				?>
				</select>
				</form>
				<?php
			}
			?>
		</td>
		<td class="deliverydeliverto deliveryall">
			<?php
				echo $deliveryRecord['Receiver']."<br />";
				echo "<a href=\"http://maps.google.com/?q={$deliveryRecord['AddrLine1']} {$deliveryRecord['AddrLine2']} {$deliveryRecord['Suburb']} {$deliveryRecord['Postcode']}, {$deliveryRecord['Country']}\" target=\"_blank\">";
				if ($deliveryRecord['AddrLine1'] != "")
				{
					echo "{$deliveryRecord['AddrLine1']}<br />";
				}
				if ($deliveryRecord['AddrLine2'] != "")
				{
					echo $deliveryRecord['AddrLine2']."<br />";
				}
				if ($deliveryRecord['Suburb'] != "" || $deliveryRecord['Postcode'] != "")
				{
					echo $deliveryRecord['Suburb']." ".$deliveryRecord['Postcode']."</a><br />";
				}
				if ($deliveryRecord['PhoneNo'] != "")
				{
					echo "<b>ph:</b> <a href=\"callto:{$deliveryRecord['PhoneNo']}\">{$deliveryRecord['PhoneNo']}</a><br />";
				}
			?>
		</td>
		<td class="note deliveryall">
			<?php echo $deliveryRecord['Note']; ?>
		</td>
		<td class="recievedby deliveryall">
        <?php
			if (isset($deliveryRecord['ReceivedByName']))
			{
				echo "<span class=\"recievedbyname\">{$deliveryRecord['ReceivedByName']}</span>";
				if (isset($deliveryRecord['ReceivedByClient']) && $deliveryRecord['ReceivedByClient'] != "")
				{
					echo " (".$deliveryRecord['ReceivedByClient'].")";
				}
			}
		?>		</td>
		<td class="deliveryproof deliveryall">
        <?php
			if ($deliveryRecord['DeliveryStatus'] == "delivered_endofday")
			{
				?>
				<a href="<?php echo $imageURL; ?>" target="_blank">Photo</a><br />
            	<?php
			}
			if ($deliveryRecord['DeliveryStatus'] == "delivered_endofday" || $deliveryRecord['DeliveryStatus'] == "delivered")
			{
				?>

                 <a href="http://maps.google.com/?q=<?php echo $deliveryRecord['Latitude'].",".$deliveryRecord['Longitude']; ?>" target="_blank">Location</a> (&plusmn;<?php echo $deliveryRecord['LocationAccuracy']; ?>m)
            	<?php
			}
		?>
		</td>
        <td class="deliveryaction deliveryall">
		<?php
			if ($deliveryRecord['DeliveryStatus'] == "unassigned" || $deliveryRecord['DeliveryStatus'] == "scheduled" || $deliveryRecord['DeliveryStatus'] == "senttodriver" || $deliveryRecord['DeliveryStatus'] == "withdriver")
			{
				?>
				<a href="?action=CANCEL&GlobalDeliveryId=<?php echo $deliveryRecord['GlobalDeliveryId']; ?>" onclick="javascript:return confirm('Are you sure you want to cancel delivery \'<?php echo $deliveryRecord['CNNO']; ?>\'?')">Cancel</a>
				<?php
			}
			else if ($deliveryRecord['DeliveryStatus'] == "withdriver_cancelpending" || $deliveryRecord['DeliveryStatus'] == "cancelled_confirmed")
			{
				?>
				<a href="?action=UNCANCEL&GlobalDeliveryId=<?php echo $deliveryRecord['GlobalDeliveryId']; ?>" onclick="javascript:return confirm('Are you sure you want to uncancel delivery \'<?php echo $deliveryRecord['CNNO']; ?>\'?')">Uncancel</a>
				<?php
			}
	
		?>
		</td>

	</tr>
	<?php
}
?>

</table>

<?php

template_footer();