<?php

// includes
require_once('DBFunctions.php');
require_once('TemplateFunctions.inc.php');
require_once('UDeliveredFunctions.inc.php');

// user auth
require_once('DoUserAuth.php');


if (isset($_GET['action']))
{

	if ($_GET['action'] == "APPROVE" || $_GET['action'] == "DEACTIVATE" || $_GET['action'] == "REACTIVATE" || $_GET['action'] == "LOST" || $_GET['action'] == "FOUND")
	{
		$deviceId = intval($_GET['DeviceId']);
		if ($deviceId <= 0)
		{
			die("invalid deviceId $deviceId");
		}
		
		$newStatus = "";
		if ($_GET['action'] == "APPROVE")
		{
			$newStatus = 'active';
		}
		else if ($_GET['action'] == "DEACTIVATE")
		{
			$newStatus = 'deactive';
		}
		else if ($_GET['action'] == "REACTIVATE")
		{
			$newStatus = 'active';
		}
		else if ($_GET['action'] == "LOST")
		{
			$newStatus = 'lost';
		}
		else if ($_GET['action'] == "FOUND")
		{
			$newStatus = 'active';
		}
		else
		{
			die("unknown action");
		}

		
		$sql = "SELECT Status FROM Devices WHERE CompanyId=$companyId AND DeviceId=$deviceId";
		$result = exec_query($sql);
		
		
		if ($deviceRecord = mysql_fetch_array($result, MYSQL_ASSOC))
		{
			$sql = "UPDATE Devices SET Status='$newStatus' WHERE CompanyId=$companyId AND DeviceId=$deviceId LIMIT 1";
			exec_query($sql);
	
		}
		else
	    {
			die("invalid");
			// no can do
	    }
		
	}
	
	// redirect to self to remove get variables
	header("Location: http://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);

	echo "Action complete";
	
	exit;
}
	

template_header("Manage Me – Devices");

$sql = "SELECT * From Companies WHERE CompanyId=$companyId";
$result = exec_query($sql);
$teamUDAC = "";

if ($companyRecord = mysql_fetch_array($result, MYSQL_ASSOC))
{
	$teamUDAC = $companyRecord['TeamUDAC'];
}
	
?>

<h3>Tip: Adding a device</h3>
<p>To add a team device, on the iPhone you wish to add, open uDelivered.proof, open the settings.  Go to "uDelivered.proof web portal" and select "Join a Company Team".  Enter the code <span class="udaccode"><?php echo $teamUDAC ?></span> which is the Team Code for your company. And press "Join Team".
The device will then appear in the list below allowing you to assign deliveries to it (after approval).
</p>
<br /><br />

<h2>Company Devices</h2>

<table>
    <tr class="deliverytable">
		<!--<td class="tableheading">
			DeviceId
		</td>-->
        <td class="tableheading">
            Device Name
		</td>
		<td class="tableheading">
			Last Known Whereabouts
		</td>
		<td class="tableheading">
			Status
		</td>
		<td class="tableheading">
        	Actions
		</td>
	</tr>

<?php


$sql = "SELECT * From Devices WHERE Devices.CompanyId=$companyId AND Status!='reassigned' ORDER BY Status";
$result = exec_query($sql);

while ($deviceRecord = mysql_fetch_array($result, MYSQL_ASSOC))
{
	$deviceId = $deviceRecord['DeviceId'];

	?>
    <tr class="deliverytable deliveryrow">
        <!--<td class="deliverygeneric deliveryall">
            <?php echo $deviceRecord['DeviceId']; ?>
		</td>-->
		<td class="deliverygeneric deliveryall">
			<?php echo $deviceRecord['DeviceName']; ?>
		</td>
		<td class="deliverylocation deliveryall">
			<?php
				
				$sql = "SELECT RequestTime, LocationLat, LocationLong, LocationAccuracy FROM RequestLog WHERE UDID='".$deviceRecord['UDID']."' AND LocationAccuracy > 0 AND LocationAccuracy < 1000 ORDER BY RequestTime DESC LIMIT 1";
				$result2 = exec_query($sql);
				
				if ($requestRecord = mysql_fetch_array($result2, MYSQL_ASSOC))
				{
					date_default_timezone_set('Asia/Chongqing');
					echo " <a href=\"http://maps.google.com/?q={$requestRecord['LocationLat']},{$requestRecord['LocationLong']}\" target=\"_blank\">";
					echo "At ".date('j M Y, g:i a',  $requestRecord['RequestTime']);
					echo " (&plusmn;{$requestRecord['LocationAccuracy']}m)";
					echo "</a>";
				}
			?>
		</td>
		<td class="deliverystatus deliveryall">
        <?php
			if ($deviceRecord['Status'] == 'pending_approval')
			{
				echo "<span class=\"pendingapproval\">";
			}
			echo getDescriptionForDeviceStatus($deviceRecord['Status']);
			if ($deviceRecord['Status'] == 'pending_approval')
			{
				echo "</span>";
			}
			?>
		</td>
        <td class="deliveryaction deliveryall">
		<?php
			if ($deviceRecord['Status'] == "pending_approval")
			{
				?>
				[<a href="?action=APPROVE&DeviceId=<?php echo $deviceRecord['DeviceId']; ?>" onclick="javascript:return confirm('Are you sure you want to approve device \'<?php echo $deviceRecord['DeviceName']; ?>\'?')">Approve</a>
				/<a href="?action=DEACTIVATE&DeviceId=<?php echo $deviceRecord['DeviceId']; ?>" onclick="javascript:return confirm('Are you sure you want to deactivate device \'<?php echo $deviceRecord['DeviceName']; ?>\'?')">Deactivate</a>]
				<?php
			}
			else if ($deviceRecord['Status'] == "active")
			{
				?>
				<a href="?action=DEACTIVATE&DeviceId=<?php echo $deviceRecord['DeviceId']; ?>" onclick="javascript:return confirm('Are you sure you want to deactivate device \'<?php echo $deviceRecord['DeviceName']; ?>\'?')">Deactivate</a>
				<?php
			}
			else if ($deviceRecord['Status'] == "deactive")
			{
				?>
				<a href="?action=REACTIVATE&DeviceId=<?php echo $deviceRecord['DeviceId']; ?>" onclick="javascript:return confirm('Are you sure you want to reactivate device \'<?php echo $deviceRecord['DeviceName']; ?>\'?')">Reactivate</a>
				<?php
			}
			else if ($deviceRecord['Status'] == "lost")
			{
				?>
				<a href="?action=FOUND&DeviceId=<?php echo $deviceRecord['DeviceId']; ?>" onclick="javascript:return confirm('Are you sure you want to report device \'<?php echo $deviceRecord['DeviceName']; ?>\' found?')">Report Found</a>
				<?php
			}
					
			if ($deviceRecord['Status'] != "lost")
			{
				?>
				<a href="?action=LOST&DeviceId=<?php echo $deviceRecord['DeviceId']; ?>" onclick="javascript:return confirm('Are you sure you want to report device \'<?php echo $deviceRecord['DeviceName']; ?>\' lost?  If you simply want to stop using the device, \'Deactivate\' it instead.')">Report Lost</a>
				<?php
			}
		?>
		</td>

	</tr>
	<?php
}
?>

</table>

<br /><br />
<a href=".">Back to Manage Me main screen</a>

<?php

template_footer();
