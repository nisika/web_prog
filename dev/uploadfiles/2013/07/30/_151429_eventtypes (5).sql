-- phpMyAdmin SQL Dump
-- version 3.5.5
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 24, 2013 at 03:15 PM
-- Server version: 5.5.28
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `udelivered_dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `eventtypes`
--

CREATE TABLE IF NOT EXISTS `eventtypes` (
  `GlobalEventTypeId` bigint(20) NOT NULL AUTO_INCREMENT,
  `CompanyId` bigint(20) NOT NULL COMMENT 'Foreign key for companies table',
  `AddedByUserId` int(11) NOT NULL COMMENT 'Foreign key for users table',
  `DisplayName` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `Description` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `Active` tinyint(4) NOT NULL,
  `EventCategory` enum('device','delivery','delivery_issue','job_accept','job_reject','product_status','silent') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'device',
  `AllowsEditingOfTimestamp` tinyint(4) NOT NULL DEFAULT '1',
  `DefaultNote` text COLLATE utf8_unicode_ci,
  `process_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `process_time` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_processed` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`GlobalEventTypeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2535 ;

--
-- Dumping data for table `eventtypes`
--

INSERT INTO `eventtypes` (`GlobalEventTypeId`, `CompanyId`, `AddedByUserId`, `DisplayName`, `Description`, `Active`, `EventCategory`, `AllowsEditingOfTimestamp`, `DefaultNote`, `process_type`, `process_time`, `last_processed`) VALUES
(4806, 200, -2, 'Pickup Location', '', 1, 'delivery', 0, '{"Title":{"Text": "Pickup Location ","FontSize":"20","FontColor":"000000",\n"BackgroundColor":"98FF98"}\n,"PLName":{"Type":"TextField","DefaultValue":"",\n"Prompt":"Name","Hints":"Name","Order":10}\n,"PLadd1":{"Type":"TextField","DefaultValue":"",\n"Prompt":"Address Line 1", "Hints":"1st Line", "Order":20}\n,"PLadd2":{"Type":"TextField","DefaultValue":"",\n"Prompt":"Address Line 2", "Hints":"2nd Line", "Order":30}\n,"PLsuburb":{"Type":"TextField","DefaultValue":"",\n"Prompt":"Suburb", "Hints":"Suburb", "Order":40}\n,"Photo":{"Type":"Photo","Order":90}\n,"HintText":{"Type":"TextPrompt","Prompt":"This screen allows you to enter the pickup location address and add a photo of location or goods","Order":80,"BackgroundColor":"FFC0C0"}\n\n,"SummaryFields":["ClientPicker"]\n,"FormId":"PL001"\n,"FormVersion":"1.0.0"\n,"hiddenvalue":{"Hidden":"this is hidden"}\n,"ServerAction":{"ServerAction":"do this on server"}\n}', '', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
