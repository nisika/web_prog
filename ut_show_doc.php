<?php

/**********************************************************************************************************************************/
/*******************************************************************************************************************[ SETTINGS ]***/
// Set sorting properties.
$sort = array(
    array('key'=>'mtime','sort'=>'asc'), // ... this sets the initial sort "column" and order ...
	array('key'=>'lname','sort'=>'asc'), // ... this sets the initial sort "column" and order ...
	array('key'=>'size','sort'=>'asc') // ... for items with the same initial sort value, sort this way.
);

/**********************************************************************************************************************************/
/************************************************************************************************************[ DIRECTORY LOGIC ]***/
// Get this folder and files name.
$this_script = basename(__FILE__);
$this_folder = str_replace('/'.$this_script, '', $_SERVER['SCRIPT_NAME']);
$s_program_name="ut_show_doc.php";
$s_site_url='http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
$s_site_url=str_replace($s_program_name,'',$s_site_url);
$s_site_path='C:/PKNPC/webdev'.$_SERVER['PHP_SELF'];
$s_site_path='d:/tdx/html'.$_SERVER['PHP_SELF'];
$s_site_path=str_replace($s_program_name,'',$s_site_path);

// Declare vars used beyond this point.
$ar_file_list = array();
$ar_folder_list = array();
$ar_pdf_files_list = array();
$total_size = 0;

// Open the current directory...
if ($handle = opendir('.'))
{
	// ...start scanning through it.
    while (false !== ($file = readdir($handle)))
	{
		// Make sure we don't list this folder, file or their links.
        if ($file != "." && $file != ".." && $file != $this_script)
		{
            $item['file_name']=trim(strtoupper($file));
			array_push($ar_file_list, $item);
        }
    }
	// Close the directory when finished.
    closedir($handle);
}
// Sort folder list.
if($ar_file_list)
	$ar_file_list = pf_a100_php_multisort($ar_file_list, $sort);

B100_PROCESS_FILES:

$i=0;
$i_folder_cnt=0;
$i_file_cnt=0;
$s_filename='';
$s_actual_filename='';
$s_thumb_filename='';
$s_title_filename='';
$s_pdf_filename='';
while (trim($ar_file_list[$i]['file_name'])<>'')
{
    $s_filename=$ar_file_list[$i]['file_name'];
    //echo 's_filename='.$s_filename.'<br>';
    $i_extension_pos=strpos($s_filename,'.');
    //echo 'i_extension_pos='.$i_extension_pos.'<br>';

B200_DO_FILE_PROCESSING:
    if ($i_extension_pos>0)
    {
        $s_thumb_filename=$s_filename;
        $s_actual_filename=substr($s_filename,0,$i_extension_pos);
        // pkn 20140114 - need to be > 0 so check from 2nd char onwards
        $s_check_filename=substr($s_actual_filename,1,100);
        $s_check_filename=trim($s_check_filename);
        //echo 's_filename='.$s_filename.'<br>';
        //echo 's_actual_filename='.$s_actual_filename.'<br>';
        $s_thumb_filename=pf_b100_get_thumb_filename($ar_file_list,$s_check_filename);
        $s_title_filename=pf_b200_get_title_filename($ar_file_list,$s_check_filename);
        $s_pdf_filename=pf_b300_get_pdf_filename($ar_file_list,$s_check_filename);
        if (trim($s_thumb_filename)=='')
        {
            goto B900_READ_NEXT_REC;
        }
        if (trim($s_title_filename)=='')
        {
            goto B900_READ_NEXT_REC;
        }
        if (trim($s_pdf_filename)=='')
        {
            goto B900_READ_NEXT_REC;
        }
        $ar_pdf_files_list[$i_file_cnt]['image']=$s_site_url.$s_thumb_filename;
        $s_title=pf_c100_get_title($s_site_path,$s_title_filename);
        $ar_pdf_files_list[$i_file_cnt]['title']=$s_title;
        $ar_pdf_files_list[$i_file_cnt]['pdf_filename_url']=$s_site_url.$s_pdf_filename;

        $i_file_cnt++;
    }

B300_DO_FOLDER_PROCESSING:
    if ($i_extension_pos=='')
    {
        // pkn 20140114 - need to be > 0 so check from 2nd char onwards
        $s_check_filename=substr($s_filename,1,100);
        $s_thumb_filename=pf_b100_get_thumb_filename($ar_file_list,$s_check_filename);
        $s_title_filename=pf_b200_get_title_filename($ar_file_list,$s_check_filename);
        if (trim($s_thumb_filename)=='')
        {
            goto B900_READ_NEXT_REC;
        }
        if (trim($s_title_filename)=='')
        {
            goto B900_READ_NEXT_REC;
        }
        $ar_folder_files_list[$i_folder_cnt]['image']=$s_site_url.$s_thumb_filename;
        $s_title=pf_c100_get_title($s_site_path,$s_title_filename);
        $ar_folder_files_list[$i_folder_cnt]['title']=$s_title;
        $ar_folder_files_list[$i_file_cnt]['folder_url']=$s_site_url.$s_filename.'/'.$s_program_name;
        $i_folder_cnt++;
    }
    B900_READ_NEXT_REC:
    $i++;
}

//print_r($ar_folder_files_list);

$s_image_file=$s_site_url.$s_thumb_filename;
$s_pdf_file=$s_site_url.$s_pdf_filename;

//echo 's_thumb_filename='.$s_thumb_filename.'<br>';
//echo 's_title_filename='.$s_title_filename.'<br>';
//echo 's_pdf_filename='.$s_pdf_filename.'<br>';
//echo 's_image_file='.$s_image_file.'<br>';
//echo 's_actual_filename='.$s_actual_filename.'<br>';

/**********************************************************************************************************************************/
/******************************************************************************************************************[ FUNCTIONS ]***/

/**
 *	http://us.php.net/manual/en/function.array-multisort.php#83117
 */

/**********************************************************************************************************************************/
/*******************************************************************************************************************[ TEMPLATE ]***/
?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>TDX Documentation</title>
        <style type="text/css">
            body{
                font-family: "Lucida Grande",Calibri,Arial;
                font-size: 9pt;
                color: #333;
                background: #f8f8f8;
            }
            a{
                color: #b00;
                font-size: 11pt;
                font-weight: bold;
                text-decoration: none;
            }
            a:hover{
                color: #000;
            }
            img{
                vertical-align: bottom;
                padding: 0 3px 0 0;
            }
            table{
                margin: 0 auto;
                padding: 0;
                width: 600px;
            }
            table td{
                padding: 5px;
            }
            thead td{
                padding-left: 0;
                font-family: "Trebuchet MS";
                font-size: 11pt;
                font-weight: bold;
            }
            tbody .folder td{
                border: solid 1px #f8f8f8;
            }
            tbody .file td{
                background: #fff;
                border: solid 1px #ddd;
            }
            tbody .file td.size,
            tbody .file td.time{
                white-space: nowrap;
                width: 1%;
                padding: 5px 10px;
            }
            tbody .file td.size span{
                color: #999;
                font-size: 8pt;
            }
            tbody .file td.time{
                color: #555;
            }
            tfoot td{
                padding: 5px 0;
                color: #777;
                font-size: 8pt;
                background: #f8f8f8;
                border-color: #f8f8f8;
            }
            tfoot td.copy{
                text-align: right;
                white-space: nowrap;
            }
            tfoot td.cc{
                padding: 40px;
                text-align: center;
            }
            tfoot td.cc img{
                padding: 0;
                border: none;
            }
        </style>
    </head>

    <body>
    <table width="100%">
        <tr>
            <td>TDX Documentation</td>
            <td></td>
            <td></td>
        </tr>
        </thead>
        <tbody>
        <!-- folders -->
        <?php foreach($ar_folder_files_list as $s_folder)
        {
        ?>
                <tr class="folder">
                    <td colspan="3" class="name"><img src="<?php echo $s_folder['image']?>"> <a href="<?php echo $s_folder['folder_url']?>" target="_blank"><?php echo $s_folder['title']?></td>
                </tr>
        <?php
        }?>
        <!-- /folders -->
        <!-- files -->
        <?php
        foreach($ar_pdf_files_list as $s_pdf_file)
        {
        ?>
            <tr class="file">
                <td class="name"><img src="<?php echo $s_pdf_file['image']?>"> <a href="<?php echo $s_pdf_file['pdf_filename_url']?>" target="_blank"><?php echo $s_pdf_file['title']?></td>
                <td class="size"><?=$item['size']['num']?><span><?=$item['size']['str']?></span></td>
                <td class="time"><?=$item['mtime']?></td>
            </tr>
        <?php
        }?>
        <!-- /files -->
        </tbody>
    </table>

    </body>
    </html>

<?php

function pf_a100_php_multisort($data,$keys)
{
    foreach ($data as $key => $row)
    {
        foreach ($keys as $k)
        {
            $cols[$k['key']][$key] = $row[$k['key']];
        }
    }
    $idkeys = array_keys($data);
    $i=0;
    foreach ($keys as $k)
    {
        if($i>0){$sort.=',';}
        $sort.='$cols['.$k['key'].']';
        if($k['sort']){$sort.=',SORT_'.strtoupper($k['sort']);}
        if($k['type']){$sort.=',SORT_'.strtoupper($k['type']);}
        $i++;
    }
    $sort .= ',$idkeys';
    $sort = 'array_multisort('.$sort.');';
    eval($sort);
    foreach($idkeys as $idkey)
    {
        $result[$idkey]=$data[$idkey];
    }
    return $result;
}

function pf_b100_get_thumb_filename($par_file_list,$ps_check_filename)
{
    $i=0;
    $s_filename='';
    $s_thumb_filename='';
    $s_check_filename=$ps_check_filename.'-THUMB.PNG';
    //echo 's_check_filename2='.$s_check_filename.'<br>';
    while (trim($par_file_list[$i]['file_name'])<>'')
    {
        $s_filename=$par_file_list[$i]['file_name'];
        $i_thumb_pos=strpos($s_filename,$s_check_filename);
        //echo 'i_thumb_pos='.$i_thumb_pos.'<br>';
        if ($i_thumb_pos>0)
        {
            $s_thumb_filename=$s_filename;
            //echo 's_thumb_filename1='.$s_thumb_filename.'<br>';
        }

        $i++;
    }
    //echo 's_thumb_filename2='.$s_thumb_filename.'<br>';
    return $s_thumb_filename;
}

function pf_b200_get_title_filename($par_file_list,$ps_check_filename)
{
    $i=0;
    $s_filename='';
    $s_title_filename='';
    $s_check_filename=$ps_check_filename.'-TITLE.TXT';
    while (trim($par_file_list[$i]['file_name'])<>'')
    {
        $s_filename=$par_file_list[$i]['file_name'];
        $i_title_pos=strpos($s_filename,$s_check_filename);
        if ($i_title_pos>0)
        {
            $s_title_filename=$s_filename;
        }

        $i++;
    }
    //echo 's_title_filename2='.$s_title_filename.'<br>';
    return $s_title_filename;
}

function pf_b300_get_pdf_filename($par_file_list,$ps_check_filename)
{
    $i=0;
    $s_filename='';
    $s_pdf_filename='';
    while (trim($par_file_list[$i]['file_name'])<>'')
    {
        $s_filename=$par_file_list[$i]['file_name'];
        $i_pdf_pos=strpos($s_filename,$ps_check_filename.'.PDF');
        if ($i_pdf_pos>0)
        {
            $s_pdf_filename=$s_filename;
        }

        $i++;
    }
    //echo 's_pdf_filename2='.$s_pdf_filename.'<br>';
    return $s_pdf_filename;
}

function pf_c100_get_title($s_site_path,$s_title_filename)
{
    A100_OPEN_TITLE_FILE:

    $s_file_name=$s_site_path.$s_title_filename;
    if(file_exists($s_file_name))
    {
        $s_title_file = file($s_file_name);
    }
    else
    {
        echo "File Not Found: ".$s_file_name;
        goto X100_EXIT;
    }

    B100_READ_FILE:

    foreach($s_title_file as $key=>$s_file_line)
    {
        if(trim($s_file_line)=="")
        {
            goto B900_READ_NEXT_REC;
        }

        $s_title=trim($s_file_line);
        goto X100_EXIT;

        B900_READ_NEXT_REC:
    }

    X100_EXIT:

    return $s_title;
}
