<?php

//eg http://dev.tdx.com.au/als_web_prog/ut_menu.php?fnid=101&map=udp_navleft.htm
//eg http://dev.tdx.com.au/als_web_prog/ut_menu.php?fnid=101&map=udp_navleft.htm&p=|%!start_SITEPARAMS_!end%

function pf_function_to_program($ps_function_id,$ps_Values)
{

// echo "<br> utmenu toprogram ps_function_id=".$ps_function_id."  ps_Values=".$ps_Values."";

    $sProgram = "NONE";
    if (strtoupper(trim($ps_function_id))== "101"){
        $sProgram = "HTML";
    }
    if (strtoupper(trim($ps_function_id))== "201"){
        $sProgram = "JCL";
    }
    if (strtoupper(trim($ps_function_id))== "202"){
        $sProgram = "JCL+KEEP";
    }
    if (strtoupper(trim($ps_function_id))== "203"){
        $sProgram = "REPEXT";
    }
    if (strtoupper(trim($ps_function_id))== "208"){
        $sProgram = "JCLRERUN";
    }
    if (strtoupper(trim($ps_function_id))== "209"){
        $sProgram = "JCL+SESSION";
    }
    if (strtoupper(trim($ps_function_id))== "210"){
        $sProgram = "SESSIONVARS+JCL";
    }
    if (strtoupper(trim($ps_function_id))== "301"){
        $sProgram = "RUNCOOKIEJCL";
    }
    return $sProgram;
}

function pf_customError($errno,$errstr,$errfile,$errline,$errcontext)
  {
// NOTE **** THIS ONLY CATCHES NON-FATAL ERRORS
//  echo "<b>Error gw12:</b> [$errno] $errstr $errfile $errline $errcontext.<br>";
        switch ($errno) {
        case E_USER_ERROR:
            echo "<b>My ERROR</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            echo "  Fatal error on line $errline in file $errfile";
            echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
            echo "Aborting...<br />\n";
            exit(1);
            break;

        case E_USER_WARNING:
            echo "<b>My WARNING</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        case E_USER_NOTICE:
            echo "<b>My NOTICE</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        default:
            echo "Unknown error type: [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;
        }

    /* Don't execute PHP internal error handler */
    return true;

  }

 //set error handler
 set_error_handler("pf_customError");
date_default_timezone_set('Australia/Brisbane');
  session_start();

// gw 20111026  session_start();

//SET_LANGUAGE
    $_SESSION['sys_language'] = "ENGLISH";
//    $_SESSION['sys_language'] = "FRENCH";
//    $_SESSION['sys_language'] = "GERMAN";

// echo "<br> utmenu toprogram 1 ";



    if (isset($_COOKIE['ud_sys_language']))
    {
//    ECHO "        COOKE IS SET TO ".$_COOKIE['ud_sys_language'];
       $_SESSION['sys_language']= $_COOKIE['ud_sys_language'];
    }else{
//       ECHO "COOKE IS SETUP ELSE";
       setcookie('ud_sys_language','ENGLISH',time()+60*60*24*30);
    }



//.    echo getcwd()."gwgwgwgwg<br>";
    if (isset($_SESSION['ko_prog_path']))
    {
// gw 20100314        ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
    }else{
    $_SESSION['ko_prog_path'] = "";
    }

    if (isset($_SESSION['ko_dbase_to_connectto']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
    }else{
    $_SESSION['ko_dbase_to_connectto'] = "tdxpryda";
    }


// A000_DEFINE_VARIABLES
    $s_sessionno = "";
    $s_prog_name = "ud_menu";
    $sys_debug = '';
    $s_helplocation = "";
    $function_id = "";
    $map = "";
    $get_userid = "";
    $s_filename="";
    $s_details_def="";
    $s_details_data="";
    $s_siteparams = "";
    $s_url_siteparams = "";
    $s_usr_style_list = "";
    $s_usr_style_values= "";
    $tmp_array = array();

    // A100_INITIALISE_VARIABLES

    // can change this to the users requirements

    //A200_LOAD_LIBRARIES
    $sys_debug = strtoupper("no");
//    $sys_debug = strtoupper("yes");

// echo "<br> utmenu toprogram 2 ";

     IF ($sys_debug == "YES"){echo $s_prog_name." started debug=".$sys_debug."<br>";};
    $s_filename = $_SESSION['ko_prog_path'].'lib/class_sql.php';
    if(!file_exists($s_filename))
    {
        die("ut_menu - err:0013 - unable to find file []".$s_filename."[]");
    }
     require_once($s_filename);
     $class_sql = new wp_SqlClient();
     IF ($sys_debug == "YES"){echo $s_prog_name." after class_sql<br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_main.php');
     $class_main = new clmain();
     IF ($sys_debug == "YES"){echo  $s_prog_name." after class_main <br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_myplib1.php5');
     $class_myplib1 = new myplib1();
     IF ($sys_debug == "YES"){echo  $s_prog_name." after class_myplib1<br>";};
     IF ($sys_debug == "YES"){echo "_get=";print_r($_GET);echo "<br>";};
     IF ($sys_debug == "YES"){echo "_post=";print_r($_POST);echo "<br>";};
//A300_CONNECT_TODBASE
// echo "<br> utmenu toprogram 2a ";
     $dbcnx = $class_sql->c_sqlclient_connect();
// echo "<br> utmenu toprogram 2b ";

//A400_GET_PARAMETERS
     if (isset($_POST['function_id'])) $function_id = $_POST["function_id"];
     if (isset($_GET['fnid'])) $function_id = $_GET["fnid"];
     if (isset($_GET['map'])) $map = $_GET["map"];
     if (isset($_GET['p'])) $s_url_siteparams = $_GET["p"];


    if (strpos(strtolower(trim($map)),"tdxmappath") !==false)
    {
        $map = str_replace("tdxmappath", $_SESSION["ko_map_path"],strtolower(trim($map)));
    }

A200_SETUP_USER:
//gw20110926 - do everytime
//
     if (!isset($_GET['setupuserid']))
     {
//gw20110926         echo "utmenu no setupuserid  _get=";print_r($_GET);echo "<br>";
        goto A500_SET_VALUES;
     }
//         echo "utmenu  doing       **************** setupuserid  _get=";print_r($_GET);echo "<br>";

     $s_setupuserid = $_GET['setupuserid'];
     $ar_setupuserid =  explode("|",$s_setupuserid);
     $user_preferences = pf_set_user_preferences($dbcnx,$ar_setupuserid[0],$ar_setupuserid[1]);
        //  echo "<!--";
//gw20110926
//echo "<br>utmenu test of sessions at set_user <br><br>";
//gw20110926
//         print_r($_SESSION);
//gw20110926
//          echo "<br>utmenu test of sessions at set_user <br><br>";
A500_SET_VALUES:
     IF ($sys_debug == "YES"){echo "function_id=".$function_id."<br>";};
     IF ($sys_debug == "YES"){echo "fnid=".$function_id."<br>";};
     IF ($sys_debug == "YES"){echo "map=".$map."<br>" ;};
     IF ($sys_debug == "YES"){echo "p=".$s_url_siteparams."<br>";};
// echo "<br> utmenu toprogram 3 ";
     if (isset($_GET['dieme']))
     {
        echo "<br>dieme from utmenu <br><br>";
        echo "<br>$_GET <br><br>";
        print_r($_GET);
        echo "<br>$ session var <br><br>";
        print_r($_SESSION);
        die ("<br>die in utmenu");
     }

// echo "<br> utmenu toprogram 4 ";

    $s_sessionno = "";
    $username = "";

//B100_PROCESS_ID
     IF ($sys_debug == "YES"){echo "b100_process_id<br>";};
     $s_program = "";
     $Values = "";
     $s_program = pf_function_to_program($function_id,$Values);
     IF ($sys_debug == "YES"){echo "Function (".$function_id.") has been assigned to program (".$s_program.")<br>";};

     IF ($s_program == "NONE") {
         echo '<body> There is no option for this funtion '.$function_id.'</body>';
         EXIT;
     }
//gw - 20100308 - no standard paramater array
     if (strpos($s_url_siteparams,"^") === false)
     {
              $s_url_siteparams = "";
     }
     //201001027 - if siteparams have not been set then set them
        if (strpos($s_url_siteparams,"%!") > 0 )
        {
              $s_url_siteparams = $s_sessionno &"^";
        }

     //20100127 - move to above ud_logonname check
    if (trim($s_url_siteparams)!=="")
    {
//20091204 $s_url_siteparams 'sessionno^"
        $s_sessionno = $class_main->clmain_get_param($s_url_siteparams,"s_sessionno","no");
        if (isset($_SESSION[$s_sessionno.'username']))
        {
                $username = $_SESSION[$s_sessionno.'username'];
        }ELSE{
//gw20100627 - if the username is not set then it should stay as blank
//                $_SESSION[$s_sessionno.'username'] = "utmenu";
                $_SESSION[$s_sessionno.'username'] = "";
        }
    }else{
        $s_sessionno = "utmenu2";
        $_SESSION[$s_sessionno.'username'] = "utmenu3";
        $username = $_SESSION[$s_sessionno.'username'];
    }
//##################################################################################################
//gw20110926          echo "<br>utmenu test of sessions at logon cookie <br><br>";
//gw20110926          print_r($_SESSION);
//gw20110926          echo "<br>test of sessions at logon cookie <br><br>";

// echo "<br> utmenu toprogram 5 ";
     IF ($sys_debug == "YES"){echo "ud_cookie<br>";};

     if (isset($_COOKIE['ud_logonname']))
    {
            $_SESSION[$s_sessionno.'username']= $_COOKIE['ud_logonname'];
            $username = $_SESSION[$s_sessionno.'username'];
    }else{
        setcookie('ud_logonname',"utmenu");
    }

     IF ($sys_debug == "YES"){echo $s_prog_name." s_url_siteparams = ".$s_url_siteparams."<br>";};
     IF ($sys_debug == "YES"){echo $s_prog_name." s_sessionno = ".$s_sessionno."<br>";};
     IF ($sys_debug == "YES"){echo $s_prog_name." username = ".$username."<br>";};


    $s_siteparams = "".$s_sessionno."^".$username;
    if (trim($s_url_siteparams)!==""){
        $s_siteparams = $s_url_siteparams;
    }
     IF ($sys_debug == "YES"){echo $s_prog_name." site params = ".$s_siteparams."<br>";};

//##################################################################################################
//gw20110926          echo "<br>utmenu  test of sessions at odd_or_even <br><br>";
//gw20110926          print_r($_SESSION);
//gw20110926          echo "<br>test of sessions at odd_or_even <br><br>";

    $_SESSION[$s_sessionno.'ODD_OR_EVEN_LINE'] = "even";


//gw20110313 - added user timezone
    if (isset($_SESSION[$s_sessionno.'ut_logon_timezone']))
    {
        $timezone=$_SESSION[$s_sessionno.'ut_logon_timezone'];
        date_default_timezone_set($timezone);
    }else{
    }
//##################################################################################################
//gw20110926          echo "<br>utmenu  test of sessions at set_style <br><br>";
//gw20110926         print_r($_SESSION);
//gw20110926          echo "<br>utmenu  test of sessions at set_style <br><br>";
//        die ("<br>die in utmenu");

//B200_SET_STYLE
     IF ($sys_debug == "YES"){echo "b200_set_style<br>";};
    $tmp_array = explode("^",$class_main->clmain_set_user_style($username,"NO"));
    $s_usr_style_list = $tmp_array[0];
    $s_usr_style_values = $tmp_array[1];

     IF ($sys_debug == "YES"){echo $s_prog_name." s_usr_style_list = ".$s_usr_style_list."<br>";};
     IF ($sys_debug == "YES"){echo $s_prog_name." s_usr_style_values = ".$s_usr_style_values."<br>";};

//##################################################################################################
        //  echo "<br>test of sessions at html s_program".$s_program." <br><br>";
        //  print_r($_SESSION);
        //  echo "-->";

//c100_run_programs
     IF ($s_program == "HTML") {
          IF ($sys_debug == "YES"){echo "<br>doing html <br>";};
//       $class_main->clmain_activity_log($dbcnx,$get_userid." has logged into the system",$get_userid, "mainpage");
       $s_filename=$map;
//       echo "<br>pre clmain set the map html".$map;
       $s_filename = $class_main->clmain_set_map_path_n_name("",$s_filename);

       $s_details_def='START|USERNAME';
       $s_details_data='START|'.$username;
//       $s_details_def.=$s_usr_style_list;
//       $s_details_data.=$s_usr_style_values;
       $s_details_def.="|SITEPARAMS";
       $s_details_data.='|'.$s_siteparams;

       if (isset($_SESSION[$s_sessionno.'session_def']))
       {
           $s_details_def.=$_SESSION[$s_sessionno.'session_def'];
           $s_details_data.=$_SESSION[$s_sessionno.'session_data'];
       }
       $s_details_def.='|END';
       $s_details_data.='|END';


//echo "<br> utmenu html s_prog_name=".$s_prog_name."  s_filename =".$s_filename."<br>";

        IF ($sys_debug == "YES"){echo "".$s_prog_name." s_filename =".$s_filename."<br>";}
        IF ($sys_debug == "YES"){echo "".$s_prog_name." s_details_def =".$s_details_def."<br>";}
        IF ($sys_debug == "YES"){echo $s_prog_name." s_details_data =".$s_details_data."<br>";}

//GW20100104        echo $class_main->clmain_v100_load_html_screen($s_filename,$s_details_def,$s_details_data,"NO","ALL");
        echo $class_main->clmain_v100_load_html_screen($s_filename,$s_details_def,$s_details_data,"NO","WHOLE_FILE");

//echo "<br> utmenu html after v100 s_prog_name=".$s_prog_name."  s_filename =".$s_filename."<br>";

        $_SESSION[$s_sessionno.'session_def'] = "";
        $_SESSION[$s_sessionno.'session_data'] = "";
        exit;
     }
//ECHO "<br>GW UTM PROGRAM=".$s_program;

//##################################################################################################
        //  echo "<br>test of sessions at jcl+session <br><br>";
        // print_r($_SESSION);

     IF ($s_program == "JCL+SESSION") {
          IF ($sys_debug == "YES"){echo "do jcl+session<br>";};
//ECHO "<br>GW2 UTM PROGRAM=".$s_program;
       $s_session_name = $s_sessionno.$_GET["svname"];
       $s_session_value = $_GET["svvalue"];
       $_SESSION[$s_session_name] = $s_session_value;

       $map=$_SESSION[$s_sessionno.'jclmap'];
       $s_siteparams=$_SESSION[$s_sessionno.'jclparams'];

//// gw 20120823       echo " doing 209 map=[]".$map."[]  s_siteparams=[]".$s_siteparams."[]   s_session_name=[]".$s_session_name."[]   s_session_value=[]".$s_session_value."[]  ";

//ECHO "<br>GW3 UTM session=".$s_session_name."   UTM sessionvalue=".$_SESSION["$s_session_name"];
       $s_program = "JCL";
    }
//##################################################################################################
        //  echo "<br>test of sessions at jclrun <br><br>";
        //  print_r($_SESSION);

    IF ($s_program == "SESSIONVARS+JCL") {
        IF ($sys_debug == "YES"){echo "do SESSIONVARS+JCL<br>";};
    //ECHO "<br>GW2 UTM PROGRAM=".$s_program;
        $s_session_name = $_GET["svname"];
        $s_session_value = $_GET["svvalue"];
    // split svname and svvalues on , and set each sessionvar then run the jcl map


        $ar_session_name = explode(",",$s_session_name);
        $ar_session_value = explode ( ",", $s_session_value);

        $count = count($ar_session_name);
        for ($i = 0; $i < $count; $i++) {
//            echo "Name:[]".$s_sessionno.$ar_session_name[$i]."[] value=[]".$ar_session_value[$i]."[]";
            if(strtoupper(trim($ar_session_value[$i])) <> "NOCHANGE")
            {
                $_SESSION[$s_sessionno.$ar_session_name[$i]] = $ar_session_value[$i];
            }
        }

//        die("ut_menu 210 s_session_name=[]".$s_session_name."[] s_session_value=[]".$s_session_value."[]");

        $map=$_SESSION[$s_sessionno.'jclmap'];
        $s_siteparams=$s_url_siteparams;
        $s_program = "JCL";

    }
//##################################################################################################
//  echo "<br>test of sessions at jclrun <br><br>";
//  print_r($_SESSION);


    IF ($s_program == "JCLRERUN") {
//ECHO "<br>GW2 UTM PROGRAM=".$s_program;
        IF ($sys_debug == "YES"){echo "do jclrerun <br>";};
       $map=$_SESSION[$s_sessionno.'jclmap'];
       $s_siteparams=$_SESSION[$s_sessionno.'jclparams'];
//ECHO "<br>GW3 UTM s_sessionno=".$s_sessionno;
//ECHO "<br>GW4 map=".$map."   s_siteparams=".$s_siteparams;

       $s_program = "JCL";
    }
     IF ($s_program == "JCL+KEEP") {
       IF ($sys_debug == "YES"){echo "jcl+keep <br>";};
       $_SESSION[$s_sessionno."jclmap"]=$map;
       $_SESSION[$s_sessionno."jclparams"]=$s_siteparams;
       $s_program = "JCL";
    }

     IF ($s_program == "JCL") {
        IF ($sys_debug == "YES"){echo " do jcl<br>";};
        echo "<!-- pageheader!start -->";
        echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
        echo '<html>';
        echo '<head>';
        echo '<meta HTTP-EQUIV="REFRESH" content="0; url=ut_jcl.php?map='.$map.'&p='.$s_siteparams.'">';
        echo '</head>';
        echo '</html>';
        exit;
      }

     IF ($s_program == "REPEXT") {
        IF ($sys_debug == "YES"){echo "do repext <br>";};
        echo "<!-- pageheader!start -->";
        echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
        echo '<html>';
        echo '<head>';
        echo '<meta HTTP-EQUIV="REFRESH" content="0; url=ut_repext.php?map='.$map.'&p='.$s_siteparams.'">';
        echo '</head>';
        echo '</html>';
        exit;
      }
exit;

?>


<?php

function pf_set_user_preferences($ps_dbcnx,$ps_sessionno,$ps_userid)
{
// get the utl_users_preferences record if the table exists
// set the preferences based on the values in utl_users_preferences.data_blob
    global $class_main;


//         echo "utmenu  doing    user-preferences   **************** setupuserid  _get=";print_r($_GET);echo "<br>";


            $_SESSION[$ps_sessionno.'__GWTEST__'] = "start utmenu set user pref";

    $s_deviceframe_width = "";
    $_SESSION[$ps_sessionno.'udp_site_css'] = "udp_site.css";
    $_SESSION[$ps_sessionno.'udp_bdyleft_css'] = "udp_bdyleft.css";
    $_SESSION[$ps_sessionno.'udp_navleft_css'] = "udp_navleft.css";
    $_SESSION[$ps_sessionno.'udp_bdymain_css'] = "udp_bydmain.css";

    $_SESSION[$ps_sessionno.'udj_site_css'] = "udj_site.css";

    $s_deviceframe_width = "150";
    $_SESSION[$ps_sessionno.'udp_deviceframe_width'] = $s_deviceframe_width;
    $_SESSION[$ps_sessionno.'udp_deviceframe_tabwidth'] = $s_deviceframe_width - 20;
    $_SESSION[$ps_sessionno.'udp_devicedesc'] = "User";

    $_SESSION[$ps_sessionno.'userbunit'] = "JOBMANAGEMENT";
    $_SESSION[$ps_sessionno.'ut_logon_timezone'] = "Australia/Darwin";
//       $_SESSION[$ps_sessionno.'ut_logon_timezone'] = "Europe/London";
    $_SESSION[$ps_sessionno.'user_id'] = $ps_userid;

    $sys_out = "";
    $sys_out = $sys_out.'<?xml version="1.0" encoding="ISO-8859-1"?><data_blob>';
    $sys_out = $sys_out.'<no_preferences>NoPreferences</no_preferences>';
    $sys_out = $sys_out.'</data_blob>';


        $_SESSION['udp_devicedesc'] = "User";

//        echo "<br>device desc = ".$_SESSION['udp_devicedesc'];
//         echo "utmenu  doing    get prefere ces user-preferences   **************** setupuserid  _get=";print_r($_GET);echo "<br>";

A100_GET_USER:
     $s_user_preferences = $class_main->clmain_B210_get_user_preferences($ps_dbcnx,$ps_userid,"NO",$ps_sessionno);

     $sys_out= $s_user_preferences;
B100_SET_SESSION_VARS:

//gw20110926
//  goto Z900_EXIT;


//ECHO "<BR>utmenu setpref start ";
//echo "<br>".print_r($_SESSION);
//ECHO "<BR>utmenu setpref end ";
//echo "<br>".print_r(str_replace("<","openbracket^",$s_user_preferences));
//ECHO "<BR>utmenu setpref end ";
//die ("<br> gwdie sessionno=.".$ps_sessionno);
     if (strpos(strtoupper($s_user_preferences),"*ERROR") !== false )
     {
       $_SESSION[$ps_sessionno.'ut_logon_timezone'] = "Australia/Sydney";
       $_SESSION[$ps_sessionno.'userbunit'] = "none";
//        echo "<br> utmenu user preferences is failing for userid=(".$ps_userid.") session_no=(".$ps_sessionno.") ";

//        die ("died utmentu user pref:");
         GOTO B900_END;
      }

//         echo "utmenu  doing    get prefere ces user-preferences  GOT PREF **************** setupuserid  _get=";print_r($_GET);echo "<br>";

//         echo "utmenu  doing    get prefere ces user-preferences  GOT PREF **************** setupuserid  _get=";print_r($_SESSION);echo "<br>";
    $s_temp = $class_main->clmain_u592_get_field_from_xml($s_user_preferences,"timezone", "no", "utmenu_upref","Australia/Darwin");
    $_SESSION[$ps_sessionno.'ut_logon_timezone'] = $s_temp;


// gw 201111    $_SESSION[$ps_sessionno.'ut_logon_timezone'] = 'Australia/Brisbane';

   $s_temp = $class_main->clmain_u592_get_field_from_xml($s_user_preferences,"userbunit", "no", "utmenu_upref","Australia/Darwin");
   $_SESSION[$ps_sessionno.'userbunit'] = $s_temp;
// gw 201111    $_SESSION[$ps_sessionno.'userbunit'] = "gwtst";

   // $xml = new SimpleXMLElement($s_user_preferences);
//    if (isset($xml->userbunit[0])){
//        $s_temp = $xml->userbunit[0];
//        $_SESSION[$ps_sessionno.'userbunit'] = $s_temp;
//        $_SESSION[$ps_sessionno.'userbunit'] = "darwin";
//    }
//    if (isset($xml->timezone[0])){
//        $s_temp = $xml->timezone[0];
//        echo "timezone s_temp=".$s_temp;
//        echo "<br>timezone s_temp=".$s_temp;
//        $_SESSION[$ps_sessionno.'ut_logon_timezone'] = $s_temp;
//    $_SESSION[$ps_sessionno.'ut_logon_timezone'] = "Australia/Darwin";
//    }

//ECHO "<BR>utmenu2 setpref start ";
//echo "<br>".print_r($_SESSION);
//ECHO "<BR>utmenu2 setpref end ";
//echo "<br>".print_r(str_replace("<","openbracket^",$s_user_preferences));
//ECHO "<BR>utmenu2 setpref end ";
B900_END:

Z900_EXIT:
    return $sys_out;
}

  ?>
