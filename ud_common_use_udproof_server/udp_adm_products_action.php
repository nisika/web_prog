<?php

function pf_customError($errno,$errstr,$errfile,$errline,$errcontext)
  {
// NOTE **** THIS ONLY CATCHES NON-FATAL ERRORS
//  echo "<b>Error gw12:</b> [$errno] $errstr $errfile $errline $errcontext.<br>";
// gw 20100315       switch ($error_level) {
       switch ($errno) {
        case E_USER_ERROR:
            echo "<b>My ERROR</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            echo "  Fatal error on line $errline in file $errfile";
            echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
            echo "Aborting...<br />\n";
            exit(1);
            break;

        case E_USER_WARNING:
            echo "<b>My WARNING</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        case E_USER_NOTICE:
            echo "<b>My NOTICE</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        default:
            echo "<BR>Unknown error type: [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;
        }

    /* Don't execute PHP internal error handler */
    return true;

  }


$ko_map_path="";

A000_SET_RUN:
     //set error handler
    set_error_handler("pf_customError", E_ALL);
    date_default_timezone_set('Australia/Brisbane');
    session_start();

    if (isset($_SESSION['ko_prog_path']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
    }else{
    $_SESSION['ko_prog_path'] = "";
    }
    if (isset($_SESSION['ko_dbase_to_connectto']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
    }else{
    $_SESSION['ko_dbase_to_connectto'] = "tdxpryda";
    }

    if (isset($_SESSION['ko_map_path']))
    {
//    ECHO "        ko_prog_path is set  ".$_SESSION['ko_prog_path'];
        $ko_map_path = $_SESSION['ko_map_path'];
    }else{
    $_SESSION['ko_map_path'] = "";
    }

A000_DEFINE_VARIABLES:
    $sys_prog_name = "";
    $sys_debug = '';
    $s_file_exists = '';
    $s_filename="";
    $map = "";
    $tmp_array = array();
    $sys_function_out = "";
    $s_temp_value = "";

A200_LOAD_LIBRARIES:
    $sys_debug = strtoupper("NO");
//    $sys_debug = strtoupper("yes");

     IF ($sys_debug == "YES"){echo $sys_prog_name." started debug=".$sys_debug." *** remember to view source - it will save you hours  <br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_sql.php');
     $class_sql = new wp_SqlClient();
     IF ($sys_debug == "YES"){echo $sys_prog_name." after class_sql<br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_main.php');
     $class_main = new clmain();
     IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_main <br>";};

     require_once($_SESSION['ko_map_path'].'lib/class_app.php');
     $class_apps = new AppClass();

     IF ($sys_debug == "YES"){echo "_get=";print_r($_GET);echo "<br>";};
     IF ($sys_debug == "YES"){echo "_post=";print_r($_POST);echo "<br>";};

A300_CONNECT_TODBASE:
     $dbcnx = $class_sql->c_sqlclient_connect();

    $s_action = "";
    $deviceId = "";

A400_GET_PARAMETERS:
     if (isset($_GET['action'])) $s_action = $_GET["action"];
     if (isset($_GET['GlobalProductId'])) $s_GlobalProductId = $_GET["GlobalProductId"];

//     echo "<br>deviceId=".$deviceId;
//     echo "<br>s_action".$s_action;

A500_SET_VALUES:
    $s_action = strtoupper(trim($s_action," "));
    $s_GlobalProductId = intval(trim($s_GlobalProductId," "));

//     echo "<br>deviceId a=".$deviceId;
//     echo "<br>s_action a ".$s_action;
B100_PROCESS:

    if ($s_action =="ACTIVATE")
    {
        $active = 'active';
        GOTO B210_UPDATE;
    }
    if ($s_action =="DEACTIVATE")
    {
        $active = 'deactive';
        GOTO B210_UPDATE;
    }

    die("udp_adm_products_action = unknown action of ".$s_action);

B210_UPDATE:
    $sql = "UPDATE Products SET Status='".$active."' where GlobalProductId=".$s_GlobalProductId." LIMIT 1";

    $result = mysql_query($sql,$dbcnx);
    if (!$result) {
        die('udp_adm_products_action.php  - Invalid query: ' . mysql_error()."<br>sql=".$sql);
    }

A900_END:
B000_START:


Z800_GET_NEXT:

Z700_END:
Z900_EXIT:
//echo "do the redirect";
        // redirect to self to remove get variables
//        header("Location: http://fff".$_SERVER['HTTP_HOST']."ut_menu.php?fnid=208&p=|%!start_SITEPARAMS_!end%|");
    $s_do_url =  "http://dev.tdx.com.au/als_web_prog/ut_menu.php?fnid=208&p=|%!start_SITEPARAMS_!end%|";
    $s_do_url = STR_REPLACE("#P","|",$s_do_url);
    $s_do_url = STR_REPLACE("#p","|",$s_do_url);
//    $s_do_url = $class_main->clmain_v200_load_line($s_do_url,$ps_details_def,$ps_details_data,"no",$ps_sessionno,"ut_action b900 ");

    $s_do_url = "http://".$_SERVER['HTTP_HOST']."/als_web_prog/".$_SESSION['ko_live_or_dev']."/ut_menu.php?fnid=208&p=u^";
    header("Location: $s_do_url");
  //  echo $s_do_url;
  //      die;
        exit;

    ?>