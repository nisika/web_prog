<?php
function pf_customError($errno,$errstr,$errfile,$errline,$errcontext)
  {
// NOTE **** THIS ONLY CATCHES NON-FATAL ERRORS
//  echo "<b>Error gw12:</b> [$errno] $errstr $errfile $errline $errcontext.<br>";
// gw 20100315       switch ($error_level) {
       switch ($errno) {
        case E_USER_ERROR:
            echo "<b>My ERROR</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            echo "  Fatal error on line $errline in file $errfile";
            echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
            echo "Aborting...<br />\n";
            exit(1);
            break;

        case E_USER_WARNING:
            echo "<b>My WARNING</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        case E_USER_NOTICE:
            echo "<b>My NOTICE</b> [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;

        default:
            echo "<BR>Unknown error type: [$errno] $errstr line $errline filename $errfile context $errcontext<br />\n";
            break;
        }

    /* Don't execute PHP internal error handler */
    return true;

  }


A000_SET_RUN:
     //set error handler
set_error_handler("pf_customError", E_ALL);
date_default_timezone_set('Australia/Brisbane');

// A000_DEFINE_VARIABLES
    session_start();

//echo "<br>".print_r($_SESSION);

function pf_set_sessionid($ps_loginname)
{

    //
    // should be done in kickoff.php so this code should never run

    // print_r($_SESSION);
            echo "way pf_set_sessionid many  exit ";
    $sSessionid = "500";
    $sCheckSid = $sSessionid."udSid";
    while (isset($_SESSION[$sCheckSid])):
        $sSessionid = $sSessionid + 1;
        $sCheckSid = $sSessionid."udSid";
        IF ($sSessionid > 508 ) // want to keep the session no at a single digit
        {
//            echo "error setting id";
            break;
        }
        IF ($sSessionid > 520 )
        {
            echo "way too many  exit ";
            exit;
        }
    endwhile;
    $_SESSION[$sCheckSid] = $sCheckSid;
    return $sSessionid;
}

    $sys_debug = '';
    $sys_prog_name = "logon1";

    $loginname = "";
    $password = "";
    $s_sessionno = "";
    $action = "";
    $s_params = "";
    $s_sys_language = "";
    $s_action_type = "";
    $s_ret_map = "";
    $s_error_map = "";
    $s_ok_map="";

// A100_INITIALISE_VARIABLES
    $action = "ERROR";
    $s_ret_map = "ut_logon_screen.html";
    $s_error_map = "ut_logon_error_snippet";
    $s_ok_map="udp_p1.htm";
    $s_user_status_or_id = "";

//A200_LOAD_LIBRARIES
    $sys_debug = strtoupper("NO");
     IF ($sys_debug == "YES"){echo $sys_prog_name." started debug=".$sys_debug."<br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_sql.php');
     $class_sql = new wp_SqlClient();
     IF ($sys_debug == "YES"){echo $sys_prog_name." after class_sql<br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_main.php');
     $class_main = new clmain();
     IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_main <br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_myplib1.php5');
     $class_myplib1 = new myplib1();
     IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_myplib1<br>";};
     IF ($sys_debug == "YES"){echo "_get=";print_r($_GET);echo "<br>";};
     IF ($sys_debug == "YES"){echo "_post=";print_r($_POST);echo "<br>";};

//A300_CONNECT_TODBASE
     $dbcnx = $class_sql->c_sqlclient_connect();

     //A400_GET_PARAMETERS
     if (isset($_POST['ln'])) $loginname = $_POST["ln"];
     if (isset($_POST['pw'])) $password = $_POST["pw"];
     if (isset($_POST['params'])) $s_params = $_POST["params"];
     if (isset($_POST['sys_language'])) $s_sys_language = $_POST["sys_language"];
     if (isset($_POST['action_type'])) $s_action_type = $_POST["action_type"];
     if (isset($_POST['ret_map'])) $s_ret_map = $_POST["ret_map"];
     if (isset($_POST['error_map'])) $s_error_map = $_POST["error_map"];
     if (isset($_POST['ok_map'])) $s_ok_map = $_POST["ok_map"];

     IF ($sys_debug == "YES"){print_r($sys_prog_name."  ".$_POST);};


//A500_SET_VALUES
// if no values in params yet it will only contain ^
//    if (strlen($s_params) < 2 ){
// if the st char is a ^ then no session id has been set

    if (strpos($s_params,"^") == 0) {
        IF ($sys_debug == "YES"){print_r($sys_prog_name."getting new session id");};
        $s_sessionno = pf_set_sessionid($loginname);
    }else{
        $s_sessionno = substr($s_params,0,strpos($s_params,"^"));
    }
    IF ($sys_debug == "YES"){print_r($sys_prog_name."session id = ".$s_sessionno);};


    $username = $loginname;
    $s_error_message = "noerror";
    if (TRIM($password) == ""){
        $s_error_message="INSERTCODE_".$s_error_map."_E2003";
     }
    if (TRIM($username) == ""){
        $s_error_message="INSERTCODE_".$s_error_map."_E2002";
     }

     if ($s_action_type =="SETLANG")
     {
        $s_error_message="INSERTCODE_".$s_error_map."_E2004";
        setcookie("ud_sys_language",$s_sys_language,time()+60*60*24*30);
     }


//A600_CHECK_USERNAME
    if ($s_error_message <> "noerror")
    {
        goto A700_SET_SESSION_VALUES;
    }

    setcookie('ud_logonname',$username,time()+60*60*24*30);
     $s_user_status_or_id = $class_main->clmain_B200_check_user($dbcnx,$username,$password,"NO",$s_sessionno);
     IF ($sys_debug == "YES"){echo "<br>USER STATUS = ".$s_user_status_or_id."<br>";};

//     if (strpos(strtoupper($s_user_status_or_id), "ERROR" ) > 0)
     if (strpos(strtoupper($s_user_status_or_id),"*ERROR") === false )
      {
        GOTO A610_USER_GOOD;
      }

A605_CHECK_UTL_SYSTEM_USER:
     $s_user_status_or_id = $class_main->clmain_B214_get_utl_pref_user($dbcnx,$username,$password,"NO",$s_sessionno);
     IF ($sys_debug == "YES"){echo "<br>USER STATUS = ".$s_user_status_or_id."<br>";};
     if (strpos(strtoupper($s_user_status_or_id),"*ERROR") === false )
     {
         GOTO A610_USER_GOOD;
     }

A605_CHECK_DMA:
//  check the users table instead of the user table
// if exists goto a610_user_good
    $s_sql2 = "SELECT * FROM user WHERE id='".$username."'(SKIPERRORFAIL)";
    $result2 = $class_sql->c_sqlclient_exec_query($dbcnx,$s_sql2);
    if($result2 === false)
    {
        GOTO A620_USER_ERROR;
    }
    if ($row2 = mysqli_fetch_array($result2, MYSQLI_ASSOC))
    {
        $s_user_status_or_id = "Possible password error (dma)";
        if($row2['Password'] == $password)
        {
            $s_user_status_or_id = "DMA";
            GOTO A610_USER_GOOD;
        }
    }

    GOTO A620_USER_ERROR;

A610_USER_GOOD:
     IF ($sys_debug == "YES"){echo "<br>USER GOOD STATUS = ".$s_user_status_or_id."<br>";};
     setcookie('ud_logonname',$username,time()+60*60*24*30);
     $action = "MAINPAGE";
     IF ($sys_debug == "YES"){echo "BEFORE PREFERENCES <br>";};
     $user_preferences = pf_set_user_preferences($dbcnx,$s_sessionno,$s_user_status_or_id);
     IF ($sys_debug == "YES"){echo "<br>AFTER PREFERENCES user_preferences=".$user_preferences."<br>";};

//echo "<br>".print_r(str_replace("<","openbracket^",$user_preferences));
//ECHO "<BR>utmenu user_preferences end ";


     if (strpos(strtoupper($user_preferences),"*ERROR") !== false )
     {
         IF ($sys_debug == "YES"){echo "<br>doing preference error <br>";};
         GOTO A700_SET_SESSION_VALUES;
      }
     //     $s_ok_map = s
// user status will return the xml datablob
    $xml = new SimpleXMLElement($user_preferences);


//echo "<br>".print_r(str_replace("<","openbracket^",$xml));
//ECHO "<BR>utmenu setpref end ";

//ECHO "<BR>ut_logon user perferences start user_preference  ";
//echo "<br>".print_r($_SESSION);
//ECHO "<BR>ut_logon user perferencesend user_preference  ";


     IF ($sys_debug == "YES"){echo "<br>AFTER SIMPLE XML<br>";};
    if (isset($xml->logon_initpage[0])){
        $s_ok_map =$xml->logon_initpage[0];
//echo "<br>".print_r(str_replace("<","openbracket^",$s_ok_map));
//ECHO "<BR>utmenu $s_ok_map end ";

    }
     IF ($sys_debug == "YES"){"<br>s_ok_map = ".$s_ok_map."";};

     GOTO A700_SET_SESSION_VALUES;

A620_USER_ERROR:
     $s_error_message = TRIM($s_user_status_or_id);
     $s_error_id = str_replace("*","",$s_user_status_or_id);
     $s_error_id = str_replace("ERROR ","",$s_error_id);
     $s_error_message = $s_error_id;
     IF (strtoUpper(TRIM($s_error_id)) == "E2001_UNKNOWN_USER")
     {
        $s_error_message="INSERTCODE_".$s_error_map."_E2001";//, "UNKNOWN_USER");
     }
     IF (strtoUpper(TRIM($s_error_id)) == "E2006_WRONG_PWD")
     {
        $s_error_message="INSERTCODE_".$s_error_map."_E2006";//, "UNKNOWN_USER");
     }
     GOTO A700_SET_SESSION_VALUES;

A700_SET_SESSION_VALUES:
    $s_siteparams = "".$s_sessionno."^";
    $_SESSION[$s_sessionno.'username'] = $username;
    $s_siteparams = "".$s_sessionno."^"."logonid"."^";

    if ($s_user_status_or_id <> "DMA")
    {
        goto A750_SKIP_DMA;
    };
    $_SESSION[$s_sessionno.'dma_logon_name'] = $row2['Name'];
    $_SESSION[$s_sessionno.'dma_logon_company'] = $row2['Company'];
    $_SESSION[$s_sessionno.'dma_logon_email'] = $row2['Email'];
    $_SESSION[$s_sessionno.'dma_logon_accounts_access'] = $row2['Accounts_access'];
    $_SESSION[$s_sessionno.'dma_logon_role'] = $row2['role'];
    $_SESSION[$s_sessionno.'dma_logon_transport_company'] = $row2['transport_company'];

    $_SESSION[$s_sessionno.'dma_logon_company_id_sql'] = "and company_id in ('".$row2['Accounts_access']."')";
    $_SESSION[$s_sessionno.'dma_logon_company_id_sql'] = str_replace(",","','",$_SESSION[$s_sessionno.'dma_logon_company_id_sql']);
    if (strtoupper(trim($row2['Accounts_access'])) == "ALL")
    {
        $_SESSION[$s_sessionno.'dma_logon_company_id_sql'] = " ";
        GOTO A710_COMPANY_ID_DONE;
    }
    if (strpos(trim($row2['Accounts_access']),"%") > 0)
    {
        $_SESSION[$s_sessionno.'dma_logon_company_id_sql'] = "and company_id like '".$row2['Accounts_access']."'";
        GOTO A710_COMPANY_ID_DONE;
    }

    $s_logon_company_id_sql =  "and ('".$row2['Accounts_access']."')";
    $s_logon_company_id_sql = str_replace(",","','",$s_logon_company_id_sql);
    $s_logon_company_id_sql = str_replace("('","(company_id = '",$s_logon_company_id_sql);
    $s_logon_company_id_sql = str_replace(","," or company_id = ",$s_logon_company_id_sql);

    $_SESSION[$s_sessionno.'dma_logon_company_id_sql'] = $s_logon_company_id_sql;

A710_COMPANY_ID_DONE:

A750_SKIP_DMA:

A790_END:
     IF ($sys_debug == "YES"){die ("<br><br>utlogon die in debug - action = ".$action." s_ok_map=".$s_ok_map." s_ret_map=".$s_ret_map.' s_siteparams='.$s_siteparams.") <br>");};


//    echo "<br> action  = ".$action."<br>";
//    echo "<br> s_ok_map  = ".$s_ok_map."<br>";

//die ("<br> gwdie sessionno=.".$ps_sessionno);

    //B100_ACTION
  if (trim($action) == "ERROR"){
    $s_details_def = "|ERROR_MESSAGE";
    $s_details_data = "|".$s_error_message;
    $_SESSION[$s_sessionno.'session_def'] = $s_details_def;
    $_SESSION[$s_sessionno.'session_data'] = $s_details_data;
    echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
    echo '<html>';
    echo '<head>';
    echo '<meta HTTP-EQUIV="REFRESH" content="0; url=ut_menu.php?fnid=101&map='.$s_ret_map.'&p='.$s_siteparams.'">';
    echo '</head>';
    echo '</html>';
    exit;
  }

  if (trim($action) == "MAINPAGE"){
    echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
    echo '<html>';
    echo '<head>';
    echo '<meta HTTP-EQUIV="REFRESH" content="0; url=ut_menu.php?fnid=101&map='.$s_ok_map.'&p='.$s_siteparams.'&setupuserid='.$s_sessionno.'|'.$s_user_status_or_id.'">';
    echo '</head>';
    echo '</html>';
    exit;
  }

//    echo '<meta HTTP-EQUIV="REFRESH" content="0; url=ut_menu.php?fnid=101&map='.$s_ok_map.'&p='.$s_siteparams.'&setupuserid='.$s_sessionno.'|'.$s_user_status_or_id.'&dieme=yes">';

    echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
    echo '<html>';
    echo '<head>';
    echo '</head>';
    echo '<body> There is no option for this funtion '.$function_id.'<BR> USERNAME='.$username.'<BR> PWD='.$password.'<BR> SESSIONID='.$s_sessionno.'</body>';
// echo "<br><br><br>".print_r($_SESSION);
    echo '</html>';
    exit;

//########################################################################################################################################################################################


function pf_set_user_preferences($ps_dbcnx,$ps_sessionno,$ps_userid)
{
// get the utl_users_preferences record if the table exists
// set the preferences based on the values in utl_users_preferences.data_blob
    global $class_main;
/* gw20110415 done in utmenu
    $s_deviceframe_width = "";
    $_SESSION[$ps_sessionno.'udp_site_css'] = "udp_site.css";
    $_SESSION[$ps_sessionno.'udp_bdyleft_css'] = "udp_bdyleft.css";
    $_SESSION[$ps_sessionno.'udp_navleft_css'] = "udp_navleft.css";
    $_SESSION[$ps_sessionno.'udp_bdymain_css'] = "udp_bydmain.css";

    $_SESSION[$ps_sessionno.'udj_site_css'] = "udj_site.css";

    $s_deviceframe_width = "150";
    $_SESSION[$ps_sessionno.'udp_deviceframe_width'] = $s_deviceframe_width;
    $_SESSION[$ps_sessionno.'udp_deviceframe_tabwidth'] = $s_deviceframe_width - 20;
    $_SESSION[$ps_sessionno.'udp_devicedesc'] = "User";

    $_SESSION[$ps_sessionno.'userbunit'] = "JOBMANAGEMENT";
    $_SESSION[$ps_sessionno.'ut_logon_timezone'] = "Australia/Sydney";

    $_SESSION['gwut_logon_timezone'] = "Australia/Sydney";

*/
    $sys_out = "";
    $sys_out = $sys_out.'<?xml version="1.0" encoding="ISO-8859-1"?><data_blob>';
    $sys_out = $sys_out.'<no_preferences>NoPreferences</no_preferences>';
    $sys_out = $sys_out.'</data_blob>';

A100_GET_USER:
     $s_user_preferences = $class_main->clmain_B210_get_user_preferences($ps_dbcnx,$ps_userid,"NO",$ps_sessionno);
     $sys_out= $s_user_preferences;
B100_SET_SESSION_VARS:

//echo "<br>".print_r(str_replace("<","openbracket^",$s_user_preferences));
//ECHO "<BR>utmenu setpref end ";

//ECHO "<BR>ut_logon user perferences start user_preference  ";
//echo "<br>".print_r($_SESSION);
//ECHO "<BR>ut_logon user perferencesend user_preference  ";
//die ("<br> gwdie sessionno=.".$ps_sessionno);
     if (strpos(strtoupper($s_user_preferences),"*ERROR") !== false )
     {
         GOTO B900_END;
      }
    $xml = new SimpleXMLElement($s_user_preferences);
    if (isset($xml->userbunit[0])){
        $_SESSION[$ps_sessionno.'userbunit'] = (string)$xml->userbunit[0];
// gw 201111        $_SESSION[$ps_sessionno.'userbunit'] = "ut_logon_gwtst";
    }
    if (isset($xml->timezone[0])){

// gw 201111        echo "ut_logon<br>";
        $s_temp = (string)$xml->timezone[0];
// gw 201111        echo "s_temp = ".$s_temp."<br>";
// gw 201111        echo "timezone[0] = ".$xml->timezone[0]."<br>";
       $_SESSION[$ps_sessionno.'ut_logon_timezone'] = $s_temp ;
// gw 201111        echo "session  timezone = ".$_SESSION[$ps_sessionno.'ut_logon_timezone']."<br>";
        // gw 201111IF (is_string($s_temp))
// gw 201111        {
// gw 201111               echo "IS A STRING = ".$s_temp."<br>";
// gw 201111        }else{
// gw 201111               echo "IS not A STRING = ".$s_temp." is ".gettype($s_temp)."<br>";
// gw 201111        }

        // gw 20111108 causes an error on windows so hardcoded this      $_SESSION[$ps_sessionno.'ut_logon_timezone'] = 'Australia/Brisbane';

// gw 201111     die ("gwdied set time zone session var   $ps_sessionno ".$_SESSION[$ps_sessionno.'ut_logon_timezone']);
    }



B900_END:

Z900_EXIT:
    return $sys_out;
}
  ?>
