<?php
/*<!--
history
gjw  20110405 - create from mypalletbook help
-->
*/
    set_error_handler("pf_customError", E_ALL);
    date_default_timezone_set('Australia/Brisbane');
    session_start();

    //gw20110921 - added user timezone
    if (isset($_SESSION[$s_sessionno.'ut_logon_timezone']))
    {
        $timezone=$_SESSION[$s_sessionno.'ut_logon_timezone'];
        date_default_timezone_set($timezone);
    }else{
    }



     IF ($sys_debug == "YES"){echo $sys_prog_name." started debug=".$sys_debug." *** remember to view source - it will save you hours  <br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_sql.php');
     $class_sql = new wp_SqlClient();
     IF ($sys_debug == "YES"){echo $sys_prog_name." after class_sql<br>";};
     require_once($_SESSION['ko_prog_path'].'lib/class_main.php');
     $class_main = new clmain();
     IF ($sys_debug == "YES"){echo  $sys_prog_name." after class_main <br>";};

     $dbcnx = $class_sql->c_sqlclient_connect();


/*
function lf_setquantity($ps_quantity,$ps_activity)
{
    $s_quantity_calc = "0";
    $s_activity = strtolower(trim($ps_activity));

    if ($s_activity == "givetothem")
    {  $s_quantity_calc = $ps_quantity * -1;
    }; // off mypallet balance
    if ($s_activity == "receievedfromthem")
    {  $s_quantity_calc = $ps_quantity * 1;
    };  // onto mypallet balance
    if ($s_activity == "exchange" )
    {  $s_quantity_calc = $ps_quantity * 0;
    };  // no effect on mypallet balance
    if ($s_activity == "transferto" )
    {  $s_quantity_calc = $ps_quantity * -1;
    }; // onto mypallet balance
    if ($s_activity == "transferfrom" )
    {  $s_quantity_calc = $ps_quantity * 1;
    }; // off mypallet balance
    if ($s_activity == "hire")
    {  $s_quantity_calc = $ps_quantity * 1;
    };  // onto mypallet balance
    if ($s_activity == "dehire")
    {  $s_quantity_calc = $ps_quantity * -1;
    }; // off mypallet balance
    if ($s_activity == "reqdrop")
    {  $s_quantity_calc = $ps_quantity * 0;
    };  // no effect on mypallet balance

    return $s_quantity_calc;
}
function lf_setUK($ps_UK)
{
    $s_UKCnt = 1;
    $s_UK = $ps_UK;
//   see if the $s_UK exists in the table
// if it does then add 1 to $s_UKCnt
// $s_UK = $ps_UK.$s_UKcnt
    return $s_UK;
}
function lf_set_length($ps_length, $ps_field,$ps_addchar,$ps_addto)
{
    $s_string = $ps_field;
    while (strlen($s_string) < $ps_length)
    {
        if (strtoupper(trim($ps_addto))=="START")
            {$s_string = $ps_addchar.$s_string ;
        }else
            {$s_string = $s_string.$ps_addchar ;
        }

    }
    return $s_string ;
}
*/
   $s_debug = "";
     $s_debug = "NO";
    $s_helpuk = "";
    $s_mypalletid = "";
    $s_helpuk = "";
    $s_helplocation = "";
    $s_notes = "";

   $s_utlh_id  = "";
   $s_utlh_userid  = "";
   $s_utlh_helplocation  = "";
   $s_utlh_recgroup  = "";
   $s_utlh_rectype  = "";
   $s_companyid  = "";
   $s_createddnt   = date("YmdHisu");
   $s_createduserid  = "";
   $s_updateddnt   = date("YmdHisu");
   $s_updateduserid  = $_SESSION['ud_companyid'];
   $s_createdutime  = time();
   $s_updatedutime  = time();
   $s_data_blob  = "";
   $s_summary  = "";

   if (isset($_POST['utlh_id'])) $s_utlh_id   = $_POST["utlh_id"];
   if (isset($_POST['utlh_userid'])) $s_utlh_userid   = $_POST["utlh_userid"];
   if (isset($_POST['utlh_helplocation'])) $s_utlh_helplocation   = $_POST["utlh_helplocation"];
   if (isset($_POST['utlh_recgroup'])) $s_utlh_recgroup   = $_POST["utlh_recgroup"];
   if (isset($_POST['utlh_rectype'])) $s_utlh_rectype   = $_POST["utlh_rectype"];
   if (isset($_POST['companyid'])) $s_companyid   = $_POST["companyid"];
   if (isset($_POST['data_blob'])) $s_data_blob   = $_POST["data_blob"];
   if (isset($_POST['summary'])) $s_summary   = $_POST["summary"];

   $s_data_blob = str_replace("'","",$s_data_blob);
   $s_summary = str_replace("'","",$s_summary);

   $s_helplocation = $s_utlh_helplocation;
   if (strtoupper($s_utlh_userid) == "SYSADMIN")
   {
       $s_helplocation = substr($s_helplocation,strpos($s_helplocation,":"));
   }

    if ($s_utlh_id == "NEWREC")
    {
        $s_utlh_id = date("ymdHis");
        $sql="INSERT INTO utl_help (utlh_id,utlh_userid,utlh_helplocation,utlh_recgroup,utlh_rectype,companyid,createddnt,createduserid,updateddnt,updateduserid,createdutime,updatedutime,data_blob,summary)
            VALUES
            ('".$s_utlh_id."','".$s_utlh_userid."','".$s_helplocation."','".$s_utlh_recgroup."','".$s_utlh_rectype."','".$s_companyid."','".$s_createddnt."','".$s_createduserid."','".$s_updateddnt."','".$s_updateduserid."','".$s_createdutime."','".$s_updatedutime."','".$s_data_blob."','".$s_summary."')";
        $s_utlh_id = "NEWREC";
    }



    if ($s_utlh_id !== "NEWREC")
    {
        $sql="UPDATE utl_help SET utlh_userid='".$s_utlh_userid."',utlh_helplocation='".$s_helplocation."',utlh_recgroup='".$s_utlh_recgroup."',utlh_rectype='".$s_utlh_rectype."',companyid='".$s_companyid."',updateddnt='".$s_updateddnt."',updateduserid='".$s_updateduserid."',updatedutime='".$s_updatedutime."',data_blob='".$s_data_blob."',summary='".$s_summary."' WHERE utlh_id = '".$s_utlh_id."'";
    }

    if (!mysql_query($sql,$dbcnx))
   {echo( "<P> ut_help_update. - Unable to do sql <br> ".$sql.".  <br>     connection: ".$dbcnx1." sql   Error: ".mysql_error()."</P>" );
    exit();
    }

    mysql_close($dbcnx);
    $s_map_name = "ut_help_update_map.html";
    $s_map_group = "help_map";
    echo $class_main->clmain_v100_load_html_screen($s_map_name,$s_details_def,$s_details_data,"NO",$s_map_group);

?>
