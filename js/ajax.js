var is_open = null;

function createXHR()
{
    var request = false;
        try {
            request = new ActiveXObject('Msxml2.XMLHTTP');
        } catch(err2) {
	        try {
	            request = new ActiveXObject('Microsoft.XMLHTTP');
	        } catch(err3) {
				try {
					request = new XMLHttpRequest();
				} catch (err1)  {
					request = false;
				}
            }
        }
    return request;
}

function jsl_ajax_dopost(page,data,callback,load_id) {
	var xhr = createXHR();
	xhr.onreadystatechange  = function() {
		if(load_id!=null) {
			var load = document.getElementById(load_id);
			load.style.backgroundImage="url(images/loadinfo.gif)";
			load.style.backgroundRepeat="no-repeat";
			load.style.backgroundPosition="center right";
		}
		if(xhr.readyState  == 4) {
			if(xhr.status  == 200) {
				if(load_id!=null) {
					load.style.backgroundImage="url(none)";
					load.style.backgroundRepeat="";
					load.style.backgroundPosition="";
				}

//test
				var json = eval("(" + xhr.responseText + ")");
				callback(json);
			} else {
				callback(xhr.status);
			}
		}
	};
	xhr.open("POST", page, true);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.send(data);
}


function jsl_ajax_dopost_v2(page,data,callback,load_id) {
    var xhr = createXHR();
    xhr.onreadystatechange  = function() {
        if(load_id!=null) {
            var load = document.getElementById(load_id);
            load.style.backgroundImage="url(images/loadinfo.gif)";
            load.style.backgroundRepeat="no-repeat";
            load.style.backgroundPosition="center right";
        }
        if(xhr.readyState  == 4) {
            if(xhr.status  == 200) {
                if(load_id!=null) {
                    load.style.backgroundImage="url(none)";
                    load.style.backgroundRepeat="";
                    load.style.backgroundPosition="";
                }

//test
                var json = eval("(" + xhr.responseText + ")");
                callback(load_id,json);
            } else {
                callback(load_id,xhr.status);
            }
        }
    };
    xhr.open("POST", page, true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send(data);
}

function jsl_ajax_check_key(e) {
	var key;
	var keychar;
	if (window.event) {
		key = window.event.keyCode;
	} else if (e) {
		key = e.which;
	} else {
		return true;
	}
	keychar = String.fromCharCode(key);
	keychar = keychar.toLowerCase();

	re = /[\x20-\x7F]/;
	return re.test(keychar);
}

function check_data(data) {
	if(data==null) {
		return "";
	} else {
		return data;
	}
}

function clickedOutsideElement(elemId) {
    var theElem = getEventTarget(window.event);
    while(theElem != null) {
        if(theElem.id == elemId)
            return false;
        theElem = theElem.offsetParent;
    }
    return true;
}

function getEventTarget(evt) {
    var targ = (evt.target) ? evt.target : evt.srcElement;
    if(targ != null) {
        if(targ.nodeType == 3)
            targ = targ.parentNode;
    }
    return targ;
}

document.onclick = function() {
    if(clickedOutsideElement(window.is_open) && window.is_open!=null) {
    	var obj = document.getElementById(window.is_open);
		obj.style.zIndex = 0;
		obj.style.display = "none";
		obj.style.height = 0;
		is_open = null;
    }
}